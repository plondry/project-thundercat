# Known issues and troubleshooting resolution techniques

---

# General Docker issues

## Docker and the command line

- Cannot use VM on windows 10 and Docker at the same time
- Cannot VPN into network and use Docker (nginx crashes) when working remotely
  - This seems to be related to the network drives?
  - If docker is running and then I vpn into the network, the computer detects no network drives
  - After running 'docker-compose down' and restating docker, the network drives appear
- Cannot execute 'docker exec' command in git bash
- If you are using DockerToolbox rather than Docker for Windows on Windows, see rm-docker-toolbox-setup

## Docker doesn't want to succeed for some reason

If you have run Docker pretty extensively in the past, try running `docker image prune -a`, `docker volume prune`, and `docker system prune`. Doing so will remove any images & force them to be obtained & rebuilt from scratch.

## Cannot start service nginx

On Windows 10, sometimes docker shows the following errors when starting up

```shell
$ docker-compose up
Creating network "project-thundercat_default" with the default driver
Creating project-thundercat_frontend_1 ... done
Creating project-thundercat_db_1       ... done
Creating project-thundercat_backend_1  ... done
Creating project-thundercat_nginx_1    ... error

ERROR: for project-thundercat_nginx_1  Cannot start service nginx: OCI runtime create failed: container_linux.go:344: starting container process caused "process_linux.go:424: container init caused \"rootfs_linux.go:58: mounting \\\"/host_mnt/c/_DEV/git/project-thundercat/nginx/nginx-proxy.conf\\\" to rootfs \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged\\\" at \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged/etc/nginx/conf.d/default.conf\\\" caused \\\"not a directory\\\"\"": unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type

ERROR: for nginx  Cannot start service nginx: OCI runtime create failed: container_linux.go:344: starting container process caused "process_linux.go:424: container init caused \"rootfs_linux.go:58: mounting \\\"/host_mnt/c/_DEV/git/project-thundercat/nginx/nginx-proxy.conf\\\" to rootfs \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged\\\" at \\\"/var/lib/docker/overlay2/393d58faaa3c6a244293fcff1a14b5bb93f9d2aec735e29346454824d30556c3/merged/etc/nginx/conf.d/default.conf\\\" caused \\\"not a directory\\\"\"": unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type
Encountered errors while bringing up the project.
```

To solve this, do the following steps

Ensure the application is not running

```shell
docker-compose down
```

Run PowerShell as admin.

Run

```shell
Set-NetConnectionProfile -interfacealias "vEthernet (DockerNAT)" -NetworkCategory Private
```

Close powershell

Restart docker (right-click on the docker icon -> restart)

After the restart, run

```shell
docker-compose up
```

And everything should be working again

## Drive sharing errors

if you get a similar error to the following:  
`ERROR: for project-thundercat_backend_1 Cannot create container for service backend: b'Drive sharing seems blocked by a firewall'`  
Then the following steps can be performed to remedy the situation.

1. Go to the network adapters
2. Go to DockerNAT adapter properties
3. Uninstall File and Printer Sharing
4. Install File and Printer Sharing under Microsoft
5. Run the the "error 1" instructions above

All errors should be resolved when using docker-compose up

## [emerg] 1#1: host not found in upstream "backend:8000" in /etc/nginx/conf.d/default.conf:3

After making changes to the docker config, nginx will not start properly

To fix this, run

```shell
docker-compose up --build
```

rather than

```shell
docker-compose up
```

## Changes from DockerFile need to be applied

If for some reason changes from a DockerFile need to be applied (e.g. transitioning from an older version of CAT that used PostgreSQL and multiple changes to the DockerFile were made to use MS SQL instead), then you have to run this command to rerun the logic in the DockerFile:

```sh
docker-compose build
```

This rebuilds the Docker containers from scratch.

Example of an error encountered that indicates that a rebuild for the Docker containers may need to be done:

![transition-from-old-thundercat-build-error](images/transition-from-old-thundercat-build-error.png)

# Backend & Database Containers

## Unable to get MS SQL related files in a volume

Delete the `C:\MSSQL` folder and then try doing docker-compose up again.

Example of an error of this nature:

![mssql-loading-error](images/mssql-loading-error.png)

## Backend cannot connect to the Database Docker container (invalid login credentials)

Simply either wait a little longer or `Ctrl+C` to gracefully shutdown the other Docker containers & then do `docker-compose up` to restart the Docker compose process. The issue here is that the Database container may take a longer time to finish loading than the Backend container, so the Backend container will automatically try again after some time and will be able to connect to the Database container properly.

Example screenshots of this error:

![docker-invalid-db-credentials-error](images/docker-invalid-db-credentials-error.png)

![docker-invalid-db-credentials-error-2](images/docker-invalid-db-credentials-error-2.png)

## MS SQL: out of memory error for running long SQL scripts

If you encounter this error:

`There is insufficient system memory in resource pool 'default' to run this query.`

Go to Docker -> Settings -> Advanced and make sure "Memory" is set to a higher value (recommended minimum: 4096 MB).

# Frontend

## Every single yarn test is failing

Run `yarn test --clearCache` and then run `yarn test` once again.

## Frontend container fails to build: react-scripts not found

If you encounter this error:

```shell
frontend_1  | /bin/sh: 1: react-scripts: not found
```

Try deleting the node_modules folder and making sure your `yarn.lock` file contents do not differ from the repo's.

Afterwards, run `docker-compose build` in your project, and then run `docker-compose up` afterwards.

## Frontend: you should not use <Link /> outside of <Router />

If you encounter the error shown here:

![link-outside-error-error](./images/link-outside-router-error.png)

Either one of 2 things are likely happening:

- You referenced a wrong localization string key in `frontend/src/resources/textResources.js` (in this case, please double-check the string key you are referencing)
- Redux is causing some kind of an infinite loop based on how the React components are set up (in this case, please review the [official React Component Lifecycle document](https://reactjs.org/docs/react-component.html) - it is likely you need to use `componentDidUpdate()` and/or `componentDidMount()`)

## Frontend: slow response times from rendering large lists

With the help of the Google Performance Profiler, it was determined that the `react-axe` dev dependency is causing a massive slowdown in our application when rendering large lists in the DOM.

- To avoid this error, the .env file now contains the following environment variable: `REACT_APP_ENABLE_REACT_AXE=false`
  - Note that all environment variables read into `process.env` (to be detected by the web app) must be prefixed with `REACT_APP_`.
  - Also note that `NODE_ENV` **cannot be overridden manually** (intentional by the React developers). This will automatically update depending on the command used to build & run the application.
  - See the official React docs for more details: https://create-react-app.dev/docs/adding-custom-environment-variables

---

# Archives (resolved issues)

### Port 80 error

```shell
PS C:\_DEV\IdeaProjects\thundercat\project-thundercat> docker-compose up
Creating network "project-thundercat_default" with the default driver
Creating project-thundercat_postgres   ... done
Creating project-thundercat_frontend_1 ... done
Creating project-thundercat_backend_1  ... done
Creating project-thundercat_nginx_1    ... error

ERROR: for project-thundercat_nginx_1  Cannot start service nginx: driver failed programming external connectivity on endpoint project-thundercat_nginx_1 (31bebcca94c04c380b7ad1af1bc492488d3bf3fb5b19db9356592e0b5a7028ed): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied

ERROR: for nginx  Cannot start service nginx: driver failed programming external connectivity on endpoint project-thundercat_nginx_1 (31bebcca94c04c380b7ad1af1bc492488d3bf3fb5b19db9356592e0b5a7028ed): Error starting userland proxy: Bind for 0.0.0.0:80: unexpected error Permission denied
ERROR: Encountered errors while bringing up the project.
```

To fix this issue, you simply need change the nginx port in _docker-compose.yml_ file. You can use ports: 81:8080. If you do that, make sure that you are not committing these changes to master.

And everything should be working again

**Note that this is a temporary fix and we need to discuss it later.**

## Fixing issues related to line terminator characters '...\r'

To permanently fix this issue, if adding .gitattributes alone does not fix the issue in the repo, please run some of the git commands here (it should resolve the issue after fresh cloning the repo after trying some of these git commands): https://stackoverflow.com/questions/2517190/how-do-i-force-git-to-use-lf-instead-of-crlf-under-windows

We have seen issues with how new lines are saved in different environemnts depending on where they were created. This _should_ be fixed with the `.vscode/settings.json` file and the `.gitattributes` file, but this will be kept in the document for archive purposes.

Open '.gitattributes'

Add '<file_pattern> -crlf'

Commit '.gitattributes'

Open file in question in notepad++

Open Find+Replace dialog

Replace '\r' with ''. Make sure that 'Search Mode' is in 'Extended mode'

i.e.

' /usr/bin/env: ‘python\r’: No such file or directory' in error message

Open '.gitattributes'

Add '\*.py -crlf'

Commit, replace in notepad++
