# Test Defintion

## Current Test Structure

### Overview of an emib test

- _test_ -> **Test** object; _item_text_ (with visual test name)
  - _address_book_
    - _contact_ -> _item_text_ (name and organization)
  - _overview_ -> _item_text_ in markdown format
  - _sections_
    - _section_ -> _item_text_ title to be displayed on the side tab; corresponds to a page in a sidenav object; each section may contain one or more children
      - _markdown_ -> _item_text_ in markdown format; page to be displayed
      - _tree_view_ -> _item_text_ containing the title; the root node of an org chart
        - _organizational_structure_tree_child_ -> _item_text_ containing the name; total org chart
          - _organizational_structure_tree_child_ -> recursive
        - _team_information_tree_child_ -> _item_text_ containing the name; team org chart
          - _team_information_tree_child_ -> recursive
  - _question_ -> **Question** object of type email; _item_text_ is not visible to the candidate for an email type question
    - _subject_ -> _item_text_ containing the visible subject
    - _from_ _item_text_ containing the visible from
    - _to_ _item_text_ containing the visible to
    - _date_ _item_text_ containing the visible date
    - _body_ _item_text_ containing the visible email body in markdown format

See more detailed explaination of the tables under Model Definition

## Future Steps

### Test Type Table?

**Test**'s _test_type_ will probably become a foreign key to a test type table. However, we currently have exactly one test type, so there is no need to flesh this out at the moment

### Instructions?

Instuction pages will likely be markdown items inside an instruction item. We may make this test specific (lots of duplication) or test_type specific (linked to a test_type item), linked to the test type object (each test type would have 1 set of instructions). This will need a lot of discussion.

### Multiple Choice Questions

Answers have not been implimented yet, as emib style tests are open answer. When they are implimented, answers will likely need to be an additional table, linked to an **Item** (like **Test** and **Question** currently are). The visible text will be stored in **ItemText**. Addtionally, **Item**s already have the concept of order and validity periods. However, an answer will likely also need:

- point value -> interger specifing how many points this answer is worth
- is_correct -> boolean specifying if this is the right answer (may be extraneous, considering point value?)
- other values?

**Question** may also need the following changes:

- new types

  - mulitple choice, single answer -> can select only one answer
  - multiple choice, multi answer -> can select more than one answer
  - multiple choice, ordered -> give an answers a weight/order

- scoring algorithm?
  - most automatically scored tests will be scored by summing the total value of all answers; however tests like the 840 require a more complex algorithm

### Cleanup concept of _sections_ and _section_ items

Sections is really a group of pages, while a section is a page. We should make these concepts clearer by using more rational names.

### Randomizer item

We will eventually need an item_type that randomly returns and orders a certain number of its children (all, or n), which are other **Item**s. These children will be other randomizers or questions (and potentially other item_types).

### Duplicated Text

Some **ItemText** is duplicated for more than one **Item** ( _tree_view_, _contact_, _to_, and _from_ **Item**s in the emib test can all have the exact same text). The ideal would be a reference table where **Item**s can be linked to other **Item**s to use their **ItemText**

## Upload Process

We currently seed two tests into the application by default using django migrations.

- The eMIB Sample test. This test is publicly available to all candidates so they can try out the interface before taking the test.
- The Pizza test. This test is filler content, and is used to develop on and have seeded test data representing a non-public test.

These two tests do not contain proctected data and can be used to demo a working sample and a working "real" test to anyone.

To upload a test outside of a migration, this is the current process (update as this becomes more efficient):
Write SQL inserts. An example of this can be found in the following directory: `S:\CMB-DGGM\ITSD-DSTI\CAT\eMIB A and B\Upload Scripts`

- Save the files as `.sql`
- Open Filezilla
- Select File -> Site Manage
- Add a new site
  - Select SFTP from the dropdown
  - Enter the ip address and credentials
  - Connect
- Upload files through filezilla
- Open SecureCRT
- Select Session manager (on the sidebar)
- Add a new session
  - Default protocol
  - Fill out the name/IP address, port, and username
  - Finish
- Connect to the new session and login
- In the shell, run the following: `psql -U cat_user -d cat_db -a -f <path to file>.sql`
- Finally call the test-meta-data and test-questions endpoints on localhost and the upload environment
  - Open a diff tool
  - paste the results of the api calls in the tool
  - regex replace all `"true_id": [0-9]*` with `"true_id": 000`
  - the results of the api call should be identical

## eMIB Overview

The eMIB test consists of the following parts.

| Section                | Types of content                                              | Stored                                      | Notes                                                   |
| ---------------------- | ------------------------------------------------------------- | ------------------------------------------- | ------------------------------------------------------- |
| Meta data              | Sets of flags and strings                                     | Content database                            | isPublic, testName, testId, defaultTime, etc            |
| Overview page          | Single markdown file                                          | Content database                            | The header is taken from the test name in the meta data |
| Instructions           | Strings, email components                                     | Frontend translations file                  | These are the same for each eMIB test                   |
| Background information | Sets of markdown files and org charts (tree views and images) | Content database (images still in frontend) | This is different for each test                         |
| Inbox emails           | Single markdown file                                          | Content database                            | The header is taken from the test name in the meta data |
| Address Book           | Strings                                                       | Content database                            | This is different for each test                         |

The names in the address book should match exactly with the names in the emails. If they don't the way to select recipients will break. Ideally these should also match the names in the org chart, but this will not break the application in any way, just confuse the candidate.

## Model definition

### **Item**

The **Item** is the core of the model design. Each **Item** is linked to an **ItemType** that helps the code understand how to handle it

| Column Name    | Description                                                                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _item_id_      | the PK of the **Item** table                                                                                                                                        |
| _order_        | used to organize siblings (**Item**s with the same _parent_id_ ) so they are always displayed in the correct order                                                  |
| _date_created_ | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_      | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_    | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |
| _item_type_id_ | RK to the **ItemType** table; helps the code understand how to handle the item                                                                                      |
| _parent_id_    | RK to another **Item** that is the parent of the current **Item**; for **Item**s of type test, this value is always null                                            |

### **ItemText**

Most **Item**s have an **ItemText**. This contains strings intended to be displayed to the user of the application or to test owners for reference purposes

| Column Name    | Description                                                                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _item_text_id_ | PK for the **ItemText** table                                                                                                                                       |
| _text_detail_  | contains strings intended to be visible to the user of the application or test owner                                                                                |
| _date_created_ | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_      | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_    | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |
| _item_id_      | RK to the **Item** table                                                                                                                                            |
| _language_id_  | RK to the **Language** table                                                                                                                                        |

### **ItemType**

Contains a _type_desc_ string that helps the code determine an **Item**'s purpose, what kind of children (other **Item**s) it should have, how the children should be displayed in relation to the parent, and what other tables will contain additional information for the given item

| Column Name    | Description                                                                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _item_type_id_ | PK for the **ItemType** table                                                                                                                                       |
| _type_desc_    | string used by the code to determine how to handle the **Item** and what relations it should have                                                                   |
| _date_created_ | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_      | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_    | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |

### **Language**

Contains ISO codes for each of the official **Languages**. Used to help determine which **ItemText** to display based on the current language setting

| Column Name    | Description                                                                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _language_id_  | PK for the **Language** table                                                                                                                                       |
| _ISO_CODE_1_   | 2 character code for language (i.e. "en", "fr")                                                                                                                     |
| _ISO_CODE_2_   | longer code for the language (i.e. "en-ca", "fr-ca")                                                                                                                |
| _date_created_ | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_      | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_    | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |

### **Question**

Contains additional information for an **Item** of **ItemType** "question" that is unique to this particular **ItemType**

| Column Name        | Description                                                                                                                                                         |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _question_id_      | PK of the **Question** table                                                                                                                                        |
| _date_created_     | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_          | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_        | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |
| _item_id_          | RK to the **Item** table                                                                                                                                            |
| _question_type_id_ | RK to the **QuestionType** table                                                                                                                                    |

### **QuestionType**

Similar to **ItemType**; contains a _question_type_desc_ string, which is used by the code to determine how to display the given question, what children it should have, etc.

| Column Name          | Description                                                                                                                                                         |
| -------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| _question_type_id_   | PK of the **QuestionType** table                                                                                                                                    |
| _question_type_desc_ | string used by the code to determine how to handle the question **Item** and what relations it should have with other **Item**s                                     |
| _date_created_       | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_            | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_          | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |

### **Test**

Contains additional information for an **Item** of **ItemType** "test" that is unique to this particular **ItemType**

This table's primary key is the _test_name_ string, an internally used value used to look up test meta data, overview, questions, etc. This prevents the need for consistant integer ids across environments. The **ItemText**s associated with the **Item** contain the publically visible test name.

| Column Name | Description                                                                                                                                                                    |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| _test_name_ | string PK to allow code to consistently access the same test on any enviornment; value is to be used internally only                                                           |
| _is_public_ | boolean; flag to desigante if the test can be viewed by anyone (including users who are not logged in) at any time or if it can only be assigned within an actual test session |
| _test_time_ | the time alloted to take the test; if null there is unlimited time                                                                                                             |
| _test_type_ | string used help the code determine how to process the test; this may one day be expanded to a TestType table                                                                  |
| _item_id_   | the **Item** accociated with this **Test**; the root item of the test                                                                                                          |

### TestAccessCode

Contains data created on TA checkin, allow candidates to check into an active session and start taking a test. Rows are deleted when a TA checks out of the session

| Column Name                        | Description                                                                                                               |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| id                                 |
| PK of the **TestAccessCode** table |
| test_access_code                   | The random hash generated by checkin; candidates can use this hash to check into a session and be assigned the given test |
| staffing_process_number            | the string linking the session to the staffing process                                                                    |
| test_session_language_id           | RK to the **Language** table; the language the test responses should be written in; used to help determine scorers        |
| ta_username_id                     | RK to **User** table; the user that opened the session                                                                    |
| test_id                            | RK to the **Test** table; the **Test** that is being assigned                                                             |

### AssignedTest

Contains all test assignment data in relation to the candidate. Also contains many of the values from the more temporary **TestAccessCode** table

| Column Name              | Description                                                                                                               |
| ------------------------ | ------------------------------------------------------------------------------------------------------------------------- |
| id                       | PK of the **AssignedTest** table                                                                                          |
| status                   | integer value refering to a map in the backend of the application; this includes active, submitted, quit, timed out, etc  |
| scheduled_date           | The date when the test was assigned to the candidate; unused currently                                                    |
| start_date               | The date when the user actually started the test                                                                          |
| modify_date              | The date that the user last modified something related to the test (saved a response,submitted, etc)                      |
| submit_date              | The submission, quit, or timeout date                                                                                     |
| test_id                  | RK to the **Test** table; the **Test** that is being assigned                                                             |
| username_id              | RK to **User** table; the candidate that i taking/took the test                                                           |
| ta_id                    | RK to **User** table; the user (TA) that opened the session                                                               |
| test_access_code         | The random hash generated by checkin; candidates can use this hash to check into a session and be assigned the given test |
| test_session_language_id | RK to the **Language** table; the language the test responses should be written in; used to help determine scorers        |

### AssignedQuestion

An linking table that connects **AssignedTest** to the **Question** table; this will become a lot more relevant when questions are randomly generated and not every candidate will see the same questions in the same order

| Column Name      | Description                          |
| ---------------- | ------------------------------------ |
| id               | PK of the **AssignedQuestion** Table |
| assigned_test_id | RK to the **AssignedTest** table     |
| question_id      | RK to the **Question** table         |

### CandidateResponse

Table linking responses to **AssignedQuestion** objects

| Column Name          | Description                                                                           |
| -------------------- | ------------------------------------------------------------------------------------- |
| id                   | PK of the **CandidateResponse** table                                                 |
| date_modified        | The date that the response was last modified                                          |
| response_type_id     | RK to the **ResponseType** table; used to determine how to store and display the data |
| assigned_question_id | RK to the **AssignedQuestion** Table; links the response back to the actual question  |

### ResponseType

Similar to **ItemType** and **QuestionType**; contains a _response_desc_ string, which is used by the code to determine how to display the given response, and which table will contain more data (**EmailResponse** or **TaskResponse** currently)

| Column Name    | Description                                                                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| id             | PK of the **ResponseType** table                                                                                                                                    |
| response_desc  | string used by the code to determine how to handle the **Response** and what tables will contain more data                                                          |
| _date_created_ | creation date of the row; mostly for documentation purposes                                                                                                         |
| _date_to_      | expiration date; rows that are past the current date should be ignored by the code; usually null                                                                    |
| _date_from_    | activation date; rows that are before the current date should be ignored by the code; usually the same as creation date, but can also be set to a time in the futue |

### EmailResponse

Table storing additional data for a _email_ type **CandidateResponse**

| Column Name          | Description                                                                                             |
| -------------------- | ------------------------------------------------------------------------------------------------------- |
| id                   | PK of the **EmailResponse** table                                                                       |
| to                   | a list of comma seperated ids, refering to _contact_ type **Item**s, can be parsed to recover the names |
| cc                   | a list of comma seperated ids, refering to _contact_ type **Item**s, can be parsed to recover the names |
| response             | the body of the email                                                                                   |
| reason               | stores the reason text where the candidate can justify their actions                                    |
| canidate_response_id | RK to the **CandidateResponse** table                                                                   |

### TaskResponse

Table storing additional data for a _task_ type **CandidateResponse**

| Column Name          | Description                                                          |
| -------------------- | -------------------------------------------------------------------- |
| id                   | PK of the **TaskResponse** table                                     |
| task                 | stores the body of the task                                          |
| reason               | stores the reason text where the candidate can justify their actions |
| canidate_response_id | RK to the **CandidateResponse** table                                |

### User

Contains data necessary for a **User**. This one is mostly self-explanatory

| Column Name         | Description                                                 |
| ------------------- | ----------------------------------------------------------- |
| id                  | PK of the **User** table                                    |
| password            | hash of password                                            |
| last_login          | last time they logged in                                    |
| is_superuser        | is a django admin?                                          |
| username            | username/email                                              |
| first_name          | user's real first name                                      |
| last_name           | user's real last name                                       |
| email               | user's email address                                        |
| is_staff            | is a django admin?                                          |
| is_active           | can be used to deactivate an account                        |
| date_joined         | when the account was created                                |
| birth_date          | user's birth date, only inclused the last digit of the year |
| pri_or_military_nbr | optional field to include PRI or military number            |

### User_Groups

Table connecting **User**s to **Auth_Group**s

| Column Name | Description                     |
| ----------- | ------------------------------- |
| id          | PK of the **User_Groups** table |
| user_id     | RK of the **User** table        |
| group_id    | RK of the **Auth_Group** table  |

### DataBaseCheckModel

Unsure what this table is actually used for

| Column Name | Description                  |
| ----------- | ---------------------------- |
| id          | PK of **DataBaseCheckModel** |
| name        | ???; a string of some kind   |
