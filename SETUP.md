# Setup Dev Environment

## Requirements to run the application

Ensure that you have docker and docker-compose installed on your machine, as well as the rest of the Python stack.

- GitBash
- Python 3.6.7
  - https://www.python.org/downloads/release/python-367/ - Windows x86-64 executable installer
  - “Install now”
  - Bypass character count limit option
  - You should be able to open a Python 3.6 Desktop App and run lines of Python
- PiP
  - Download get-pip.py
    - Go to https://bootstrap.pypa.io/
    - Right click on 'get-pip.py' -> 'Save link as...'
    - Select a download location
  - Navigate to the file in a terminal, run 'python get-pip.py'
  - Add pip's directory to the path system environment variable (usually "<Python Directory>\Scripts")
  - Pip is used to build the backend so that snyk can anaylze it locally
- Docker
- Docker Compose
- Katalon ( Initially from [Katalon Tutorial](https://gcdocs.gc.ca/psc-cfp/llisapi.dll?func=ll&objaction=overview&objid=8240554) )
  - [Create a Katalon account](https://www.katalon.com/create-account/)
  - Download Katalon from the [website](https://www.katalon.com/)
    - Place the zip file in the folder where you want to install it (C:/\_dev for example)
    - Unzip it
    - Run katalon.exe
  - Test recorder
    - Chrome add on
      - May not be allowed, based on permissions
      - Visit https://chrome.google.com/webstore/detail/katalon-recorder/ljdobmomdgdljniojadhoplhkpialdid to add it to Chrome
    - Internet Explorer Addon
      - Select the Internet Explorer icon in Katalon's web recorder
      - If the add on is not installed, you will be prompted to do so
      - This will require admin permission
    - Accessing Test Recorder
      - Start a new project or open an existing one
      - Click on the Globe button with the record circle
      - The test will not start recording till you hit the record button
    - Recording tests
      - Create a new Test Suite
      - Hit the record button
      - Begin recording. Note, EVERY mouse click will be recorded, even those unrelated to the browser
      - Hit Stop to save your test
    - Running a saved test
      - To run a saved test, or test suite, press play or play suite
    - Exporting test
      - Hit export
      - Select the scripting format for the export
        - Java
        - C#
        - Ruby
        - Python
        - XML
        - Katalon Studio file (.groovy).
  - Other references:
    - See [Version Control Test Scripting Guide (Halifax)](https://gcdocs.gc.ca/psc-cfp/llisapi.dll?func=ll&objaction=overview&objid=9742266) on how to initialize Katalon scripts in a new repo
  -

## Firefox extensions to install

- [Redux Devtools](https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools)
- [React Developer Tools](https://addons.mozilla.org/en-CA/firefox/addon/react-devtools/)
- [a11y-outline (accessibility)](https://addons.mozilla.org/en-CA/firefox/addon/a11y-outline/)
- [Katalon Recorder (automated functional testing)](https://addons.mozilla.org/en-US/firefox/addon/katalon-automation-record/)

## Requirements for development environment

We use VS Code, and specific linting setup when developing the application. This ensures consistency in our code style.

## VSCode Extensions to install

Press Ctrl+Shift+X for the hotkey to select the tab on the left-hand side of the IDE, then install the following VSCode extensions:

- Bracket Pair Colorizer 2, by CoenraadS
- CSS Peek, by Pranay Prakash
- Docker, by Microsoft
- Django, by Baptiste Darthenay
- ESLint, by Dirk Baeumer
- Git History Diff, by Hui Zhou
- Git Graph, by mhutchie
- Path Intellisense, by Christian Kohler
- Peacock, by John Papa
- PostgreSQL, by Chris Kolkman
- Prettier - Code formatter, by Esben Petersen
- Python, by Microsoft
- React Redux ES6 Snippets, by Timothy McLane
- SQL Server (mssql), by Microsoft

## Configuring VSCode Extensions

We use VS Code, and specific linting setup when developing the application. This ensures consistency in our code style.

Linting and autoformatting settings are present in the repository, but the .vscode/settings.json file for your local project still needs to be configured properly.

### SQL Server extension configuration

- Click on the "SQL Server" icon on the left-hand side of VSCode (looks like a cellphone)
- Click on "Add Connection" in the left-hand menu

Fill in the fields with the following information when prompted:

- `hostname`: localhost
- `Database name`: Press Enter to skip (this is important)
- `Authentication Type`: SQL Login
- `Username`: SA
- `Password`: someSecurePassword10!
- `Save Password`: Yes
- `Profile Name`: optional (name it whatever you want, or just press "Enter" to continue)

### Linting Configuration

If you have not done so already, open the project in VSCode by right-clicking on the folder in File Explorer and select "Open with Code".

- ESLint:

  - The file `.vscode/settings.json` should already exist & be in use as a workspace settings.json file. If not, feel free to port the eslint-specific settings to your own user settings.json file.
  - To ensure that ESLint works, open the Terminal window within VSCode by pressing `` Ctrl+Shift+` ``, then click on the "Output" tab and select "ESLint" from the dropdown menu. There should be no errors shown in the Output window if ESLint is working correctly when a .js, .jsx or .json file is open in another VSCode tab.
  - ESLint is now installed and configured!

    - If ESLint is not working, do the following steps:
      1. Close Docker: Right Click on Docker icon + Quit Docker
      2. Navigate in your Windows explorer and delete the folder called '_node_modules_' under '.\\\\project-thundercat\\\\frontend'
      3. Start Docker
      4. Open a powershell window and do a '_docker-compose up_'
      5. Wait a couple minutes to make sure that all packages have been downloaded
      6. Restart VS Code

  - Click [here](https://eslint.org/) for more details about **ESLint** or [here](https://github.com/airbnb/javascript/tree/master/react#basic-rules) to learn more about the Airbnb React style.

- Pylint:
  1.  Open a python file, hit save and there should be a popup saying that _Linter pylint is not installed_. Hit _Install_.
  2.  Now, you can edit the desired settings from _.pylintrc_ file (to disable messages, see line 54 of the config file)
  3.  Click [here](http://pylint-messages.wikidot.com/all-messages) for more details on most of the Pylint errors/warnings

## Local automated validation

Learn more about these tools in [REPORTING.md](./REPORTING.md)

### Snyk

[Snyk](https://snyk.io/) is a security scanning tool that helps ensure the security of open source packages.

- Run 'npm install -g snyk' in a terminal
- Run 'npm install -g snyk-to-html' in a terminal
  - This allows for clean output to html files when snyk is run locally
  - Locally detected vulnerabilities can be documented in the repo and version controlled
- See [snyk.md](docs/snyk.md) for an explanation of why snyk is installed locally rather than in the containers
- Add the npm directory containing snyk to the path system environment variable (usually "C:\Users\<name>\AppData\Roaming\npm")
  - Note: You may need to restart your computer for this change to take effect
- Create an account on snyk.io
- Run 'snyk auth' in a terminal
- Login when prompted (either in using the provided URL or in the opened browser)

### Accessibility Check Tool (pa11y)

Accessibility requirements cannot be ensured entirely through automated checks, but they do help ensure we are meeting basic standards. [Pa11y](https://github.com/pa11y/pa11y) is a tool that helps us do this.

- Run 'run 'npm install -g pa11y' in a terminal
- Run also 'npm install -g pa11y pa11y-reporter-html' in a terminal
  - This allows for clean output to html file when this tool is run locally
  - It detects errors, warnings and notices related to accessibility of the application
- Make sure that the npm directory containing pa11y is part of the path system environment variable (usually "C:\Users\<name>\AppData\Roaming\npm")
  - Should have already been added during Snyk configuration (see above)
- Click [here](https://github.com/pa11y/pa11y) to have more details about pa11y.

## Troubleshooting

If you are having any issues with getting the project to run, please refer to the [troubleshooting.md document](docs/troubleshooting.md) to help resolve your issues.
