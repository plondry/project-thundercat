import json
from channels.consumer import AsyncConsumer
from backend.custom_models.channels_room import ChannelsRoom
from backend.custom_models.channels_presence import ChannelsPresence
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.utils import get_user_info_from_token_string


class TestSessionConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("====================== CONNECTED ======================", event)
        room_test_access_code = self.scope["url_route"]["kwargs"]["test_access_code"]
        jwt_token = self.scope["url_route"]["kwargs"]["jwt_token"]
        # getting user info from provided jwt token
        user_info = get_user_info_from_token_string(jwt_token)
        # creating new group
        await self.channel_layer.group_add(room_test_access_code, self.channel_name)
        # initializing room_id
        room_id = None

        # validating test access code (making sure that the test access code exists)
        # the provided test access code does not match any of the existing ones in test_access_code and assigned_test tables
        if (
            TestAccessCode.objects.filter(
                test_access_code=room_test_access_code
            ).count()
            <= 0
            and AssignedTest.objects.filter(
                test_access_code=room_test_access_code
            ).count()
            <= 0
        ):
            # denying websocket connection (triggers a disconnect)
            await self.send({"type": "websocket.close"})

        # there is no room created
        if not ChannelsRoom.objects.filter(channel_name=room_test_access_code):
            # creating new room in channels_presence_room table
            ChannelsRoom.objects.create(channel_name=room_test_access_code)

        # getting room_id
        room_id = ChannelsRoom.objects.get(channel_name=room_test_access_code).id

        # checking if the current user is the TA
        # initializing is_ta to False
        is_ta = False
        # there is one or more assigned tests with the provided test access code, the right status and where the current username is the same as the ta_username
        # OR
        # there is an active test access code with the provided test access code and where the current username is the same as the ta_username_id
        if (
            AssignedTest.objects.filter(
                test_access_code=room_test_access_code,
                ta_id=user_info["username"],
                status__in=(
                    AssignedTestStatus.ASSIGNED,
                    AssignedTestStatus.READY,
                    AssignedTestStatus.LOCKED,
                    AssignedTestStatus.PAUSED,
                    AssignedTestStatus.ACTIVE,
                ),
            ).count()
            > 0
            or TestAccessCode.objects.filter(
                test_access_code=room_test_access_code,
                ta_username_id=user_info["username"],
            ).count()
            > 0
        ):
            # updating is_ta to True
            is_ta = True

        # initializing already connected variable
        user_already_connected_to_socket = False
        # check if the user is already connected to the socket and make sure that the user is not a TA
        if (
            not is_ta
            and ChannelsPresence.objects.filter(
                room_id=room_id, user_id=user_info["user_id"]
            ).count()
            > 0
        ):
            user_already_connected_to_socket = True
            # denying websocket connection (triggers a disconnect)
            await self.send({"type": "websocket.close"})

        # note that this condition will be true if the user is a TA
        if not user_already_connected_to_socket:
            # creating new presence in channels_presence_presence table
            ChannelsPresence.objects.create(
                room_id=room_id, user_id=user_info["user_id"]
            )
            # accepting websocket connection
            await self.send({"type": "websocket.accept"})

    async def websocket_receive(self, event):
        print("====================== RECEIVE ======================", event)
        action_text = event.get("text", None)
        if action_text is not None:
            loaded_dict_data = json.loads(action_text)
            action = loaded_dict_data["action"]
            parameters = loaded_dict_data["parameters"]
            test_access_code = loaded_dict_data["test_access_code"]
            pause_time = loaded_dict_data.get("pause_time", None)

            response = {
                "action": action,
                "parameters": parameters,
                "test_access_code": test_access_code,
            }
            if pause_time:
                response["pause_time"] = pause_time

            # broadcasts the message event to be sent
            await self.channel_layer.group_send(
                test_access_code, {"type": "send_action", "text": json.dumps(response)}
            )

    async def send_action(self, event):
        # sends the actual message
        await self.send({"type": "websocket.send", "text": event["text"]})

    async def websocket_disconnect(self, event):
        print("====================== DISCONNECTED ======================", event)
        # if not disconnected because of denied access
        if event["code"] != 1006:
            # getting needed parameters
            room_test_access_code = self.scope["url_route"]["kwargs"][
                "test_access_code"
            ]
            jwt_token = self.scope["url_route"]["kwargs"]["jwt_token"]
            # getting user info from provided jwt token
            user_info = get_user_info_from_token_string(jwt_token)

            # deleting presence of channels_presence_presence table
            # getting room ID
            room_id = ChannelsRoom.objects.get(channel_name=room_test_access_code).id
            # getting channel presences (in case there is a username connected more than once to the same websocket)
            channel_presences = ChannelsPresence.objects.filter(
                room_id=room_id, user_id=user_info["user_id"]
            )
            # deleting the last channel_presence (in order to keep the fist modify date one)
            channel_presences.delete()

            # if there is no presence remaining
            if ChannelsPresence.objects.filter(room_id=room_id).count() <= 0:
                # deleting room of channel_presence_room table
                ChannelsRoom.objects.get(channel_name=room_test_access_code).delete()
