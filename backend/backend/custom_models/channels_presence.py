from django.db import models
from user_management.user_management_models.user_models import User
from backend.custom_models.channels_room import ChannelsRoom


class ChannelsPresence(models.Model):
    room = models.ForeignKey(ChannelsRoom, on_delete=models.DO_NOTHING)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    modify_date = models.DateTimeField(auto_now=True)
