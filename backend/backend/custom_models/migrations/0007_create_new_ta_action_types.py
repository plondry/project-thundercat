from django.db import migrations


def create_new_ta_action_types(apps, _):
    # get models
    ta_action_types = apps.get_model("custom_models", "taactiontypes")
    # creating all needed TA Action Types
    action_type_1 = ta_action_types(action_type="RE_SYNC")
    action_type_1.save()
    action_type_2 = ta_action_types(action_type="UN_ASSIGN")
    action_type_2.save()


def rollback_new_ta_action_types(apps, schema_editor):
    # get models
    ta_action_types = apps.get_model("custom_models", "taactiontypes")
    # get db alias
    db_alias = schema_editor.connection.alias
    # delete the types
    ta_action_types.objects.using(db_alias).filter(action_type="RE_SYNC").delete()
    ta_action_types.objects.using(db_alias).filter(action_type="UN_ASSIGN").delete()


class Migration(migrations.Migration):
    dependencies = [("custom_models", "0006_assignedtest_test_order_number")]
    operations = [
        migrations.RunPython(create_new_ta_action_types, rollback_new_ta_action_types)
    ]
