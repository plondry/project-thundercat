from django.db import models
from backend.custom_models.ta_actions import TaActions

# Pause/Unpause Test Actions Model (data for all Pause/Unpause Actions)


class LockTestActions(models.Model):
    lock_start_date = models.DateTimeField(blank=False, null=False)
    lock_end_date = models.DateTimeField(blank=True, null=True)
    ta_action = models.ForeignKey(
        TaActions, to_field="id", on_delete=models.DO_NOTHING, null=False, blank=False
    )
