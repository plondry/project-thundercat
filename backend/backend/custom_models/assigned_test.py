from django.db import models
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection
from user_management.user_management_models.user_models import User
from backend.custom_models.language import Language


class AssignedTest(models.Model):
    username = models.ForeignKey(User, to_field="username", on_delete=models.DO_NOTHING)
    status = models.IntegerField(blank=True, null=False)
    previous_status = models.IntegerField(blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    modify_date = models.DateTimeField(auto_now=True)
    submit_date = models.DateTimeField(blank=True, null=True)
    test_access_code = models.CharField(max_length=10, null=True, blank=True)
    test_order_number = models.CharField(max_length=12, blank=False, null=True)
    total_score = models.IntegerField(blank=False, null=True)
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)
    test_session_language = models.ForeignKey(
        Language,
        to_field="language_id",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
    ta = models.ForeignKey(
        User,
        to_field="username",
        related_name="ta_username",
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
    )
    test_section = models.ForeignKey(
        TestSection, on_delete=models.DO_NOTHING, null=True, blank=True
    )
