from django.db import models
from backend.custom_models.assigned_test_section import AssignedTestSection
from django.utils import timezone


class AssignedTestSectionAccessTimes(models.Model):
    assigned_test_section = models.ForeignKey(
        AssignedTestSection, on_delete=models.DO_NOTHING)
    time_type = models.IntegerField()
    time = models.DateTimeField(default=timezone.now, blank=True)

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0}".format(
            self.assigned_test_section.test_section.en_title)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Assigned Test Section Access Times"
