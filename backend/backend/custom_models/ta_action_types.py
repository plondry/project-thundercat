from django.db import models

# TA Action Types Model (mapping action types data)


class TaActionTypes(models.Model):
    action_type = models.CharField(primary_key=True, max_length=15)
