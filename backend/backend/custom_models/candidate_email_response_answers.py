from django.db import models
from backend.custom_models.candidate_answers import CandidateAnswers
from cms.cms_models.address_book_contact import AddressBookContact

MAX_LENGTH = 25


class CandidateEmailResponseAnswers(models.Model):
    candidate_answers = models.ForeignKey(
        CandidateAnswers, on_delete=models.DO_NOTHING)
    answer_id = models.IntegerField()
    email_type = models.CharField(max_length=MAX_LENGTH)
    email_body = models.TextField(null=True)
    email_to = models.ManyToManyField(
        AddressBookContact, related_name='%(class)s_email_to')
    email_cc = models.ManyToManyField(
        AddressBookContact, related_name='%(class)s_email_cc')
    reasons_for_action = models.TextField(null=True)

    class Meta:
        # setting the model name in Django Admin Console
        unique_together = (('answer_id', 'candidate_answers'),)
