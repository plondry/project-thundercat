from django.db import models


class ChannelsRoom(models.Model):
    channel_name = models.CharField(max_length=10, null=False, blank=False)
