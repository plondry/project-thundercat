from django.db import models
from backend.custom_models.assigned_test import AssignedTest
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.test_section_component import TestSectionComponent


class CandidateAnswers(models.Model):
    assigned_test = models.ForeignKey(AssignedTest, on_delete=models.DO_NOTHING)
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING)
    test_section_component = models.ForeignKey(
        TestSectionComponent, on_delete=models.DO_NOTHING
    )
    time_spent = models.IntegerField(default=0)
