from django.db import models
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.competency_type import CompetencyType
from django.utils import timezone


class AssignedTestAnswerScore(models.Model):
    scorer_assigned_test = models.ForeignKey(
        TestScorerAssignment, on_delete=models.DO_NOTHING
    )
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING)
    competency = models.ForeignKey(
        CompetencyType, null=True, on_delete=models.DO_NOTHING
    )
    rationale = models.TextField()
    score = models.FloatField()

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Assigned Test Answer Score"
