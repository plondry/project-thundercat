from django.db import models
from cms.cms_models.test_section import TestSection
from backend.custom_models.assigned_test import AssignedTest


class AssignedTestSection(models.Model):
    test_section = models.ForeignKey(TestSection, on_delete=models.DO_NOTHING)
    assigned_test = models.ForeignKey(AssignedTest, on_delete=models.DO_NOTHING)
    test_section_time = models.IntegerField(blank=True, null=True)
    score = models.IntegerField(blank=False, null=True)

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0} for {1}".format(self.test_section.en_title, self.test_section_time)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Assigned Test Section"
