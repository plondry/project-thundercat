from django.db import models
from backend.custom_models.candidate_answers import CandidateAnswers
from cms.cms_models.answer import Answer


class CandidateMultipleChoiceAnswers(models.Model):
    answer = models.ForeignKey(Answer, on_delete=models.DO_NOTHING, null=True)
    candidate_answers = models.ForeignKey(CandidateAnswers, on_delete=models.DO_NOTHING)
