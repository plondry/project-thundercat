from .language import Language
from .test_access_code_model import TestAccessCode
from .notepad import Notepad
from .test_scorer_assignment import TestScorerAssignment
from .candidate_answers import CandidateAnswers
from .candidate_multiple_choice_answers import CandidateMultipleChoiceAnswers
from .assigned_test import AssignedTest
from .assigned_test_section import AssignedTestSection
from .assigned_test_section_access_times import AssignedTestSectionAccessTimes
from .ta_action_types import TaActionTypes
from .ta_actions import TaActions
from .pause_test_actions import PauseTestActions
from .lock_test_actions import LockTestActions
from .candidate_email_response_answers import CandidateEmailResponseAnswers
from .candidate_task_response_answers import CandidateTaskResponseAnswers
from .channels_room import ChannelsRoom
from .channels_presence import ChannelsPresence
from .assigned_test_answer_score import AssignedTestAnswerScore

# These imports help to auto discover the models
