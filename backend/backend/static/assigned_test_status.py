# This file is a collection of all possilble candidate testing process test statuses.


class AssignedTestStatus:
    # candidate testing process (from 11 to 30)
    # if you update these statuses, don't forget to also update ".../components/ta/Constants.jsx" file (frontend)
    ASSIGNED = 11  # "Assigned to a Candidate"
    READY = 12  # "Approved from Responsible TA"
    ACTIVE = 13  # "Active"
    LOCKED = 14  # "Test Locked"
    PAUSED = 15  # "Test Paused"
    NEVER_STARTED = 16  # "Never Started"
    INACTIVITY = 17  # "Expired by Inactivity"
    TIMED_OUT = 18  # "User timed out, still counts as submitted"
    QUIT = 19  # "User Quit"
    SUBMITTED = 20  # "Submitted"
    UNASSIGNED = (
        21
    )  # "un-assigned by TA (user never got access to the test, meaning that he was never able to see the content)"

    # scoring process (from 31 to 39)
    # TODO: add needed statuses (start at 31)

