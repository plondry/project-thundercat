from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined
from backend.custom_models.candidate_answers import CandidateAnswers


class Action:
    UPDATE = "UPDATE"


def update_time_spent(request, needed_parameters, action):
    parameters = []
    # handling missing parameters
    for parameter in needed_parameters:
        parameter_name = "{}".format(parameter)
        parameter = request.query_params.get(parameter_name, None)
        if is_undefined(parameter):
            return Response(
                {"error": "no '{}' parameter".format(parameter_name)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        parameters.append(parameter)

    try:
        # getting candidate answer object
        candidate_answer = CandidateAnswers.objects.get(
            assigned_test_id=parameters[0],
            question_id=parameters[1],
            test_section_component_id=parameters[2],
        )

        # update time spent
        if action == Action.UPDATE:
            # getting current time spent value
            current_time_spent = candidate_answer.time_spent
            # adding current time spent to provided time
            candidate_answer.time_spent = int(parameters[3]) + current_time_spent
            # saving result
            candidate_answer.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(
                {"error": "{} action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except CandidateAnswers.DoesNotExist:
        return Response(
            {"error": "the specified candidate answer does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except ValueError:
        return Response(
            {"error": "the specified parameters are invalid"},
            status=status.HTTP_400_BAD_REQUEST,
        )
