from rest_framework import status
from rest_framework.response import Response
from backend.custom_models.notepad import Notepad
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.custom_models.assigned_test_answer_score import AssignedTestAnswerScore
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.competency_type import CompetencyType
from backend.serializers.assigned_test_answer_score_serializer import (
    AssignedTestAnswerScoreSerializer,
)


def save_score(parameters):
    test = TestScorerAssignment.objects.get(id=parameters["scorer_test"])
    question = NewQuestion.objects.get(id=parameters["question"])
    competency = CompetencyType.objects.get(id=parameters["competency"])

    score = AssignedTestAnswerScore.objects.filter(
        question=question, scorer_assigned_test=test, competency=competency
    ).first()
    if not score:
        score = AssignedTestAnswerScore(
            question=question, scorer_assigned_test=test, competency=competency
        )

    score.rationale = parameters["rationale"]
    score.score = parameters["score"]

    score.save()

    return Response(status=status.HTTP_200_OK)


def get_scores(parameters):
    test = TestScorerAssignment.objects.get(id=parameters["scorer_test"])

    scores = AssignedTestAnswerScore.objects.filter(scorer_assigned_test=test)

    return Response(
        AssignedTestAnswerScoreSerializer(scores, many=True).data,
        status=status.HTTP_200_OK,
    )
