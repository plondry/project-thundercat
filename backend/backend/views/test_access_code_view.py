from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.test_access_code import (
    populate_test_access_code_table,
    delete_test_access_code_row,
    get_active_test_access_codes,
)


class GetNewTestAccessCode(APIView):
    def get(self, request):
        return populate_test_access_code_table(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class DeleteTestAccessCode(APIView):
    def post(self, request):
        return delete_test_access_code_row(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetActiveTestAccessCodes(APIView):
    def get(self, request):
        return get_active_test_access_codes(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
