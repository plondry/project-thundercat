import random
import string
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined, get_user_info_from_jwt_token
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.language import Language
from backend.serializers.test_access_code_serializer import TestAccessCodeSerializer
from user_management.user_management_models.user_models import User
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions


# get a new randomly generated room number
def generate_random_test_access_code():
    allowed_chars = string.ascii_uppercase + string.digits
    random_test_access_code = "".join(random.choice(allowed_chars) for i in range(10))
    return random_test_access_code


def access_code_already_exists(random_code):
    return TestAccessCode.objects.filter(test_access_code=random_code).count() > 0


# populate the room number table with the needed parameters, which contains the generated room number
def populate_test_access_code_table(request):
    user_info = get_user_info_from_jwt_token(request)
    # making sure that we have the needed parameters
    test_order_number = request.query_params.get("test_order_number", None)
    test_session_language = request.query_params.get("test_session_language", None)
    test_id = request.query_params.get("test_id", None)
    if is_undefined(test_order_number):
        return Response(
            {"error": "no 'test_order_number' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_session_language):
        return Response(
            {"error": "no 'test_session_language' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_id):
        return Response(
            {"error": "no 'test_id' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    # call the function that generates the random room number
    random_test_access_code = generate_random_test_access_code()
    while access_code_already_exists(random_test_access_code):
        random_test_access_code = generate_random_test_access_code()
    # saving the response with the needed parameters
    response = TestAccessCode.objects.create(
        test_access_code=random_test_access_code,
        ta_username=User.objects.get(username=user_info["username"]),
        test_order_number=test_order_number,
        test_session_language=Language.objects.get(language_id=test_session_language),
        test=TestDefinition.objects.get(id=test_id),
    )
    response.save()
    # return test access code
    return Response(random_test_access_code)


def delete_test_access_code_row(request):
    user_info = get_user_info_from_jwt_token(request)
    # making sure that we have the needed parameters
    test_access_code = request.query_params.get("test_access_code", None)
    if is_undefined(test_access_code):
        return Response(
            {"error": "no 'test_access_code' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    # get the room number id
    try:
        test_access_code_id = TestAccessCode.objects.get(
            test_access_code=test_access_code
        ).id
    except TestAccessCode.DoesNotExist:
        return Response(
            {"error": "no room number found"}, status=status.HTTP_400_BAD_REQUEST
        )

    # delete the whole row based on parameters above
    TestAccessCode.objects.get(
        id=test_access_code_id,
        test_access_code=test_access_code,
        ta_username_id=user_info["username"],
    ).delete()
    return Response(status=status.HTTP_200_OK)


def get_active_test_access_codes(request):
    user_info = get_user_info_from_jwt_token(request)
    try:
        active_test_access_codes = TestAccessCode.objects.filter(
            ta_username_id=user_info["username"]
        )
        serializer = TestAccessCodeSerializer(active_test_access_codes, many=True)
        return Response(serializer.data)
    # should not happen
    except TestPermissions.DoesNotExist:
        return Response(
            {
                "error": "there are no matching results between test permissions and test access codes tables"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
