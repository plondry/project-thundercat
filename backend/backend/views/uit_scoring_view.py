from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.views.uit_score_test import uit_scoring, Action
from cms.views.utils import get_needed_parameters

# api/uit-test-section-scoring


class UitScoreTestSection(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_id", "test_section_id"]
        success, dictionary = get_needed_parameters(needed_parameters, request)
        if success:
            return uit_scoring(dictionary, Action.TEST_SECTION)
        else:
            return Response(dictionary, status=status.HTTP_400_BAD_REQUEST)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UitScoreTest(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_id"]
        success, dictionary = get_needed_parameters(needed_parameters, request)
        if success:
            return uit_scoring(dictionary, Action.TEST)
        else:
            return Response(dictionary, status=status.HTTP_400_BAD_REQUEST)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
