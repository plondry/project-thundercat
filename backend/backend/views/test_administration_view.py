from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.get_ta_assigned_candidates import get_ta_assigned_candidates
from backend.views.test_administrator_actions import (
    test_administrator_actions,
    TaActionsConstants,
    lock_unlock_all_candidates_test,
)


class GetTestAdministratorAssignedCandidates(APIView):
    def get(self, request):
        return get_ta_assigned_candidates(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UpdateTestTime(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_section_id", "test_time"]
        optional_parameters = []
        return test_administrator_actions(
            request,
            needed_parameters,
            optional_parameters,
            TaActionsConstants.UPDATE_TIME,
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class ApproveCandidate(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = []
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.APPROVE
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class LockCandidateTest(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = ["test_section_id"]
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.LOCK
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UnlockCandidateTest(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = ["test_section_id"]
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.UNLOCK
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class LockAllCandidatesTest(APIView):
    def post(self, request):
        return lock_unlock_all_candidates_test(request, TaActionsConstants.LOCK_ALL)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UnlockAllCandidatesTest(APIView):
    def post(self, request):
        return lock_unlock_all_candidates_test(request, TaActionsConstants.UNLOCK_ALL)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class PauseCandidateTest(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id", "pause_test_time"]
        optional_parameters = ["test_section_id"]
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.PAUSE
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UnpauseCandidateTest(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = ["test_section_id"]
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.UNPAUSE
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class ReSyncCandidate(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id", "test_access_code"]
        optional_parameters = []
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.RESYNC
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UnAssignCandidate(APIView):
    def post(self, request):
        needed_parameters = ["username_id", "test_id"]
        optional_parameters = []
        return test_administrator_actions(
            request, needed_parameters, optional_parameters, TaActionsConstants.UNASSIGN
        )

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
