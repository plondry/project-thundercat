from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.retrieve_reports_data import (
    retrieve_ta_assigned_test_order_numbers,
    retrieve_all_existing_test_order_numbers,
    retrieve_tests_based_on_test_order_number,
    retrieve_results_report_data,
    retrieve_financial_report_data
)


class GetTaAssignedTestOrderNumbers(APIView):
    def get(self, request):
        return retrieve_ta_assigned_test_order_numbers(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetAllExistingTestOrderNumbers(APIView):
    def get(self, request):
        return retrieve_all_existing_test_order_numbers(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetTestsBasedOnTestOrderNumber(APIView):
    def get(self, request):
        return retrieve_tests_based_on_test_order_number(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetResultsReportData(APIView):
    def get(self, request):
        return retrieve_results_report_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]

class GetFinancialReportData(APIView):
    def get(self, request):
        return retrieve_financial_report_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
