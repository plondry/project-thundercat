from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.retrieve_notepad_view import save_notepad, get_notepad


class SaveNotepad(APIView):
    def post(self, request):
        return save_notepad(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetNotepad(APIView):
    def get(self, request):
        return get_notepad(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
