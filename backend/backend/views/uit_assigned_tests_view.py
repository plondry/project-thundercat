from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.uit_test_actions import test_actions, Action


class UitSeenQuestion(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_id", "question_id"]
        return test_actions(request, needed_parameters, Action.SEEN_QUESTION)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class UitSaveAnswer(APIView):
    def post(self, request):
        needed_parameters = ["assigned_test_id", "question_id", "answer_id"]
        return test_actions(request, needed_parameters, Action.SAVE_ANSWERS)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


class GetTestAnswers(APIView):
    def get(self, request):
        needed_parameters = ["assigned_test_id", "test_section_component_id"]
        return test_actions(request, needed_parameters, Action.GET_ANSWERS)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
