from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.time_spent import update_time_spent, Action


class UpdateTimeSpent(APIView):
    def post(self, request):
        needed_parameters = [
            "assigned_test_id",
            "question_id",
            "test_section_component_id",
            "time",
        ]
        return update_time_spent(request, needed_parameters, Action.UPDATE)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
