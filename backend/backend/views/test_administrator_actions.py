from django.utils import timezone
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.pause_test_actions import PauseTestActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.custom_models.channels_room import ChannelsRoom
from backend.custom_models.channels_presence import ChannelsPresence
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.utils import is_undefined, get_user_info_from_jwt_token
from user_management.user_management_models.user_models import User
from cms.cms_models.test_definition import TestDefinition

# TA Actions Definition
class TaActionsConstants:
    UPDATE_TIME = "UPDATE_TIME"
    APPROVE = "APPROVE"
    LOCK = "LOCK"
    LOCK_ALL = "LOCK_ALL"
    UNLOCK = "UNLOCK"
    UNLOCK_ALL = "UNLOCK_ALL"
    PAUSE = "PAUSE"
    UNPAUSE = "UNPAUSE"
    RESYNC = "RE_SYNC"
    UNASSIGN = "UN_ASSIGN"
    REPORT = "REPORT"


# Assigned Test Statuses Definition based on Selected Action (statuses to include in assigned test filter)
class AssignedTestStatuses:
    UPDATE_TIME = [AssignedTestStatus.ASSIGNED]
    APPROVE = [AssignedTestStatus.ASSIGNED]
    RE_SYNC = [
        AssignedTestStatus.ASSIGNED,
        AssignedTestStatus.READY,
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.LOCKED,
        AssignedTestStatus.PAUSED,
    ]
    LOCK = [AssignedTestStatus.READY, AssignedTestStatus.ACTIVE]
    LOCK_ALL = [
        AssignedTestStatus.READY,
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.PAUSED,
    ]
    UNLOCK = [AssignedTestStatus.LOCKED]
    UNLOCK_ALL = [AssignedTestStatus.LOCKED]
    PAUSE = [AssignedTestStatus.READY, AssignedTestStatus.ACTIVE]
    UNPAUSE = [AssignedTestStatus.PAUSED]
    UN_ASSIGN = [AssignedTestStatus.ASSIGNED]
    REPORT = [
        AssignedTestStatus.ASSIGNED,
        AssignedTestStatus.READY,
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.LOCKED,
        AssignedTestStatus.PAUSED,
    ]


# generic function for test administrator actions (update test time, approve candidate, lock candidate and report candidate)
# needed_parameters must be in order in your request
def test_administrator_actions(request, needed_parameters, optional_parameters, action):
    parameters = {}
    # handling missing parameters
    for parameter in needed_parameters:
        parameter_name = "{}".format(parameter)
        parameter = request.query_params.get(parameter_name, None)
        if is_undefined(parameter):
            return Response(
                {"error": "no '{}' parameter".format(parameter_name)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        parameters[parameter_name] = parameter

    for parameter in optional_parameters:
        parameter_name = "{}".format(parameter)
        parameter = request.query_params.get(parameter_name, None)
        parameters[parameter_name] = parameter

    try:
        if action is not TaActionsConstants.UPDATE_TIME:
            user_info = get_user_info_from_jwt_token(request)
            # getting current assigned_test
            assigned_test = AssignedTest.objects.get(
                username_id=User.objects.get(username=parameters["username_id"]),
                test_id=TestDefinition.objects.get(id=parameters["test_id"]),
                ta_id=user_info["username"],
                status__in=(getattr(AssignedTestStatuses, action)),
            )
        # updating test status depending on the provided action
        # update time action
        if action == TaActionsConstants.UPDATE_TIME:
            assigned_test_section = AssignedTestSection.objects.get(
                id=parameters["assigned_test_section_id"]
            )
            assigned_test_section.test_section_time = parameters["test_time"]
            assigned_test_section.save()

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UPDATE_TIME,
                assigned_test_id=assigned_test_section.assigned_test_id,
            )
            ta_action.save()
        # approve action
        elif action == TaActionsConstants.APPROVE:
            assigned_test.status = AssignedTestStatus.READY
            assigned_test.save()

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.APPROVE,
                assigned_test_id=assigned_test.id,
            )
            ta_action.save()
        # lock action
        elif action == TaActionsConstants.LOCK:
            # if test status is READY
            if assigned_test.status == AssignedTestStatus.READY:
                # update previous_status
                assigned_test.previous_status = AssignedTestStatus.READY
            assigned_test.status = AssignedTestStatus.LOCKED
            assigned_test.save()

            # creating new entry in ta actions table
            test_section_id = None
            if parameters["test_section_id"] != "null":
                test_section_id = parameters["test_section_id"]
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.LOCK,
                assigned_test_id=assigned_test.id,
                test_section_id=test_section_id,
            )
            ta_action.save()

            # creating new entry in lock test actions table
            lock_test_action = LockTestActions.objects.create(
                lock_start_date=timezone.now(), ta_action_id=ta_action.id
            )
            lock_test_action.save()
        # unlock action
        elif action == TaActionsConstants.UNLOCK:
            # if previous_status is READY
            if assigned_test.previous_status == AssignedTestStatus.READY:
                # update status and previous_status
                assigned_test.status = AssignedTestStatus.READY
                assigned_test.previous_status = None
                assigned_test.save()

            # no READY previous_status
            else:
                assigned_test.status = AssignedTestStatus.ACTIVE
                assigned_test.save()

            # creating new entry in ta actions table
            test_section_id = None
            if parameters["test_section_id"] != "null":
                test_section_id = parameters["test_section_id"]
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UNLOCK,
                assigned_test_id=assigned_test.id,
                test_section_id=test_section_id,
            )
            ta_action.save()

            # update lock_end_date field in lock test actions table
            last_lock_ta_action = TaActions.objects.filter(
                action_type_id=TaActionsConstants.LOCK,
                assigned_test_id=assigned_test.id,
            ).last()
            lock_test_action = LockTestActions.objects.get(
                ta_action_id=last_lock_ta_action.id
            )
            lock_test_action.lock_end_date = timezone.now()
            lock_test_action.save()
        # pause action
        elif action == TaActionsConstants.PAUSE:
            # if test status is READY
            if assigned_test.status == AssignedTestStatus.READY:
                # update previous_status
                assigned_test.previous_status = AssignedTestStatus.READY
            assigned_test.status = AssignedTestStatus.PAUSED
            assigned_test.save()
            # creating new entry in ta actions table
            test_section_id = None
            if parameters["test_section_id"] != "null":
                test_section_id = parameters["test_section_id"]
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.PAUSE,
                assigned_test_id=assigned_test.id,
                test_section_id=test_section_id,
            )
            ta_action.save()

            # creating new entry in pause test actions table
            pause_test_action = PauseTestActions.objects.create(
                pause_start_date=timezone.now(),
                pause_test_time=parameters["pause_test_time"],
                ta_action_id=ta_action.id,
            )
            pause_test_action.save()
        # unpause action
        elif action == TaActionsConstants.UNPAUSE:
            # if previous_status is READY
            if assigned_test.previous_status == AssignedTestStatus.READY:
                # update status and previous_status
                assigned_test.status = AssignedTestStatus.READY
                assigned_test.previous_status = None
                assigned_test.save()
            # no READY previous_status
            else:
                assigned_test.status = AssignedTestStatus.ACTIVE
                assigned_test.save()

            # creating new entry in ta actions table
            test_section_id = None
            if parameters["test_section_id"] != "null":
                test_section_id = parameters["test_section_id"]
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UNPAUSE,
                assigned_test_id=assigned_test.id,
                test_section_id=test_section_id,
            )
            ta_action.save()

            # update pause_end_date field in pause test actions table
            last_pause_ta_action = TaActions.objects.filter(
                action_type_id=TaActionsConstants.PAUSE,
                assigned_test_id=assigned_test.id,
            ).last()
            pause_test_action = PauseTestActions.objects.get(
                ta_action_id=last_pause_ta_action.id
            )
            pause_test_action.pause_end_date = timezone.now()
            pause_test_action.save()
        # re-sync action
        elif action == TaActionsConstants.RESYNC:
            # get channels room ID
            channels_room_id = ChannelsRoom.objects.get(
                channel_name=parameters["test_access_code"]
            ).id
            try:
                # get channels presence element
                channels_presence = ChannelsPresence.objects.get(
                    room_id=channels_room_id,
                    user_id=User.objects.get(username=parameters["username_id"]).id,
                )
                # delete channels presence
                channels_presence.delete()
            except:
                return Response(
                    {
                        "status": status.HTTP_200_OK,
                        "info": "channelspresence table does not contain any data related to room_id:'{}'".format(
                            channels_room_id
                        ),
                    }
                )

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.RESYNC,
                assigned_test_id=assigned_test.id,
            )
            ta_action.save()
        # re-sync action
        elif action == TaActionsConstants.UNASSIGN:
            assigned_test.status = AssignedTestStatus.UNASSIGNED
            assigned_test.save()

            # creating new entry in ta actions table
            ta_action = TaActions.objects.create(
                action_type_id=TaActionsConstants.UNASSIGN,
                assigned_test_id=assigned_test.id,
            )
            ta_action.save()

        # elif action == TaActionsConstants.REPORT:
        # no action found
        else:
            return Response(
                {"error": "'{}' action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if action is not TaActionsConstants.UPDATE_TIME:
            return Response(
                {
                    "status": status.HTTP_200_OK,
                    "test_access_code": assigned_test.test_access_code,
                }
            )
        else:
            return Response(status.HTTP_200_OK)

    except User.DoesNotExist:
        return Response(
            {
                "error": "the specified user (candidate and/or test administrator) does not exist"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "no assigned test found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TypeError:
        return Response(
            {"error": "no assigned test found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AttributeError:
        return Response(
            {"error": "'{}' action does not exist".format(action)},
            status=status.HTTP_400_BAD_REQUEST,
        )


# locking/unlocking all candidates test (only if test status is ready, active or paused, not assigned)
def lock_unlock_all_candidates_test(request, action):
    user_info = get_user_info_from_jwt_token(request)
    try:
        # get all active assigned candidates (assigned, ready, active, locked or paused)
        active_candidates = AssignedTest.objects.filter(
            ta_id=user_info["username"],
            status__in=(getattr(AssignedTestStatuses, action)),
        )

        # initializing candidates_to_update array
        candidates_to_update = []

        # for each candidate
        for candidate in active_candidates:
            # get assigned test
            assigned_test = AssignedTest.objects.get(
                username_id=User.objects.get(username=candidate.username),
                test_id=TestDefinition.objects.get(id=candidate.test.id),
                ta_id=user_info["username"],
                status__in=(getattr(AssignedTestStatuses, action)),
            )

            # save assigned_test to candidates_to_update array
            candidates_to_update.append(assigned_test)

        # for each assigned test in candidates_to_update array
        for assigned_test in candidates_to_update:
            # LOCK_ALL action
            if action == TaActionsConstants.LOCK_ALL:
                # if test status is READY
                if assigned_test.status == AssignedTestStatus.READY:
                    # update previous_status
                    assigned_test.previous_status = AssignedTestStatus.READY
                # updating test status to LOCKED
                assigned_test.status = AssignedTestStatus.LOCKED

                # creating new entry in ta actions table
                # test section id is defined
                if assigned_test.test_section_id != "null":
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.LOCK,
                        assigned_test_id=assigned_test.id,
                        test_section_id=assigned_test.test_section_id,
                    )
                # test section id is not defined, meaning that the test has been locked/paused before the candidate even started the test
                else:
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.LOCK,
                        assigned_test_id=assigned_test.id,
                    )
                ta_action.save()

                # creating new entry in lock test actions table
                lock_test_action = LockTestActions.objects.create(
                    lock_start_date=timezone.now(), ta_action_id=ta_action.id
                )
                lock_test_action.save()
            # UNLOCK_ALL action
            elif action == TaActionsConstants.UNLOCK_ALL:
                # previous_status is READY
                if assigned_test.previous_status == AssignedTestStatus.READY:
                    # update status and previous_status
                    assigned_test.status = AssignedTestStatus.READY
                    assigned_test.previous_status = None
                # no READY previous status
                else:
                    # update status
                    assigned_test.status = AssignedTestStatus.ACTIVE

                # creating new entry in ta actions table
                # test section id is defined
                if assigned_test.test_section_id != "null":
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.UNLOCK,
                        assigned_test_id=assigned_test.id,
                        test_section_id=assigned_test.test_section_id,
                    )
                # test section id is not defined, meaning that the test has been locked/paused before the candidate even started the test
                else:
                    ta_action = TaActions.objects.create(
                        action_type_id=TaActionsConstants.UNLOCK,
                        assigned_test_id=assigned_test.id,
                    )
                ta_action.save()

                # update lock_end_date field in lock test actions table
                last_lock_ta_action = TaActions.objects.filter(
                    action_type_id=TaActionsConstants.LOCK,
                    assigned_test_id=assigned_test.id,
                ).last()
                lock_test_action = LockTestActions.objects.get(
                    ta_action_id=last_lock_ta_action.id
                )
                lock_test_action.lock_end_date = timezone.now()
                lock_test_action.save()
            # no action found
            else:
                return Response(
                    {"error": "'{}' action does not exist".format(action)},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            # saving new assigned_test
            assigned_test.save()
        serializer = AssignedTestsForLockUnlockAll(candidates_to_update, many=True)
        return Response({"status": status.HTTP_200_OK, "candidates": serializer.data})

    except User.DoesNotExist:
        return Response(
            {"error": "the specified test administrator does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTest.DoesNotExist:
        return Response(
            {"error": "no assigned test found based on provided parameters"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except TypeError:
        return Response(
            {
                "error": "no assigned test found based on provided parameters",
                "candidate": "{}".format(candidate.username),
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AttributeError:
        return Response(
            {"error": "'{}' action does not exist".format(action)},
            status=status.HTTP_400_BAD_REQUEST,
        )


class AssignedTestsForLockUnlockAll(serializers.ModelSerializer):
    class Meta:
        model = AssignedTest
        fields = "__all__"
