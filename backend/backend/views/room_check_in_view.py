from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework import status
from backend.views.room_check_in import check_into_room
from backend.views.utils import is_undefined, get_user_info_from_jwt_token


class RoomCheckIn(APIView):
    def post(self, request):
        user_info = get_user_info_from_jwt_token(request)
        test_access_code = request.query_params.get("test_access_code", None)
        if is_undefined(test_access_code) or test_access_code == "":
            return Response(
                {"error": "The test access code was not defined."},
                status=status.HTTP_400_BAD_REQUEST,
            )
        return check_into_room(test_access_code, user_info["username"])

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
