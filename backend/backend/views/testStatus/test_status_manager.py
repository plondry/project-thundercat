from math import ceil
from datetime import datetime, timedelta
from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.views.retrieve_notepad_view import delete_notepad
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.pause_test_actions import PauseTestActions
from backend.views.test_administrator_actions import TaActionsConstants
from backend.views.utils import is_undefined, get_user_info_from_jwt_token


def update_test_status(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)
    test.status = new_status
    return save_and_return(test)


def activate_test(test_id, start_date, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if test.status != AssignedTestStatus.ACTIVE:
        # /1000 because js timestamps are in milliseconds and python in seconds
        test.start_date = datetime.fromtimestamp(ceil(float(start_date) / 1000))

    test.status = new_status

    return save_and_return(test)


# create a new instance of assigned_test_status for this test, if it was submitted or timedout
def prepare_test_for_scoring(test_id, new_status):
    if new_status in (AssignedTestStatus.SUBMITTED, AssignedTestStatus.TIMED_OUT):
        check = TestScorerAssignment.objects.filter(assigned_test_id=test_id)
        if not check:
            assignment = TestScorerAssignment(assigned_test_id=test_id)
            assignment.save()


def submit_test(test_id, new_status):
    test = AssignedTest.objects.get(id=test_id)

    if new_status in (
        AssignedTestStatus.SUBMITTED,
        AssignedTestStatus.TIMED_OUT,
        AssignedTestStatus.QUIT,
    ):
        test.submit_date = datetime.now()

    test.status = new_status

    # clean out the notepad
    delete_notepad(test_id)

    prepare_test_for_scoring(test_id, new_status)

    return save_and_return(test)


def save_and_return(test):
    test.save()
    return Response()


def update_candidate_test_status(request):
    user_info = get_user_info_from_jwt_token(request)
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    test_status = request.query_params.get("test_status", None)
    previous_status = request.query_params.get("previous_status", None)
    if is_undefined(assigned_test_id):
        return Response(
            {"error": "The assigned_test_id was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_status):
        return Response(
            {"error": "The test_status was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    test_status_exists = False
    # looping in AssignedTestStatus attributes
    for attr in vars(AssignedTestStatus):
        # exclude unwanted attributes (generic python attributes)
        if not attr.startswith("__"):
            if getattr(AssignedTestStatus, attr) == int(test_status):
                test_status_exists = True
    # specified test status does not exists
    if not test_status_exists:
        return Response(
            {"error": "the specified test status does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        try:
            assigned_test = AssignedTest.objects.get(
                id=assigned_test_id, username_id=user_info["username"]
            )
            assigned_test.status = test_status
            assigned_test.save()

            # previous status is PAUSED + current status is ACTIVE (unpausing test on timer timeout action)
            if (
                int(previous_status) == AssignedTestStatus.PAUSED
                and int(test_status) == AssignedTestStatus.ACTIVE
            ):
                # getting last pause ta action
                last_pause_ta_action = TaActions.objects.filter(
                    action_type_id=TaActionsConstants.PAUSE,
                    assigned_test_id=assigned_test_id,
                ).last()
                # getting pause test action
                pause_test_action = PauseTestActions.objects.get(
                    ta_action_id=last_pause_ta_action.id
                )
                # adding pause test time to pause start date value
                updated_date = pause_test_action.pause_start_date + timedelta(
                    minutes=pause_test_action.pause_test_time
                )
                # updating lock_end_date field
                pause_test_action.pause_end_date = updated_date
                pause_test_action.save()
            return Response(status=status.HTTP_200_OK)

        except AssignedTest.DoesNotExist:
            return Response(
                {"error": "the specified assigned test id does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        except TaActions.DoesNotExist:
            return Response(
                {"error": "unable to find a ta action based on provided data"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        except PauseTestActions.DoesNotExist:
            return Response(
                {"error": "unable to find a pause test action based on provided data"},
                status=status.HTTP_400_BAD_REQUEST,
            )
