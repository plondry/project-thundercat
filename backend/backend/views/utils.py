import jwt
from django.conf import settings

# function that verifies if the specified parameter is undefined


def is_undefined(value):
    if value is None:
        return True
    if value == "undefined":
        return True
    if value == "null":
        return True
    return False


# function that is getting the user information from decrypted provided auth token (JWT token)
def get_user_info_from_jwt_token(request):
    # getting complete auth token
    complete_auth_token = request.headers["Authorization"]
    # removing "JWT" from token
    auth_token = complete_auth_token[4:]
    # decrypting jwt token to get user's information
    user_info = jwt.decode(
        auth_token, settings.SECRET_KEY, algorithms=["HS256"])
    # returning user info object
    return user_info


def get_user_info_from_token_string(token_string):
    auth_token = token_string
    # decrypting jwt token to get user's information
    user_info = jwt.decode(
        auth_token, settings.SECRET_KEY, algorithms=["HS256"])
    # returning user info object
    return user_info
