from rest_framework.response import Response
from rest_framework import status
from django.db.models import Count
from backend.views.utils import is_undefined, get_user_info_from_jwt_token
from backend.custom_models.assigned_test import AssignedTest
from backend.serializers.reports_data_serializer import (
    TestOrderNumberSerializer,
    ResultsReportDataSerializer,
    FinancialReportDataSerializer,
)
from cms.cms_models.test_permissions_model import TestPermissions


# retrieving TA assigned test order numbers
def retrieve_ta_assigned_test_order_numbers(request):
    ta_id = request.query_params.get("ta_id", None)
    user_info = get_user_info_from_jwt_token(request)
    # initializing query_list
    query_list = None
    # ta_id parameter is provided
    if ta_id:
        query_list = TestPermissions.objects.filter(username_id=ta_id)
    # ta_id is not provided (use the token to get the username)
    else:
        query_list = TestPermissions.objects.filter(username_id=user_info["username"])

    # removing duplicates
    assigned_test_ids_without_duplicate = []
    assigned_test_order_numbers = []

    for assigned_test in query_list:
        if assigned_test.test_order_number not in assigned_test_order_numbers:
            assigned_test_ids_without_duplicate.append(assigned_test.id)
            assigned_test_order_numbers.append(assigned_test.test_order_number)

    query_list_without_duplicates = TestPermissions.objects.filter(
        id__in=assigned_test_ids_without_duplicate
    )

    # returning data
    data = TestOrderNumberSerializer(query_list_without_duplicates, many=True)
    return Response(data.data)


# retrieving all existing test order numbers
def retrieve_all_existing_test_order_numbers(request):
    # initializing query_list
    query_list = TestPermissions.objects.all()

    # removing duplicates
    test_order_numbers = []
    test_order_number_ids_without_duplicates = []

    for test_permission in query_list:
        if test_permission.test_order_number not in test_order_numbers:
            test_order_number_ids_without_duplicates.append(test_permission.id)
            test_order_numbers.append(test_permission.test_order_number)

    query_list_without_duplicates = TestPermissions.objects.filter(
        id__in=test_order_number_ids_without_duplicates
    )

    # returning data
    data = TestOrderNumberSerializer(query_list_without_duplicates, many=True)
    return Response(data.data)


# retrieving tests based on provided test order number
def retrieve_tests_based_on_test_order_number(request):
    test_order_number = request.query_params.get("test_order_number", None)
    if is_undefined(test_order_number) or test_order_number == "":
        return Response(
            {"error": "The test order number was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    query_list = TestPermissions.objects.filter(test_order_number=test_order_number)
    # returning data
    data = TestOrderNumberSerializer(query_list, many=True)
    return Response(data.data)


# retrieving report data based on provided test order number and test (and potentially username_id)
def retrieve_results_report_data(request):
    ta_id = request.query_params.get("ta_id", None)
    test_order_number = request.query_params.get("test_order_number", None)
    # test_ids parameter needs to be provided as a string using the following format: "<test_id>,<test_id>,<test_id>,..."
    test_ids = request.query_params.get("test_ids", None)
    username_id = request.query_params.get("username_id", None)
    # boolean that allows you to keep or remove duplicates
    remove_duplicates = request.query_params.get("remove_duplicates", None)
    if is_undefined(test_order_number) or test_order_number == "":
        return Response(
            {"error": "The test order number was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_ids) or test_ids == "":
        return Response(
            {"error": "The test_ids was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(remove_duplicates) or test_ids == "":
        return Response(
            {"error": "The remove_duplicates was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # initializing test_ids array
    test_ids_params = []

    # creating list based on query parameters (test_ids)
    for test_id in test_ids.split(","):
        test_ids_params.append(int(test_id))
    # set assigned_test_status_min_level
    # test that has been inactive for too long, submitted, quit, timed out, etc
    assigned_test_status_min_level = 17
    # initializing query_list
    query_list = None
    # ta_id and username_id parameters are provided
    if ta_id and username_id:
        query_list = AssignedTest.objects.filter(
            ta_id=ta_id,
            test_order_number=test_order_number,
            test_id__in=test_ids_params,
            username_id=username_id,
            status__gte=assigned_test_status_min_level,
        )
    # ta_id parameter is provided
    elif ta_id and not username_id:
        query_list = AssignedTest.objects.filter(
            ta_id=ta_id,
            test_order_number=test_order_number,
            test_id__in=test_ids_params,
            status__gte=assigned_test_status_min_level,
        )
    # username_id parameter is provided
    elif username_id and not ta_id:
        query_list = AssignedTest.objects.filter(
            test_order_number=test_order_number,
            test_id__in=test_ids_params,
            username_id=username_id,
            status__gte=assigned_test_status_min_level,
        )
    # ta_id and username_id not provided (used for etta and ppc folks)
    else:
        query_list = AssignedTest.objects.filter(
            test_order_number=test_order_number,
            status__gte=assigned_test_status_min_level,
            test_id__in=test_ids_params,
        )

    # initializing final_query_set
    final_query_set = []

    # remove_duplicates is set to true
    if remove_duplicates == "true":
        # removing duplicates
        candidate_ids_without_duplicate = []
        usernames = []

        for assigned_test in query_list:
            if assigned_test.username_id not in usernames:
                candidate_ids_without_duplicate.append(assigned_test.id)
                usernames.append(assigned_test.username_id)

        final_query_set = AssignedTest.objects.filter(
            id__in=candidate_ids_without_duplicate
        )
    # remove_duplicates is set to false
    else:
        final_query_set = query_list
    # returning data
    data = ResultsReportDataSerializer(final_query_set, many=True)
    return Response(data.data)


def retrieve_financial_report_data(request):
    test_order_number = request.query_params.get("test_order_number", None)
    if is_undefined(test_order_number) or test_order_number == "":
        return Response(
            {"error": "The test order number was not defined."},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # set assigned_test_status_min_level
    # test that has been inactive for too long, submitted, quit, timed out, etc
    assigned_test_status_min_level = 17

    # generating query_set (GROUP_BY "test_id")
    query_set = (
        AssignedTest.objects.filter(
            test_order_number=test_order_number,
            status__gte=assigned_test_status_min_level,
        )
        .values("test_id", "test_order_number")
        .annotate(number_of_tests=Count("test_id"))
    )

    # returning data
    data = FinancialReportDataSerializer(query_set, many=True)
    return Response(data.data)
