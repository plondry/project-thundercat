from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.serializers.assigned_test_serializer import UitAnswersSerializer
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.answer import Answer
from cms.static.question_type import QuestionType


class Action:
    SEEN_QUESTION = "SEEN_QUESTION"
    SAVE_ANSWERS = "SAVE_ANSWERS"
    GET_ANSWERS = "GET_ANSWERS"


def test_actions(request, needed_parameters, action):
    parameters = []
    # handling missing parameters
    for parameter in needed_parameters:
        parameter_name = "{}".format(parameter)
        parameter = request.query_params.get(parameter_name, None)
        if is_undefined(parameter):
            return Response(
                {"error": "no '{}' parameter".format(parameter_name)},
                status=status.HTTP_400_BAD_REQUEST,
            )
        parameters.append(parameter)

    try:
        # get question object based on question id
        if action != Action.GET_ANSWERS:
            question = NewQuestion.objects.get(id=parameters[1])

            # initializing question already seen flag
            question_already_seen = False

            # verifying if question has already been seen
            try:
                # getting candidate answers object
                candidate_answers_obj = CandidateAnswers.objects.get(
                    assigned_test_id=AssignedTest.objects.get(id=parameters[0]).id,
                    question_id=parameters[1],
                    test_section_component_id=question.test_section_component_id,
                )

                # set question_already_seen to true
                question_already_seen = True

            except AssignedTest.DoesNotExist:
                return Response(
                    {"error": "the specified assigned test does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            # question has never been seen
            except CandidateAnswers.DoesNotExist:
                # set question_already_seen to false
                question_already_seen = False

        # seen question (add row to CandidateAnswers and CandidateMultipleChoiceAnswers tables)
        if action == Action.SEEN_QUESTION:
            # question has never been seen
            if not question_already_seen:
                # creating new candidate answers object
                candidate_answers_obj = CandidateAnswers.objects.create(
                    assigned_test_id=parameters[0],
                    question_id=parameters[1],
                    test_section_component_id=question.test_section_component_id,
                    time_spent=0,
                )
                # create a new row in candidate multiple choice answers table only if the question type is 'multiple_choice'
                if question.question_type == QuestionType.MULTIPLE_CHOICE:
                    # creating new candidate multiple choice answers object
                    CandidateMultipleChoiceAnswers.objects.create(
                        answer_id=None, candidate_answers_id=candidate_answers_obj.id
                    )

            return Response(status=status.HTTP_200_OK)
        elif action == Action.SAVE_ANSWERS:
            try:
                # getting candidate multiple choice answers object
                candidate_multiple_choice_answers_obj = CandidateMultipleChoiceAnswers.objects.get(
                    candidate_answers_id=candidate_answers_obj.id
                )
                # updating candidate answers object
                candidate_multiple_choice_answers_obj.answer_id = Answer.objects.get(
                    id=parameters[2], question_id=parameters[1]
                ).id
                candidate_multiple_choice_answers_obj.save()
                return Response(status=status.HTTP_200_OK)

            except CandidateMultipleChoiceAnswers.DoesNotExist:
                return Response(
                    {
                        "error": "the specified candidate multiple choice answer does not exist"
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

            except Answer.DoesNotExist:
                return Response(
                    {"error": "the specified answer does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        elif action == Action.GET_ANSWERS:
            # making sure that the test_section_component_id exists

            if not TestSectionComponent.objects.filter(id=parameters[1]):
                return Response(
                    {"error": "the specified test section component does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            # getting assigned test object
            assigned_test = AssignedTest.objects.filter(id=parameters[0])

            # assigned test exists
            if assigned_test:
                serializer = UitAnswersSerializer(
                    assigned_test,
                    many=True,
                    context={"test_section_component_id": parameters[1]},
                )
                return Response(serializer.data)
            # assigned test does not exist
            else:
                return Response(
                    {"error": "the specified assigned test does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {"error": "{} action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except CandidateAnswers.DoesNotExist:
        return Response(
            {"error": "the specified candidate answer does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except ValueError:
        return Response(
            {"error": "the specified parameters are invalid"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    except NewQuestion.DoesNotExist:
        return Response(
            {"error": "the specified question does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
