from rest_framework.response import Response
from rest_framework import status
from backend.custom_models.test_access_code_model import TestAccessCode
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_test_section import AssignedTestSection
from user_management.user_management_models.user_models import User
from cms.cms_models.test_section import TestSection


def check_into_room(test_access_code, username):
    unsupervised_test = False
    # find DAOs
    room = TestAccessCode.objects.filter(test_access_code=test_access_code).first()
    if room.ta_username_id is None:
        unsupervised_test = True
    if not unsupervised_test:
        test_admin = User.objects.get(username=room.ta_username)
    user = User.objects.get(username=username)
    test_id = room.test_id

    if not unsupervised_test:
        # don't allow a TA to take their own exam
        if user.username == test_admin.username:
            return Response(
                {
                    "error": "You cannot be a candidate in an exam you are administering."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

    # check if this rooms test has already been assigned
    if not has_test_for_test_access_code_already_been_assigned(
        test_access_code, username
    ):
        if not unsupervised_test:
            assigned_test = AssignedTest(
                status=AssignedTestStatus.ASSIGNED,
                test_id=test_id,
                username_id=user.username,
                ta=test_admin,
                test_access_code=test_access_code,
                test_order_number=room.test_order_number,
                test_session_language=room.test_session_language,
            )
            assigned_test.save()
        else:
            assigned_test = AssignedTest(
                status=AssignedTestStatus.READY,
                test_id=test_id,
                username_id=user.username,
                ta=None,
                test_access_code=test_access_code,
                test_order_number=None,
                test_session_language=None,
            )
            assigned_test.save()

        # find all test sections for this test and create an assigned test section for it
        test_sections = TestSection.objects.filter(test_definition_id=test_id)
        for test_section in test_sections:
            assigned_test_section = AssignedTestSection(
                test_section=test_section,
                assigned_test=assigned_test,
                test_section_time=test_section.default_time,
            )
            assigned_test_section.save()

        # deleting test access code only if the specified test access code has no assigned ta username (meaning that this is a unsupervised test)
        if room.ta_username_id is None:
            TestAccessCode.objects.get(test_access_code=test_access_code).delete()

    return Response(status=status.HTTP_200_OK)


def has_test_for_test_access_code_already_been_assigned(test_access_code, username):
    return (
        AssignedTest.objects.filter(
            username__username=username, test_access_code=test_access_code
        ).count()
        > 0
    )
