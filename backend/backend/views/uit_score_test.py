from operator import or_
from functools import reduce
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from backend.views.utils import is_undefined
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.answer import Answer
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.test_section import TestSection
from cms.static.test_section_component_type import TestSectionComponentType
from backend.static.test_section_scoring_type import TestSectionScoringType


class Action:
    TEST_SECTION = "TEST_SECTION"
    TEST = "TEST"


def uit_scoring(parameters, action):
    try:
        # Score Test Section
        if action == Action.TEST_SECTION:
            try:
                # getting test section scoring_type
                scoring_type = TestSection.objects.get(
                    id=parameters["test_section_id"]
                ).scoring_type

            except TestSection.DoesNotExist:
                return Response(
                    {"error": "the specified test section does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            # test section scoring_type
            if scoring_type == TestSectionScoringType.AUTO_SCORE:
                # getting test section component id based on provided test_section_id
                test_section_component_id = TestSectionComponent.objects.get(
                    test_section=parameters["test_section_id"],
                    component_type__in=[TestSectionComponentType.QUESTION_LIST],
                ).id

                # getting candidate answers objects for specified assigned test
                candidate_answers = CandidateAnswers.objects.filter(
                    assigned_test_id=parameters["assigned_test_id"],
                    test_section_component_id=test_section_component_id,
                )

                # candidate answers contains at least one object
                if candidate_answers:
                    # getting questions answered
                    questions_answered = CandidateMultipleChoiceAnswers.objects.filter(
                        reduce(
                            or_,
                            [
                                Q(candidate_answers_id=candidate_answer.id)
                                for candidate_answer in candidate_answers
                            ],
                        )
                    )

                    # initializing score
                    score = 0

                    for question_answered in questions_answered:
                        # making sure that the current question has an answer (not null)
                        if question_answered.answer_id is not None:
                            # making sure that the current question is not a pilot question and that it is part of the current test section
                            is_pilot_question = NewQuestion.objects.get(
                                id=CandidateAnswers.objects.get(
                                    id=question_answered.candidate_answers_id
                                ).question_id
                            ).pilot

                            # current question is not a pilot question
                            if not is_pilot_question:
                                # getting current question's scoring value
                                scoring_value = Answer.objects.get(
                                    id=question_answered.answer_id
                                ).scoring_value
                                # adding current scoring value to score
                                score += scoring_value

                    try:
                        # saving score in assigned test section table
                        assigned_test_section = AssignedTestSection.objects.get(
                            assigned_test_id=parameters["assigned_test_id"],
                            test_section_id=parameters["test_section_id"],
                        )
                        assigned_test_section.score = score
                        assigned_test_section.save()

                    except AssignedTestSection.DoesNotExist:
                        return Response(
                            {
                                "error": "the specified assigned test section does not exist"
                            },
                            status=status.HTTP_400_BAD_REQUEST,
                        )

                    # returning calculated score
                    # temp json response
                    return Response({"status": status.HTTP_200_OK, "score": score})

                # no question
                else:
                    return Response(
                        {
                            "error": "there are no questions answered based on provided parameters"
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )

            # test section not auto-scored
            else:
                # returning calculated score
                # temp json response
                return Response(
                    {
                        "status": status.HTTP_200_OK,
                        "info": "this test section is not auto-scored",
                    }
                )

        # Score Test
        elif action == Action.TEST:
            # initializing total score
            total_score = 0

            # getting all assigned test sections of current assigned test
            assigned_test_sections = AssignedTestSection.objects.filter(
                assigned_test_id=parameters["assigned_test_id"]
            )

            # flag if this test will need a scorer or not
            requires_scorer = False

            # looping in assigned test sections
            for test_section in assigned_test_sections:
                # Check if the section is manually scored
                if (
                    test_section.test_section.scoring_type
                    == TestSectionScoringType.COMPETENCY
                ):
                    requires_scorer = True
                # making sure that the score is not null (will be null if the test section is not auto-scored)
                if test_section.score is not None:
                    # adding current test section score to total score
                    total_score += test_section.score

            # if it requires a scorer, then add it to the unassigned tests
            if requires_scorer:
                # ensure it is not already in the scorer assignment test table
                if not TestScorerAssignment.objects.filter(
                    assigned_test_id=parameters["assigned_test_id"]
                ):
                    # create create a test that is not assigned to any scorer
                    assignment = TestScorerAssignment(
                        assigned_test_id=parameters["assigned_test_id"]
                    )
                    assignment.save()

            try:
                # saving total score in uit assigned test table
                assigned_test = AssignedTest.objects.get(
                    id=parameters["assigned_test_id"]
                )
                assigned_test.total_score = total_score
                assigned_test.save()

            except AssignedTest.DoesNotExist:
                return Response(
                    {"error": "the specified assigned test does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            # returning calculated total score
            # temp json response
            return Response({"status": status.HTTP_200_OK, "total_score": total_score})

        # Action does not exist
        else:
            return Response(
                {"error": "{} action does not exist".format(action)},
                status=status.HTTP_400_BAD_REQUEST,
            )

    except ValueError:
        return Response(
            {"error": "the specified parameters are invalid"},
            status=status.HTTP_400_BAD_REQUEST,
        )
