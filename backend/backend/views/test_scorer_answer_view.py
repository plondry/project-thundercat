from rest_framework.views import APIView
from rest_framework import permissions
from backend.views.test_scorer_answer import save_score, get_scores
from rest_framework import status
from rest_framework.response import Response
from cms.views.utils import get_needed_parameters


class SaveScore(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["competency", "question", "rationale", "score", "scorer_test"], request
        )
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return save_score(parameters)

    def get_permissions(self):
        # TODO is test assigned to scorer
        return [permissions.IsAuthenticated()]


class GetScores(APIView):
    def get(self, request):
        success, parameters = get_needed_parameters(["scorer_test"], request)
        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return get_scores(parameters)

    def get_permissions(self):
        # TODO is test assigned to scorer
        return [permissions.IsAuthenticated()]
