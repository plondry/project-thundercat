from rest_framework import serializers
from backend.custom_models.candidate_task_response_answers import (
    CandidateTaskResponseAnswers,
)
from backend.serializers.candidate_task_response_answers_serializer import (
    CandidateTaskResponseAnswersSerializer,
)
from backend.serializers.candidate_email_response_answers_serializer import (
    CandidateEmailResponseAnswersSerializer,
)
from backend.custom_models.candidate_email_response_answers import (
    CandidateEmailResponseAnswers,
)
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.candidate_answers import CandidateAnswers
from backend.custom_models.candidate_multiple_choice_answers import (
    CandidateMultipleChoiceAnswers,
)
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.pause_test_actions import PauseTestActions
from backend.views.test_administrator_actions import TaActionsConstants
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section_component import TestSectionComponent
from cms.static.test_section_component_type import TestSectionComponentType
from cms.serializers.test_section_serializer import AssignedTestSectionSerializer
from user_management.user_management_models.user_models import User


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class AssignedTestSerializer(serializers.ModelSerializer):
    test = TestSerializer(many=False)
    assigned_test_sections = serializers.SerializerMethodField()

    candidate_id = serializers.SerializerMethodField()
    candidate_first_name = serializers.SerializerMethodField()
    candidate_last_name = serializers.SerializerMethodField()
    candidate_dob = serializers.SerializerMethodField()
    pause_test_time = serializers.SerializerMethodField()
    pause_start_date = serializers.SerializerMethodField()
    previous_status = serializers.SerializerMethodField()

    def get_assigned_test_sections(self, assigned_test):
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=assigned_test.id
        )
        return AssignedTestSectionSerializer(assigned_test_sections, many=True).data

    def get_candidate_id(self, assigned_test):
        candidate_id = User.objects.get(username=assigned_test.username).id
        return candidate_id

    def get_candidate_first_name(self, assigned_test):
        candidate_first_name = User.objects.get(
            username=assigned_test.username
        ).first_name
        return candidate_first_name

    def get_candidate_last_name(self, assigned_test):
        candidate_last_name = User.objects.get(
            username=assigned_test.username
        ).last_name
        return candidate_last_name

    def get_candidate_dob(self, assigned_test):
        candidate_dob = User.objects.get(username=assigned_test.username).birth_date
        return candidate_dob

    def get_pause_test_time(self, assigned_test):
        try:
            ta_action_id = (
                TaActions.objects.filter(
                    assigned_test_id=assigned_test.id,
                    action_type_id=TaActionsConstants.PAUSE,
                )
                .last()
                .id
            )

            pause_test_time = PauseTestActions.objects.get(
                ta_action_id=ta_action_id
            ).pause_test_time

        # ta_action_id does not exist
        except:
            pause_test_time = None
            pass
        return pause_test_time

    def get_pause_start_date(self, assigned_test):
        try:
            ta_action_id = (
                TaActions.objects.filter(
                    assigned_test_id=assigned_test.id,
                    action_type_id=TaActionsConstants.PAUSE,
                )
                .last()
                .id
            )

            pause_start_date = PauseTestActions.objects.get(
                ta_action_id=ta_action_id
            ).pause_start_date

        # ta_action_id does not exist
        except:
            pause_start_date = None
            pass
        return pause_start_date

    def get_previous_status(self, assigned_test):
        previous_status = AssignedTest.objects.get(id=assigned_test.id).previous_status
        return previous_status

    class Meta:
        model = AssignedTest
        fields = "__all__"


class UitAssignedTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignedTest
        fields = "__all__"


class UitFormattedAnswersSerializer(serializers.ModelSerializer):
    answer_id = serializers.SerializerMethodField()

    def get_answer_id(self, candidate_answers):
        answer_id = CandidateMultipleChoiceAnswers.objects.get(
            candidate_answers_id=candidate_answers.id
        ).answer_id
        return answer_id

    class Meta:
        model = CandidateAnswers
        fields = ["question_id", "answer_id"]


class UitFormattedInboxAnswersSerializer(serializers.ModelSerializer):
    email_answers = serializers.SerializerMethodField()
    task_answers = serializers.SerializerMethodField()

    def get_email_answers(self, candidate_answers):
        obj = CandidateEmailResponseAnswers.objects.filter(
            candidate_answers_id=candidate_answers.id
        )
        return CandidateEmailResponseAnswersSerializer(obj, many=True).data

    def get_task_answers(self, candidate_answers):
        obj = CandidateTaskResponseAnswers.objects.filter(
            candidate_answers_id=candidate_answers.id
        )
        return CandidateTaskResponseAnswersSerializer(obj, many=True).data

    class Meta:
        model = CandidateAnswers
        fields = ["question_id", "email_answers", "task_answers"]


class UitAnswersSerializer(serializers.ModelSerializer):
    answers = serializers.SerializerMethodField()

    def get_answers(self, assigned_test):
        test_section_component_id = self.context.get("test_section_component_id", None)
        test_section_component = TestSectionComponent.objects.get(
            id=test_section_component_id
        )
        # getting candidate_answers objects
        candidate_answers = CandidateAnswers.objects.filter(
            assigned_test_id=assigned_test.id
        )
        # get answers depending on question type
        if (
            test_section_component.component_type
            == TestSectionComponentType.QUESTION_LIST
        ):
            # getting answers data using UitFormattedAnswersSerializer serializer
            answers_serializer = UitFormattedAnswersSerializer(
                candidate_answers, many=True
            )

        elif test_section_component.component_type == TestSectionComponentType.INBOX:
            answers_serializer = UitFormattedInboxAnswersSerializer(
                candidate_answers, many=True
            )
        # returning serialized data
        return answers_serializer.data

    class Meta:
        model = AssignedTest
        fields = "__all__"
