from rest_framework import serializers
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.serializers.assigned_test_serializer import AssignedTestSerializer


# Serializers define the API representation
class GetTestScorerAssignmentSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = TestScorerAssignment
        fields = ["id", "scorer_username", "status", "score_date", "details"]

    def to_representation(self, obj):
        """Move fields from get details to main object."""
        # reduces number of queries
        representation = super().to_representation(obj)
        details_representation = representation.pop("details")
        for key in details_representation:
            representation[key] = details_representation[key]
        return representation

    def get_details(self, test_scorer_assignment):
        data = {}
        assigned_test = AssignedTest.objects.get(
            id=test_scorer_assignment.assigned_test_id
        )
        data["assigned_test_id"] = assigned_test.id
        data["assigned_test_status"] = assigned_test.status
        data["assigned_test_start_date"] = assigned_test.start_date
        data["assigned_test_test_code"] = assigned_test.test.test_code
        data["assigned_test_en_name"] = assigned_test.test.en_name
        data["assigned_test_fr_name"] = assigned_test.test.fr_name
        data["assigned_test_submit_date"] = assigned_test.submit_date.date()
        data["assigned_test_submit_time"] = assigned_test.submit_date.time()
        language = assigned_test.test_session_language
        if language is None:
            language = None
        else:
            language = language.ISO_Code_1
        data["assigned_test_test_session_language"] = language
        return data
