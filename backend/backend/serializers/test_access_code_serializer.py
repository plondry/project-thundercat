from rest_framework import serializers
from backend.custom_models.test_access_code_model import TestAccessCode
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions


class TestAccessCodeSerializer(serializers.ModelSerializer):
    en_test_name = serializers.SerializerMethodField()
    fr_test_name = serializers.SerializerMethodField()
    staffing_process_number = serializers.SerializerMethodField()
    department_ministry_code = serializers.SerializerMethodField()
    is_org = serializers.SerializerMethodField()
    is_ref = serializers.SerializerMethodField()
    billing_contact = serializers.SerializerMethodField()
    billing_contact_info = serializers.SerializerMethodField()

    def get_test_permission_data(self, request):
        test_permission_data = TestPermissions.objects.get(
            username_id=request.ta_username_id,
            test_id=request.test_id,
            test_order_number=request.test_order_number,
        )
        return test_permission_data

    def get_en_test_name(self, request):
        en_test_name = TestDefinition.objects.get(id=request.test_id).en_name
        return en_test_name

    def get_fr_test_name(self, request):
        fr_test_name = TestDefinition.objects.get(id=request.test_id).fr_name
        return fr_test_name

    def get_staffing_process_number(self, request):
        return self.get_test_permission_data(request).staffing_process_number

    def get_department_ministry_code(self, request):
        return self.get_test_permission_data(request).department_ministry_code

    def get_is_org(self, request):
        return self.get_test_permission_data(request).is_org

    def get_is_ref(self, request):
        return self.get_test_permission_data(request).is_ref

    def get_billing_contact(self, request):
        return self.get_test_permission_data(request).billing_contact

    def get_billing_contact_info(self, request):
        return self.get_test_permission_data(request).billing_contact_info

    class Meta:
        model = TestAccessCode
        fields = "__all__"
