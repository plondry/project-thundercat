from rest_framework import serializers
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.test_definition import TestDefinition
from cms.serializers.test_section_serializer import AssignedTestSectionSerializer
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.pause_test_actions import PauseTestActions
from backend.views.test_administrator_actions import TaActionsConstants
from user_management.user_management_models.user_models import User


class TestOrderNumberSerializer(serializers.ModelSerializer):
    staffing_process_number = serializers.SerializerMethodField()
    en_test_name = serializers.SerializerMethodField()
    fr_test_name = serializers.SerializerMethodField()

    def get_staffing_process_number(self, request):
        staffing_process_number = TestPermissions.objects.get(
            id=request.id
        ).staffing_process_number
        return staffing_process_number

    def get_en_test_name(self, request):
        en_test_name = TestDefinition.objects.get(id=request.test_id).en_name
        return en_test_name

    def get_fr_test_name(self, request):
        fr_test_name = TestDefinition.objects.get(id=request.test_id).fr_name
        return fr_test_name

    class Meta:
        model = TestPermissions
        fields = [
            "id",
            "test_id",
            "en_test_name",
            "fr_test_name",
            "test_order_number",
            "staffing_process_number",
        ]


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class ResultsReportDataSerializer(serializers.ModelSerializer):
    test = TestSerializer(many=False)
    assigned_test_sections = serializers.SerializerMethodField()

    candidate_id = serializers.SerializerMethodField()
    candidate_first_name = serializers.SerializerMethodField()
    candidate_last_name = serializers.SerializerMethodField()
    candidate_dob = serializers.SerializerMethodField()
    candidate_pri_or_military_nbr = serializers.SerializerMethodField()
    pause_test_time = serializers.SerializerMethodField()
    pause_start_date = serializers.SerializerMethodField()
    previous_status = serializers.SerializerMethodField()
    staffing_process_number = serializers.SerializerMethodField()

    def get_assigned_test_sections(self, assigned_test):
        assigned_test_sections = AssignedTestSection.objects.filter(
            assigned_test_id=assigned_test.id
        )
        return AssignedTestSectionSerializer(assigned_test_sections, many=True).data

    def get_candidate_id(self, assigned_test):
        candidate_id = User.objects.get(username=assigned_test.username).id
        return candidate_id

    def get_candidate_first_name(self, assigned_test):
        candidate_first_name = User.objects.get(
            username=assigned_test.username
        ).first_name
        return candidate_first_name

    def get_candidate_last_name(self, assigned_test):
        candidate_last_name = User.objects.get(
            username=assigned_test.username
        ).last_name
        return candidate_last_name

    def get_candidate_dob(self, assigned_test):
        candidate_dob = User.objects.get(username=assigned_test.username).birth_date
        return candidate_dob

    def get_candidate_pri_or_military_nbr(self, assigned_test):
        candidate_pri_or_military_nbr = User.objects.get(
            username=assigned_test.username
        ).pri_or_military_nbr
        return candidate_pri_or_military_nbr

    def get_pause_test_time(self, assigned_test):
        try:
            ta_action_id = (
                TaActions.objects.filter(
                    assigned_test_id=assigned_test.id,
                    action_type_id=TaActionsConstants.PAUSE,
                )
                .last()
                .id
            )

            pause_test_time = PauseTestActions.objects.get(
                ta_action_id=ta_action_id
            ).pause_test_time

        # ta_action_id does not exist
        except:
            pause_test_time = None
            pass
        return pause_test_time

    def get_pause_start_date(self, assigned_test):
        try:
            ta_action_id = (
                TaActions.objects.filter(
                    assigned_test_id=assigned_test.id,
                    action_type_id=TaActionsConstants.PAUSE,
                )
                .last()
                .id
            )

            pause_start_date = PauseTestActions.objects.get(
                ta_action_id=ta_action_id
            ).pause_start_date

        # ta_action_id does not exist
        except:
            pause_start_date = None
            pass
        return pause_start_date

    def get_previous_status(self, assigned_test):
        previous_status = AssignedTest.objects.get(id=assigned_test.id).previous_status
        return previous_status

    def get_staffing_process_number(self, request):
        staffing_process_number = (
            TestPermissions.objects.filter(test_order_number=request.test_order_number)
            .first()
            .staffing_process_number
        )
        return staffing_process_number

    class Meta:
        model = AssignedTest
        fields = "__all__"


class FinancialReportDataSerializer(serializers.ModelSerializer):
    staffing_process_number = serializers.SerializerMethodField()
    department_ministry_code = serializers.SerializerMethodField()
    is_org = serializers.SerializerMethodField()
    is_ref = serializers.SerializerMethodField()
    billing_contact = serializers.SerializerMethodField()
    billing_contact_info = serializers.SerializerMethodField()
    test_description_fr = serializers.SerializerMethodField()
    test_description_en = serializers.SerializerMethodField()
    number_of_tests = serializers.SerializerMethodField()

    def get_staffing_process_number(self, request):
        staffing_process_number = (
            TestPermissions.objects.filter(
                test_order_number=request["test_order_number"]
            )
            .first()
            .staffing_process_number
        )
        return staffing_process_number

    def get_department_ministry_code(self, request):
        department_ministry_code = (
            TestPermissions.objects.filter(
                test_order_number=request["test_order_number"]
            )
            .first()
            .department_ministry_code
        )
        return department_ministry_code

    def get_is_org(self, request):
        is_org = (
            TestPermissions.objects.filter(
                test_order_number=request["test_order_number"]
            )
            .first()
            .is_org
        )
        return is_org

    def get_is_ref(self, request):
        is_ref = (
            TestPermissions.objects.filter(
                test_order_number=request["test_order_number"]
            )
            .first()
            .is_ref
        )
        return is_ref

    def get_billing_contact(self, request):
        billing_contact = (
            TestPermissions.objects.filter(
                test_order_number=request["test_order_number"]
            )
            .first()
            .billing_contact
        )
        return billing_contact

    def get_billing_contact_info(self, request):
        billing_contact_info = (
            TestPermissions.objects.filter(
                test_order_number=request["test_order_number"]
            )
            .first()
            .billing_contact_info
        )
        return billing_contact_info

    def get_test_description_fr(self, request):
        test_description_fr = TestDefinition.objects.get(id=request["test_id"]).fr_name
        return test_description_fr

    def get_test_description_en(self, request):
        test_description_en = TestDefinition.objects.get(id=request["test_id"]).en_name
        return test_description_en

    def get_number_of_tests(self, request):
        number_of_tests = request["number_of_tests"]
        return number_of_tests

    class Meta:
        model = AssignedTest
        fields = [
            "test_order_number",
            "staffing_process_number",
            "department_ministry_code",
            "is_org",
            "is_ref",
            "billing_contact",
            "billing_contact_info",
            "test_id",
            "test_description_fr",
            "test_description_en",
            "number_of_tests",
        ]
