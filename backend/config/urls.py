from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from django.urls import path, include
from django.contrib import admin
from rest_framework import routers
from rest_framework_swagger.views import get_swagger_view
from backend.views.testStatus import update_test_status
from backend.views import (
    views,
    database_check_view,
    assigned_tests_view,
    room_check_in_view,
    notepad_view,
    update_email_answer_view,
    test_scorer_answer_view,
)
from cms.views import (
    retrieve_test_information_view,
    test_permissions,
    test_section_data_view,
    get_file_view,
    public_tests,
    get_test_definitions_view,
    scorer_test_section_view,
    test_builder_view,
)
from rest_framework_jwt.views import (
    obtain_jwt_token,
    refresh_jwt_token,
    verify_jwt_token,
)

from backend.views import (
    test_access_code_view,
    test_scorer_assignment_view,
    tics_view,
    uit_assigned_tests_view,
    psrs_generate_test_access_code_simulation,
    time_spent_view,
    uit_scoring_view,
    test_administration_view,
    reports_data_view,
)
from user_management.views import (
    permissions,
    user_personal_info,
    user_accommodations_view,
)

schema_view = get_swagger_view(title="ThunderCAT APIs")

router = routers.DefaultRouter()
router.register(r"oec-cat/api/database-check", database_check_view.DatabaseViewSet)

urlpatterns = [
    url(r"^$", schema_view),
    url(r"^oec-cat/api/admin/", admin.site.urls),
    url(r"^oec-cat/api/auth/", include("djoser.urls")),
    url(r"^oec-cat/api/auth/", include("djoser.urls.authtoken")),
    path(r"oec-cat/api/backend-status", views.index, name="index"),
    path("", include(router.urls)),
    url(
        r"^oec-cat/api/auth/",
        include("rest_framework.urls", namespace="rest_framework"),
    ),
    url(r"^oec-cat/api/auth/jwt/create_token/", obtain_jwt_token),
    url(r"^oec-cat/api/auth/jwt/refresh_token/", refresh_jwt_token),
    url(r"^oec-cat/api/auth/jwt/verify_token/", verify_jwt_token),
    url(
        r"^oec-cat/api/update-user-personal-info",
        user_personal_info.UpdateUserPersonalInfo.as_view(),
    ),
    url(
        r"^oec-cat/api/update-user-last-password-change-time",
        user_personal_info.UpdateUserLastPasswordChangeTime.as_view(),
    ),
    url(
        r"^oec-cat/api/get-active-non-public-tests",
        retrieve_test_information_view.GetActiveNonPublicTests.as_view(),
    ),
    url(r"^oec-cat/api/assigned-tests", assigned_tests_view.AssignedTestsSet.as_view()),
    url(
        r"^oec-cat/api/update-test-time",
        test_administration_view.UpdateTestTime.as_view(),
    ),
    url(
        r"^oec-cat/api/approve-candidate",
        test_administration_view.ApproveCandidate.as_view(),
    ),
    url(
        r"^oec-cat/api/lock-candidate-test",
        test_administration_view.LockCandidateTest.as_view(),
    ),
    url(
        r"^oec-cat/api/lock-all-candidates-test",
        test_administration_view.LockAllCandidatesTest.as_view(),
    ),
    url(
        r"^oec-cat/api/unlock-candidate-test",
        test_administration_view.UnlockCandidateTest.as_view(),
    ),
    url(
        r"^oec-cat/api/unlock-all-candidates-test",
        test_administration_view.UnlockAllCandidatesTest.as_view(),
    ),
    url(
        r"^oec-cat/api/pause-candidate-test",
        test_administration_view.PauseCandidateTest.as_view(),
    ),
    url(
        r"^oec-cat/api/unpause-candidate-test",
        test_administration_view.UnpauseCandidateTest.as_view(),
    ),
    url(
        r"^oec-cat/api/re-sync-candidate",
        test_administration_view.ReSyncCandidate.as_view(),
    ),
    url(
        r"^oec-cat/api/un-assign-candidate",
        test_administration_view.UnAssignCandidate.as_view(),
    ),
    url(r"^oec-cat/api/quit-test", update_test_status.QuitTest.as_view()),
    url(
        r"^oec-cat/api/expire-test-inactivity",
        update_test_status.ExpireTestInactivity.as_view(),
    ),
    url(r"^oec-cat/api/timeout-test", update_test_status.SubmitTestTimeout.as_view()),
    url(
        r"^oec-cat/api/update-test-status",
        update_test_status.UpdateTestStatus.as_view(),
    ),
    url(r"^oec-cat/api/update-time-spent", time_spent_view.UpdateTimeSpent.as_view()),
    url(r"^oec-cat/api/get-permissions", permissions.GetPermissions.as_view()),
    url(
        r"^oec-cat/api/send-permission-request",
        permissions.SendPermissionRequest.as_view(),
    ),
    url(
        r"^oec-cat/api/get-pending-permissions",
        permissions.GetPendingPermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/get-user-pending-permissions",
        permissions.GetUserPendingPermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/get-user-accommodations",
        user_accommodations_view.GetUserAccommodations.as_view(),
    ),
    url(
        r"^oec-cat/api/save-user-accommodations",
        user_accommodations_view.SaveUserAccommodations.as_view(),
    ),
    url(r"^oec-cat/api/grant-permission", permissions.GrantPermission.as_view()),
    url(r"^oec-cat/api/deny-permission", permissions.DenyPermission.as_view()),
    url(
        r"^oec-cat/api/get-active-permissions",
        permissions.GetActivePermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/get-found-active-permissions",
        permissions.GetFoundActivePermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/update-active-permission",
        permissions.UpdateActivePermissionData.as_view(),
    ),
    url(
        r"^oec-cat/api/delete-active-permission",
        permissions.DeleteActivePermission.as_view(),
    ),
    url(r"^oec-cat/api/get-user-permissions", permissions.GetUserPermissions.as_view()),
    url(
        r"^oec-cat/api/get-available-permissions",
        permissions.GetAvailablePermissionsForRequest.as_view(),
    ),
    url(
        r"^oec-cat/api/get-users-based-on-specified-permission",
        permissions.GetUsersBasedOnSpecifiedPermission.as_view(),
    ),
    url(
        r"^oec-cat/api/grant-test-permission",
        test_permissions.GrantTestPermission.as_view(),
    ),
    url(
        r"^oec-cat/api/get-test-permissions",
        test_permissions.GetTestPermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/get-test-permission-financial-data",
        test_permissions.GetTestPermissionFinancialData.as_view(),
    ),
    url(
        r"^oec-cat/api/get-all-active-test-permissions",
        test_permissions.GetAllActiveTestPermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/get-found-active-test-permissions",
        test_permissions.GetFoundActiveTestPermissions.as_view(),
    ),
    url(
        r"^oec-cat/api/delete-test-permission",
        test_permissions.DeleteTestPermission.as_view(),
    ),
    url(
        r"^oec-cat/api/update-test-permission",
        test_permissions.UpdateTestPermission.as_view(),
    ),
    url(
        r"^oec-cat/api/get-new-test-access-code",
        test_access_code_view.GetNewTestAccessCode.as_view(),
    ),
    url(
        r"^oec-cat/api/delete-test-access-code",
        test_access_code_view.DeleteTestAccessCode.as_view(),
    ),
    url(
        r"^oec-cat/api/get-active-test-access-codes",
        test_access_code_view.GetActiveTestAccessCodes.as_view(),
    ),
    url(r"^oec-cat/api/room-check-in", room_check_in_view.RoomCheckIn.as_view()),
    url(
        r"^oec-cat/api/get-test-administrator-assigned-candidates",
        test_administration_view.GetTestAdministratorAssignedCandidates.as_view(),
    ),
    url(r"^oec-cat/api/update_notepad", notepad_view.SaveNotepad.as_view()),
    url(r"^oec-cat/api/get_notepad", notepad_view.GetNotepad.as_view()),
    url(r"^oec-cat/api/save-score", test_scorer_answer_view.SaveScore.as_view()),
    url(r"^oec-cat/api/get-scores", test_scorer_answer_view.GetScores.as_view()),
    # CMS
    url(
        r"^oec-cat/api/get-test-definition-labels",
        get_test_definitions_view.GetTestDefinitionsLabelsView.as_view(),
    ),
    url(
        r"^oec-cat/api/get-test-definition-versions-collected",
        get_test_definitions_view.GetTestDefinitionsVersionsCollectedView.as_view(),
    ),
    url(
        r"^oec-cat/api/get-test-section",
        test_section_data_view.TestSectionData.as_view(),
    ),
    url(
        r"^oec-cat/api/get-updated-time-after-lock-pause-actions",
        test_section_data_view.GetUpdatedTimeAfterLockPause.as_view(),
    ),
    url(r"^oec-cat/api/get-test-extract", test_builder_view.ExtractTestView.as_view()),
    url(
        r"^oec-cat/api/test-definition", test_builder_view.TestDefinitionView.as_view()
    ),
    url(
        r"^oec-cat/api/upload-test", test_builder_view.TestBuilderUploaderView.as_view()
    ),
    url(
        r"^oec-cat/api/try-test-section", test_builder_view.TryTestSectionView.as_view()
    ),
    url(
        r"^oec-cat/api/get_scorer_unassigned_tests",
        test_scorer_assignment_view.GetScorerUnassignedTests.as_view(),
    ),
    url(
        r"^oec-cat/api/get_scorer_assigned_tests",
        test_scorer_assignment_view.GetScorerAssignedTests.as_view(),
    ),
    url(r"^oec-cat/api/get-tics-data", tics_view.GetTicsData.as_view()),
    url(
        r"^oec-cat/api/assign_test_to_scorer",
        test_scorer_assignment_view.AssignTestToScorer.as_view(),
    ),
    url(r"^oec-cat/api/public-tests", public_tests.PublicTests.as_view()),
    # OIT
    url(
        r"^oec-cat/api/uit-seen-question",
        uit_assigned_tests_view.UitSeenQuestion.as_view(),
    ),
    url(
        r"^oec-cat/api/uit-save-answer", uit_assigned_tests_view.UitSaveAnswer.as_view()
    ),
    url(
        r"^oec-cat/api/get-test-answers",
        uit_assigned_tests_view.GetTestAnswers.as_view(),
    ),
    url(
        r"^oec-cat/api/generate-uit-test-access-code",
        psrs_generate_test_access_code_simulation.GetNewUitTestAccessCode.as_view(),
    ),
    url(
        r"^oec-cat/api/delete-uit-test-access-code",
        psrs_generate_test_access_code_simulation.DeleteUitTestAccessCode.as_view(),
    ),
    url(
        r"^oec-cat/api/uit-assigned-tests",
        assigned_tests_view.UitAssignedTestsSet.as_view(),
    ),
    url(r"^oec-cat/api/uit-score-test", uit_scoring_view.UitScoreTest.as_view()),
    url(
        r"^oec-cat/api/uit-test-section-scoring",
        uit_scoring_view.UitScoreTestSection.as_view(),
    ),
    # New eMIB style
    url(
        r"^oec-cat/api/update-email-answer",
        update_email_answer_view.UpdateEmailAnswer.as_view(),
    ),
    # protected files
    url(r"^oec-cat/api/get-file", get_file_view.GetFile.as_view()),
    # scorer
    url(
        r"^oec-cat/api/get-scorer-test-section",
        scorer_test_section_view.ScorerTestSection.as_view(),
    ),
    # reports
    url(
        r"^oec-cat/api/get-ta-assigned-test-order-numbers",
        reports_data_view.GetTaAssignedTestOrderNumbers.as_view(),
    ),
    url(
        r"^oec-cat/api/get-all-existing-test-order-numbers",
        reports_data_view.GetAllExistingTestOrderNumbers.as_view(),
    ),
    url(
        r"^oec-cat/api/get-tests-based-on-test-order-number",
        reports_data_view.GetTestsBasedOnTestOrderNumber.as_view(),
    ),
    url(
        r"^oec-cat/api/get-results-report-data",
        reports_data_view.GetResultsReportData.as_view(),
    ),
    url(
        r"^oec-cat/api/get-financial-report-data",
        reports_data_view.GetFinancialReportData.as_view(),
    ),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [url(r"^__debug__/", include(debug_toolbar.urls))]
