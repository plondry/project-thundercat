from .base import *
import json

file = os.environ.get("DATABASE_FILE", None)
if not file:
    print('Environment variable "DATABASE_FILE" is invalid or unprovided\n')


# Opening JSON file
f = open(file)

# returns JSON object as
# a dictionary
file_data = json.load(f)

# Closing file
f.close()

DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "sql_server.pyodbc",
        "NAME": file_data.get("DATABASE_NAME", ""),
        "USER": file_data.get("DATABASE_USER", ""),
        "PASSWORD": file_data.get("DATABASE_PASSWORD", ""),
        "HOST": file_data.get("DATABASE_HOST", ""),
        "PORT": file_data.get("DATABASE_PORT", ""),
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},  # odbc driver installed
    }
}

STATIC_URL = "/static_backend/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

ALLOWED_HOSTS = ["*"]

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")

# TODO: put <needed environment> oauth provider credentials here
OAUTH_PROVIDER_CREDENTIALS = ""
