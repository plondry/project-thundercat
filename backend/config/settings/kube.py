from .base import *


DEBUG = False

DATABASES = {
    "default": {
        "ENGINE": "sql_server.pyodbc",
        "NAME": os.environ.get("DATABASE_NAME", ""),
        "USER": os.environ.get("DATABASE_USER", ""),
        "PASSWORD": os.environ.get("DATABASE_PASSWORD", ""),
        "HOST": os.environ.get("DATABASE_HOST", ""),
        "PORT": os.environ.get("DATABASE_PORT", ""),
        "OPTIONS": {"driver": "ODBC Driver 17 for SQL Server"},  # odbc driver installed
    }
}

STATIC_URL = "/static_backend/"
STATIC_ROOT = os.path.join(BASE_DIR, "static")

ALLOWED_HOSTS = ["*"]

MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(BASE_DIR, "media/")

# TODO: put <needed environment> oauth provider credentials here
OAUTH_PROVIDER_CREDENTIALS = ""
