from django.conf.urls import url
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator
from backend.channels.consumers import TestSessionConsumer

application = ProtocolTypeRouter(
    {
        "websocket": AllowedHostsOriginValidator(
            AuthMiddlewareStack(
                URLRouter(
                    [
                        url(
                            # note that if you update this path, you'll also need to update testSessionChannels.js file (CHANNELS_PATH object)
                            r"^oec-cat/api/test-administrator-room/(?P<jwt_token>[A-zÀ-ú0-9.-]+)/(?P<test_access_code>[a-zA-Z0-9]+)",
                            TestSessionConsumer,
                        )
                    ]
                )
            )
        )
    }
)
