from collections import OrderedDict
from django.test import TestCase
from django.utils.timezone import now
from backend.views.test_scorer_assignment_view import get_test_scorer_assignment
from backend.custom_models.test_scorer_assignment import TestScorerAssignment
from backend.static.assigned_test_status import AssignedTestStatus
from backend.custom_models.assigned_test import AssignedTest
from user_management.user_management_models.user_models import User


class RetrieveTestScorerAssignments(TestCase):
    # def test_scorer_assignment(self):
    #     # initialize the data
    #     cand1_username = "TESTING_BACKEND_EMAIL_can1@email.ca"
    #     cand2_username = "TESTING_BACKEND_EMAIL_can2@email.ca"
    #     scorer_username = "TESTING_BACKEND_EMAIL_scorer@email.ca"
    #     pizza_test = "emibPizzaTest"
    #     dob = "2020-02-10"

    #     datetime = now()
    #     date = datetime.date()
    #     time = datetime.time()

    #     cand1 = User(
    #         username=cand1_username,
    #         first_name="can 1",
    #         last_name="can 1",
    #         email=cand1_username,
    #         birth_date=dob,
    #     )
    #     cand1.save()

    #     cand2 = User(
    #         username=cand2_username,
    #         first_name="can 2",
    #         last_name="can 2",
    #         email=cand2_username,
    #         birth_date=dob,
    #     )
    #     cand2.save()

    #     scorer = User(
    #         username=scorer_username,
    #         first_name="scorer",
    #         last_name="scorer",
    #         email=scorer_username,
    #         birth_date=dob,
    #     )
    #     scorer.save()

    #     assigned_test1 = AssignedTest(
    #         status=AssignedTestStatus.ASSIGNED,
    #         test_id=pizza_test,
    #         username=cand1,
    #         submit_date=datetime,
    #         test_time=180,
    #     )
    #     assigned_test1.save()

    #     assigned_test2 = AssignedTest(
    #         status=AssignedTestStatus.ASSIGNED,
    #         test_id=pizza_test,
    #         username=cand2,
    #         submit_date=datetime,
    #         test_time=180,
    #     )
    #     assigned_test2.save()

    #     # check no assignments exist
    #     expected_unassigned_tests = []
    #     expected_assigned_tests = []

    #     self.check_responses(
    #         scorer_username, expected_unassigned_tests, expected_assigned_tests
    #     )

    #     # submit both tests, assign to none
    #     assigned_test1.status = AssignedTestStatus.SUBMITTED

    #     assignment1 = TestScorerAssignment(assigned_test=assigned_test1)
    #     assignment1.save()

    #     assignment2 = TestScorerAssignment(assigned_test=assigned_test2)
    #     assignment2.save()

    #     expected_unassigned_tests = [
    #         OrderedDict(
    #             [
    #                 ("id", 2),
    #                 ("assigned_test_id", assignment2.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", None),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #         OrderedDict(
    #             [
    #                 ("id", 1),
    #                 ("assigned_test_id", assignment1.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", None),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #     ]

    #     expected_assigned_tests = [
    #         OrderedDict(
    #             [
    #                 ("id", 1),
    #                 ("assigned_test_id", assignment1.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", None),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #         OrderedDict(
    #             [
    #                 ("id", 2),
    #                 ("assigned_test_id", assignment2.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", None),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #     ]
    #     # TODO delete the above, uncomment the below when ETTA assigns tests; also fix line in test_scorer_assignment_view
    #     # expected_assigned_tests = []

    #     self.check_responses(
    #         scorer_username, expected_unassigned_tests, expected_assigned_tests
    #     )

    #     # assign test 1 to scorer 1
    #     assignment1.scorer_username = scorer
    #     assignment1.save()

    #     expected_unassigned_tests = [
    #         OrderedDict(
    #             [
    #                 ("id", 2),
    #                 ("assigned_test_id", assignment2.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", None),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         )
    #     ]

    #     expected_assigned_tests = [
    #         OrderedDict(
    #             [
    #                 ("id", 2),
    #                 ("assigned_test_id", assignment2.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", None),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #         OrderedDict(
    #             [
    #                 ("id", 1),
    #                 ("assigned_test_id", assignment1.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", scorer_username),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #     ]
    #     # TODO delete the above, uncomment the below when ETTA assigns tests; also fix line in test_scorer_assignment_view
    #     # expected_assigned_tests = [
    #     #     OrderedDict([
    #     #         ("id", 2),
    #     #         ("assigned_test_id", 2),
    #     #         ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #     #         ("assigned_test_start_date", None),
    #     #         ("assigned_test_test_name", "emibPizzaTest"),
    #     #         ("assigned_test_en_name", "Pizza Test"),
    #     #         ("assigned_test_fr_name", "FR Pizza Test"),
    #     #         ("assigned_test_submit_date", date),
    #     #         ("assigned_test_submit_time", time),
    #     #         ("assigned_test_test_session_language", None),
    #     #         ("scorer_username", scorer_username),
    #     #         ("status", 0),
    #     #         ("score_date", None),
    #     #     ]),
    #     # ]

    #     self.check_responses(
    #         scorer_username, expected_unassigned_tests, expected_assigned_tests
    #     )

    #     # assign test 2 to scorer 1
    #     assignment2.scorer_username = scorer
    #     assignment2.save()

    #     expected_unassigned_tests = []

    #     expected_assigned_tests = [
    #         OrderedDict(
    #             [
    #                 ("id", 1),
    #                 ("assigned_test_id", assignment1.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", scorer_username),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #         OrderedDict(
    #             [
    #                 ("id", 2),
    #                 ("assigned_test_id", assignment2.assigned_test_id),
    #                 ("assigned_test_status", AssignedTestStatus.ASSIGNED),
    #                 ("assigned_test_start_date", None),
    #                 ("assigned_test_test_name", "emibPizzaTest"),
    #                 ("assigned_test_en_name", "Pizza Test"),
    #                 ("assigned_test_fr_name", "FR Pizza Test"),
    #                 ("assigned_test_submit_date", date),
    #                 ("assigned_test_submit_time", time),
    #                 ("assigned_test_test_session_language", None),
    #                 ("scorer_username", scorer_username),
    #                 ("status", 0),
    #                 ("score_date", None),
    #             ]
    #         ),
    #     ]

    #     self.check_responses(
    #         scorer_username, expected_unassigned_tests, expected_assigned_tests
    #     )

    #     # cleanup
    #     assignment1.delete()
    #     assignment2.delete()
    #     assigned_test1.delete()
    #     assigned_test2.delete()
    #     cand1.delete()
    #     cand2.delete()
    #     scorer.delete()

    def check_responses(
        self, scorer_username, expected_unassigned_tests, expected_assigned_tests
    ):
        unassigned_tests = get_test_scorer_assignment(None)
        assigned_tests = get_test_scorer_assignment(scorer_username)

        self.assertEqual(unassigned_tests, expected_unassigned_tests)
        self.assertEqual(assigned_tests, expected_assigned_tests)
