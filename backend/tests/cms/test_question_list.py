"""
It tests the queryset that is used in the view (database_check_view.py),
but also the model itself (database_check_model.py)
"""
from django.test import TestCase
import unittest

from cms.cms_models.new_question import NewQuestion
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.question_list_rule import QuestionListRule
from cms.cms_models.question_block_type import QuestionBlockType

from cms.static.test_section_component_type import TestSectionComponentType


class QuestionListMain(TestCase):
    def setUp(self):
        TestSectionComponent.objects.create(
            order=0,
            component_type=TestSectionComponentType.QUESTION_LIST,
            en_title="en title",
            fr_title="fr title",
        )

    def test_question_list_contains_2_questions(self):
        self.assertEqual(True, False)


# class DatabaseCheckViewTests(APITestCase):
#     client = APIClient()

#     @staticmethod
#     def create_database_entry(name=""):
#         if (name) != "":
#             DatabaseCheckModel.objects.create_database_entry(name=name)

#     def setUp(self):
#         self.create_database_entry(name="name1")


# class GetAllDatabaseEntriesTest(DatabaseCheckViewTests):
#     def test_get_all_users(self):
#         response = self.client.get("http://localhost:80/api/database-check/")

#         # SELECT * FROM backend_databasecheckmodel;
#         expected = DatabaseCheckModel.objects.all()
#         serialized = database_check_serializer.DatabaseCheckSerializer(
#             expected, many=True
#         )

#         self.assertEqual(response.data, serialized.data)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)


# class GetSpecificDataTest(DatabaseCheckViewTests):
#     def test_get_specific_user(self):
#         response = self.client.get("http://localhost:80/api/database-check/")

#         # SELECT name FROM backend_databasecheckmodel WHERE name='name1';
#         expected = DatabaseCheckModel.objects.values_list("name", flat=True).get(
#             name="name1"
#         )

#         self.assertEqual(expected, "name1")
#         self.assertEqual(response.status_code, status.HTTP_200_OK)

if __name__ == "__main__":
    unittest.main()
