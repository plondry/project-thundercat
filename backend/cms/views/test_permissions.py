from rest_framework.views import APIView
from rest_framework import permissions
from cms.views.grant_test_permissions import grant_test_permissions
from cms.views.get_test_permissions import (
    get_test_permissions,
    get_test_permission_financial_data,
    get_all_active_test_permissions,
    get_found_active_test_permissions,
)
from cms.views.delete_test_permission import delete_test_permission
from cms.views.update_test_permission import update_test_permission


# assigning test permission to user
class GrantTestPermission(APIView):
    def post(self, request):
        return grant_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting test permissions of a specific user
class GetTestPermissions(APIView):
    def get(self, request):
        return get_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting specific test permission's financial data
class GetTestPermissionFinancialData(APIView):
    def get(self, request):
        return get_test_permission_financial_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting all active test permissions
class GetAllActiveTestPermissions(APIView):
    def get(self, request):
        return get_all_active_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# getting found active test permissions
class GetFoundActiveTestPermissions(APIView):
    def get(self, request):
        return get_found_active_test_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# deleting test permission
class DeleteTestPermission(APIView):
    def post(self, request):
        return delete_test_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


# updating test permission
class UpdateTestPermission(APIView):
    def post(self, request):
        return update_test_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
