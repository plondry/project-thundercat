from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.test_section import TestSection
from cms.serializers.test_section_serializer import TestSectionSerializer
from backend.custom_models.assigned_test import AssignedTest
from backend.static.assigned_test_status import AssignedTestStatus
from backend.static.test_section_scoring_type import TestSectionScoringType


def retrieve_scorer_test_section_data(assigned_test_id):
    context = {"scorer": True, "assigned_test_id": assigned_test_id}
    # get the assigned test
    assigned_test = AssignedTest.objects.get(id=assigned_test_id)

    # check if the test is still active or assigned
    if assigned_test.status not in (AssignedTestStatus.SUBMITTED,):
        return Response(
            {"error": "assigned test is in {} state".format(assigned_test.status)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # only returns first section right now
    test_section = TestSection.objects.filter(
        test_definition_id=assigned_test.test_id,
        scoring_type=TestSectionScoringType.COMPETENCY,
    ).first()

    # serialize the data for return
    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["read_only"] = True

    return Response(serialized, status=status.HTTP_200_OK)
