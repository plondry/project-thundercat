from rest_framework.response import Response
from rest_framework import status
from user_management.user_management_models.user_models import User
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions
from cms.views.utils import is_undefined

# populating 'testpermissions' table based on the provided parameters
def grant_test_permissions(request):
    # making sure that we have the needed parameters
    usernames = request.query_params.get("usernames", None)
    test_ids = request.query_params.get("test_ids", None)
    expiry_date = request.query_params.get("expiry_date", None)
    test_order_number = request.query_params.get("test_order_number", None)
    staffing_process_number = request.query_params.get("staffing_process_number", None)
    department_ministry_code = request.query_params.get(
        "department_ministry_code", None
    )
    is_org = request.query_params.get("is_org", None)
    is_ref = request.query_params.get("is_ref", None)
    billing_contact = request.query_params.get("billing_contact", None)
    billing_contact_info = request.query_params.get("billing_contact_info", None)
    if is_undefined(usernames):
        return Response(
            {"error": "no 'usernames' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(test_ids):
        return Response(
            {"error": "no 'test_ids' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(expiry_date):
        return Response(
            {"error": "no 'expiry_date' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(test_order_number):
        return Response(
            {"error": "no 'test_order_number' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(staffing_process_number):
        return Response(
            {"error": "no 'staffing_process_number' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(department_ministry_code):
        return Response(
            {"error": "no 'department_ministry_code' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(is_org):
        return Response(
            {"error": "no 'is_org' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(is_ref):
        return Response(
            {"error": "no 'is_ref' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(billing_contact):
        return Response(
            {"error": "no 'billing_contact' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(billing_contact_info):
        return Response(
            {"error": "no 'billing_contact_info' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # creating usernames and test names arrays based on received parameters
    usernames_array = usernames.split(",")
    test_ids_array = test_ids.split(",")

    # creating a new array for pending test permissions
    pending_test_permissions = []

    # looping in usernames array
    for username in usernames_array:
        # looping in test_names array
        for test_id in test_ids_array:
            try:
                # checking if the current user has already a test permission with the same test order number and same test ID
                test_permission_already_exists = TestPermissions.objects.filter(
                    username=User.objects.get(username=username),
                    test_order_number=test_order_number,
                    test=TestDefinition.objects.get(id=test_id),
                )

                # there are no existing test permissions with the same parameters as the ones provided
                if not test_permission_already_exists:
                    # creating a new object without saving it yet
                    response = TestPermissions(
                        username=User.objects.get(username=username),
                        test=TestDefinition.objects.get(id=test_id),
                        expiry_date=expiry_date,
                        test_order_number=test_order_number,
                        staffing_process_number=staffing_process_number,
                        department_ministry_code=department_ministry_code,
                        is_org=is_org,
                        is_ref=is_ref,
                        billing_contact=billing_contact,
                        billing_contact_info=billing_contact_info,
                    )
                    # adding the new object created in the pending test permissions array
                    pending_test_permissions.append(response)

                # there are existing test permissions with the same parameters as the ones provided
                else:
                    # getting english/french test names
                    test = TestDefinition.objects.get(id=test_id)
                    # getting username's first and last name
                    first_name = User.objects.get(username=username).first_name
                    last_name = User.objects.get(username=username).last_name
                    # returning response (formatted error)
                    return Response(
                        {
                            "status": 409,
                            "error": "{} already has this {} test permission".format(
                                username, test_id
                            ),
                            "first_name": first_name,
                            "last_name": last_name,
                            "test_order_number": test_order_number,
                            "en_test_name": test.en_name,
                            "fr_test_name": test.fr_name,
                        },
                        status=status.HTTP_409_CONFLICT,
                    )
            except TestDefinition.DoesNotExist:
                return Response(
                    {"error": "the specified test does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
            except User.DoesNotExist:
                return Response(
                    {"error": "the specified username does not exist"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
    # if there is no error, loop in pending test permissions array and save all created objects
    for test_permission in pending_test_permissions:
        test_permission.save()
    return Response(status=status.HTTP_200_OK)
