from django.db import transaction, IntegrityError
from django.core.exceptions import FieldError
from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.file_resource_details import FileResourceDetails
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.page_section_type_tree_description import (
    PageSectionTypeTreeDescription,
)
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section import TestSection

from cms.cms_models.next_section_button_type_proceed import NextSectionButtonTypeProceed
from cms.cms_models.next_section_button_type_popup import NextSectionButtonTypePopup

from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.section_component_page import SectionComponentPage
from cms.cms_models.page_section import PageSection
from cms.cms_models.page_section_type_markdown import PageSectionTypeMarkdown
from cms.cms_models.page_section_type_image_zoom import PageSectionTypeImageZoom
from cms.cms_models.page_section_type_sample_email import PageSectionTypeSampleEmail
from cms.cms_models.page_section_type_sample_email_response import (
    PageSectionTypeSampleEmailResponse,
)
from cms.cms_models.page_section_type_sample_task_response import (
    PageSectionTypeSampleTaskResponse,
)
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown
from cms.cms_models.question_section_type_markdown_end import (
    QuestionSectionTypeMarkdownEnd,
)
from cms.cms_models.answer import Answer
from cms.cms_models.address_book_contact import AddressBookContact
from cms.cms_models.address_book_contact_details import AddressBookContactDetails
from cms.cms_models.email_question_details import EmailQuestionDetails
from cms.cms_models.answer_details import AnswerDetails
from cms.cms_models.question_block_type import QuestionBlockType
from cms.cms_models.competency_type import CompetencyType
from cms.cms_models.question_list_rule import QuestionListRule
from cms.cms_models.question_situation import QuestionSituation
from cms.cms_models.situation_example_rating import SituationExampleRating
from cms.cms_models.situation_example_rating_details import (
    SituationExampleRatingDetails,
)

from cms.static.next_section_button_type import NextSectionButtonType
from cms.static.page_section_type import PageSectionType
from cms.static.question_difficulty_type import QuestionDifficultyType
from cms.static.question_section_type import QuestionSectionType
from cms.static.question_type import QuestionType
from cms.serializers.test_section_serializer import TestSectionSerializer

from backend.custom_models.language import Language
from backend.views.utils import is_undefined


#  This function uploads a test from json API.
#  If the action is TRY then the test is not permanently stored
#  in the database, only so that the serializer can make a test section out of it
#  and then the database transaction is rolled back.
#
#  If the action is upload the data base transaction is attempted with no rollback.
class Action:
    TRY = 1
    UPLOAD = 2


def try_test_section(section_order_to_view, new_test, action):
    test_section_to_view = {}

    # error message is an array so the front end can .map it and print out different lines
    error_message = []

    # perform all the uploading actions in a db transaction
    # so if anything goes wrong we can roll it all back
    try:
        with transaction.atomic():
            # only raise key errors so that any other unexpected exceptions are caught
            # and reported to the console/log and not silenced by accident
            test_section_id_map = {}
            test_section_component_id_map = {}
            section_component_pages_id_map = {}
            page_sections_id_map = {}
            # using array because the id is a tuple of differnt objects and types
            page_section_definitions_id_map = []
            address_book_contact_id_map = {}
            contact_details_id_map = {}
            email_id_map = {}
            mc_id_map = {}
            question_id_map = {}
            email_details_id_map = {}
            answer_id_map = {}
            answer_details_id_map = {}
            question_section_id_map = {}
            question_section_definition_id_map = {}
            question_block_type_id_map = {}
            competency_type_id_map = {}
            question_situation_id_map = {}
            situation_example_id_map = {}

            # -- create test definition
            for test_definition in new_test["test_definition"]:
                try:
                    new_test_def = TestDefinition(
                        test_code=test_definition["test_code"],
                        version=test_definition["version"],
                        en_name=test_definition["en_name"],
                        fr_name=test_definition["fr_name"],
                        is_public=test_definition["is_public"],
                        active=test_definition["active"],
                    )
                    new_test_def.save()
                except IntegrityError:
                    error_message = [
                        "Matching test definition with the same version and test code.",
                        "Cannot override existing test definitions.",
                        "If you wish to do this, contact the IT team.",
                    ]
                    raise KeyError

            # ---- create test sections
            for test_section in new_test["test_sections"]:
                default_time = test_section["default_time"]
                if default_time == "":
                    default_time = None

                new_test_section = TestSection(
                    test_definition=new_test_def,
                    order=test_section["order"],
                    section_type=test_section["section_type"],
                    default_time=default_time,
                    en_title=test_section["en_title"],
                    fr_title=test_section["fr_title"],
                    next_section_button_type=test_section["next_section_button_type"],
                    scoring_type=test_section["scoring_type"],
                    default_tab=test_section["default_tab"],
                    uses_notepad=test_section["uses_notepad"],
                    block_cheating=test_section["block_cheating"],
                )
                new_test_section.save()
                # add test section to the dictionary map of local json ids to real (db) ids
                test_section_id_map[test_section["id"]] = new_test_section.id

            # ------ create test section buttons
            for next_section_button in new_test["next_section_buttons"]:
                create_next_section_button(
                    next_section_button, test_section_id_map, error_message
                )

            # ------ create test section components
            for test_section_component in new_test["test_section_components"]:
                # check if the component has already been created
                # this happens if a component is visible in two test sections
                if test_section_component["id"] in test_section_component_id_map.keys():
                    continue
                new_test_section_component = TestSectionComponent(
                    order=test_section_component["order"],
                    component_type=test_section_component["component_type"],
                    en_title=test_section_component["en_title"],
                    fr_title=test_section_component["fr_title"],
                    shuffle_all_questions=test_section_component.get(
                        "shuffle_all_questions", False
                    ),
                    language=Language.objects.get(
                        ISO_Code_1=test_section_component["language"]
                    ),
                )
                new_test_section_component.save()

                # add test section to the dictionary map of local json ids to real (db) ids
                test_section_component_id_map[
                    test_section_component["id"]
                ] = new_test_section_component.id

                for test_section_id in test_section_component["test_section"]:
                    # add the specific test section based on local id to the many to many relationship
                    new_test_section_component.test_section.add(
                        TestSection.objects.get(id=test_section_id_map[test_section_id])
                    )

            # -------- create question blocks
            if new_test.get("competency_types", None):
                for comp_type in new_test["competency_types"]:
                    new_comp_type = CompetencyType(
                        en_name=comp_type["en_name"],
                        fr_name=comp_type["fr_name"],
                        max_score=comp_type["max_score"],
                        test_definition=new_test_def,
                    )
                    new_comp_type.save()
                    competency_type_id_map[comp_type.get("id")] = new_comp_type.id

            # -------- create question blocks
            if new_test.get("question_block_types", None):
                for block_type in new_test["question_block_types"]:
                    new_block_type = QuestionBlockType(
                        name=block_type["name"], test_definition=new_test_def
                    )
                    new_block_type.save()
                    question_block_type_id_map[block_type.get("id")] = new_block_type.id

            # -------- create question list rules
            if new_test.get("question_list_rules", None):
                for rule in new_test["question_list_rules"]:
                    new_rule = QuestionListRule(
                        number_of_questions=rule.get("number_of_questions", 0),
                        test_section_component=TestSectionComponent.objects.get(
                            id=test_section_component_id_map[
                                rule.get("test_section_component", "")
                            ]
                        ),
                        shuffle=rule.get("shuffle"),
                    )
                    new_rule.save()
                    for block_type in rule["question_block_type"]:
                        new_rule.question_block_type.add(
                            QuestionBlockType.objects.get(
                                id=question_block_type_id_map[block_type]
                            )
                        )

            # -------- create section component pages
            for section_component_pages in new_test["section_component_pages"]:
                new_test_section_component = TestSectionComponent.objects.get(
                    id=test_section_component_id_map[
                        section_component_pages["test_section_component"]
                    ]
                )
                new_section_component_page = SectionComponentPage(
                    order=section_component_pages["order"],
                    en_title=section_component_pages["en_title"],
                    fr_title=section_component_pages["fr_title"],
                    test_section_component=new_test_section_component,
                )
                new_section_component_page.save()
                section_component_pages_id_map[
                    section_component_pages["id"]
                ] = new_section_component_page.id

            # ---------- create page sections
            for page_section in new_test["page_sections"]:
                new_section_component_page = SectionComponentPage.objects.get(
                    id=section_component_pages_id_map[
                        page_section["section_component_page"]
                    ]
                )
                new_page_section = PageSection(
                    order=page_section["order"],
                    page_section_type=page_section["page_section_type"],
                    section_component_page=new_section_component_page,
                )
                new_page_section.save()
                page_sections_id_map[page_section["id"]] = new_page_section.id

            # -------- create address book contacts
            for contact in new_test["address_book"]:
                if contact["id"] not in address_book_contact_id_map:

                    new_address_book_contact = AddressBookContact(name=contact["name"])
                    new_address_book_contact.save()
                    address_book_contact_id_map[
                        contact["id"]
                    ] = new_address_book_contact.id

            # ---------- create address book contact details
            for contact_details in new_test["contact_details"]:
                contact = AddressBookContact.objects.get(
                    id=address_book_contact_id_map[contact_details["contact"]]
                )
                new_contact_details = AddressBookContactDetails(
                    title=contact_details["title"],
                    language=Language.objects.get(
                        ISO_Code_1=contact_details["language"]
                    ),
                    contact=contact,
                )
                new_contact_details.save()

                contact_details_id_map[contact_details["id"]] = new_contact_details.id

            # -------- create contact relationship (test section component + parent)
            for address_book_contact in new_test["address_book"]:
                try:
                    # find the real object based off the local id stored earlier when creating
                    contact_obj = AddressBookContact.objects.get(
                        id=address_book_contact_id_map[address_book_contact["id"]]
                    )

                    # add the parent obj
                    if not is_undefined(address_book_contact["parent"]):
                        parent_obj = AddressBookContact.objects.get(
                            id=address_book_contact_id_map[
                                address_book_contact["parent"]
                            ]
                        )
                        contact_obj.parent = parent_obj
                        contact_obj.save()
                except KeyError:
                    error_message = [
                        "Could not find an address book contact.",
                        "Did you delete a contact from the address book and forget to remove them from Tree Descriptions or emails questions?",
                    ]
                    raise KeyError

                # add the related test section components
                for test_section_id in address_book_contact["test_section"]:
                    # add the specific test section based on local id to the many to many relationship
                    if test_section_id in test_section_id_map:
                        contact_obj.test_section.add(
                            TestSection.objects.get(
                                id=test_section_id_map[test_section_id]
                            )
                        )
            # ------------ create page section types
            for section in new_test["page_section_definitions"]:
                # check if unique tuple has already been created
                # done to prevent componets in multiple sections from
                # creating duplicate children
                if [
                    section["id"],
                    section["page_section_type"],
                ] in page_section_definitions_id_map:
                    continue
                new_page_section = PageSection.objects.get(
                    id=page_sections_id_map[section["page_section"]]
                )
                create_page_section_types(
                    new_page_section,
                    section,
                    address_book_contact_id_map,
                    new_test_def,
                    page_section_definitions_id_map,
                    error_message,
                )

            # -------- create questions
            for question in new_test["questions"]:
                if question["test_section_component"] in test_section_component_id_map:
                    block_type = None
                    try:
                        if (
                            question.get("question_block_type", "") != ""
                            and question_block_type_id_map
                        ):
                            block_type = QuestionBlockType.objects.get(
                                id=question_block_type_id_map[
                                    question.get("question_block_type")
                                ]
                            )
                    except KeyError:
                        pass
                    test_section_component = TestSectionComponent.objects.get(
                        id=test_section_component_id_map[
                            question["test_section_component"]
                        ]
                    )
                    new_question = NewQuestion(
                        test_section_component=test_section_component,
                        question_type=question["question_type"],
                        pilot=question["pilot"],
                        question_block_type=block_type,
                        order=question.get("order", 0),
                    )
                    new_question.save()
                    question_id_map[question["id"]] = new_question.id

            # ------- create question situation
            if new_test.get("question_situations", False):
                for situation in new_test["question_situations"]:
                    new_question = NewQuestion.objects.get(
                        id=question_id_map[situation["question"]]
                    )
                    language = Language.objects.get(ISO_Code_1=situation["language"])

                    new_situation_details = QuestionSituation(
                        question=new_question,
                        language=language,
                        situation=situation["situation"],
                    )
                    new_situation_details.save()

            # ------- create situation example ratings
            if new_test.get("situation_example_ratings", False):
                for example in new_test["situation_example_ratings"]:
                    question = NewQuestion.objects.get(
                        id=question_id_map[example["question"]]
                    )
                    competency_type = CompetencyType(
                        id=competency_type_id_map[example["competency_type"]]
                    )
                    new_example = SituationExampleRating(
                        score=example["score"],
                        question=question,
                        competency_type=competency_type,
                    )
                    new_example.save()
                    situation_example_id_map[example["id"]] = new_example.id

            # ------- create situation example ratings
            if new_test.get("situation_example_rating_details", False):
                for details in new_test["situation_example_rating_details"]:
                    example_rating = SituationExampleRating.objects.get(
                        id=situation_example_id_map[details["example_rating"]]
                    )
                    language = Language.objects.get(ISO_Code_1=details["language"])
                    new_details = SituationExampleRatingDetails(
                        language=language,
                        example_rating=example_rating,
                        example=details["example"],
                    )
                    new_details.save()

            # ------- create question dependencies
            for question in new_test["questions"]:
                new_question = NewQuestion.objects.get(
                    id=question_id_map[question["id"]]
                )
                if question.get("dependencies", None):
                    for dependent_id in question["dependencies"]:
                        new_question.dependencies.add(
                            NewQuestion.objects.get(id=question_id_map[dependent_id])
                        )

            # ---------- create email questions
            for email in new_test["emails"]:
                if email["question"] in question_id_map:
                    try:
                        question = NewQuestion.objects.get(
                            id=question_id_map[email["question"]]
                        )
                        new_email = EmailQuestion(
                            question=question,
                            email_id=email["email_id"],
                            from_field=AddressBookContact.objects.get(
                                id=address_book_contact_id_map[email.get("from_field")]
                            ),
                        )

                        new_email.save()

                        for contact_id in email["to_field"]:
                            new_email.to_field.add(
                                AddressBookContact.objects.get(
                                    id=address_book_contact_id_map[contact_id]
                                )
                            )
                        for contact_id in email["cc_field"]:
                            new_email.cc_field.add(
                                AddressBookContact.objects.get(
                                    id=address_book_contact_id_map[contact_id]
                                )
                            )
                        for competency in email["competency_types"]:
                            new_email.competency_types.add(
                                CompetencyType.objects.get(
                                    id=competency_type_id_map[competency]
                                )
                            )
                        # competency_type_id_map
                        email_id_map[email["id"]] = new_email.id
                    except KeyError:
                        error_message = [
                            "Could not create email question.",
                            "Make sure that you have fully completed all details on the component.",
                        ]
                        raise KeyError

            # ---------- create email questions
            for email_detail in new_test["email_details"]:
                if email_detail["email_question"] in email_id_map:
                    email_question = EmailQuestion.objects.get(
                        id=email_id_map[email_detail["email_question"]]
                    )
                    new_email_details = EmailQuestionDetails(
                        email_question=email_question,
                        language=Language.objects.get(
                            ISO_Code_1=email_detail["language"]
                        ),
                        subject_field=email_detail["subject_field"],
                        date_field=email_detail["date_field"],
                        body=email_detail["body"],
                    )
                    new_email_details.save()

                    email_details_id_map[email_detail["id"]] = new_email_details.id

            # ---------- create mc questions
            for mc in new_test["multiple_choice_question_details"]:
                question = NewQuestion.objects.get(id=question_id_map[mc["question"]])
                new_multiple_choice = MultipleChoiceQuestion(
                    question=question,
                    question_difficulty_type=mc["question_difficulty_type"],
                )
                new_multiple_choice.save()
                mc_id_map[mc["id"]] = new_multiple_choice.id

            # ---------- create question answers
            for answer in new_test["answers"]:
                question = NewQuestion.objects.get(
                    id=question_id_map[answer["question"]]
                )
                new_answer = Answer(
                    question=question, scoring_value=answer["scoring_value"]
                )
                new_answer.save()
                answer_id_map[answer["id"]] = new_answer.id

            # ---------- create answer details
            for answer_detail in new_test["answer_details"]:
                answer = Answer.objects.get(id=answer_id_map[answer_detail["answer"]])
                create_answer_details(
                    answer_detail, answer, answer_details_id_map, error_message
                )

            # ---------- create question sections
            for question_section in new_test["question_sections"]:
                new_question = NewQuestion.objects.get(
                    id=question_id_map[question_section["question"]]
                )
                new_question_section = QuestionSection(
                    order=question_section["order"],
                    question=new_question,
                    question_section_type=question_section["question_section_type"],
                )
                new_question_section.save()
                question_section_id_map[
                    question_section["id"]
                ] = new_question_section.id

            # ---------- create question sections definitions
            for section_details in new_test["question_section_definitions"]:
                new_question_section = QuestionSection.objects.get(
                    id=question_section_id_map[section_details["question_section"]]
                )
                create_question_section(
                    new_question_section,
                    section_details,
                    question_section_definition_id_map,
                    error_message,
                )

            if action == Action.TRY:
                test_section_to_view = serialize_test_section(
                    section_order_to_view, new_test_def
                )

                # uncomment this and the test will not be uploaded, good for testing
                raise FieldError

            return Response(
                {"message": "successfully uploaded new test"}, status=status.HTTP_200_OK
            )

    except FieldError:
        # raising an exception so the test definition is not persisted.
        return Response(test_section_to_view, status=status.HTTP_200_OK)

    # catching all exceptions so we can return an error to the frontend.
    # specific exceptions are caught within the transaction.
    except KeyError as error:
        return Response(
            {"error": str(error), "message": error_message},
            status=status.HTTP_400_BAD_REQUEST,
        )


def serialize_test_section(order, test_definition):
    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=order
    )
    serialized = TestSectionSerializer(test_section, many=False).data
    serialized["start_time"] = ""
    return serialized


def create_next_section_button(next_section_button, test_section_id_map, error_message):
    new_test_section = TestSection.objects.get(
        id=test_section_id_map[next_section_button["test_section"]]
    )
    button_type = new_test_section.next_section_button_type
    language = Language.objects.get(ISO_Code_1=next_section_button["language"])
    if button_type == NextSectionButtonType.PROCEED:
        new_button = NextSectionButtonTypeProceed(
            test_section=new_test_section,
            language=language,
            button_text=next_section_button["button_text"],
        )

        new_button.save()

    elif button_type == NextSectionButtonType.POPUP:
        new_button = NextSectionButtonTypePopup(
            test_section=new_test_section,
            language=language,
            content=next_section_button["content"],
            title=next_section_button["title"],
            button_text=next_section_button["button_text"],
            confirm_proceed=next_section_button.get("confirm_proceed", False),
        )
        new_button.save()


def create_page_section_types(
    new_page_section,
    section,
    address_book_contact_id_map,
    new_test_definition,
    page_section_definitions_id_map,
    error_message,
):

    section_type = new_page_section.page_section_type
    language = Language.objects.get(ISO_Code_1=section["language"])

    if section_type == PageSectionType.MARKDOWN:
        new_section = PageSectionTypeMarkdown(
            page_section=new_page_section, language=language, content=section["content"]
        )
        new_section.save()

    elif section_type == PageSectionType.ZOOM_IMAGE:
        try:
            new_section = PageSectionTypeImageZoom(
                page_section=new_page_section,
                language=language,
                large_image=section["large_image"],
                small_image=section["small_image"],
            )
            new_section.save()
        except KeyError:
            error_message = ["Zoom Image page section is incomplete."]
            raise KeyError
        # create a file resource object to manage access to the file
        # NOTE right now we only use one image for both small and large here
        # this is due to reacts implementation. Consider modifying the data
        # model to reflect that.
        new_file_resource = FileResourceDetails(
            path=new_section.large_image, test_definition=new_test_definition
        )
        new_file_resource.save()

    elif section_type == PageSectionType.SAMPLE_EMAIL:
        new_section = PageSectionTypeSampleEmail(
            page_section=new_page_section,
            language=language,
            email_id=section["email_id"],
            subject_field=section.get("subject_field", ""),
            from_field=section.get("from_field", ""),
            to_field=section.get("to_field", ""),
            date_field=section.get("date_field", ""),
            body=section.get("body", ""),
        )
        new_section.save()

    elif section_type == PageSectionType.SAMPLE_EMAIL_RESPONSE:

        new_section = PageSectionTypeSampleEmailResponse(
            page_section=new_page_section,
            language=language,
            to_field=section["to_field"],
            cc_field=section["cc_field"],
            response=section["response"],
            reason=section["reason"],
        )
        new_section.save()

    elif section_type == PageSectionType.SAMPLE_TASK_RESPONSE:
        new_section = PageSectionTypeSampleTaskResponse(
            page_section=new_page_section,
            language=language,
            response=section["response"],
            reason=section["reason"],
        )
        new_section.save()

    elif section_type == PageSectionType.TREE_DESCRIPTION:
        new_section = PageSectionTypeTreeDescription(
            page_section=new_page_section,
            language=language,
            address_book_contact=AddressBookContact.objects.get(
                id=address_book_contact_id_map[section["address_book_contact"]]
            ),
        )
        new_section.save()

    elif section_type == PageSectionType.PRIVACY_NOTICE:
        # currently no data to save for a privacy notice section
        return
    page_section_definitions_id_map.append(
        [section["id"], section["page_section_type"]]
    )


def create_answer_details(answer_details, answer, answer_details_id_map, error_message):
    language = Language.objects.get(ISO_Code_1=answer_details["language"])
    new_answer = ""
    new_answer = AnswerDetails(
        answer=answer, language=language, content=answer_details["content"]
    )
    new_answer.save()
    answer_details_id_map[answer_details["id"]] = new_answer.id
    return


def create_question_section(
    new_question_section,
    section_details,
    question_section_definition_id_map,
    error_message,
):
    section_type = new_question_section.question_section_type
    language = Language.objects.get(ISO_Code_1=section_details["language"])
    new_section = ""
    if section_type == QuestionSectionType.MARKDOWN:
        new_section = QuestionSectionTypeMarkdown(
            question_section=new_question_section,
            language=language,
            content=section_details["content"],
        )
    elif section_type == QuestionSectionType.END:
        new_section = QuestionSectionTypeMarkdownEnd(
            question_section=new_question_section,
            language=language,
            content=section_details["content"],
        )

    new_section.save()
    question_section_definition_id_map[section_details["id"]] = new_section.id
    return
