from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.renderers import JSONRenderer
from cms.views.try_test_section import try_test_section, Action
from cms.views.extract_test import extract_test
from backend.views.utils import is_undefined
from backend.api_permissions.role_based_api_permissions import HasSystemAdminPermission
from cms.views.utils import get_needed_parameters
from cms.cms_models.test_definition import TestDefinition

# This is for uploading an entirely new test
# currently it does not handle versioning


# URL: upload-test
class TestBuilderUploaderView(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(["new_test"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return try_test_section(0, parameters["new_test"], Action.UPLOAD)

    def get_permissions(self):
        try:
            return [HasSystemAdminPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# URL: try-test-section
class TryTestSectionView(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(["new_test", "order"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return try_test_section(parameters["order"], parameters["new_test"], Action.TRY)

    def get_permissions(self):
        try:
            return [HasSystemAdminPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# URL: get-test-extract
class ExtractTestView(APIView):
    renderer_classes = [JSONRenderer]

    def get(self, request):
        success, parameters = get_needed_parameters(["test_definition"], request)

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        return extract_test(parameters["test_definition"])

    def get_permissions(self):
        try:
            return [HasSystemAdminPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# URL: oec-cat/api/test-definition
class TestDefinitionView(APIView):
    def post(self, request):
        success, parameters = get_needed_parameters(
            ["test_definition", "active"], request
        )

        if not success:
            return Response(parameters, status=status.HTTP_400_BAD_REQUEST)

        test = TestDefinition.objects.filter(id=parameters["test_definition"]).first()

        if not test:
            return Response(
                {"message": "could not find test definition"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        test.active = parameters["active"]
        test.save()

        return Response({"message": "success"}, status=status.HTTP_200_OK)

    def get_permissions(self):
        try:
            return [HasSystemAdminPermission()]

        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]
