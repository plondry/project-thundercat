import jwt
from django.conf import settings

# function that verifies if the specified parameter is undefined
def is_undefined(value):
    if value is None:
        return True
    if value == "undefined":
        return True
    if value == "null":
        return True
    return False


# function that is getting the user information from decrypted provided auth token (JWT token)
def get_user_info_from_jwt_token(request):
    # getting complete auth token
    complete_auth_token = request.headers["Authorization"]
    # removing "JWT" from token
    auth_token = complete_auth_token[4:]
    # decrypting jwt token to get user's information
    user_info = jwt.decode(auth_token, settings.SECRET_KEY, algorithms=["HS256"])
    # returning user info object
    return user_info


# function that takes an array of strings in the form of (for example)
# ["assigned_test_id", "test_section_id", ...] and a request object
# and returns two variables => success(boolean) and
# the parameters dictionary or the error response dictionary
# NOTE: GET requests must have data in query param and POST in body
def get_needed_parameters(needed_parameters, request):
    parameters = {}
    for parameter in needed_parameters:
        parameter_name = "{}".format(parameter)
        # try query params first
        parameter = request.query_params.get(parameter_name, None)
        # if not in query params try request body
        if not parameter:
            parameter = request.data.get(parameter_name, None)
        if is_undefined(parameter):
            return False, {"error": "no '{}' parameter".format(parameter_name)}

        else:
            parameters[parameter_name] = parameter
    return True, parameters
