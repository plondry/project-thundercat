from datetime import timedelta
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.test_section import TestSection
from cms.serializers.test_section_serializer import TestSectionSerializer
from backend.custom_models.assigned_test_section import AssignedTestSection
from backend.custom_models.assigned_test import AssignedTest
from backend.custom_models.assigned_test_section_access_times import (
    AssignedTestSectionAccessTimes,
)
from backend.custom_models.ta_actions import TaActions
from backend.custom_models.lock_test_actions import LockTestActions
from backend.custom_models.pause_test_actions import PauseTestActions
from backend.views.utils import is_undefined
from backend.views.test_administrator_actions import TaActionsConstants

from backend.static.assigned_test_section_access_time_type import (
    AssignedTestSectionAccessTimeType,
)
from backend.static.assigned_test_status import AssignedTestStatus
from cms.static.test_section_type import TestSectionType
from backend.views.uit_score_test import uit_scoring, Action


class SpecialSection:
    QUIT = "quit"


def is_test_public(test_definition):
    # function that returns true if the test is public and false on the other hand
    # return Test.objects.get(test_name=test_name).is_public
    return True


def retrieve_test_section_data(order, assigned_test_id, quit_section=False):
    context = {}
    # get the assigned test
    assigned_test = AssignedTest.objects.get(id=assigned_test_id)
    parameters = {"assigned_test_id": assigned_test.id}

    # check if the test is still active or assigned
    if assigned_test.status not in (
        AssignedTestStatus.ACTIVE,
        AssignedTestStatus.READY,
        AssignedTestStatus.LOCKED,
        AssignedTestStatus.PAUSED,
    ):
        return Response(
            {"error": "assigned test is in {} state".format(assigned_test.status)},
            status=status.HTTP_400_BAD_REQUEST,
        )

    # updating assigned test start date (only if null)
    if assigned_test.start_date is None:
        assigned_test.start_date = timezone.now()
        assigned_test.save()

    ###
    # finish current test section
    ###
    if order != "0":

        # get the current test section
        test_section = TestSection.objects.get(
            test_definition_id=assigned_test.test_id, order=order
        )
        # get the assigned test section for the above
        assigned_test_section = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test.id, test_section=test_section
        )

        # create a "finish" time stamp for the current test section
        access_time = AssignedTestSectionAccessTimes(
            assigned_test_section=assigned_test_section,
            time_type=AssignedTestSectionAccessTimeType.FINISH,
        )
        access_time.save()

        # score the current test section (adds together in final scoring)
        parameters["test_section_id"] = test_section.id
        uit_scoring(parameters, Action.TEST_SECTION)
    # change the test status to active (first time accessing) + removing previous_state (if it has one)
    else:
        assigned_test.status = AssignedTestStatus.ACTIVE
        assigned_test.previous_status = None
        assigned_test.save()

    ###
    # start new test section now
    ###
    next_test_section_order = ""

    if quit_section:
        # get the quit section of the test
        test_section = TestSection.objects.get(
            test_definition_id=assigned_test.test_id, section_type=TestSectionType.QUIT
        )

        next_test_section_order = str(test_section.order)

        # set the test to quit + update test section id
        assigned_test.status = AssignedTestStatus.QUIT
        assigned_test.test_section_id = test_section.id
        assigned_test.save()
    else:
        next_section_invalid = True
        # get a valid test section order number
        while next_section_invalid:
            next_test_section_order = str(int(order) + 1)
            # find the new test section
            test_section = TestSection.objects.get(
                test_definition_id=assigned_test.test_id, order=next_test_section_order
            )

            assigned_test_section = AssignedTestSection.objects.get(
                assigned_test_id=assigned_test.id, test_section=test_section
            )

            # update test section id
            assigned_test.test_section_id = test_section.id
            assigned_test.save()

            # if the next section is not timed, we can return this section
            if is_undefined(test_section.default_time):
                break

            # if the next test section is timed and already completed, get another one
            if (
                AssignedTestSectionAccessTimes.objects.filter(
                    assigned_test_section=assigned_test_section,
                    time_type=AssignedTestSectionAccessTimeType.FINISH,
                ).count()
                == 0
            ):
                break
            else:
                order = int(order) + 1

    # check if the new test section is special (a finish section) and change test status
    if test_section.section_type == TestSectionType.FINISH:
        # set the test to submitted
        assigned_test.status = AssignedTestStatus.SUBMITTED
        assigned_test.submit_date = timezone.now()
        assigned_test.save()
        # we need to score the whole test now
        uit_scoring(parameters, Action.TEST)

    # set the test section time to the new amount of time assigned by an admin
    test_section_set_time = assigned_test_section.test_section_time

    # check for oldest existing start times on this section
    # so we can set the new section start time to this
    access_time = (
        AssignedTestSectionAccessTimes.objects.filter(
            assigned_test_section=assigned_test_section,
            time_type=AssignedTestSectionAccessTimeType.START,
        )
        .order_by("time")
        .first()
    )
    start_time = ""
    if not is_undefined(access_time):
        start_time = access_time.time

    # create a "start" time stamp for the new test section for records
    access_time = AssignedTestSectionAccessTimes(
        assigned_test_section=assigned_test_section,
        time_type=AssignedTestSectionAccessTimeType.START,
    )
    access_time.save()

    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=assigned_test.test_id, order=next_test_section_order
    )
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=assigned_test.test_id,
        order=str(int(next_test_section_order) + 1),
    ).first()
    if next_test_section:
        next_section_time = next_test_section.default_time
        next_assigned_test_section = AssignedTestSection.objects.get(
            assigned_test_id=assigned_test.id, test_section=next_test_section
        )
        if next_assigned_test_section.test_section_time:
            next_section_time = next_assigned_test_section.test_section_time
        context["next_test_section_time"] = next_section_time

    # serialize the data for return
    serialized = TestSectionSerializer(test_section, many=False, context=context).data

    # set the time on the test section
    if not is_undefined(test_section_set_time):
        serialized["default_time"] = test_section_set_time
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)


def retrieve_public_test_section_data(order, test_definition, special_section):
    context = {}
    ###
    # start new test section now
    ###
    next_test_section_order = ""

    next_test_section_order = str(int(order) + 1)
    # find the new test section
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=next_test_section_order
    )

    start_time = ""

    # fetch test section data
    test_section = TestSection.objects.get(
        test_definition_id=test_definition.id, order=next_test_section_order
    )
    # fetch the next section time
    next_test_section = TestSection.objects.filter(
        test_definition_id=test_definition.id,
        order=str(int(next_test_section_order) + 1),
    ).first()
    if next_test_section:
        next_test_section_time = next_test_section.default_time
        if not is_undefined(next_test_section_time):
            context["next_test_section_time"] = next_test_section_time

    serialized = TestSectionSerializer(test_section, many=False, context=context).data
    serialized["start_time"] = start_time

    return Response(serialized, status=status.HTTP_200_OK)


def getting_updated_time_after_lock_pause(request):
    assigned_test_id = request.query_params.get("assigned_test_id", None)
    test_section_id = request.query_params.get("test_section_id", None)
    if is_undefined(assigned_test_id):
        return Response(
            {"error": "no 'assigned_test_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_section_id):
        return Response(
            {"error": "no 'test_section_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    try:
        # getting LOCKED and PAUSED ta actions based on provided assigned_test_id and test_section_id
        ta_actions = TaActions.objects.filter(
            assigned_test_id=assigned_test_id,
            test_section_id=test_section_id,
            action_type_id__in=(TaActionsConstants.LOCK, TaActionsConstants.PAUSE),
        )
        # there is at least one existing PAUSED/LOCKED ta action
        if ta_actions:
            # initializing time calculation variables
            locked_paused_time = 0
            initial_time = None
            # looping in filtered ta_actions
            for ta_action in ta_actions:
                # getting test_section to make sure that this is a timed section
                test_section = TestSection.objects.get(id=ta_action.test_section_id)
                # this is a timed section (default_time exists)
                if test_section.default_time is not None:
                    # getting assigned test section access time (first one)
                    assigned_test_section = AssignedTestSection.objects.get(
                        assigned_test_id=assigned_test_id,
                        test_section_id=ta_action.test_section_id,
                    )
                    assigned_test_section_access_time = AssignedTestSectionAccessTimes.objects.filter(
                        assigned_test_section_id=assigned_test_section.id
                    ).first()

                    # assigned test section access time exists
                    if assigned_test_section_access_time is not None:
                        # updating initial_time with assigned_test_section_access_time value
                        initial_time = assigned_test_section_access_time.time

                        # LOCK action
                        if ta_action.action_type_id == TaActionsConstants.LOCK:
                            # getting lock test action object
                            lock_test_action = LockTestActions.objects.get(
                                ta_action_id=ta_action.id
                            )

                            # no laock test action end date (still on lock)
                            if lock_test_action.lock_end_date is None:
                                return Response(
                                    {
                                        "info": "there is not lock end date yet for this lock test action {}".format(
                                            lock_test_action.id
                                        )
                                    },
                                    status=status.HTTP_200_OK,
                                )
                            else:
                                # calculating locked time in seconds (keeping only 3 digits after the decimal point)
                                # incrementing locked_pause_time value
                                locked_paused_time += round(
                                    (
                                        lock_test_action.lock_end_date
                                        - lock_test_action.lock_start_date
                                    ).total_seconds(),
                                    3,
                                )

                        # PAUSE action
                        elif ta_action.action_type_id == TaActionsConstants.PAUSE:
                            # getting pause test action object
                            pause_test_action = PauseTestActions.objects.get(
                                ta_action_id=ta_action.id
                            )

                            # no pause test action end date (still on pause)
                            if pause_test_action.pause_end_date is None:
                                return Response(
                                    {
                                        "info": "there is not pause end date yet for this pause test action {}".format(
                                            pause_test_action.id
                                        )
                                    },
                                    status=status.HTTP_200_OK,
                                )
                            else:
                                # calculating paused time in seconds (keeping only 3 digits after the decimal point)
                                # incrementing locked_pause_time value
                                locked_paused_time += round(
                                    (
                                        pause_test_action.pause_end_date
                                        - pause_test_action.pause_start_date
                                    ).total_seconds(),
                                    3,
                                )
                    else:
                        return Response(
                            {
                                "error": "unable to find any assigned test section access time based on provided parameters"
                            },
                            status=status.HTTP_400_BAD_REQUEST,
                        )
                else:
                    return Response(
                        {
                            "info": "test section id {} is not a timed section".format(
                                test_section.id
                            )
                        },
                        status.HTTP_200_OK,
                    )

            # initial_time has been updated at least once
            if initial_time is not None:
                # getting updated time
                updated_time = initial_time + timedelta(seconds=locked_paused_time)
                return Response(updated_time)
            # the current test section has not been started yet
            else:
                return Response(
                    {
                        "error": "test section id {} has not been started yet".format(
                            test_section_id
                        )
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

        else:
            return Response(
                {
                    "info": "there are no LOCKED and/or PAUSED ta actions based on provided parameters"
                },
                status=status.HTTP_200_OK,
            )

    except AssignedTestSection.DoesNotExist:
        return Response(
            {
                "error": "unable to find any assigned test section based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )

    except AssignedTestSectionAccessTimes.DoesNotExist:
        return Response(
            {
                "error": "unable to find any assigned test section access time based on provided parameters"
            },
            status=status.HTTP_400_BAD_REQUEST,
        )
