from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from cms.cms_models.test_definition import TestDefinition
from cms.serializers.test_definition_serializer import (
    TestDefinitionLabelSerializer,
    TestDefinitionVersionsCollectedSerializer,
)

# api/get-test-definition-labels
class GetTestDefinitionsLabelsView(APIView):
    def get(self, request):
        return Response(
            TestDefinitionLabelSerializer(TestDefinition.objects, many=True).data
        )

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated()]

        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# api/get-test-definition-versions-collected
class GetTestDefinitionsVersionsCollectedView(APIView):
    def get(self, request):
        return Response(
            TestDefinitionVersionsCollectedSerializer(
                TestDefinition.objects.order_by().values("test_code").distinct(),
                many=True,
            ).data
        )

    def get_permissions(self):
        try:
            return [permissions.IsAuthenticated()]

        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]
