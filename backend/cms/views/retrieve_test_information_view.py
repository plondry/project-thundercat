from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from cms.serializers.get_test_information_serializer import (
    GetActiveNonPublicTestsSerializer,
)
from cms.cms_models.test_definition import TestDefinition


# getting non public tests (where is_public is set to false)
class GetActiveNonPublicTests(APIView):
    def get(self, request):
        return Response(get_active_non_public_tests())

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_active_non_public_tests():
    active_non_public_test_name = TestDefinition.objects.filter(
        is_public=False, active=True
    )
    serialized = GetActiveNonPublicTestsSerializer(
        active_non_public_test_name, many=True
    )
    return serialized.data
