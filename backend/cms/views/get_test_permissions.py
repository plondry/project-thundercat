from operator import or_, and_
from functools import reduce
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from user_management.user_management_models.user_models import User
from user_management.views.utils import CustomPagination
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.test_definition import TestDefinition
from cms.views.utils import is_undefined, get_user_info_from_jwt_token
from cms.serializers.get_test_information_serializer import GetTestPermissionsSerializer

# getting test permissions data for a specified user
def get_test_permissions(request):
    user_info = get_user_info_from_jwt_token(request)
    test_permissions = TestPermissions.objects.filter(username=user_info["username"])
    serializer = GetTestPermissionsSerializer(test_permissions, many=True)
    return Response(serializer.data)


# getting specific test permission's financial data
def get_test_permission_financial_data(request):
    user_info = get_user_info_from_jwt_token(request)
    # making sure that we have the needed parameters
    test_order_number = request.query_params.get("test_order_number", None)
    test_to_administer = request.query_params.get("test_to_administer", None)
    if is_undefined(test_order_number):
        return Response(
            {"error": "no 'test_order_number' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(test_to_administer):
        return Response(
            {"error": "no 'test_to_administer' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    try:
        # getting test permission's financial data based on the ta username, test order number and test ID
        test_permission_financial_data = TestPermissions.objects.filter(
            username_id=user_info["username"],
            test_order_number=test_order_number,
            test_id=TestDefinition.objects.get(id=test_to_administer),
        )

        # if queryset is empty
        if not test_permission_financial_data:
            return Response(
                {
                    "error": "the specified test permission based on the paramters does not exist"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        # getting serialized test permissions data
        serializer = GetTestPermissionsSerializer(
            test_permission_financial_data, many=True
        )
        return Response(serializer.data)
    except TestDefinition.DoesNotExist:
        return Response(
            {"error": "the specified test does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all active test permissions
def get_all_active_test_permissions(request):
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # get all active test permissions
    active_test_permissions = TestPermissions.objects.all()
    # serializer = GetTestPermissionsSerializer(active_test_permissions, many=True)

    # ordering active test permissions by last_name
    # getting serialized active test permissions data
    serialized_active_test_permissions_data = GetTestPermissionsSerializer(
        active_test_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_active_test_permissions = sorted(
        serialized_active_test_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    test_permission_ids_array = []
    # looping in ordered active test permissions
    for i in ordered_active_test_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        test_permission_ids_array.insert(len(test_permission_ids_array), i["id"])

    # sorting active test permissions queryset based on ordered (by last name) user permission ids
    new_active_test_permissions = list(
        TestPermissions.objects.filter(id__in=test_permission_ids_array)
    )
    new_active_test_permissions.sort(
        key=lambda t: test_permission_ids_array.index(t.id)
    )

    # getting page results based on queryset (new_active_test_permissions)
    page = paginator.paginate_queryset(new_active_test_permissions, request)
    # serializing the data
    serialized = GetTestPermissionsSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)


def get_found_active_test_permissions(request):
    keyword = request.query_params.get("keyword", None)
    current_language = request.query_params.get("current_language", None)
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(keyword):
        return Response(
            {"error": "no 'keyword' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(current_language):
        return Response(
            {"error": "no 'current_language' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # ==================== TEST NAME SEARCH (BEGIN) ====================
    # getting all tests data (need ids of each test)
    tests = TestDefinition.objects.all()

    # search while interface is in English
    if current_language == "en":
        # getting all tests that contains the keyword string (compared with English test name only)
        # and at least one of the test ids from tests object
        found_test_names = TestDefinition.objects.filter(
            reduce(or_, [Q(id=test.id, en_name__icontains=keyword) for test in tests])
        )
    # search while interface is in French
    else:
        # getting all tests that contains the keyword string (compared with French test name only)
        # and at least one of the test ids from tests object
        found_test_names = TestDefinition.objects.filter(
            reduce(or_, [Q(id=test.id, fr_name__icontains=keyword) for test in tests])
        )

    # there is at least one matching test_name
    if found_test_names:
        # getting all test_permission_ids based on found_test_names object
        test_permission_ids_based_on_found_tests = TestPermissions.objects.filter(
            reduce(or_, [Q(test_id=test.id) for test in found_test_names])
        )
    # there are no matching found_test_names
    else:
        test_permission_ids_based_on_found_tests = []
    # ==================== TEST NAME SEARCH (END) ====================

    # ==================== USER NAMES SEARCH (BEGIN) ====================
    # since there is a comma between last name and first name in active test permissions table, if the user put
    # a comma in one of the search keyword words, remove it
    keyword_without_comma = keyword.replace(",", "")
    # splitting keyword string
    split_keyword = keyword_without_comma.split()

    # if keyword contains more than one word
    if len(split_keyword) > 1:
        # getting all usernames based on matching first and last names
        usernames = User.objects.filter(
            reduce(
                and_,
                [
                    Q(first_name__icontains=splitted_keyword)
                    | Q(last_name__icontains=splitted_keyword)
                    for splitted_keyword in split_keyword
                ],
            )
        )
    # keyword is empty or contains only one word
    else:
        # getting all usernames based on matching first name and/or last name
        usernames = User.objects.filter(
            Q(first_name__icontains=keyword) | Q(last_name__icontains=keyword)
        )

    # there is at least one matching name
    if usernames:
        test_permission_ids_based_on_found_names = TestPermissions.objects.filter(
            reduce(or_, [Q(username_id=user.username) for user in usernames])
        )
        # if queryset is empty (meaning that the username(s) exists, but doesn't have any test permissions)
        if not bool(test_permission_ids_based_on_found_names):
            test_permission_ids_based_on_found_names = []
    # there are no matching names
    else:
        test_permission_ids_based_on_found_names = []
    # ==================== USER NAMES SEARCH (END) ====================

    # ==================== TEST ORDER NUMBER SEARCH (BEGIN) ====================
    # getting all test order numbers that contains keyword string
    order_numbers = TestPermissions.objects.filter(test_order_number__icontains=keyword)
    # if there is at least one matching order number
    if order_numbers:
        test_permission_ids_based_on_found_order_nbr = order_numbers
    # there are no matching order numbers
    else:
        test_permission_ids_based_on_found_order_nbr = []
    # ==================== TEST ORDER NUMBER SEARCH (END) ====================

    # if at least one result has been found based on the keyword provided
    if (
        test_permission_ids_based_on_found_tests != []
        or test_permission_ids_based_on_found_names != []
        or test_permission_ids_based_on_found_order_nbr != []
    ):

        # results have been found based on test names
        if test_permission_ids_based_on_found_tests != []:
            found_active_test_permissions_tests = TestPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=test.id)
                        for test in test_permission_ids_based_on_found_tests
                    ],
                )
            )
        # no results have been found based on test names
        else:
            found_active_test_permissions_tests = TestPermissions.objects.none()

        # results have been found based on user names (first and/or last name)
        if test_permission_ids_based_on_found_names != []:
            found_active_test_permissions_names = TestPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=name.id)
                        for name in test_permission_ids_based_on_found_names
                    ],
                )
            )
        # no results have been found based on user names (first and/or last name)
        else:
            found_active_test_permissions_names = TestPermissions.objects.none()

        # results have been found based on test order number
        if test_permission_ids_based_on_found_order_nbr != []:
            found_active_test_permissions_order_nbrs = TestPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(id=order_number.id)
                        for order_number in test_permission_ids_based_on_found_order_nbr
                    ],
                )
            )
        # no results have been found based on test order number
        else:
            found_active_test_permissions_order_nbrs = TestPermissions.objects.none()

        # merging all found results together
        found_active_test_permissions = found_active_test_permissions_names.union(
            found_active_test_permissions_tests,
            found_active_test_permissions_order_nbrs,
        )

    else:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # ordering found active test permissions by last_name
    # getting serialized active test permissions data
    serialized_found_active_test_permissions_data = GetTestPermissionsSerializer(
        found_active_test_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_found_active_test_permissions = sorted(
        serialized_found_active_test_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # initializing test permissions ids array
    test_permission_ids_array = []
    # looping in ordered active test permissions
    for i in ordered_found_active_test_permissions:
        # inserting test permissions ids (ordered respectively by last name) in an array
        test_permission_ids_array.insert(len(test_permission_ids_array), i["id"])

    # sorting active test permissions queryset based on ordered (by last name) user permission ids
    new_found_active_test_permissions = list(
        TestPermissions.objects.filter(id__in=test_permission_ids_array)
    )
    new_found_active_test_permissions.sort(
        key=lambda t: test_permission_ids_array.index(t.id)
    )

    # getting page results based on queryset (new_found_active_test_permissions)
    page = paginator.paginate_queryset(new_found_active_test_permissions, request)
    # serializing the data
    serialized = GetTestPermissionsSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)
