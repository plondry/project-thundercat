from django.core.exceptions import ObjectDoesNotExist
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from cms.views.retrieve_test_section_data import (
    retrieve_test_section_data,
    retrieve_public_test_section_data,
    is_test_public,
    getting_updated_time_after_lock_pause,
)
from backend.views.utils import is_undefined
from cms.cms_models.test_definition import TestDefinition


class SpecialSection:
    FINISH = "finish"
    QUIT = "quit"


# This is the new api for getting test sections related to the factory


class TestSectionData(APIView):
    def get(self, request):
        order = request.query_params.get("order", None)
        special_section = request.query_params.get("special_section", None)
        assigned_test_id = request.query_params.get("assigned_test_id", None)
        test_id = request.query_params.get("test_id", None)
        # always need a test definition param

        test_definition = TestDefinition.objects.get(id=test_id)
        public_test = test_definition.is_public

        # if the test is public, don't record any data
        if public_test and is_undefined(assigned_test_id):
            return retrieve_public_test_section_data(
                order, test_definition, special_section
            )

        if is_undefined(assigned_test_id):
            return Response(
                {"error": "no 'assigned_test_id' parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        if is_undefined(order):
            return Response(
                {"error": "no 'order' parameter"}, status=status.HTTP_400_BAD_REQUEST
            )

        # check that parameters are valid
        if is_undefined(special_section):
            return retrieve_test_section_data(order, assigned_test_id)

        elif special_section == SpecialSection.QUIT:
            return retrieve_test_section_data(
                order, assigned_test_id, quit_section=True
            )

        else:
            return Response(
                {"error": "bad parameters"}, status=status.HTTP_400_BAD_REQUEST
            )

    # assign the permissions depending on if the test is public or not

    def get_permissions(self):
        try:
            # is_public = true
            if is_test_public(self.request.query_params.get("test_definition", None)):
                return [permissions.IsAuthenticatedOrReadOnly()]
            # is_public = false
            else:
                return [permissions.IsAuthenticated()]
        # is_test_public does not exist
        except ObjectDoesNotExist:
            return [permissions.IsAdminUser()]


# URL: api/get-test-section


class GetUpdatedTimeAfterLockPause(APIView):
    def get(self, request):
        return getting_updated_time_after_lock_pause(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]
