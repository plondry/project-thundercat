from rest_framework import status
from rest_framework.response import Response
from cms.cms_models.test_definition import TestDefinition
from cms.serializers.test_extraction.flat_test_extract_serializer import (
    FlatTestDefinitionExtractSerializer,
)


def extract_test(test_definition):
    test_definition = TestDefinition.objects.get(id=test_definition)
    data = {}
    data["new_test"] = FlatTestDefinitionExtractSerializer(
        test_definition, many=False
    ).data
    return Response(data, status=status.HTTP_200_OK)


# may be useful
# mixed_query = list(invoices) + list(pcustomers)
# json_str = serializers.serialize('json', mixed_query))
# response = HttpResponse(json_str, content_type='application/json')
# response['Content-Disposition'] = 'attachment; filename=export.json'
# https://stackoverflow.com/questions/37298021/serving-json-file-to-download
# https://www.django-rest-framework.org/api-guide/renderers/
