from rest_framework import serializers
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_permissions_model import TestPermissions
from user_management.user_management_models.user_models import User


class GetActiveNonPublicTestsSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class GetEnFrTestNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = ("id", "en_name", "fr_name", "version")


class GetTestPermissionsSerializer(serializers.ModelSerializer):
    test = serializers.SerializerMethodField()
    first_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    def get_test(self, request):
        test = TestDefinition.objects.get(id=request.test_id)
        return GetEnFrTestNameSerializer(test).data

    # getting ta first name
    def get_first_name(self, request):
        first_name = User.objects.get(username=request.username).first_name
        return first_name

    # getting ta last name
    def get_last_name(self, request):
        get_last_name = User.objects.get(username=request.username).last_name
        return get_last_name

    class Meta:
        model = TestPermissions
        fields = "__all__"
