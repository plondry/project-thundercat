from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.page_section_type_sample_email import PageSectionTypeSampleEmail


class PageSectionTypeSampleEmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeSampleEmail
        fields = [
            "email_id",
            "from_field",
            "to_field",
            "date_field",
            "body",
            "subject_field",
        ]
