from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.section_component_page import SectionComponentPage
from cms.cms_models.page_section import PageSection
from cms.serializers.page_section_serializer import PageSectionSerializer

from backend.static.languages import Languages


class SectionComponentPageSerializer(serializers.ModelSerializer):
    pages = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()

    class Meta:
        model = TestSectionComponent
        fields = ["order", "id", "title", "pages"]

    def get_title(self, request):
        language = self.context.get("language")
        if language == Languages.EN:
            return request.en_title
        return request.fr_title

    def get_component_type(self, request):
        return request.component_type.component_type

    def get_pages(self, request):
        data = {}
        language = self.context.get("language")

        pages_sections = PageSection.objects.filter(
            section_component_page=request.id).order_by("order")
        data["page_sections"] = PageSectionSerializer(
            pages_sections, many=True, context={"language": language}).data
        return data
