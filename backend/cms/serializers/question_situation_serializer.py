from rest_framework import serializers
from cms.cms_models.question_situation import QuestionSituation


class QuestionSituationSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSituation
        fields = "__all__"
