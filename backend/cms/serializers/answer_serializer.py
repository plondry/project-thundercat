from rest_framework import serializers
from cms.cms_models.answer import Answer
from cms.cms_models.answer_details import AnswerDetails
from cms.serializers.answer_details_serializer import AnswerDetailsSerializer


class AnswerSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = Answer
        fields = "__all__"

    def to_representation(self, obj):
        """Move fields from answer details to answer."""
        representation = super().to_representation(obj)
        details_representation = representation.pop("details")
        for key in details_representation:
            if key != "id":
                representation[key] = details_representation[key]
        return representation

    def get_details(self, request):
        language = self.context.get("language")

        obj = AnswerDetails.objects.get(answer=request.id, language=language)
        return AnswerDetailsSerializer(
            obj, many=False, context={"language": language}
        ).data
