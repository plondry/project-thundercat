from rest_framework import serializers
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown
from cms.cms_models.question_section_type_markdown_end import (
    QuestionSectionTypeMarkdownEnd,
)
from cms.serializers.question_section_type_markdown_serializer import (
    QuestionSectionTypeMarkdownSerializer,
)
from cms.serializers.question_section_type_markdown_end_serializer import (
    QuestionSectionTypeMarkdownEndSerializer,
)
from cms.static.question_section_type import QuestionSectionType


class QuestionSectionSerializer(serializers.ModelSerializer):
    question_section_content = serializers.SerializerMethodField()

    class Meta:
        model = QuestionSection
        fields = "__all__"

    def get_question_section_content(self, request):
        language = self.context.get("language")

        if request.question_section_type == QuestionSectionType.MARKDOWN:
            data = QuestionSectionTypeMarkdown.objects.get(
                question_section=request.id, language=language
            )
            return QuestionSectionTypeMarkdownSerializer(data).data

        elif request.question_section_type == QuestionSectionType.END:
            data = QuestionSectionTypeMarkdownEnd.objects.get(
                question_section=request.id, language=language
            )
            return QuestionSectionTypeMarkdownEndSerializer(data).data
