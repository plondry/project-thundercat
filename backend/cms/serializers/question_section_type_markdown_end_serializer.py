from rest_framework import serializers
from cms.cms_models.question_section_type_markdown_end import (
    QuestionSectionTypeMarkdownEnd,
)


class QuestionSectionTypeMarkdownEndSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSectionTypeMarkdownEnd
        fields = ["content"]
