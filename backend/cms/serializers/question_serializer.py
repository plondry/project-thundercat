from rest_framework import serializers
from random import shuffle
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.answer import Answer
from cms.cms_models.question_situation import QuestionSituation
from cms.cms_models.situation_example_rating import SituationExampleRating

from cms.serializers.answer_serializer import AnswerSerializer
from cms.serializers.multiple_choice_question_serializer import (
    MultipleChoiceQuestionSerializer,
)
from cms.serializers.question_section_serializer import QuestionSectionSerializer
from cms.serializers.email_question_serializer import (
    EmailQuestionSerializer,
    NoLanguageDataEmailQuestionSerializer,
)
from cms.serializers.situation_example_rating_serializer import (
    SituationExampleRatingSerializer,
)
from cms.serializers.question_situation_serializer import QuestionSituationSerializer
from cms.static.question_type import QuestionType


class QuestionSerializer(serializers.ModelSerializer):
    sections = serializers.SerializerMethodField()
    details = serializers.SerializerMethodField()
    answers = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    situation = serializers.SerializerMethodField()
    example_ratings = serializers.SerializerMethodField()

    class Meta:
        model = NewQuestion
        fields = [
            "id",
            "sections",
            "question_type",
            "details",
            "answers",
            "email",
            "situation",
            "example_ratings",
        ]

    def get_details(self, request):
        return_data = {}

        if request.question_type == QuestionType.MULTIPLE_CHOICE:
            # get the extended question info
            m_c_question = MultipleChoiceQuestion.objects.get(question=request.id)
            m_c_data = MultipleChoiceQuestionSerializer(m_c_question, many=False).data
            return_data["diffculty"] = m_c_data["question_difficulty_type"]
        return return_data

    def get_sections(self, request):
        sections = QuestionSection.objects.filter(question=request.id).order_by("order")
        return QuestionSectionSerializer(sections, many=True, context=self.context).data

    def get_situation(self, request):
        if not self.context.get("scorer", None):
            return {}

        situation = QuestionSituation.objects.filter(
            question=request.id, language=self.context["language"]
        ).first()

        if situation:
            return QuestionSituationSerializer(situation, context=self.context).data
        else:
            return {}

    def get_answers(self, request):
        answers = list(Answer.objects.filter(question=request.id))
        shuffle(answers)
        return AnswerSerializer(answers, many=True, context=self.context).data

    def get_email(self, request):
        email = EmailQuestion.objects.filter(question=request.id).first()
        return EmailQuestionSerializer(email, context=self.context).data

    def get_example_ratings(self, request):
        if not self.context.get("scorer", None):
            return []

        ratings = SituationExampleRating.objects.filter(question=request.id)

        if not ratings:
            return []

        return SituationExampleRatingSerializer(
            ratings, many=True, context=self.context
        ).data


class NoLanguageDataQuestionSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField()

    class Meta:
        model = NewQuestion
        fields = ["id", "question_type", "email"]

    def get_email(self, request):
        email = EmailQuestion.objects.get(question=request.id)
        return NoLanguageDataEmailQuestionSerializer(email).data
