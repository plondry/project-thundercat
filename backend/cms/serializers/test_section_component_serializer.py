from random import shuffle, choice
from rest_framework import serializers
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.section_component_page import SectionComponentPage
from cms.serializers.section_component_page_serializer import (
    SectionComponentPageSerializer,
)
from cms.cms_models.question_list_rule import QuestionListRule
from cms.cms_models.new_question import NewQuestion
from cms.serializers.question_serializer import (
    QuestionSerializer,
    NoLanguageDataQuestionSerializer,
)
from cms.cms_models.address_book_contact import AddressBookContact
from cms.serializers.address_book_contact_serializer import AddressBookContactSerializer
from cms.static.test_section_component_type import TestSectionComponentType
from backend.static.languages import Languages


class TestSectionComponentSerializer(serializers.ModelSerializer):
    test_section_component = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    component_type = serializers.SerializerMethodField()

    class Meta:
        model = TestSectionComponent
        fields = [
            "order",
            "id",
            "title",
            "component_type",
            "test_section_component",
            "language",
        ]

    def get_title(self, request):
        language = request.language
        if language == Languages.EN:
            return request.en_title
        return request.fr_title

    def get_component_type(self, request):
        return request.component_type

    def get_test_section_component(self, request):
        context = self.context
        context["language"] = request.language

        component_type = request.component_type
        # factory pattern for test definition model serializing
        if (
            component_type == TestSectionComponentType.SINGLE_PAGE
            or component_type == TestSectionComponentType.SIDE_NAVIGATION
        ):

            # get single component
            pages = SectionComponentPage.objects.filter(
                test_section_component=request.id
            ).order_by("order")

            return SectionComponentPageSerializer(
                pages, many=True, context=context
            ).data

        elif component_type == TestSectionComponentType.INBOX:
            data = {}
            # get question list using Question List Rules
            questions = get_question_list(request.id, request.shuffle_all_questions)

            data["questions"] = QuestionSerializer(
                questions, many=True, context=context
            ).data
            address_book = AddressBookContact.objects.filter(
                test_section__in=request.test_section.all()
            )
            data["address_book"] = AddressBookContactSerializer(
                address_book, many=True, context=context
            ).data
            return data

        elif component_type == TestSectionComponentType.QUESTION_LIST:
            data = {}
            # get question list using Question List Rules
            questions = get_question_list(request.id, request.shuffle_all_questions)

            data["questions"] = QuestionSerializer(
                questions, many=True, context=context
            ).data
            return data


def get_question_list(test_section_component_id, shuffle_all):
    questions = []
    question_id_array = []
    questions_to_add = []
    question_ids = []

    rules = QuestionListRule.objects.filter(
        test_section_component=test_section_component_id
    ).order_by("order")
    # for each rule get number of questions
    for rule in rules:
        block_type = rule.question_block_type.all()
        # if there is more than one block,
        # randomly select the block to take questions from
        blocks = []
        for block in block_type:
            blocks.append(block)
        block_type = choice(blocks)

        # if null/0 get all questions
        if rule.number_of_questions == 0:
            question_ids = NewQuestion.objects.filter(
                test_section_component=test_section_component_id,
                question_block_type=block_type,
            ).values("id")
        else:
            question_ids = NewQuestion.objects.filter(
                test_section_component=test_section_component_id,
                question_block_type=block_type,
            ).values("id")[: rule.number_of_questions]
            # splice array after shuffle or same questions returned

        # if rule includes shuffling questions
        for q_id in question_ids:
            questions_to_add.append(q_id)
        if rule.shuffle:
            shuffle(questions_to_add)

    # if there are no rules, just return every question
    if not rules:
        questions_to_add = []
        for question in NewQuestion.objects.filter(
            test_section_component=test_section_component_id
        ).values("id"):
            questions_to_add.append(question)

    # if the test section component config to shuffle all questions
    if shuffle_all:
        shuffle(questions_to_add)
    # get the entire question details now
    for question_id in questions_to_add:
        question = NewQuestion.objects.get(id=question_id["id"])
        # make sure that there is no duplicate questions
        if question.id in question_id_array:
            continue

        # add dependent questions in order/sequence
        questions = add_dependencies(questions, question, question_id_array)

    return questions


def add_dependencies(questions, question, question_id_array):
    # if this testlet has already been added
    if question.id in question_id_array:
        return questions

    dependents = [question]
    for dependent_question in question.dependencies.all():
        dependents.append(dependent_question)

    # sort by order
    dependents.sort(key=lambda q: q.order)

    # add dependent question is the order they were given
    for question in dependents:
        if question.id in question_id_array:
            continue
        questions.append(question)
        question_id_array.append(question.id)

    return questions


class InboxScorerSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField()

    class Meta:
        model = TestSectionComponent
        fields = ["questions"]

    def get_questions(self, request):
        # right now this returns all questions. Later it might be worth returning
        # only questions in the assigned test
        assigned_test_id = self.context["assigned_test_id"]
        questions = NewQuestion.objects.filter(test_section_component=request)

        return NoLanguageDataQuestionSerializer(questions, many=True).data
