from rest_framework import serializers
from cms.cms_models.test_definition import TestDefinition


class TestDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestDefinition
        fields = "__all__"


class TestDefinitionLabelSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = TestDefinition
        fields = ["details"]

    def to_representation(self, obj):
        """Move fields from details to user test def."""
        representation = super().to_representation(obj)
        details_representation = representation.pop("details")
        for key in details_representation:
            representation[key] = details_representation[key]
        return representation

    def get_details(self, request):
        data = {}
        data["label"] = "{0} version {1}".format(request.test_code, request.version)
        data["value"] = request.id
        return data


class TestDefinitionVersionsCollectedSerializer(serializers.ModelSerializer):
    details = serializers.SerializerMethodField()

    class Meta:
        model = TestDefinition
        fields = ["details", "test_code"]

    def get_details(self, request):
        # get all the versions
        versions = TestDefinition.objects.filter(
            test_code=request["test_code"]
        ).order_by()
        return TestDefinitionSerializer(versions, many=True).data
