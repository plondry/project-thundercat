from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.page_section_type_sample_email_response import (
    PageSectionTypeSampleEmailResponse,
)


class PageSectionTypeSampleEmailResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeSampleEmailResponse
        fields = ["cc_field", "to_field", "response", "reason"]
