from rest_framework import serializers
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown


class QuestionSectionTypeMarkdownSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionSectionTypeMarkdown
        fields = ['content']
