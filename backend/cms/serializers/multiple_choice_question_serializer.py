from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion


class MultipleChoiceQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MultipleChoiceQuestion
        fields = ["question_difficulty_type"]
