from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.test_section_reducer import TestSectionReducer


class TestSectionReducerSerializer(serializers.ModelSerializer):
    class Meta:
        model = TestSectionReducer
        fields = ['reducer']
