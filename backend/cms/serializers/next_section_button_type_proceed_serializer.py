from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.next_section_button_type_proceed import NextSectionButtonTypeProceed


class NextSectionButtonTypeProceedSerializer(serializers.ModelSerializer):
    class Meta:
        model = NextSectionButtonTypeProceed
        fields = ["button_text"]
