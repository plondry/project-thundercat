from django.utils import timezone
from rest_framework import serializers
from cms.cms_models.page_section_type_markdown import PageSectionTypeMarkdown


class PageSectionTypeMarkdownSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageSectionTypeMarkdown
        fields = ['content']
