from rest_framework import serializers
from cms.cms_models.page_section import PageSection
from cms.cms_models.page_section_type_markdown import PageSectionTypeMarkdown
from cms.cms_models.page_section_type_sample_email import PageSectionTypeSampleEmail
from cms.cms_models.page_section_type_sample_email_response import (
    PageSectionTypeSampleEmailResponse,
)
from cms.cms_models.page_section_type_sample_task_response import (
    PageSectionTypeSampleTaskResponse,
)
from cms.cms_models.page_section_type_image_zoom import PageSectionTypeImageZoom
from cms.serializers.page_section_type_markdown_serializer import (
    PageSectionTypeMarkdownSerializer,
)
from cms.serializers.page_section_type_sample_email_serializer import (
    PageSectionTypeSampleEmailSerializer,
)
from cms.serializers.page_section_type_zoom_image_serializer import (
    PageSectionTypeImageZoomSerializer,
)
from cms.serializers.page_section_type_sample_email_response_serializer import (
    PageSectionTypeSampleEmailResponseSerializer,
)
from cms.serializers.page_section_type_sample_task_response_serializer import (
    PageSectionTypeSampleTaskResponseSerializer,
)
from cms.cms_models.page_section_type_tree_description import (
    PageSectionTypeTreeDescription,
)
from cms.serializers.page_section_type_tree_description_serializer import (
    PageSectionTypeTreeDescriptionSerializer,
)
from cms.static.page_section_type import PageSectionType


class PageSectionSerializer(serializers.ModelSerializer):
    page_section_content = serializers.SerializerMethodField()

    class Meta:
        model = PageSection
        fields = "__all__"

    def get_page_section_content(self, request):
        language = self.context.get("language")
        if request.page_section_type == PageSectionType.MARKDOWN:
            data = PageSectionTypeMarkdown.objects.get(
                page_section_id=request.id, language=language
            )
            return PageSectionTypeMarkdownSerializer(data).data

        elif request.page_section_type == PageSectionType.ZOOM_IMAGE:
            data = PageSectionTypeImageZoom.objects.get(
                page_section_id=request.id, language=language
            )
            return PageSectionTypeImageZoomSerializer(data).data

        elif request.page_section_type == PageSectionType.SAMPLE_EMAIL:
            data = PageSectionTypeSampleEmail.objects.get(
                page_section_id=request.id, language=language
            )
            return PageSectionTypeSampleEmailSerializer(data).data

        elif request.page_section_type == PageSectionType.SAMPLE_EMAIL_RESPONSE:
            data = PageSectionTypeSampleEmailResponse.objects.get(
                page_section_id=request.id, language=language
            )
            return PageSectionTypeSampleEmailResponseSerializer(data).data

        elif request.page_section_type == PageSectionType.SAMPLE_TASK_RESPONSE:
            data = PageSectionTypeSampleTaskResponse.objects.get(
                page_section_id=request.id, language=language
            )
            return PageSectionTypeSampleTaskResponseSerializer(data).data

        elif request.page_section_type == PageSectionType.TREE_DESCRIPTION:
            data = PageSectionTypeTreeDescription.objects.get(
                page_section_id=request.id, language=language
            )
            return PageSectionTypeTreeDescriptionSerializer(
                data, context={"language": language}
            ).data
