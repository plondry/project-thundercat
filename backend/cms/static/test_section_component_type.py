
class TestSectionComponentType:
    SINGLE_PAGE = 1
    SIDE_NAVIGATION = 2
    INBOX = 3
    QUESTION_LIST = 4  # multiple choice questions

# mirror of
# Constants.js in frontend/src/components/testFactory
