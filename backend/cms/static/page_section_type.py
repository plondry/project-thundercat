
class PageSectionType:
    MARKDOWN = 1
    ZOOM_IMAGE = 2
    SAMPLE_EMAIL = 3
    SAMPLE_EMAIL_RESPONSE = 4
    SAMPLE_TASK_RESPONSE = 5
    TREE_DESCRIPTION = 6
    PRIVACY_NOTICE = 7

# mirror of
# Constants.js in frontend/src/components/testFactory
