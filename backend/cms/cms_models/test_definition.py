from django.db import models

MAX_CHAR_LEN = 100


class TestDefinition(models.Model):
    test_code = models.CharField(max_length=MAX_CHAR_LEN)  # EMIB-880
    version = models.IntegerField(null=False, blank=False)
    en_name = models.CharField(max_length=MAX_CHAR_LEN)
    fr_name = models.CharField(max_length=MAX_CHAR_LEN)
    is_public = models.BooleanField(default=True)
    active = models.BooleanField(default=False)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0} code: {1} version: {2}".format(
            self.en_name, self.test_code, self.version
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Definition"
        unique_together = (("test_code", "version"),)
        # this will order all returned results by these fields
        ordering = ["test_code", "version"]
