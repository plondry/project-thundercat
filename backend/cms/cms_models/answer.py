from django.db import models
from cms.cms_models.new_question import NewQuestion
from backend.custom_models.language import Language


class Answer(models.Model):
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING)
    scoring_value = models.IntegerField(default=0, blank=False, null=False)

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "question id: {0}, answerid: {2}, score: {1}".format(
            self.question.id, self.scoring_value, self.id
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Answer"
