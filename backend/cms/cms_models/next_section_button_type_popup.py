from django.db import models
from cms.cms_models.test_section import TestSection
from backend.custom_models.language import Language

MAX_CHAR_LEN = 20


class NextSectionButtonTypePopup(models.Model):
    test_section = models.ForeignKey(TestSection, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    content = models.TextField()
    title = models.TextField()
    button_text = models.CharField(max_length=MAX_CHAR_LEN)
    confirm_proceed = models.BooleanField(default=False)

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0}: {1} ({2}) {3}".format(
            self.test_section.test_definition.test_code,
            self.test_section.en_title,
            self.language,
            self.content,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Next Section Button Type Popup"
