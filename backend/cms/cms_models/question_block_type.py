from django.db import models
from cms.cms_models.test_definition import TestDefinition

MAX_CHAR_LEN = 100

# this model replaces the old question model


class QuestionBlockType(models.Model):
    name = models.CharField(max_length=10)
    test_definition = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING)
    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} type name: {1}".format(self.id, self.name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question Block Type"
