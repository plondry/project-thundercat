from django.db import models
from cms.cms_models.test_definition import TestDefinition

MAX_LENGTH = 200


class FileResourceDetails(models.Model):
    path = models.CharField(max_length=MAX_LENGTH)
    test_definition = models.ForeignKey(
        TestDefinition, on_delete=models.DO_NOTHING)

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0}: {1}, test: {2} v {3}".format(
            self.id, self.path, self.test_definition.test_code, self.test_definition.version)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "File Resource Details"
        unique_together = (('path', 'test_definition'),)
