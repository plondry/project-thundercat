from django.db import models
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.question_block_type import QuestionBlockType

MAX_CHAR_LEN = 100

# this model replaces the old question model


class NewQuestion(models.Model):
    test_section_component = models.ForeignKey(
        TestSectionComponent, on_delete=models.DO_NOTHING
    )
    question_type = models.IntegerField()
    question_block_type = models.ForeignKey(
        QuestionBlockType, on_delete=models.DO_NOTHING, null=True, blank=True
    )
    pilot = models.BooleanField(default=False, blank=False, null=False)
    dependencies = models.ManyToManyField("self")
    order = models.IntegerField(default=0)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} ({1}) section: {2}, type: {3}".format(
            self.id,
            self.test_section_component.en_title,
            self.question_type,
            self.question_block_type,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question"
