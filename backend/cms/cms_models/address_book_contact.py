from django.db import models
from cms.cms_models.test_section import TestSection

NAME_MAX_LENGTH = 30
MAX_LENGTH = 150


class AddressBookContact(models.Model):
    # this is for tree descriptions
    name = models.CharField(max_length=NAME_MAX_LENGTH, null=True)
    parent = models.ForeignKey(
        "self", blank=True, null=True, on_delete=models.DO_NOTHING
    )
    test_section = models.ManyToManyField(TestSection)
    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0} {1}".format(self.id, self.name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Address Book Contact"
