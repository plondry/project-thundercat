from django.db import models
from cms.cms_models.new_question import NewQuestion
from backend.custom_models.language import Language

MAX_CHAR_LEN = 100


class QuestionSituation(models.Model):
    situation = models.TextField()
    question = models.ForeignKey(NewQuestion, on_delete=models.CASCADE)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} type name: {1}".format(self.id, self.name)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question Situation"
