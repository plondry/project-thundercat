from django.contrib import admin
from cms.cms_models.test_permissions_model import TestPermissions
from cms.cms_models.page_section import PageSection
from cms.cms_models.page_section_type_markdown import PageSectionTypeMarkdown
from cms.cms_models.reducer_control import ReducerControl
from cms.cms_models.test_definition import TestDefinition
from cms.cms_models.test_section_reducer import TestSectionReducer
from cms.cms_models.test_section import TestSection
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.page_section_type_image_zoom import PageSectionTypeImageZoom
from cms.cms_models.page_section_type_sample_email import PageSectionTypeSampleEmail
from cms.cms_models.page_section_type_sample_email_response import (
    PageSectionTypeSampleEmailResponse,
)
from cms.cms_models.page_section_type_sample_task_response import (
    PageSectionTypeSampleTaskResponse,
)
from cms.cms_models.section_component_page import SectionComponentPage
from cms.cms_models.file_resource_details import FileResourceDetails
from cms.cms_models.next_section_button_type_proceed import NextSectionButtonTypeProceed
from cms.cms_models.next_section_button_type_popup import NextSectionButtonTypePopup

from cms.cms_models.multiple_choice_question import MultipleChoiceQuestion
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.question_block_type import QuestionBlockType
from cms.cms_models.question_section_type_markdown import QuestionSectionTypeMarkdown
from cms.cms_models.question_section_type_markdown_end import (
    QuestionSectionTypeMarkdownEnd,
)
from cms.cms_models.question_section import QuestionSection
from cms.cms_models.answer import Answer
from cms.cms_models.answer_details import AnswerDetails
from cms.cms_models.question_list_rule import QuestionListRule

# email stuff
from cms.cms_models.address_book_contact import AddressBookContact
from cms.cms_models.address_book_contact_details import AddressBookContactDetails
from cms.cms_models.page_section_type_tree_description import (
    PageSectionTypeTreeDescription,
)
from cms.cms_models.email_question import EmailQuestion
from cms.cms_models.email_question_details import EmailQuestionDetails
from cms.cms_models.competency_type import CompetencyType
from cms.cms_models.question_situation import QuestionSituation
from cms.cms_models.situation_example_rating import SituationExampleRating
from cms.cms_models.situation_example_rating_details import (
    SituationExampleRatingDetails,
)

admin.site.register(TestPermissions)
admin.site.register(PageSection)
admin.site.register(PageSectionTypeMarkdown)
admin.site.register(ReducerControl)
admin.site.register(TestDefinition)
admin.site.register(TestSectionReducer)
admin.site.register(TestSection)
admin.site.register(TestSectionComponent)
admin.site.register(PageSectionTypeImageZoom)
admin.site.register(SectionComponentPage)
admin.site.register(PageSectionTypeSampleEmail)
admin.site.register(PageSectionTypeSampleEmailResponse)
admin.site.register(PageSectionTypeSampleTaskResponse)
admin.site.register(FileResourceDetails)

admin.site.register(NextSectionButtonTypeProceed)
admin.site.register(NextSectionButtonTypePopup)

admin.site.register(MultipleChoiceQuestion)
admin.site.register(NewQuestion)
admin.site.register(QuestionSectionTypeMarkdown)
admin.site.register(QuestionSectionTypeMarkdownEnd)
admin.site.register(QuestionSection)
admin.site.register(Answer)
admin.site.register(AnswerDetails)

admin.site.register(AddressBookContact)
admin.site.register(AddressBookContactDetails)
admin.site.register(PageSectionTypeTreeDescription)
admin.site.register(EmailQuestion)
admin.site.register(EmailQuestionDetails)
admin.site.register(QuestionBlockType)
admin.site.register(QuestionListRule)
admin.site.register(CompetencyType)
admin.site.register(QuestionSituation)
admin.site.register(SituationExampleRating)
admin.site.register(SituationExampleRatingDetails)
