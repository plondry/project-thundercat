from django.db import models
from backend.custom_models.language import Language
from cms.cms_models.address_book_contact import AddressBookContact

MAX_LENGTH = 150


class AddressBookContactDetails(models.Model):
    title = models.CharField(max_length=MAX_LENGTH)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    contact = models.ForeignKey(AddressBookContact, on_delete=models.DO_NOTHING)

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "{0}, id: {1}, contact id: {2}".format(
            self.title, self.id, self.contact.id
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Address Book Contact Details"
