from django.db import models
from cms.cms_models.page_section import PageSection
from backend.custom_models.language import Language


class PageSectionTypeSampleTaskResponse(models.Model):
    page_section = models.ForeignKey(
        PageSection, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1")
    response = models.TextField(null=True, blank=True)
    reason = models.TextField(null=True, blank=True)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "({0}) [{1}] {2}".format(
            self.page_section.section_component_page.en_title, self.language.ISO_Code_1, self.response[:20])
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Page Section Type Sample Task Response"
