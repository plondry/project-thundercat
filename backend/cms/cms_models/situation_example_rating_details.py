from django.db import models
from cms.cms_models.situation_example_rating import SituationExampleRating
from backend.custom_models.language import Language

MAX_CHAR_LEN = 100


class SituationExampleRatingDetails(models.Model):
    example_rating = models.ForeignKey(
        SituationExampleRating, on_delete=models.CASCADE
    )
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    example = models.TextField()
    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0} example_rating: {1}".format(
            self.id, self.example_rating[:20]
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Situation Example Rating Details"
