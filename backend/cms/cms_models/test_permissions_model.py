from django.db import models
from user_management.user_management_models.user_models import User
from cms.cms_models.test_definition import TestDefinition

##################################################################################
# TEST PERMISSIONS MODEL
##################################################################################


class TestPermissions(models.Model):
    username = models.ForeignKey(
        User, to_field="username", on_delete=models.DO_NOTHING, null=False
    )
    test = models.ForeignKey(TestDefinition, on_delete=models.DO_NOTHING, null=False)
    date_assigned = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    expiry_date = models.DateField(blank=False, null=False, default="2020-01-01")
    test_order_number = models.CharField(
        max_length=12, blank=False, null=False, default="default"
    )
    staffing_process_number = models.CharField(
        max_length=50, blank=False, null=False, default="default"
    )
    department_ministry_code = models.CharField(
        max_length=10, blank=False, null=False, default="default"
    )
    is_org = models.CharField(max_length=16, blank=False, null=False, default="default")
    is_ref = models.CharField(max_length=20, blank=False, null=False, default="default")
    billing_contact = models.CharField(
        max_length=180, blank=False, null=False, default="default"
    )
    billing_contact_info = models.CharField(
        max_length=255, blank=False, null=False, default="default"
    )

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0} - V{1} ({2})".format(
            self.test.test_code, self.test.version, self.username
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Permissions"
