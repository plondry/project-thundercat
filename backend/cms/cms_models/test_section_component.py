from django.db import models
from cms.cms_models.test_section import TestSection
from backend.custom_models.language import Language

MAX_CHAR_LEN = 100


class TestSectionComponent(models.Model):
    test_section = models.ManyToManyField(TestSection, blank=True)
    order = models.IntegerField()
    component_type = models.IntegerField()
    en_title = models.CharField(max_length=MAX_CHAR_LEN)
    fr_title = models.CharField(max_length=MAX_CHAR_LEN)
    language = models.ForeignKey(
        Language,
        on_delete=models.DO_NOTHING,
        to_field="ISO_Code_1",
        null=True,
        blank=True,
    )
    # shuffles all questions across all rules
    shuffle_all_questions = models.BooleanField(default=False)

    # provide user frendly names in Django Admin Console
    # this is getting a no test def error

    def __str__(self):
        ret = "id: {3} section: {0}, component: {1}, order: {2}".format(
            self.test_section.all().values("test_definition_id", "en_title"),
            self.component_type,
            self.order,
            self.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Test Section Component"
