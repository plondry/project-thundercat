from django.db import models
from cms.cms_models.test_section_component import TestSectionComponent
from cms.cms_models.question_block_type import QuestionBlockType

# this is a rule for a question list

# rules are (at this time) a simple way of querying X questions of type Y
# from the available questions.


class QuestionListRule(models.Model):
    test_section_component = models.ForeignKey(
        TestSectionComponent, on_delete=models.DO_NOTHING
    )
    # number of questions to collect
    number_of_questions = models.IntegerField()
    # type of question to collect
    question_block_type = models.ManyToManyField(QuestionBlockType)
    order = models.IntegerField(null=True, blank=True)
    # shuffle the questions in this rule (keeps testlets unshuffled)
    shuffle = models.BooleanField()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "id: {0})-> {1} questions of type: {2}".format(
            self.id, self.number_of_questions, self.question_block_type.name
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question List Rule"
