from django.db import models

MAX_CHAR_LEN = 20


class ReducerControl(models.Model):
    reducer = models.CharField(
        primary_key=True, max_length=MAX_CHAR_LEN)
