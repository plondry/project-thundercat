from django.db import models
from cms.cms_models.new_question import NewQuestion


class QuestionSection(models.Model):
    order = models.IntegerField()
    question = models.ForeignKey(
        NewQuestion, on_delete=models.DO_NOTHING)
    question_section_type = models.IntegerField()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "({0}) type: {1}, page section: {2}".format(
            self.question.id, self.question_section_type, self.order)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Question Section"
