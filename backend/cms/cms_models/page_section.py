from django.db import models
from cms.cms_models.section_component_page import SectionComponentPage


class PageSection(models.Model):
    order = models.IntegerField()
    page_section_type = models.IntegerField()
    section_component_page = models.ForeignKey(
        SectionComponentPage, on_delete=models.DO_NOTHING)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "({0}) type: {1}, section: {2}, page section: {3}".format(
            self.section_component_page.en_title, self.page_section_type, self.section_component_page.test_section_component.order, self.order)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Page Section"
