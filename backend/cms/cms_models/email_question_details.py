from django.db import models
from backend.custom_models.language import Language
from cms.cms_models.email_question import EmailQuestion


class EmailQuestionDetails(models.Model):
    email_question = models.ForeignKey(EmailQuestion, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    subject_field = models.TextField()
    date_field = models.TextField()
    body = models.TextField()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "(question id: {2}) from [{0}],  subject: {1}".format(
            self.email_question.question.test_section_component.test_section.all().values(
                "test_definition_id", "en_title"
            ),
            self.subject_field[:20],
            self.email_question.question.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Email Question Details"
