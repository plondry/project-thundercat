from django.db import models
from cms.cms_models.answer import Answer
from backend.custom_models.language import Language


class AnswerDetails(models.Model):
    answer = models.ForeignKey(Answer, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    content = models.TextField()

    # provide user frendly names in Django Admin Console

    def __str__(self):
        ret = "question id: {0}, answerid: {3}, [{1}] {2}".format(
            self.answer.question.id,
            self.language.ISO_Code_1,
            self.content[:20],
            self.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Answer Details"
