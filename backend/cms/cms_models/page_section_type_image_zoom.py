from django.db import models
from cms.cms_models.page_section import PageSection
from backend.custom_models.language import Language

MAX_CHAR_LEN = 250


class PageSectionTypeImageZoom(models.Model):
    page_section = models.ForeignKey(
        PageSection, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1")
    large_image = models.CharField(max_length=MAX_CHAR_LEN)
    small_image = models.CharField(max_length=MAX_CHAR_LEN)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "({0}) {1}".format(self.language,
                                 self.large_image.split("/")[-1])
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Page Section Type Zoom Image"
