from django.db import models
from cms.cms_models.test_section import TestSection
from backend.custom_models.language import Language

MAX_CHAR_LEN = 20


class NextSectionButtonTypeProceed(models.Model):
    test_section = models.ForeignKey(TestSection, on_delete=models.DO_NOTHING)
    language = models.ForeignKey(
        Language, on_delete=models.DO_NOTHING, to_field="ISO_Code_1"
    )
    button_text = models.TextField()

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "{0}: {1} ({2}) {3}".format(
            self.test_section.test_definition.test_code,
            self.test_section.en_title,
            self.language,
            self.button_text,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Next Section Button Type Proceed"
