from django.db import models
from cms.cms_models.reducer_control import ReducerControl
from cms.cms_models.test_section import TestSection


class TestSectionReducer(models.Model):
    reducer = models.ForeignKey(
        ReducerControl, on_delete=models.DO_NOTHING)
    test_section = models.ForeignKey(
        TestSection, on_delete=models.DO_NOTHING)
