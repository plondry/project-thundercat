from django.db import models
from cms.cms_models.test_section_component import TestSectionComponent

MAX_CHAR_LEN = 100


class SectionComponentPage(models.Model):
    order = models.IntegerField()
    en_title = models.CharField(max_length=MAX_CHAR_LEN)
    fr_title = models.CharField(max_length=MAX_CHAR_LEN)
    test_section_component = models.ForeignKey(
        TestSectionComponent, on_delete=models.DO_NOTHING)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "({0}) section: {1}, title: {2}, order: {3}".format(
            self.test_section_component.test_section.all().values("test_definition_id", "en_title", "id"), self.test_section_component.order, self.en_title, self.order)
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Section Component Page"
