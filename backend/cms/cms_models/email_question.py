from django.db import models
from backend.custom_models.language import Language
from cms.cms_models.new_question import NewQuestion
from cms.cms_models.address_book_contact import AddressBookContact
from cms.cms_models.competency_type import CompetencyType


class EmailQuestion(models.Model):
    question = models.ForeignKey(NewQuestion, on_delete=models.DO_NOTHING)
    email_id = models.IntegerField()
    from_field = models.ForeignKey(
        AddressBookContact,
        on_delete=models.DO_NOTHING,
        related_name="%(class)s_from_field",
    )
    to_field = models.ManyToManyField(
        AddressBookContact, related_name="%(class)s_to_field"
    )
    cc_field = models.ManyToManyField(
        AddressBookContact, related_name="%(class)s_cc_field", blank=True
    )
    competency_types = models.ManyToManyField(CompetencyType)

    # provide user frendly names in Django Admin Console
    def __str__(self):
        ret = "(question id: {1}) from [{0}]".format(
            self.question.test_section_component.test_section.all().values(
                "test_definition_id", "en_title"
            ),
            self.question.id,
        )
        return ret

    class Meta:
        # setting the model name in Django Admin Console
        verbose_name_plural = "Email Question"
