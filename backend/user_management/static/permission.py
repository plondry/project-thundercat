# This file is a collection of the valid permissions for user permissions.


class Permission:
    TEST_ADMINISTRATOR = 1
    SYSTEM_ADMINISTRATOR = 2
    PPC_ADMINISTRATOR = 3
    SCORER = 4
