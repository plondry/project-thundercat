from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.user_models import User
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.user_management_models.accommodations import Accommodations

# These imports help to auto discover the models
