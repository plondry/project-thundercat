from django.db import migrations, models


def update_permission_names(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    # get db alias
    db_alias = schema_editor.connection.alias

    try:
        # get is_test_administrator permission_id
        is_test_administrator = (
            custom_permissions.objects.using(db_alias)
            .filter(codename="is_test_administrator")
            .last()
        )

        # get is_etta permission_id
        is_etta = (
            custom_permissions.objects.using(db_alias).filter(codename="is_etta").last()
        )

        # get is_ppc permission_id
        is_ppc = (
            custom_permissions.objects.using(db_alias).filter(codename="is_ppc").last()
        )

        # update text
        is_test_administrator.en_name = "Test Administrator"
        is_test_administrator.fr_name = "Administrateur de test"
        is_test_administrator.save()

        is_etta.en_name = "System Administrator"
        is_etta.fr_name = "Administrateur du système"
        is_etta.save()

        is_ppc.en_name = "PPC administrator"
        is_ppc.fr_name = "Administrateur du CPP"
        is_ppc.save()

    # should only happen while testing (backend tests)
    except Exception as e:
        print("Exception (should only be displayed while testing): ", e)


def rollback(apps, schema_editor):
    # get models
    custom_permissions = apps.get_model("user_management_models", "CustomPermissions")
    # get db alias
    db_alias = schema_editor.connection.alias

    # get is_test_administrator permission_id
    is_test_administrator = (
        custom_permissions.objects.using(db_alias)
        .filter(codename="is_test_administrator")
        .last()
    )

    # get is_etta permission_id
    is_etta = (
        custom_permissions.objects.using(db_alias).filter(codename="is_etta").last()
    )

    # get is_ppc permission_id
    is_ppc = custom_permissions.objects.using(db_alias).filter(codename="is_ppc").last()

    # update text
    is_test_administrator.en_name = "Is Test Administrator"
    is_test_administrator.save()

    is_etta.en_name = "Is E-Testing and Test Administration"
    is_etta.save()

    is_ppc.en_name = "Is Personnel Psychology Center"
    is_ppc.save()


class Migration(migrations.Migration):

    dependencies = [("user_management_models", "0006_auto_20191223_0821")]

    operations = [migrations.RunPython(update_permission_names, rollback)]
