from operator import or_, and_
from functools import reduce
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions, status
from django.db import IntegrityError
from django.db.models import Q
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from user_management.user_management_models.user_models import User
from user_management.user_management_models.custom_user_permissions_model import (
    CustomUserPermissions,
)
from user_management.user_management_models.custom_permissions_model import (
    CustomPermissions,
)
from user_management.user_management_models.permission_request_model import (
    PermissionRequest,
)
from user_management.views.utils import (
    is_undefined,
    CustomPagination,
    get_user_info_from_jwt_token,
)
from user_management.serializers.permissions_serializers import (
    GetPermissionsSerializer,
    GetPendingPermissionsSerializer,
    GetActivePermissionsSerializer,
    GetUsersBasedOnSpecifiedPermissionSerializer,
)

# getting existing permissions
class GetPermissions(APIView):
    def get(self, request):
        return Response(get_existing_permissions(request))

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_existing_permissions(request):
    custom_permissions = CustomPermissions.objects.all()
    serialized = GetPermissionsSerializer(custom_permissions, many=True)
    return serialized.data


# checking if the specified user is a TA
class GetUserPermissions(APIView):
    def get(self, request):
        user_info = get_user_info_from_jwt_token(request)
        user = User.objects.get(username=user_info["username"])
        # get permissions of the specified user
        custom_user_permissions = CustomUserPermissions.objects.filter(user=user)
        serialized = GetActivePermissionsSerializer(custom_user_permissions, many=True)
        return Response(serialized.data)


# sending permission request
class SendPermissionRequest(APIView):
    def post(self, request):
        return send_permission_request(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def send_permission_request(request):
    user_info = get_user_info_from_jwt_token(request)
    goc_email = request.query_params.get("goc_email", None)
    pri_or_military_nbr = request.query_params.get("pri_or_military_nbr", None)
    supervisor = request.query_params.get("supervisor", None)
    supervisor_email = request.query_params.get("supervisor_email", None)
    rationale = request.query_params.get("rationale", None)
    permission_requested = request.query_params.get("permission_requested", None)
    # making sure that we have the needed parameters
    if is_undefined(goc_email):
        return Response(
            {"error": "no 'goc_email' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(pri_or_military_nbr):
        return Response(
            {"error": "no 'pri_or_military_nbr' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(supervisor):
        return Response(
            {"error": "no 'supervisor' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(supervisor_email):
        return Response(
            {"error": "no 'supervisor_email' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(rationale):
        return Response(
            {"error": "no 'rationale' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(permission_requested):
        return Response(
            {"error": "no 'permission_requested' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        # check if the requested permission is already associated to the username
        try:
            current_user_permissions = CustomUserPermissions.objects.get(
                user_id=user_info["username"], permission_id=permission_requested
            ).permission_id
            if current_user_permissions > 0:
                return Response(
                    {"error": "this permission is already associated to this username"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        except:
            pass
        response = PermissionRequest.objects.create(
            username=User.objects.get(username=user_info["username"]),
            goc_email=goc_email,
            pri_or_military_nbr=pri_or_military_nbr,
            supervisor=supervisor,
            supervisor_email=supervisor_email,
            rationale=rationale,
            permission_requested=CustomPermissions.objects.get(
                permission_id=permission_requested
            ),
        )
        response.save()
        return Response(status=status.HTTP_200_OK)
    except CustomPermissions.DoesNotExist:
        return Response(
            {"error": "this permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    except IntegrityError:
        return Response(
            {"error": "this permission has already been requested"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all pending permissions (permissions that have been requested, but not approved yet)
class GetPendingPermissions(APIView):
    def get(self, request):
        return get_pending_permissions()

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_pending_permissions():
    # return all existing pending permissions
    response = PermissionRequest.objects.all()

    serializer = GetPendingPermissionsSerializer(response, many=True)
    return Response(serializer.data)


# getting user pending permissions (permissions that have been requested, but not approved yet)
class GetUserPendingPermissions(APIView):
    def get(self, request):
        return get_user_pending_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_user_pending_permissions(request):
    user_info = get_user_info_from_jwt_token(request)
    # return only pending permissions of the specified user
    response = PermissionRequest.objects.filter(username=user_info["username"])

    serializer = GetPendingPermissionsSerializer(response, many=True)
    return Response(serializer.data)


# getting available permissions (for permission requests)
# available permissions: permissions that are not in user' permissions and/or in user' pending permissions
class GetAvailablePermissionsForRequest(APIView):
    def get(self, request):
        return get_available_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_available_permissions(request):
    user_info = get_user_info_from_jwt_token(request)
    # initializing available_permissions array
    available_permissions = []

    # getting all existing permissions, user' permissions and user' pending permissions
    existing_permissions = CustomPermissions.objects.all()
    current_user_permissions = CustomUserPermissions.objects.filter(
        Q(user_id=user_info["username"])
    )
    current_user_pending_permissions = PermissionRequest.objects.filter(
        Q(username=user_info["username"])
    )

    # checking what permission should be avaliable, meaning permissions that are not part of user' permissions and user' pending permissions
    # looping in existing permissions
    for existing_permissions in existing_permissions:
        # initializing push_permission flag
        push_permission = True
        # looping in user' permissions
        for user_permissions in current_user_permissions:
            # if user has the specified permission
            if existing_permissions.permission_id == user_permissions.permission_id:
                # don't push this permission to available permissions
                push_permission = False
        # looping in user' pending permissions
        for pending_user_permissions in current_user_pending_permissions:
            # if user has the specified pending permission
            if (
                existing_permissions.permission_id
                == pending_user_permissions.permission_requested_id
            ):
                # don't push this permission to available permissions
                push_permission = False

        # if no push_permission flag has been updated to False, that means the specified permission is available for permission request
        if push_permission:
            available_permissions.append(existing_permissions.permission_id)
    # looking for permission ids that are in available_permissions array
    response = CustomPermissions.objects.filter(permission_id__in=available_permissions)
    # using GetPermissionsSerializer to get all the needed data
    serializer = GetPermissionsSerializer(response, many=True)
    return Response(serializer.data)


# granting specified permission to specified user
class GrantPermission(APIView):
    def post(self, request):
        return grant_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def grant_permission(request):
    username = request.query_params.get("username", None)
    permission_id = request.query_params.get("permission_id", None)
    # making sure that we have the needed parameters
    if is_undefined(username):
        return Response(
            {"error": "no 'username' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(permission_id):
        return Response(
            {"error": "no 'permission_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    else:
        try:
            # creating new custom permissions object
            response = CustomUserPermissions.objects.create(
                permission_id=CustomPermissions.objects.get(
                    permission_id=permission_id
                ).permission_id,
                user_id=User.objects.get(username=username).username,
                goc_email=PermissionRequest.objects.get(
                    username=username, permission_requested_id=permission_id
                ).goc_email,
                pri_or_military_nbr=PermissionRequest.objects.get(
                    username=username, permission_requested_id=permission_id
                ).pri_or_military_nbr,
                supervisor=PermissionRequest.objects.get(
                    username=username, permission_requested_id=permission_id
                ).supervisor,
                supervisor_email=PermissionRequest.objects.get(
                    username=username, permission_requested_id=permission_id
                ).supervisor_email,
                rationale=PermissionRequest.objects.get(
                    username=username, permission_requested_id=permission_id
                ).rationale,
            )
            # saving new object
            response.save()

            # deleting associated permission request
            pending_permission_id = PermissionRequest.objects.get(
                username=username, permission_requested_id=permission_id
            )
            # deleting permission object
            pending_permission_id.delete()
            return Response(status=status.HTTP_200_OK)
        except CustomPermissions.DoesNotExist:
            return Response(
                {"error": "the specified permission does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except User.DoesNotExist:
            return Response(
                {"error": "the specified username does not exist"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except PermissionRequest.DoesNotExist:
            return Response(
                {"error": "there is no pending permission associated to this request"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except IntegrityError:
            return Response(
                {"error": "this permission is already associated to this username"},
                status=status.HTTP_400_BAD_REQUEST,
            )


# denying requested permission
class DenyPermission(APIView):
    def post(self, request):
        return deny_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def deny_permission(request):
    permission_request_id = request.query_params.get("permission_request_id", None)
    # making sure that we have the needed parameters
    if is_undefined(permission_request_id):
        return Response(
            {"error": "no 'permission_request_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    try:
        # getting specified permission
        permission = PermissionRequest.objects.get(
            permission_request_id=permission_request_id
        )
        # deleting the permission from permission request table
        permission.delete()
        return Response(status=status.HTTP_200_OK)
    except PermissionRequest.DoesNotExist:
        return Response(
            {"error": "the specified permission request ID does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting active permissions
class GetActivePermissions(APIView):
    def get(self, request):
        return get_active_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_active_permissions(request):
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()
    # getting all active permissions
    active_permissions = CustomUserPermissions.objects.all()

    # ordering active permissions by last_name
    # getting serialized active permissions data
    serialized_active_permissions_data = GetActivePermissionsSerializer(
        active_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_active_permissions = sorted(
        serialized_active_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    user_permission_ids_array = []
    # looping in ordered active permissions
    for i in ordered_active_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        user_permission_ids_array.insert(
            len(user_permission_ids_array), i["user_permission_id"]
        )

    # sorting active permissions queryset based on ordered (by last name) user permission ids
    new_active_permissions = list(
        CustomUserPermissions.objects.filter(
            user_permission_id__in=user_permission_ids_array
        )
    )
    new_active_permissions.sort(
        key=lambda t: user_permission_ids_array.index(t.user_permission_id)
    )

    # getting page results based on queryset (new_active_permissions)
    page = paginator.paginate_queryset(new_active_permissions, request)
    # serializing the data
    serialized = GetActivePermissionsSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)


# getting found active permissions (triggered by a search)
class GetFoundActivePermissions(APIView):
    def get(self, request):
        return get_found_active_permissions(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_found_active_permissions(request):
    keyword = request.query_params.get("keyword", None)
    current_language = request.query_params.get("current_language", None)
    page = request.query_params.get("page", None)
    page_size = request.query_params.get("page_size", None)
    # making sure that we have the needed parameters
    if is_undefined(keyword):
        return Response(
            {"error": "no 'keyword' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(current_language):
        return Response(
            {"error": "no 'current_language' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(page):
        return Response(
            {"error": "no 'page' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(page_size):
        return Response(
            {"error": "no 'page_size' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )

    # pagination class
    pagination_class = CustomPagination
    paginator = pagination_class()

    # search while interface is in English
    if current_language == "en":
        # getting all permission ids based on keyword that is matching with en_name
        permission_ids = CustomPermissions.objects.filter(Q(en_name__icontains=keyword))
    # search while interface is in French
    else:
        # getting all permission ids based on keyword that is matching with fr_name
        permission_ids = CustomPermissions.objects.filter(Q(fr_name__icontains=keyword))

    # since there is a comma between last name and first name in active permissions table, if the user put
    # a comma in one of the search keyword words, remove it
    keyword_without_comma = keyword.replace(",", "")

    # splitting keyword string
    split_keyword = keyword_without_comma.split()
    # if keyword contains more than one word
    if len(split_keyword) > 1:
        # getting all user ids based on keyword that is matching with first_name and/or last_name and/or username
        user_ids = User.objects.filter(
            reduce(
                and_,
                [
                    Q(first_name__icontains=splitted_keyword)
                    | Q(last_name__icontains=splitted_keyword)
                    | Q(username__icontains=splitted_keyword)
                    for splitted_keyword in split_keyword
                ],
            )
        )
    # keyword is empty or contains only one word
    else:
        # getting all user ids based on keyword that is matching with first_name and/or last_name and/or username
        user_ids = User.objects.filter(
            Q(first_name__icontains=keyword)
            | Q(last_name__icontains=keyword)
            | Q(username__icontains=keyword)
        )

    # if at least one result has been found based on the keyword provided
    if permission_ids.exists() or user_ids.exists():

        # if there are matches in both (permissions and names)
        if permission_ids.exists() and user_ids.exists():
            # getting all active permissions where there are matches with provided keyword and names
            found_active_permissions_names = CustomUserPermissions.objects.filter(
                reduce(or_, [Q(user_id=user.username) for user in user_ids])
            )
            # getting all active permissions where there are matches with provided keyword and permissions
            found_active_permissions_permissions = CustomUserPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(permission_id=permission.permission_id)
                        for permission in permission_ids
                    ],
                )
            )
            found_active_permissions = found_active_permissions_names.union(
                found_active_permissions_permissions
            )

        # if there are matches only in names
        elif user_ids.exists():
            # getting all active permissions where there are matches with provided keyword and names
            found_active_permissions = CustomUserPermissions.objects.filter(
                reduce(or_, [Q(user_id=user.username) for user in user_ids])
            )

        # if there are matches only in permissions
        elif permission_ids.exists():
            # getting all active permissions where there are matches with provided keyword and permissions
            found_active_permissions = CustomUserPermissions.objects.filter(
                reduce(
                    or_,
                    [
                        Q(permission_id=permission.permission_id)
                        for permission in permission_ids
                    ],
                )
            )

    # there are no results found based on the keyword provided
    else:
        return Response({"no results found"}, status=status.HTTP_200_OK)

    # ordering found active permissions by last_name
    # getting serialized active permissions data
    serialized_found_active_permissions_data = GetActivePermissionsSerializer(
        found_active_permissions, many=True
    ).data
    # getting sorted serialized data by last_name (ascending)
    ordered_found_active_permissions = sorted(
        serialized_found_active_permissions_data,
        key=lambda k: k["last_name"].lower(),
        reverse=False,
    )
    # initializing user permissions ids array (the user_permission_id order here represents the order)
    user_permission_ids_array = []
    # looping in ordered active permissions
    for i in ordered_found_active_permissions:
        # inserting user permissions ids (ordered respectively by last name) in an array
        user_permission_ids_array.insert(
            len(user_permission_ids_array), i["user_permission_id"]
        )

    # sorting active permissions queryset based on ordered (by last name) user permission ids
    new_found_active_permissions = list(
        CustomUserPermissions.objects.filter(
            user_permission_id__in=user_permission_ids_array
        )
    )
    new_found_active_permissions.sort(
        key=lambda t: user_permission_ids_array.index(t.user_permission_id)
    )

    # getting page results based on queryset (new_found_active_permissions)
    page = paginator.paginate_queryset(new_found_active_permissions, request)
    # serializing the data
    serialized = GetActivePermissionsSerializer(page, many=True)
    # retuning final data using pagination library
    return paginator.get_paginated_response(serialized.data)


# updating specified active permission
class UpdateActivePermissionData(APIView):
    def post(self, request):
        return update_active_permission_data(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def update_active_permission_data(request):
    user_permission_id = request.query_params.get("user_permission_id", None)
    goc_email = request.query_params.get("goc_email", None)
    supervisor = request.query_params.get("supervisor", None)
    supervisor_email = request.query_params.get("supervisor_email", None)
    # making sure that we have the needed parameters
    if is_undefined(user_permission_id):
        return Response(
            {"error": "no 'user_permission_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    if is_undefined(goc_email):
        return Response(
            {"error": "no 'goc_email' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(supervisor):
        return Response(
            {"error": "no 'supervisor' parameter"}, status=status.HTTP_400_BAD_REQUEST
        )
    if is_undefined(supervisor_email):
        return Response(
            {"error": "no 'supervisor_email' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )
    try:
        # getting specified user permission
        user_permission = CustomUserPermissions.objects.get(
            user_permission_id=user_permission_id
        )

        # validating goc email (in case a manual update is done directly in the database)
        try:
            validate_email(goc_email)
        except ValidationError:
            return Response(
                {"error": "The goc email is invalid"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # validating supervisor email (in case a manual update is done directly in the database)
        try:
            validate_email(supervisor_email)
        except ValidationError:
            return Response(
                {"error": "The supervisor email is invalid"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # updating needed fields
        user_permission.goc_email = goc_email
        user_permission.supervisor = supervisor
        user_permission.supervisor_email = supervisor_email

        # saving new data
        user_permission.save()

        return Response(status=status.HTTP_200_OK)
    except CustomUserPermissions.DoesNotExist:
        return Response(
            {"error": "the specified user permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# deleting specified user permission
class DeleteActivePermission(APIView):
    def post(self, request):
        return delete_active_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def delete_active_permission(request):
    user_permission_id = request.query_params.get("user_permission_id", None)
    # making sure that we have the needed parameters
    if is_undefined(user_permission_id):
        return Response(
            {"error": "no 'user_permission_id' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        # getting specified user permission
        user_permission = CustomUserPermissions.objects.get(
            user_permission_id=user_permission_id
        )

        # deleting specified user permission
        user_permission.delete()
        return Response(status=status.HTTP_200_OK)
    except CustomUserPermissions.DoesNotExist:
        return Response(
            {"error": "the specified user permission does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )


# getting all users that have specified permission
class GetUsersBasedOnSpecifiedPermission(APIView):
    def get(self, request):
        return get_users_based_on_specified_permission(request)

    def get_permissions(self):
        return [permissions.IsAuthenticated()]


def get_users_based_on_specified_permission(request):
    permission_codename = request.query_params.get("permission_codename", None)
    # making sure that we have the needed parameters
    if is_undefined(permission_codename):
        return Response(
            {"error": "no 'permission_codename' parameter"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        permission_id = CustomPermissions.objects.get(
            codename=permission_codename
        ).permission_id
    except CustomPermissions.DoesNotExist:
        return Response(
            {"error": "the specified permission codename does not exist"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    users = CustomUserPermissions.objects.filter(permission_id=permission_id)
    serializer = GetUsersBasedOnSpecifiedPermissionSerializer(users, many=True)

    return Response(serializer.data)
