import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver


class CAT643(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-643"""

    # expected English text for each element
    MAIN_HEADER = "Welcome to the Candidate Assessment Tool"
    MAIN_PARAGRAPH = "This website is used to assess candidates for positions in the federal public service. To access your tests, you must login below. If you do not have an account, you may register for one using your email address."
    LOGIN_TEXT = "Login"
    CREATE_ACCOUNT_TAB = "Create an account"
    FORM_PARAGRAPH = "An account is required to proceed further. To log in, enter your credentials below."
    EMAIL_LABEL = "Email Address:"
    PASSWORD_LABEL = "Password:"

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()

    def test_changing_language_works(self):
        dvr = self.driver
        login = Login(self.driver)

        # ensure we are on English to start
        if login.get_language() == "fr":
            login.toggle_language()

        # elements whose language we will check
        main_header = dvr.find_element(By.CSS_SELECTOR, "#main-content > h1")
        main_paragraph = dvr.find_element(By.CSS_SELECTOR, "#main-content > p")
        login_tab = dvr.find_element(By.ID, "login-tabs-tab-login")
        create_account_tab = dvr.find_element(By.ID, "login-tabs-tab-account")
        form_header = dvr.find_element(By.CSS_SELECTOR, ".tab-content h2")
        form_paragraph = dvr.find_element(By.CSS_SELECTOR, ".tab-content span")
        email_label = dvr.find_element(By.CSS_SELECTOR, "label[for='username']")
        password_label = dvr.find_element(By.CSS_SELECTOR, "label[for='password']")
        login_button = dvr.find_element(By.CSS_SELECTOR, "input[type='submit']")

        # verify text is correct in English
        self.assertEqual(main_header.text, self.MAIN_HEADER)
        self.assertEqual(main_paragraph.text, self.MAIN_PARAGRAPH)
        self.assertEqual(login_tab.text, self.LOGIN_TEXT)
        self.assertEqual(create_account_tab.text, self.CREATE_ACCOUNT_TAB)
        self.assertEqual(form_header.text, self.LOGIN_TEXT)
        self.assertEqual(form_paragraph.text, self.FORM_PARAGRAPH)
        self.assertEqual(email_label.text, self.EMAIL_LABEL)
        self.assertEqual(password_label.text, self.PASSWORD_LABEL)
        self.assertEqual(login_button.get_attribute("value"), self.LOGIN_TEXT)

        # change to French and verify text has changed
        # (i.e., current text should be different from expected English text)
        login.toggle_language()
        self.assertNotEqual(main_header.text, self.MAIN_HEADER)
        self.assertNotEqual(main_paragraph.text, self.MAIN_PARAGRAPH)
        self.assertNotEqual(login_tab.text, self.LOGIN_TEXT)
        self.assertNotEqual(create_account_tab.text, self.CREATE_ACCOUNT_TAB)
        self.assertNotEqual(form_header.text, self.LOGIN_TEXT)
        self.assertNotEqual(form_paragraph.text, self.FORM_PARAGRAPH)
        self.assertNotEqual(email_label.text, self.EMAIL_LABEL)
        self.assertNotEqual(password_label.text, self.PASSWORD_LABEL)
        self.assertNotEqual(login_button.get_attribute("value"), self.LOGIN_TEXT)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
