import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], ".."))

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from common.pages.login import Login
from common.web_driver_util import init_web_driver


class CAT617(unittest.TestCase):
    """https://cfp-psc.atlassian.net/browse/CAT-617"""

    def setUp(self):
        self.driver = init_web_driver()
        self.driver.maximize_window()

    def test_splash_screen_shows(self):
        Login(self.driver, auto_select_language=False)

        # verify splash screen text exists
        page_text = self.driver.page_source
        self.assertIn("Candidate Assessment Tool", page_text)
        self.assertIn("Outil d'évaluation des candidats", page_text)

        # verify exactly two buttons exist
        self.assertEqual(len(self.driver.find_elements(By.CSS_SELECTOR, "button")), 2)

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
