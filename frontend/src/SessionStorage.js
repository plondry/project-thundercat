export const ACTION = {
  SET: "SET",
  GET: "GET",
  REMOVE: "REMOVE",
  CLEAR: "CLEAR"
};

export const ITEM = {
  STATE: "state",
  CAT_LANGUAGE: "catLanguage",
  AUTH_TOKEN: "authToken",
  TEST_ACTIVE: "testActive"
};

function SessionStorage(action, item, value) {
  // SET
  if (action === ACTION.SET) {
    return sessionStorage.setItem(item, value);
    // GET
  } else if (action === ACTION.GET) {
    return sessionStorage.getItem(item);
    // REMOVE
  } else if (action === ACTION.REMOVE) {
    return sessionStorage.removeItem(item);
    // CLEAR
  } else if (action === ACTION.CLEAR) {
    return sessionStorage.clear();
  }
}

export default SessionStorage;
