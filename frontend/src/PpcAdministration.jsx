import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { styles as SystemAdministrationStyles } from "./components/etta/SystemAdministration";
import ContentContainer from "./components/commons/ContentContainer";
import SideNavigation from "./components/eMIB/SideNavigation";
import Reports from "./components/ppc/Reports";

class PpcAdministration extends Component {
  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getPpcAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.ppcAdministration.sideNavItems.reports,
        body: <Reports />
      }
    ];
  };

  render() {
    const specs = this.getPpcAdministrationSections();
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.ppcAdministration}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={SystemAdministrationStyles.header}
              tabIndex={0}
              aria-labelledby="user-welcome-message"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.ppcAdministration.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                <div>
                  <label style={SystemAdministrationStyles.sectionContainerLabel}>
                    {LOCALIZE.ppcAdministration.containerLabel}
                    <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={SystemAdministrationStyles.sectionContainer}>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={SystemAdministrationStyles.tabContainer}
                  tabContentStyle={SystemAdministrationStyles.tabContent}
                  navStyle={SystemAdministrationStyles.nav}
                  bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { PpcAdministration as unconnectedPpcAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PpcAdministration);
