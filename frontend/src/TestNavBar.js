import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import Settings from "./components/commons/Settings";
import Translation from "./components/commons/Translation";
import LOCALIZE from "./text_resources";
import psc_logo_light_en from "./images/psc_logo_light_en.png";
import psc_logo_light_fr from "./images/psc_logo_light_fr.png";
import { Navbar, Nav } from "react-bootstrap";
import QuitTest from "./components/commons/QuitTest";
import SelectLanguage from "./SelectLanguage";
import { LANGUAGES } from "./modules/LocalizeRedux";
import { PATH } from "./App";

class TestNavBar extends Component {
  static propTypes = {
    quitTestHidden: PropTypes.bool,
    // Props from Redux
    currentLanguage: PropTypes.string,
    isTestActive: PropTypes.bool.isRequired,
    authenticateAction: PropTypes.func,
    logoutAction: PropTypes.func,
    handleQuitTest: PropTypes.func,
    testNameId: PropTypes.string
  };

  state = {
    setFocusOnQuitTestButton: false
  };

  render() {
    // Determine if user has already selected a language.
    const isLanguageSelected = this.props.currentLanguage !== "";
    return (
      <div>
        <a href="#main-content" className="visually-hidden">
          {LOCALIZE.mainTabs.skipToMain}
        </a>
        {!isLanguageSelected && <SelectLanguage />}
        {isLanguageSelected && window.location.pathname.indexOf(PATH.sampleTests) === -1 && (
          <div>
            <div>
              <Navbar bg="dark" variant="dark" id="unit-test-active-test-navbar">
                <Navbar.Brand>
                  <img
                    alt=""
                    src={
                      this.props.currentLanguage === LANGUAGES.french
                        ? psc_logo_light_fr
                        : psc_logo_light_en
                    }
                    width="370px"
                    height="27.75px"
                    className="d-inline-block align-top"
                  />
                </Navbar.Brand>
                <Nav className="mr-auto" />
                {!this.props.quitTestHidden && (
                  <QuitTest
                    id="unit-test-quit-test-navbar-button"
                    setFocusOnQuitTestButton={this.state.setFocusOnQuitTestButton}
                    handleQuitTest={this.props.handleQuitTest}
                  />
                )}
                <Settings
                  isTestActive={this.props.isTestActive}
                  id="unit-test-settings-navbar-button"
                />
                <Translation
                  isTestActive={this.props.isTestActive}
                  id="unit-test-translation-navbar-button"
                />
              </Navbar>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export { TestNavBar as UnconnectedTestNavBar };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isTestActive: state.testStatus.isTestActive,
    authenticated: state.login.authenticated,
    testNameId: state.testStatus.currentTestId
  };
};

export default connect(mapStateToProps)(TestNavBar);
