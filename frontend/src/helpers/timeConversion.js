// source: https://www.w3resource.com/javascript-exercises/javascript-date-exercise-13.php
// converting minutes to hours and minutes
function minutesToHours(number) {
  const hours = number / 60;
  const rhours = Math.floor(hours);
  const minutes = (hours - rhours) * 60;
  const rminutes = Math.round(minutes);
  return { hours: rhours, minutes: rminutes };
}

export function hoursMinutesToMinutes(hours, minutes) {
  let result = 0;
  result = minutes + hours * 60;
  return result;
}

// getting formatted time (in hours and minutes)
export function getTimeInHoursMinutes(minutes) {
  // getting time in hours and minutes
  let convertedTime = minutesToHours(minutes);
  // retuning time in format (hours : minutes): 00 : 00
  return {
    hours: Number(convertedTime.hours),
    formattedHours: getFormattedTime(convertedTime.hours),
    minutes: Number(convertedTime.minutes),
    formattedMinutes: getFormattedTime(convertedTime.minutes)
  };
}

export function getFormattedTime(time) {
  // initializing formattedTime
  let formattedTime = time;
  // adding zero in front of the hours number if less than 10 (to have something like 01:30 not 1:30)
  if (time < 10) {
    formattedTime = `0${time}`;
  }
  // returning formattedTime
  return `${formattedTime}`;
}

// getting time in seconds between two dates
// source: https://stackoverflow.com/questions/2024198/how-many-seconds-between-two-dates
// date1 and date2 must be strings, to avoid unexpected object format errors (example: new Date(Date.now()).toString())
export function getTimeInSecondsBetweenTwoDates(date1, date2) {
  const dif = new Date(date1).getTime() - new Date(date2).getTime();
  const SecondsFromD1ToD2 = dif / 1000;
  const time = Math.round(Math.abs(SecondsFromD1ToD2));
  // returning rounded time in seconds
  return time;
}

export default minutesToHours;
