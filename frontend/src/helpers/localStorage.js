import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

export function clearTestLocalStorage() {
  SessionStorage(ACTION.CLEAR);
}

// checking if there is an active test based on the local storage
export function isTestActive() {
  if (SessionStorage(ACTION.GET, ITEM.TEST_ACTIVE) === "1") {
    return true;
  } else {
    return false;
  }
}
