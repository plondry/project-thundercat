// populate custom date year options (from current year to current year + 5)
function populateCustomFiveYearsFutureDateOptions() {
  let yearOptionsArray = [];
  const currentYear = new Date().getFullYear();
  // loop from current year to current year + 5 years
  for (let i = currentYear; i <= currentYear + 5; i++) {
    // push each value in yearOptionsArray
    yearOptionsArray.push({ value: i, label: `${i}` });
  }
  // returning array
  return yearOptionsArray;
}

export default populateCustomFiveYearsFutureDateOptions;
