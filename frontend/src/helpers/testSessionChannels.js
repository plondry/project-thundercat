export const CHANNELS_PATH = {
  testAdministratorRoom: "/oec-cat/api/test-administrator-room/"
};

export const CHANNELS_ACTION = {
  approveCandidate: "APPROVE_CANDIDATE", // when the TA approves a candidate
  unAssignCandidate: "UNASSIGN_CANDIDATE", // when the TA un-assign a candidate's test
  lockCandidate: "LOCK_CANDIDATE", // when the TA locks a candidate
  unlockCandidate: "UNLOCK_CANDIDATE", // when the TA unlocks a candidate
  lockAllCandidates: "LOCK_ALL_CANDIDATES", // when the TA locks all candidates
  unlockAllCandidates: "UNLOCK_ALL_CANDIDATES", // when the TA unlocks all candidates
  pauseCandidate: "PAUSE_CANDIDATE", // when the TA pauses a candidate
  unpauseCandidate: "UNPAUSE_CANDIDATE", // when the TA unpauses a candidate
  readyToActive: "READY_TO_ACTIVE", // when candidate hits view test (updatade test status from READY to ACTIVE)
  pauseTestTimeout: "PAUSE_TEST_TIMEOUT", // when candidate's pause test time gets to zero (updatade test status from PAUSED to READY or ACTIVE)
  checkin: "CHECKIN", // when candidate checks in
  finishOrQuit: "FINISH_OR_QUIT" // when candidate finishes or quits the test
};

// path parameters:
//  - jwt_token
//  - test_access_code
function candidateSideTestSessionChannels(path, username) {
  const loc = window.location;
  let wsStart = "ws://";
  if (loc.protocol === "https") {
    wsStart = "wss://";
  }
  const endpoint = wsStart + loc.host + path;
  const socket = new WebSocket(endpoint);

  // uncomment for debugging
  //====================================
  // socket.onopen = function(e) {
  //   console.log("open", e);
  // };
  // socket.onclose = function(e) {
  //   console.log("close", e);
  // };
  //====================================

  return socket;
}

// path parameters:
//  - jwt_token
//  - test_access_code
export function testAdministratorSideTestSessionChannels(path) {
  const loc = window.location;
  let wsStart = "ws://";
  if (loc.protocol === "https") {
    wsStart = "wss://";
  }
  const endpoint = wsStart + loc.host + path;
  const socket = new WebSocket(endpoint);

  // uncomment for debugging
  //====================================
  // socket.onopen = function(e) {
  //   console.log("open", e);
  // };
  // socket.onerror = function(e) {
  //   console.log("error", e);
  // };
  // socket.onclose = function(e) {
  //   console.log("close", e);
  // };
  //====================================

  return socket;
}

export async function getCurrentSocket(socketsArray, testAccessCode) {
  // initializing currentSocket
  let currentSocket = null;
  // looping in sockets array
  for (let i = 0; i < socketsArray.length; i++) {
    // current socket url contains specified test access code
    if (testAccessCode === socketsArray[i].url.split("/")[7]) {
      // saving socket as current socket
      currentSocket = socketsArray[i];
    }
  }
  // waiting for currentSocket to connect
  const sleep = ms => new Promise(res => setTimeout(res, ms));
  while (currentSocket.readyState !== currentSocket.OPEN) {
    await sleep(1);
  }
  // once connected, return it
  return currentSocket;
}

export async function validateSocketConnection(path) {
  const loc = window.location;
  let wsStart = "ws://";
  if (loc.protocol === "https") {
    wsStart = "wss://";
  }

  const endpoint = wsStart + loc.host + path;
  const socket = new WebSocket(endpoint);

  // waiting for socket to connect (connection successful or not)
  const sleep = ms => new Promise(res => setTimeout(res, ms));
  while (socket.readyState !== socket.OPEN && socket.readyState !== socket.CLOSED) {
    await sleep(1);
  }

  // connection successful
  if (socket.readyState === socket.OPEN) {
    // closing connection and retuning true (valid connection)
    socket.close();
    return true;
    // connection error
  } else {
    // closing connection and retuning false (invalid connection)
    socket.close();
    return false;
  }
}

export default candidateSideTestSessionChannels;
