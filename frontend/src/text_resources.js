import LocalizedStrings from "react-localization";

const LOCALIZE = new LocalizedStrings({
  en: {
    // Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Home",
      homeTabTitleAuthenticated: "Home",
      dashboardTabTitle: "Dashboard",
      sampleTest: "Sample e-MIB",
      sampleTests: "Sample Tests",
      statusTabTitle: "Status",
      psc: "Public Service Commission of Canada",
      canada: "Government of Canada",
      skipToMain: "Skip to main content",
      menu: "MENU"
    },

    //HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "CAT - Sample Tests",
      eMIBOverview: "e-MIB Overview",
      eMIB: "e-MIB Assessment",
      uitTest: "UIT Assessment",
      status: "CAT - System Status",
      home: "CAT - Home",
      homeWithError: "Error - CAT - Home",
      profile: "CAT - Profile",
      systemAdministration: "CAT - System Administration",
      testAdministration: "CAT - Test Sessions",
      ppcAdministration: "CAT - PPC Administration",
      websocketConnectionError: "CAT - Websocket Connection Error"
    },

    accommodations: {
      notificationPopup: {
        title: "Accessibility Display Settings",
        description1: "The display settings currently in effect have been saved.",
        description2: "When you log in, these settings will be automatically applied."
      },
      defaultFonts: {
        fontSize: "16px (default)",
        fontFamily: "Nunito Sans (Default)"
      }
    },

    //authentication
    authentication: {
      login: {
        title: "Login",
        content: {
          title: "Login",
          description:
            "An account is required to proceed further. To log in, enter your credentials below.",
          inputs: {
            emailTitle: "Email Address:",
            passwordTitle: "Password:"
          }
        },
        button: "Login",
        invalidCredentials: "Invalid credentials!",
        passwordFieldSelected: "Password field selected."
      },
      createAccount: {
        title: "Create an account",
        content: {
          title: "Create an account",
          description:
            "An account is required to proceed further. To create an account, fill out the following.",
          inputs: {
            valid: "valid",
            firstNameTitle: "First name:",
            firstNameError: "Must be a valid first name",
            lastNameTitle: "Last name:",
            lastNameError: "Must be a valid last name",
            dobDayTitle: "Date of birth:",
            dobError: "Must be a valid date",
            psrsAppIdTitle: "PSRS Applicant ID",
            psrsAppIdError: "Must be a valid PSRS Applicant ID",
            emailTitle: "Email address:",
            emailError: "Must be a valid email address",
            priOrMilitaryNbrTitle: "PRI or Service number (if applicable):",
            priOrMilitaryNbrError: "Must be a valid PRI or Service number",
            passwordTitle: "Password:",
            passwordErrors: {
              description: "Your password must satisfy the following:",
              upperCase: "At least one uppercase letter",
              lowerCase: "At least one lowercase letter",
              digit: "At least one digit",
              specialCharacter: "At least one special character (#?!@$%^&*-)",
              length: "Minimum of 8 characters and maximum of 15"
            },
            passwordConfirmationTitle: "Confirm password:",
            passwordConfirmationError: "Your password confirmation must match the Password"
          }
        },
        privacyNotice:
          "I have read and agreed to how the Public Service Commission collects, uses and discloses personal information, as set out in the {0}.",
        privacyNoticeLink: "Privacy Notice",
        privacyNoticeError: "You must accept the privacy notice by clicking on the checkbox",
        button: "Create account",
        accountAlreadyExistsError: "An account is already associated to this email address",
        passwordTooCommonError: "This password is too common",
        passwordTooSimilarToUsernameError: "The password is too similar to the username",
        passwordTooSimilarToFirstNameError: "The password is too similar to the first name",
        passwordTooSimilarToLastNameError: "The password is too similar to the last name",
        passwordTooSimilarToEmailError: "The password is too similar to the email",
        privacyNoticeDialog: {
          title: "Privacy and Security",
          privacyNoticeStatement: "Privacy Notice Statement – e-MIB Pilot Study",
          privacyParagraph1:
            "The personal information, including test results and supervisor ratings, pertaining to this research project will be used by the Public Service Commission of Canada (PSC) for research, analysis and test development purposes.  It may be possible to use the results for future staffing related matters with your consent. All information is collected under the authority of sections 11, 30, 35 and 36 of the Public Service Employment Act in accordance with the Privacy Act. The PSC is committed to protecting the privacy rights of individuals.",
          publicServiceEmploymentActLink: "Public Service Employment Act",
          privacyActLink: "Privacy Act",
          privacyParagraph2:
            "“Personal information” is any information, recorded in any form, about an identifiable individual.",
          privacyCommissionerLink: "Privacy Commissioner of Canada",
          privacyParagraph3:
            "The use and collection of test results is defined in Personal Information Bank numbers PSC-PPU-025. Personal information related to research, analysis and test development is retained for 10 years and then destroyed. Anonymous test responses and demographic information are retained in electronic format indefinitely.",
          privacyParagraph4:
            "Participation is voluntary. However, should you chose not to provide your personal information, you will not be able to participate in this study.",
          privacyParagraph5:
            "Individuals have the right to access to their personal information and to request corrections where the individual believes there is an error or omission. Individuals may contact the PSC’s Access to Information and Privacy Office to request access to your personal information and to request corrections.",
          privacyParagraph6:
            "Personal information collected by the PSC is protected from disclosure to unauthorized persons and/or departments and agencies subject to the provisions of the Privacy Act. However personal information may be disclosed without your consent in certain specific circumstances, per section 8 of the Privacy Act. ",
          accessToInformationLink: "Access to Information and Privacy Protection Division",
          privacyParagraph7:
            "Individuals have the right to file a complaint with the Privacy Commissioner of Canada regarding the department’s handling of their personal information.",
          infoSourceChapterLink: "Info Source Chapter",
          reproductionTitle: "Unauthorized Reproduction or Disclosure of Test Content",
          reproductionWarning:
            "This test and its contents are designated Protected B. Reproduction or recording in any form of the content of this test is strictly forbidden, and all material related to the test, including rough notes, must remain with the test administrator following the administration of the test. Any unauthorized reproduction, recording and/or disclosure of test content is in contravention of the Government Security Policy and the use of such improperly obtained or transmitted information could be found to contravene the provisions of the Public Service Employment Act (PSEA). Parties involved in the disclosure of or improper use of protected test content may be the subject of an investigation under the PSEA, where a finding of fraud may be punishable on summary conviction or may be referred to the Royal Canadian Mounted Police.",
          cheatingTitle: "Cheating",
          cheatingWarning:
            "Please note that suspected cheating will in all cases be referred to the responsible manager and the Personnel Psychology Centre for action. Suspected cheating may result in the invalidation of test results and may be the subject of an investigation under the PSEA, where a finding of fraud may be punishable on summary conviction or may be referred to the Royal Canadian Mounted Police."
        }
      }
    },

    //Menu Items
    menu: {
      scorer: "Scorer",
      etta: "System Administrator",
      ppc: "PPC Administrator",
      ta: "Test Administrator",
      testBuilder: "Test Builder",
      checkIn: "Check-in",
      profile: "My Profile",
      incidentReport: "Incident Reports",
      MyTests: "My Tests",
      ContactUs: "Contact Us",
      logout: "Logout"
    },

    //Token Expired Popup Box
    tokenExpired: {
      title: "Session Expired",
      description:
        "Due to inactivity, your session has expired. Enter your email and password to log back in."
    },

    //Websocket Connection Error Page
    websocketConnectionErrorPage: {
      title: "Connection Error",
      description1:
        "Something went wrong between your computer and our server. If you are in the middle of a test, please let the test administrator know right away!"
    },

    //Home Page
    homePage: {
      welcomeMsg: "Welcome to the Candidate Assessment Tool",
      description:
        "This website is used to assess candidates for positions in the federal public service. To access your tests, you must login below. If you do not have an account, you may register for one using your email address."
    },

    //Sample Tests Page
    sampleTestDashboard: {
      title: "Sample Tests",
      description:
        'Listed below are sample tests that are available on the system. Sample tests are not scored and answers are not submitted at the end of a test. Click on "View" to access the sample test.',
      table: {
        columnOne: "Name of test",
        columnTwo: "Action",
        viewButton: "View",
        viewButtonAccessibilityLabel: "View {0} test"
      }
    },

    //Dashboard Page
    dashboard: {
      title: "Welcome, {0} {1}.",
      description:
        "You are logged into your account. If you are scheduled to take a test, please wait for the test administrator's instructions.",
      table: {
        columnOne: "Name of test",
        columnTwo: "Action",
        viewButton: "View",
        viewButtonAccessibilityLabel: "View {0} test",
        noTests: "You have no assigned tests."
      }
    },

    //Checkin action
    candidateCheckIn: {
      button: "Check-in",
      loading: "Searching for active tests...",
      checkedInText: "You checked-in with Test Access code: ",
      popup: {
        title: "Enter your Test Access Code",
        description:
          'Please type the Test Access Code provided by the test administrator in the box below. Then select "Check-in" to access your test.',
        textLabel: "Test Access Code:",
        textLabelError: "Please enter a valid test access code"
      }
    },

    //Test Builder Page
    testBuilder: {
      containerLabel: "Test Builder",
      goBackButton: "Back to Test Builder",
      inTestView: "In-Test View",
      errors: {
        nondescriptError:
          "Non descript error. Report this to IT. Maybe a user is logged into the adminconsole.",
        cheatingPopup: {
          title: "Caught Cheating Attempt",
          description:
            "You are not allowed to switch or open new tabs. Any attempt to minimize the screen or change focus will be considered a cheating attempt.",
          warning:
            "After {0} more detections your test will be terminated and your results invalidated. If this was an error, alert your TA."
        }
      },
      testDefinitionSelectPlaceholder: "Select a Test Section to View",
      sideNavItems: {
        testDefinition: "Test Definition",
        testSections: "Test Sections",
        testSectionComponents: "Test Section Components",
        sectionComponentPages: "Section Component Pages",
        componentPageSections: "Component Page Sections",
        questions: "Questions",
        questionBlockTypes: "Question Block Types",
        competencyTypes: "Competency Types",
        answers: "Answers",
        addressBook: "Address Book Contacts"
      },
      //some values are underscored because python uses underscores
      testDefinition: {
        title: "Test Definition",
        description: "These are the top level properties of a test.",
        activate: {
          button: "Activate",
          popupTitle: "Activate Test Definition",
          popupContent:
            "Activating a Test Definition version will make this version available to be assigned to Test Adminstrators. Are you sure you wish to continue?"
        },
        deactivate: {
          button: "Deactivate",
          popupTitle: "Deactivate Test Definition",
          popupContent:
            "Deactivating a Test Definition version will remove this version from being available to be assigned to Test Adminstrators. Are you sure you wish to continue?"
        },
        deleteTestDefinition: {
          title: "Delete this Test Definition",
          description:
            "Deleting a Test Definition will erase all data associated to this test. Are you sure you wish to continue?"
        },
        createTestDefinition: {
          button: "Create a New Test Definition",
          title: "Create a New Test Definition",
          description:
            "Creating a new Test Definition will erase any data that you have created and not uploaded. Are you sure you wish to continue?"
        },
        uploadTestDefinition: {
          button: "Upload",
          title: "Upload Test Definition",
          description:
            "Uploading this test definition will create the new version. Are you sure you wish to continue?"
        },
        uploadJSONTestDefinition: {
          button: "Upload JSON",
          title: "Upload JSON Test Definition",
          description: "Please paste the JSON to upload below."
        },
        JSONUpload: {
          title: "JSON",
          titleTooltip:
            "Paste the JSON download from a Test Builder. This can be from different environments.",
          errorMessage: "Not valid JSON"
        },
        downloadTestDefinition: {
          button: "Download JSON"
        },
        selectTestDefinition: {
          button: "Select a Test Definition",
          title: "Test Definition",
          description: "A list of all available test definitions to modify.",
          titleTooltip: "Select a test definition to modify.",
          header: "Select a Test Definition"
        },
        test_code: {
          title: "Test Code",
          titleTooltip: "Unique Identifier for a test definition",
          errorMessage: "Not a valid Test Code"
        },
        version: {
          title: "Version Number",
          titleTooltip: "the latest version of the test definition",
          errorMessage: "Not a valid Version Number"
        },
        fr_name: {
          title: "French Name of the test",
          titleTooltip: "Name display to candidates when they have an active test",
          errorMessage: "Not a valid Name"
        },
        en_name: {
          title: "English Name of the test",
          titleTooltip: "Name display to candidates when they have an active test",
          errorMessage: "Not a valid Name"
        },
        is_public: {
          title: "Sample Test",
          titleTooltip: "Is this a sample test?",
          errorMessage: "not a valid choice"
        },
        active: {
          title: "Active",
          titleTooltip: "Is this test active and available to use?",
          errorMessage: "not a valid choice"
        },
        test_language: {
          title: "Test Language",
          titleTooltip: "Is this test presented in a single language?",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        }
      },
      testSection: {
        title: "Test Section",
        description:
          "These are the test sections belonging to the Test Definition. an example of a Test Section would be the Overview page, the Instruction Page, or the Quit page of a test",
        collapsableItemName: "Test Section {0}: {1}",
        addButton: "Add Test Section",
        delete: {
          title: "Delete Test Section Component",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        fr_title: {
          title: "French Name of the test section",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name of the test section",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        uses_notepad: {
          title: "Notepad",
          titleTooltip: "Does this test section use the notepad component?",
          errorMessage: "not a valid choice"
        },
        block_cheating: {
          title: "Block Cheating",
          titleTooltip: "Should this test section report the user for switching tabs etc.?",
          errorMessage: "not a valid choice"
        },
        scoring_type: {
          title: "Scoring Method",
          titleTooltip: "Set the correct scoring method for the components in this test section.",
          errorMessage: "not a valid choice"
        },
        default_time: {
          title: "Default Time",
          titleTooltip:
            "How long does a candidate have on this section? If the section is not timed, then leave this empty. This field can be overridden by a TA",
          errorMessage: "Only numbers are allowed"
        },
        default_tab: {
          title: "Default Tab",
          titleTooltip: "Which top tab should be displayed when a candidate begins this section",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        order: {
          title: "Section Order",
          titleTooltip: "The order in the test that this section appears.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        next_section_button: {
          title: "Next Section Button",
          titleTooltip: "Define a button to take the user to the next section.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "0"
        },
        reducers: {
          title: "Reducers",
          titleTooltip:
            "Which aspects of the test should be cleared when a user navigates to this section?",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        },
        section_type: {
          title: "Section Type",
          titleTooltip:
            "The type of test section. Such as: Single page, Top Tab navigation, Finish, Quit...",
          errorMessage: "Not a valid Type"
        },
        deletePopup: {
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      nextSectionButton: {
        header: "Next Section Button",
        button_type: {
          title: "Button Type",
          titleTooltip: "This is selected based on the test section component."
        },
        confirm_proceed: {
          title: "Confirmation Box",
          titleTooltip:
            "A checkbox the user has to click before being allowed to press the action button."
        },
        button_text: {
          title: "Button Text",
          titleTooltip: "The text that the button displays to the user"
        },
        title: {
          title: "Popup Title",
          titleTooltip:
            "The text that appears on the header of the popup when the next section button is clicked."
        },
        content: {
          title: "Popup Content",
          titleTooltip:
            "The text that appears in the body of the popup when the next section button is clicked."
        }
      },
      testSectionComponents: {
        title: "Test Section Components",
        description:
          "These are the test section components belonging to the Test Section below. an example of a Test Section Component would be a Question List, Inbox, Side Navigation, or Single Page appearance/component.",
        collapsableItemName: "Test Section Component {0}: {1}",
        addButton: "Add Test Section Component",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        parentTestSection: {
          title: "Parent Test Section",
          titleTooltip: "The test section that the displayed objects appear in."
        },
        fr_title: {
          title: "French Name",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name",
          titleTooltip: "Hidden name used for easy identification by internal users",
          errorMessage: "Not a valid Name"
        },
        order: {
          title: "Order",
          titleTooltip: "The order in which this component appears in a Top Tab navigation.",
          errorMessage: "Not a valid Name"
        },
        component_type: {
          title: "Component Type",
          titleTooltip: "The type of component. Such as: Inbox, Single Page, Side Navigation...",
          errorMessage: "Not a valid Type"
        },
        shuffle_all_questions: {
          title: "Shuffle Questions",
          titleTooltip:
            "Shuffle the questions in this question list? Question dependencies remain in the order they were given."
        },
        language: {
          title: "Content Language",
          titleTooltip:
            "Which language will the content in this section be provided in? This is mandatory for screen readers to use the correct accent.",
          errorMessage: "not a valid choice",
          selectPlaceholder: "--"
        }
      },
      questionListRules: {
        title: "Block Type Rules",
        collapsableItemName: "Rule {0}",
        addButton: {
          title: "Add Rule",
          titleTooltip: "You must create Question Block Types in order to add rules."
        },
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        number_of_questions: {
          title: "Number of Questions",
          titleTooltip: "The number of questions to select of the following question block type."
        },
        question_block_type: {
          title: "Question Block Type",
          titleTooltip: "The question block type from which to select questions."
        },
        order: {
          title: "Order",
          titleTooltip:
            "The order that this rule will appear in the question list. If there is no order, leave this field empty."
        },
        shuffle: {
          title: "Shuffle Questions",
          titleTooltip:
            "Shuffle the questions in this rule? Question dependencies remain in the order they were given."
        }
      },
      sectionComponentPages: {
        title: "Section Component Pages",
        description:
          "These are the section component pages belonging to the Test Section and Section Component below. A Section Component Page must exist for each section component where you want to display information (markdown, sample email, images). If this is combined with a Side Navigation Component, the titles will be used as the navigation buttons on the side panel",
        addButton: "Add Section Component Page",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        collapsableItemName: "Section Component Page {0}: {1}",
        parentTestSectionComponent: {
          title: "Parent Test Section Component",
          titleTooltip: "The parent test section the displayed objects are displayed in."
        },
        fr_title: {
          title: "French Name",
          titleTooltip:
            "Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "Not a valid Name"
        },
        en_title: {
          title: "English Name",
          titleTooltip:
            "Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "Not a valid Name"
        },
        order: {
          title: "Order",
          titleTooltip: "The order in which this page appears in a side navigation panel.",
          errorMessage: "Not a valid Name"
        }
      },
      componentPageSections: {
        title: "Component Page Sections",
        description:
          "These are the Component Page Sections related to the parent objects below. An example of a Component Page Section is a sample email, markdown, a sample email response, an image, etc. They are the components that make up what is visually shown to a user on information pages.",
        collapsableItemName: "Section Component Page Order: {0}, Type: {1}",
        parentSectionComponentPage: {
          title: "Parent Section Component Page",
          titleTooltip: "The parent side navigation tab that the objects displayed will belong to."
        },
        pageSectionLaguage: {
          title: "Language",
          titleTooltip:
            "If a test is bilingual, you must create page sections for each language. This will show all the page sections created for the selected language."
        },
        addButton: "Add Section Component Page",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Order",
          titleTooltip: "The order in which this page appears in a side navigation panel.",
          errorMessage: "Not a valid Name"
        },
        page_section_type: {
          title: "Page Section Type",
          titleTooltip:
            "The type of section to display (i.e. Markdwon, sample email, sample teask response).",
          errorMessage: "Not a valid type."
        }
      },
      markdownPageSection: {
        content: {
          title: "Content",
          titleTooltip: "The markdown content to display."
        }
      },
      sampleEmailPageSection: {
        email_id: {
          title: "Email ID",
          titleTooltip: "The email ID to display"
        },
        from_field: {
          title: "From",
          titleTooltip: "Who the email is from."
        },
        to_field: {
          title: "To",
          titleTooltip: "Who the email is to"
        },
        date_field: {
          title: "Date",
          titleTooltip: "The date displayed on the email."
        },
        subject_field: {
          title: "Subject",
          titleTooltip: "The subject displayed on the email."
        },
        body: {
          title: "Body",
          titleTooltip: "The body of the email to display."
        }
      },
      sampleEmailResonsePageSection: {
        cc_field: {
          title: "CC",
          titleTooltip: "Who is CC'd on the email"
        },
        to_field: {
          title: "To",
          titleTooltip: "Who the email is to"
        },
        response: {
          title: "Response",
          titleTooltip: "The email response."
        },
        reason: {
          title: "Reason",
          titleTooltip: "The reason for responding with this email."
        }
      },
      sampleTaskResonsePageSection: {
        response: {
          title: "Response",
          titleTooltip: "The task response."
        },
        reason: {
          title: "Reason",
          titleTooltip: "The reason for responding with this task."
        }
      },
      zoomImagePageSection: {
        small_image: {
          title: "Full Image Name",
          titleTooltip: "name of the image file including extension."
        },
        note: "Note",
        noteDescription:
          "In order for this image to be displayed properly you must provide the image to the IT team to upload to the application server."
      },
      treeDescriptionPageSection: {
        address_book_contact: {
          title: "Root Contact",
          titleTooltip: "The root contact that the tree description will start from."
        }
      },
      addressBook: {
        topTitle: "Address Book",
        description:
          "These are email like contacts used in Inbox style Test Section Components and Tree Description Component Page Sections.",
        collapsableItemName: "{0}",
        addButton: "Add Address Book Contact",
        name: {
          title: "Name",
          titleTooltip: "The name of the contact."
        },
        title: {
          title: "Title",
          titleTooltip: "The title of the contact."
        },
        parent: {
          title: "Parent Contact",
          titleTooltip: "The contact that this contact reports to."
        },
        test_section: {
          title: "Test Sections",
          titleTooltip:
            "Which test sections can a user send emails to this contact or if there is a tree description in a test section then this contact must be in that test section."
        }
      },
      questions: {
        topTitle: "Questions",
        description:
          "These are questions used in an Email Inbox or a Question List. Questions can be emails or multiple choice for example.",
        collapsableItemName: "Question {0} of Block: {1}",
        emailCollapsableItemName: "Email ID: {0}, From: {1}",
        addButton: "Add Question",
        searchResults: "{0} Question(s) found",
        pilot: {
          title: "Pilot Question",
          titleTooltip:
            "Is the question a pilot question? It's answer will not be counted towards a final score.",
          description: ""
        },
        questionBlockSelection: {
          title: "Question Block Type",
          titleTooltip: "The Question Block Type to narrow the number of results."
        },
        question_type: {
          title: "Question Type",
          titleTooltip:
            "i.e. multiple choice, email. This is pre-determined by the Test Section Component Type (Question List or Inbox)"
        },
        question_block_type: {
          title: "Question Block Type",
          titleTooltip:
            "The question block type identifier. Used to determine what questions are returned for a question lists rule set."
        },
        dependencies: {
          title: "Dependents",
          titleTooltip: "Questions that this question must be presented with (in order)."
        },
        order: {
          title: "Dependent Order",
          titleTooltip:
            "The order this question is presented in when dependents exist. This is only used for ordering questions in a dependency sitaution."
        }
      },
      questionSituations: {
        situation: {
          title: "Situation",
          titleTooltip:
            "The situation text to be displayed at the top of the question when a test is viewed in scorer mode."
        }
      },
      questionBlockTypes: {
        topTitle: "Question Block Types",
        description:
          "These are types associated with questions. These types are used to form rules for question lists. For example if you have created the question types of EASY and HARD you could create a rule for a question list the would select 10 EASY and 10 HARD questions.",
        collapsableItemName: "Question Block Type: {0}",
        addButton: "Add Question Block Type",
        name: {
          title: "Name",
          titleTooltip: "The unique identifier for a question block type."
        }
      },
      competencyTypes: {
        topTitle: "Competency Types",
        description:
          "These are types associated with questions. These types are assigned to questions for manual marking.",
        collapsableItemName: "Competency Type: {0}",
        addButton: "Add Competency Type",
        en_name: {
          title: "English Name",
          titleTooltip: "The unique identifier for a question block type."
        },
        fr_name: {
          title: "French Name",
          titleTooltip: "The unique identifier for a question block type."
        },
        max_score: {
          title: "Max Score",
          titleTooltip: "The max score a candidate can get for this competency."
        }
      },
      questionSections: {
        title: "Question Sections",
        collapsableItemName: "Question Section {0}",
        description:
          "These are the test section components belonging to the Test Section below. an example of a Test Section Component would be a Question List, Inbox, Side Navigation, or Single Page appearance/component.",
        addButton: "Add Question Section",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "Order",
          titleTooltip: "The order in which this component appears in a Top Tab navigation.",
          errorMessage: "Not a valid Name"
        },
        question_section_type: {
          title: "Section Type",
          titleTooltip: "The type of section. Such as: Markdown, Image, Video...",
          errorMessage: "Not a valid Type"
        }
      },
      answers: {
        title: "Answers",
        collapsableItemName: "Scoring Value {0}",
        addButton: "Add Answer",
        delete: {
          title: "Delete",
          description:
            "Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        content: {
          title: "content",
          titleTooltip: "The text to display for this answer.",
          errorMessage: "Not a valid input"
        },
        scoring_value: {
          title: "Scorng Value",
          titleTooltip:
            "The score to be added to the total test score when this answer is selected.",
          errorMessage: "Not a valid value"
        }
      },
      questionSectionMarkdown: {
        content: {
          title: "Content",
          titleTooltip: "Markdown to appear on the question."
        },
        delete: {
          title: "Delete Confirmation",
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      questionSectionEnd: {
        content: {
          title: "Specific Question",
          titleTooltip:
            "The literal question after context to be answered. This allows screen readers to navigate directly to this section."
        },
        delete: {
          title: "Delete Confirmation",
          content:
            "Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      multipleChoiceQuestions: {
        header: "Question Sections"
      },
      emailQuestions: {
        scoringTitle: "Scoring",
        exampleRatingsTitle: "Example Ratings",
        competency_types: {
          title: "Competencies",
          titleTooltip: "The competencies being assessed in this question."
        },
        deleteDescription:
          "Deleting this item will delete the current object and any children this object may have. Are you sure you want to delete this object?",
        email_id: {
          title: "Email Id",
          titleTooltip:
            "Email Id is displayed at the top of the email and is the order in which emails are displayed."
        },
        to_field: {
          title: "To Field",
          titleTooltip: "The contact the email is to."
        },
        from_field: {
          title: "From Field",
          titleTooltip: "The Contact the email is from."
        },
        cc_field: {
          title: "CC Field",
          titleTooltip: "Contacts that are CC'd on this email."
        },
        date_field: {
          title: "Date",
          titleTooltip: "The date text to display on the email"
        },
        subject_field: {
          title: "Subject",
          titleTooltip: "The subject field to display on the email"
        },
        body: {
          title: "Body",
          titleTooltip: "The content of the email to display."
        }
      },
      situationRating: {
        score: {
          title: "Rating Score",
          titleTooltip:
            "This is the value that the example answered would be scored. Keep this to one decimal place."
        },
        example: {
          title: "Example Rationale",
          titleTooltip: "The Rationale behind giving the candidates answer this score."
        }
      }
    },
    //Test Administration Page
    testAdministration: {
      title: "Welcome, {0} {1}.",
      containerLabel: "Test Administration",
      sideNavItems: {
        testAccessCodes: "Test Access Codes",
        activeCandidates: "Active Candidates",
        reports: "Reports"
      },
      testAccessCodes: {
        description: "Manage the Test Access Codes you have generated.",
        table: {
          testAccessCode: "Test Access Code",
          test: "Test",
          staffingProcessNumber: "Staffing Process Number",
          action: "Action",
          actionButton: "Disable",
          generateNewCode: "Generate Test Access Code"
        },
        generateTestAccessCodePopup: {
          title: "Enter Session Information",
          description:
            "Provide an order number, the test to administer and the test session language.",
          testOrderNumber: "Test Order Number:",
          testOrderNumberCurrentValueAccessibility: "Current selected test order number is:",
          testToAdminister: "Test to Administer:",
          testToAdministerCurrentValueAccessibility: "Current selected test to administer is:",
          testSessionLanguage: "Test Session Language",
          testSessionLanguageCurrentValueAccessibility:
            "Current selected test session language is:",
          billingInformation: {
            title: "Billing Information:",
            staffingProcessNumber: "Staffing Process Number:",
            departmentMinistry: "Department/Ministry Code:",
            contact: "Contact:",
            isOrg: "IS Org:",
            isRef: "IS Ref:"
          },
          generateButton: "Generate"
        },
        disableTestAccessCodeConfirmationPopup: {
          title: "Confirm Disable",
          description:
            "Once disabled, {0} Test Access Code will no longer be valid. Candidates will not be able to check-in using it.",
          warning: "Warning!"
        }
      },
      activeCandidates: {
        description:
          "As candidates Check-in, you will see their names in the list of Active Candidates below. After all candidates in attendance are Checked-in, disable the Test Access Code.",
        lockAllButton: "Lock All",
        unlockAllButton: "Unlock All",
        table: {
          candidate: "Candidate",
          dob: "Date of Birth",
          status: "Status",
          timer: "Total Time",
          actions: "Actions",
          actionTooltips: {
            updateTestTimer: "Update Test Time",
            approve: "Approve Candidate",
            unAssign: "Un-assign",
            lockPause: "Lock/Pause Test",
            unlockUnpause: "Unlock/Unpause Test",
            reSync: "Re-Sync",
            report: "Report Candidate"
          },
          updateTestTimerAriaLabel:
            "Update test timer of {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          approveAriaLabel:
            "Approve {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          unAssignAriaLabel:
            "Un-assign {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          lockPauseAriaLabel:
            "Lock/Pause {0} {1}'s test. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          unlockUnpauseAriaLabel:
            "Unlock/Unpause {0} {1}'s test. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          reSyncAriaLabel:
            "Re-sync {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          reportAriaLabel: "Report {0} {1} user."
        },
        noActiveCandidates: "There are no active candidates",
        editTimePopup: {
          title: "Edit Test Time",
          description1: "For use for accomodation pusposes only.",
          description2: "Adjust the amount of time for {0} {1}.",
          description3:
            "The timers cannot be set for less than the original amount of time allowed for the respective test section.",
          description4: "Total Test Time: {0} Hours {1} Minutes",
          hours: "HOURS",
          minutes: "MINUTES",
          incrementHoursButton: "Increment hours button. Current value is {0}.",
          decrementHoursButton: "Decrement hours button. Current value is {0}.",
          incrementMinutesButton: "Increment minutes button. Current value is {0}.",
          decrementMinutesButton: "Decrement minutes button. Current value is {0}.",
          timerCurrentValue: "Current Timer value: {0} hours and {1} minutes.",
          setTimerButton: "Set Timer"
        },
        lockPausePopup: {
          title: "Lock Test",
          description1: "You are about to lock {0} {1}'s test.",
          checkboxPart1: "Lock test for a specified period of time (Pause). ",
          checkboxPart2: "This is only to be used for accomodation purposes.",
          description2: "When the timer expires, the candidate's test timer will resume.",
          lockButton: "Lock Test",
          pauseButton: "Pause Test"
        },
        unlockUnpausePopup: {
          title: "Unlock Test",
          description1: "You are about to unlock {0} {1}'s test.",
          description2: "The test timer will resume once you unlock the test.",
          description3: "Be sure to complete an incident report regarding this.",
          unlockButton: "Unlock Test",
          unpauseButton: "Unpause Test"
        },
        lockAllPopup: {
          title: "Lock All Candidate Tests",
          description1: "You are about to lock this test for all of your active candidates.",
          description2:
            "If after the incident that has caused you to lock the tests, it is determined that the test can be continued you will need to unlock the tests for the candidates that wish to continue testing.",
          description3:
            "Be sure to complete session report and a candidate report for any candidates not able to continue after the incident.",
          lockTestsButton: "Lock Tests"
        },
        unlockAllPopup: {
          title: "Unlock All Candidate Tests",
          description1: "You are about to unlock this test for all of your active candidates.",
          description2:
            "Be sure to lock any test for a candidate not wishing or able to continue and fill out an incident report for those candidates.",
          description3: "Remeber to file a Session Report for the incident.",
          unlockTestsButton: "Unlock Tests"
        },
        reSyncPopup: {
          title: "Re-Sync Confirmation",
          description: "Are you sure you want to re-sync with {0} {1}?"
        },
        unAssignPopup: {
          title: "Un-Assign Confirmation",
          description: "Are you sure you want to un-assign {0} {1}'s test.?"
        }
      },
      reports: {
        description: "Generate a results report for a Test Order you have administered.",
        reportTypes: {
          individualScoreSheet: "Individual Score Sheet",
          resultsReport: "Results Report",
          financialReport: "Financial Report"
        }
      }
    },

    //PPC  Administration Page
    ppcAdministration: {
      title: "Welcome, {0} {1}.",
      containerLabel: "PPC Administration",
      sideNavItems: {
        reports: "Reports"
      },
      reports: {
        title: "Reports",
        description: "Generate reports based on Test Orders."
      }
    },

    //Report Generator Component
    reports: {
      reportTypeLabel: "Report Type",
      testOrderNumberLabel: "Test Order Number",
      testLabel: "Test",
      candidateLabel: "Candidate",
      generateButton: "Generate Report",
      noDataPopup: {
        title: "No Data",
        description: "There is no data to show based on the  provided parameters."
      }
    },

    //DatePicker Component
    datePicker: {
      dayField: "Day",
      dayFieldSelected: "Day field selected.",
      monthField: "Month",
      monthFieldSelected: "Month field selected.",
      yearField: "Year",
      yearFieldSelected: "Year field selected.",
      currentValue: "Current value is:",
      none: "None",
      datePickedError: "Must be a valid date",
      futureDatePickedError: "Must be a future date"
    },

    //Profile Page
    profile: {
      title: "Welcome, {0} {1}.",
      sideNavItems: {
        personalInfo: "Personal Info",
        password: "Password",
        preferences: "Preferences",
        permissions: "Permissions",
        profileMerge: "Profile Merge"
      },
      personalInfo: {
        title: "Your Contact Information",
        nameSection: {
          title: "Name:",
          firstName: "First Name",
          lastName: "Last Name"
        },
        emailAddressesSection: {
          title: "Email Addresses:",
          primary: "Primary",
          secondary: "Secondary",
          secondaryTooltip: "Secondary email tooltip (TEMP TEXT)",
          emailError: "Must be a valid email address"
        },
        dateOfBirth: {
          title: "Date of Birth:",
          titleTooltip: "Date of birth tooltip (TEMP TEXT)",
          yearField: "Year",
          yearFieldSelected: "Year field selected.",
          monthField: "Month",
          monthFieldSelected: "Month field selected.",
          dayField: "Day",
          dayFieldSelected: "Day field selected.",
          currentValue: "Current value is:",
          none: "None"
        },
        priOrMilitaryNbr: {
          title: "PRI or Service Number:",
          titleTooltip: "PRI or Military number tooltip (TEMP TEXT)",
          priOrMilitaryNbrError: "Must be a valid PRI or Service number"
        },
        optionalField: "(Optional)",
        saveConfirmationPopup: {
          title: "Save Confirmation",
          description: "Your personal information has been saved successfully."
        }
      },
      password: {
        newPassword: {
          title: "Password",
          updatedDate: "Your password was last updated on: {0}",
          updatedDateNever: "never",
          currentPassword: "Current Password:",
          newPassword: "New Password:",
          confirmPassword: "Confirm Password:",
          popup: {
            title: "Save Confirmation",
            description: "Your password has been updated successfully."
          }
        },
        passwordRecovery: {
          title: "Password Recovery",
          secretQuestion: "Secret Question:",
          secretAnswer: "Secret Answer:",
          secretQuestionUpdatedConfirmation: "Your secret question has been updated successfully"
        }
      },
      preferences: {
        title: "Preferences",
        description: "Modify your preferences.",
        notifications: {
          title: "Notifications",
          checkBoxOne:
            "Always send an email to my primary email address when I receive a notification",
          checkBoxTwo:
            "Always send an email to my secondary email address when I receive a notification"
        },
        display: {
          title: "Display",
          checkBoxOne: "Allow others to see my profile picture",
          checkBoxTwo: "Hide tooltip icons"
        }
      },
      permissions: {
        title: "System Permissions",
        description:
          "You have all the accesses needed to check-in and write any test and/or evaluation needed for staffing process(es) to which you have applied.",
        systemPermissionInformation: {
          title: "System Permission Information",
          titleTooltip: "System Permission Information tooltip (TEMP TEXT)",
          addPermission: "Permission",
          pending: "Pending",
          superUser: {
            title: "Super User",
            permission: "User has full system access and permissions"
          }
        },
        addPermissionPopup: {
          title: "System Permission Request",
          description: "Request permissions to perform administration activities.",
          gocEmail: "GOC email:",
          gocEmailTooltip: "GOC Email tooltip (TEMP TEXT)",
          gocEmailError: "Must be a valid email address",
          pri: "PRI or Service Number:",
          priError: "Must be a valid PRI or Service number",
          supervisor: "Supervisor:",
          supervisorError: "Must be a valid name",
          supervisorEmail: "Supervisor email:",
          supervisorEmailError: "Must be a valid email address",
          rationale: "Rationale:",
          rationaleError: "Provide a rationale for this request",
          permissions: "Permissions:",
          permission: "Permission:",
          permissionRequested: "Permission Requested:",
          permissionsError: "Must select at least one permission to request"
        }
      },
      testPermissions: {
        title: "Test Access Permissions",
        description: "As a Test Administrator, you have access to administer the following tests:",
        // temporary until we have other test types
        eMIB: "Electronic Managerial In-Box (e-MIB)",
        table: {
          title: "Test Accesses",
          column1: "Test Version",
          column1OrderItem: "Order by test versions",
          column2: "Order Number",
          column2OrderItem: "Order by test order numbers",
          column3: "Staffing Process Number",
          column3OrderItem: "Order by staffing process number",
          column4: "Expiration Date",
          column4OrderItem: "Order by expiration dates"
        }
      },
      saveButton: "Save"
    },

    //System Administrator Page
    systemAdministrator: {
      title: "Welcome, {0} {1}.",
      containerLabel: "System Administration",
      sideNavItems: {
        dashboard: "Dashboard",
        activeTests: "Active Tests",
        testAccessCodes: "Test Access Codes",
        permissions: "Permissions",
        testAccesses: "TA Test Accesses",
        incidentReports: "Incident Reports",
        scoring: "Scoring",
        reScoring: "Re-Scoring",
        reports: "Reports"
      },
      permissions: {
        description: "Manage permission requests and assign permissions to users.",
        tabs: {
          permissionRequests: {
            title: "Permission Requests",
            noPermission: "There are no permission requests at the moment.",
            permissionRequestedAccessibility: "Permission requested:",
            lastNameAccessibility: "Last name:",
            firstNameAccessibility: "First name:",
            viewButtonAccessibility: "View request details",
            popup: {
              title: "Permission Request",
              description: "Approve or deny the permission for request made by {0}",
              deniedReason: "Denied Reason:",
              denyButton: "Deny permission",
              grantButton: "Grant permission",
              grantPermissionErrors: {
                notEmptyDeniedReasonError:
                  "This field must be empty if you want to approve this permission request",
                missingDeniedReasonError: "Must put a reason to deny this permission",
                usernameDoesNotExistError: "This username no longer exists"
              }
            }
          },
          activePermissions: {
            title: "Active Permissions",
            searchBarTitle: "Search:",
            multipleResultsFound: "{0} results found",
            singularResultFound: "{0} result found",
            clearSearch: "Clear Search",
            noResultsFound: "There are no results found based on your search.",
            displayOptionLabel: "Display:",
            displayOptionAccessibility:
              "You can choose how many results per page you want by using this dropdown.",
            displayOptionCurrentValueAccessibility: "Current selected value is:",
            table: {
              permission: "Permission",
              user: "User",
              action: "Action",
              actionButtonLabel: "Open",
              actionButtonAriaLabel: "View permission details of user: {0} {1}, permission: {2}."
            },
            viewEditDetailsPopup: {
              title: "System Permission Information",
              description: "Review and modify {0}'s permission.",
              deleteButton: "Delete permission",
              saveButton: "Save changes",
              reasonForModification: "Reason for modifications:",
              reasonForModificationError: "Provide a reason for modifications for this request"
            },
            deletePermissionConfirmationPopup: {
              title: "Are you sure you want to delete this permission?",
              systemMessageTitle: "Warning!",
              systemMessageDescription:
                "This action will revoke {0} permission from {1}'s account. Are you sure you want to proceed?"
            },
            updatePermissionDataConfirmationPopup: {
              title: "Updates Confirmation",
              description: "{0}'s permission data has been updated successfully."
            },
            nextPageButton: "Next page",
            previousPageButton: "Previous page"
          }
        }
      },
      testAccesses: {
        title: "Test Administrator Test Accesses",
        description: "Manage test accesses for test administrators.",
        tabs: {
          assignTestAccesses: {
            version: {
              title: "{0} Version {1}",
              titleTooltip: "Version number of the test."
            },
            testDescription: "{0} Version {1}",
            title: "Assign Test Accesses",
            testOrderNumberLabel: "Test Order Number:",
            testOrderNumberCurrentValueAccessibility: "Current selected test order number is:",
            staffingProcessNumber: "Staffing Process Number:",
            departmentMinistry: "Department/Ministry:",
            isOrg: "IS Org (Funds Center):",
            isRef: "IS Ref (Funds Commitment):",
            billingContact: "Billing Contact:",
            billingContactInfo: "Billing Contact Info:",
            users: "Test Administrator(s):",
            username: "Username:",
            usersCurrentValueAccessibility: "Current selected users are:",
            noUserAccessibility: "none",
            expiryDate: "Expiry Date:",
            testAccesses: "Test Accesses:",
            testAccess: "Test Access:",
            testAccessesError: "At least one test access must be selected",
            testAccessAlreadyExistsError:
              "{0} already has this {1} access combined with {2} test order number",
            emptyFieldError:
              "This field is empty or exceed the maximum amount of characters allowed",
            unableToAccessOrderingServiceError:
              "Unable to access ordering service. Please enter financial data manually.",
            searchButton: "Search",
            manuelEntryButton: "Manual entry",
            noTestOrderNumberFound: "This order number cannot be found, an exact match is required",
            // temporary until we have other test types
            eMIB: "Electronic Managerial In-Box (e-MIB)",
            refreshButton: "Clear All Fields",
            refreshConfirmationPopup: {
              title: "Clear all fields?",
              description: "Description here.. (to be determined)"
            },
            saveButton: "Save",
            saveConfirmationPopup: {
              title: "Confirm Test Access Assignment(s)",
              usersSection: "You are about to allow the following users(s):",
              testsSection: "Access to administer the following tests:",
              orderInfoSection: {
                title: "For the following test order information:",
                testOrderNumber: "Test Order Number:",
                staffingProcessNumber: "Staffing Process Number:",
                departmentMinistry: "Department:"
              },
              expiriDateSection: "These accesses will expire on:",
              confirmation: "Please confirm this information is correct.",
              testAccessesGrantedConfirmationPopup: {
                title: "Confirmation",
                description: "Test accesses have been granted successfully."
              }
            }
          },
          activeTestAccesses: {
            title: "Active Test Accesses",
            searchBarTitle: "Search:",
            displayOptionLabel: "Display:",
            displayOptionAccessibility:
              "You can choose how many results per page you want by using this dropdown.",
            displayOptionCurrentValueAccessibility: "Current selected value is:",
            table: {
              testAdministrator: "Test Administrator",
              test: "Test",
              orderNumber: "Order Number",
              expiryDate: "Expiry Date",
              action: "Action",
              actionButtonLabel: "Open",
              actionButtonAriaLabel:
                "View test access details of user: {0} {1}, test: {2}, order number: {3}, expiry date: {4}."
            },
            viewTestPermissionPopup: {
              title: "Test Access Information",
              description: `Review {0}'s test access permission.`,
              deleteButtonAccessibility: "Delete",
              saveButtonAccessibility: "Save"
            },
            deleteConfirmationPopup: {
              title: "Are you sure you want to delete this test access?",
              systemMessageTitle: "Warning!",
              systemMessageDescription:
                "This action will remove {0} test access from {1}'s account. Are you sure you want to proceed?"
            },
            saveConfirmationPopup: {
              title: "Updates Confirmation",
              description: "{0}'s test access has been updated successfully."
            }
          }
        }
      },
      reports: {
        title: "Reports",
        description: "Generate reports based on Test Orders."
      }
    },

    //Scorer Pages
    scorer: {
      situationTitle: "Situation",
      assignedTest: {
        displayOptionLabel: "Display:",
        displayOptionAccessibility:
          "You can choose how many results per page you want by using this dropdown.",
        displayOptionCurrentValueAccessibility: "Current selected value is:",
        table: {
          assignedTestId: "Assigned Test ID",
          completionDate: "Completion Date",
          testName: "Test Name",
          testLanguage: "Test Language",
          action: "Action",
          actionButtonLabel: "Score Test",
          en: "English",
          fr: "French",
          actionButtonAriaLabel:
            "Score test of: assigned test id: {0}, completion date: {1}, test name: {2}, test language: {3}."
        },
        noResultsFound: "There are no tests waiting to be scored."
      },
      sidebar: {
        title: "Scoring Panel",
        competency: "Competency",
        rational: "Rating rationale",
        placeholder: "Input rationale here..."
      },
      submitButton: "Submit Final Scores",
      unsupportedScoringType:
        "The scoring type: {0} is not supported. The test definition must be incorrect.",
      selectQuestion: "Select a question to continue",
      competency: {
        bar: {
          title: "Behavioural Achored Ratings",
          description:
            "Below is a complete view of the BAR for this question and competency. Once selected, you can use the arrows to navigate, expand, and collapse information.",
          barButton: "View BAR"
        }
      }
    },

    //Status Page
    statusPage: {
      title: "CAT Status",
      logo: "Thunder CAT Logo",
      welcomeMsg:
        "Internal status page to quickly determine the status / health of the Candidate Assessment Tool.",
      versionMsg: "Candidate Assessment Tool Version: ",
      gitHubRepoBtn: "GitHub Repository",
      serviceStatusTable: {
        title: "Service Status",
        frontendDesc: "Front end application built and serving successfully",
        backendDesc: "Back end application completing API requests successfully",
        databaseDesc: "Database completing API requests sccessfully"
      },
      systemStatusTable: {
        title: "System Status",
        javaScript: "JavaScript",
        browsers: "IE 10+, Chrome, Firefox",
        screenResolution: "Screen resolution minimum of 1024 x 768"
      },
      additionalRequirements:
        "Additionally, the following requirements must be met to use this application in a test centre.",
      secureSockets: "Secure Socket Layer (SSL) encryption enabled",
      fullScreen: "Full-screen mode enabled",
      copyPaste: "Copy + paste functionality enabled",
      colorOptions: "IE Internet Options > Colours enabled to users",
      fontsEnabled: "IE Internet Options > Fonts enabled to users",
      accessibilityOptions: "IE Internet Options > Accessibility enabled to users"
    },

    // Settings Dialog
    settings: {
      systemSettings: "Accessibility Display Settings",
      zoom: {
        title: "Zoom (+/-)",
        instructionsListItem1: "Select the View button at the top left bar in Internet Explorer.",
        instructionsListItem2: "Select Zoom.",
        instructionsListItem3:
          "You can select a predefined zoom level, or a custom level by selecting Custom and entering a zoom value.",
        instructionsListItem4:
          "Alternatively, you can hold down CTRL and the + / - keys on your keyboard to zoom in or out."
      },
      fontSize: {
        title: "Font size",
        description: "Select the minimum size of the font text should apprea in:"
      },
      fontStyle: {
        title: "Font style",
        description: "Select the font style/family you want to use:"
      },
      lineSpacing: {
        title: "Line and word spacing",
        description: "Turn on accessibility line and word spacing."
      },
      color: {
        title: "Text and background colour",
        instructionsListItem1: "Select the Tools button and select Internet options.",
        instructionsListItem2: "In the General tab, under Appearance, select Accessibility.",
        instructionsListItem3: "Select the Ignore colors specified on webpages on the check box.",
        instructionsListItem4: "Select OK.",
        instructionsListItem5: "In the General tab, under Appearance, select Colors.",
        instructionsListItem6: "Uncheck the Use Windows colors check box.",
        instructionsListItem7:
          "For each color that you want to change, select the color box, select a new color, and then select OK.",
        instructionsListItem8: "Select OK, and then select OK again."
      }
    },

    //Multiple Choice  test
    mcTest: {
      questionList: {
        questionId: "Question {0}",
        nextButton: "Next",
        previousButton: "Previous",
        reviewButton: "Mark for review",
        markedReviewButton: "Unmark for review",
        seenQuestion: "This question has been seen",
        unseenQuestion: "This question has not been seen",
        answeredQuestion: "This question has been answered",
        unansweredQuestion: "This question has not been answered",
        reviewQuestion: "This question has been marked for review"
      },
      finishPage: {
        homeButton: "Return Home"
      }
    },
    //eMIB Test
    emibTest: {
      //Home Page
      homePage: {
        testTitle: "Sample e-MIB",
        welcomeMsg: "Welcome to the eMIB Sample Test"
      },

      //HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Tips on taking the e-MIB",
          part1: {
            description:
              "The e-MIB presents you with situations that will give you the opportunity to demonstrate the Key Leadership Competencies. Here are some tips that will help you provide assessors with the information they need to evaluate your performance on the Key Leadership Competencies:",
            bullet1:
              "Answer all the questions asked in the emails you received. Also, note that there may be situations that span several emails and for which no specific questions are asked, but that you can respond to. This will ensure that you take advantage of all the opportunities provided to demonstrate the competencies.",
            bullet2:
              "Don’t hesitate to provide your recommendations where appropriate, even if they are only initial thoughts.  If needed, you can then note other information you would need to reach a final decision.",
            bullet3:
              "For situations in which you feel you need to speak with someone in person before you can make a decision, you should indicate what information you need and how this would affect your decision one way or the other.",
            bullet4:
              "Use only the information provided in the emails and the background information. Do not make any inferences based on the culture of your own organization. Avoid making assumptions that are not reasonably supported by either the background information or the emails.",
            bullet5:
              "The e-MIB is set within a specific industry in order to provide enough context for the situations presented. However, effective responses rely on the competency targeted and not on specific knowledge of the industry."
          },
          part2: {
            title: "Other important information",
            bullet1:
              "You will be assessed on the content of your emails, the information provided in your task list and the reasons listed for your actions. Information left on your notepad will not be evaluated.",
            bullet2:
              "You will not be assessed on how you write. No points will be deducted for spelling, grammar, punctuation or incomplete sentences. However, your writing will need to be clear enough to ensure that the assessors understand which situation you are responding to and your main points.",
            bullet3: "You can answer the emails in any order you want.",
            bullet4: "You are responsible for managing your own time."
          }
        },
        testInstructions: {
          title: "Test instructions",
          hiddenTabNameComplementary: 'Under "Instructions"',
          onlyOneTabAvailableForNowMsg:
            "Note that there is only one main tab available for now. As soon as you start the test, the other main tabs will become available.",
          para1:
            "When you start the test, first read the background information which describes your position and the fictitious organization you work for. We recommend you take approximately 10 minutes to read it. Then proceed to the in-box where you can read the emails you received and take action to respond to them as though you were a manager within the fictitious organization.",
          para2: "While you are in the email in-box, you will have access to the following:",
          bullet1: "The test instructions;",
          bullet2:
            "The background information describing your job as the manager and the fictitious organization where you work;",
          bullet3: "A notepad to serve as scrap paper.",
          step1Section: {
            title: "Step 1 - Responding to emails",
            description:
              "You can respond to the emails you received in two ways: by writing an email or by adding tasks to your task list. A description of both ways of responding is presented below, followed by examples.",
            part1: {
              title: "Example of an email you have received:",
              para1:
                "Two options are provided below to demonstrate different ways of responding to the email. You can choose one of the two options presented or a combination of the two. The responses are only provided to illustrate how to use each of the two ways of responding. They do not necessarily demonstrate the Key Leadership Competencies assessed in this situation."
            },
            part2: {
              title: "Responding with an email response",
              para1:
                "You can write an email in response to one you received in your in-box. The written response should reflect how you would respond as a manager.",
              para2:
                "You can use the following options: reply, reply all or forward. If you choose to forward an email, you will have access to a directory with all of your contacts. You can write as many emails as you like to respond to an email or to manage situations you identified across several of the emails you received."
            },
            part3: {
              title: "Example of responding with an email response:"
            },
            part4: {
              title: "Adding a task to the task list",
              para1:
                "In addition to, or instead of, responding by email, you can add tasks to the task list. A task is an action that you intend to take to address a situation presented in the emails. For example, tasks could include planning a meeting or asking a colleague for information. You should provide enough information in your task description to ensure it is clear which situation you are addressing. You should also specify what actions you plan to take, and with whom. You can return to the email to add, delete or edit tasks at any time."
            },
            part5: {
              title: "Example of adding a task to the task list:"
            },
            part6: {
              title: "How to choose a way of responding",
              para1:
                "There are no right or wrong ways to respond. When responding to an email, you can:",
              bullet1: "send an email or emails; or",
              bullet2: "add a task or tasks to your task list; or",
              bullet3: "do both.",
              para2:
                "You will be assessed on the content of your responses, and not on your method of responding (i.e. whether you responded by email and/or by adding a task to your task list). As such, answers need to be detailed and clear enough for assessors to evaluate how you are addressing the situation. For example, if you plan to schedule a meeting, you will need to specify what will be discussed at that meeting.",
              para3Part1:
                "When responding to an email you received, if you decide to write an email ",
              para3Part2: "and",
              para3Part3:
                " to add a task to your task list, you do not need to repeat the same information in both places. For example, if you mention in an email that you will schedule a meeting with a co-worker, you do not need to add that meeting to your task list."
            }
          },
          step2Section: {
            title: "Step 2 - Adding reasons for your actions (optional)",
            description:
              "After writing an email or adding a task, if you feel the need to explain why you took a specific action, you can write it in the reasons for your actions section, located below your email response or tasks. Providing reasons for your actions is optional. Note that you may choose to explain some actions and not others if they do not require any additional explanations. Similarly, you may decide to add the reasons for your actions when responding to some emails and not to others. This also applies to tasks in the task list.",
            part1: {
              title: "Example of an email response with reasons for your actions:"
            },
            part2: {
              title: "Example of a task list with reasons for your actions:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (Manager, Quality Assurance Team)",
            from: "Geneviève Bédard (Director, Research and Innovation Unit)",
            subject: "Preparing Mary for her assignment",
            date: "Friday, November 4",
            body:
              "Hello T.C.,\n\nI was pleased to hear that one of your quality assurance analysts, Mary Woodside, has accepted a six-month assignment with my team, starting on January 2. I understand she has experience in teaching and using modern teaching tools from her previous work as a college professor. My team needs help developing innovative teaching techniques that promote employee productivity and general well-being. Therefore, I think Mary’s experience will be a good asset for the team.\n\nAre there any areas in which you might want Mary to gain more experience and which could be of value when she returns to your team? I want to maximize the benefits of the assignment for both our teams.\n\nLooking forward to getting your input,\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Hi Geneviève,\n\nI agree that we should plan Mary’s assignment so both our teams can benefit from it. I suggest that we train Mary in the synthesis of data from multiple sources. Doing so could help her broaden her skill set and would be beneficial to my team when she comes back. Likewise, your team members could benefit from her past experience in teaching. I’ll consult her directly as I would like to have her input on this. I’ll get back to you later this week once I have more information to provide you on this matter.\n\nThat being said, what are your expectations? Are there any current challenges or specific team dynamics that should be taken into account? I’d like to consider all factors, such as the current needs of your team, challenges and team dynamics before I meet with Mary to discuss her assignment.\n\nThanks,\n\nT.C.",
            reasonsForAction:
              "I am planning to meet with Mary to discuss her expectations regarding the assignment and to set clear objectives. I want to ensure she feels engaged and knows what is expected of her, to help her prepare accordingly. I will also check what her objectives were for the year to ensure that what I propose is consistent with her professional development plan."
          },
          exampleTaskResponse: {
            task:
              "- Reply to Geneviève’s email:\n     > Suggest training Mary in the synthesis of information from multiple sources so that she can broaden her skill set.\n     > Ask what her expectations are and what the challenges are on her team so I can consider all factors when determining how her team could benefit from Mary’s experience in providing training.\n     > Inform her that I will get more information from Mary and will respond with suggestions by the end of the week.\n- Schedule a meeting with Mary to discuss her assignment objectives and ensure she feels engaged and knows what is expected of her.\n- Refer to Mary’s past and current objectives to ensure that what I propose is in line with her professional development plan.",
            reasonsForAction:
              "Training Mary in the synthesis of information from multiple sources would be beneficial to my team which needs to consolidate information gathered from many sources. Asking Geneviève what her expectations and challenges are will help me better prepare Mary and ensure that the assignment is beneficial to both our teams."
          }
        },
        evaluation: {
          title: "Evaluation",
          bullet1:
            "Both the actions you take and the explanations you give will be considered when assessing your performance on each of the Key Leadership Competencies (described below). You will be assessed on the extent to which they demonstrate the Key Leadership Competencies.",
          bullet2:
            "Your actions will be evaluated on effectiveness. Effectiveness is measured by whether your actions would have a positive or a negative impact on resolving the situations presented and by how widespread that impact would be.",
          bullet3:
            "Your responses will also be evaluated on how well they meet the organizational objectives presented in the background information.",
          keyLeadershipCompetenciesSection: {
            title: "Key Leadership Competencies",
            para1Title: "Create Vision and Strategy: ",
            para1:
              "Managers help to define the future and chart a path forward. To do so, they take into account context. They leverage their knowledge and seek and integrate information from diverse sources to implement concrete activities. They consider different perspectives and consult as needed. Managers balance organizational priorities and improve outcomes.",
            para2Title: "Mobilize People: ",
            para2:
              "Managers inspire and motivate the people they lead. They manage the performance of their employees and provide constructive and respectful feedback to encourage and enable performance excellence. They lead by example, setting goals for themselves that are more demanding than those that they set for others.",
            para3Title: "Uphold Integrity and Respect: ",
            para3:
              "Managers exemplify ethical practices, professionalism and personal integrity, acting in the interest of Canada and Canadians. They create respectful, inclusive and trusting work environments where sound advice is valued. They encourage the expression of diverse opinions and perspectives, while fostering collegiality.",
            para4Title: "Collaborate with Partners and Stakeholders: ",
            para4:
              "Managers are deliberate and resourceful about seeking a wide spectrum of perspectives. In building partnerships, they manage expectations and aim to reach consensus. They demonstrate openness and flexibility to improve outcomes and bring a whole-of-organization perspective to their interactions. Managers acknowledge the role of partners in achieving outcomes.",
            para5Title: "Promote Innovation and Guide Change: ",
            para5:
              "Managers create an environment that supports bold thinking, experimentation and intelligent risk taking. When implementing change, managers maintain momentum, address resistance and anticipate consequences. They use setbacks as a valuable source of insight and learning.",
            para6Title: "Achieve Results: ",
            para6:
              "Managers ensure that they meet team objectives by managing resources. They anticipate, plan, monitor progress and adjust as needed. They demonstrate awareness of the context when making decisions. Managers take personal responsibility for their actions and the outcomes of their decisions."
          }
        }
      },

      //Background Page
      background: {
        hiddenTabNameComplementary: 'under "Background Information"',
        orgCharts: {
          link: "Image Description",
          ariaLabel: "Image description of the organization chart",
          treeViewInstructions:
            "Below is a tree view of the organization chart. Once selected, you can use the arrow keys to navigation, expand, and collapse information."
        }
      },

      //Inbox Page
      inboxPage: {
        tabName: 'under "In-Box"',
        emailId: " email ",
        subject: "Subject",
        to: "To",
        from: "From",
        date: "Date",
        addReply: "Add email response",
        addTask: "Add task list",
        yourActions: `You responded with {0} emails and {1} tasks`,
        editActionDialog: {
          addEmail: "Add email response",
          editEmail: "Edit email response",
          addTask: "Add task list",
          editTask: "Edit task",
          save: "Save response"
        },
        characterLimitReached: "(Limit reached)",
        emailCommons: {
          to: "To:",
          toFieldSelected: "To field selected.",
          cc: "CC:",
          ccFieldSelected: "Cc field selected.",
          currentSelectedPeople: "Current selected people are: {0}",
          currentSelectedPeopleAreNone: "None",
          reply: "Reply",
          replyAll: "Reply all",
          forward: "Forward",
          editButton: "Edit response",
          deleteButton: "Delete response",
          originalEmail: "Original email",
          toAndCcFieldsPlaceholder: "Select from address book",
          yourResponse: "Your response"
        },
        addEmailResponse: {
          selectResponseType: "Please select how you would like to respond to the original email:",
          response: "Your email response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit",
          emailResponseTooltip: "Write a response to the email you received.",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information",
          invalidToFieldError: "This field cannot be empty"
        },
        emailResponse: {
          title: "Email Response #",
          description: "For this response, you've chosen to:",
          response: "Your email response:",
          reasonsForAction: "Your reasons for action (optional):"
        },
        addEmailTask: {
          header: "Email #{0}: {1}",
          task: "Your task list response: {0} character limit",
          reasonsForAction: "Your reasons for actions (optional): {0} character limit"
        },
        taskContent: {
          title: "Task List Response #",
          task: "Your task list response:",
          taskTooltipPart1: "An action you intend to take to address a situation in the emails.",
          taskTooltipPart2: "Example: Planning a meeting, asking a colleague for information.",
          reasonsForAction: "Your reasons for action:",
          reasonsForActionTooltip:
            "Here, you can explain why you took a specific action in response to a situation if you feel you need to provide additional information"
        },
        deleteResponseConfirmation: {
          title: "Are you sure you want to cancel this response?",
          systemMessageTitle: "Warning!",
          systemMessageDescription:
            "Your response will not be saved if you proceed. If you wish to save your answer, you may return to the response. All of your responses can be edited or deleted before submission.",
          description:
            'If you do not wish to save the response, click the "Delete response" button.'
        }
      },

      //Lock Screen
      lockScreen: {
        tabTitle: "Test Locked",
        description: {
          part1: "Your test has been locked by the test administrator.",
          part2: "Your test timer is on hold.",
          part3: "Your most recent responses have been saved.",
          part4: "Please see your test administrator for further instructions."
        }
      },

      //Pause Screen
      pauseScreen: {
        tabTitle: "Test Paused",
        description: {
          part1:
            "Your test has been paused by the test administrator. It will resume once the timer reaches 00:00:00.",
          part2: "Your test timer is on hold.",
          part3: "Your most recent responses have been saved."
        }
      },

      //Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Congratulations! Your test has been submitted.",
        feedbackSurvey:
          "We would appreciate your feedback on the assessment. Please fill out this optional {0} before logging out and leaving.",
        optionalSurvey: "15 minute survey",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=en-US",
        logout:
          "For security reasons, please ensure you log out of your account in the top right corner of this page. You may quietly gather your belongings and leave the test session. If you have any questions or concerns about your test, please contact {0}.",
        thankYou: "Thank you for completing your assessment. Good luck!"
      },

      //Quit Confirmation Page
      quitConfirmationPage: {
        title: "You have quit the test",
        instructionsTestNotScored1: "Your test ",
        instructionsTestNotScored2: "will not be scored.",
        instructionsRaiseHand:
          "Please raise your hand. The test administrator will come see you with further instructions.",
        instructionsEmail:
          "If you have any questions or concerns about your test, please contact {0}."
      },

      //Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Your test time is over...",
        timeoutSaved:
          "Your responses have been saved and submitted for scoring. Please note that the information in the notepad is not saved.",
        timeoutIssue:
          'If there was an issue, please advise your test administrator. Click "Continue" to exit this test session and to receive further instructions.'
      },

      //Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Background Information",
        inboxTabTitle: "In-Box",
        disabled: "You cannot access the inbox until you start the test.",
        nextButton: "Next",
        previousButton: "Previous"
      },

      //Test Footer
      testFooter: {
        timer: {
          timer: "Timer",
          timeLeft: "Time left in test session:",
          timerHidden: "Timer hidden.",
          timerShow: "Show timer",
          timerHide: "Hide timer"
        },
        submitTestPopupBox: {
          title: "Confirm test submission?",
          warning: {
            title: "Warning! The notepad will not be saved.",
            message:
              "Anything written in the notepad will not be submitted with the test for scoring. Ensure that you have reviewed all of your responses before submitting the test as you will not be able to go back to make changes."
          },
          description:
            "If you are ready to send your test in for scoring, click the “Submit Test” button. You will be exited out of this test session and provided further instructions."
        },
        quitTestPopupBox: {
          title: "Are you sure you want to quit this test?",
          description:
            "All answers will be deleted. You will not be able to recover your answers, and will forfeit from this assessment. To quit, you must acknowledge the following:",
          checkboxOne: "I voluntarily withdraw from this examination",
          checkboxTwo: "My test will not be scored",
          checkboxThree:
            "I am aware that the retest period for this test may apply, should I wish to write this test again"
        }
      }
    },

    //Screen Reader
    ariaLabel: {
      mainMenu: "Main Menu",
      tabMenu: "eMIB Tab Menu",
      instructionsMenu: "Instructions Menu",
      languageToggleBtn: "language-toggle-button",
      authenticationMenu: "Authentication Menu",
      emailHeader: "email header",
      responseDetails: "response details",
      reasonsForActionDetails: "reasons for action details",
      taskDetails: "task details",
      emailOptions: "email options",
      taskOptions: "task options",
      taskTooltip: "task tooltip",
      emailResponseTooltip: "email response tooltip",
      reasonsForActionTooltip: "reasons for action tooltip",
      passwordConfirmationRequirements: "It must match your password",
      dobDayField: "Day field selected",
      dobMonthField: "Month field selected",
      dobYearField: "Year field selected",
      emailsList: "Emails list",
      questionList: "Question list",
      topNavigationSection: "Top navigation",
      sideNavigationSection: "Side navigation",
      notepadSection: "Notepad section",
      quitTest: "Quit test",
      addPermission: "Add permission button",
      selectedPermission: "Selected permission:",
      description: "Description:",
      pendingStatus: "Pending status:",
      add: "Add",
      scorerPanel: "Scorer Panel"
    },

    //Commons
    commons: {
      psc: "Public Service Commission",
      nextButton: "Next",
      backButton: "Back",
      enterEmibSample: "Proceed to sample e-MIB test",
      enterEmibReal: "Continue to test instructions",
      resumeEmibReal: "Resume e-MIB",
      startTest: "Start test",
      resumeTest: "Resume test",
      confirmStartTest: {
        aboutToStart: "You are about to start the timed test.",
        newtimerWarning: `You will have {0} to complete the test section.`,
        timerWarning: `After clicking start, you'll be taken to the "Background Information" tab. You will have {0} to complete the test.`,
        instructionsAccess:
          "You will have access to the instructions from within the test. Good luck!",
        timeUnlimited: "unlimited time",
        numberMinutes: "{0} minutes",
        confirmProceed: "Are you sure you wish to proceed?"
      },
      submitTestButton: "Submit Test",
      quitTest: "Quit Test",
      returnToTest: "Return to test",
      returnToResponse: "Return to response",
      passStatus: "Pass",
      failStatus: "Fail",
      error: "Error",
      enabled: "Enabled",
      disabled: "Disabled",
      backToTop: "Back to top",
      notepad: {
        title: "Hide/Show Notepad",
        placeholder: "Put your notes here..."
      },
      cancel: "Cancel",
      cancelChanges: "Cancel changes",
      cancelResponse: "Cancel response",
      deleteButton: "Delete",
      saveButton: "Save",
      applyButton: "Apply",
      close: "Close",
      login: "Login",
      ok: "Ok",
      deleteConfirmation: "Delete Confirmation",
      continue: "Continue",
      sendRequest: "Send request",
      na: "N/A",
      valid: "valid",
      confirm: "Confirm",
      none: "None",
      english: "English",
      french: "French",
      bilingual: "Bilingual",
      status: {
        checkedIn: "Checked-in",
        ready: "Ready",
        active: "Testing",
        locked: "Locked",
        paused: "Paused"
      }
    }
  },

  fr: {
    //Main Tabs
    mainTabs: {
      homeTabTitleUnauthenticated: "Accueil",
      homeTabTitleAuthenticated: "Accueil",
      dashboardTabTitle: "Tableau de bord",
      sampleTest: "Échantillon de la BRG-e",
      sampleTests: "Échantillons de tests",
      statusTabTitle: "Statut",
      psc: "Commission de la fonction publique du Canada",
      canada: "Gouvernement du canada",
      skipToMain: "Passer au contenu principal",
      menu: "MENU"
    },

    //HTML Page Titles
    titles: {
      CAT: "OÉC/CAT - CFP/PSC",
      sampleTests: "OÉC - Échantillons de tests",
      eMIBOverview: "Aperçu de la BRG-e",
      eMIB: "Évaluation BRG-e ",
      uitTest: "FR UIT Assessment",
      status: "OÉC - État du system",
      home: "OÉC - Accueil",
      homeWithError: "Erreur - OÉC - Accueil",
      profile: "CAT - Profile",
      systemAdministration: "FR CAT - System Administration",
      testAdministration: "OÉC - Séances de test",
      ppcAdministration: "FR CAT - PPC Administration",
      websocketConnectionError: "FR CAT - Websocket Connection Error"
    },

    accommodations: {
      notificationPopup: {
        title: "FR Accessibility Display Settings",
        description1: "FR The display settings currently in effect have been saved.",
        description2: "FR When you log in, these settings will be automatically applied."
      },
      defaultFonts: {
        fontSize: "16px (défaut)",
        fontFamily: "Nunito Sans (défaut)"
      }
    },

    //authentication
    authentication: {
      login: {
        title: "Connexion",
        content: {
          title: "Connexion",
          description:
            "Un compte est nécessaire pour continuer. Pour ouvrir une session, entrez votre adresse courriel et votre mot de passe.",
          inputs: {
            emailTitle: "Adresse courriel :",
            passwordTitle: "Mot de passe :"
          }
        },
        button: "Connexion",
        invalidCredentials: "Identifiants invalides!",
        passwordFieldSelected: "Champ Mot de passe sélectionné"
      },
      createAccount: {
        title: "Créer un compte",
        content: {
          title: "Créer un compte",
          description:
            "Un compte est nécessaire pour continuer. Pour créer un compte, remplissez le formulaire suivant.",
          inputs: {
            valid: "valide",
            firstNameTitle: "Prénom :",
            firstNameError: "Doit être un prénom valide",
            lastNameTitle: "Nom de famille :",
            lastNameError: "Doit être un nom de famille valide",
            dobDayTitle: "Date de naissance :",
            dobError: "Doit être une date valide",
            psrsAppIdTitle: "FR PSRS Applicant ID",
            psrsAppIdError: "FR Must be a valid PSRS Applicant ID",
            emailTitle: "Adresse courriel :",
            emailError: "Doit être une adresse courriel valide",
            priOrMilitaryNbrTitle: "CIDP ou numéro matricule (s’il y a lieu) :",
            priOrMilitaryNbrError: "Doit être un CIDP ou numéro matricule valide",
            passwordTitle: "Mot de passe :",
            passwordErrors: {
              description: "Votre mot de passe doit satisfaire aux exigences suivantes :",
              upperCase: "Au moins une lettre majuscule",
              lowerCase: "Au moins une lettre minuscule",
              digit: "Au moins un chiffre",
              specialCharacter: "Au moins un caractère spécial (#?!@$%^&*-)",
              length: "Un minimum de 8 caractères et un maximum de 15 caractères "
            },
            passwordConfirmationTitle: "Confirmer le mot de passe :",
            passwordConfirmationError:
              "Votre confirmation de mot de passe doit correspondre au mot de passe"
          }
        },
        privacyNotice:
          "J'ai lu et accepté la façon dont la Commission de la fonction publique recueille, utilise et divulgue des renseignements personnels, comme énoncées dans {0}.",
        privacyNoticeLink: "l'avis de confidentialité",
        privacyNoticeError: "Vous devez accepter l’avis de confidentialité en cliquant sur la case",
        button: "Créer un compte",
        accountAlreadyExistsError: "Un compte est déjà associé à cette adresse de courriel",
        passwordTooCommonError: "Ce mot de passe est trop commun",
        passwordTooSimilarToUsernameError:
          "Le mot de passe est trop semblable au nom d’utilisateur",
        passwordTooSimilarToFirstNameError: "Le mot de passe est trop semblable au prénom",
        passwordTooSimilarToLastNameError: "Le mot de passe est trop semblable au nom de famille",
        passwordTooSimilarToEmailError: "Le mot de passe est trop semblable au courriel",
        privacyNoticeDialog: {
          title: "Confidentialité et sécurité",
          privacyNoticeStatement: "Énoncé de confidentialité — Étude pilote de la BRG-e",
          privacyParagraph1:
            "La Commission de la fonction publique du Canada (CFP) utilisera les renseignements personnels concernant le présent projet de recherche, lesquels comprennent les résultats obtenus aux tests et les évaluations des superviseurs, à des fins de recherche, d’analyse et d’élaboration de tests. Les résultats pourront éventuellement servir à des questions futures liées à la dotation avec votre consentement. Tous les renseignements sont recueillis en vertu des articles 11, 30, 35 et 36 de la Loi sur l’emploi dans la fonction publique conformément à la Loi sur la protection de renseignements personnels. La CFP s’engage à protéger le droit des personnes à la vie privée.",
          publicServiceEmploymentActLink: "Loi sur l’emploi dans la fonction publique",
          privacyActLink: "Loi sur la protection des renseignements personnels",
          privacyParagraph2:
            "Les « renseignements personnels » se définissent comme étant les renseignements, quels que soient leur forme et leur support, concernant un individu identifiable.",
          privacyCommissionerLink: "commissaire à la protection de la vie privée du Canada",
          privacyParagraph3:
            "L’utilisation et la collecte des résultats obtenus aux tests sont définies dans le fichier de renseignements personnels CFP-PPU-025. Les renseignements personnels liés à la recherche, à l’analyse et à l’élaboration de test sont conservés pendant 10 ans avant d’être détruits. Les réponses anonymes aux tests et les données démographiques sont conservées indéfiniment sous forme de fichiers informatisés.",
          privacyParagraph4:
            "La participation est facultative. Cependant, si vous décidez de ne pas fournir vos renseignements personnels, vous ne pourrez pas participer à cette étude.",
          privacyParagraph5:
            "Toute personne a droit à la protection et à la consultation de ses renseignements personnels, et est en droit de demander que des corrections y soient apportées si elle estime qu’il y a une erreur ou une omission. Toute personne souhaitant consulter ses renseignements personnels ou y apporter des corrections peut communiquer avec la Division de l’accès à l’information et de la protection des renseignements personnels de la CFP.",
          privacyParagraph6:
            "Les renseignements personnels recueillis par la CFP sont protégés contre toute communication à des personnes non autorisées ou à des ministères et organismes assujettis aux dispositions de la Loi sur la protection des renseignements personnels. Cependant, les renseignements personnels peuvent être communiqués sans votre consentement dans des circonstances particulières, conformément à l’article 8 de la Loi sur la protection des renseignements personnels.",
          accessToInformationLink:
            "Division de l’Accès à l’information et de la protection des renseignements personnels",
          privacyParagraph7:
            "Toute personne a le droit de déposer une plainte auprès du Commissaire à la protection de la vie privée du Canada concernant le traitement de ses renseignements personnels par un ministère.",
          infoSourceChapterLink: "chapitre d’Info Source",
          reproductionTitle: "Reproduction ou divulgation non autorisées du contenu du test",
          reproductionWarning:
            "Ce test et son contenu portent le niveau de sécurité Protégé B. La reproduction ou l'enregistrement du contenu de ce test, sous quelque forme que ce soit, sont strictement interdits. Tous les documents liés au test, y compris les brouillons, doivent être remis à l'administrateur du test à la fin de celui-ci. La reproduction, l'enregistrement ou la divulgation non autorisées du contenu du test contreviennent à la Politique du gouvernement sur la sécurité, et l'utilisation de renseignements obtenus ou transmis de manière inappropriée peut constituer une infraction à la Loi sur l'emploi dans la fonction publique (LEFP). Les parties impliquées dans la divulgation ou l'utilisation inappropriée de contenu de test protégé pourraient faire l'objet d'une enquête en vertu de la LEFP. Au terme de cette enquête, les personnes reconnues coupables de fraude pourraient faire l'objet d'une déclaration de culpabilité par procédure sommaire ou voir leur dossier renvoyé à la Gendarmerie royale du Canada.",
          cheatingTitle: "Tricherie",
          cheatingWarning:
            "Veuillez prendre note que tous les cas présumés de tricherie seront renvoyés au gestionnaire responsable et au Centre de psychologie du personnel, qui prendront les mesures nécessaires. En cas de tricherie présumée, les résultats de test pourraient être invalidés, et les parties impliquées pourraient faire l'objet d'une enquête en vertu de la LEFP. Au terme de cette enquête, les personnes reconnues coupables de fraude pourraient faire l'objet d'une déclaration de culpabilité par procédure sommaire ou voir leur dossier renvoyé à la Gendarmerie royale du Canada."
        }
      }
    },

    //Menu Items
    menu: {
      scorer: "Évaluateur",
      etta: "Administrateur du système",
      ppc: "Administrateur du CPP",
      ta: "Administrateur de test",
      testBuilder: "FR Test Builder",
      checkIn: "M'enregistrer",
      profile: "Mon profil",
      incidentReport: "Rapports d'incident",
      MyTests: "Mes tests",
      ContactUs: "Nous joindre",
      logout: "Déconnexion"
    },

    //Token Expired Popup Box
    tokenExpired: {
      title: "Session expirée",
      description:
        "Votre session a pris fin en raison de votre inactivité. Entrez votre courriel et mot de passe pour ouvrir une nouvelle session."
    },

    //Websocket Connection Error Page
    websocketConnectionErrorPage: {
      title: "FR Connection Error",
      description1:
        "FR Something went wrong between your computer and our server. If you are in the middle of a test, please let the test administrator know right away!"
    },

    //Home Page
    homePage: {
      welcomeMsg: "Bienvenue dans l'Outil d'évaluation des candidats",
      description:
        "Ce site Web est utilisé pour évaluer les candidats pour des postes au sein de la fonction publique fédérale. Pour accéder à vos tests, vous devez ouvrir une session ci-dessous. Si vous n’avez pas de compte, vous pouvez en créer un à l’aide de votre adresse de courriel."
    },

    //Sample Tests Page
    sampleTestDashboard: {
      title: "Échantillons de tests",
      description:
        "Vous trouverez ci-dessous la liste des échantillons de tests qui sont disponibles. Les échantillons de tests ne sont pas corrigés et les réponses ne sont pas soumises à la fin d’un test. Cliquer sur « Afficher » pour accéder à l’échantillon de test.",
      table: {
        columnOne: "Nom du test",
        columnTwo: "Action",
        viewButton: "Afficher",
        viewButtonAccessibilityLabel: "FR View {0} test"
      }
    },

    //Dashboard Page
    dashboard: {
      title: "Bienvenue, {0} {1}.",
      description:
        "Vous êtes connecté à votre compte. Si vous devez passer un test, veuillez attendre les instructions de l'administrateur de test.",
      table: {
        columnOne: "Nom du test",
        columnTwo: "Action",
        viewButton: "Afficher",
        viewButtonAccessibilityLabel: "FR View {0} test",
        noTests: "Il n’y a aucun test affecté à votre compte"
      }
    },

    //Checkin action
    candidateCheckIn: {
      button: "M'enregistrer",
      loading: "Recherche de tests actifs...",
      checkedInText: "Vous vous êtes enregistré avec le code d'accès au test : ",
      popup: {
        title: "Saisissez votre code d'accès au test",
        description:
          "Veuillez saisir le code d'accès au test fourni par l'administrateur de test dans le champ ci-dessous. Ensuite, cliquez sur « M'enregistrer ».",
        textLabel: "Code d'accès au test :",
        textLabelError: "Saisissez un code d'accès au test valide"
      }
    },

    //Test Builder Page
    testBuilder: {
      containerLabel: "FR Test Builder",
      goBackButton: "FR Back to Test Builder",
      inTestView: "FR In-Test View",
      errors: {
        nondescriptError:
          "FR Non descript error. Report this to IT. Maybe a user is logged into the adminconsole.",
        cheatingPopup: {
          title: "FR Caught Cheating Attempt",
          description:
            "FR You are not allowed to switch or open new tabs. Any attempt to minimize the screen or change focus will be considered a cheating attempt.",
          warning:
            "FR After {0} more detections your test will be terminated and your results invalidated. If this was an error, alert your TA."
        }
      },
      testDefinitionSelectPlaceholder: "FR Select a Test Section to View",
      sideNavItems: {
        testDefinition: "FR Test Definition",
        testSections: "FR Test Sections",
        testSectionComponents: "FR Test Section Components",
        sectionComponentPages: "FR Section Component Pages",
        componentPageSections: "FR Component Page Sections",
        questions: "FR Questions",
        questionBlockTypes: "FR Question Block Types",
        competencyTypes: "FR Competency Types",
        answers: "FR Answers",
        addressBook: "FR Address Book Contacts"
      },
      //some values are underscored because python uses underscores
      testDefinition: {
        title: "FR Test Definition",
        description: "FR These are the top level properties of a test.",
        activate: {
          button: "FR Activate",
          popupTitle: "FR Activate Test Definition",
          popupContent:
            "FR Activating a Test Definition version will make this version available to be assigned to Test Adminstrators. Are you sure you wish to continue?"
        },
        deactivate: {
          button: "FR Deactivate",
          popupTitle: "FR Deactivate Test Definition",
          popupContent:
            "FR Deactivating a Test Definition version will remove this version from being available to be assigned to Test Adminstrators. Are you sure you wish to continue?"
        },
        deleteTestDefinition: {
          title: "FR Delete this Test Definition",
          description:
            "FR Deleting a Test Definition will erase all data associated to this test. Are you sure you wish to continue?"
        },
        createTestDefinition: {
          button: "FR Create a New Test Definition",
          title: "FR Create a New Test Definition",
          description:
            "FR Creating a new Test Definition will erase any data that you have created and not uploaded. Are you sure you wish to continue?"
        },
        uploadTestDefinition: {
          button: "FR Upload",
          title: "FR Upload Test Definition",
          description:
            "FR Uploading this test definition will create the new version. Are you sure you wish to continue?"
        },
        uploadJSONTestDefinition: {
          button: "FR Upload JSON",
          title: "FR Upload JSON Test Definition",
          description: "FR Please paste the JSON to upload below."
        },
        JSONUpload: {
          title: "JSON",
          titleTooltip:
            "FR Paste the JSON download from a Test Builder. This can be from different environments.",
          errorMessage: "FR Not valid JSON"
        },
        downloadTestDefinition: {
          button: "FR Download JSON"
        },
        selectTestDefinition: {
          button: "FR Select a Test Definition",
          title: "FR Test Definition",
          description: "FR A list of all available test definitions to modify.",
          titleTooltip: "FR Select a test definition to modify.",
          header: "FR Select a Test Definition"
        },
        test_code: {
          title: "FR Test Code",
          titleTooltip: "FR Unique Identifier for a test definition",
          errorMessage: "FR Not a valid Test Code"
        },
        version: {
          title: "FR Version Number",
          titleTooltip: "FR the latest version of the test definition",
          errorMessage: "FR Not a valid Version Number"
        },
        fr_name: {
          title: "FR French Name of the test",
          titleTooltip: "FR Name display to candidates when they have an active test",
          errorMessage: "FR Not a valid Name"
        },
        en_name: {
          title: "FR English Name of the test",
          titleTooltip: "FR Name display to candidates when they have an active test",
          errorMessage: "FR Not a valid Name"
        },
        is_public: {
          title: "FR Public",
          titleTooltip: "FR Is this a publicly available test?",
          errorMessage: "FR not a valid choice"
        },
        active: {
          title: "FR Active",
          titleTooltip: "FR Is this test active and available to use?",
          errorMessage: "FR not a valid choice"
        },
        test_language: {
          title: "FR Test Language",
          titleTooltip: "FR Is this test presented in a single language?",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        }
      },
      testSection: {
        title: "FR Test Section",
        description:
          "FR These are the test sections belonging to the Test Definition. an example of a Test Section would be the Overview page, the Instruction Page, or the Quit page of a test",
        collapsableItemName: "FR Test Section {0}: {1}",
        addButton: "FR Add Test Section",
        delete: {
          title: "FR Delete Test Section Component",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        fr_title: {
          title: "FR French Name of the test section",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "FR English Name of the test section",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        uses_notepad: {
          title: "FR Notepad",
          titleTooltip: "FR Does this test section use the notepad component?",
          errorMessage: "FR not a valid choice"
        },
        block_cheating: {
          title: "FR Block Cheating",
          titleTooltip: "FR Should this test section report the user for switching tabs etc.?",
          errorMessage: "FR not a valid choice"
        },
        scoring_type: {
          title: "FR Scoring Method",
          titleTooltip:
            "FR Set the correct scoring method for the components in this test section.",
          errorMessage: "FR not a valid choice"
        },
        default_time: {
          title: "FR Default Time",
          titleTooltip:
            "FR How long does a candidate have on this section? If the section is not timed, then leave this empty. This field can be overridden by a TA",
          errorMessage: "FR Only numbers are allowed"
        },
        default_tab: {
          title: "FR Default Tab",
          titleTooltip: "FR Which top tab should be displayed when a candidate begins this section",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        order: {
          title: "FR Section Order",
          titleTooltip: "FR The order in the test that this section appears.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        next_section_button: {
          title: "FR Next Section Button",
          titleTooltip: "FR Define a button to take the user to the next section.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "0"
        },
        reducers: {
          title: "FR Reducers",
          titleTooltip:
            "FR Which aspects of the test should be cleared when a user navigates to this section?",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        },
        section_type: {
          title: "FR Section Type",
          titleTooltip:
            "FR The type of test section. Such as: Single page, Top Tab navigation, Finish, Quit...",
          errorMessage: "FR Not a valid Type"
        },
        deletePopup: {
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      nextSectionButton: {
        header: "FR Next Section Button",
        button_type: {
          title: "FR Button Type",
          titleTooltip: "FR This is selected based on the test section component."
        },
        confirm_proceed: {
          title: "FR Confirmation Box",
          titleTooltip:
            "FR A checkbox the user has to click before being allowed to press the action button."
        },
        button_text: {
          title: "FR Button Text",
          titleTooltip: "FR The text that the button displays to the user"
        },
        title: {
          title: "FR Popup Title",
          titleTooltip:
            "FR The text that appears on the header of the popup when the next section button is clicked."
        },
        content: {
          title: "FR Popup Content",
          titleTooltip:
            "FR The text that appears in the body of the popup when the next section button is clicked."
        }
      },
      testSectionComponents: {
        title: "FR Test Section Components",
        description:
          "FR These are the test section components belonging to the Test Section below. an example of a Test Section Component would be a Question List, Inbox, Side Navigation, or Single Page appearance/component.",
        collapsableItemName: "FR Test Section Component {0}: {1}",
        addButton: "FR Add Test Section Component",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        parentTestSection: {
          title: "FR Parent Test Section",
          titleTooltip: "FR The test section that the displayed objects appear in."
        },
        fr_title: {
          title: "FR French Name",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "FR English Name",
          titleTooltip: "FR Hidden name used for easy identification by internal users",
          errorMessage: "FR Not a valid Name"
        },
        order: {
          title: "FR Order",
          titleTooltip: "FR The order in which this component appears in a Top Tab navigation.",
          errorMessage: "FR Not a valid Name"
        },
        component_type: {
          title: "FR Component Type",
          titleTooltip: "FR The type of component. Such as: Inbox, Single Page, Side Navigation...",
          errorMessage: "FR Not a valid Type"
        },
        shuffle_all_questions: {
          title: "FR Shuffle Questions",
          titleTooltip:
            "FR Shuffle the questions in this question list? Question dependencies remain in the order they were given."
        },
        language: {
          title: "FR Content Language",
          titleTooltip:
            "FR Which language will the content in this section be provided in? This is mandatory for screen readers to use the correct accent.",
          errorMessage: "FR not a valid choice",
          selectPlaceholder: "--"
        }
      },
      questionListRules: {
        title: "FR Block Type Rules",
        collapsableItemName: "FR Rule {0}",
        addButton: {
          title: "FR Add Rule",
          titleTooltip: "FR You must create Question Block Types in order to add rules."
        },
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        number_of_questions: {
          title: "FR Number of Questions",
          titleTooltip: "FR The number of questions to select of the following question block type."
        },
        question_block_type: {
          title: "FR Question Block Type",
          titleTooltip: "FR The question block type from which to select questions."
        },
        order: {
          title: "FR Order",
          titleTooltip:
            "FR The order that this rule will appear in the question list. If there is no order, leave this field empty."
        },
        shuffle: {
          title: "FR Shuffle Questions",
          titleTooltip:
            "FR Shuffle the questions in this rule? Question dependencies remain in the order they were given."
        }
      },
      sectionComponentPages: {
        title: "FR Section Component Pages",
        description:
          "FR These are the section component pages belonging to the Test Section and Section Component below. A Section Component Page must exist for each section component where you want to display information (markdown, sample email, images). If this is combined with a Side Navigation Component, the titles will be used as the navigation buttons on the side panel",
        addButton: "FR Add Section Component Page",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        collapsableItemName: "FR Section Component Page {0}: {1}",
        parentTestSectionComponent: {
          title: "FR Parent Test Section Component",
          titleTooltip: "FR The parent test section the displayed objects are displayed in."
        },
        fr_title: {
          title: "FR French Name",
          titleTooltip:
            "FR Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "FR Not a valid Name"
        },
        en_title: {
          title: "FR English Name",
          titleTooltip:
            "FR Name of the side navigation tab when the site language is French. This field is hidden when the Test Section Component is SINGLE_PAGE.",
          errorMessage: "FR Not a valid Name"
        },
        order: {
          title: "FR Order",
          titleTooltip: "FR The order in which this page appears in a side navigation panel.",
          errorMessage: "FR Not a valid Name"
        }
      },
      componentPageSections: {
        title: "FR Component Page Sections",
        description:
          "FR These are the Component Page Sections related to the parent objects below. An example of a Component Page Section is a sample email, markdown, a sample email response, an image, etc. They are the components that make up what is visually shown to a user on information pages.",
        collapsableItemName: "FR Section Component Page Order: {0}, Type: {1}",
        parentSectionComponentPage: {
          title: "FR Parent Section Component Page",
          titleTooltip:
            "FR The parent side navigation tab that the objects displayed will belong to."
        },
        pageSectionLaguage: {
          title: "FR Language",
          titleTooltip:
            "FR If a test is bilingual, you must create page sections for each language. This will show all the page sections created for the selected language."
        },
        addButton: "FR Add Section Component Page",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "FR Order",
          titleTooltip: "FR The order in which this page appears in a side navigation panel.",
          errorMessage: "FR Not a valid Name"
        },
        page_section_type: {
          title: "FR Page Section Type",
          titleTooltip:
            "FR The type of section to display (i.e. Markdwon, sample email, sample teask response).",
          errorMessage: "FR Not a valid type."
        }
      },
      markdownPageSection: {
        content: {
          title: "FR Content",
          titleTooltip: "FR The markdown content to display."
        }
      },
      sampleEmailPageSection: {
        email_id: {
          title: "FR Email ID",
          titleTooltip: "FR The email ID to display"
        },
        from_field: {
          title: "FR From",
          titleTooltip: "FR Who the email is from."
        },
        to_field: {
          title: "FR To",
          titleTooltip: "FR Who the email is to"
        },
        date_field: {
          title: "FR Date",
          titleTooltip: "FR The date displayed on the email."
        },
        subject_field: {
          title: "FR Subject",
          titleTooltip: "FR The subject displayed on the email."
        },
        body: {
          title: "FR Body",
          titleTooltip: "FR The body of the email to display."
        }
      },
      sampleEmailResonsePageSection: {
        cc_field: {
          title: "FR CC",
          titleTooltip: "FR Who is CC'd on the email"
        },
        to_field: {
          title: "FR To",
          titleTooltip: "FR Who the email is to"
        },
        response: {
          title: "FR Response",
          titleTooltip: "FR The email response."
        },
        reason: {
          title: "FR Reason",
          titleTooltip: "FR The reason for responding with this email."
        }
      },
      sampleTaskResonsePageSection: {
        response: {
          title: "FR Response",
          titleTooltip: "FR The task response."
        },
        reason: {
          title: "FR Reason",
          titleTooltip: "FR The reason for responding with this task."
        }
      },
      zoomImagePageSection: {
        small_image: {
          title: "FR Full Image Name",
          titleTooltip: "FR name of the image file including extension."
        },
        note: "FR Note",
        noteDescription:
          "FR In order for this image to be displayed properly you must provide the image to the IT team to upload to the application server."
      },
      treeDescriptionPageSection: {
        address_book_contact: {
          title: "FR Root Contact",
          titleTooltip: "FR The root contact that the tree description will start from."
        }
      },
      addressBook: {
        topTitle: "FR Address Book",
        description:
          "FR These are email like contacts used in Inbox style Test Section Components and Tree Description Component Page Sections.",
        collapsableItemName: "{0}",
        addButton: "FR Add Address Book Contact",
        name: {
          title: "FR Name",
          titleTooltip: "FR The name of the contact."
        },
        title: {
          title: "FR Title",
          titleTooltip: "FR The title of the contact."
        },
        parent: {
          title: "FR Parent Contact",
          titleTooltip: "FR The contact that this contact reports to."
        },
        test_section: {
          title: "FR Test Sections",
          titleTooltip:
            "FR Which test sections can a user send emails to this contact or if there is a tree description in a test section then this contact must be in that test section."
        }
      },
      questions: {
        topTitle: "FR Questions",
        description:
          "FR These are questions used in an Email Inbox or a Question List. Questions can be emails or multiple choice for example.",
        collapsableItemName: "FR Question {0} of Block: {1}",
        emailCollapsableItemName: "FR Email ID: {0}, From: {1}",
        addButton: "FR Add Question",
        searchResults: "{0} Question(s) found",
        pilot: {
          title: "FR Pilot Question",
          titleTooltip:
            "FR Is the question a pilot question? It's answer will not be counted towards a final score.",
          description: ""
        },
        questionBlockSelection: {
          title: "FR Question Block Type",
          titleTooltip: "FR The Question Block Type to narrow the number of results."
        },
        question_type: {
          title: "FR Question Type",
          titleTooltip:
            "FR i.e. multiple choice, email. This is pre-determined by the Test Section Component Type (Question List or Inbox)"
        },
        question_block_type: {
          title: "FR Question Block Type",
          titleTooltip:
            "FR The question block type identifier. Used to determine what questions are returned for a question lists rule set."
        },
        dependencies: {
          title: "FR Dependents",
          titleTooltip: "FR Questions that this question must be presented with (in order)."
        },
        order: {
          title: "FR Dependent Order",
          titleTooltip:
            "FR The order this question is presented in when dependents exist. This is only used for ordering questions in a dependency sitaution."
        }
      },
      questionSituations: {
        situation: {
          title: "FR Situation",
          titleTooltip:
            "FR The situation text to be displayed at the top of the question when a test is viewed in scorer mode."
        }
      },
      questionBlockTypes: {
        topTitle: "FR Question Block Types",
        description:
          "FR These are types associated with questions. These types are used to form rules for question lists. For example if you have created the question types of EASY and HARD you could create a rule for a question list the would select 10 EASY and 10 HARD questions.",
        collapsableItemName: "FR Question Block Type: {0}",
        addButton: "FR Add Question Block Type",
        name: {
          title: "FR Name",
          titleTooltip: "FR The unique identifier for a question block type."
        }
      },
      competencyTypes: {
        topTitle: "FR Competency Types",
        description:
          "FR These are types associated with questions. These types are assigned to questions for manual marking.",
        collapsableItemName: "FR Competency Type: {0}",
        addButton: "FR Add Competency Type",
        en_name: {
          title: "FR English Name",
          titleTooltip: "FR The unique identifier for a question block type."
        },
        fr_name: {
          title: "FR French Name",
          titleTooltip: "FR The unique identifier for a question block type."
        },
        max_score: {
          title: "FR Max Score",
          titleTooltip: "FR The max score a candidate can get for this competency."
        }
      },
      questionSections: {
        title: "FR Question Sections",
        collapsableItemName: "FR Question Section {0}",
        description:
          "FR These are the test section components belonging to the Test Section below. an example of a Test Section Component would be a Question List, Inbox, Side Navigation, or Single Page appearance/component.",
        addButton: "FR Add Question Section",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        order: {
          title: "FR Order",
          titleTooltip: "FR The order in which this component appears in a Top Tab navigation.",
          errorMessage: "FR Not a valid Name"
        },
        question_section_type: {
          title: "FR Section Type",
          titleTooltip: "FR The type of section. Such as: Markdown, Image, Video...",
          errorMessage: "FR Not a valid Type"
        }
      },
      answers: {
        title: "FR Answers",
        collapsableItemName: "FR Scoring Value {0}",
        addButton: "FR Add Answer",
        delete: {
          title: "FR Delete",
          description:
            "FR Deleting an element will delete all child elements as well. Are you sure you wish to continue?"
        },
        content: {
          title: "FR content",
          titleTooltip: "FR The text to display for this answer.",
          errorMessage: "FR Not a valid input"
        },
        scoring_value: {
          title: "FR Scorng Value",
          titleTooltip:
            "FR The score to be added to the total test score when this answer is selected.",
          errorMessage: "FR Not a valid value"
        }
      },
      questionSectionMarkdown: {
        content: {
          title: "FR Content",
          titleTooltip: "FR Markdown to appear on the question."
        },
        delete: {
          title: "FR Delete Confirmation",
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      questionSectionEnd: {
        content: {
          title: "FR Specific Question",
          titleTooltip:
            "FR The literal question after context to be answered. This allows screen readers to navigate directly to this section."
        },
        delete: {
          title: "FR Delete Confirmation",
          content:
            "FR Deleting this item will delete all the content displayed. Are you sure you want to delete this object?"
        }
      },
      multipleChoiceQuestions: {
        header: "FR Question Sections"
      },
      emailQuestions: {
        scoringTitle: "FR Scoring",
        exampleRatingsTitle: "FR Example Ratings",
        competency_types: {
          title: "FR Competencies",
          titleTooltip: "FR The competencies being assessed in this question."
        },
        deleteDescription:
          "FR Deleting this item will delete the current object and any children this object may have. Are you sure you want to delete this object?",
        email_id: {
          title: "FR Email Id",
          titleTooltip:
            "FR Email Id is displayed at the top of the email and is the order in which emails are displayed."
        },
        to_field: {
          title: "FR To Field",
          titleTooltip: "FR The contact the email is to."
        },
        from_field: {
          title: "FR From Field",
          titleTooltip: "FR The Contact the email is from."
        },
        cc_field: {
          title: "FR CC Field",
          titleTooltip: "FR Contacts that are CC'd on this email."
        },
        date_field: {
          title: "FR Date",
          titleTooltip: "FR The date text to display on the email"
        },
        subject_field: {
          title: "FR Subject",
          titleTooltip: "FR The subject field to display on the email"
        },
        body: {
          title: "FR Body",
          titleTooltip: "FR The content of the email to display."
        }
      },
      situationRating: {
        score: {
          title: "FR Rating Score",
          titleTooltip:
            "FR This is the value that the example answered would be scored. Keep this to one decimal place."
        },
        example: {
          title: "FR Example Rationale",
          titleTooltip: "FR The Rationale behind giving the candidates answer this score."
        }
      }
    },
    //Test Administration Page
    testAdministration: {
      title: "Bienvenue, {0} {1}.",
      containerLabel: "FR Test Administration",
      sideNavItems: {
        testAccessCodes: "FR Test Access Codes",
        activeCandidates: "FR Active Candidates",
        reports: "FR Reports"
      },
      testAccessCodes: {
        description: "FR Manage the Test Access Codes you have generated.",
        table: {
          testAccessCode: "FR Test Access Code",
          test: "FR Test",
          staffingProcessNumber: "FR Staffing Process Number",
          action: "FR Disable",
          actionButton: "FR Disable",
          generateNewCode: "FR Generate Test Access Code"
        },
        generateTestAccessCodePopup: {
          title: "FR Enter Session Information",
          description:
            "FR Provide an order number, the test to administer and the test session language.",
          testOrderNumber: "FR Test Order Number:",
          testOrderNumberCurrentValueAccessibility: "FR Current selected test order number is:",
          testToAdminister: "FR Test to Administer:",
          testToAdministerCurrentValueAccessibility: "FR Current selected test to administer is:",
          testSessionLanguage: "FR Test Session Language",
          testSessionLanguageCurrentValueAccessibility:
            "FR Current selected test session language is:",
          billingInformation: {
            title: "FR Billing Information:",
            staffingProcessNumber: "FR Staffing Process Number:",
            departmentMinistry: "FR Department/Ministry Code:",
            contact: "FR Contact:",
            isOrg: "FR IS Org:",
            isRef: "FR IS Ref:"
          },
          generateButton: "FR Generate"
        },
        disableTestAccessCodeConfirmationPopup: {
          title: "FR Confirm Disable",
          description:
            "FR Once disabled, {0} Test Access Code will no longer be valid. Candidates will not be able to check-in using it.",
          warning: "FR Warning!"
        }
      },
      activeCandidates: {
        description:
          "FR As candidates Check-in, you will see their names in the list of Active Candidates below. After all candidates in attendance are Checked-in, disable the Test Access Code.",
        lockAllButton: "FR Lock All",
        unlockAllButton: "FR Unlock All",
        table: {
          candidate: "FR Candidate",
          dob: "Date de naissance",
          status: "FR Status",
          timer: "FR Total Time",
          actions: "FR Actions",
          actionTooltips: {
            updateTestTimer: "FR Update Test Time",
            approve: "FR Approve Candidate",
            unAssign: "FR Un-assign",
            lockPause: "FR Lock/Pause Test",
            unlockUnpause: "FR Unlock/Unpause Test",
            reSync: "FR Re-Sync",
            report: "FR Report Candidate"
          },
          updateTestTimerAriaLabel:
            "FR Update test timer of {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          approveAriaLabel:
            "FR Approve {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          unAssignAriaLabel:
            "FR Un-assign {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          lockPauseAriaLabel:
            "FR Lock/Pause {0} {1}'s test. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          unlockUnpauseAriaLabel:
            "FR Unlock/Unpause {0} {1}'s test. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          reSyncAriaLabel:
            "FR Re-sync {0} {1} user. His date of birth is {2}. His current test status is {3}. His test timer is currently set to {4} hours and {5} minutes.",
          reportAriaLabel: "FR Report {0} {1} user."
        },
        noActiveCandidates: "FR There are no active candidates",
        editTimePopup: {
          title: "FR Edit Test Time",
          description1: "FR For use for accomodation pusposes only.",
          description2: "FR Adjust the amount of time for {0} {1}.",
          description3:
            "FR The timers cannot be set for less than the original amount of time allowed for the respective test section.",
          description4: "FR Total Test Time: {0} Hours {1} Minutes",
          hours: "HEURES",
          minutes: "MINUTES",
          incrementHoursButton: "FR Increment hours button. Current value is {0}.",
          decrementHoursButton: "FR Decrement hours button. Current value is {0}.",
          incrementMinutesButton: "FR Increment minutes button. Current value is {0}.",
          decrementMinutesButton: "FR Decrement minutes button. Current value is {0}.",
          timerCurrentValue: "FR Current Timer value: {0} hours and {1} minutes.",
          setTimerButton: "FR Set Timer"
        },
        lockPausePopup: {
          title: "FR Lock Test",
          description1: "FR You are about to lock {0} {1}'s test.",
          checkboxPart1: "FR Lock test for a specified period of time (Pause). ",
          checkboxPart2: "FR This is only to be used for accomodation purposes.",
          description2: "FR When the timer expires, the candidate's test timer will resume.",
          lockButton: "FR Lock Test",
          pauseButton: "FR Pause Test"
        },
        unlockUnpausePopup: {
          title: "FR Unlock Test",
          description1: "FR You are about to unlock {0} {1}'s test.",
          description2: "FR The test timer will resume once you unlock the test.",
          description3: "FR Be sure to complete an incident report regarding this.",
          unlockButton: "FR Unlock Test",
          unpauseButton: "FR Unpause Test"
        },
        lockAllPopup: {
          title: "FR Lock All Candidate Tests",
          description1: "FR You are about to lock this test for all of your active candidates.",
          description2:
            "FR If after the incident that has caused you to lock the tests, it is determined that the test can be continued you will need to unlock the tests for the candidates that wish to continue testing.",
          description3:
            "FR Be sure to complete session report and a candidate report for any candidates not able to continue after the incident.",
          lockTestsButton: "FR Lock Tests"
        },
        unlockAllPopup: {
          title: "FR Unlock All Candidate Tests",
          description1: "FR You are about to unlock this test for all of your active candidates.",
          description2:
            "FR Be sure to lock any test for a candidate not wishing or able to continue and fill out an incident report for those candidates.",
          description3: "FR Remeber to file a Session Report for the incident.",
          unlockTestsButton: "FR Unlock Tests"
        },
        reSyncPopup: {
          title: "FR Re-Sync Confirmation",
          description: "FR Are you sure you want to re-sync with {0} {1}?"
        },
        unAssignPopup: {
          title: "FR Un-Assign Confirmation",
          description: "FR Are you sure you want to un-assign {0} {1}'s test.?"
        }
      },
      reports: {
        description: "FR Generate a results report for a Test Order you have administered.",
        reportTypes: {
          individualScoreSheet: "FR Individual Score Sheet",
          resultsReport: "FR Results Report",
          financialReport: "FR Financial Report"
        }
      }
    },

    //PPC  Administration Page
    ppcAdministration: {
      title: "Bienvenue, {0} {1}.",
      containerLabel: "FR PPC Administration",
      sideNavItems: {
        reports: "FR Reports"
      },
      reports: {
        title: "FR Reports",
        description: "FR Generate reports based on Test Orders."
      }
    },

    //Report Generator Component
    reports: {
      reportTypeLabel: "FR Report Type",
      testOrderNumberLabel: "FR Test Order Number",
      testLabel: "FR Test",
      candidateLabel: "FR Candidate",
      generateButton: "FR Generate Report",
      noDataPopup: {
        title: "FR No Data",
        description: "FR There is no data to show based on the provided parameters."
      }
    },

    //DatePicker Component
    datePicker: {
      dayField: "Jour",
      dayFieldSelected: "FR Day field selected.",
      monthField: "Mois",
      monthFieldSelected: "FR Month field selected.",
      yearField: "Année",
      yearFieldSelected: "FR Year field selected.",
      currentValue: "Fr Current value is:",
      none: "FR None",
      datePickedError: "FR Must be a valid date",
      futureDatePickedError: "FR Must be a future date"
    },

    //Profile Page
    profile: {
      title: "Bienvenue, {0} {1}.",
      sideNavItems: {
        personalInfo: "Information Personnelle",
        password: "Mot de passe",
        preferences: "Préférences",
        permissions: "Permissions",
        profileMerge: "FR Profile Merge"
      },
      personalInfo: {
        title: "FR Your Contact Information",
        nameSection: {
          title: "Nom :",
          firstName: "Prénom",
          lastName: "Nom de famille"
        },
        emailAddressesSection: {
          title: "Adresses courriel :",
          primary: "Primaire",
          secondary: "Secondaire",
          secondaryTooltip: "FR Secondary email tooltip (TEMP TEXT)",
          emailError: "Doit être une adresse courriel valide"
        },
        dateOfBirth: {
          title: "Date de naissance :",
          titleTooltip: "FR Date of birth tooltip (TEMP TEXT)",
          yearField: "Année",
          yearFieldSelected: "FR Year field selected.",
          monthField: "Mois",
          monthFieldSelected: "FR Month field selected.",
          dayField: "Jour",
          dayFieldSelected: "FR Day field selected.",
          currentValue: "FR Current value is:",
          none: "FR None"
        },
        priOrMilitaryNbr: {
          title: "CIDP ou numéro matricule :",
          titleTooltip: "FR PRI or Military number tooltip (TEMP TEXT)",
          priOrMilitaryNbrError: "Doit être un CIDP ou numéro matricule valide"
        },
        optionalField: "(Optionnelle)",
        saveConfirmationPopup: {
          title: "FR Save Confirmation",
          description: "FR Your personal information has been saved successfully."
        }
      },
      password: {
        newPassword: {
          title: "Mot de passe",
          updatedDate: "FR Your password was last updated on: {0}",
          updatedDateNever: "FR never",
          currentPassword: "Mot de passe actuel:",
          newPassword: "Nouveau mot de passe:",
          confirmPassword: "FR Confirm Password:",
          popup: {
            title: "FR Save Confirmation",
            description: "Votre mot de passe a été mis à jour avec succès."
          }
        },
        passwordRecovery: {
          title: "FR Password Recovery",
          secretQuestion: "FR Secret Question:",
          secretAnswer: "FR Secret Answer:",
          secretQuestionUpdatedConfirmation: "FR Your secret question has been updated successfully"
        }
      },
      preferences: {
        title: "Préférences",
        description: "FR Modify your preferences.",
        notifications: {
          title: "Notifications",
          checkBoxOne:
            "FR Always send an email to my primary email address when I receive a notification",
          checkBoxTwo:
            "FR Always send an email to my secondary email address when I receive a notification"
        },
        display: {
          title: "FR Display",
          checkBoxOne: "FR Allow others to see my profile picture",
          checkBoxTwo: "FR Hide tooltip icons"
        }
      },
      permissions: {
        title: "FR System Permissions",
        description:
          "FR You have all the accesses needed to check-in and write any test and/or evaluation needed for staffing process(es) to which you have applied.",
        systemPermissionInformation: {
          title: "FR System Permission Information",
          titleTooltip: "FR System Permission Information tooltip (TEMP TEXT)",
          addPermission: "FR Permission",
          pending: "FR Pending",
          superUser: {
            title: "Super Utilisateur",
            permission: "L'utilisateur détient tous les rôles et accès dans le système"
          }
        },
        addPermissionPopup: {
          title: "FR System Permission Request",
          description: "FR Request permissions to perform administration activities.",
          gocEmail: "FR GOC email:",
          gocEmailTooltip: "FR GOC Email tooltip (TEMP TEXT)",
          gocEmailError: "Doit être une adresse courriel valide",
          pri: "CIDP ou numéro matricule :",
          priError: "Doit être un CIDP ou numéro matricule valide",
          supervisor: "FR Supervisor:",
          supervisorError: "Doit être un nom valide",
          supervisorEmail: "FR Supervisor email:",
          supervisorEmailError: "Doit être une adresse courriel valide",
          rationale: "FR Rationale:",
          rationaleError: "FR Provide a rationale for this request",
          permissions: "FR Permissions:",
          permission: "FR Permission:",
          permissionRequested: "FR Permission Requested:",
          permissionsError: "FR Must select at least one permission to request"
        }
      },
      testPermissions: {
        title: "FR Test Access Permissions",
        description:
          "FR As a Test Administrator, you have access to administer the following tests:",
        // temporary until we have other test types
        eMIB: "FR Electronic Managerial In-Box (e-MIB)",
        table: {
          title: "FR Test Accesses",
          column1: "FR Test Version",
          column1OrderItem: "FR Order by test versions",
          column2: "FR Order Number",
          column2OrderItem: "FR Order by test order numbers",
          column3: "FR Staffing Process Number",
          column3OrderItem: "FR Order by staffing process number",
          column4: "FR Expiration Date",
          column4OrderItem: "FR Order by expiration dates"
        }
      },
      saveButton: "Sauvegarder"
    },

    //System Administrator Page
    systemAdministrator: {
      title: "Bienvenue, {0} {1}.",
      containerLabel: "FR System Administration",
      sideNavItems: {
        dashboard: "FR Dashboard",
        activeTests: "FR Active Tests",
        testAccessCodes: "FR Test Access Codes",
        permissions: "FR Permissions",
        testAccesses: "FR TA Test Accesses",
        incidentReports: "FR Incident Reports",
        scoring: "FR Scoring",
        reScoring: "FR Re-Scoring",
        reports: "FR Reports"
      },
      permissions: {
        description: "FR Manage permission requests and assign permissions to users.",
        tabs: {
          permissionRequests: {
            title: "FR Permission Requests",
            noPermission: "FR There are no permission requests at the moment.",
            permissionRequestedAccessibility: "FR Permission requested:",
            lastNameAccessibility: "FR Last name:",
            firstNameAccessibility: "FR First name:",
            viewButtonAccessibility: "FR View request details",
            popup: {
              title: "FR Permission Request",
              description: "FR Approve or deny the permission for request made by {0}",
              deniedReason: "FR Denied Reason:",
              denyButton: "FR Deny permission",
              grantButton: "FR Grant permission",
              grantPermissionErrors: {
                notEmptyDeniedReasonError:
                  "FR This field must be empty if you want to approve this permission request",
                missingDeniedReasonError: "FR Must put a reason to deny this permission",
                usernameDoesNotExistError: "FR This username no longer exists"
              }
            }
          },
          activePermissions: {
            title: "FR Active Permissions",
            searchBarTitle: "FR Search:",
            multipleResultsFound: "{0} FR results found",
            singularResultFound: "{0} FR result found",
            clearSearch: "FR Clear Search",
            noResultsFound: "FR There are no results found based on your search.",
            displayOptionLabel: "FR Display:",
            displayOptionAccessibility:
              "FR You can choose how many results per page you want by using this dropdown.",
            displayOptionCurrentValueAccessibility: "FR Current selected value is:",
            table: {
              permission: "FR Permission",
              user: "FR User",
              action: "FR Action",
              actionButtonLabel: "FR Open",
              actionButtonAriaLabel: "FR View permission details of user: {0} {1}, permission: {2}."
            },
            viewEditDetailsPopup: {
              title: "FR System Permission Information",
              description: "FR Review and modify {0}'s permission.",
              deleteButton: "FR Delete permission",
              saveButton: "FR Save changes",
              reasonForModification: "FR Reason for modifications:",
              reasonForModificationError: "FR Provide a reason for modifications for this request"
            },
            deletePermissionConfirmationPopup: {
              title: "FR Are you sure you want to delete this permission?",
              systemMessageTitle: "FR Warning!",
              systemMessageDescription:
                "FR This action will revoke {0} permission from {1}'s account. Are you sure you want to proceed?"
            },
            updatePermissionDataConfirmationPopup: {
              title: "FR Updates Confirmation",
              description: "FR {0}'s permission data has been updated successfully."
            },
            nextPageButton: "FR Next page",
            previousPageButton: "FR Previous page"
          }
        }
      },
      testAccesses: {
        title: "FR Test Administrator Test Accesses",
        description: "FR Manage test accesses for test administrators.",
        tabs: {
          assignTestAccesses: {
            version: {
              title: "FR {0} Version {1}",
              titleTooltip: "FR Version number of the test."
            },
            testDescription: "FR {0} Version {1}",
            title: "FR Assign Test Accesses",
            testOrderNumberLabel: "FR Test Order Number:",
            testOrderNumberCurrentValueAccessibility: "FR Current selected test order number is:",
            staffingProcessNumber: "FR Staffing Process Number:",
            departmentMinistry: "FR Department/Ministry:",
            isOrg: "FR IS Org (Funds Center):",
            isRef: "FR IS Ref (Funds Commitment):",
            billingContact: "FR Billing Contact:",
            billingContactInfo: "FR Billing Contact Info:",
            users: "Administrateur(s) de test :",
            username: "FR Username:",
            usersCurrentValueAccessibility: "FR Current selected users are:",
            noUserAccessibility: "FR none",
            expiryDate: "FR Expiry Date:",
            testAccesses: "FR Test Accesses:",
            testAccess: "FR Test Access:",
            testAccessesError: "FR At least one test access must be selected",
            testAccessAlreadyExistsError:
              "FR {0} already has this {1} access combined with {2} test order number",
            emptyFieldError:
              "FR This field is empty or exceed the maximum amount of characters allowed",
            unableToAccessOrderingServiceError:
              "FR Unable to access ordering service. Please enter financial data manually.",
            searchButton: "FR Search",
            manuelEntryButton: "FR Manual entry",
            noTestOrderNumberFound:
              "FR This order number cannot be found, an exact match is required",
            // temporary until we have other test types
            eMIB: "FR Electronic Managerial In-Box (e-MIB)",
            refreshButton: "FR Clear All Fields",
            refreshConfirmationPopup: {
              title: "FR Clear all fields?",
              description: "FR Description here.. (to be determined)"
            },
            saveButton: "FR Save",
            saveConfirmationPopup: {
              title: "FR Confirm Test Access Assignment(s)",
              usersSection: "FR You are about to allow the following users(s):",
              testsSection: "FR Access to administer the following tests:",
              orderInfoSection: {
                title: "FR For the following test order information:",
                testOrderNumber: "FR Test Order Number:",
                staffingProcessNumber: "FR Staffing Process Number:",
                departmentMinistry: "FR Department:"
              },
              expiriDateSection: "FR These accesses will expire on:",
              confirmation: "FR Please confirm this information is correct.",
              testAccessesGrantedConfirmationPopup: {
                title: "FR Confirmation",
                description: "FR Test accesses have been granted successfully."
              }
            }
          },
          activeTestAccesses: {
            title: "FR Active Test Accesses",
            searchBarTitle: "FR Search:",
            displayOptionLabel: "FR Display:",
            displayOptionAccessibility:
              "FR You can choose how many results per page you want by using this dropdown.",
            displayOptionCurrentValueAccessibility: "FR Current selected value is:",
            table: {
              testAdministrator: "FR Test Administrator",
              test: "FR Test",
              orderNumber: "FR Order Number",
              expiryDate: "FR Expiry Date",
              action: "FR Action",
              actionButtonLabel: "FR Open",
              actionButtonAriaLabel:
                "FR View test access details of user: {0} {1}, test: {2}, order number: {3}, expiry date: {4}."
            },
            viewTestPermissionPopup: {
              title: "FR Test Access Information",
              description: `FR Review {0}'s test access permission.`,
              deleteButtonAccessibility: "FR Delete",
              saveButtonAccessibility: "FR Save"
            },
            deleteConfirmationPopup: {
              title: "FR Are you sure you want to delete this test access?",
              systemMessageTitle: "FR Warning!",
              systemMessageDescription:
                "FR This action will remove {0} test access from {1}'s account. Are you sure you want to proceed?"
            },
            saveConfirmationPopup: {
              title: "FR Updates Confirmation",
              description: "FR {0}'s test access has been updated successfully."
            }
          }
        }
      },
      reports: {
        title: "FR Reports",
        description: "FR Generate reports based on Test Orders."
      }
    },

    //Scorer Pages
    scorer: {
      situationTitle: "Situation",
      assignedTest: {
        displayOptionLabel: "FR Display:",
        displayOptionAccessibility:
          "FR You can choose how many results per page you want by using this dropdown.",
        displayOptionCurrentValueAccessibility: "FR Current selected value is:",
        table: {
          assignedTestId: "FR Assigned Test ID",
          completionDate: "FR Completion Date",
          testName: "FR Test Name",
          testLanguage: "FR Test Language",
          action: "FR Action",
          actionButtonLabel: "FR Score Test",
          en: "Anglais",
          fr: "Français",
          actionButtonAriaLabel:
            "FR Score test of: assigned test id: {0}, completion date: {1}, test name: {2}, test language: {3}."
        },
        noResultsFound: "FR There are no tests waiting to be scored."
      },
      sidebar: {
        title: "FR Scoring Panel",
        competency: "FR Competency",
        rational: "FR Rating rationale",
        placeholder: "FR Input rationale here..."
      },
      submitButton: "FR Submit Final Scores",
      unsupportedScoringType:
        "FR The scoring type: {0} is not supported. The test definition must be incorrect.",
      selectQuestion: "FR Select a question to continue",
      competency: {
        bar: {
          title: "FR Behavioural Achored Ratings",
          description:
            "FR Below is a complete view of the BAR for this question and competency. Once selected, you can use the arrows to navigate, expand, and collapse information.",
          barButton: "FR View BAR"
        }
      }
    },

    //Status Page
    statusPage: {
      title: "Statut de OÉC",
      logo: "Logo Thunder CAT",
      welcomeMsg:
        "Page de statut interne afin de déterminer rapidement l'état / la santé de l'outil d'évaluation des candidats.",
      versionMsg: "Version de l'Outil d'évaluation des candidats: ",
      gitHubRepoBtn: "Répertoire GitHub",
      serviceStatusTable: {
        title: "Statut des services",
        frontendDesc: "La Face avant de l'application est construite et utilisée avec succès",
        backendDesc: "La Face arrière de l'application réussit les demandes API avec succès",
        databaseDesc: "La Base de données réussit les demandes API avec succès"
      },
      systemStatusTable: {
        title: "Statut du système",
        javaScript: "JavaScript",
        browsers: "IE 10+, Firefox, Chrome",
        screenResolution: "Résolution d'écran minimum de 1024 x 768"
      },
      additionalRequirements:
        "De plus, les exigences suivantes doivent être respectées pour l’utilisation de cette application dans un centre de test.",
      secureSockets: "Chiffrement Secure Socket Layer (SSL) activé",
      fullScreen: "Mode plein écran activé",
      copyPaste: "Fonction copier-coller activée",
      colorOptions: "Options Internet IE > Modification des couleurs permise",
      fontsEnabled: "Options Internet IE > Modification de la police permise",
      accessibilityOptions: "Options Internet IE > Options d’ergonomie permises"
    },

    // Settings Dialog
    settings: {
      systemSettings: "FR Accessibility Display Settings",
      zoom: {
        title: "Zoom avant et zoom arrière (+ / -)",
        instructionsListItem1:
          "Cliquer sur le bouton Visualiser dans la barre de menu supérieure à gauche dans Internet Explorer.",
        instructionsListItem2: "Sélectionner Zoom.",
        instructionsListItem3:
          "Vous pouvez choisir un niveau de zoom prédéfini ou un niveau sur mesure (sélectionner Sur mesure avant de saisir une valeur de zoom).",
        instructionsListItem4:
          "Vous pouvez également appuyer simultanément sur les touches CTRL et + / - de votre clavier pour effectuer un zoom avant ou un zoom arrière."
      },
      fontSize: {
        title: "Taille de texte",
        description: "FR Select the minimum size of the font text should apprea in:"
      },
      fontStyle: {
        title: "Police de caractères",
        description: "FR Select the font style/family you want to use:"
      },
      lineSpacing: {
        title: "FR Line and word spacing",
        description: "FR Turn on accessibility line and word spacing."
      },
      color: {
        title: "Couleur du texte et de l’arrière plan",
        instructionsListItem1: "Cliquer sur le bouton Outils et sélectionner Options Internet.",
        instructionsListItem2: "Dans l’onglet Général, sous Apparence, sélectionner Accessibilité.",
        instructionsListItem3: "Cocher la case Ignorer les couleurs spécifiées sur les pages Web.",
        instructionsListItem4: "Cliquer sur OK.",
        instructionsListItem5: "Dans l’onglet Général, sous Apparence, sélectionner Couleurs.",
        instructionsListItem6: "Décocher la case Utiliser les couleurs Windows.",
        instructionsListItem7:
          "Pour chaque couleur que vous désirez modifier, cliquer sur la case de couleur, choisir une nouvelle couleur et cliquer sur OK.",
        instructionsListItem8: "Cliquer sur OK, puis encore une fois sur OK."
      }
    },

    //Multiple Choice  test
    mcTest: {
      questionList: {
        questionId: "Question {0}",
        nextButton: "Suivant",
        previousButton: "Précédent",
        reviewButton: "À revoir",
        markedReviewButton: "FR Unmark for review",
        seenQuestion: "FR This question has been seen",
        unseenQuestion: "FR This question has not been seen",
        answeredQuestion: "FR This question has been answered",
        unansweredQuestion: "FR This question has not been answered",
        reviewQuestion: "FR This question has been marked for review"
      },
      finishPage: {
        homeButton: "FR Return Home"
      }
    },
    //eMIB Test
    emibTest: {
      //Home Page
      homePage: {
        testTitle: "Échantillon de la BRG-e",
        welcomeMsg: "Bienvenue dans le test pratique de BRG-e"
      },

      //HowTo Page
      howToPage: {
        tipsOnTest: {
          title: "Conseils pour répondre à la BRG-e",
          part1: {
            description:
              "La BRG-e vous présente des situations qui vous donneront l’occasion de démontrer les compétences clés en matière de leadership. Voici quelques conseils qui vous aideront à fournir aux évaluateurs l’information dont ils ont besoin pour évaluer votre rendement par rapport à ces compétences clés en leadership :",
            bullet1:
              "Répondez à toutes les questions posées dans les courriels que vous avez reçus. Notez également que des situations pour lesquelles on ne pose pas de questions précises peuvent être abordées dans plusieurs courriels. Vous profiterez ainsi de toutes les occasions qui vous sont offertes de démontrer ces compétences.",
            bullet2:
              "N’hésitez pas à présenter vos recommandations si nécessaire, et ce, même s’il s’agit seulement de réflexions préliminaires. S’il le faut, vous pouvez ensuite noter les autres renseignements dont vous auriez besoin pour en arriver à une décision finale.",
            bullet3:
              "Si dans certaines situations vous croyez avoir besoin de parler en personne avec quelqu’un avant de prendre une décision, indiquez les renseignements dont vous avez besoin et comment cela pourrait influencer votre décision.",
            bullet4:
              "Utilisez uniquement l’information fournie dans les courriels et l’information contextuelle. Ne tirez aucune conclusion fondée sur la culture de votre propre organisation. Évitez de faire des suppositions qui ne sont pas raisonnablement corroborées par l’information contextuelle ou les courriels.",
            bullet5:
              "Les situations présentées dans la BRG-e s’inscrivent dans un domaine particulier afin de vous donner suffisamment de contexte pour y répondre.  Pour être efficaces, vos réponses doivent démontrer la compétence ciblée, et non la connaissance du domaine en question."
          },
          part2: {
            title: "Autres renseignements importants",
            bullet1:
              "Le contenu de vos courriels, l'information fournie dans votre liste de tâches et les justifications des mesures prises seront évalués. Le contenu du bloc-notes ne sera pas évalué.",
            bullet2:
              "Votre rédaction ne sera pas évaluée. Aucun point ne sera enlevé pour les fautes d’orthographe, de grammaire, de ponctuation ou pour les phrases incomplètes. Votre rédaction devra toutefois être suffisamment claire pour que les évaluateurs comprennent la situation que vous traitez et vos principaux arguments.",
            bullet3: "Vous pouvez répondre aux courriels dans l’ordre que vous désirez.",
            bullet4: "Vous êtes responsable de la gestion de votre temps."
          }
        },
        testInstructions: {
          title: "Instructions du test",
          hiddenTabNameComplementary: "Sous « Instructions »",
          onlyOneTabAvailableForNowMsg:
            "Veuillez noter qu’il n’y a qu’un seul onglet principal disponible pour l’instant. Dès que vous commencerez le test, les autres principaux onglets deviendront disponibles.",
          para1:
            "Lorsque vous commencez le test, lisez d’abord l’information contextuelle qui décrit votre poste et l’organisation fictive où vous travaillez. Nous vous recommandons de prendre environ 10 minutes pour la lire. Passez ensuite à la boîte de réception pour lire les courriels que vous avez reçus et prenez des mesures pour y répondre, comme si vous étiez gestionnaire dans cette organisation fictive.",
          para2:
            "Lorsque vous serez dans la boîte de réception, vous aurez accès aux éléments suivants :",
          bullet1: "les instructions du test;",
          bullet2:
            "l’information contextuelle décrivant votre rôle en tant que gestionnaire et l’organisation fictive où vous travaillez;",
          bullet3: "un bloc-notes pouvant servir de papier brouillon.",
          step1Section: {
            title: "Étape 1 — Répondre aux courriels",
            description:
              "Vous pouvez répondre aux courriels que vous avez reçus de deux façons : en écrivant un courriel ou en ajoutant des tâches à votre liste de tâches. Ces deux façons de répondre sont décrites ci-dessous, suivies d’exemples.",
            part1: {
              title: "Exemple d’un courriel que vous avez reçu :",
              para1:
                "Vous trouverez ci-dessous deux façons de répondre au courriel. Vous pouvez choisir l’une ou l’autre des deux options présentées, ou combiner les deux. Les réponses fournies ne sont présentées que pour illustrer comment utiliser chacune des deux façons de répondre. Elles ne démontrent pas nécessairement les compétences clés en leadership qui sont évaluées dans cette situation."
            },
            part2: {
              title: "Ajouter une réponse par courriel",
              para1:
                "Vous pouvez écrire un courriel pour répondre à celui que vous avez reçu dans votre boîte de réception. Votre réponse écrite devrait refléter la façon dont vous répondriez en tant que gestionnaire.",
              para2:
                "Vous pouvez utiliser les fonctions suivantes : répondre, répondre à tous ou transférer. Si vous choisissez de transférer un courriel, vous aurez accès à un répertoire qui contient tous vos contacts. Vous pouvez écrire autant de courriels que vous le souhaitez pour répondre à un courriel ou pour gérer des situations que vous remarquez dans plusieurs des courriels reçus."
            },
            part3: {
              title: "Exemple d’une réponse par courriel :"
            },
            part4: {
              title: "Ajouter une tâche à la liste de tâches",
              para1:
                "En plus de répondre par courriel, ou au lieu d’en écrire un, vous pouvez ajouter des tâches à la liste de tâches. Une tâche représente une mesure que vous comptez prendre pour gérer une situation présentée dans les courriels. Voici des exemples de tâches : planifier une rencontre ou communiquer avec un collègue afin d’obtenir de l’information. Assurez-vous de fournir suffisamment d’information dans votre description de la tâche pour que nous sachions à quelle situation vous répondez. Vous devez également préciser quelles mesures vous comptez prendre et qui devra participer à cette tâche. Vous pouvez en tout temps retourner à un courriel pour ajouter, supprimer ou modifier des tâches."
            },
            part5: {
              title: "Exemple d’ajout d’une tâche à la liste de tâches :"
            },
            part6: {
              title: "Comment choisir une façon de répondre",
              para1:
                "Il n’y a pas de bonne ou de mauvaise façon de répondre. Lorsque vous répondez à un courriel, vous pouvez :",
              bullet1: "envoyer un ou des courriels;",
              bullet2: "ajouter une ou des tâches à votre liste de tâches;",
              bullet3: "faire les deux.",
              para2:
                "C’est le contenu de vos réponses qui sera évalué, et non la façon de répondre (c’est-à-dire si vous avez répondu par courriel ou en ajoutant une tâche à votre liste de tâches). Par conséquent, vos réponses doivent être suffisamment détaillées et claires pour que les évaluateurs puissent évaluer comment vous gérez la situation. Par exemple, si vous prévoyez organiser une réunion, vous devez préciser de quoi il y sera question.",
              para3Part1: "Si vous décidez d’écrire un courriel ",
              para3Part2: "et",
              para3Part3:
                " d’ajouter une tâche à votre liste de tâches pour répondre à un courriel que vous avez reçu, vous n’avez pas à répéter la même information aux deux endroits. Par exemple, si vous mentionnez dans un courriel que vous organiserez une réunion avec un collègue, vous n’avez pas à ajouter cette réunion à votre liste de tâches."
            }
          },
          step2Section: {
            title: "Étape 2 — Justifier les mesures prises (facultatif)",
            description:
              "Après avoir écrit un courriel ou ajouté une tâche, vous pouvez expliquer votre raisonnement dans la section réservée à cet effet, si vous le souhaitez. Cette section se situe au bas des réponses par courriel et des tâches. La justification des mesures prises est facultative. Notez que vous pouvez choisir d’expliquer certaines mesures que vous avez prises tandis que d’autres ne nécessitent pas d’explications supplémentaires. De même, vous pouvez décider de justifier les mesures prises lorsque vous répondez à certains courriels, et non à d'autres. Cela s'applique également aux tâches de la liste de tâches.",
            part1: {
              title:
                "Exemple d’une réponse par courriel accompagnée de justifications des mesures prises:"
            },
            part2: {
              title:
                "Exemple d’une liste de tâches accompagnée de justifications des mesures prises:"
            }
          },
          exampleEmail: {
            to: "T.C. Bernier (gestionnaire, Équipe de l’assurance de la qualité)",
            from: "Geneviève Bédard (directrice, Unité de recherche et innovation)",
            subject: "Préparer Mary à son affectation",
            date: "vendredi 4 novembre",
            body:
              "Bonjour T.C.,\n\nJ’ai été ravie d’apprendre qu’une de tes analystes de l’assurance de la qualité, Mary Woodside, avait accepté une affectation de six mois avec mon équipe, à compter du 2 janvier. Je crois comprendre qu’elle a de l’expérience en enseignement et en utilisation d’outils pédagogiques modernes dans le cadre de son travail antérieur de professeure au niveau collégial. Mon équipe a besoin d’aide pour mettre au point des techniques d’enseignement novatrices qui favorisent la productivité et le bien-être général des employés. Je pense donc que l’expérience de Mary sera un bon atout pour l’équipe.\n\nY a-t-il des domaines dans lesquels tu aimerais que Mary acquière plus d’expérience, laquelle serait utile lors de son retour dans ton équipe? Je tiens à maximiser les avantages de l’affectation pour nos deux équipes.\n\nAu plaisir de recevoir tes commentaires.\n\nGeneviève"
          },
          exampleEmailResponse: {
            emailBody:
              "Bonjour Geneviève,\n\nJe suis d’accord que nous devrions planifier l’affectation de Mary afin que nos deux équipes en tirent parti. Je suggère de former Mary à la synthèse de données provenant de sources multiples. Cela l’aiderait à élargir ses compétences et serait utile à mon équipe à son retour. De même, les membres de ton équipe pourraient profiter de son expérience en enseignement. Je la consulterai directement, car j’aimerais connaître son avis à ce sujet. Je te recontacterai au cours de la semaine, quand j’aurai plus d’information à te fournir.\n\nCela dit, quelles sont tes attentes? Y a-t-il présentement certains défis ou des aspects particuliers de la dynamique de ton équipe dont il faudrait tenir compte? Avant de rencontrer Mary pour discuter de son affectation, j’aimerais tenir compte de tous les facteurs, tels que les besoins actuels, les défis et la dynamique de ton équipe.\n\nMerci.\n\nT.C.",
            reasonsForAction:
              "Je compte rencontrer Mary pour discuter de ses attentes concernant l’affectation et pour établir des objectifs clairs. Je veux qu’elle se sente motivée et sache ce qu’on attend d’elle, afin de l’aider à se préparer en conséquence. J’examinerai également ses objectifs pour l’année afin de m’assurer que ce que je propose cadre bien avec son plan de perfectionnement professionnel."
          },
          exampleTaskResponse: {
            task:
              "- Répondre au courriel de Geneviève :\n     > lui proposer de former Mary à la synthèse de l’information provenant de sources multiples afin qu’elle puisse élargir ses compétences;\n     >	lui demander quelles sont ses attentes et quels sont les défis de son équipe afin que je puisse tenir compte de tous les facteurs pour déterminer comment son équipe pourrait profiter de l’expérience de Mary dans le domaine de la formation;\n     > l’informer que je travaille à recueillir plus d’information auprès de Mary, et que je lui ferai part de mes suggestions d’ici la fin de la semaine.\n- Organiser une réunion avec Mary pour discuter des objectifs de son affectation et veiller à ce qu’elle se sente motivée et à ce qu’elle sache ce qui est attendu d’elle.\n- Consulter les objectifs passés et actuels de Mary pour vérifier que ce que je propose est conforme à son plan de perfectionnement professionnel.",
            reasonsForAction:
              "Former Mary à la synthèse de l’information provenant de sources multiples serait avantageux pour mon équipe, qui a besoin de consolider l’information recueillie auprès de nombreuses sources. Demander à Geneviève quels sont ses attentes et ses défis m’aidera à mieux préparer Mary et à m’assurer que l’affectation sera avantageuse pour nos deux équipes."
          }
        },
        evaluation: {
          title: "Évaluation",
          bullet1:
            "Les mesures que vous prenez et les explications que vous donnez seront prises en compte dans l’évaluation de votre rendement pour chacune des compétences clés en leadership (décrites ci-dessous). On évaluera à quel point ces mesures et explications démontrent les compétences clés en leadership.",
          bullet2:
            "L’efficacité des mesures prises sera évaluée. Le niveau d’efficacité est déterminé par l’effet positif ou négatif que ces mesures auraient sur la résolution des situations présentées, et par l’étendue de cet effet.",
          bullet3:
            "Vos réponses seront également évaluées en fonction de leur contribution à l’atteinte des objectifs organisationnels présentés dans l’information contextuelle.",
          keyLeadershipCompetenciesSection: {
            title: "Compétences clés en leadership",
            para1Title: "Créer une vision et une stratégie : ",
            para1:
              "Les gestionnaires contribuent à définir l’avenir et à tracer la voie à suivre. Pour ce faire, ils tiennent compte du contexte. Ils mettent à contribution leurs connaissances. Ils obtiennent et intègrent de l’information provenant de diverses sources pour la mise en œuvre d’activités concrètes. Ils considèrent divers points de vue et consultent d’autres personnes, au besoin. Les gestionnaires assurent l’équilibre entre les priorités organisationnelles et contribuent à améliorer les résultats.",
            para2Title: "Mobiliser les personnes : ",
            para2:
              "Les gestionnaires inspirent et motivent les personnes qu’ils dirigent. Ils gèrent le rendement de leurs employés et leur offrent de la rétroaction constructive et respectueuse pour encourager et rendre possible l’excellence en matière de rendement. Ils dirigent en donnant l’exemple et se fixent des objectifs personnels qui sont plus exigeants que ceux qu’ils établissent pour les autres.",
            para3Title: "Préserver l’intégrité et le respect : ",
            para3:
              "Les gestionnaires donnent l’exemple sur le plan des pratiques éthiques, du professionnalisme et de l’intégrité personnelle, en agissant dans l’intérêt du Canada, des Canadiens et des Canadiennes. Ils créent des environnements de travail inclusifs, empreints de respect et de confiance, où les conseils judicieux sont valorisés. Ils encouragent les autres à faire part de leurs points de vue, tout en encourageant la collégialité.",
            para4Title: "Collaborer avec les partenaires et les intervenants : ",
            para4:
              "Les gestionnaires cherchent à obtenir, de façon délibérée et ingénieuse, un grand éventail de perspectives. Lorsqu’ils établissent des partenariats, ils gèrent les attentes et visent à atteindre un consensus. Ils font preuve d’ouverture et de souplesse afin d’améliorer les résultats et apportent une perspective globale de l’organisation à leurs interactions. Les gestionnaires reconnaissent le rôle des partenaires dans l’obtention des résultats.",
            para5Title: "Promouvoir l’innovation et orienter le changement : ",
            para5:
              "Les gestionnaires créent un environnement propice aux idées audacieuses, à l’expérimentation et à la prise de risques en toute connaissance de cause. Lors de la mise en œuvre d’un changement, ils maintiennent l’élan, surmontent la résistance et anticipent les conséquences. Ils perçoivent les revers comme une bonne occasion de comprendre et d’apprendre.",
            para6Title: "Obtenir des résultats : ",
            para6:
              "Les gestionnaires s’assurent de répondre aux objectifs de l’équipe en gérant les ressources. Ils prévoient, planifient et surveillent les progrès, et font des ajustements au besoin. Ils démontrent leur connaissance du contexte lors de la prise de décisions. Les gestionnaires assument la responsabilité personnelle à l’égard de leurs actions et des résultats de leurs décisions."
          }
        }
      },

      //Background Page
      background: {
        hiddenTabNameComplementary: "Sous « Information contextuelle »",
        orgCharts: {
          link: "Description de l'image",
          ariaLabel: "Description de l'image de l'Organigramme Équipe",
          treeViewInstructions:
            "Ci-dessous, vous trouverez la vue arborescente de l’organigramme. Une fois sélectionné, vous pouvez utiliser les touches fléchées pour la navigation, l’expansion et l’effondrement de l’information."
        }
      },

      //Inbox Page
      inboxPage: {
        tabName: "Sous « Boîte de réception »",
        emailId: " courriel ",
        subject: "Objet",
        to: "À",
        from: "De",
        date: "Date",
        addReply: "Ajouter une réponse par courriel",
        addTask: "Ajouter une réponse par liste de tâches",
        yourActions: `Vous avez répondu avec {0} courriel(s) et {1} liste(s) de tâches`,
        editActionDialog: {
          addEmail: "Ajouter une réponse par courriel",
          editEmail: "Modifier la réponse par courriel",
          addTask: "Ajouter à la liste de tâches",
          editTask: "Modifier la tâche",
          save: "Sauvegarder la réponse"
        },
        characterLimitReached: "(Limite atteinte)",
        emailCommons: {
          to: "À :",
          toFieldSelected: "Champ À choisi",
          cc: "Cc :",
          ccFieldSelected: "Champ CC choisi",
          currentSelectedPeople: "Les personnes choisies actuellement sont : {0}",
          currentSelectedPeopleAreNone: "Aucunes",
          reply: "répondre",
          replyAll: "répondre à tous",
          forward: "transmettre",
          editButton: "Modifier la réponse",
          deleteButton: "Supprimer la réponse",
          originalEmail: "Courriel d’origine",
          toAndCcFieldsPlaceholder: "Sélectionnez à partir du carnet d’adresses",
          yourResponse: "Votre réponse"
        },
        addEmailResponse: {
          selectResponseType:
            "Veuillez choisir la manière dont vous souhaitez répondre au courriel d’origine :",
          response: "Votre réponse par courriel : limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif) : limite de {0} caractères",
          emailResponseTooltip: "Rédiger une réponse au courriel que vous avez reçu.",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires.",
          invalidToFieldError: "Ce champ ne peut être vide"
        },
        emailResponse: {
          title: "Réponse par courriel no. ",
          description: "Pour cette réponse, vous avez choisi de :",
          response: "Votre réponse par courriel :",
          reasonsForAction: "Votre justification des mesures prises (facultatif) :"
        },
        addEmailTask: {
          header: "Courriel no {0}: {1}",
          task: "Votre réponse par liste de tâches : limite de {0} caractères",
          reasonsForAction:
            "Justification des mesures prises (facultatif) : limite de {0} caractères"
        },
        taskContent: {
          title: "Réponse par liste de tâches no. ",
          task: "Votre réponse par liste de tâches :",
          taskTooltipPart1:
            "Une action que vous comptez prendre pour résoudre une situation dans les courriels.",
          taskTooltipPart2:
            "Exemple : planifier une réunion, demander de l'information à un collègue.",
          reasonsForAction: "Votre justification des mesures prises (facultatif) :",
          reasonsForActionTooltip:
            "Dans cette section, vous pouvez expliquer pourquoi vous avez pris une certaine mesure en réponse à une situation, si vous souhaitez fournir des renseignements supplémentaires."
        },
        deleteResponseConfirmation: {
          title: "Êtes-vous certain de vouloir annuler cette réponse?",
          systemMessageTitle: "Avertissement!",
          systemMessageDescription:
            "Votre réponse ne sera pas sauvegardée si vous continuez. Si vous souhaitez enregistrer votre réponse, vous pouvez y retourner. Toutes vos réponses peuvent être modifiées ou supprimées avant de soumettre le test.",
          description:
            "Si vous ne voulez pas sauvegarder la réponse, cliquez sur le bouton « Supprimer la réponse »."
        }
      },

      //Lock Screen
      lockScreen: {
        tabTitle: "FR Test Locked",
        description: {
          part1: "FR Your test has been locked by the test administrator.",
          part2: "FR Your test timer is on hold.",
          part3: "FR Your most recent responses have been saved.",
          part4: "FR Please see your test administrator for further instructions."
        }
      },

      //Pause Screen
      pauseScreen: {
        tabTitle: "FR Test Paused",
        description: {
          part1:
            "FR Your test has been paused by the test administrator. It will resume once the timer reaches 00:00:00.",
          part2: "FR Your test timer is on hold.",
          part3: "FR Your most recent responses have been saved."
        }
      },

      //Confirmation Page
      confirmationPage: {
        submissionConfirmedTitle: "Félicitations! Votre test a été soumis.",
        feedbackSurvey:
          "Nous aimerions recevoir vos commentaires sur l’évaluation. Veuillez remplir ce {0} facultatif avant de vous déconnecter et de quitter.",
        optionalSurvey: "sondage de 15 minutes",
        surveyLink: "https://surveys-sondages.psc-cfp.gc.ca/s/se.ashx?s=25113745259E9113&c=fr-CA",
        logout:
          "Pour des raisons de sécurité, assurez-vous de fermer votre session dans le coin supérieur droit de cette page. Vous pouvez discrètement recueillir vos effets personnels et quitter la séance de test. Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}.",
        thankYou: "Nous vous remercions d’avoir terminé votre évaluation. Bonne chance!"
      },

      //Quit Confirmation Page
      quitConfirmationPage: {
        title: "Vous avez quitté le test",
        instructionsTestNotScored1: "Votre test ",
        instructionsTestNotScored2: "ne sera pas corrigé.",
        instructionsRaiseHand:
          "Veuillez lever la main. L’administrateur de tests viendra vous donner d’autres directives.",
        instructionsEmail:
          "Si vous avez des questions ou des préoccupations au sujet de votre test, veuillez communiquer avec {0}."
      },

      //Timeout Page
      timeoutConfirmation: {
        timeoutConfirmedTitle: "Votre temps de test est écoulé...",
        timeoutSaved:
          "Vos réponses ont été sauvegardées et soumises aux fins de notation. Veuillez prendre note que l’information dans le bloc-notes n’est pas sauvegardée.",
        timeoutIssue:
          "S’il y a eu un problème, veuillez en informer votre administrateur de test. Cliquez sur « Continuer » pour quitter cette séance de test et pour recevoir d’autres instructions."
      },

      //Test tabs
      tabs: {
        instructionsTabTitle: "Instructions",
        backgroundTabTitle: "Information contextuelle",
        inboxTabTitle: "Boîte de réception",
        disabled: "Vous ne pouvez accéder à la boîte de courriel avant d'avoir commencé le test.",
        nextButton: "Suivant",
        previousButton: "Précédent"
      },

      //Test Footer
      testFooter: {
        timer: {
          timer: "Minuterie",
          timeLeft: "Temps restant dans la séance de test :",
          timerHidden: "Minuterie cachée."
        },
        submitTestPopupBox: {
          title: "Confirmer l’envoi du test?",
          warning: {
            title: "Avertissement : your notebook will not be saved.",
            message:
              "Le contenu du bloc-notes ne sera pas soumis pour la notation. Assurez-vous d’avoir examiné toutes vos réponses avant de soumettre le test puisque vous ne serez pas en mesure d’y retourner pour apporter des changements."
          },
          description:
            "Si vous êtes prêt(e) à soumettre votre test pour la notation, cliquez sur le bouton « Soumettre le test ». La séance de test sera fermée et vous recevrez d’autres instructions."
        },
        quitTestPopupBox: {
          title: "Souhaitez-vous mettre fin à cette séance de test?",
          description:
            "Vous ne pourrez pas récupérer vos réponses et n’aurez plus accès à la séance de test. Ce faisant, vous affirmez et reconnaissez :",
          checkboxOne: "Je me retire volontairement de ce test;",
          checkboxTwo: "Mon test ne sera pas noté;",
          checkboxThree:
            "Je suis conscient(e) que la période d'attente pour ce test peut s’appliquer, si je veux écrire ce test de nouveau dans le futur."
        }
      }
    },

    //Screen Reader
    ariaLabel: {
      mainMenu: "Menu Principal",
      tabMenu: "Menu des onglets de la BRG-e",
      instructionsMenu: "Menu des instructions",
      languageToggleBtn: "bouton-de-langue-a-bascule",
      authenticationMenu: "Menu d'authentification",
      emailHeader: "en-tête du courriel",
      responseDetails: "détails de la réponse",
      reasonsForActionDetails: "motifs de l'action",
      taskDetails: "détails sur la ou les tâches",
      emailOptions: "options de messagerie",
      taskOptions: "options de tâche",
      taskTooltip: "infobulle de tâche",
      emailResponseTooltip: "Infobulle pour les réponses par courriel",
      reasonsForActionTooltip: "infobulle des motifs de l'action",
      passwordConfirmationRequirements: "Il doit correspondre à votre mot de passe",
      dobDayField: "Champ Journée sélectionné",
      dobMonthField: "Champ Mois sélectionné",
      dobYearField: "Champ Année sélectionné",
      emailsList: "Liste des courriels",
      questionList: "List des questions",
      topNavigationSection: "Barre de navigation du haut",
      sideNavigationSection: "Barre de navigation du côté",
      notepadSection: "Section bloc-notes",
      quitTest: "Quitter de test",
      addPermission: "FR Add permission button",
      selectedPermission: "Permission sélectionnée :",
      description: "Description :",
      pendingStatus: "FR Pending status :",
      add: "FR Add",
      scorerPanel: "FR Scorer Panel"
    },

    //Commons
    commons: {
      psc: "Commission de la fonction publique",
      nextButton: "Suivant",
      backButton: "Retour",
      enterEmibSample: "Passer à l’échantillon du test de la BRG-e",
      enterEmibReal: "Passer aux instructions du test",
      resumeEmibReal: "Rentrez la BRG-e",
      startTest: "Commencer le test",
      resumeTest: "Reprendre le test",
      confirmStartTest: {
        aboutToStart: "Vous êtes sur le point de commencer le test.",
        newtimerWarning: "Vous aurez {0} pour terminer le section du test.",
        timerWarning:
          "Après avoir cliqué sur commencer, vous serez dirigé vers l’onglet « Information contextuelle ». Vous aurez {0} pour terminer le test.",
        instructionsAccess:
          "Vous aurez accès aux instructions et à votre bloc-notes durant le test. Bonne chance!",
        timeUnlimited: "temps illimité",
        numberMinutes: "{0} minutes",
        confirmProceed: "FR Are you sure you wish to proceed?"
      },
      submitTestButton: "Soumettre le test",
      quitTest: "Quitter le test",
      returnToTest: "Retourner au test",
      returnToResponse: "Retourner à la réponse",
      passStatus: "Réussi",
      failStatus: "Échoue",
      error: "Erreur",
      enabled: "Activé",
      disabled: "Désactivé",
      backToTop: "Haut de la page",
      notepad: {
        title: "FR Hide/Show Notepad",
        placeholder: "Mettez vos notes ici..."
      },
      cancel: "Annuler",
      cancelChanges: "Annuler les modifications",
      cancelResponse: "Annuler la réponse",
      deleteButton: "FR Delete",
      saveButton: "FR Save",
      applyButton: "FR Apply",
      close: "Fermer",
      login: "Connexion",
      ok: "Ok",
      deleteConfirmation: "FR Delete Confirmation",
      continue: "Continuer",
      sendRequest: "Envoyer la requête",
      na: "S/O",
      valid: "valide",
      confirm: "FR Confirm",
      none: "FR None",
      english: "Anglais",
      french: "Français",
      bilingual: "Bilingue",
      status: {
        checkedIn: "FR Checked-in",
        ready: "FR Ready",
        active: "FR Testing",
        locked: "FR Locked",
        paused: "FR Paused"
      }
    }
  }
});

export default LOCALIZE;
