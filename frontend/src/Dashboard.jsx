import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "./modules/LoginRedux";
import { bindActionCreators } from "redux";
import AssignedTestTable from "./components/eMIB/AssignedTestTable";
import ContentContainer from "./components/commons/ContentContainer";
import { Helmet } from "react-helmet";

class Dashboard extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  componentDidUpdate = prevProps => {
    if (prevProps.isUserLoading !== this.props.isUserLoading) {
      // focusing on welcome message after content load
      document.getElementById("user-welcome-message-div").focus();
    }
  };

  render() {
    // This renders two versions of the dashboard
    // If the user data has loaded, then it shows the candidate dash
    // otherwise it shows the header common to every dashboard
    // and a loading circle
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.home}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div id="user-welcome-message-div" tabIndex={0} aria-labelledby="user-welcome-message">
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.dashboard.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
              <div>
                <p>{LOCALIZE.dashboard.description}</p>
                <AssignedTestTable
                  username={this.props.username}
                  isUserLoading={this.props.isUserLoading}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Dashboard as UnconnectedDashboard };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    isUserLoading: state.user.isUserLoading
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
