import accommodations, {
  setFontFamily,
  setAccommodations
} from "../../modules/AccommodationsRedux";

describe("Accommodations", () => {
  const initialState = { fontFamily: "Nunito Sans" };

  it("sets font family", () => {
    const currentAccommAction = setFontFamily("Arial Black");
    const currentTestState = accommodations(initialState, currentAccommAction);
    expect(currentTestState).toEqual({
      fontFamily: "Arial Black"
    });
  });

  it("sets all accommodations", () => {
    const currentAccommAction = setAccommodations({ font_family: "Arial Black", font_size: 16 });
    const currentTestState = accommodations(initialState, currentAccommAction);
    expect(currentTestState).toEqual({ fontFamily: "Arial Black", fontSize: 16 });
  });
});
