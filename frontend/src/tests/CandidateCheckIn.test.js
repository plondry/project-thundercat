import React from "react";
import { mount, shallow } from "enzyme";
import { UnconnectedCandidateCheckIn } from "../CandidateCheckIn";
import LOCALIZE from "../text_resources";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import CustomButton, { THEME } from "../components/commons/CustomButton";
const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};

function getComponent(props) {
  return mount(
    shallow(
      <Provider store={mockStore(initialState)}>
        <UnconnectedCandidateCheckIn {...props} />
      </Provider>
    ).get(0)
  );
}
describe("renders right wording depending on the checkedIn state", () => {
  it("checkedIn is set to true)", () => {
    const wrapper = getComponent({ isCheckedIn: true });
    wrapper.setState({ testAccessCode: "test" });
    const rawText = LOCALIZE.candidateCheckIn.checkedInText;
    expect(wrapper.containsMatchingElement(rawText)).toEqual(true);
  });

  it("checkedIn room code is displayed)", () => {
    const wrapper = getComponent({ isCheckedIn: true });
    const testAccessCode = "test";
    wrapper.setState({ testAccessCode: testAccessCode });
    expect(wrapper.containsMatchingElement(testAccessCode)).toEqual(true);
  });
});

describe("renders right objects depending on the checkedIn state", () => {
  it("when checkedIn is set to false", () => {
    const wrapper = shallow(<UnconnectedCandidateCheckIn isCheckedIn={false} isLoaded={true} />);
    wrapper.setState({ testAccessCode: "test" });
    const button = <CustomButton label={LOCALIZE.candidateCheckIn.button}></CustomButton>;

    expect(wrapper.containsMatchingElement(button)).toEqual(true);
  });

  it("when checkedIn is set to false and is not loaded", () => {
    const wrapper = shallow(<UnconnectedCandidateCheckIn isCheckedIn={false} isLoaded={false} />);
    wrapper.setState({ testAccessCode: "test" });
    const button = <CustomButton label={LOCALIZE.candidateCheckIn.loading}></CustomButton>;

    expect(wrapper.containsMatchingElement(button)).toEqual(true);
  });
});
