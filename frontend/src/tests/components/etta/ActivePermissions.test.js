import React from "react";
import { shallow } from "enzyme";
import { unconnectedActivePermissions as ActivePermissions } from "../../../components/etta/permissions/ActivePermissions";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <ActivePermissions
      activePermissions={[]}
      selectedActivePermission={{}}
      rowsDefinition={{}}
      populateActivePermissions={() => {}}
      populateFoundActivePermissions={() => {}}
      numberOfPages={1}
      resultsFound={0}
    />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders search bar label", () => {
    const label = (
      <label id="permissions-search-bar-title">
        {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.searchBarTitle}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
  });

  it("renders display option label", () => {
    const label = (
      <label id="display-options-label">
        {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.displayOptionLabel}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
  });
});
