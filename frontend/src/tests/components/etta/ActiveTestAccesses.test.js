import React from "react";
import { shallow } from "enzyme";
import { unconnectedActiveTestAccesses as ActiveTestAccesses } from "../../../components/etta/test_accesses/ActiveTestAccesses";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<ActiveTestAccesses />, {
    disableLifecycleMethods: true
  });

  it("renders search bar label", () => {
    const label = (
      <label id="search-bar-title">
        {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.searchBarTitle}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
  });

  it("renders display option label", () => {
    const label = (
      <label id="display-options-label">
        {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.displayOptionLabel}
      </label>
    );
    expect(wrapper.containsMatchingElement(label)).toEqual(true);
  });
});
