import React from "react";
import { mount, shallow } from "enzyme";
import { unconnectedAnswerForm as AnswerForm } from "../../../components/testBuilder/AnswerForm";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  testBuilder: { test_definition: [{ test_language: "--" }] },
  accommodations: { fontFamily: "Nunito Sans" }
};

const modifyTestDefinitionField = jest.fn();

describe("It renders address book contact property", () => {
  it("scoring value", () => {
    const wrapper = shallow(<AnswerForm {...testProps} />);
    expect(wrapper.find("#scoring_value-input").exists()).toEqual(true);
  });
  it("content en", () => {
    const wrapper = mount(
      shallow(
        <Provider store={mockStore(initialState)}>
          <AnswerForm {...testProps} />
        </Provider>
      ).get(0)
    );
    expect(wrapper.find("#question-section-en-content-field-input").exists()).toEqual(true);
  });
  it("content fr", () => {
    const wrapper = mount(
      shallow(
        <Provider store={mockStore(initialState)}>
          <AnswerForm {...testProps} />
        </Provider>
      ).get(0)
    );
    expect(wrapper.find("#question-section-fr-content-field-input").exists()).toEqual(true);
  });
  it("save and delete buttons", () => {
    const wrapper = shallow(<AnswerForm {...testProps} />);
    expect(wrapper.find("#unit-test-delete-item-button").exists()).toEqual(true);
    expect(wrapper.find("#unit-test-save-item-button").exists()).toEqual(true);
  });

  it("delete popup", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <AnswerForm {...testProps} testLanguage={"en"} />
      </Provider>
    );
    wrapper.find("#unit-test-delete-item-button").simulate("click");
    expect(wrapper.find("#unit-test-popup").exists()).toEqual(true);
  });
});

describe("It calls redux", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("save", () => {
    const wrapper = shallow(
      <AnswerForm {...testProps} modifyTestDefinitionField={modifyTestDefinitionField} />
    );
    wrapper.find("#unit-test-save-item-button").simulate("click");
    // expect to call 3 times because english and french contact details
    // and the main contact
    expect(modifyTestDefinitionField).toHaveBeenCalledTimes(3);
  });
  it("2 times for french", () => {
    const wrapper = shallow(
      <AnswerForm
        {...testProps}
        testLanguage={"fr"}
        modifyTestDefinitionField={modifyTestDefinitionField}
      />
    );
    wrapper.find("#unit-test-save-item-button").simulate("click");
    expect(modifyTestDefinitionField).toHaveBeenCalledTimes(2);
  });
  it("2 times for english", () => {
    const wrapper = shallow(
      <AnswerForm
        {...testProps}
        testLanguage={"en"}
        modifyTestDefinitionField={modifyTestDefinitionField}
      />
    );
    wrapper.find("#unit-test-save-item-button").simulate("click");
    expect(modifyTestDefinitionField).toHaveBeenCalledTimes(2);
  });
});

describe("It updates correctly", () => {
  it("en content", () => {
    const wrapper = mount(
      shallow(
        <Provider store={mockStore(initialState)}>
          <AnswerForm {...testProps} />
        </Provider>
      ).get(0)
    );
    wrapper.find("#question-section-en-content-field-input").instance().value = "Clayton Perroni";
    wrapper.find("#question-section-en-content-field-input").simulate("change");
    expect(wrapper.state().enDefinition.content).toEqual("Clayton Perroni");
  });
  it("fr content", () => {
    const wrapper = mount(
      shallow(
        <Provider store={mockStore(initialState)}>
          <AnswerForm {...testProps} />
        </Provider>
      ).get(0)
    );
    wrapper.find("#question-section-fr-content-field-input").instance().value =
      "Creator of the Test Builder and Test Factory";
    wrapper.find("#question-section-fr-content-field-input").simulate("change");
    expect(wrapper.state().frDefinition.content).toEqual(
      "Creator of the Test Builder and Test Factory"
    );
  });
  it("score", () => {
    const wrapper = mount(
      shallow(
        <Provider store={mockStore(initialState)}>
          <AnswerForm {...testProps} />
        </Provider>
      ).get(0)
    );

    wrapper.find("#scoring_value-input").instance().value = 10;
    wrapper.find("#scoring_value-input").simulate("change");
    expect(wrapper.state().answer.scoring_value).toEqual("10");
  });
});

describe("It shows error", () => {
  it("score", () => {
    const wrapper = mount(
      shallow(
        <Provider store={mockStore(initialState)}>
          <AnswerForm {...testProps} />
        </Provider>
      ).get(0)
    );
    wrapper.find("#scoring_value-input").instance().value = "Clayton Perroni!";
    wrapper.find("#scoring_value-input").simulate("change");
    expect(wrapper.find("#invalid-scoring_value-msg").exists()).toEqual(false);
    expect(wrapper.state().answer.scoring_value).toEqual(undefined);
  });
});

describe("It shows correct number of top tabs depending on test language", () => {
  it("english 1 tab", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <AnswerForm {...testProps} testLanguage={"en"} />
      </Provider>
    );
    expect(wrapper.find("#top-tabs-tab-0").exists()).toEqual(true);
    expect(
      wrapper
        .find("#top-tabs-tab-0")
        .first()
        .hasClass("active")
    ).toEqual(true);
    expect(wrapper.find("#top-tabs-tab-1").exists()).toEqual(false);
  });
  it("french 1 tab", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <AnswerForm {...testProps} testLanguage={"fr"} />
      </Provider>
    );
    expect(wrapper.find("#top-tabs-tab-0").exists()).toEqual(false);
    expect(wrapper.find("#top-tabs-tab-1").exists()).toEqual(true);
    expect(
      wrapper
        .find("#top-tabs-tab-1")
        .first()
        .hasClass("active")
    ).toEqual(true);
  });
  it("bilingual 2 tab", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <AnswerForm {...testProps} testLanguage={"--"} />
      </Provider>
    );
    expect(wrapper.find("#top-tabs-tab-0").exists()).toEqual(true);
    expect(
      wrapper
        .find("#top-tabs-tab-0")
        .first()
        .hasClass("active")
    ).toEqual(true);
    expect(wrapper.find("#top-tabs-tab-1").exists()).toEqual(true);
  });
  it("bilingual 2 tab (default language value)", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <AnswerForm {...testProps} testLanguage={null} />
      </Provider>
    );
    expect(wrapper.find("#top-tabs-tab-0").exists()).toEqual(true);
    expect(
      wrapper
        .find("#top-tabs-tab-0")
        .first()
        .hasClass("active")
    ).toEqual(true);
    expect(wrapper.find("#top-tabs-tab-1").exists()).toEqual(true);
  });
});

const testProps = {
  question: {
    id: 1591,
    question_type: 1,
    pilot: false,
    order: 1,
    test_section_component: 712,
    question_block_type: 612,
    dependencies: []
  },
  answer: { id: 3003, scoring_value: 0, question: 1591 },
  frDefinition: { id: 6005, content: "Temp", answer: 3003, language: "fr" },
  enDefinition: { id: 6006, content: "Temp", answer: 3003, language: "en" },
  currentLanguage: "en",
  answers: [
    { id: 3003, scoring_value: 0, question: 1591 },
    { id: 3004, scoring_value: 0, question: 1591 }
  ],
  answerDetails: [
    { id: 6005, content: "Temp", answer: 3003, language: "fr" },
    { id: 6006, content: "Temp", answer: 3003, language: "en" },
    { id: 6007, content: "Temp", answer: 3004, language: "fr" },
    { id: 6008, content: "Temp", answer: 3004, language: "en" }
  ],
  testLanguage: null,
  modifyTestDefinitionField: jest.fn(),
  expandItem: () => {}
};
