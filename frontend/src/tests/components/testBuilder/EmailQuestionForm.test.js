import React from "react";
import { mount, shallow } from "enzyme";
import { UnconnectedEmailQuestionForm } from "../../../components/testBuilder/EmailQuestionForm";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();

const functions = {
  modifyTestDefinitionField: jest.fn(),
  handleSave: jest.fn(),
  handleDelete: jest.fn()
};
const initialState = {
  localize: {
    language: "en"
  },
  testBuilder: { test_definition: [{ test_language: "--" }] },
  accommodations: { fontFamily: "Nunito Sans" }
};

describe("It renders markdown page section property", () => {
  it("en body", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#address-book-question-en-body-input").exists()).toEqual(true);
    expect(wrapper.find("#address-book-question-en-body-title").exists()).toEqual(true);
  });
  it("fr body", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#address-book-question-fr-body-input").exists()).toEqual(true);
    expect(wrapper.find("#address-book-question-fr-body-title").exists()).toEqual(true);
  });
  it("en date", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#address-book-question-en-date-field-input").exists()).toEqual(true);
    expect(wrapper.find("#address-book-question-en-date-field-title").exists()).toEqual(true);
  });
  it("fr date", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#address-book-question-fr-date-field-input").exists()).toEqual(true);
    expect(wrapper.find("#address-book-question-fr-date-field-title").exists()).toEqual(true);
  });
  it("en subject", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#address-book-question-en-subject-field-input").exists()).toEqual(true);
    expect(wrapper.find("#address-book-question-en-subject-field-title").exists()).toEqual(true);
  });
  it("fr subject", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#address-book-question-fr-subject-field-input").exists()).toEqual(true);
    expect(wrapper.find("#address-book-question-fr-subject-field-title").exists()).toEqual(true);
  });
  it("email id", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    expect(wrapper.find("#email_id-input").exists()).toEqual(true);
    expect(wrapper.find("#email_id-title").exists()).toEqual(true);
  });
});

describe("It calls save redux", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("3 times for bilingual", () => {
    const wrapper = shallow(
      <UnconnectedEmailQuestionForm {...testProps} testLanguage={"--"} {...functions} />
    );
    wrapper.instance().handleSave();
    expect(functions.modifyTestDefinitionField).toHaveBeenCalledTimes(3);
    expect(functions.handleSave).toHaveBeenCalledTimes(1);
  });
  it("2 time for french", () => {
    const wrapper = shallow(
      <UnconnectedEmailQuestionForm {...testProps} testLanguage={"fr"} {...functions} />
    );
    wrapper.instance().handleSave();
    expect(functions.modifyTestDefinitionField).toHaveBeenCalledTimes(2);
    expect(functions.handleSave).toHaveBeenCalledTimes(1);
  });
  it("2 time for english", () => {
    const wrapper = shallow(
      <UnconnectedEmailQuestionForm {...testProps} testLanguage={"en"} {...functions} />
    );
    wrapper.instance().handleSave();
    expect(functions.modifyTestDefinitionField).toHaveBeenCalledTimes(2);
    expect(functions.handleSave).toHaveBeenCalledTimes(1);
  });
});

describe("It calls delete redux", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("popup and delete called", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} testLanguage={"--"} {...functions} />
      </Provider>
    );
    wrapper
      .find("#unit-test-delete-item-button")
      .hostNodes()
      .simulate("click");
    expect(wrapper.find("#unit-test-popup").exists()).toEqual(true);
    wrapper
      .find("#unit-test-right-btn")
      .hostNodes()
      .simulate("click");
    expect(functions.handleDelete).toHaveBeenCalledTimes(1);
  });
});

describe("It updates correctly", () => {
  it("fr subject", () => {
    const id = "address-book-question-fr-subject-field";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "subject_field" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().frEmail.subject_field).toEqual(
      "test string !@#!$%^*&*("
    );
  });
  it("en subject", () => {
    const id = "address-book-question-en-subject-field";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "subject_field" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().enEmail.subject_field).toEqual(
      "test string !@#!$%^*&*("
    );
  });
  it("fr date", () => {
    const id = "address-book-question-fr-date-field";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "date_field" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().frEmail.date_field).toEqual(
      "test string !@#!$%^*&*("
    );
  });
  it("en date", () => {
    const id = "address-book-question-en-date-field";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "date_field" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().enEmail.date_field).toEqual(
      "test string !@#!$%^*&*("
    );
  });
  it("fr body", () => {
    const id = "address-book-question-fr-body";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "body" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().frEmail.body).toEqual(
      "test string !@#!$%^*&*("
    );
  });
  it("en body", () => {
    const id = "address-book-question-en-body";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "body" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().enEmail.body).toEqual(
      "test string !@#!$%^*&*("
    );
  });
  it("email id (bad input no error)", () => {
    const id = "email_id";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let currentId = wrapper.find(UnconnectedEmailQuestionForm).state().email.email_id;
    let actionObj = {
      target: { value: "test string !@#!$%^*&*(", name: "email_id" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().email.email_id).toEqual(currentId);
  });
  it("email id", () => {
    const id = "email_id";
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let currentId = wrapper.find(UnconnectedEmailQuestionForm).state().email.email_id;
    let actionObj = {
      target: { value: "23", name: "email_id" }
    };
    expect(wrapper.find(`#${id}-input`).exists()).toEqual(true);
    wrapper.find(`#${id}-input`).simulate("change", actionObj);

    expect(wrapper.find(`#${id}-msg`).exists()).toEqual(false);
    expect(wrapper.find(UnconnectedEmailQuestionForm).state().email.email_id).toEqual("23");
  });
  it("from field", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    wrapper
      .find(".from_field__control")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".from_field__control")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".from_field__control")
      .first()
      .simulate("keyDown", { key: "Enter", keyCode: 13 });
    expect(
      wrapper
        .find(".from_field__control")
        .first()
        .text()
    ).toEqual(testProps.addressBook[1].name);
  });
  it("to field", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let currentText = wrapper
      .find(".to_field__control")
      .first()
      .text();
    if (currentText === "Select...") {
      currentText = "";
    }
    wrapper
      .find(".to_field__control")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".to_field__control")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".to_field__control")
      .first()
      .simulate("keyDown", { key: "Enter", keyCode: 13 });
    expect(
      wrapper
        .find(".to_field__control")
        .first()
        .text()
    ).toEqual(currentText + testProps.addressBook[1].name);
  });
  it("cc field", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedEmailQuestionForm {...testProps} {...functions} />
      </Provider>
    );
    let currentText = wrapper
      .find(".cc_field__control")
      .first()
      .text();
    if (currentText === "Select...") {
      currentText = "";
    }

    wrapper
      .find(".cc_field__control")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".cc_field__control")
      .first()
      .simulate("keyDown", { key: "ArrowDown", keyCode: 40 });
    wrapper
      .find(".cc_field__control")
      .first()
      .simulate("keyDown", { key: "Enter", keyCode: 13 });
    expect(
      wrapper
        .find(".cc_field__control")
        .first()
        .text()
    ).toEqual(currentText + testProps.addressBook[1].name);
  });
});

const testProps = {
  testLanguage: "--",
  question: {
    id: 34,
    question_type: 2,
    pilot: false,
    order: 0,
    test_section_component: 19,
    question_block_type: null,
    dependencies: []
  },
  competencyTypes: [],
  questionDetails: {
    email: { id: 1, email_id: 1, question: 34, from_field: 14, to_field: [12], cc_field: [] },
    title: "Email ID: 1, From: Serge Duplessis",
    fr: {
      id: 1,
      subject_field: "Boîte de réception",
      date_field: "Le jeudi 3 novembre",
      body:
        "Bonjour T.C.,\r\n\r\nAlors que vous vous familiarisez avec vos nouvelles fonctions, j’aimerais vous faire part de certaines de mes opinions concernant les changements que l’on propose d’apporter à nos demandes de services et à nos pratiques en matière de documentation.\r\n\r\nJe travaille au sein de l’Équipe de l’assurance de la qualité depuis plus de 12 ans. J’estime que, dans l’ensemble, nous réussissons bien à comprendre et à traiter les demandes de service. Le passage à un système automatisé et informatisé implique une certaine période d’adaptation qui pourrait compromettre la qualité de notre service. Par exemple, une conversation en personne ou par téléphone avec un client peut nous aider à mieux comprendre ses problèmes, car cela nous permet d’obtenir des clarifications et des renseignements importants sur chaque cas. En adoptant cette nouvelle technologie, nous risquons d’avoir plus de problèmes de TI et des retards imprévus à long terme.\r\n\r\nJ’ai déjà exprimé mon opinion lors de réunions précédentes, mais je n’ai pas l’impression que mon opinion compte. Tous les autres sont dans l’équipe depuis moins de deux ans et je me sens ignoré parce que je suis le plus âgé de l’équipe. Je vous encourage à tenir compte de mon opinion afin que nous ne commettions pas d’erreur coûteuse.\r\n\r\nSerge",
      email_question: 1,
      language: "fr"
    },
    en: {
      id: 2,
      subject_field: "Bad experience with Serv",
      date_field: "Thursday, November 3",
      body:
        "Hello T.C.,\r\n\r\nAs you are settling into this position, I was hoping to share with you some of my thoughts about the proposed changes to our service requests and documentation practices.\r\n\r\nI have been working on the Quality Assurance team for over 12 years. I feel that, overall, we are quite successful in understanding and processing service requests. Switching to an automated, computerized system would take a very long time to adapt to and could jeopardize the quality of our service. For example, having a face-to-face or telephone conversation with a client can help us better understand the client’s issues in more depth because it allows us to ask probing questions and receive important information related to each case. By buying this new technology, we risk having more IT problems and unexpected delays in the long run.\r\n\r\nI have voiced my opinion in previous meetings, but I do not feel that my opinion matters. Everyone else has been on the team for less than two years and I feel ignored because I’m the oldest member on the team. I urge you to consider my opinion so that we do not make a costly mistake.\r\n\r\nSerge",
      email_question: 1,
      language: "en"
    }
  },
  enSituation: {
    id: 2,
    question: 34,
    language: "en",
    situation: ""
  },
  frSituation: {
    id: 2,
    question: 34,
    language: "fr",
    situation: ""
  },
  currentLanguage: "en",
  addressBook: [
    { id: 1, name: "Jenna Icard", parent: null, test_section: [22, 23] },
    { id: 2, name: "Amari Kinsler", parent: 1, test_section: [22, 23] },
    { id: 3, name: "Geneviève Bédard", parent: 1, test_section: [22, 23] },
    { id: 4, name: "Bartosz Greco", parent: 1, test_section: [22, 23] },
    { id: 5, name: "Nancy Ward", parent: 1, test_section: [22, 23] },
    { id: 6, name: "Marc Sheridan", parent: 2, test_section: [22, 23] },
    { id: 7, name: "Bob McNutt", parent: 2, test_section: [22, 23] },
    { id: 8, name: "Lana Hussad", parent: 2, test_section: [22, 23] },
    { id: 9, name: "Lucy Trang", parent: 5, test_section: [22, 23] },
    { id: 10, name: "Geoffrey Hamma", parent: 5, test_section: [22, 23] },
    { id: 11, name: "Haydar Kalil", parent: 5, test_section: [22, 23] },
    { id: 12, name: "T.C. Bernier", parent: 5, test_section: [22, 23] },
    { id: 13, name: "Danny McBride", parent: 12, test_section: [22, 23] },
    { id: 14, name: "Serge Duplessis", parent: 12, test_section: [22, 23] },
    { id: 15, name: "Marina Richter", parent: 12, test_section: [22, 23] },
    { id: 16, name: "Mary Woodside", parent: 12, test_section: [22, 23] },
    { id: 17, name: "Charlie Wang", parent: 12, test_section: [22, 23] },
    { id: 18, name: "Jack Laurier", parent: 12, test_section: [22, 23] }
  ]
};
