import React from "react";
import { shallow } from "enzyme";
import { unconnectedActiveCandidates as ActiveCandidates } from "../../../components/ta/ActiveCandidates";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <ActiveCandidates
      testAdministratorAssignedCandidates={[]}
      rowsDefinition={{}}
      currentlyLoading={false}
      selectedCandidateData={{}}
      triggerShowEditTimePopup={false}
      triggerShowLockPausePopup={false}
      triggerShowUnlockUnpausePopup={false}
      populateTestAdministratorAssignedCandidates={() => {}}
    />,
    { disableLifecycleMethods: true }
  );

  it("renders title", () => {
    const title = <h2>{LOCALIZE.testAdministration.sideNavItems.activeCandidates}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders description", () => {
    const description = <p>{LOCALIZE.testAdministration.activeCandidates.description}</p>;
    expect(wrapper.containsMatchingElement(description)).toEqual(true);
  });
});
