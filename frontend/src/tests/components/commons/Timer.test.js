import React from "react";
import { mount } from "enzyme";
import { UnconnectedTimer } from "../../../components/commons/Timer";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontFamily: "Nunito Sans"
  }
};
it("defaults to showing the time", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <UnconnectedTimer timeout={() => {}} timeRemaining={() => {}} />
    </Provider>
  );
  expect(wrapper.find("#unit-test-time-label").exists()).toEqual(true);
});

it("hides the time on click", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <UnconnectedTimer timeout={() => {}} timeRemaining={() => {}} />
    </Provider>
  );
  wrapper
    .find("#unit-test-toggle-timer")
    .first()
    .simulate("click");
  expect(wrapper.find("#unit-test-time-label").exists()).toEqual(false);
});
