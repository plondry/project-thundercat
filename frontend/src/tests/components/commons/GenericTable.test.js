import React from "react";
import { mount } from "enzyme";
import GenericTable from "../../../components/commons/GenericTable";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

const mockData = [
  {
    column_1: "Row #1 / Column #1",
    column_2: "Row #1 / Column #2",
    column_3: "Row #1 / Column #3",
    column_4: "Row #1 / Column #4"
  },
  {
    column_1: "Row #2 / Column #1",
    column_2: "Row #2 / Column #2",
    column_3: "Row #2 / Column #3",
    column_4: "Row #2 / Column #4"
  },
  {
    column_1: "Row #3 / Column #1",
    column_2: "Row #3 / Column #2",
    column_3: "Row #3 / Column #3",
    column_4: "Row #3 / Column #4"
  }
];

const rowsDefinition = {
  column_1_style: {},
  column_2_style: {},
  column_3_style: {},
  column_4_style: {},
  data: mockData
};

const columnsDefinition = [
  { label: "Column #1", style: {} },
  { label: "Column #2", style: {} },
  { label: "Column #3", style: {} },
  { label: "Column #4", style: {} }
];

describe("renders component content", () => {
  const wrapper = mount(
    <GenericTable
      columnsDefinition={columnsDefinition}
      rowsDefinition={rowsDefinition}
      emptyTableMessage={"no data"}
      currentlyLoading={false}
    />
  );

  wrapper.setState({ dataProvided: true });

  it("renders all four column labels", () => {
    const column1 = <th id="column-1">Column #1</th>;
    const column2 = <th id="column-2">Column #2</th>;
    const column3 = <th id="column-3">Column #3</th>;
    const column4 = <th id="column-4">Column #4</th>;
    expect(wrapper.containsMatchingElement(column1)).toEqual(true);
    expect(wrapper.containsMatchingElement(column2)).toEqual(true);
    expect(wrapper.containsMatchingElement(column3)).toEqual(true);
    expect(wrapper.containsMatchingElement(column4)).toEqual(true);
  });

  it("renders row #1 data", () => {
    const row1 = (
      <tr id="table-row-0">
        <td id="column-1-label">
          <div>Row #1 / Column #1</div>
        </td>
        <td id="column-2-label">
          <div>Row #1 / Column #2</div>
        </td>
        <td id="column-3-label">
          <div>Row #1 / Column #3</div>
        </td>
        <td id="column-4-label">
          <div>Row #1 / Column #4</div>
        </td>
      </tr>
    );
    expect(wrapper.containsMatchingElement(row1)).toEqual(true);
  });

  it("renders row #2 data", () => {
    const row2 = (
      <tr id="table-row-1">
        <td id="column-1-label">
          <div>Row #2 / Column #1</div>
        </td>
        <td id="column-2-label">
          <div>Row #2 / Column #2</div>
        </td>
        <td id="column-3-label">
          <div>Row #2 / Column #3</div>
        </td>
        <td id="column-4-label">
          <div>Row #2 / Column #4</div>
        </td>
      </tr>
    );
    expect(wrapper.containsMatchingElement(row2)).toEqual(true);
  });

  it("renders row #3 data", () => {
    const row3 = (
      <tr id="table-row-2">
        <td id="column-1-label">
          <div>Row #3 / Column #1</div>
        </td>
        <td id="column-2-label">
          <div>Row #3 / Column #2</div>
        </td>
        <td id="column-3-label">
          <div>Row #3 / Column #3</div>
        </td>
        <td id="column-4-label">
          <div>Row #3 / Column #4</div>
        </td>
      </tr>
    );
    expect(wrapper.containsMatchingElement(row3)).toEqual(true);
  });

  it("renders no data message when there is no data (way #1)", () => {
    // first way to provide no data
    const wrapper = mount(
      <GenericTable
        columnsDefinition={columnsDefinition}
        // no data (way #1)
        rowsDefinition={{}}
        emptyTableMessage={"no data"}
        currentlyLoading={false}
      />
    );
    const noDataMessage = (
      <tr id="no-data">
        <td>
          <label>no data</label>
        </td>
      </tr>
    );
    expect(wrapper.containsMatchingElement(noDataMessage)).toEqual(true);
  });

  it("renders no data message when there is no data (way #2)", () => {
    // second way to provide no data
    const wrapper = mount(
      <GenericTable
        columnsDefinition={columnsDefinition}
        // no data (way #2)
        rowsDefinition={{ data: [] }}
        emptyTableMessage={"no data"}
        currentlyLoading={false}
      />
    );
    const noDataMessage = (
      <tr id="no-data">
        <td>
          <label>no data</label>
        </td>
      </tr>
    );
    expect(wrapper.containsMatchingElement(noDataMessage)).toEqual(true);
  });

  it("renders loading row while loading", () => {
    const wrapper = mount(
      <GenericTable
        columnsDefinition={columnsDefinition}
        rowsDefinition={{ data: [] }}
        emptyTableMessage={"no data"}
        // loading is set to true
        currentlyLoading={true}
      />
    );
    const loadingRow = (
      <tr>
        <td colSpan={columnsDefinition.length}>
          <label className="fa fa-spinner fa-spin">
            <FontAwesomeIcon icon={faSpinner} />
          </label>
        </td>
      </tr>
    );
    expect(wrapper.containsMatchingElement(loadingRow)).toEqual(true);
  });
});
