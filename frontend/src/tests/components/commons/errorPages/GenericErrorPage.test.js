import React from "react";
import { shallow } from "enzyme";
import { unconnectedGenericErrorPage as GenericErrorPage } from "../../../../components/commons/errorPages/GenericErrorPage";

describe("renders component content", () => {
  const wrapper = shallow(
    <GenericErrorPage
      tabTitle={"Tab Title"}
      title={"Error Page Title"}
      titleCustomStyle={{ padding: 12 }}
      description={
        <div>
          <p>This is a test</p>
        </div>
      }
    />,
    {
      disableLifecycleMethods: true
    }
  );
  it("renders error page title with custom styles", () => {
    const pageTitle = <h1 style={{ padding: 12 }}>Error Page Title</h1>;
    expect(wrapper.containsMatchingElement(pageTitle)).toEqual(true);
  });

  it("renders error page description", () => {
    const description = (
      <div>
        <p>This is a test</p>
      </div>
    );
    expect(wrapper.containsMatchingElement(description)).toEqual(true);
  });
});
