import React from "react";
import { shallow } from "enzyme";
import { unconnectedPauseScreen as PauseScreen } from "../../../components/commons/PauseScreen";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(<PauseScreen />, {
    disableLifecycleMethods: true
  });

  it("renders tab title", () => {
    const tabTitle = (
      <label>
        {LOCALIZE.emibTest.pauseScreen.tabTitle}
        <span></span>
      </label>
    );
    expect(wrapper.containsMatchingElement(tabTitle)).toEqual(true);
  });

  it("renders tab content description", () => {
    const part1 = <p>{LOCALIZE.emibTest.pauseScreen.description.part1}</p>;
    const part2 = <p>{LOCALIZE.emibTest.pauseScreen.description.part2}</p>;
    const part3 = <p>{LOCALIZE.emibTest.pauseScreen.description.part3}</p>;
    expect(wrapper.containsMatchingElement(part1)).toEqual(true);
    expect(wrapper.containsMatchingElement(part2)).toEqual(true);
    expect(wrapper.containsMatchingElement(part3)).toEqual(true);
  });
});
