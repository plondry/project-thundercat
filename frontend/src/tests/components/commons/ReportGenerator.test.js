import React from "react";
import { shallow } from "enzyme";
import {
  unconnectedReportGenerator as ReportGenerator,
  REPORT_REQUESTOR
} from "../../../components/commons/reports/ReportGenerator";
import LOCALIZE from "../../../text_resources";
import { reportTypesDefinition, REPORT_TYPES } from "../../../components/commons/reports/Constants";

describe("renders component content", () => {
  const wrapper = shallow(
    <ReportGenerator
      reportRequestor={REPORT_REQUESTOR.etta}
      reportTypeOptions={reportTypesDefinition()}
      populateTestOrderNumberOptions={() => {}}
      populateTestOptions={() => {}}
      populateCandidateOptions={() => {}}
      generateReport={() => {}}
    />
  );

  it("renders report type label and dropdown (initial states)", () => {
    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should only display report (label and dropdown), since all other labels are hidden
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#report-type-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-order-number-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#test-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#candidate-dropdown").exists()).toEqual(false);
  });

  it("renders also test order number label and dropdown (test and candidate should be hidden)", () => {
    // simulating that report type (option 1 - Individual Score Sheet) has been selected
    wrapper.setState({
      testOrderNumberVisible: true,
      selectedReportType: {
        label: REPORT_TYPES.individualScoreSheet.label,
        value: REPORT_TYPES.individualScoreSheet.value
      }
    });

    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should only display report and test order number (label and dropdown), since all other labels are hidden
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#report-type-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#candidate-dropdown").exists()).toEqual(false);
  });

  it("renders also test label and dropdown (candidate should be hidden)", () => {
    // simulating that test order number has been selected
    wrapper.setState({ testVisible: true });

    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should only display report, test order number and test (label and dropdown), since all other labels are hidden
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#report-type-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#candidate-dropdown").exists()).toEqual(false);
  });

  it("renders also candidate label and dropdown only if selected report type is 'Individual Score Sheet'", () => {
    // simulating that test has been selected
    wrapper.setState({ candidateVisible: true });

    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should display all labels and dropdowns
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(true);
    expect(wrapper.find("#report-type-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#candidate-dropdown").exists()).toEqual(true);
  });

  it("does not render candidate label and dropdown if selected report type is 'Results Report'", () => {
    // simulating that report type (option 2 - Results Report) has been selected
    wrapper.setState({
      candidateVisible: false,
      selectedReportType: {
        label: REPORT_TYPES.resultsReport.label,
        value: REPORT_TYPES.resultsReport.value
      }
    });

    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should only display report, test order number and test (label and dropdown), since selected report type is 2 (Results Report)
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#report-type-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#candidate-dropdown").exists()).toEqual(false);
  });

  it("does not render candidate and test labels and dropdowns if selected report type is 'Financial Report'", () => {
    // simulating that report type (option 2 - Results Report) has been selected
    wrapper.setState({
      candidateVisible: false,
      selectedReportType: {
        label: REPORT_TYPES.financialReport.label,
        value: REPORT_TYPES.financialReport.value
      }
    });

    const reportTypeLabel = (
      <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
    );
    const testOrderNumberLabel = (
      <label id="test-order-number-label">{LOCALIZE.reports.testOrderNumberLabel}</label>
    );
    const testLabel = <label id="test-label">{LOCALIZE.reports.testLabel}</label>;
    const CandidateLabel = <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>;
    // should only display report, test order number and test (label and dropdown), since selected report type is 2 (Results Report)
    expect(wrapper.containsMatchingElement(reportTypeLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testOrderNumberLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(testLabel)).toEqual(false);
    expect(wrapper.containsMatchingElement(CandidateLabel)).toEqual(false);
    expect(wrapper.find("#report-type-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-order-number-dropdown").exists()).toEqual(true);
    expect(wrapper.find("#test-dropdown").exists()).toEqual(false);
    expect(wrapper.find("#candidate-dropdown").exists()).toEqual(false);
  });
});
