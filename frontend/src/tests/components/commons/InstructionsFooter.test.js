import React from "react";
import { shallow } from "enzyme";
import InstructionsFooter from "../../../components/testFactory/InstructionsFooter";

describe("displays and calls the right buttons depending on the testIsStarted state", () => {
  const submitMock1 = jest.fn();

  it("test is not started - calls start test when the buttons is clicked", () => {
    const wrapper = shallow(
      <InstructionsFooter timeout={() => {}} startTest={submitMock1} testIsStarted={false} />
    );
    wrapper.find("#unit-test-start-btn").simulate("click");
    expect(wrapper.find("#unit-test-start-btn").exists()).toEqual(true);
    expect(submitMock1).toHaveBeenCalledTimes(1);
  });
});
