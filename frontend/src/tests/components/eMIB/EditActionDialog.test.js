import React from "react";
import { shallow, mount } from "enzyme";
import { UnconnectedEditActionDialog as EditActionDialog } from "../../../components/eMIB/EditActionDialog";
import { ACTION_TYPE, EDIT_MODE, EMAIL_TYPE } from "../../../components/eMIB/constants";
import { addressBook, emailStub } from "./constants";

describe("email action type", () => {
  it("renders Add Email dialog", () => {
    testCore(ACTION_TYPE.email, EDIT_MODE.create);
  });

  it("renders Modify Email dialog", () => {
    testCore(ACTION_TYPE.email, EDIT_MODE.update);
  });
});

describe("task action type", () => {
  it("renders Add Task dialog", () => {
    testCore(ACTION_TYPE.task, EDIT_MODE.create);
  });

  it("renders Modify Task dialog", () => {
    testCore(ACTION_TYPE.task, EDIT_MODE.update);
  });
});

function testCore(actionType, editMode) {
  const updateEmailTestResponse = jest.fn();
  const addEmailResponse = jest.fn();
  const addTaskResponse = jest.fn();
  const updateEmailResponse = jest.fn();
  const updateTaskResponse = jest.fn();
  //Simulation of the save function

  //shallow wrapper of the dialog
  const wrapper = shallow(
    <EditActionDialog
      email={emailStub}
      showDialog={true}
      handleClose={() => {}}
      actionType={actionType}
      editMode={editMode}
      addressBook={addressBook}
      updateEmailTestResponse={updateEmailTestResponse}
      addEmailResponse={addEmailResponse}
      addTaskResponse={addTaskResponse}
      updateEmailResponse={updateEmailResponse}
      updateTaskResponse={updateTaskResponse}
    />
  );

  // adding action.emailTo = [0] (from emailStub) in order to respect 'if (this.state.action.emailTo.length !== 0) {}' condition in the component
  wrapper.setState(prevState => {
    let action = Object.assign({}, prevState.action); // creating copy of state variable jasper
    action.emailTo = [{ id: emailStub.id }]; // update the name property, assign a new value
    return { action }; // return new object jasper object
  });

  // Check if it is an email or a task
  if (actionType === ACTION_TYPE.email) {
    // verify that the empty emailType is overridden to be a reply
    expect(wrapper.state("action").emailType).toEqual(EMAIL_TYPE.reply);
  }

  //Check that the button click triggers the function
  wrapper.find("#unit-test-email-response-button").simulate("click");
  if (actionType === ACTION_TYPE.email && editMode === EDIT_MODE.create) {
    expect(addEmailResponse).toHaveBeenCalledTimes(1);
    expect(updateEmailResponse).toHaveBeenCalledTimes(0);
    expect(updateEmailTestResponse).toHaveBeenCalledTimes(1);
    expect(addTaskResponse).toHaveBeenCalledTimes(0);
    expect(updateTaskResponse).toHaveBeenCalledTimes(0);
  } else if (actionType === ACTION_TYPE.email && editMode === EDIT_MODE.update) {
    expect(addEmailResponse).toHaveBeenCalledTimes(0);
    expect(updateEmailResponse).toHaveBeenCalledTimes(1);
    expect(updateEmailTestResponse).toHaveBeenCalledTimes(1);
    expect(addTaskResponse).toHaveBeenCalledTimes(0);
    expect(updateTaskResponse).toHaveBeenCalledTimes(0);
  } else if (actionType === ACTION_TYPE.task && editMode === EDIT_MODE.create) {
    expect(addEmailResponse).toHaveBeenCalledTimes(0);
    expect(updateEmailResponse).toHaveBeenCalledTimes(0);
    expect(updateEmailTestResponse).toHaveBeenCalledTimes(1);
    expect(addTaskResponse).toHaveBeenCalledTimes(1);
    expect(updateTaskResponse).toHaveBeenCalledTimes(0);
  } else if (actionType === ACTION_TYPE.task && editMode === EDIT_MODE.update) {
    expect(addEmailResponse).toHaveBeenCalledTimes(0);
    expect(updateEmailResponse).toHaveBeenCalledTimes(0);
    expect(updateEmailTestResponse).toHaveBeenCalledTimes(1);
    expect(addTaskResponse).toHaveBeenCalledTimes(0);
    expect(updateTaskResponse).toHaveBeenCalledTimes(1);
  }
}

describe("check status of inputs in email dialog", () => {
  it("renders Add Email dialog without filled inputs", () => {
    testMode(ACTION_TYPE.email, EDIT_MODE.create);
  });

  it("renders Modify Email dialog with filled inputs", () => {
    testMode(ACTION_TYPE.email, EDIT_MODE.update);
  });
});

describe("check status of inputs in task dialog", () => {
  it("renders Add Task dialog without filled inputs", () => {
    testMode(ACTION_TYPE.task, EDIT_MODE.create);
  });

  it("renders Modify Task dialog with filled inputs", () => {
    testMode(ACTION_TYPE.task, EDIT_MODE.update);
  });
});

function testMode(actionType, editMode) {
  // constants used to create the Dialog and to check that the values are present in the inputs later
  const reasonsForAction = "reasons";
  const emailTo = [];
  const emailCc = [];
  const emailBody = "body of email";
  const task = "task";
  const emailType = EMAIL_TYPE.forward;

  const updateEmailTestResponse = jest.fn();
  const addEmailResponse = jest.fn();
  const addTaskResponse = jest.fn();
  const updateEmailResponse = jest.fn();
  const updateTaskResponse = jest.fn();
  const handleClose = jest.fn();

  //mount wrapper of the dialog
  const wrapper = mount(
    <EditActionDialog
      email={emailStub}
      showDialog={true}
      handleClose={() => {}}
      actionType={actionType}
      editMode={editMode}
      action={{
        actionType: actionType,
        reasonsForAction: reasonsForAction,
        emailTo: emailTo,
        emailCc: emailCc,
        emailBody: emailBody,
        task: task,
        emailType: emailType
      }}
      addressBook={addressBook}
      updateEmailTestResponse={updateEmailTestResponse}
      addEmailResponse={addEmailResponse}
      addTaskResponse={addTaskResponse}
      updateEmailResponse={updateEmailResponse}
      updateTaskResponse={updateTaskResponse}
    />
  );

  if (actionType === ACTION_TYPE.email) {
    //set default values when in "create" mode
    let valEmailTo = [];
    let valEmailCc = [];
    let valEmailBody = "";
    let valReasonsForAction = "";
    if (editMode == EDIT_MODE.update) {
      // change defaults when in 'update' mode
      valEmailTo = emailTo;
      valEmailCc = emailCc;
      valEmailBody = emailBody;
      valReasonsForAction = reasonsForAction;
    }
    expect(wrapper.find("#your-response-text-area").props().value).toEqual(valEmailBody);
    expect(wrapper.find("#reasons-for-action-text-area").props().value).toEqual(
      valReasonsForAction
    );
  } else if (actionType === ACTION_TYPE.task) {
    //set default values when in "create" mode
    let valTask = "";
    let valReasonsForAction = "";
    if (editMode == EDIT_MODE.update) {
      // change defaults when in 'update' mode
      valTask = task;
      valReasonsForAction = reasonsForAction;
    }
    expect(wrapper.find("#your-tasks-text-area").props().value).toEqual(valTask);
    expect(wrapper.find("#reasons-for-action-text-area").props().value).toEqual(
      valReasonsForAction
    );
  }
}

it("clicking on the button only adds the email once", () => {
  const updateEmailTestResponse = jest.fn();
  const addEmailResponse = jest.fn();
  const addTaskResponse = jest.fn();
  const updateEmailResponse = jest.fn();
  const updateTaskResponse = jest.fn();
  const handleClose = jest.fn();

  const wrapper = shallow(
    <EditActionDialog
      email={emailStub}
      showDialog={true}
      handleClose={handleClose}
      addTask={() => {}}
      updateEmail={() => {}}
      updateTask={() => {}}
      readEmail={() => {}}
      actionType={ACTION_TYPE.email}
      editMode={EDIT_MODE.create}
      addressBook={addressBook}
      updateEmailTestResponse={updateEmailTestResponse}
      addEmailResponse={addEmailResponse}
      addTaskResponse={addTaskResponse}
      updateEmailResponse={updateEmailResponse}
      updateTaskResponse={updateTaskResponse}
    />
  );

  // setting action.EmailTo = 1, so it goes inside 'if (this.state.action.emailTo.length !== 0) {}' in the component
  wrapper.setState({ action: { emailTo: [{ id: emailStub.id }] } });

  wrapper.find("#unit-test-email-response-button").simulate("click");
  // In the test, calling handleClose does not change the showDialog value
  // However, in the actual UI, it does; so if this has been called,
  // The the button is disabled in the UI
  expect(handleClose).toHaveBeenCalledTimes(1);
  expect(updateEmailTestResponse).toHaveBeenCalledTimes(1);
});

it("check that undefined email type is overriden to be reply", () => {
  checkEmailTypeOverride(undefined);
});

it("check that replyAll email type is not overridden", () => {
  checkEmailTypeOverride(EMAIL_TYPE.replyAll);
});

it("check that forward email type is not overridden", () => {
  checkEmailTypeOverride(EMAIL_TYPE.forward);
});

function checkEmailTypeOverride(emailType) {
  const updateEmailTestResponse = jest.fn();
  const addEmailResponse = jest.fn();
  const addTaskResponse = jest.fn();
  const updateEmailResponse = jest.fn();
  const updateTaskResponse = jest.fn();
  const handleClose = jest.fn();
  const wrapper = shallow(
    <EditActionDialog
      email={emailStub}
      emailResponseId={0}
      questionId={0}
      showDialog={true}
      handleClose={handleClose}
      actionType={ACTION_TYPE.email}
      editMode={EDIT_MODE.create}
      action={{ ...sampleAction, actionType: ACTION_TYPE.email, emailType: emailType }}
      actionId={0}
      addressBook={addressBook}
      updateEmailTestResponse={updateEmailTestResponse}
      addEmailResponse={addEmailResponse}
      addTaskResponse={addTaskResponse}
      updateEmailResponse={updateEmailResponse}
      updateTaskResponse={updateTaskResponse}
    />
  );
  if (emailType === undefined) {
    expect(wrapper.state("action").emailType).toEqual(EMAIL_TYPE.reply);
  } else {
    expect(wrapper.state("action").emailType).toEqual(emailType);
  }
}

const sampleAction = {
  emailType: "reply",
  emailBody: "",
  emailTo: [
    {
      id: 14,
      parent: 12,
      test_section_component: [10, 11],
      name: "Serge Duplessis",
      title: "Quality Assurance Analyst, Quality Assurance Team",
      label: "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)",
      value: 14,
      language: "en",
      contact: 14,
      text: "Serge Duplessis (Quality Assurance Analyst, Quality Assurance Team)"
    }
  ],
  emailToSelectedValues: [],
  emailCc: [],
  emailCcSelectedValues: [],
  reasonsForAction: "",
  questionId: 3,
  answerId: 0
};
