import React from "react";
import { shallow, mount } from "enzyme";
import CollapsingItemContainer, {
  ICON_TYPE
} from "../../../components/eMIB/CollapsingItemContainer";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faTasks, faAngleDown, faAngleUp } from "@fortawesome/free-solid-svg-icons";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";

const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  accommodations: {
    fontSize: "16px"
  }
};

describe("Icons content types", () => {
  const emailIcon = <FontAwesomeIcon icon={faEnvelope} />;
  const taskIcon = <FontAwesomeIcon icon={faTasks} />;

  it("renders email icon", () => {
    const emailWrapper = mount(
      <Provider store={mockStore(initialState)}>
        <CollapsingItemContainer
          iconType={ICON_TYPE.email}
          title={<label>title</label>}
          body={<div>body</div>}
        />
      </Provider>
    );
    expect(emailWrapper.containsMatchingElement(emailIcon)).toEqual(true);
    expect(emailWrapper.containsMatchingElement(taskIcon)).toEqual(false);
  });

  it("renders task icon", () => {
    const taskWrapper = mount(
      <Provider store={mockStore(initialState)}>
        <CollapsingItemContainer
          iconType={ICON_TYPE.task}
          title={<label>title</label>}
          body={<div>body</div>}
        />
      </Provider>
    );
    expect(taskWrapper.containsMatchingElement(emailIcon)).toEqual(false);
    expect(taskWrapper.containsMatchingElement(taskIcon)).toEqual(true);
  });
});

describe("Icons collapsing types", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <CollapsingItemContainer
        iconType={ICON_TYPE.email}
        title={<label>title</label>}
        body={<div>body</div>}
      />
    </Provider>
  );

  const arrowDownIconDisplayed = <FontAwesomeIcon icon={faAngleDown} />;
  const arrowUpIconDisplayed = <FontAwesomeIcon icon={faAngleUp} />;

  it("renders arrow up icon", () => {
    wrapper
      .find(CollapsingItemContainer)
      .instance()
      .setState({ isCollapsed: true });
    wrapper.update();
    expect(wrapper.containsMatchingElement(arrowDownIconDisplayed)).toEqual(true);
    expect(wrapper.containsMatchingElement(arrowUpIconDisplayed)).toEqual(false);
  });

  it("renders arrow down icon", () => {
    wrapper
      .find(CollapsingItemContainer)
      .instance()
      .setState({ isCollapsed: false });
    wrapper.update();
    expect(wrapper.containsMatchingElement(arrowDownIconDisplayed)).toEqual(false);
    expect(wrapper.containsMatchingElement(arrowUpIconDisplayed)).toEqual(true);
  });
});

it("renders title and/or body depending on the 'isCollapsed ' state", () => {
  const wrapper = mount(
    <Provider store={mockStore(initialState)}>
      <CollapsingItemContainer
        iconType={ICON_TYPE.email}
        title={<label>title</label>}
        body={<div>body</div>}
      />
    </Provider>
  );
  const titleComponent = <label>title</label>;
  const bodyComponent = <div>body</div>;

  //collapsing item is closed
  wrapper
    .find(CollapsingItemContainer)
    .instance()
    .setState({ isCollapsed: true });
  wrapper.update();
  expect(wrapper.containsMatchingElement(titleComponent)).toEqual(true);
  expect(wrapper.containsMatchingElement(bodyComponent)).toEqual(false);

  //collapsing item is expanded
  wrapper
    .find(CollapsingItemContainer)
    .instance()
    .setState({ isCollapsed: false });
  wrapper.update();
  expect(wrapper.containsMatchingElement(titleComponent)).toEqual(true);
  expect(wrapper.containsMatchingElement(bodyComponent)).toEqual(true);
});

describe("conditional prop passing", () => {
  it("doesn't pass props for editing to plain <div>", () => {
    const wrapper = shallow(
      <CollapsingItemContainer title={<label>empty div</label>} body={<div>empty div</div>} />
    );

    expect(wrapper.find("div").prop("userIsEditing")).toBe(undefined);
  });
});
