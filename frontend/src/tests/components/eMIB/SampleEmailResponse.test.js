import React from "react";
import { shallow } from "enzyme";
import SampleEmailResponse from "../../../components/eMIB/SampleEmailResponse";
import LOCALIZE from "../../../text_resources";
import { ACTION_TYPE, EMAIL_TYPE } from "../../../components/eMIB/constants";
import { faReply, faReplyAll, faShareSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const sampleEmail = {
  id: 1,
  to: "Tony Stark",
  cc: "The Hulk",
  from: "Doctor Strange",
  subject: "You stole my tech",
  date: "Friday November 4",
  body: "Please give me my tech back.",
  reason: "Because."
};
const sampleEmailResponse = {
  actionType: ACTION_TYPE.email,
  emailType: EMAIL_TYPE.reply,
  emailTo: sampleEmail.to, // Geneviève Bédard in the address book
  emailCc: sampleEmail.cc,
  emailBody: sampleEmail.body,
  reasonsForAction: sampleEmail.reason
};

it("renders all email fields", () => {
  const wrapper = shallow(<SampleEmailResponse action={sampleEmailResponse} />);
  // wrapper.setState({ buttonVisible: true });
  expect(wrapper.find("#unit-test-email-response-type").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-response-to").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-response-cc").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-response-body").exists()).toEqual(true);
  expect(wrapper.find("#unit-test-response-reasons").exists()).toEqual(true);
});

describe("Renders email type icons correctly for", () => {
  const replyResponseIcon = <FontAwesomeIcon icon={faReply} />;
  const replyAllResponseIcon = <FontAwesomeIcon icon={faReplyAll} />;
  const forwardResponseIcon = <FontAwesomeIcon icon={faShareSquare} />;

  let newSampleResponse = { ...sampleEmailResponse };
  it("reply", () => {
    newSampleResponse.emailType = EMAIL_TYPE.reply;
    const wrapper = shallow(<SampleEmailResponse action={newSampleResponse} />);
    expect(wrapper.containsMatchingElement(replyResponseIcon)).toEqual(true);
    expect(wrapper.containsMatchingElement(replyAllResponseIcon)).toEqual(false);
    expect(wrapper.containsMatchingElement(forwardResponseIcon)).toEqual(false);
  });
  it("reply all", () => {
    newSampleResponse.emailType = EMAIL_TYPE.replyAll;
    const wrapper = shallow(<SampleEmailResponse action={newSampleResponse} />);
    expect(wrapper.containsMatchingElement(replyResponseIcon)).toEqual(false);
    expect(wrapper.containsMatchingElement(replyAllResponseIcon)).toEqual(true);
    expect(wrapper.containsMatchingElement(forwardResponseIcon)).toEqual(false);
  });
  it("forward", () => {
    newSampleResponse.emailType = EMAIL_TYPE.forward;
    const wrapper = shallow(<SampleEmailResponse action={newSampleResponse} />);
    expect(wrapper.containsMatchingElement(replyResponseIcon)).toEqual(false);
    expect(wrapper.containsMatchingElement(replyAllResponseIcon)).toEqual(false);
    expect(wrapper.containsMatchingElement(forwardResponseIcon)).toEqual(true);
  });
});

it("renders correct to field", () => {
  const wrapper = shallow(<SampleEmailResponse action={sampleEmailResponse} />);
  let text = LOCALIZE.emibTest.inboxPage.emailCommons.to + " " + sampleEmailResponse.emailTo;
  expect(wrapper.find("#unit-test-response-to").text()).toEqual(text);
});

it("renders correct cc field", () => {
  const wrapper = shallow(<SampleEmailResponse action={sampleEmailResponse} />);
  let text = LOCALIZE.emibTest.inboxPage.emailCommons.cc + " " + sampleEmailResponse.emailCc;
  expect(wrapper.find("#unit-test-response-cc").text()).toEqual(text);
});

it("renders correct body field", () => {
  const wrapper = shallow(<SampleEmailResponse action={sampleEmailResponse} />);
  let text = LOCALIZE.emibTest.inboxPage.emailResponse.response + sampleEmailResponse.emailBody;
  expect(wrapper.find("#unit-test-response-body-header").text()).toEqual(text);
  text = sampleEmailResponse.emailBody;
  expect(wrapper.find("#unit-test-response-body").text()).toEqual(text);
});

it("renders correct reason field", () => {
  const wrapper = shallow(<SampleEmailResponse action={sampleEmailResponse} />);
  let text =
    LOCALIZE.emibTest.inboxPage.emailResponse.reasonsForAction +
    sampleEmailResponse.reasonsForAction;
  expect(wrapper.find("#unit-test-response-reasons-header").text()).toEqual(text);
  text = sampleEmailResponse.reasonsForAction;
  expect(wrapper.find("#unit-test-response-reasons").text()).toEqual(text);
});
