import React from "react";
import { shallow } from "enzyme";
import { UnconnectedAssignedTestTable as AssignedTestTable } from "../../../components/eMIB/AssignedTestTable";
import { LANGUAGES } from "../../../modules/LocalizeRedux";

it("renders no assigned test row plus header", () => {
  const wrapper = shallow(
    <AssignedTestTable
      getTestMetaData={() => {}}
      updateTestMetaDataState={() => {}}
      startTest={() => {}}
      getNotepad={() => {}}
      setNotepadContent={() => {}}
    />,
    {
      disableLifecycleMethods: true
    }
  );
  wrapper.setProps({ currentLanguage: LANGUAGES.english });
  wrapper.setState({
    displayTableContent: true,
    isLoaded: true,
    assigned_tests: [{ test_id: 1, scheduled_date: 1, test: { en_name: "", fr_name: "" } }],
    isLoading: false
  });
  expect(wrapper.find("tr").exists()).toEqual(true);
  expect(wrapper.find("tr")).toHaveLength(2);
});

it("renders two rows", () => {
  const wrapper = shallow(
    <AssignedTestTable
      getTestMetaData={() => {}}
      updateTestMetaDataState={() => {}}
      startTest={() => {}}
      getNotepad={() => {}}
      setNotepadContent={() => {}}
    />,
    {
      disableLifecycleMethods: true
    }
  );
  wrapper.setProps({ currentLanguage: LANGUAGES.english });
  wrapper.setState({
    assigned_tests: [
      { test_id: 1, scheduled_date: 1, test: { en_name: "", fr_name: "" } },
      { test_id: 2, scheduled_date: 1, test: { en_name: "", fr_name: "" } }
    ],
    displayTableContent: true,
    isLoaded: true,
    isLoading: false
  });
  expect(wrapper.find("tr")).toHaveLength(3);
  expect(wrapper.find("#unit-test-button-0")).toHaveLength(1);
  expect(wrapper.find("#unit-test-button-1")).toHaveLength(1);
});
