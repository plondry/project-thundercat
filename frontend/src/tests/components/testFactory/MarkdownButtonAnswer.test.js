import React from "react";
import { shallow, mount } from "enzyme";
import { UnconnectedMarkdownButtonAnswer as MarkdownButtonAnswer } from "../../../components/testFactory/MarkdownButtonAnswer";

const handleClick = jest.fn();
const answer = { content: "sample content" };

describe("It renders a", () => {
  it("button answer", () => {
    const wrapper = shallow(
      <MarkdownButtonAnswer
        key={0}
        answer={answer}
        answerId={0}
        questionId={0}
        handleClick={handleClick}
      />
    );
    expect(wrapper.find("#unit-test-markdown-button-answer-text").exists()).toEqual(true);
  });
});

describe("It calls the function", () => {
  it("handle click", () => {
    const wrapper = shallow(
      <MarkdownButtonAnswer
        key={0}
        answer={answer}
        answerId={0}
        questionId={0}
        handleClick={handleClick}
      />
    );
    expect(wrapper.find("#unit-test-markdown-button-answer-input").exists()).toEqual(true);
    wrapper.find("#unit-test-markdown-button-answer-input").simulate("change");
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});

describe("It renders the correct content", () => {
  it("button answer", () => {
    const wrapper = mount(
      <MarkdownButtonAnswer
        key={0}
        answer={answer}
        answerId={0}
        questionId={0}
        handleClick={handleClick}
      />
    );
    let text = answer.content;
    expect(
      wrapper
        .find("#unit-test-markdown-button-answer-text")
        .last()
        .text()
    ).toEqual(text);
  });
});
