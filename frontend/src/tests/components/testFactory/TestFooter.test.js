import React from "react";
import { shallow } from "enzyme";
import TestFooter from "../../../components/testFactory/TestFooter";

const submitTest = jest.fn();
const timeout = jest.fn();

describe("It renders a", () => {
  it("next section button", () => {
    const wrapper = shallow(
      <TestFooter
        submitTest={submitTest}
        timeout={timeout}
        timeLimit={300}
        startTime={new Date(Date.now).toString()}
        buttonText={"button text"}
      />
    );
    expect(wrapper.find("#unit-test-submit-btn").exists()).toEqual(true);
  });
  it("timer", () => {
    const wrapper = shallow(
      <TestFooter
        submitTest={submitTest}
        timeout={timeout}
        timeLimit={300}
        startTime={new Date(Date.now).toString()}
        buttonText={"button text"}
      />
    );
    expect(wrapper.find("#unit-test-timer").exists()).toEqual(true);
  });
});
