import React from "react";
import { shallow } from "enzyme";
import { UnconnectedTestBuilderTestPage } from "../../../components/testFactory/TestBuilderTestPage";
import { BUILDER_PROPS as testProps } from "./SampleTestPageData";

const functions = {
  setLanguage: jest.fn(),
  tryTestSection: jest.fn().mockReturnValue(Promise.resolve({ ...testProps.response })),
  updateTestSection: jest.fn(),
  setContentLoaded: jest.fn(),
  setStartTime: jest.fn()
};
const tryTestSection = jest.fn().mockReturnValue(Promise.resolve({ ...testProps.untimedResponse }));
const errorTryTestSection = jest
  .fn()
  .mockReturnValue(Promise.resolve({ ok: false, body: ["error"] }));

describe("It calls correct functions", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("on mount, timed", async () => {
    const wrapper = shallow(<UnconnectedTestBuilderTestPage {...testProps} {...functions} />);
    expect(functions.tryTestSection).toHaveBeenCalledTimes(1);
    await functions.tryTestSection();
    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setStartTime).toHaveBeenCalledTimes(1);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
  });
  it("on mount, not timed", async () => {
    const wrapper = shallow(
      <UnconnectedTestBuilderTestPage
        {...testProps}
        {...functions}
        tryTestSection={tryTestSection}
      />
    );
    expect(tryTestSection).toHaveBeenCalledTimes(1);
    await tryTestSection();
    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setStartTime).toHaveBeenCalledTimes(0);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
  });
});
describe("It displays", () => {
  it("error message", async () => {
    const wrapper = shallow(
      <UnconnectedTestBuilderTestPage
        {...testProps}
        {...functions}
        tryTestSection={errorTryTestSection}
      />
    );
    expect(errorTryTestSection).toHaveBeenCalledTimes(1);
    await tryTestSection();
    expect(functions.updateTestSection).toHaveBeenCalledTimes(0);
    expect(functions.setStartTime).toHaveBeenCalledTimes(0);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(0);
    expect(wrapper.find("#test-page-error-message").exists()).toEqual(true);
  });
});
