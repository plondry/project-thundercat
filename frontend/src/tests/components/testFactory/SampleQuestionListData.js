import { TestSectionComponentType } from "../../../components/testFactory/Constants";

export const MC_QL_PROPS = {
  questions: [
    {
      id: 1,
      sections: [
        {
          id: 1,
          question_section_content: { content: "### A1\nTestlet question 1" },
          order: 1,
          question_section_type: 1,
          question: 1
        }
      ],
      question_type: 1,
      details: { diffculty: 1 },
      answers: [
        { id: 2, scoring_value: 5, question: 1, content: "5", answer: 2, language: "en" },
        { id: 1, scoring_value: 0, question: 1, content: "Temp", answer: 1, language: "en" }
      ],
      email: []
    },
    {
      id: 9,
      sections: [
        {
          id: 9,
          question_section_content: { content: "### A1\nTestlet question 2" },
          order: 1,
          question_section_type: 1,
          question: 9
        }
      ],
      question_type: 1,
      details: { diffculty: 1 },
      answers: [
        { id: 18, scoring_value: 0, question: 9, content: "Temp", answer: 18, language: "en" },
        { id: 17, scoring_value: 5, question: 9, content: "5", answer: 17, language: "en" }
      ],
      email: []
    }
  ],
  defaultActiveKey: 1,
  headerFooterPX: 232,
  questionSummaries: { "1": { isRead: true }, "9": { isRead: true }, "11": { isRead: true } },
  testSectionId: 7,
  testSectionComponentId: 2,
  questionType: 1,
  componentType: TestSectionComponentType.QUESTION_LIST,
  assignedTestId: 1
};

export const INBOX_QL_PROPS = {
  questions: [
    {
      email: [{ email_id: 1 }],
      id: 1,
      sections: [
        {
          id: 1,
          question_section_content: { content: "### A1\nTestlet question 1" },
          order: 1,
          question_section_type: 1,
          question: 1
        }
      ],
      question_type: 1,
      details: { diffculty: 1 },
      answers: [
        { id: 2, scoring_value: 5, question: 1, content: "5", answer: 2, language: "en" },
        { id: 1, scoring_value: 0, question: 1, content: "Temp", answer: 1, language: "en" }
      ]
    },
    {
      email: [{ email_id: 2 }],
      id: 9,
      sections: [
        {
          id: 9,
          question_section_content: { content: "### A1\nTestlet question 2" },
          order: 1,
          question_section_type: 1,
          question: 9
        }
      ],
      question_type: 1,
      details: { diffculty: 1 },
      answers: [
        { id: 18, scoring_value: 0, question: 9, content: "Temp", answer: 18, language: "en" },
        { id: 17, scoring_value: 5, question: 9, content: "5", answer: 17, language: "en" }
      ]
    }
  ],
  defaultActiveKey: 1,
  headerFooterPX: 232,
  questionSummaries: { "1": { isRead: true }, "9": { isRead: true }, "11": { isRead: true } },
  testSectionId: 7,
  testSectionComponentId: 2,
  questionType: 2,
  componentType: TestSectionComponentType.INBOX,
  assignedTestId: 1
};
