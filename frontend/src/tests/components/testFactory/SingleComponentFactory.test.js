import React from "react";
import { shallow } from "enzyme";
import { UnconnectedSingleComponentFactory as SingleComponentFactory } from "../../../components/testFactory/SingleComponentFactory";
import {
  SINGLE_PAGE_TYPE_SECTION,
  SIDE_NAV_TYPE_SECTION,
  EMAIL_QUESTION_LIST_TYPE_SECTION,
  QUESTION_LIST_TYPE_SECTION
} from "./SampleSingleComponentFactoryData";
import { TestSectionComponentType } from "../../../components/testFactory/Constants";

const seenUitQuestion = jest.fn();
const readQuestion = jest.fn();

describe("Renders correct single component type", () => {
  it("single page", () => {
    const wrapper = shallow(
      <SingleComponentFactory
        testSection={SINGLE_PAGE_TYPE_SECTION}
        type={TestSectionComponentType.SINGLE_PAGE}
        seenUitQuestion={seenUitQuestion}
        readQuestion={readQuestion}
      />
    );
    expect(wrapper.find("#unit-test-page-section-factory").exists()).toEqual(true);
  });
  it("side navigation", () => {
    const wrapper = shallow(
      <SingleComponentFactory
        testSection={SIDE_NAV_TYPE_SECTION}
        type={TestSectionComponentType.SIDE_NAVIGATION}
        seenUitQuestion={seenUitQuestion}
        readQuestion={readQuestion}
      />
    );
    expect(wrapper.find("#unit-test-side-navigation-page").exists()).toEqual(true);
  });
  it("question list", () => {
    const wrapper = shallow(
      <SingleComponentFactory
        changeCurrentQuestion={() => {}}
        testSection={QUESTION_LIST_TYPE_SECTION}
        type={TestSectionComponentType.QUESTION_LIST}
        seenUitQuestion={seenUitQuestion}
        readQuestion={readQuestion}
      />
    );
    expect(wrapper.find("#unit-test-question-list").exists()).toEqual(true);
  });

  it("email question list", () => {
    const wrapper = shallow(
      <SingleComponentFactory
        testSection={EMAIL_QUESTION_LIST_TYPE_SECTION}
        type={TestSectionComponentType.INBOX}
        seenUitQuestion={seenUitQuestion}
        readQuestion={readQuestion}
      />
    );
    expect(wrapper.find("#unit-test-email-question-list").exists()).toEqual(true);
  });

  it("unsupported type", () => {
    const wrapper = shallow(
      <SingleComponentFactory
        testSection={SINGLE_PAGE_TYPE_SECTION}
        type={1000}
        seenUitQuestion={seenUitQuestion}
        readQuestion={readQuestion}
      />
    );
    expect(wrapper.find("#unit-test-single-component-factory-unsupported").exists()).toEqual(true);
  });
});
