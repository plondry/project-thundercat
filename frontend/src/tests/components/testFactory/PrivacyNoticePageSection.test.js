import React from "react";
import { shallow, mount } from "enzyme";
import PrivacyNoticePageSection from "../../../components/testFactory/PrivacyNoticePageSection";
import LOCALIZE from "../../../text_resources";

const setNextButtonDisabled = jest.fn();

describe("It renders a", () => {
  it("checkbox", () => {
    const wrapper = shallow(<PrivacyNoticePageSection setNextButtonDisabled={() => {}} />);
    expect(wrapper.find("#unit-test-privacy-notice-checkbox").exists()).toEqual(true);
  });
  it("description", () => {
    const wrapper = shallow(<PrivacyNoticePageSection setNextButtonDisabled={() => {}} />);
    expect(wrapper.find("#unit-test-privacy-notice-description").exists()).toEqual(true);
  });
});

describe("It calls the function", () => {
  it("handle click", () => {
    const wrapper = shallow(
      <PrivacyNoticePageSection setNextButtonDisabled={setNextButtonDisabled} />
    );
    wrapper.find("#unit-test-privacy-notice-checkbox").simulate("click");
    expect(setNextButtonDisabled).toHaveBeenCalledTimes(1);
  });
});

describe("It renders the correct content", () => {
  it("button answer", () => {
    const wrapper = mount(
      <PrivacyNoticePageSection setNextButtonDisabled={setNextButtonDisabled} />
    );
    let text = LOCALIZE.formatString(
      LOCALIZE.authentication.createAccount.privacyNotice,
      LOCALIZE.authentication.createAccount.privacyNoticeLink
    );
    expect(wrapper.find("#unit-test-privacy-notice-description").text()).toEqual(text);
  });
});
