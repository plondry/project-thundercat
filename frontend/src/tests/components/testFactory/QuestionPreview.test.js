import React from "react";
import { shallow } from "enzyme";
import QuestionPreview, { styles } from "../../../components/testFactory/QuestionPreview";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faEyeSlash,
  faExclamationCircle,
  faCircle as faCircleSolid
} from "@fortawesome/free-solid-svg-icons";
import { faCircle } from "@fortawesome/free-regular-svg-icons";

describe("It renders a", () => {
  it("icon section", () => {
    const wrapper = shallow(
      <QuestionPreview
        index={0}
        questionId={0}
        isAnswered={false}
        isRead={false}
        isSelected={false}
      />
    );
    expect(wrapper.find("#unit-test-question-preview-icons").exists()).toEqual(true);
  });
  it("question number", () => {
    const wrapper = shallow(
      <QuestionPreview
        index={0}
        questionId={0}
        isAnswered={false}
        isRead={false}
        isSelected={false}
      />
    );

    expect(wrapper.find("#unit-test-question-id-label").exists()).toEqual(true);
  });
});

describe("It renders the correct content", () => {
  it("question preview text", () => {
    let index = 0;
    const wrapper = shallow(
      <QuestionPreview
        index={index}
        questionId={0}
        isAnswered={false}
        isRead={false}
        isSelected={true}
      />
    );
    let text = LOCALIZE.formatString(
      LOCALIZE.mcTest.questionList.questionId.toUpperCase(),
      index + 1
    );
    expect(wrapper.find("#unit-test-question-id-label").text()).toEqual(text);
  });
});

describe("It renders the correct images", () => {
  it("unreadIcon", () => {
    testCore(false, true, false, false);
  });
  it("readIcon", () => {
    testCore(true, true, false, false);
  });
  it("answeredIcon", () => {
    testCore(true, true, true, false);
  });
  it("unansweredIcon", () => {
    testCore(true, true, false, false);
  });
  it("reviewIcon", () => {
    testCore(true, true, false, true);
  });
  it("no reviewIcon", () => {
    testCore(true, true, false, false);
  });
});

const testCore = function(isRead, isSelected, isAnswered, isReview) {
  //READ/UNREAD CHECK
  //defaults, or if unread
  let buttonBackgroundColor = styles.buttonUnreadBackground;
  if (isRead) {
    //if it is read
    buttonBackgroundColor = styles.buttonReadBackground;
  }

  //SELECTED/UNSELECTED CHECK
  //defaults, or unselected
  let buttonTextColor = styles.buttonUnselectedText;
  let imageStyle = styles.buttonUnselectedSymbol;
  if (isSelected) {
    //if it is selected
    buttonBackgroundColor = styles.buttonSelectedBackground;
    buttonTextColor = styles.buttonSelectedText;
    imageStyle = styles.buttonSelectedText;
  }

  let buttonStyle = { ...styles.button, ...buttonTextColor, ...buttonBackgroundColor };
  let emptyIcon = { ...styles.emptyIcon, ...buttonTextColor, ...buttonBackgroundColor };
  const readIcon = (
    <FontAwesomeIcon
      icon={faEye}
      style={imageStyle}
      title={LOCALIZE.mcTest.questionList.seenQuestion}
    />
  );
  const unReadIcon = (
    <FontAwesomeIcon
      icon={faEyeSlash}
      style={imageStyle}
      title={LOCALIZE.mcTest.questionList.unseenQuestion}
    />
  );
  const answeredIcon = (
    <FontAwesomeIcon
      icon={faCircleSolid}
      style={imageStyle}
      title={LOCALIZE.mcTest.questionList.answeredQuestion}
    />
  );
  const unansweredIcon = (
    <FontAwesomeIcon
      icon={faCircle}
      style={imageStyle}
      title={LOCALIZE.mcTest.questionList.unansweredQuestion}
    />
  );
  const reviewIcon = (
    <FontAwesomeIcon
      icon={faExclamationCircle}
      style={imageStyle}
      title={LOCALIZE.mcTest.questionList.reviewQuestion}
    />
  );

  const wrapper = shallow(
    <QuestionPreview
      index={0}
      questionId={0}
      isAnswered={isAnswered}
      isRead={isRead}
      isSelected={isSelected}
      isMarkedForReview={isReview}
    />
  );
  if (isRead) {
    expect(wrapper.contains(readIcon)).toEqual(true);
  }
  if (!isRead) {
    expect(wrapper.contains(unReadIcon)).toEqual(true);
  }
  if (isAnswered) {
    expect(wrapper.contains(answeredIcon)).toEqual(true);
  }
  if (!isAnswered) {
    expect(wrapper.contains(unansweredIcon)).toEqual(true);
  }
  if (isReview) {
    expect(wrapper.contains(reviewIcon)).toEqual(true);
  }
  if (!isReview) {
    expect(wrapper.contains(reviewIcon)).toEqual(false);
  }
};
