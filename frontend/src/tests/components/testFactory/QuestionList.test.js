import React from "react";
import { shallow } from "enzyme";
import { UnconnectedQuestionList } from "../../../components/testFactory/QuestionList";
import { MC_QL_PROPS as testProps, INBOX_QL_PROPS as inboxProps } from "./SampleQuestionListData";

// const modifyTestDefinitionField = jest.fn();
const functions = {
  readQuestion: jest.fn(),
  changeCurrentQuestion: jest.fn(),
  answerQuestion: jest.fn(),
  markForReview: jest.fn(),
  saveUitTestResponse: jest.fn(),
  updateTimeSpent: jest.fn().mockReturnValue(Promise.resolve()),
  getTestResponses: jest
    .fn()
    .mockReturnValue(Promise.resolve({ ok: true, body: [{ answers: {} }] })),
  loadAnswers: jest.fn(),
  setStartTime: jest.fn(),
  seenUitQuestion: jest.fn(),
  loadInboxAnswers: jest.fn()
};

describe("It calls correct functions", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("readQuestion", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    expect(functions.readQuestion).toHaveBeenCalledTimes(1);
  });
  it("getTestResponses", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    // calls twice because of component update
    expect(functions.getTestResponses).toHaveBeenCalledTimes(2);
  });
  it("loadAnswers MC", async () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    await functions.getTestResponses();
    expect(functions.loadAnswers).toHaveBeenCalledTimes(2);
  });
  it("loadAnswers Inbox", async () => {
    const wrapper = shallow(<UnconnectedQuestionList {...inboxProps} {...functions} />);
    await functions.getTestResponses();
    expect(functions.loadInboxAnswers).toHaveBeenCalledTimes(2);
  });
  it("updateTimeSpent", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    wrapper.setState({ previousQuestionId: 10000 });
    wrapper.instance().changeQuestion();
    expect(functions.updateTimeSpent).toHaveBeenCalledTimes(1);
  });
  it("setStartTime", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    expect(functions.setStartTime).toHaveBeenCalledTimes(1);
  });
  it("changeCurrentQuestion", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    expect(functions.changeCurrentQuestion).toHaveBeenCalledTimes(1);
  });

  it("changeQuestion is called", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    wrapper.find("#question-tab-1").simulate("click");
    expect(functions.changeCurrentQuestion).toHaveBeenCalledTimes(1);
    expect(functions.readQuestion).toHaveBeenCalledTimes(1);
    expect(functions.seenUitQuestion).toHaveBeenCalledTimes(1);
  });
});

describe("It renders correct number of", () => {
  it("question previews", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    expect(wrapper.find("#question-tab-0").exists()).toEqual(true);
    expect(wrapper.find("#question-tab-1").exists()).toEqual(true);
  });
  it("question contents", () => {
    const wrapper = shallow(<UnconnectedQuestionList {...testProps} {...functions} />);
    expect(wrapper.find("#question-content-0").exists()).toEqual(true);
    expect(wrapper.find("#question-content-1").exists()).toEqual(true);
  });
});
