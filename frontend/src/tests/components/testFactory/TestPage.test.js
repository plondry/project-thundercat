import React from "react";
import { shallow, mount } from "enzyme";
import { UnconnectedTestPage } from "../../../components/testFactory/TestPage";
import {
  TEST_PROPS as testProps,
  TEST_PROPS_NONE_BUTTON as noButtonProps
} from "./SampleTestPageData";
import LockScreen from "../../../components/commons/LockScreen";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import PauseScreen from "../../../components/commons/PauseScreen";
import PopupBox from "../../../components/commons/PopupBox";
const mockStore = configureStore();

const initialState = {
  localize: {
    language: "en"
  },
  testSection: {
    testSection: testProps.testSection,
    testSectionLoaded: true
  },
  testStatus: { isTestActive: true },
  login: { authenticated: true },
  assignedTest: { assignedTestId: 1 },
  user: { username: "username@email.ca" },
  accommodations: { fontFamily: "Nunito Sans" }
};

// const modifyTestDefinitionField = jest.fn();
const functions = {
  setLanguage: jest.fn(),
  getTestSection: jest.fn().mockReturnValue(Promise.resolve({ ...testProps.testSection })),
  setTestHeight: jest.fn(),
  updateTestSection: jest.fn(),
  setContentLoaded: jest.fn().mockReturnValue(Promise.resolve({})),
  setContentUnLoaded: jest.fn(),
  setStartTime: jest.fn(),
  triggerTimeSpentCalculation: jest.fn(),
  resetTestFactoryState: jest.fn(),
  resetAssignedTestState: jest.fn(),
  resetInboxState: jest.fn(),
  resetNotepadState: jest.fn(),
  resetQuestionListState: jest.fn(),
  resetTopTabsState: jest.fn(),
  scorescoreUitTestSection: jest.fn(),
  scoreUitTest: jest.fn(),
  updateCheatingAttempts: jest.fn(),
  deactivateTest: jest.fn(),
  quitTest: jest.fn(),
  getUpdatedTestStartTime: jest.fn().mockReturnValue(Promise.resolve({})),
  history: { push: jest.fn() }
};

describe("It calls correct functions", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("on mount, not timed", async () => {
    const wrapper = shallow(<UnconnectedTestPage {...testProps} {...functions} />);
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
    await functions.getTestSection();
    expect(functions.setTestHeight).toHaveBeenCalledTimes(1);
    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
  });
  it("on mount, timed", async () => {
    const wrapper = shallow(
      <UnconnectedTestPage
        {...testProps}
        {...functions}
        default_time={100}
        assignedTestId={1}
        testSectionLoaded={true}
      />
    );
    expect(functions.getUpdatedTestStartTime).toHaveBeenCalledTimes(1);
  });
  it("on mount, get updated time", async () => {
    const wrapper = shallow(
      <UnconnectedTestPage {...testProps} {...functions} default_time={100} assignedTestId={1} />
    );
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
    await functions.getTestSection();
    expect(functions.setTestHeight).toHaveBeenCalledTimes(1);
    expect(functions.updateTestSection).toHaveBeenCalledTimes(1);
    expect(functions.setContentLoaded).toHaveBeenCalledTimes(1);
    expect(functions.setStartTime).toHaveBeenCalledTimes(1);
  });
  it("get next test section", () => {
    const wrapper = shallow(<UnconnectedTestPage {...testProps} {...functions} />, {
      disableLifecycleMethods: true
    });
    wrapper.instance().handleNext();
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
  });
  it("finish test", () => {
    const wrapper = shallow(<UnconnectedTestPage {...testProps} {...functions} />, {
      disableLifecycleMethods: true
    });
    wrapper.instance().handleFinishTest();
    expect(functions.triggerTimeSpentCalculation).toHaveBeenCalledTimes(1);
  });
  it("quit test", () => {
    const wrapper = shallow(<UnconnectedTestPage {...testProps} {...functions} />, {
      disableLifecycleMethods: true
    });
    wrapper.instance().handleQuitTest();
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
    expect(functions.quitTest).toHaveBeenCalledTimes(1);
  });
  it("timeout test", () => {
    const wrapper = shallow(<UnconnectedTestPage {...testProps} {...functions} />, {
      disableLifecycleMethods: true
    });
    wrapper.instance().timeout();
    expect(functions.getTestSection).toHaveBeenCalledTimes(1);
  });
  it("cheating popup", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedTestPage {...testProps} {...functions} isTestLocked={true} />
      </Provider>
    );
    wrapper
      .find(UnconnectedTestPage)
      .instance()
      .openCheatingPopup();
    // expect(functions.updateCheatingAttempts).toHaveBeenCalledTimes(1);
    expect(wrapper.find(PopupBox).exists()).toEqual(true);
  });
  it("visibility change", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedTestPage {...testProps} {...functions} isTestLocked={true} />
      </Provider>
    );
    wrapper
      .find(UnconnectedTestPage)
      .instance()
      .visibilityChange();
    // expect(functions.updateCheatingAttempts).toHaveBeenCalledTimes(1);
    expect(wrapper.find(PopupBox).exists()).toEqual(true);
  });
  it("unload", () => {
    const wrapper = shallow(<UnconnectedTestPage {...noButtonProps} {...functions} />, {
      disableLifecycleMethods: true
    });
    wrapper.instance().onUnload();
    expect(functions.deactivateTest).toHaveBeenCalledTimes(1);
    expect(functions.history.push).toHaveBeenCalledTimes(1);
  });
});

describe("It displays correct", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("lock screen", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedTestPage {...testProps} {...functions} isTestLocked={true} />
      </Provider>
    );
    expect(wrapper.find(LockScreen).exists()).toEqual(true);
  });
  it("pause screen", () => {
    const wrapper = mount(
      <Provider store={mockStore(initialState)}>
        <UnconnectedTestPage {...testProps} {...functions} isTestPaused={true} />
      </Provider>
    );
    expect(wrapper.find(PauseScreen).exists()).toEqual(true);
  });
  // not working for some reason, may come back to this
  // it("nav bar", () => {
  //   const wrapper = mount(
  //     <Provider store={mockStore(initialState)}>
  //       <UnconnectedTestPage {...testProps} {...functions} />
  //     </Provider>
  //   );
  //   expect(wrapper.find(TestNavBar).exists()).toEqual(true);
  // });
});
