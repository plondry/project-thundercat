import React from "react";
import { shallow } from "enzyme";
import { unconnectedPersonalInfo as PersonalInfo } from "../../../components/profile/PersonalInfo";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  // mock data
  const firstName = "FirstName";
  const lastName = "LastName";
  const primaryEmail = "firstname.lastname@email.ca";
  const dateOfBirthDay = { value: 10, label: "10" };
  const dateOfBirthMonth = { value: 12, label: "12" };
  const dateOfBirthYear = { value: 1980, label: "1980" };
  const pri = "12345679";

  const wrapper = shallow(
    <PersonalInfo
      firstName={""}
      lastName={""}
      primaryEmail={""}
      dateOfBirth={""}
      priOrMilitaryNbr={""}
      accommodations={{ fontSize: "16px" }}
    />
  );
  wrapper.setState({
    firstNameContent: firstName,
    lastNameContent: lastName,
    primaryEmailContent: primaryEmail,
    dateOfBirthDaySelectedValue: dateOfBirthDay,
    dateOfBirthMonthSelectedValue: dateOfBirthMonth,
    dateOfBirthMonthSelectedValue: dateOfBirthMonth,
    dateOfBirthMonthSelectedValue: dateOfBirthMonth,
    dateOfBirthYearSelectedValue: dateOfBirthYear,
    priOrMilitaryNbrContent: pri
  });

  it("renders page title", () => {
    const title = <h2>{LOCALIZE.profile.personalInfo.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders first name content", () => {
    const firstNameInput = wrapper.find("#first-name-input").props();
    expect(firstNameInput.value).toEqual(firstName);
    const firstNameLabel = <label>{LOCALIZE.profile.personalInfo.nameSection.firstName}</label>;
    expect(wrapper.containsMatchingElement(firstNameLabel)).toEqual(true);
  });

  it("renders last name content", () => {
    const lastNameInput = wrapper.find("#last-name-input").props();
    expect(lastNameInput.value).toEqual(lastName);
    const lastNameLabel = <label>{LOCALIZE.profile.personalInfo.nameSection.lastName}</label>;
    expect(wrapper.containsMatchingElement(lastNameLabel)).toEqual(true);
  });

  it("renders email content", () => {
    const primaryEmailInput = wrapper.find("#primary-email-input").props();
    expect(primaryEmailInput.value).toEqual(primaryEmail);
    const primaryEmailLabel = (
      <label>{LOCALIZE.profile.personalInfo.emailAddressesSection.primary}</label>
    );
    expect(wrapper.containsMatchingElement(primaryEmailLabel)).toEqual(true);
  });

  it("renders pri or military number content", () => {
    const priOrMilitaryNumberInput = wrapper.find("#pri-or-military-number-input").props();
    expect(priOrMilitaryNumberInput.value).toEqual(pri);
    const priOrMilitaryNumberTitleLabel = (
      <label>{LOCALIZE.profile.personalInfo.priOrMilitaryNbr.title}</label>
    );
    const priOrMilitaryNumberOptionalLabel = (
      <label>{LOCALIZE.profile.personalInfo.optionalField}</label>
    );
    expect(wrapper.containsMatchingElement(priOrMilitaryNumberTitleLabel)).toEqual(true);
    expect(wrapper.containsMatchingElement(priOrMilitaryNumberOptionalLabel)).toEqual(true);
  });
});
