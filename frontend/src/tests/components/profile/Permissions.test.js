import React from "react";
import { shallow } from "enzyme";
import { unconnectedPermissions as Permissions } from "../../../components/profile/Permissions";
import LOCALIZE from "../../../text_resources";

describe("renders component content", () => {
  const wrapper = shallow(
    <Permissions username={""} priOrMilitaryNbrProp={""} accommodations={{ fontSize: "16px" }} />,
    {
      disableLifecycleMethods: true
    }
  );

  it("renders permissions section title", () => {
    const title = <h2>{LOCALIZE.profile.permissions.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders test permissions section title if these is at least one existing test permission", () => {
    wrapper.setState({ testPermissions: [{ name: "Permission 1", expiration: null }] });
    const title = <h2>{LOCALIZE.profile.testPermissions.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("does not render test permissions section title if these is no existing test permission", () => {
    wrapper.setState({ testPermissions: [] });
    const title = <h2>{LOCALIZE.profile.testPermissions.title}</h2>;
    expect(wrapper.containsMatchingElement(title)).toEqual(false);
  });
});
