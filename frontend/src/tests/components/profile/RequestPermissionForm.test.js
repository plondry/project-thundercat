import React from "react";
import { shallow } from "enzyme";
import { unconnectedRequestPermissionForm as RequestPermissionForm } from "../../../components/profile/RequestPermissionForm";
import LOCALIZE from "../../../text_resources";

const mockData = [
  { name: "Permission 1", description: "Description of Permission 1", checked: false },
  { name: "Permission 2", description: "Description of Permission 2", checked: false },
  { name: "Permission 3", description: "Description of Permission 3", checked: false },
  { name: "Permission 4", description: "Description of Permission 4", checked: true },
  { name: "Permission 5", description: "Description of Permission 5", checked: true }
];

describe("renders component content", () => {
  const wrapper = shallow(
    <RequestPermissionForm
      permissionsDefinition={mockData}
      triggerValidation={false}
      accommodations={{ fontSize: "16px" }}
    />,
    { disableLifecycleMethods: true }
  );

  it("renders page title", () => {
    const title = <p>{LOCALIZE.profile.permissions.addPermissionPopup.title}</p>;
    expect(wrapper.containsMatchingElement(title)).toEqual(true);
  });

  it("renders some page labels", () => {
    const label1 = <label>{LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}</label>;
    const label2 = <label>{LOCALIZE.profile.permissions.addPermissionPopup.rationale}</label>;
    expect(wrapper.containsMatchingElement(label1)).toEqual(true);
    expect(wrapper.containsMatchingElement(label2)).toEqual(true);
  });

  it("renders first permission details based on permissionsDefinition props", () => {
    const permissionName = <label id="permission-name-0">Permission 1</label>;
    const permissionDescription = (
      <label id="permission-description-0">Description of Permission 1</label>
    );
    const permissionChecked = <input id="permission-checkbox-0" checked={false}></input>;
    expect(wrapper.containsMatchingElement(permissionName)).toEqual(true);
    expect(wrapper.containsMatchingElement(permissionDescription)).toEqual(true);
    expect(wrapper.containsMatchingElement(permissionChecked)).toEqual(true);
  });

  it("renders fourth permission details based on permissionsDefinition props", () => {
    const permissionName = <label id="permission-name-3">Permission 4</label>;
    const permissionDescription = (
      <label id="permission-description-3">Description of Permission 4</label>
    );
    const permissionChecked = <input id="permission-checkbox-3" checked={true}></input>;
    expect(wrapper.containsMatchingElement(permissionName)).toEqual(true);
    expect(wrapper.containsMatchingElement(permissionDescription)).toEqual(true);
    expect(wrapper.containsMatchingElement(permissionChecked)).toEqual(true);
  });
});
