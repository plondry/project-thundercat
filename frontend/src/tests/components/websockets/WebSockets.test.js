import WS from "jest-websocket-mock";
import { CHANNELS_PATH } from "../../../helpers/testSessionChannels";

describe("sending/receiving messages", () => {
  const websocketPath = `ws://localhost:81${CHANNELS_PATH.testAdministratorRoom}1234567890`;

  afterEach(() => {
    WS.clean();
  });

  // source: https://www.npmjs.com/package/jest-websocket-mock
  test("the server keeps track of received messages, and yields them as they come in", async () => {
    const server = new WS(websocketPath);
    const client = new WebSocket(websocketPath);

    await server.connected;
    client.send("hello");
    await expect(server).toReceiveMessage("hello");
    expect(server).toHaveReceivedMessages(["hello"]);
  });

  test("the mock server sends messages to connected clients", async () => {
    const server = new WS(websocketPath);
    const client1 = new WebSocket(websocketPath);
    await server.connected;
    const client2 = new WebSocket(websocketPath);
    await server.connected;

    const messages = { client1: [], client2: [] };
    client1.onmessage = e => {
      messages.client1.push(e.data);
    };
    client2.onmessage = e => {
      messages.client2.push(e.data);
    };

    server.send("hello everyone");
    expect(messages).toEqual({
      client1: ["hello everyone"],
      client2: ["hello everyone"]
    });
  });

  test("the mock server seamlessly handles JSON protocols", async () => {
    const server = new WS(websocketPath, { jsonProtocol: true });
    const client = new WebSocket(websocketPath);

    await server.connected;
    client.send(`{ "type": "GREETING", "payload": "hello" }`);
    await expect(server).toReceiveMessage({ type: "GREETING", payload: "hello" });
    expect(server).toHaveReceivedMessages([{ type: "GREETING", payload: "hello" }]);

    let message = null;
    client.onmessage = e => {
      message = e.data;
    };

    server.send({ type: "CHITCHAT", payload: "Nice weather today" });
    expect(message).toEqual(`{"type":"CHITCHAT","payload":"Nice weather today"}`);
  });

  test("the mock server sends errors to connected clients", async () => {
    const server = new WS(websocketPath);
    const client = new WebSocket(websocketPath);
    await server.connected;

    let disconnected = false;
    let error = null;
    client.onclose = () => {
      disconnected = true;
    };
    client.onerror = e => {
      error = e;
    };

    server.send("hello everyone");
    server.error();
    expect(disconnected).toBe(true);
    expect(error.origin).toBe(websocketPath);
    expect(error.type).toBe("error");
  });

  it("the server can refuse connections", async () => {
    const server = new WS(websocketPath);
    server.on("connection", socket => {
      socket.close({ wasClean: false, code: 1003, reason: "NOPE" });
    });

    const client = new WebSocket(websocketPath);
    client.onclose = function(event) {
      expect(event.code).toBe(1003);
      expect(event.wasClean).toBe(false);
      expect(event.reason).toBe("NOPE");
    };

    expect(client.readyState).toBe(WebSocket.CONNECTING);

    await server.connected;
    expect(client.readyState).toBe(WebSocket.CLOSING);

    await server.closed;
    expect(client.readyState).toBe(WebSocket.CLOSED);
  });
});
