import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "./text_resources";
import { Helmet } from "react-helmet";
import { Button } from "react-bootstrap";
import psc_logo_fr from "./images/psc_logo_fr.png";
import canada_logo from "./images/canada_logo.png";
import { setLanguage, LANGUAGES } from "./modules/LocalizeRedux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { clearTestLocalStorage } from "./helpers/localStorage";
import Splash from "./images/Splash.jpg";

// Strings won't get translated because they will always appear in both
// languages because the user has not selected a language yet.
const strings = {
  cat: "Candidate Assessment Tool",
  oec: "Outil d'évaluation des candidats",
  en: "English",
  fr: "Français"
};

const styles = {
  logo: {
    padding: "10px 20px"
  },
  selectorContainer: {
    width: 600,
    height: 300,
    backgroundColor: "#FFFFFF",
    padding: 20,
    border: "1px solid #00565e",
    borderRadius: 5,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto"
  },
  imageContainer: {
    margin: "10px 0px"
  },
  buttonsContainer: {
    margin: "10px 0px",
    borderTop: "1px solid #CECECE"
  },
  langButton: {
    margin: 10,
    minWidth: 90
  },
  splash: {
    width: "calc(100vw)",
    height: "calc(100vh)",
    backgroundImage: `url(${Splash})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center"
  }
};

class SelectLanguage extends Component {
  static propTypes = {
    // this function initializes language so that we don't
    // always redirect to the choose english or french page.
    initializeLanguage: PropTypes.func.isRequired,
    // Props from Redux
    setLanguage: PropTypes.func.isRequired
  };

  setLanguageToEnglish = () => {
    this.props.initializeLanguage();
    this.props.setLanguage(LANGUAGES.english);
  };

  setLanguageToFrench = () => {
    this.props.initializeLanguage();
    this.props.setLanguage(LANGUAGES.french);
  };

  componentDidMount = () => {
    clearTestLocalStorage();
  };

  render() {
    return (
      <div className="app">
        <Helmet>
          <title>{LOCALIZE.titles.CAT}</title>
        </Helmet>
        <div style={styles.splash}>
          <div style={styles.selectorContainer}>
            <div style={styles.imageContainer}>
              <img
                alt={LOCALIZE.mainTabs.psc}
                src={psc_logo_fr}
                width="370px"
                height="44.67px"
                className="d-inline-block align-top"
                style={styles.logo}
              />
              <img
                alt={LOCALIZE.mainTabs.canada}
                src={canada_logo}
                width="120px"
                height="39.17px"
                className="d-inline-block align-top"
                style={styles.logo}
              />
            </div>
            <h2 lang={LANGUAGES.french}>{strings.oec}</h2>
            <h2>{strings.cat}</h2>
            <div style={styles.buttonsContainer}>
              <Button
                lang={LANGUAGES.french}
                onClick={this.setLanguageToFrench}
                style={styles.langButton}
              >
                {strings.fr}
              </Button>
              <Button onClick={this.setLanguageToEnglish} style={styles.langButton}>
                {strings.en}
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage
    },
    dispatch
  );

export default connect(null, mapDispatchToProps)(SelectLanguage);
