import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import ContentContainer from "./ContentContainer";
import { getTestSection } from "../../modules/TestSectionRedux";
import { unlockTest } from "../../modules/TestStatusRedux";

const styles = {
  tabSection: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  tabLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  tabContentContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    height: "calc(100vh - 140px",
    padding: "12px 0",
    marginBottom: 24
  },
  descriptionContainer: {
    margin: 60,
    fontWeight: "bold"
  }
};

class LockScreen extends Component {
  componentDidMount = () => {
    // focusing on test locked tab
    if (document.getElementById("test-locked-tab")) {
      document.getElementById("test-locked-tab").focus();
    }
  };

  render() {
    return (
      <div>
        <ContentContainer>
          <div id="test-locked-tab" style={styles.tabSection} tabIndex={0}>
            <div>
              <label style={styles.tabLabel}>
                {LOCALIZE.emibTest.lockScreen.tabTitle}
                <span style={styles.tabBorder}></span>
              </label>
            </div>
          </div>
          <div style={styles.tabContentContainer} tabIndex={0}>
            <div style={styles.descriptionContainer}>
              <p>{LOCALIZE.emibTest.lockScreen.description.part1}</p>
              <p>{LOCALIZE.emibTest.lockScreen.description.part2}</p>
              <p>{LOCALIZE.emibTest.lockScreen.description.part3}</p>
              <br />
              <p>{LOCALIZE.emibTest.lockScreen.description.part4}</p>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { LockScreen as unconnectedLockScreen };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    assignedTestId: state.assignedTest.assignedTestId,
    testId: state.assignedTest.testId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestSection,
      unlockTest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(LockScreen);
