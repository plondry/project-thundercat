import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import LOCALIZE from "../../text_resources";
import { quitTest } from "../../modules/TestStatusRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  button: {
    marginRight: 15,
    minWidth: 25
  },
  checkboxZone: {
    paddingTop: 8
  },
  startTestBtn: {
    textAlign: "center",
    marginTop: 32
  }
};

const quitConditions = () => {
  return [
    { text: LOCALIZE.emibTest.testFooter.quitTestPopupBox.checkboxOne, checked: false },
    { text: LOCALIZE.emibTest.testFooter.quitTestPopupBox.checkboxTwo, checked: false },
    { text: LOCALIZE.emibTest.testFooter.quitTestPopupBox.checkboxThree, checked: false }
  ];
};

class QuitTest extends Component {
  static propTypes = {
    setFocusOnQuitTestButton: PropTypes.bool.isRequired,
    // Provided by Redux
    isTestActive: PropTypes.bool,
    currentAssignedTestId: PropTypes.number,
    handleQuitTest: PropTypes.func
  };

  state = {
    showQuitPopup: false,
    quitConditions: quitConditions()
  };

  closePopup = () => {
    this.setState({ showQuitPopup: false });
    //reset all checkbox states on close
    this.resetCheckboxStates();
  };

  openQuitPopup = () => {
    // Ensure language state is reset.
    this.setState({ quitConditions: quitConditions() });
    // Open the dialog.
    this.setState({ showQuitPopup: true });
  };

  resetCheckboxStates = () => {
    this.setState({ quitConditions: quitConditions() });
  };

  toggleCheckbox = id => {
    let updatedQuitConditions = Array.from(this.state.quitConditions);
    updatedQuitConditions[id].checked = !updatedQuitConditions[id].checked;
    this.setState({ quitConditions: updatedQuitConditions });
  };

  isChecked = currentCheckbox => {
    return currentCheckbox.checked;
  };

  // focusing on Quit test button on enter eMIB action
  componentDidMount = () => {
    if (this.props.setFocusOnQuitTestButton) {
      document.getElementById("quit-test-btn").focus();
    }
  };

  render() {
    const { quitConditions } = this.state;
    const allChecked = quitConditions.every(this.isChecked);

    const submitButtonState = allChecked ? BUTTON_STATE.enabled : BUTTON_STATE.disabled;
    return (
      <div>
        {this.props.isTestActive && (
          <div>
            <CustomButton
              label={<FontAwesomeIcon icon={faSignOutAlt} />}
              action={this.openQuitPopup}
              customStyle={styles.button}
              buttonTheme={this.props.isTestActive ? THEME.PRIMARY_IN_TEST : THEME.PRIMARY}
              ariaLabel={LOCALIZE.ariaLabel.quitTest}
            />
            <PopupBox
              show={this.state.showQuitPopup}
              handleClose={this.closePopup}
              title={LOCALIZE.emibTest.testFooter.quitTestPopupBox.title}
              description={
                <div>
                  <p>{LOCALIZE.emibTest.testFooter.quitTestPopupBox.description}</p>
                  <div>
                    {this.state.quitConditions.map((condition, id) => {
                      return (
                        <div
                          key={id}
                          className="custom-control custom-checkbox"
                          style={styles.checkboxZone}
                        >
                          <input
                            type="checkbox"
                            className="custom-control-input"
                            id={id}
                            checked={condition.checked}
                            onChange={() => this.toggleCheckbox(id)}
                          />
                          <label className="custom-control-label" htmlFor={id}>
                            {condition.text}
                          </label>
                        </div>
                      );
                    })}
                  </div>
                </div>
              }
              leftButtonType={BUTTON_TYPE.danger}
              leftButtonTitle={LOCALIZE.commons.quitTest}
              leftButtonAction={this.props.handleQuitTest}
              leftButtonState={submitButtonState}
              rightButtonType={BUTTON_TYPE.primary}
              rightButtonTitle={LOCALIZE.commons.returnToTest}
            />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.testStatus.isTestActive,
    currentAssignedTestId: state.testStatus.currentAssignedTestId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      quitTest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuitTest);
