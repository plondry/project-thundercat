import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronRight,
  faChevronLeft,
  faExclamationCircle
} from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  buttonRowPadding: {
    paddingBottom: 60,
    paddingTop: 10,
    margin: "0 auto"
  },
  buttonNext: {
    float: "right"
  },
  buttonReview: {
    justifyContent: "center",
    width: "100%"
  },
  span: {
    paddingLeft: 5,
    paddingRight: 5
  }
};

class NextPreviousButtonNav extends Component {
  static propTypes = {
    showNext: PropTypes.bool,
    showPrevious: PropTypes.bool,
    onChangeToNext: PropTypes.func.isRequired,
    onChangeToPrevious: PropTypes.func.isRequired,
    onMarkForReview: PropTypes.func
  };

  render() {
    return (
      <Row style={styles.buttonRowPadding}>
        <Col sm>
          {this.props.showPrevious && (
            <CustomButton
              tabIndex={"-1"}
              label={
                <>
                  <FontAwesomeIcon icon={faChevronLeft} />
                  <span style={styles.span}>{LOCALIZE.mcTest.questionList.previousButton}</span>
                </>
              }
              action={this.props.onChangeToPrevious}
              customStyle={styles.previousButton}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.ariaLabel.previousButton}
            ></CustomButton>
          )}
        </Col>

        <Col sm>
          {this.props.showReview && (
            <CustomButton
              tabIndex={"-1"}
              label={
                <div>
                  <FontAwesomeIcon icon={faExclamationCircle} />
                  <span style={styles.span}>
                    {this.props.isMarkedForReview
                      ? LOCALIZE.mcTest.questionList.markedReviewButton
                      : LOCALIZE.mcTest.questionList.reviewButton}
                  </span>
                </div>
              }
              action={this.props.onMarkForReview}
              customStyle={styles.buttonReview}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.ariaLabel.markedReviewButton}
            ></CustomButton>
          )}
        </Col>

        <Col sm>
          {this.props.showNext && (
            <CustomButton
              tabIndex={"-1"}
              label={
                <div>
                  <span style={styles.span}>{LOCALIZE.mcTest.questionList.nextButton}</span>
                  <FontAwesomeIcon icon={faChevronRight} />
                </div>
              }
              action={this.props.onChangeToNext}
              customStyle={styles.buttonNext}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.ariaLabel.nextButton}
            ></CustomButton>
          )}
        </Col>
      </Row>
    );
  }
}

export default NextPreviousButtonNav;
