import React, { Component } from "react";
import PropTypes from "prop-types";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../text_resources";
import FontFamily from "./FontFamily";
import FontSize from "./FontSize";
import CustomButton, { THEME } from "../../components/commons/CustomButton";
import Spacing from "./Spacing";

const styles = {
  button: {
    minWidth: 40,
    marginRight: 15
  }
};

class Settings extends Component {
  state = {
    isDialogOpen: false
  };

  static propTypes = {
    isTestActive: PropTypes.bool
  };

  toggleDialog = () => {
    this.setState({ isDialogOpen: !this.state.isDialogOpen });
  };

  render() {
    return (
      <div>
        <CustomButton
          label={<FontAwesomeIcon icon={faCog} />}
          action={this.toggleDialog}
          customStyle={styles.button}
          buttonTheme={this.props.isTestActive ? THEME.PRIMARY_IN_TEST : THEME.PRIMARY}
          ariaLabel={LOCALIZE.settings.systemSettings}
        />
        <PopupBox
          show={this.state.isDialogOpen}
          handleClose={this.toggleDialog}
          title={LOCALIZE.settings.systemSettings}
          description={
            <div>
              <section aria-labelledby="zoom-instructions">
                <h2 id="zoom-instructions">{LOCALIZE.settings.zoom.title}</h2>
                <ol>
                  <li>{LOCALIZE.settings.zoom.instructionsListItem1}</li>
                  <li>{LOCALIZE.settings.zoom.instructionsListItem2}</li>
                  <li>{LOCALIZE.settings.zoom.instructionsListItem3}</li>
                  <li>{LOCALIZE.settings.zoom.instructionsListItem4}</li>
                </ol>
              </section>
              <section aria-labelledby="text-size-instructions">
                <h2 id="text-size-instructions">{LOCALIZE.settings.fontSize.title}</h2>
                <p>{LOCALIZE.settings.fontSize.description}</p>
                <FontSize />
              </section>
              <section aria-labelledby="font-instructions">
                <h2 id="font-instructions">{LOCALIZE.settings.fontStyle.title}</h2>
                <p>{LOCALIZE.settings.fontStyle.description}</p>
                <FontFamily />
              </section>
              <section aria-labelledby="line-spacing-instructions">
                <h2 id="line-spacing-instructions">{LOCALIZE.settings.lineSpacing.title}</h2>
                <p>{LOCALIZE.settings.lineSpacing.description}</p>
                <Spacing />
              </section>
              <section aria-labelledby="color-instructions">
                <h2 id="color-instructions">{LOCALIZE.settings.color.title}</h2>
                <ol>
                  <li>{LOCALIZE.settings.color.instructionsListItem1}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem2}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem3}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem4}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem5}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem6}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem7}</li>
                  <li>{LOCALIZE.settings.color.instructionsListItem8}</li>
                </ol>
              </section>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.toggleDialog}
        />
      </div>
    );
  }
}

export default Settings;
