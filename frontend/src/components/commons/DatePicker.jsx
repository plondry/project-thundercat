import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Select from "react-select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import "../../css/date-picker.css";
import {
  updateDatePicked,
  updateDatePickedValidState,
  resetDatePickedStates
} from "../../modules/DatePickerRedux";

const styles = {
  datePickerContainer: {
    display: "table"
  },
  monthAndDayField: {
    paddingRight: 36,
    display: "table-cell"
  },
  yearField: {
    display: "table-cell"
  },
  labelContainer: {
    textAlign: "center"
  },
  dateLabel: {
    padding: "3px 0",
    margin: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  validIcon: {
    color: "#278400",
    marginTop: 8,
    float: "left"
  },
  checkMark: {
    marginRight: 6
  }
};

class DatePicker extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      // provide refs if you need focus functionality for error validation
      dateDayFieldRef: PropTypes.object,
      dateMonthFieldRef: PropTypes.object,
      dateYearFieldRef: PropTypes.object,
      // provide DOB label id for accessibility/aria-labels
      dateLabelId: PropTypes.string,
      // options: {value: <value>, label: <label>}
      // if you do not provide custom options, the options will be the default ones (Day: from 1 to 31, Month: from 1 to 12, Year: from 1900 to today's date)
      customDayOptions: PropTypes.array,
      customMonthOptions: PropTypes.array,
      customYearOptions: PropTypes.array,
      // helps for validation
      triggerValidation: PropTypes.bool.isRequired,
      // allow displaying/hiding valid checkmark icon (default is true)
      displayValidIcon: PropTypes.bool,
      // provide initial field values if needed ({value: <dsired_initial_value>, label: <dsired_initial_value>})
      initialDateDayValue: PropTypes.object,
      initialDateMonthValue: PropTypes.object,
      initialDateYearValue: PropTypes.object,
      // provide using a trigger state if you want to reset all fields values (day, month and year) to empty
      triggerResetFieldValues: PropTypes.bool,
      // provided by redux
      updateDatePicked: PropTypes.func,
      updateDatePickedValidState: PropTypes.func,
      resetDatePickedStates: PropTypes.func,
      // allow to validate that the selected date is a valid date, but also a future date (will be invalid if the selected date is in the past)
      futureDateValidation: PropTypes.bool
    };

    DatePicker.defaultProps = {
      displayValidIcon: true,
      futureDateValidation: false
    };
  }

  state = {
    dateDayOptions: [],
    dateDaySelectedValue: this.props.initialDateDayValue ? this.props.initialDateDayValue : null,
    dateMonthOptions: [],
    dateMonthSelectedValue: this.props.initialDateMonthValue
      ? this.props.initialDateMonthValue
      : null,
    dateYearOptions: [],
    dateYearSelectedValue: this.props.initialDateYearValue ? this.props.initialDateYearValue : null,
    isValidCompleteDatePicked: true,
    isValidFutureDatePicked: true,
    validationTriggered: false
  };

  componentDidMount = () => {
    // reseting date picked valid state (redux state)
    this.props.resetDatePickedStates();
    // populating dropdowns
    this.populateDateDayOptions();
    this.populateDateMonthOptions();
    this.populateDateYearOptions();
    // update date picked redux state if initial values have been provided
    if (
      this.props.initialDateYearValue &&
      this.props.initialDateMonthValue &&
      this.props.initialDateDayValue
    ) {
      this.props.updateDatePicked(
        `${this.state.dateYearSelectedValue.value}-${this.state.dateMonthSelectedValue.value}-${this.state.dateDaySelectedValue.value}`
      );
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if validation gets triggered
    if (prevProps.triggerValidation !== this.props.triggerValidation) {
      const validDateBool = this.validateDatePicked(this.props.completeDatePicked);
      const validFutureDateBool = this.validateFutureDatePicked(this.props.completeDatePicked);
      // validating date and updating state
      this.setState({
        isValidCompleteDatePicked: validDateBool,
        isValidFutureDatePicked: validFutureDateBool,
        validationTriggered: true
      });
      // updating valid date picked redux state
      if (this.props.futureDateValidation) {
        this.props.updateDatePickedValidState(validDateBool && validFutureDateBool);
      } else {
        this.props.updateDatePickedValidState(validDateBool);
      }
      // updating complete date picked redux state
      this.props.updateDatePicked(this.props.completeDatePicked);
    }
    // getting initial date day, month, year values if provided on props change and updating completeDatePicked state
    // if day, month or year props or state changes
    if (
      prevProps.initialDateDayValue !== this.props.initialDateDayValue ||
      prevProps.initialDateMonthValue !== this.props.initialDateMonthValue ||
      prevProps.initialDateYearValue !== this.props.initialDateYearValue ||
      prevState.dateDaySelectedValue !== this.state.dateDaySelectedValue ||
      prevState.dateMonthSelectedValue !== this.state.dateMonthSelectedValue ||
      prevState.dateYearSelectedValue !== this.state.dateYearSelectedValue
    ) {
      // updating completeDatePicked state with the new value(s)
      this.props.updateDatePicked(
        `${
          this.state.dateYearSelectedValue !== null ? this.state.dateYearSelectedValue.label : null
        }-${
          this.state.dateMonthSelectedValue !== null
            ? this.getFormattedDateField(this.state.dateMonthSelectedValue.label)
            : null
        }-${
          this.state.dateDaySelectedValue !== null
            ? this.getFormattedDateField(this.state.dateDaySelectedValue.label)
            : null
        }`
      );
    }
    // if triggerResetFieldValues get updated
    if (prevProps.triggerResetFieldValues !== this.props.triggerResetFieldValues) {
      // resetting selected options
      this.setState({
        dateDaySelectedValue: null,
        dateMonthSelectedValue: null,
        dateYearSelectedValue: null,
        isValidCompleteDatePicked: true,
        isValidFutureDatePicked: true
      });
    }
  };

  // getting formatted date fields (day or month fields only)
  // if label is 5 for example, it will return 05 and if label is 16, it will return 16 as expected
  getFormattedDateField = label => {
    // if label is only one digit (5 for exemple)
    if (label.length < 2) {
      // put a '0' in front of the label
      return `0${label}`;
      // else simply return the label
    } else {
      return label;
    }
  };

  // populate day field from 1 to 31
  populateDateDayOptions = () => {
    let dayOptionsArray = [];
    // loop from 1 to 31
    for (let i = 1; i <= 31; i++) {
      // push each value in dayOptionsArray
      if (i < 10) {
        dayOptionsArray.push({ value: i, label: `0${i}` });
      } else {
        dayOptionsArray.push({ value: i, label: `${i}` });
      }
    }
    // saving result in state
    this.setState({ dateDayOptions: dayOptionsArray });
  };

  // populate month field from 1 to 12
  populateDateMonthOptions = () => {
    let monthOptionsArray = [];
    // loop from 1 to 12
    for (let i = 1; i <= 12; i++) {
      // push each value in monthOptionsArray
      if (i < 10) {
        monthOptionsArray.push({ value: i, label: `0${i}` });
      } else {
        monthOptionsArray.push({ value: i, label: `${i}` });
      }
    }
    // saving result in state
    this.setState({ dateMonthOptions: monthOptionsArray });
  };

  // populate year field from 1900 to current year
  populateDateYearOptions = () => {
    let yearOptionsArray = [];
    // loop from 1900 to current year
    for (let i = 1900; i <= new Date().getFullYear(); i++) {
      // push each value in yearOptionsArray
      yearOptionsArray.push({ value: i, label: `${i}` });
    }
    // sorting (descending order)
    yearOptionsArray.reverse();
    // saving result in state
    this.setState({ dateYearOptions: yearOptionsArray });
  };

  // get selected day
  getSelectedDateDay = selectedOption => {
    this.setState({
      dateDaySelectedValue: selectedOption
    });
  };

  // get selected month
  getSelectedDateMonth = selectedOption => {
    this.setState({
      dateMonthSelectedValue: selectedOption
    });
  };

  // get selected year
  getSelectedDateYear = selectedOption => {
    this.setState({
      dateYearSelectedValue: selectedOption
    });
  };

  //ref: https://stackoverflow.com/questions/8647893/regular-expression-leap-years-and-more
  validateDatePicked(input) {
    // replacing all dashes with forward slashes (to avoid IE date validation incompatibility)
    let newInput = input.split("-").join("/");
    const date = new Date(newInput);
    input = newInput.split("/");
    return (
      date.getFullYear() === +input[0] &&
      date.getMonth() + 1 === +input[1] &&
      date.getDate() === +input[2]
    );
  }

  validateFutureDatePicked(input) {
    // replacing all dashes with forward slashes (to avoid IE date validation incompatibility)
    let newInput = input.split("-").join("/");
    const date = new Date(newInput);
    input = newInput.split("/");
    // if futureDateValidation props is defined/set to true
    if (this.props.futureDateValidation) {
      // this is a future date (from today to future date)
      if (date >= new Date()) {
        return true;
        // valid date, but in the past, so invalid
      } else {
        return false;
      }
      // futureDateValidation is not defined/set to false, so return true since it is valid
    } else {
      return true;
    }
  }

  render() {
    const { dateDaySelectedValue, dateMonthSelectedValue, dateYearSelectedValue } = this.state;

    // converting current font size in Int
    const fontSizeInt = parseInt(this.props.accommodations.fontSize.substring(0, 2));
    // initializing dropdown width array (used to define the width of the date picker dropdowns depending on the font size selected)
    let dropdownWidthArray = [];
    // initializing label margin (used only when font size is really big)
    let monthLabelMargin = "0px";
    // converting date picker dropdowns width
    switch (true) {
      case fontSizeInt <= 22:
        dropdownWidthArray = [120, 110];
        break;
      case fontSizeInt > 22 && fontSizeInt <= 30:
        dropdownWidthArray = [130, 130];
        break;
      case fontSizeInt > 30 && fontSizeInt <= 36:
        dropdownWidthArray = [140, 150];
        break;
      case fontSizeInt > 36:
        dropdownWidthArray = [160, 175];
        monthLabelMargin = "0 -30px";
        break;
      default:
        dropdownWidthArray = [120, 150];
    }

    return (
      <div style={styles.datePickerContainer}>
        <div>
          <div style={{ ...styles.monthAndDayField, ...{ width: dropdownWidthArray[0] } }}>
            <Select
              ref={this.props.dateDayFieldRef}
              id="selected-day-field"
              className={
                this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked
                  ? "valid-field"
                  : "invalid-field"
              }
              name="date-day-field"
              aria-labelledby={`${this.props.dateLabelId} ${
                !this.state.isValidCompleteDatePicked ? "date-error" : ""
              } ${
                this.state.isValidCompleteDatePicked && !this.state.isValidFutureDatePicked
                  ? "future-date-error"
                  : ""
              } day-field-selected current-day-value-intro day-field-value`}
              aria-required={true}
              placeholder=""
              options={
                this.props.customDayOptions
                  ? this.props.customDayOptions
                  : this.state.dateDayOptions
              }
              onChange={this.getSelectedDateDay}
              clearable={false}
              value={dateDaySelectedValue}
            ></Select>
            <div style={styles.labelContainer}>
              <label id="day-field-unit-test" style={styles.dateLabel}>
                {LOCALIZE.datePicker.dayField}
              </label>
              <label id="day-field-selected" style={styles.hiddenText}>
                {LOCALIZE.datePicker.dayFieldSelected}
              </label>
              <label id="current-day-value-intro" style={styles.hiddenText}>
                {LOCALIZE.datePicker.currentValue}
              </label>
              <label id="day-field-value" style={styles.hiddenText}>
                {this.props.completeDatePicked.split("-")[2] !== "null"
                  ? this.props.completeDatePicked.split("-")[2]
                  : LOCALIZE.datePicker.none}
              </label>
            </div>
          </div>
          <div
            style={{
              ...styles.monthAndDayField,
              ...{ width: dropdownWidthArray[0] }
            }}
          >
            <Select
              ref={this.props.dateMonthFieldRef}
              id="selected-month-field"
              className={
                this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked
                  ? "valid-field"
                  : "invalid-field"
              }
              name="date-month-field"
              aria-labelledby={`${this.props.dateLabelId} month-field-selected current-month-value-intro month-field-value`}
              aria-required={true}
              placeholder=""
              options={
                this.props.customMonthOptions
                  ? this.props.customMonthOptions
                  : this.state.dateMonthOptions
              }
              onChange={this.getSelectedDateMonth}
              clearable={false}
              value={dateMonthSelectedValue}
            ></Select>
            <div style={styles.labelContainer}>
              <label
                id="month-field-unit-test"
                style={{ ...styles.dateLabel, ...{ margin: monthLabelMargin } }}
              >
                {LOCALIZE.datePicker.monthField}
              </label>
              <label id="month-field-selected" style={styles.hiddenText}>
                {LOCALIZE.datePicker.monthFieldSelected}
              </label>
              <label id="current-month-value-intro" style={styles.hiddenText}>
                {LOCALIZE.datePicker.currentValue}
              </label>
              <label id="month-field-value" style={styles.hiddenText}>
                {this.props.completeDatePicked.split("-")[1] !== "null"
                  ? this.props.completeDatePicked.split("-")[1]
                  : LOCALIZE.datePicker.none}
              </label>
            </div>
          </div>
          <div style={{ ...styles.yearField, ...{ width: dropdownWidthArray[1] } }}>
            <Select
              ref={this.props.dateYearFieldRef}
              id="selected-year-field"
              className={
                this.state.isValidCompleteDatePicked && this.state.isValidFutureDatePicked
                  ? "valid-field"
                  : "invalid-field"
              }
              name="date-year-field"
              aria-labelledby={`${this.props.dateLabelId} year-field-selected current-year-value-intro year-field-value`}
              aria-required={true}
              placeholder=""
              options={
                this.props.customYearOptions
                  ? this.props.customYearOptions
                  : this.state.dateYearOptions
              }
              onChange={this.getSelectedDateYear}
              clearable={false}
              value={dateYearSelectedValue}
            ></Select>
            <div style={styles.labelContainer}>
              <label id="year-field-unit-test" style={styles.dateLabel}>
                {LOCALIZE.datePicker.yearField}
              </label>
              <label id="year-field-selected" style={styles.hiddenText}>
                {LOCALIZE.datePicker.yearFieldSelected}
              </label>
              <label id="current-year-value-intro" style={styles.hiddenText}>
                {LOCALIZE.datePicker.currentValue}
              </label>
              <label id="year-field-value" style={styles.hiddenText}>
                {this.props.completeDatePicked.split("-")[0] !== "null"
                  ? this.props.completeDatePicked.split("-")[0]
                  : LOCALIZE.datePicker.none}
              </label>
            </div>
          </div>
          {this.props.displayValidIcon &&
            this.state.isValidCompleteDatePicked &&
            this.state.isValidFutureDatePicked &&
            this.state.validationTriggered && (
              <div style={styles.validIcon}>
                <FontAwesomeIcon style={styles.checkMark} icon={faCheckCircle} />
                {LOCALIZE.commons.valid}
              </div>
            )}
        </div>
        {!this.state.isValidCompleteDatePicked && (
          <label id="date-error" htmlFor="selected-day-field" style={styles.errorMessage}>
            {LOCALIZE.datePicker.datePickedError}
          </label>
        )}
        {this.state.isValidCompleteDatePicked && !this.state.isValidFutureDatePicked && (
          <label id="future-date-error" htmlFor="selected-day-field" style={styles.errorMessage}>
            {LOCALIZE.datePicker.futureDatePickedError}
          </label>
        )}
      </div>
    );
  }
}

export { DatePicker as unconnectedDatePicker };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateDatePicked,
      updateDatePickedValidState,
      resetDatePickedStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(DatePicker);
