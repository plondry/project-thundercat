import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Col, Row } from "react-bootstrap";
import Switch from "react-switch";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setSpacing, saveUserAccommodations } from "../../modules/AccommodationsRedux";
import "../../css/font-select.css";

class Spacing extends Component {
  onChange = event => {
    this.props.setSpacing(event);
    if (this.props.authenticated) {
      this.props.saveUserAccommodations({ ...this.props.accommodations, spacing: event });
    }
  };

  render() {
    // defining styles here because it wouldn't allow modifcation of values when defined outside.
    let styles = {
      container: {
        margin: "12px 25px"
      },
      textSizeContainer: {
        display: "table"
      },
      textSizeLabel: {
        marginTop: "auto",
        marginBottom: "auto",
        width: 200,
        display: "table-cell"
      },
      switchContainer: {
        position: "relative"
      },
      child: {
        position: "absolute",
        top: "50%",
        transform: "translateY(-50%)"
      }
    };

    return (
      <div style={styles.container}>
        <Row style={styles.textSizeContainer}>
          <Col style={styles.textSizeLabel}>{LOCALIZE.settings.lineSpacing.title}</Col>
          <Col style={styles.switchContainer}>
            <div style={styles.child}>
              <Switch onChange={this.onChange} checked={this.props.accommodations.spacing} />
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    fontFamily: state.accommodations.fontFamily,
    authenticated: state.login.authenticated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setSpacing, saveUserAccommodations }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Spacing);
