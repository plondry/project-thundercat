// This component simply displays a span containing time in hours, minutes, and seconds
// This is based off of properties passed in from the parent

import React, { Component } from "react";
import PropTypes from "prop-types";

class TimerCalculation extends Component {
  static propTypes = {
    startTime: PropTypes.string,
    timeLimit: PropTypes.number,
    timeoutFunction: PropTypes.func.isRequired,
    customStyle: PropTypes.object
  };

  state = {
    millisecondTime: 0
  };

  updateRemainingTime = () => {
    const { startTime, timeLimit } = this.props;
    // If no start time, then we have nothing to calculate from
    if (startTime === null) {
      this.setState({ millisecondTime: 0 });
      clearInterval(this.timer);
      return;
    }
    const newTime = calculateRemainingTime(
      startTime,
      timeLimit,
      Date.now(),
      this.props.timeoutFunction
    );
    if (newTime == null) {
      clearInterval(this.timer);
      return; // this return is important, as it prevents an update on unmounted component warning
    }
    this.setState({
      millisecondTime: newTime
    });
  };

  componentDidMount() {
    // Call once to set the time accurately, then set the interval
    // If it is not called immediately, then the user will see 00:00:00 for a second before it updates
    this.updateRemainingTime();
    this.timer = setInterval(this.updateRemainingTime, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  render() {
    const { millisecondTime } = this.state;
    // calcuate seconds, minutes, and hours
    var seconds = Math.floor((millisecondTime / 1000) % 60);
    var minutes = Math.floor((millisecondTime / (1000 * 60)) % 60);
    var hours = Math.floor((millisecondTime / (1000 * 60 * 60)) % 24);
    // ensure that the numbers are nicely formatted
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    return (
      <span style={this.props.customStyle ? this.props.customStyle : {}}>
        {hours}:{minutes}:{seconds}
      </span>
    );
  }
}

// This is a seperate function so that it can be tested
export const calculateRemainingTime = (startTime, timeLimit, now, timeoutFunction) => {
  // If there is no start time, then there is nothing to calculate
  if (startTime === null) {
    return 0;
  }
  // this should never happen, but placeholder
  if (now === null) {
    return 0;
  }
  // If there is no timelimit, then we count up
  if (timeLimit === null || typeof timeLimit === "undefined") {
    return now - new Date(startTime).getTime();
  }
  // Otherwise we count down
  // Convert time limit from minutes to milliseconds
  const limitInMilisecs = 60000 * timeLimit;
  const timeRemaining = limitInMilisecs + new Date(startTime).getTime() - now;
  if (timeRemaining <= 0) {
    timeoutFunction();
    // return null to flag that it has ended
    return null;
  }
  return timeRemaining;
};

export default TimerCalculation;
