import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Col, Row } from "react-bootstrap";
import ReactSelect from "react-select";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setFontFamily, saveUserAccommodations } from "../../modules/AccommodationsRedux";
import "../../css/font-select.css";
import { fontFamilyDefinition } from "./Constants";

class FontFamily extends Component {
  state = { fontStyle: { label: "default", value: "default" } };

  static propTypes = {};

  onChange = event => {
    this.props.setFontFamily(event.value);
    if (this.props.authenticated) {
      this.props.saveUserAccommodations({ ...this.props.accommodations, fontFamily: event.value });
    }
  };

  getValue = () => {
    return { label: this.props.fontFamily, value: this.props.fontFamily };
  };

  render() {
    const value = this.getValue();

    const options = fontFamilyDefinition();

    // defining styles here because it wouldn't allow modifcation of values when defined outside.
    let styles = {
      container: {
        margin: "12px 25px",
        width: "100%"
      },
      textSizeContainer: {
        display: "table"
      },
      textSizeLabel: {
        marginTop: "auto",
        marginBottom: "auto",
        width: "50%",
        display: "table-cell"
      },
      dropdown: {
        padding: 0,
        width: 250
      }
    };
    if (value && value !== "default") {
      styles.container.fontFamily = value.value;
    }

    return (
      <div style={styles.container}>
        <Row style={styles.textSizeContainer}>
          <Col style={styles.textSizeLabel}>{LOCALIZE.settings.fontStyle.title}</Col>
          <Col style={styles.dropdown}>
            <ReactSelect
              classNamePrefix={"font-family"}
              styles={{
                // Fixes the overlapping problem of the component
                menu: provided => ({ ...provided, zIndex: 9999 })
              }}
              menuPlacement="auto"
              id={"unit-test-font-family-dropdown"}
              onChange={this.onChange}
              value={value}
              options={options}
              clearable={false}
              searchable={true}
              aria-labelledby={LOCALIZE.settings.fontStyle.title}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    fontFamily: state.accommodations.fontFamily,
    authenticated: state.login.authenticated,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setFontFamily, saveUserAccommodations }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FontFamily);
