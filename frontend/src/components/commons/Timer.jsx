import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle, faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TimerCalculation from "./TimerCalculation";
import CustomButton, { THEME } from "./CustomButton";

const styles = {
  container: {
    width: 350
  },
  timeContainer: {
    float: "left",
    border: "2px solid #00565e",
    padding: 5,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    fontWeight: "bold",
    color: "#00565e",
    backgroundColor: "#FFFFFF",
    textAlign: "center"
  },
  toggleButtonOpen: {
    float: "left",
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0
  },
  toggleButton: {
    float: "left",
    borderRadius: 5
  },
  label: {
    paddingLeft: 5
  },
  timeOut: {
    color: "#D3080C"
  }
};

class Timer extends Component {
  static propTypes = {
    timeout: PropTypes.func.isRequired,
    startTime: PropTypes.string,
    timeLimit: PropTypes.number
  };

  state = {
    hidden: false
  };

  toggleVisibility = () => {
    this.setState({ hidden: !this.state.hidden });
  };

  timeout = () => {
    this.props.timeout();
  };

  render() {
    const { hidden } = this.state;
    const isTimeAlmostOut = false;
    return (
      <div style={styles.container}>
        <CustomButton
          label={
            <div>
              <FontAwesomeIcon icon={hidden ? faPlusCircle : faMinusCircle} />
              <span style={styles.label}>{LOCALIZE.emibTest.testFooter.timer.timer}</span>
            </div>
          }
          id="unit-test-toggle-timer"
          action={this.toggleVisibility}
          customStyle={hidden ? styles.toggleButton : styles.toggleButtonOpen}
          ariaLabel={
            hidden
              ? LOCALIZE.emibTest.testFooter.timer.timerShow
              : LOCALIZE.emibTest.testFooter.timer.timerHide
          }
          buttonTheme={THEME.PRIMARY_TIME}
        ></CustomButton>
        <div>
          {!hidden && (
            <div style={styles.timeContainer}>
              <div style={isTimeAlmostOut ? styles.timeOut : {}}>
                <span className="visually-hidden" id="unit-test-time-label" tabIndex={0}>
                  {
                    <TimerCalculation
                      startTime={this.props.startTime}
                      timeLimit={this.props.timeLimit}
                      timeoutFunction={this.timeout}
                    />
                  }
                </span>
                <TimerCalculation
                  startTime={this.props.startTime}
                  timeLimit={this.props.timeLimit}
                  timeoutFunction={this.timeout}
                />
              </div>
            </div>
          )}
          {hidden && (
            <div className="visually-hidden">
              <TimerCalculation
                startTime={this.props.startTime}
                timeLimit={this.props.timeLimit}
                timeoutFunction={this.timeout}
              />
            </div>
          )}
        </div>
      </div>
    );
  }
}

export { Timer as UnconnectedTimer };

const mapStateToProps = (state, ownProps) => {
  return { currentLanguage: state.localize.language };
};

export default connect(mapStateToProps, null)(Timer);
