import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { Col, Row } from "react-bootstrap";
import ReactSelect from "react-select";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { setFontSize, saveUserAccommodations } from "../../modules/AccommodationsRedux";
import { fontSizeDefinition } from "./Constants";

class FontSize extends Component {
  state = {
    fontSize: { label: "default", value: "default" }
  };

  onChange = event => {
    this.props.setFontSize(event.value);
    if (this.props.authenticated) {
      this.props.saveUserAccommodations({ ...this.props.accommodations, fontSize: event.value });
    }
  };

  getValue = () => {
    return { label: this.props.fontSize, value: this.props.fontSize };
  };

  render() {
    const inputName = LOCALIZE.settings.fontSize.title;
    const value = this.getValue();

    const options = fontSizeDefinition();

    // defining size here because it wouldn't allow modifcation of values when defined outside.
    let styles = {
      container: {
        margin: "12px 25px",
        width: "100%"
      },
      textSizeContainer: {
        display: "table"
      },
      textSizeLabel: {
        marginTop: "auto",
        marginBottom: "auto",
        width: "50%",
        display: "table-cell"
      },
      dropdown: {
        padding: 0,
        width: 250
      }
    };
    if (value && value !== "default") {
      styles.container.fontFamily = value.value;
    }

    return (
      <div style={styles.container}>
        <Row style={styles.textSizeContainer}>
          <Col style={styles.textSizeLabel}>{LOCALIZE.settings.fontSize.title}</Col>
          <Col style={styles.dropdown}>
            <ReactSelect
              classNamePrefix={inputName}
              styles={{
                // Fixes the overlapping problem of the component
                menu: provided => ({ ...provided, zIndex: 9999 })
              }}
              menuPlacement="auto"
              id={inputName}
              name={inputName}
              aria-labelledby={`${inputName}-selected ${inputName}-placeholder`}
              placeholder={inputName}
              onChange={this.onChange}
              value={value}
              options={options}
              clearable={false}
              searchable={false}
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    fontSize: state.accommodations.fontSize,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ setFontSize, saveUserAccommodations }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FontSize);
