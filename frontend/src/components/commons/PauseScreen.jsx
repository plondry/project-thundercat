import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import ContentContainer from "../commons/ContentContainer";
import TimerCalculation from "../commons/TimerCalculation";
import { unpauseTest, updateTestStatus, deactivateTest } from "../../modules/TestStatusRedux";
import { TEST_STATUS } from "../ta/Constants";
import { PATH } from "../../components/commons/Constants";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import { getCurrentSocket, CHANNELS_ACTION } from "../../helpers/testSessionChannels";

const styles = {
  tabSection: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  tabLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  tabContentContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    height: "calc(100vh - 140px",
    padding: "12px 0",
    marginBottom: 24
  },
  descriptionContainer: {
    margin: 60,
    fontWeight: "bold"
  },
  timerContainer: {
    textAlign: "center"
  },
  timer: {
    width: 300,
    height: 100,
    fontSize: 48,
    color: "#00565e",
    fontWeight: "bold",
    margin: "0 auto",
    border: "1px solid #00565e",
    display: "table"
  },
  customTimerStyle: {
    display: "table-cell",
    verticalAlign: "middle"
  }
};

class PauseScreen extends Component {
  static propTypes = {
    sockets: PropTypes.array,
    testId: PropTypes.number,
    assignedTestId: PropTypes.number,
    previousStatus: PropTypes.number,
    testAccessCode: PropTypes.string,
    // Provided by Redux
    unpauseTest: PropTypes.func,
    updateTestStatus: PropTypes.func,
    deactivateTest: PropTypes.func
  };

  componentDidMount = () => {
    // focusing on test locked tab
    if (document.getElementById("test-paused-tab")) {
      document.getElementById("test-paused-tab").focus();
    }
  };

  handleTimerTimeout = () => {
    // getting needed parameters
    const paramsObj = {
      username_id: this.props.username,
      test_id: this.props.testId
    };
    // getting current socket
    getCurrentSocket(this.props.sockets, this.props.testAccessCode)
      .then(currentSocket => {
        // sending message to TA
        currentSocket.send(
          JSON.stringify({
            action: CHANNELS_ACTION.pauseTestTimeout,
            parameters: JSON.stringify(paramsObj),
            test_access_code: this.props.testAccessCode
          })
        );
      })
      .then(() => {
        // test was paused but not started yet
        if (this.props.previousStatus === TEST_STATUS.READY) {
          this.props.updateTestStatus(this.props.assignedTestId, TEST_STATUS.READY).then(() => {
            // deactivating the test
            this.props.deactivateTest();
            // removing TEST_ACTIVE from local storage
            SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);
            // redirecting to dashboard page
            window.location.href = PATH.dashboard;
          });
          // test was started and then paused
        } else {
          this.props
            .updateTestStatus(this.props.assignedTestId, TEST_STATUS.ACTIVE, TEST_STATUS.PAUSED)
            .then(() => {
              // redirecting to test page
              window.location.href = PATH.testBase;
            });
        }
      });
  };

  render() {
    return (
      <ContentContainer>
        <div id="test-paused-tab" style={styles.tabSection} tabIndex={0}>
          <div>
            <label style={styles.tabLabel}>
              {LOCALIZE.emibTest.pauseScreen.tabTitle}
              <span style={styles.tabBorder}></span>
            </label>
          </div>
        </div>
        <div style={styles.tabContentContainer} tabIndex={0}>
          <div style={styles.descriptionContainer}>
            <p>{LOCALIZE.emibTest.pauseScreen.description.part1}</p>
            <p>{LOCALIZE.emibTest.pauseScreen.description.part2}</p>
            <p>{LOCALIZE.emibTest.pauseScreen.description.part3}</p>
          </div>
          <div style={styles.timerContainer}>
            <div style={styles.timer}>
              <TimerCalculation
                startTime={this.props.pauseStartDate}
                timeLimit={this.props.pauseTime}
                timeoutFunction={this.handleTimerTimeout}
                customStyle={styles.customTimerStyle}
              />
            </div>
          </div>
        </div>
      </ContentContainer>
    );
  }
}

export { PauseScreen as unconnectedPauseScreen };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    pauseTime: state.testStatus.pauseTime,
    pauseStartDate: state.testStatus.pauseStartDate,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      unpauseTest,
      updateTestStatus,
      deactivateTest
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PauseScreen);
