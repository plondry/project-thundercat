import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "../../css/custom-button.css";

export const THEME = {
  PRIMARY: "primary",
  PRIMARY_IN_TEST: "primary-in-test",
  SECONDARY: "secondary",
  SUCCESS: "success",
  DANGER: "danger",
  PRIMARY_WIDE: "primary-wide",
  PRIMARY_TIME: "primary-time",
};

const styles = {
  basics: {
    minWidth: 100,
    fontWeight: "bold",
    padding: "6px 12px",
    borderRadius: "0.35rem",
    border: "1px solid transparent"
  }
};

class CustomButton extends Component {
  static propTypes = {
    id: PropTypes.string,
    className: PropTypes.string,
    label: PropTypes.any.isRequired,
    action: PropTypes.func,
    type: PropTypes.string,
    customStyle: PropTypes.object,
    buttonTheme: PropTypes.string,
    disabled: PropTypes.bool,
    language: PropTypes.string,
    ariaLabel: PropTypes.string,
    ariaExpanded: PropTypes.bool,
    ariaDescribedBy: PropTypes.string,
    tabIndex: PropTypes.string
  };

  render() {
    let accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      accommodationsStyle = {
        ...accommodationsStyle,
        lineHeight: "1.5em",
        letterSpacing: "0.12em",
        wordSpacing: "0.16em"
      };
    }

    return (
      <button
        id={this.props.id}
        className={this.props.buttonTheme}
        style={{ ...styles.basics, ...this.props.customStyle, ...accommodationsStyle }}
        type={this.props.type}
        onClick={this.props.action}
        disabled={this.props.disabled}
        lang={this.props.lang}
        aria-label={this.props.ariaLabel}
        aria-expanded={this.props.ariaExpanded}
        aria-describedby={this.props.ariaDescribedBy}
        tabIndex={this.props.tabIndex}
      >
        {this.props.label}
      </button>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(CustomButton);
