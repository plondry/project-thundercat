import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Select from "react-select";
import CustomButton, { THEME } from "../CustomButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPoll, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { REPORT_TYPES } from "./Constants";
import { CSVLink } from "react-csv";
import { generateIndividualScoreSheetReport } from "./GenerateIndividualScoreSheetReport";
import { generateResultsReport } from "./GenerateResultsReport";
import { generateFinancialReport } from "./GenerateFinancialReport";
import PopupBox, { BUTTON_TYPE } from "../../../components/commons/PopupBox";
import getTaAssignedTestOrderNumbers, {
  getAllExistingTestOrderNumbers,
  getTestsBasedOnTestOrderNumber,
  getResultsReportData,
  getFinancialReportData
} from "../../../modules/ReportsRedux";

export const REPORT_REQUESTOR = {
  etta: "etta",
  ppc: "ppc",
  ta: "ta"
};

const styles = {
  dropdownsMainContainer: {
    padding: 24,
    width: "100%"
  },
  optionSelectionContainer: {
    padding: "12px 24px",
    margin: "0 auto",
    width: "70%",
    display: "table"
  },
  labelContainer: {
    display: "table-cell",
    width: "35%",
    verticalAlign: "middle",
    paddingRight: 12
  },
  singleDropdownContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  generateButtonContainer: {
    textAlign: "center",
    marginTop: 18
  },
  generateButtonIcon: {
    transform: "scale(1.5)",
    padding: "1.5px",
    marginRight: 12
  },
  generateButtonLabel: {
    minWidth: 150
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class ReportGenerator extends Component {
  constructor(props) {
    super(props);
    this.createReportLinkButtonRef = React.createRef();
  }

  static propTypes = {
    reportRequestor: PropTypes.string.isRequired,
    reportTypeOptions: PropTypes.array.isRequired
  };

  state = {
    selectedReportType: null,
    testOrderNumberVisible: false,
    testOrderNumberOptions: [],
    testOrderNumberData: [],
    selectedTestOrderNumber: null,
    testVisible: false,
    testOptions: [],
    testData: [],
    selectedTest: null,
    candidateVisible: false,
    candidateOptions: [],
    candidateData: [],
    selectedCandidate: null,
    createReportButtonDisabled: true,
    reportCreationLoading: false,
    formattedReportData: [],
    reportName: "",
    displayNoDataError: false
  };

  // get selected report type
  getSelectedReportType = selectedOption => {
    // ETTA or PPC
    if (
      this.props.reportRequestor === REPORT_REQUESTOR.etta ||
      this.props.reportRequestor === REPORT_REQUESTOR.ppc
    ) {
      this.props
        .getAllExistingTestOrderNumbers()
        .then(response => {
          this.setState({ testOrderNumberData: response });
        })
        .then(() => {
          let testOrderNumberOptions = [];
          for (let i = 0; i < this.state.testOrderNumberData.length; i++) {
            testOrderNumberOptions.push({
              value: this.state.testOrderNumberData[i].test_order_number,
              label: `${this.state.testOrderNumberData[i].test_order_number} (${this.state.testOrderNumberData[i].staffing_process_number})`
            });
          }
          this.setState({
            selectedReportType: selectedOption,
            selectedTestOrderNumber: null,
            selectedTest: null,
            selectedCandidate: null,
            testOrderNumberOptions: testOrderNumberOptions,
            testOrderNumberVisible: true,
            testVisible: false,
            candidateVisible: false,
            createReportButtonDisabled: true
          });
        });
      // TA
    } else {
      this.props
        .getTaAssignedTestOrderNumbers()
        .then(response => {
          this.setState({ testOrderNumberData: response });
        })
        .then(() => {
          let testOrderNumberOptions = [];
          for (let i = 0; i < this.state.testOrderNumberData.length; i++) {
            testOrderNumberOptions.push({
              value: this.state.testOrderNumberData[i].test_order_number,
              label: `${this.state.testOrderNumberData[i].test_order_number} (${this.state.testOrderNumberData[i].staffing_process_number})`
            });
          }
          this.setState({
            selectedReportType: selectedOption,
            selectedTestOrderNumber: null,
            selectedTest: null,
            selectedCandidate: null,
            testOrderNumberOptions: testOrderNumberOptions,
            testOrderNumberVisible: true,
            testVisible: false,
            candidateVisible: false,
            createReportButtonDisabled: true
          });
        });
    }
  };

  // get selected test order number
  getSelectedTestOrderNumber = selectedOption => {
    this.props
      .getTestsBasedOnTestOrderNumber(selectedOption.value)
      .then(response => {
        this.setState({ testData: response });
      })
      .then(() => {
        let testOptions = [];
        for (let i = 0; i < this.state.testData.length; i++) {
          testOptions.push({
            value: this.state.testData[i].test_id,
            label: this.state.testData[i][`${this.props.currentLanguage}_test_name`]
          });
        }
        this.setState(
          {
            selectedTestOrderNumber: selectedOption,
            selectedTest: null,
            selectedCandidate: null,
            testOptions: testOptions,
            testVisible: true,
            candidateVisible: false
          },
          () => {
            this.createReportButtonDisableStateValidation();
          }
        );
      });
  };

  // get selected test
  getSelectedTest = selectedOption => {
    let isMulti =
      this.state.selectedReportType.value === REPORT_TYPES.individualScoreSheet.value
        ? false
        : true;

    // is multi (report type is 2 (Results Report))
    if (isMulti) {
      // selectedOption will be null if the user remove the last selected option
      if (selectedOption != null) {
        // length will be 0 if the user hit "clear all" options
        if (selectedOption.length > 0) {
          for (let i = 0; i < selectedOption.length; i++) {
            this.handleSelectTest(true, selectedOption, i);
          }
        } else {
          this.statesUpdateOnClearAllTestOptions();
        }
      } else {
        this.statesUpdateOnClearAllTestOptions();
      }
      // is not multi (single test option selection)
    } else {
      this.handleSelectTest(false, selectedOption);
    }
  };

  statesUpdateOnClearAllTestOptions = () => {
    this.setState({
      selectedTest: null,
      selectedCandidate: null,
      candidateData: [],
      candidateVisible: false,
      createReportButtonDisabled: true
    });
  };

  handleSelectTest = (isMulti, selectedOption, i) => {
    // only if multi is true, consider parameter "i"
    const ta_id = this.props.reportRequestor === REPORT_REQUESTOR.ta ? this.props.username : "";
    this.props
      .getResultsReportData(
        this.state.selectedTestOrderNumber.value,
        isMulti ? selectedOption[i].value : selectedOption.value,
        // set to true to remove duplicates
        true,
        ta_id
      )
      .then(response => {
        this.setState({ candidateData: response });
      })
      .then(() => {
        // populating candidate options
        let candidateOptions = [];
        for (let i = 0; i < this.state.candidateData.length; i++) {
          candidateOptions.push({
            value: this.state.candidateData[i].username,
            label: `${this.state.candidateData[i].candidate_last_name}, ${this.state.candidateData[i].candidate_first_name} (${this.state.candidateData[i].username})`
          });
        }
        this.setState(
          {
            selectedTest: selectedOption,
            selectedCandidate: null,
            candidateOptions: candidateOptions,
            // candidate dropdown visible only if the report type is 1 (individual Score Sheet)
            candidateVisible:
              this.state.selectedReportType.value === REPORT_TYPES.individualScoreSheet.value
                ? true
                : false
            // create report button stays disabled if the report type is 1 (individual Score Sheet)
          },
          () => {
            this.createReportButtonDisableStateValidation();
          }
        );
      });
  };

  // get selected candidate
  getSelectedCandidate = selectedOption => {
    this.setState(
      {
        selectedCandidate: selectedOption
      },
      () => {
        this.createReportButtonDisableStateValidation();
      }
    );
  };

  // generating report extract
  generateReport = async () => {
    // getting required parameters from states
    const testOrderNumber = this.state.selectedTestOrderNumber.value;
    const test = this.state.selectedTest;
    const ta_id = "";
    const candidate = this.state.selectedCandidate;
    this.setState({ reportCreationLoading: true }, () => {
      console.log(this.state.selectedReportType.value);
      switch (this.state.selectedReportType.value) {
        case REPORT_TYPES.individualScoreSheet.value:
          // third parameter is set to false, so duplicates are not removed
          this.props
            .getResultsReportData(testOrderNumber, test.value, false, ta_id, candidate.value)
            .then(response => {
              this.handleReportDataFormatting(response);
            });
          break;
        case REPORT_TYPES.resultsReport.value:
          let testsString = "";
          // looping in selected tests
          for (let i = 0; i < test.length; i++) {
            // first test, don't add comma in front of the test_id
            if (i === 0) {
              testsString += `${test[i].value}`;
              // all other tests, add comma in front of the test_id
            } else {
              testsString += `,${test[i].value}`;
            }
          }
          // third parameter is set to false, so duplicates are not removed
          this.props.getResultsReportData(testOrderNumber, testsString, false).then(response => {
            this.handleReportDataFormatting(response);
          });
          break;
        case REPORT_TYPES.financialReport.value:
          this.props.getFinancialReportData(testOrderNumber).then(response => {
            this.handleReportDataFormatting(response);
          });
          break;
        default:
          break;
      }
    });
    // }
  };

  handleReportDataFormatting = reportData => {
    // if there is at least one entry based on the provided parameters
    if (reportData.length > 0) {
      // initializing variables
      let reportName = "";
      let formattedReportData = [];
      // formatting report data and creating report name
      switch (this.state.selectedReportType.value) {
        // inidividual score sheet report type
        case REPORT_TYPES.individualScoreSheet.value:
          // generating report data
          formattedReportData = generateIndividualScoreSheetReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.testAdministration.reports.reportTypes.individualScoreSheet} - ${reportData[0].candidate_last_name} ${reportData[0].candidate_first_name}.csv`;
          break;
        // results report type
        case REPORT_TYPES.resultsReport.value:
          // generating report data
          formattedReportData = generateResultsReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.testAdministration.reports.reportTypes.resultsReport} - ${reportData[0].test_order_number} (${reportData[0].staffing_process_number}).csv`;
          break;
        // financial report type
        case REPORT_TYPES.financialReport.value:
          // generating report data
          formattedReportData = generateFinancialReport(reportData);
          // generating report name
          reportName = `${LOCALIZE.testAdministration.reports.reportTypes.financialReport} - ${reportData[0].test_order_number} (${reportData[0].staffing_process_number}).csv`;
          break;
        // should never happen
        default:
          break;
      }

      this.setState({ formattedReportData: formattedReportData, reportName: reportName }, () => {
        this.setState({ reportCreationLoading: false }, () => {
          // once all data has been created, simulate click on hidden create report link to generate the downloadable csv file
          this.createReportLinkButtonRef.current.link.click();
          // clearing all fields
          this.resetAllFields();
        });
      });
      // there is no data to show based on the provided parameters
    } else {
      // display no data popup
      this.setState({ displayNoDataError: true });
    }
  };

  // analysing all the selected data in order to enable the create report button when all needed parameters have been provided
  createReportButtonDisableStateValidation = () => {
    // initializing disabledState
    let disabledState = true;
    switch (this.state.selectedReportType.value) {
      // Individual Score Sheet
      case REPORT_TYPES.individualScoreSheet.value:
        if (
          this.state.selectedTestOrderNumber !== null &&
          this.state.selectedTest !== null &&
          this.state.selectedCandidate !== null
        ) {
          disabledState = false;
        }
        break;
      // Results Report
      case REPORT_TYPES.resultsReport.value:
        if (this.state.selectedTestOrderNumber !== null && this.state.selectedTest !== null) {
          disabledState = false;
        }
        break;
      // Financial Report
      case REPORT_TYPES.financialReport.value:
        if (this.state.selectedTestOrderNumber !== null) {
          disabledState = false;
        }
        break;
      default:
        disabledState = true;
        break;
    }
    this.setState({ createReportButtonDisabled: disabledState });
  };

  resetAllFields = () => {
    this.setState({
      selectedReportType: null,
      testOrderNumberVisible: false,
      testOrderNumberOptions: [],
      testOrderNumberData: [],
      selectedTestOrderNumber: null,
      testVisible: false,
      testOptions: [],
      testData: [],
      selectedTest: null,
      candidateVisible: false,
      candidateOptions: [],
      candidateData: [],
      selectedCandidate: null,
      createReportButtonDisabled: true,
      reportCreationLoading: false,
      formattedReportData: [],
      reportName: "",
      displayNoDataError: false
    });
  };

  render() {
    const {
      selectedReportType,
      testOrderNumberOptions,
      testOrderNumberVisible,
      selectedTestOrderNumber,
      testOptions,
      testVisible,
      selectedTest,
      candidateVisible,
      candidateOptions,
      selectedCandidate,
      createReportButtonDisabled,
      reportCreationLoading,
      formattedReportData,
      reportName,
      displayNoDataError
    } = this.state;
    return (
      <div>
        {!reportCreationLoading && (
          <div>
            <div style={styles.dropdownsMainContainer}>
              <div style={styles.optionSelectionContainer}>
                <div style={styles.labelContainer}>
                  <label id="report-type-label">{LOCALIZE.reports.reportTypeLabel}</label>
                </div>
                <div style={styles.singleDropdownContainer}>
                  <Select
                    id="report-type-dropdown"
                    name="report-type-options"
                    aria-labelledby="report-type-label report-type-dropdown"
                    placeholder=""
                    options={this.props.reportTypeOptions}
                    onChange={this.getSelectedReportType}
                    clearable={false}
                    value={selectedReportType}
                  ></Select>
                </div>
              </div>
              {testOrderNumberVisible && (
                <div style={styles.optionSelectionContainer}>
                  <div style={styles.labelContainer}>
                    <label id="test-order-number-label">
                      {LOCALIZE.reports.testOrderNumberLabel}
                    </label>
                  </div>
                  <div style={styles.singleDropdownContainer}>
                    <Select
                      id="test-order-number-dropdown"
                      name="test-order-number-options"
                      aria-labelledby="test-order-number-label test-order-number-dropdown"
                      placeholder=""
                      options={testOrderNumberOptions}
                      onChange={this.getSelectedTestOrderNumber}
                      clearable={false}
                      value={selectedTestOrderNumber}
                    ></Select>
                  </div>
                </div>
              )}
              {testVisible && selectedReportType.value !== REPORT_TYPES.financialReport.value && (
                <div style={styles.optionSelectionContainer}>
                  <div style={styles.labelContainer}>
                    <label id="test-label">{LOCALIZE.reports.testLabel}</label>
                  </div>
                  <div style={styles.singleDropdownContainer}>
                    <Select
                      id="test-dropdown"
                      name="test-options"
                      aria-labelledby="test-label test-dropdown"
                      placeholder=""
                      // is multi select only if report type is not 1 (individual Score Sheet)
                      isMulti={
                        this.state.selectedReportType.value ===
                        REPORT_TYPES.individualScoreSheet.value
                          ? false
                          : true
                      }
                      options={testOptions}
                      onChange={this.getSelectedTest}
                      clearable={false}
                      value={selectedTest}
                    ></Select>
                  </div>
                </div>
              )}
              {candidateVisible && (
                <div style={styles.optionSelectionContainer}>
                  <div style={styles.labelContainer}>
                    <label id="candidate-label">{LOCALIZE.reports.candidateLabel}</label>
                  </div>
                  <div style={styles.singleDropdownContainer}>
                    <Select
                      id="candidate-dropdown"
                      name="candidate-options"
                      aria-labelledby="candidate-label candidate-dropdown"
                      placeholder=""
                      options={candidateOptions}
                      onChange={this.getSelectedCandidate}
                      clearable={false}
                      value={selectedCandidate}
                    ></Select>
                  </div>
                </div>
              )}
            </div>
            <div style={styles.generateButtonContainer}>
              <CustomButton
                label={
                  <>
                    <span>
                      <FontAwesomeIcon icon={faPoll} style={styles.generateButtonIcon} />
                    </span>
                    <span>{LOCALIZE.reports.generateButton}</span>
                  </>
                }
                action={this.generateReport}
                customStyle={styles.generateButtonLabel}
                buttonTheme={THEME.PRIMARY}
                disabled={createReportButtonDisabled}
              />
            </div>
          </div>
        )}
        {reportCreationLoading && (
          <div style={styles.loading}>
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </label>
          </div>
        )}
        <CSVLink
          ref={this.createReportLinkButtonRef}
          asyncOnClick={true}
          data={formattedReportData}
          filename={reportName}
        ></CSVLink>
        <PopupBox
          show={displayNoDataError}
          title={LOCALIZE.reports.noDataPopup.title}
          handleClose={() => {}}
          description={
            <div>
              <p>{LOCALIZE.reports.noDataPopup.description}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={() => this.resetAllFields()}
        />
      </div>
    );
  }
}

export { ReportGenerator as unconnectedReportGenerator };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTaAssignedTestOrderNumbers,
      getAllExistingTestOrderNumbers,
      getTestsBasedOnTestOrderNumber,
      getResultsReportData,
      getFinancialReportData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ReportGenerator));
