export function generateFinancialReport(providedReportDataArray) {
  // initializing reportData
  let reportData = [];

  // pushing columns definition
  reportData.push([
    "Test Order Number",
    "Selection Process Number",
    "Department/Ministry Code",
    "is_org",
    "is_ref",
    "Billing Contact",
    "Billing Contact Info",
    "Test ID",
    "Test Description (FR)",
    "Test Description (EN)",
    "Number of Tests"
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].test_order_number,
      providedReportDataArray[i].staffing_process_number,
      providedReportDataArray[i].department_ministry_code,
      providedReportDataArray[i].is_org,
      providedReportDataArray[i].is_ref,
      providedReportDataArray[i].billing_contact,
      providedReportDataArray[i].billing_contact_info,
      providedReportDataArray[i].test_id,
      providedReportDataArray[i].test_description_fr,
      providedReportDataArray[i].test_description_en,
      providedReportDataArray[i].number_of_tests
    ]);
  }

  return reportData;
}
