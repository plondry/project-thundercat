export function generateIndividualScoreSheetReport(providedReportDataArray) {
  // initializing reportData
  let reportData = [];

  // pushing columns definition
  reportData.push([
    "No. de personne / Person ID",
    "Nom d'utilisateur / Username",
    "Nom / Name",
    "CIDP / PRI",
    "# de commande / Order #",
    "No. de processus de sélection / Assessment Process #",
    "Examen / Test",
    "Description de l'examen (FR) / Test Description (FR)",
    "Description de l'examen (EN) / Test Description (EN)",
    "Langue / Language",
    "Date d'examen / Test Date",
    "Résultat / Score"
  ]);

  // looping in provided providedReportDataArray and formatting data
  for (let i = 0; i < providedReportDataArray.length; i++) {
    reportData.push([
      providedReportDataArray[i].candidate_id,
      providedReportDataArray[i].username,
      `${providedReportDataArray[i].candidate_last_name}, ${providedReportDataArray[i].candidate_first_name}`,
      providedReportDataArray[i].candidate_pri_or_military_nbr,
      providedReportDataArray[i].test_order_number,
      providedReportDataArray[i].staffing_process_number,
      providedReportDataArray[i].test.test_code,
      providedReportDataArray[i].test.fr_name,
      providedReportDataArray[i].test.en_name,
      providedReportDataArray[i].test.test_language,
      // only getting short date without timestamp
      providedReportDataArray[i].start_date === null
        ? null
        : providedReportDataArray[i].start_date.split("T")[0],
      providedReportDataArray[i].total_score
    ]);
  }

  return reportData;
}
