import LOCALIZE from "../../../text_resources";

export const reportTypesDefinition = () => {
  return [
    { value: 1, label: LOCALIZE.testAdministration.reports.reportTypes.individualScoreSheet },
    { value: 2, label: LOCALIZE.testAdministration.reports.reportTypes.resultsReport },
    { value: 3, label: LOCALIZE.testAdministration.reports.reportTypes.financialReport }
  ];
};

export const REPORT_TYPES = {
  individualScoreSheet: reportTypesDefinition()[0],
  resultsReport: reportTypesDefinition()[1],
  financialReport: reportTypesDefinition()[2]
};
