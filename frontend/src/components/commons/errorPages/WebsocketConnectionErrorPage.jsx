import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { resetTestStatusState } from "../../../modules/TestStatusRedux";
import { resetNotepadState } from "../../../modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "../../../modules/SampleTestStatusRedux";
import { logoutAction } from "../../../modules/LoginRedux";
import { resetErrorStatusState } from "../../../modules/ErrorStatusRedux";
import { PATH } from "../../commons/Constants";

const styles = {
  buttonsDiv: {
    textAlign: "center",
    marginTop: 100
  },
  button: {
    minWidth: 175
  }
};

class WebsocketConnectionErrorPage extends Component {
  static props = {
    // Props from Redux
    resetTestStatusState: PropTypes.func,
    resetSampleTestStatusState: PropTypes.func,
    resetNotepadState: PropTypes.func,
    logoutAction: PropTypes.func
  };

  handleLogout = () => {
    // resetting needed redux states
    this.props.resetTestStatusState();
    this.props.resetSampleTestStatusState();
    this.props.resetNotepadState();
    this.props.resetErrorStatusState();
    this.props.logoutAction();
    // redirecting to login page
    window.location.href = PATH.login;
  };

  render() {
    return (
      <div>
        <div>
          <p>{LOCALIZE.websocketConnectionErrorPage.description1}</p>
        </div>
        <div style={styles.buttonsDiv}>
          <button className="btn btn-primary" style={styles.button} onClick={this.handleLogout}>
            {LOCALIZE.menu.logout}
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetTestStatusState,
      resetNotepadState,
      resetSampleTestStatusState,
      logoutAction,
      resetErrorStatusState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(WebsocketConnectionErrorPage);
