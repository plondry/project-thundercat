import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../ContentContainer";

class GenericErrorPage extends Component {
  static propTypes = {
    tabTitle: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    titleCustomStyle: PropTypes.object,
    description: PropTypes.object.isRequired
  };

  componentDidMount = () => {
    document.getElementById("page-content").focus();
  };

  render() {
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{this.props.tabTitle}</title>
        </Helmet>
        <ContentContainer>
          <div id="page-content" tabIndex={0}>
            <h1 className="green-divider" style={this.props.titleCustomStyle}>
              {this.props.title}
            </h1>
            <div>{this.props.description}</div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { GenericErrorPage as unconnectedGenericErrorPage };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(GenericErrorPage);
