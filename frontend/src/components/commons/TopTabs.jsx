import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { switchTopTab } from "../../modules/NavTabsRedux";

const styles = {
  container: {
    maxWidth: 1400,
    minWidth: 600,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto"
  },
  tabsContainerWidth: {
    // preventing the notepad to go under the EmibTabs container
    width: "calc(1%)"
    // paddingRight: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  margin0: { margin: 0 }
};

class TopTabs extends Component {
  state = {
    // to enable or disable hidden message related to the only tab available as far as the test is not fully started (for screen reader users only)
    currentTab: undefined,
    oneTabOnlyHiddenMsgEnabled: false
  };

  componentDidUpdate = prevProps => {
    // if defaultTab gets updated
    if (prevProps.defaultTab !== this.props.defaultTab) {
      this.switchTab(this.props.defaultTab);
    }
  };

  // enabling the "single tab available" message for screen reader users on enter eMIB action, meaning before the test fully starts
  componentDidMount = () => {
    this.switchTab(this.props.currentTab >= 1 ? this.props.currentTab : this.props.defaultTab);
  };

  // if the component is exported unconnected, switchTopTab is undefined
  // unconnected is used in testbuilder
  switchTab = tab => {
    if (this.props.switchTopTab) {
      this.props.switchTopTab(tab);
    } else {
      this.setState({ currentTab: tab });
    }
    if (this.props.notifySwitchTab) {
      this.props.notifySwitchTab(tab);
    }
  };

  // check if the current tab is valid for the TABS provided
  // might be false because of reusable component
  getCurrentTab = () => {
    const { currentTab, TABS } = this.props;
    for (let tab of TABS) {
      if (currentTab === tab.key) {
        return currentTab;
      }
    }
    return this.state.currentTab || this.props.defaultTab;
  };

  render() {
    // let TABS = this.getTabs();
    let { TABS } = this.props;
    let currentTab = this.getCurrentTab();
    return (
      <div>
        <Helmet>
          <title>{LOCALIZE.titles.CAT}</title>
        </Helmet>
        <Row style={styles.margin0}>
          <Col
            role="region"
            aria-label={LOCALIZE.ariaLabel.topNavigationSection}
            style={styles.tabsContainerWidth}
          >
            <div id="tab-container" tabIndex={-1}>
              {this.state.oneTabOnlyHiddenMsgEnabled && (
                <span id="only-one-tab-available-for-now-msg" style={styles.hiddenText}>
                  {LOCALIZE.emibTest.howToPage.testInstructions.onlyOneTabAvailableForNowMsg}
                </span>
              )}
              <Tabs
                defaultActiveKey={currentTab}
                id="top-tabs"
                activeKey={currentTab}
                onSelect={key => this.switchTab(key)}
                aria-labelledby="only-one-tab-available-for-now-msg"
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab
                      key={index}
                      eventKey={tab.key}
                      title={tab.tabName}
                      style={{ border: "1px", borderColor: "#00565e" }}
                    >
                      <div style={{ border: "1px", borderColor: "#00565e" }}>{tab.body}</div>
                    </Tab>
                  );
                })}
              </Tabs>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export { TopTabs as UnconnectedTopTabs };
const mapStateToProps = (state, ownProps) => {
  return {
    currentTab: state.navTabs.currentTopTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchTopTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TopTabs);
