import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import React, { Component } from "react";
import { updateQuestionScore, saveScore } from "../../modules/ScorerRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretLeft } from "@fortawesome/free-solid-svg-icons";
import { Col, Row } from "react-bootstrap";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import TreeNode from "../commons/TreeNode";
import { processTreeContent } from "../../helpers/transformations";

const SIDEBAR_COLOR = "#004db8";
const BORDER_COLOR = "#CECECE";
const styles = {
  inactiveCaret: {
    padding: "3px 24px 3px 3px"
  },
  left0: {
    left: 0
  },
  label: {
    textAlign: "left",
    paddingLeft: 5,
    float: "right",
    fontSize: "16px",
    fontWeight: "bold",
    color: "#FFFFFF",
    backgroundColor: SIDEBAR_COLOR,
    cursor: "pointer"
  },
  rationale: {
    padding: 10,
    resize: "none",
    minHeight: 200,
    width: "100%",
    marginRight: "15px",
    borderWidth: "2px",
    borderStyle: "solid",
    borderColor: BORDER_COLOR
  },
  noPadding: {
    padding: "0 0 0 0",
    margin: "0 0 0 0"
  },
  gridPointsContainer: {
    paddingLeft: 10,
    position: "relative",
    width: 60,
    height: 24
  },
  radioButtons: {
    display: "block"
  },
  caret: {
    marginRight: 5,
    color: SIDEBAR_COLOR
  },
  rowStyle: {
    margin: 0
  },
  rowStylePaddingTop5: {
    margin: 0,
    paddingTop: 5
  },
  rowStyleSaveButton: {
    width: "90%",
    margin: "auto",
    paddingTop: 5
  },
  exampleRationale: {
    width: "90%",
    border: "2px solid #00565e",
    borderRadius: 5,
    padding: 10,
    marginLeft: "-10px",
    marginTop: 0,
    zIndex: 0,
    overflow: "auto"
  },
  exampleRationaleCol: {
    marginLeft: 0,
    paddingLeft: 7
  },
  saveButton: {
    float: "right",
    minWidth: "100px"
  },
  margin3: { margin: 3 },
  radioButtonDiv: { position: "relative", top: 10 },
  noPaddingRight: { paddingRight: 0 }
};
class CompetencyTab extends Component {
  static props = {
    currentLanguage: PropTypes.string,
    currentQuestion: PropTypes.object
  };

  state = {
    selectedRating: null,
    showCompetencyHelper: false,
    showBAR: false,
    currentScore: {
      question: this.props.currentQuestion.id
    }
  };

  saveAnswer = () => {
    let obj = {
      scorer_test: this.props.assignedTestId,
      ...{ ...this.props.score }
    };
    this.props.saveScore(obj);
    this.props.updateQuestionScore({
      question: this.state.currentScore.question,
      competency: this.state.currentScore.competency,
      saved: true
    });
  };

  handleRationaleChange = (event, competency) => {
    let newObj = {
      rationale: event.target.value,
      question: this.props.currentQuestion.id,
      competency: competency,
      saved: false
    };
    this.props.updateQuestionScore(newObj);
    this.setState({
      currentScore: { ...this.state.currentScore, rationale: event.target.value }
    });
  };

  handleScoreChange = (event, competency) => {
    let newObj = {
      score: event.target.name,
      question: this.props.currentQuestion.id,
      competency: competency,
      saved: false
    };
    this.props.updateQuestionScore(newObj);
    this.setState({
      currentScore: { ...this.state.currentScore, score: event.target.name }
    });
  };

  clickRatingTab = (rating, key, competency) => {
    this.setState({
      selectedRating: { key, ...rating, question: this.props.currentQuestion.id },
      showCompetencyHelper: true,
      selectedCompetency: competency.id
    });
  };

  render() {
    const { ratings, SCALE_HEIGHT, MAX_POINTS, competency, score } = this.props;
    let ratingLocations = [];
    let radioButtonHeight = SCALE_HEIGHT / MAX_POINTS;
    let scale = [...Array(MAX_POINTS).keys()].map(x => MAX_POINTS - x - 1);

    // format the ratings so they work with TreeNode
    let treeNodes = ratings
      .sort((a, b) => (a.score < b.score ? 1 : -1))
      .map((obj, index) => {
        return {
          ...obj,
          id: index,
          text: {
            label: String(obj.score)
          },
          team_information_tree_child: [
            {
              id: ratings.length + index,
              parent: index,
              team_information_tree_child: null,
              text: {
                label: obj.details.example
              }
            }
          ]
        };
      });
    treeNodes = processTreeContent(treeNodes, "team_information_tree_child");

    //prop styles
    const renderStyles = {
      exampleRationaleColHeight: {
        ...styles.exampleRationale,
        height: SCALE_HEIGHT - radioButtonHeight / 2
      },
      radioButtonHeight: {
        height: radioButtonHeight
      },
      gradientBar: {
        height: SCALE_HEIGHT - radioButtonHeight / 2,
        width: 8,
        position: "absolute",
        top: radioButtonHeight / 4
      },
      gridPointsContainer: {
        margin: 0,
        height: SCALE_HEIGHT,
        position: "relative",
        right: 11,
        width: "5%"
      }
    };

    return (
      <>
        <Row style={styles.rowStylePaddingTop5}>
          <Col>
            <h4 style={styles.margin3}>{competency[`${this.props.currentLanguage}_name`]}</h4>
          </Col>
          <Col>
            <button
              id="unit-test-view-bar-btn"
              type="button"
              className="btn btn-secondary"
              onClick={() => this.setState({ showBAR: true })}
              style={styles.saveButton}
            >
              {LOCALIZE.scorer.competency.bar.barButton}
            </button>
          </Col>
        </Row>
        <Row style={styles.rowStyle}>
          <Col sm="0.5">
            <div style={styles.radioButtons}>
              {scale.map(val => {
                return (
                  <div style={renderStyles.radioButtonHeight} key={val}>
                    <div style={styles.radioButtonDiv}>
                      <input
                        id={`${val}-${competency.id}-id`}
                        type="radio"
                        name={val}
                        onChange={e => this.handleScoreChange(e, competency.id)}
                        checked={String(score.score) === String(val)}
                      />
                      <label htmlFor={`${val}-${competency.id}-id`}>{val}</label>
                    </div>
                  </div>
                );
              })}
            </div>
          </Col>
          <Col sm="1" style={styles.noPaddingRight}>
            <div className="gradient-bar" style={renderStyles.gradientBar}></div>
          </Col>
          <Col sm="0.5" style={renderStyles.gridPointsContainer}>
            <div style={styles.gridPointsContainer}>
              {ratings.map((rating, key) => {
                let padding =
                  SCALE_HEIGHT - (radioButtonHeight * rating.score + radioButtonHeight) + 8;
                let paddingLength = { 1: 22, 2: 16, 3: 9, 4: 0 }[String(rating.score).length];

                // used to keep scales from overlapping
                for (let location of ratingLocations) {
                  let proximity = Math.abs(padding - location);
                  if (proximity < 12) {
                    padding -= Math.abs(24 - proximity);
                  }
                }
                ratingLocations.push(padding);

                // styles
                const ratingNumber = {
                  position: "absolute",
                  top: padding,
                  left: -3,
                  height: 24,
                  zIndex: 5,
                  cursor: "pointer"
                };
                const activeRatingNumber = {
                  padding: `0px ${paddingLength}px 0px 3px`,
                  border: "2px solid #00565e",
                  borderRadius: "5px 0px 0px 5px",
                  whiteSpace: "nowrap",
                  borderRight: 0,
                  backgroundColor: "white",
                  zIndex: 5000,
                  position: "relative",
                  right: 5
                };

                let selected = false;
                if (this.state.selectedRating) {
                  selected =
                    rating.score === this.state.selectedRating.score &&
                    this.props.currentQuestion.id === this.state.selectedRating.question;
                }
                return (
                  <div
                    key={key}
                    style={selected ? { ...ratingNumber, ...styles.left0 } : ratingNumber}
                    onClick={() => this.clickRatingTab(rating, key, competency)}
                  >
                    <div style={selected ? activeRatingNumber : styles.inactiveCaret}>
                      <FontAwesomeIcon style={styles.caret} icon={faCaretLeft} />
                      {rating.score}
                    </div>
                  </div>
                );
              })}
            </div>
          </Col>
          {this.state.selectedRating &&
            this.props.currentQuestion.id === this.state.selectedRating.question &&
            this.state.selectedCompetency === competency.id && (
              <Col style={styles.exampleRationaleCol}>
                <div style={renderStyles.exampleRationaleColHeight}>
                  {this.state.selectedRating.details.example}
                </div>
              </Col>
            )}
        </Row>
        <Row style={styles.rowStyle}>
          <label>{LOCALIZE.scorer.sidebar.rational}</label>
          <textarea
            id={`${score.question}-${score.competency}-rationale`}
            aria-placeholder={LOCALIZE.scorer.sidebar.placeholder}
            maxLength="10000"
            className="text-area"
            style={styles.rationale}
            placeholder={LOCALIZE.scorer.sidebar.placeholder}
            value={score.rationale}
            onChange={e => this.handleRationaleChange(e, competency.id)}
          />
        </Row>
        <Row style={styles.rowStyleSaveButton}>
          <Col>
            <button
              id="unit-test-submit-btn"
              type="button"
              className="btn btn-success"
              onClick={this.saveAnswer}
              style={styles.saveButton}
            >
              {LOCALIZE.commons.saveButton}
            </button>
          </Col>
        </Row>
        <PopupBox
          show={this.state.showBAR}
          handleClose={() => {
            this.setState({ showBAR: !this.state.showBAR });
          }}
          title={LOCALIZE.scorer.competency.bar.title}
          description={
            <>
              <p>{LOCALIZE.scorer.competency.bar.description}</p>
              <b>{competency[`${this.props.currentLanguage}_name`]}</b>
              <TreeNode nodes={treeNodes} />
            </>
          }
          rightButtonType={BUTTON_TYPE.secondary}
          rightButtonTitle={LOCALIZE.commons.close}
          rightButtonAction={this.props.handleNext}
        />
      </>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    assignedTestId: state.scorer.assignedTestId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateQuestionScore,
      saveScore
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CompetencyTab);
