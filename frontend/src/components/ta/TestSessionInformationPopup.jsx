import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { styles as ActivePermissionsStyles } from "../etta/permissions/ActivePermissions";
import { styles as AssignTestAccessesStyle } from "../etta/test_accesses/AssignTestAccesses";
import { getTestPermissionFinancialData } from "../../modules/PermissionsRedux";
import Select from "react-select";
import {
  updateTestOrderNumberState,
  updateTestToAdministerState,
  updateTestToAdministerDropdownDisabledState,
  updateTestSessionLanguageState,
  updateTestSessionLanguageDropdownDisabledState,
  updateGenerateButtonDisabledState,
  updatePopupOverflowVisibleState,
  resetTestAdministrationStates
} from "../../modules/TestAdministrationRedux";
import { BUTTON_STATE } from "../commons/PopupBox";

const styles = {
  mainPopupContainer: {
    padding: "10px 15px"
  },
  billingInfoContainer: {
    width: "90%",
    margin: "8px auto",
    border: "1px solid #CECECE",
    backgroundColor: "#F3F3F3",
    padding: 6
  },
  noPadding: {
    padding: 0
  },
  boldText: {
    fontWeight: "bold"
  }
};

class TestSessionInformationPopup extends Component {
  static propTypes = {
    testOrderNumberOptions: PropTypes.array.isRequired,
    populateTestToAdministerOptions: PropTypes.func.isRequired,
    testToAdministerOptions: PropTypes.array.isRequired,
    populateTestSessionLanguageOptions: PropTypes.func.isRequired,
    testSessionLanguageOptions: PropTypes.array.isRequired,
    // provided by redux
    getTestPermissionFinancialData: PropTypes.func,
    updateTestOrderNumberState: PropTypes.func,
    updateTestToAdministerState: PropTypes.func,
    updateTestToAdministerDropdownDisabledState: PropTypes.func,
    updateTestSessionLanguageState: PropTypes.func,
    updateTestSessionLanguageDropdownDisabledState: PropTypes.func,
    updateGenerateButtonDisabledState: PropTypes.func,
    updatePopupOverflowVisibleState: PropTypes.func,
    resetTestAdministrationStates: PropTypes.func
  };

  state = {
    financialDataVisible: false,
    financialData: {}
  };

  componentDidMount = () => {
    // resetting redux states
    this.props.resetTestAdministrationStates();
  };

  // get selected test order number option + setting needed states
  onSelectedTestOrderNumberOptionChange = selectedOption => {
    // if selectedOption is defined
    if (typeof selectedOption !== "undefined") {
      // updating redux states
      this.props.updateTestOrderNumberState(selectedOption);
      this.props.updateTestToAdministerDropdownDisabledState(false);
      this.props.updateTestToAdministerState({ label: "", value: "" });
      this.props.updateTestSessionLanguageDropdownDisabledState(true);
      this.props.updateTestSessionLanguageState({ label: "", value: "" });
      this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);

      // populating test to administer options
      this.props.populateTestToAdministerOptions(selectedOption);

      // resetting financial data state
      this.setState({ financialData: {} });
    }
  };

  // get selected test to administer option + setting needed states
  onSelectedTestToAdministerOptionChange = selectedOption => {
    // if selectedOption is defined
    if (typeof selectedOption !== "undefined") {
      // updating redux states
      this.props.updateTestToAdministerState(selectedOption);
      this.props.updateTestSessionLanguageDropdownDisabledState(false);
      this.props.updateTestSessionLanguageState({ label: "", value: "" });
      this.props.updateGenerateButtonDisabledState(BUTTON_STATE.disabled);

      // populating test session language options
      this.props.populateTestSessionLanguageOptions();

      // resetting financial data state
      this.setState({ financialData: {} });
    }
  };

  // get selected test session language option + setting needed states + getting and setting financial data + enabling generate button
  onSelectedTestSessionLanguageOptionChange = selectedOption => {
    // if selectedOption is defined
    if (typeof selectedOption !== "undefined") {
      this.props.updateTestSessionLanguageState(selectedOption);
      // getting test permission's financial data
      this.props
        .getTestPermissionFinancialData(
          this.props.testOrderNumber.value,
          this.props.testToAdminister.value.id
        )
        .then(response => {
          // getting all needed financial data
          const staffingProcessNumber = response[0].staffing_process_number;
          const departmentMinistry = response[0].department_ministry_code;
          const contact = `${response[0].billing_contact} (${response[0].billing_contact_info})`;
          const isOrg = response[0].is_org;
          const isRef = response[0].is_ref;
          // saving data in state
          this.setState({
            financialData: {
              staffingProcessNumber: staffingProcessNumber,
              departmentMinistry: departmentMinistry,
              contact: contact,
              isOrg: isOrg,
              isRef: isRef
            },
            financialDataVisible: true
          });
          // enabling generate button
          this.props.updateGenerateButtonDisabledState(BUTTON_STATE.enabled);
        });
    }
  };

  render() {
    return (
      <div>
        <div style={styles.mainPopupContainer}>
          <p>
            {LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.description}
          </p>
          <div style={AssignTestAccessesStyle.container}>
            <div>
              <div style={AssignTestAccessesStyle.labelContainer}>
                <label id="test-order-number-label" style={AssignTestAccessesStyle.label}>
                  {
                    LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                      .testOrderNumber
                  }
                </label>
              </div>
              <div style={AssignTestAccessesStyle.dropdown}>
                <Select
                  id="test-order-number-dropdown"
                  name="test-order-number-options"
                  aria-labelledby="test-order-number-label test-order-number-current-value-accessibility"
                  placeholder=""
                  options={this.props.testOrderNumberOptions}
                  onChange={this.onSelectedTestOrderNumberOptionChange}
                  clearable={false}
                  value={this.props.testOrderNumber}
                ></Select>
                <label
                  id="test-order-number-current-value-accessibility"
                  style={ActivePermissionsStyles.hiddenText}
                >{`${
                  LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                    .testOrderNumberCurrentValueAccessibility
                } ${
                  this.props.testOrderNumber.value !== ""
                    ? this.props.testOrderNumber.value
                    : LOCALIZE.commons.none
                }`}</label>
              </div>
            </div>
            <div style={AssignTestAccessesStyle.fieldSeparator}>
              <div style={AssignTestAccessesStyle.labelContainer}>
                <label id="test-to-administer-label" style={AssignTestAccessesStyle.label}>
                  {
                    LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                      .testToAdminister
                  }
                </label>
              </div>
              <div style={AssignTestAccessesStyle.dropdown}>
                <Select
                  id="test-to-administer-dropdown"
                  name="test-to-administer-options"
                  aria-labelledby="test-to-administer-label test-to-administer-current-value-accessibility"
                  placeholder=""
                  options={this.props.testToAdministerOptions}
                  onChange={this.onSelectedTestToAdministerOptionChange}
                  clearable={false}
                  value={this.props.testToAdminister}
                  isDisabled={this.props.testToAdministerDropdownDisabled}
                ></Select>
                <label
                  id="test-to-administer-current-value-accessibility"
                  style={ActivePermissionsStyles.hiddenText}
                >{`${
                  LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                    .testToAdministerCurrentValueAccessibility
                } ${
                  this.props.testToAdminister.value !== ""
                    ? this.props.testToAdminister.value
                    : LOCALIZE.commons.none
                }`}</label>
              </div>
            </div>
            <div style={AssignTestAccessesStyle.fieldSeparator}>
              <div style={AssignTestAccessesStyle.labelContainer}>
                <label id="test-session-language-label" style={AssignTestAccessesStyle.label}>
                  {
                    LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                      .testSessionLanguage
                  }
                </label>
              </div>
              <div style={AssignTestAccessesStyle.dropdown}>
                <Select
                  id="test-session-language-dropdown"
                  name="test-session-language-options"
                  aria-labelledby="test-session-language-label test-session-language-current-value-accessibility"
                  placeholder=""
                  options={this.props.testSessionLanguageOptions}
                  onChange={this.onSelectedTestSessionLanguageOptionChange}
                  clearable={false}
                  value={this.props.testSessionLanguage}
                  isDisabled={this.props.testSessionLanguageDropdownDisabled}
                ></Select>
                <label
                  id="test-session-language-current-value-accessibility"
                  style={ActivePermissionsStyles.hiddenText}
                >{`${
                  LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                    .testSessionLanguageCurrentValueAccessibility
                } ${
                  this.props.testSessionLanguage.value !== ""
                    ? this.props.testSessionLanguage.value
                    : LOCALIZE.commons.none
                }`}</label>
              </div>
            </div>
            {this.state.financialDataVisible &&
              this.props.testToAdminister.value !== "" &&
              this.props.testSessionLanguage.value !== "" && (
                <div style={AssignTestAccessesStyle.fieldSeparator}>
                  <div style={AssignTestAccessesStyle.labelContainer}>
                    <label id="billing-info-label" style={AssignTestAccessesStyle.label}>
                      {
                        LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                          .billingInformation.title
                      }
                    </label>
                  </div>
                  <div style={styles.billingInfoContainer}>
                    <p style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.staffingProcessNumber
                        }
                      </span>{" "}
                      <span>{this.state.financialData.staffingProcessNumber}</span>
                    </p>
                    <p style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.departmentMinistry
                        }
                      </span>{" "}
                      <span>{this.state.financialData.departmentMinistry}</span>
                    </p>
                    <p style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.contact
                        }
                      </span>{" "}
                      <span>{this.state.financialData.contact}</span>
                    </p>
                    <p style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.isOrg
                        }
                      </span>{" "}
                      <span>{this.state.financialData.isOrg}</span>
                    </p>
                    <p style={styles.noPadding}>
                      <span style={styles.boldText}>
                        {
                          LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup
                            .billingInformation.isRef
                        }
                      </span>{" "}
                      <span>{this.state.financialData.isRef}</span>
                    </p>
                  </div>
                </div>
              )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username,
    testOrderNumber: state.testAdministration.testOrderNumber,
    testToAdminister: state.testAdministration.testToAdminister,
    testToAdministerDropdownDisabled: state.testAdministration.testToAdministerDropdownDisabled,
    testSessionLanguage: state.testAdministration.testSessionLanguage,
    testSessionLanguageDropdownDisabled:
      state.testAdministration.testSessionLanguageDropdownDisabled
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getTestPermissionFinancialData,
      updateTestOrderNumberState,
      updateTestToAdministerState,
      updateTestToAdministerDropdownDisabledState,
      updateTestSessionLanguageState,
      updateTestSessionLanguageDropdownDisabledState,
      updateGenerateButtonDisabledState,
      updatePopupOverflowVisibleState,
      resetTestAdministrationStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestSessionInformationPopup);
