// reference: assigned_test_status.py
export const TEST_STATUS = {
  ASSIGNED: 11,
  READY: 12,
  ACTIVE: 13,
  LOCKED: 14,
  PAUSED: 15,
  NEVER_STARTED: 16,
  INACTIVITY: 17,
  TIMED_OUT: 18,
  QUIT: 19,
  SUBMITTED: 20
};
