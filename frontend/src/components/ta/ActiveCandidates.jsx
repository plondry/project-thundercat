import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { styles as TestAccessCodesStyle } from "./TestAccessCodes";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import PopupBox, { BUTTON_TYPE, BUTTON_STATE } from "../commons/PopupBox";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faClock,
  faLock,
  faLockOpen,
  faSpinner,
  faThumbsUp,
  faEdit,
  faTrashAlt,
  faSyncAlt,
  faCheck
} from "@fortawesome/free-solid-svg-icons";
import { getTimeInHoursMinutes, hoursMinutesToMinutes } from "../../helpers/timeConversion";
import SetTimer from "../commons/SetTimer";
import { resetTimerStates, updateTimerState, setDefaultTimes } from "../../modules/SetTimerRedux";
import { getAssignedTests } from "../../modules/AssignedTestsRedux";
import {
  approveCandidate,
  unAssignCandidate,
  reSyncCandidate,
  updateTestTime,
  lockCandidateTest,
  unlockCandidateTest,
  lockAllCandidatesTest,
  unlockAllCandidatesTest,
  pauseCandidateTest,
  unpauseCandidateTest
} from "../../modules/TestAdministrationRedux";
import { TEST_STATUS } from "./Constants";
import { getCurrentSocket, CHANNELS_ACTION } from "../../helpers/testSessionChannels";
import { styles as TestAdministrationStyle } from "./TestAdministration";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  customPopupStyle: {
    minWidth: 650
  },
  bold: {
    fontWeight: "bold"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  descriptionSeparator: {
    margin: "12px 0"
  },
  checkbox: {
    display: "inline-block",
    transform: "scale(1.5)",
    marginLeft: 4
  },
  checkboxDescription: {
    display: "inline",
    padding: 12
  },
  lockUnlockAllButton: {
    float: "right",
    minWidth: 150,
    margin: "0 24px 12px"
  },
  buttonIcon: {
    transform: "scale(1.5)",
    padding: "1.5px",
    marginRight: 12
  },
  testSectionTimerMainContainer: {
    margin: "12px 0"
  },
  testSectionTimerContainer: {
    display: "table",
    width: "100%",
    padding: "0 12px"
  },
  testSectionTimerDescriptionContainer: {
    display: "table-cell",
    width: "50%",
    paddingLeft: 33,
    verticalAlign: "middle"
  },
  testSectionTimerTimeContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  editTimeTitle: {
    overflowWrap: "anywhere"
  }
};

class ActiveCandidates extends Component {
  static propTypes = {
    testAdministratorAssignedCandidates: PropTypes.array.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    currentlyLoading: PropTypes.bool.isRequired,
    selectedCandidateData: PropTypes.object.isRequired,
    triggerShowEditTimePopup: PropTypes.bool.isRequired,
    triggerShowLockPausePopup: PropTypes.bool.isRequired,
    triggerShowUnlockUnpausePopup: PropTypes.bool.isRequired,
    triggerApproveCandidate: PropTypes.bool.isRequired,
    triggerUnAssignCandidate: PropTypes.bool.isRequired,
    triggerReSyncCandidate: PropTypes.bool.isRequired,
    populateTestAdministratorAssignedCandidates: PropTypes.func.isRequired,
    updateAssignedCandidatesTable: PropTypes.func.isRequired,
    sockets: PropTypes.array,
    triggerReRender: PropTypes.bool,
    // provided by redux
    updateTestTime: PropTypes.func,
    updateTimerState: PropTypes.func,
    setDefaultTimes: PropTypes.func,
    resetTimerStates: PropTypes.func,
    approveCandidate: PropTypes.func,
    reSyncCandidate: PropTypes.func,
    lockCandidateTest: PropTypes.func,
    unlockCandidateTest: PropTypes.func,
    lockAllCandidatesTest: PropTypes.func,
    unlockAllCandidatesTest: PropTypes.func,
    pauseCandidateTest: PropTypes.func,
    unpauseCandidateTest: PropTypes.func,
    getAssignedTests: PropTypes.func
  };

  state = {
    testAdministratorAssignedCandidates: [],
    rowsDefinition: {},
    selectedCandidateData: {},
    showEditTimePopup: false,
    currentHours: [],
    totalHours: 0,
    currentMinutes: [],
    totalMinutes: 0,
    timedAssignedTestSection: [],
    pauseCheckboxChecked: false,
    showLockPausePopup: false,
    showUnlockUnpausePopup: false,
    allTestsLockedFlag: false,
    atLeastOneTestToLockUnlock: false,
    showLockAllPopup: false,
    showUnlockAllPopup: false,
    showReSyncPopup: false,
    showUnAssignPopup: false
  };

  componentDidMount = () => {
    this.props.resetTimerStates();
  };

  componentDidUpdate = prevProps => {
    // if testAdministratorAssignedCandidates gets updated
    if (
      prevProps.testAdministratorAssignedCandidates !==
      this.props.testAdministratorAssignedCandidates
    ) {
      // initializing allTestsLockedFlag to true
      let allTestsLockedFlag = true;
      let atLeastOneTestToLockUnlock = false;
      // looping in testAdministratorAssignedCandidates array
      for (let i = 0; i < this.props.testAdministratorAssignedCandidates.length; i++) {
        // if these is at least one test that is ready, active or paused
        if (
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.READY ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.ACTIVE ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.PAUSED
        ) {
          // set allTestsLockedFlag to false
          allTestsLockedFlag = false;
        }
        // if there is at least one test to lock/unlock
        if (
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.READY ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.ACTIVE ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.LOCKED ||
          this.props.testAdministratorAssignedCandidates[i].status === TEST_STATUS.PAUSED
        ) {
          // set allTestsLockedFlag to false
          atLeastOneTestToLockUnlock = true;
        }
      }
      this.setState({
        testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates,
        allTestsLockedFlag: allTestsLockedFlag,
        atLeastOneTestToLockUnlock: atLeastOneTestToLockUnlock
      });
    }
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({ rowsDefinition: this.props.rowsDefinition });
    }
    // if triggerShowEditTimePopup gets updated
    if (prevProps.triggerShowEditTimePopup !== this.props.triggerShowEditTimePopup) {
      this.setState({ showEditTimePopup: true });
    }
    // if triggerShowLockPausePopup gets updated
    if (prevProps.triggerShowLockPausePopup !== this.props.triggerShowLockPausePopup) {
      this.setState({ showLockPausePopup: true });
    }
    // if triggerShowUnlockUnpausePopup gets updated
    if (prevProps.triggerShowUnlockUnpausePopup !== this.props.triggerShowUnlockUnpausePopup) {
      this.setState({ showUnlockUnpausePopup: true });
    }
    // if triggerApproveCandidate gets updated
    if (prevProps.triggerApproveCandidate !== this.props.triggerApproveCandidate) {
      this.setState(
        {
          testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates,
          rowsDefinition: this.props.rowsDefinition
        },
        () => {
          this.handleApproveCandidate();
        }
      );
    }
    // if triggerUnAssignCandidate gets updated
    if (prevProps.triggerUnAssignCandidate !== this.props.triggerUnAssignCandidate) {
      this.setState(
        {
          testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates
        },
        () => {
          this.setState({ showUnAssignPopup: true });
        }
      );
    }
    // if triggerReSyncCandidate gets updated
    if (prevProps.triggerReSyncCandidate !== this.props.triggerReSyncCandidate) {
      this.setState(
        {
          testAdministratorAssignedCandidates: this.props.testAdministratorAssignedCandidates
        },
        () => {
          this.setState({ showReSyncPopup: true });
        }
      );
    }
    // if selectedCandidateData gets updated
    if (prevProps.selectedCandidateData !== this.props.selectedCandidateData) {
      const { assigned_test_sections } = this.props.selectedCandidateData;
      let currentHours = [];
      let currentMinutes = [];
      let timedAssignedTestSection = [];
      let defaultTimes = [];
      for (let i = 0; i < assigned_test_sections.length; i++) {
        if (assigned_test_sections[i].test_section_time !== null) {
          currentHours.push(
            getTimeInHoursMinutes(assigned_test_sections[i].test_section_time).formattedHours
          );
          currentMinutes.push(
            getTimeInHoursMinutes(assigned_test_sections[i].test_section_time).formattedMinutes
          );
          timedAssignedTestSection.push(assigned_test_sections[i]);
          defaultTimes.push(assigned_test_sections[i].test_section.default_time);
        }
      }
      this.props.setDefaultTimes(defaultTimes);
      this.setState({
        selectedCandidateData: this.props.selectedCandidateData,
        currentHours: currentHours,
        currentMinutes: currentMinutes,
        timedAssignedTestSection: timedAssignedTestSection
      });
    }
    // if timeUpdated gets updated
    if (prevProps.timeUpdated !== this.props.timeUpdated) {
      this.calculateTotalTestTime();
    }
  };

  closeEditTimePopup = () => {
    this.setState({ showEditTimePopup: false });
  };

  closeLockPausePopup = () => {
    this.setState({ showLockPausePopup: false });
  };

  closeUnlockUnpausePopup = () => {
    this.setState({ showUnlockUnpausePopup: false });
  };

  openLockAllPopup = () => {
    this.setState({ showLockAllPopup: true });
  };

  closeLockAllPopup = () => {
    this.setState({ showLockAllPopup: false });
  };

  openUnlockAllPopup = () => {
    this.setState({ showUnlockAllPopup: true });
  };

  closeUnlockAllPopup = () => {
    this.setState({ showUnlockAllPopup: false });
  };

  handleSetTimer = () => {
    const { currentTimer } = this.props;
    const currentIndex = this.getCurrentTableIndex();
    for (let i = 0; i < currentTimer.length; i++) {
      let timeInMinutes = 0;
      timeInMinutes = Number(currentTimer[i].hours) * 60 + Number(currentTimer[i].minutes);
      this.props
        .updateTestTime(this.state.timedAssignedTestSection[i].id, timeInMinutes)
        .then(() => {
          // if last element of the loop
          if (i === currentTimer.length - 1) {
            // closing edit time popup
            this.closeEditTimePopup();
            this.triggerTaActionLoading(currentIndex);
            // updating TA's assigned candidates table
            this.props.updateAssignedCandidatesTable();
          }
        });
    }
  };

  getCurrentTableIndex = () => {
    // initializing currentIndex
    let currentIndex = null;
    // looping in TA assigned candidates table to get the index of the current selected candidate
    for (let i = 0; i < this.state.testAdministratorAssignedCandidates.length; i++) {
      if (
        this.state.selectedCandidateData.id === this.state.testAdministratorAssignedCandidates[i].id
      ) {
        // updating currentIndex once found
        currentIndex = i;
        break;
      }
    }
    return currentIndex;
  };

  triggerTaActionLoading = index => {
    const { testAdministratorAssignedCandidates, rowsDefinition } = this.state;
    let tempRowsDefinition = rowsDefinition;
    for (let i = 0; i < rowsDefinition.data.length; i++) {
      // provided index is the same as the current index
      if (i === index) {
        tempRowsDefinition.data[i].column_5 = (
          <label className="fa fa-spinner fa-spin">
            <FontAwesomeIcon icon={faSpinner} />
          </label>
        );
        // other candidates than the one selected
      } else {
        tempRowsDefinition.data[i].column_5 = (
          <div>
            <CustomButton
              label={<FontAwesomeIcon icon={faClock} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faThumbsUp} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faTrashAlt} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={
                <FontAwesomeIcon
                  icon={
                    testAdministratorAssignedCandidates[i].status === TEST_STATUS.LOCKED ||
                    testAdministratorAssignedCandidates[i].status === TEST_STATUS.PAUSED
                      ? faLockOpen
                      : faLock
                  }
                />
              }
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faSyncAlt} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
            <CustomButton
              label={<FontAwesomeIcon icon={faEdit} />}
              customStyle={{
                ...TestAdministrationStyle.actionButton,
                ...TestAdministrationStyle.disabledActionButton
              }}
              disabled={true}
            />
          </div>
        );
      }
    }
    this.setState({ rowsDefinition: tempRowsDefinition });
  };

  handleApproveCandidate = () => {
    const currentIndex = this.getCurrentTableIndex();

    const username = this.state.testAdministratorAssignedCandidates[currentIndex].username;
    const test = this.state.testAdministratorAssignedCandidates[currentIndex].test.id;

    // getting needed parameters to send to socket
    const paramsObj = {
      username_id: username,
      test_id: test
    };
    // approving candidate (updating candidate's test status)
    this.props.approveCandidate(username, test).then(response => {
      // request successful
      if (response.status === 200) {
        this.triggerTaActionLoading(currentIndex);
        // getting current socket
        getCurrentSocket(this.props.sockets, response.test_access_code).then(currentSocket => {
          // sending request
          currentSocket.send(
            JSON.stringify({
              action: CHANNELS_ACTION.approveCandidate,
              parameters: JSON.stringify(paramsObj),
              test_access_code: response.test_access_code
            })
          );
        });
        // updating TA's assigned candidates table
        this.props.updateAssignedCandidatesTable();
        // should never happen
      } else {
        throw new Error("Something went wrong during approve candidate process");
      }
    });
  };

  handleLockUnlock = () => {
    // getting candidate's assigned tests data to get the updated test section id
    this.props.getAssignedTests(this.state.selectedCandidateData.id).then(assignedTest => {
      const username = this.state.selectedCandidateData.username;
      const test = this.state.selectedCandidateData.test.id;
      const currentTestStatus = this.state.selectedCandidateData.status;
      const testSectionId = assignedTest[0].test_section;

      const currentIndex = this.getCurrentTableIndex();

      // getting needed parameters to send to socket
      const paramsObj = {
        username_id: username,
        test_id: test,
        test_section_id: testSectionId
      };
      if (currentTestStatus === TEST_STATUS.ACTIVE || currentTestStatus === TEST_STATUS.READY) {
        this.props.lockCandidateTest(username, test, testSectionId).then(response => {
          // request successful
          if (response.status === 200) {
            this.triggerTaActionLoading(currentIndex);
            // getting current socket
            getCurrentSocket(this.props.sockets, response.test_access_code).then(currentSocket => {
              // sending request
              currentSocket.send(
                JSON.stringify({
                  action: CHANNELS_ACTION.lockCandidate,
                  parameters: JSON.stringify(paramsObj),
                  test_access_code: response.test_access_code
                })
              );
            });
            // closing popup
            this.closeLockPausePopup();
            this.props.updateAssignedCandidatesTable();
            // should never happen
          } else {
            throw new Error("Something went wrong during lock candidate process");
          }
        });
      } else if (currentTestStatus === TEST_STATUS.LOCKED) {
        this.props.unlockCandidateTest(username, test, testSectionId).then(response => {
          // request successful
          if (response.status === 200) {
            this.triggerTaActionLoading(currentIndex);
            // getting current socket
            getCurrentSocket(this.props.sockets, response.test_access_code).then(currentSocket => {
              // sending request
              currentSocket.send(
                JSON.stringify({
                  action: CHANNELS_ACTION.unlockCandidate,
                  parameters: JSON.stringify(paramsObj),
                  test_access_code: response.test_access_code
                })
              );
            });
            // closing popup
            this.closeUnlockUnpausePopup();
            this.props.updateAssignedCandidatesTable();
            // should never happen
          } else {
            throw new Error("Something went wrong during unlock candidate process");
          }
        });
      }
    });
  };

  handleLockUnlockAll = () => {
    // unlock all candidates
    if (this.state.allTestsLockedFlag) {
      this.props.unlockAllCandidatesTest().then(response => {
        // request successful
        if (response.status === 200) {
          // looping in candidates array
          for (let i = 0; i < response.candidates.length; i++) {
            // getting needed parameters to send to socket
            const paramsObj = {
              username_id: response.candidates[i].username,
              test_id: response.candidates[i].test
            };
            // getting current socket
            getCurrentSocket(this.props.sockets, response.candidates[i].test_access_code).then(
              currentSocket => {
                // sending request
                currentSocket.send(
                  JSON.stringify({
                    action: CHANNELS_ACTION.unlockAllCandidates,
                    parameters: JSON.stringify(paramsObj),
                    test_access_code: response.candidates[i].test_access_code
                  })
                );
              }
            );
          }
          // closing popup
          this.closeUnlockAllPopup();
          // re-populating active candidates table
          this.props.populateTestAdministratorAssignedCandidates();
          // should never happen
        } else {
          throw new Error("Something went wrong during unlock all candidates process");
        }
      });
      // lock all candidates
    } else {
      this.props.lockAllCandidatesTest().then(response => {
        // request successful
        if (response.status === 200) {
          for (let i = 0; i < response.candidates.length; i++) {
            // getting needed parameters to send to socket
            const paramsObj = {
              username_id: response.candidates[i].username,
              test_id: response.candidates[i].test
            };
            // getting current socket
            getCurrentSocket(this.props.sockets, response.candidates[i].test_access_code).then(
              currentSocket => {
                // sending request
                currentSocket.send(
                  JSON.stringify({
                    action: CHANNELS_ACTION.lockAllCandidates,
                    parameters: JSON.stringify(paramsObj),
                    test_access_code: response.candidates[i].test_access_code
                  })
                );
              }
            );
          }
          // closing popup
          this.closeLockAllPopup();
          // re-populating active candidates table
          this.props.populateTestAdministratorAssignedCandidates();
          // should never happen
        } else {
          throw new Error("Something went wrong during lock all candidates process");
        }
      });
    }
  };

  handlePauseUnpause = () => {
    // getting candidate's assigned tests data to get the updated test section id
    this.props.getAssignedTests(this.state.selectedCandidateData.id).then(assignedTest => {
      const username = this.state.selectedCandidateData.username;
      const test = this.state.selectedCandidateData.test.id;
      const currentTestStatus = this.state.selectedCandidateData.status;
      const testSectionId = assignedTest[0].test_section;

      const currentIndex = this.getCurrentTableIndex();

      // getting needed parameters to send to socket
      const paramsObj = {
        username_id: username,
        test_id: test,
        test_section_id: testSectionId
      };
      if (currentTestStatus === TEST_STATUS.ACTIVE || currentTestStatus === TEST_STATUS.READY) {
        const pauseTime = hoursMinutesToMinutes(
          Number(this.props.currentTimer[0].hours),
          Number(this.props.currentTimer[0].minutes)
        );
        this.props.pauseCandidateTest(username, test, testSectionId, pauseTime).then(response => {
          // request successful
          if (response.status === 200) {
            this.triggerTaActionLoading(currentIndex);
            // getting current socket
            getCurrentSocket(this.props.sockets, response.test_access_code).then(currentSocket => {
              // sending request
              currentSocket.send(
                JSON.stringify({
                  action: CHANNELS_ACTION.pauseCandidate,
                  parameters: JSON.stringify(paramsObj),
                  test_access_code: response.test_access_code,
                  pause_time: pauseTime
                })
              );
            });
            // closing popup
            this.closeLockPausePopup();
            this.props.updateAssignedCandidatesTable();
            // should never happen
          } else {
            throw new Error("Something went wrong during pause candidate process");
          }
        });
      } else if (currentTestStatus === TEST_STATUS.PAUSED) {
        this.props.unpauseCandidateTest(username, test, testSectionId).then(response => {
          // request successful
          if (response.status === 200) {
            this.triggerTaActionLoading(currentIndex);
            // getting current socket
            getCurrentSocket(this.props.sockets, response.test_access_code).then(currentSocket => {
              // sending request
              currentSocket.send(
                JSON.stringify({
                  action: CHANNELS_ACTION.unpauseCandidate,
                  parameters: JSON.stringify(paramsObj),
                  test_access_code: response.test_access_code
                })
              );
            });
            // closing popup
            this.closeUnlockUnpausePopup();
            this.props.updateAssignedCandidatesTable();
            // should never happen
          } else {
            throw new Error("Something went wrong during unpause candidate process");
          }
        });
      }
    });
  };

  handleUnAssign = () => {
    const currentIndex = this.getCurrentTableIndex();
    const username = this.state.testAdministratorAssignedCandidates[currentIndex].username;
    const test = this.state.testAdministratorAssignedCandidates[currentIndex].test.id;

    // getting needed parameters to send to socket
    const paramsObj = {
      username_id: username,
      test_id: test
    };

    this.props.unAssignCandidate(username, test).then(response => {
      // request successful
      if (response.status === 200) {
        this.triggerTaActionLoading(currentIndex);
        // getting current socket
        getCurrentSocket(this.props.sockets, response.test_access_code).then(currentSocket => {
          // sending request
          currentSocket.send(
            JSON.stringify({
              action: CHANNELS_ACTION.unAssignCandidate,
              parameters: JSON.stringify(paramsObj),
              test_access_code: response.test_access_code
            })
          );
        });
        // close un-assign popup
        this.handleCloseUnAssignPopup();
        // updating TA's assigned candidates table
        this.props.updateAssignedCandidatesTable();
        // should never happen
      } else {
        throw new Error("Something went wrong during approve candidate process");
      }
    });
  };

  handleReSync = () => {
    const currentIndex = this.getCurrentTableIndex();
    const username = this.state.testAdministratorAssignedCandidates[currentIndex].username;
    const test = this.state.testAdministratorAssignedCandidates[currentIndex].test.id;
    const test_access_code = this.state.testAdministratorAssignedCandidates[currentIndex]
      .test_access_code;

    this.props.reSyncCandidate(username, test, test_access_code).then(response => {
      // request successful
      if (response.status === 200) {
        this.triggerTaActionLoading(currentIndex);
        // close re-sync popup
        this.handleCloseReSyncPopup();
        // updating TA's assigned candidates table
        this.props.updateAssignedCandidatesTable();
        // should never happen
      } else {
        throw new Error("Something went wrong during re-sync candidate process");
      }
    });
  };

  // reset lock/pause popup states + timer state
  resetNeededLockPausePopupStates = () => {
    this.setState({ pauseCheckboxChecked: false });
    this.props.updateTimerState([]);
  };

  // handling toggle lock/pause popup checkbox
  toggleLockPausePopupCheckbox = () => {
    this.setState({ pauseCheckboxChecked: !this.state.pauseCheckboxChecked }, () => {
      // pauseCheckboxChecked is now set to true
      if (this.state.pauseCheckboxChecked) {
        // updating default times redux state for timer validation
        this.props.setDefaultTimes([1]);
      }
    });
  };

  onPopupClose = () => {
    // initializing currentTimer redux state to []
    this.props.updateTimerState([]);
  };

  calculateTotalTestTime = () => {
    let totalMinutes = 0;
    // calculating the total time in minutes
    for (let i = 0; i < this.props.currentTimer.length; i++) {
      totalMinutes += Number(this.props.currentTimer[i].hours) * 60;
      totalMinutes += Number(this.props.currentTimer[i].minutes);
    }
    // saving total time in hours and minutes
    this.setState({
      totalHours: getTimeInHoursMinutes(totalMinutes).hours,
      totalMinutes: getTimeInHoursMinutes(totalMinutes).minutes
    });
  };

  handleCloseReSyncPopup = () => {
    this.setState({ showReSyncPopup: false });
  };

  handleCloseUnAssignPopup = () => {
    this.setState({ showUnAssignPopup: false });
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.candidate,
        style: { width: "35%" }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.dob,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.status,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.timer,
        style: { ...{ width: "15%" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.testAdministration.activeCandidates.table.actions,
        style: { ...{ width: "20%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];

    return (
      <div style={TestAccessCodesStyle.mainContainer}>
        <div>
          <h2>{LOCALIZE.testAdministration.sideNavItems.activeCandidates}</h2>
          <p>{LOCALIZE.testAdministration.activeCandidates.description}</p>
        </div>
        <CustomButton
          label={
            <>
              <span>
                <FontAwesomeIcon
                  icon={this.state.allTestsLockedFlag ? faLockOpen : faLock}
                  style={styles.buttonIcon}
                />
              </span>
              <span>
                {!this.state.atLeastOneTestToLockUnlock
                  ? LOCALIZE.testAdministration.activeCandidates.lockAllButton
                  : this.state.allTestsLockedFlag
                  ? LOCALIZE.testAdministration.activeCandidates.unlockAllButton
                  : LOCALIZE.testAdministration.activeCandidates.lockAllButton}
              </span>
            </>
          }
          action={this.state.allTestsLockedFlag ? this.openUnlockAllPopup : this.openLockAllPopup}
          customStyle={styles.lockUnlockAllButton}
          buttonTheme={THEME.SECONDARY}
          disabled={this.props.currentlyLoading || !this.state.atLeastOneTestToLockUnlock}
        />
        <div style={TestAccessCodesStyle.tableContainer}>
          <GenericTable
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={LOCALIZE.testAdministration.activeCandidates.noActiveCandidates}
            currentlyLoading={this.props.currentlyLoading}
            triggerReRender={this.props.triggerReRender}
          />
        </div>
        {Object.keys(this.state.selectedCandidateData).length > 0 && (
          <div>
            <PopupBox
              show={this.state.showEditTimePopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              onPopupClose={this.onPopupClose}
              onPopupOpen={this.calculateTotalTestTime}
              title={LOCALIZE.testAdministration.activeCandidates.editTimePopup.title}
              description={
                <div>
                  <div>
                    <p>
                      <span style={styles.bold}>
                        {LOCALIZE.testAdministration.activeCandidates.editTimePopup.description1}
                      </span>
                      <br />
                      <span>
                        {LOCALIZE.formatString(
                          LOCALIZE.testAdministration.activeCandidates.editTimePopup.description2,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_first_name}
                          </span>,
                          <span style={styles.bold}>
                            {this.state.selectedCandidateData.candidate_last_name}
                          </span>
                        )}
                      </span>
                    </p>
                  </div>
                  {this.state.timedAssignedTestSection.map((assigned_test_section, index) => {
                    return (
                      <div key={index} style={styles.testSectionTimerMainContainer}>
                        <div style={styles.testSectionTimerContainer}>
                          <div style={styles.testSectionTimerDescriptionContainer}>
                            <p style={{ ...styles.bold, ...styles.editTimeTitle }}>
                              {
                                assigned_test_section.test_section[
                                  `${this.props.currentLanguage}_title`
                                ]
                              }
                            </p>
                            <p>
                              {
                                assigned_test_section.test_section[
                                  `${this.props.currentLanguage}_description`
                                ]
                              }
                            </p>
                          </div>
                          <div style={styles.testSectionTimerTimeContainer}>
                            <SetTimer
                              index={index}
                              currentHours={this.state.currentHours[index]}
                              currentMinutes={this.state.currentMinutes[index]}
                              minHours={
                                getTimeInHoursMinutes(
                                  assigned_test_section.test_section.default_time
                                ).hours
                              }
                              // max = 2 X default hours value
                              maxHours={
                                getTimeInHoursMinutes(
                                  assigned_test_section.test_section.default_time
                                ).hours * 2
                              }
                              minutesLeaps={5}
                              maxMinutes={55}
                            />
                          </div>
                        </div>
                      </div>
                    );
                  })}
                  <div>
                    <p>{LOCALIZE.testAdministration.activeCandidates.editTimePopup.description3}</p>
                    <p style={styles.bold}>
                      {LOCALIZE.formatString(
                        LOCALIZE.testAdministration.activeCandidates.editTimePopup.description4,
                        <span>{this.state.totalHours}</span>,
                        <span>{this.state.totalMinutes}</span>
                      )}
                    </p>
                  </div>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeEditTimePopup}
              rightButtonType={BUTTON_TYPE.success}
              rightButtonTitle={
                LOCALIZE.testAdministration.activeCandidates.editTimePopup.setTimerButton
              }
              rightButtonState={
                this.props.validTimer ? BUTTON_STATE.enabled : BUTTON_STATE.disabled
              }
              rightButtonIcon={faClock}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={this.handleSetTimer}
            />
            <PopupBox
              show={this.state.showLockPausePopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              onPopupClose={this.resetNeededLockPausePopupStates}
              title={LOCALIZE.testAdministration.activeCandidates.lockPausePopup.title}
              description={
                <div>
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.activeCandidates.lockPausePopup.description1,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.candidate_first_name}
                      </span>,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.candidate_last_name}
                      </span>
                    )}
                  </p>
                  <div style={styles.descriptionSeparator}>
                    <input
                      id="pause-checkbox"
                      type="checkbox"
                      style={styles.checkbox}
                      onClick={this.toggleLockPausePopupCheckbox}
                    ></input>
                    <label htmlFor="pause-checkbox" style={styles.checkboxDescription}>
                      <span>
                        {LOCALIZE.testAdministration.activeCandidates.lockPausePopup.checkboxPart1}
                      </span>
                      <span style={styles.bold}>
                        {LOCALIZE.testAdministration.activeCandidates.lockPausePopup.checkboxPart2}
                      </span>
                    </label>
                    {this.state.pauseCheckboxChecked && (
                      <div>
                        <SetTimer
                          index={0}
                          currentHours={"00"}
                          currentMinutes={"00"}
                          minHours={0}
                          maxHours={3}
                          minutesLeaps={1}
                          maxMinutes={55}
                        />
                      </div>
                    )}
                  </div>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeLockPausePopup}
              rightButtonType={BUTTON_TYPE.success}
              rightButtonTitle={
                this.state.pauseCheckboxChecked
                  ? LOCALIZE.testAdministration.activeCandidates.lockPausePopup.pauseButton
                  : LOCALIZE.testAdministration.activeCandidates.lockPausePopup.lockButton
              }
              rightButtonIcon={faLock}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={
                this.state.pauseCheckboxChecked ? this.handlePauseUnpause : this.handleLockUnlock
              }
              rightButtonState={
                this.state.pauseCheckboxChecked && !this.props.validTimer
                  ? BUTTON_STATE.disabled
                  : BUTTON_STATE.enabled
              }
            />
            <PopupBox
              show={this.state.showUnlockUnpausePopup}
              handleClose={() => {}}
              isBackdropStatic={true}
              shouldCloseOnEsc={false}
              customPopupStyle={styles.customPopupStyle}
              title={LOCALIZE.testAdministration.activeCandidates.unlockUnpausePopup.title}
              description={
                <div>
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.activeCandidates.unlockUnpausePopup.description1,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.candidate_first_name}
                      </span>,
                      <span style={styles.bold}>
                        {this.state.selectedCandidateData.candidate_last_name}
                      </span>
                    )}
                  </p>
                  <p>
                    {LOCALIZE.testAdministration.activeCandidates.unlockUnpausePopup.description2}
                  </p>
                  <p>
                    {LOCALIZE.testAdministration.activeCandidates.unlockUnpausePopup.description3}
                  </p>
                </div>
              }
              leftButtonType={BUTTON_TYPE.secondary}
              leftButtonTitle={LOCALIZE.commons.cancel}
              leftButtonCustomStyle={{ minWidth: 150 }}
              leftButtonIcon={faTimes}
              leftButtonAction={this.closeUnlockUnpausePopup}
              rightButtonType={BUTTON_TYPE.success}
              rightButtonTitle={
                this.state.selectedCandidateData.status === TEST_STATUS.LOCKED
                  ? LOCALIZE.testAdministration.activeCandidates.unlockUnpausePopup.unlockButton
                  : LOCALIZE.testAdministration.activeCandidates.unlockUnpausePopup.unpauseButton
              }
              rightButtonIcon={faLockOpen}
              rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
              rightButtonAction={
                this.state.selectedCandidateData.status === TEST_STATUS.LOCKED
                  ? this.handleLockUnlock
                  : this.handlePauseUnpause
              }
            />
          </div>
        )}
        <PopupBox
          show={this.state.showLockAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          customPopupStyle={styles.customPopupStyle}
          title={LOCALIZE.testAdministration.activeCandidates.lockAllPopup.title}
          description={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.lockAllPopup.description1}</p>
              <p>{LOCALIZE.testAdministration.activeCandidates.lockAllPopup.description2}</p>
              <p>{LOCALIZE.testAdministration.activeCandidates.lockAllPopup.description3}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeLockAllPopup}
          rightButtonType={BUTTON_TYPE.success}
          rightButtonTitle={
            LOCALIZE.testAdministration.activeCandidates.lockAllPopup.lockTestsButton
          }
          rightButtonIcon={faLock}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleLockUnlockAll}
        />
        <PopupBox
          show={this.state.showUnlockAllPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          customPopupStyle={styles.customPopupStyle}
          title={LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.title}
          description={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.description1}</p>
              <p>{LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.description2}</p>
              <p>{LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.description3}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonCustomStyle={{ minWidth: 150 }}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeUnlockAllPopup}
          rightButtonType={BUTTON_TYPE.success}
          rightButtonTitle={
            LOCALIZE.testAdministration.activeCandidates.unlockAllPopup.unlockTestsButton
          }
          rightButtonIcon={faLockOpen}
          rightButtonIconCustomStyle={{ transform: "scale(1.25)" }}
          rightButtonAction={this.handleLockUnlockAll}
        />
        {Object.keys(this.state.selectedCandidateData).length > 0 && (
          <PopupBox
            show={this.state.showReSyncPopup}
            handleClose={() => {}}
            isBackdropStatic={true}
            shouldCloseOnEsc={false}
            customPopupStyle={styles.customPopupStyle}
            title={LOCALIZE.testAdministration.activeCandidates.reSyncPopup.title}
            description={
              <div>
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.reSyncPopup.description,
                    <span style={styles.bold}>
                      {this.state.selectedCandidateData.candidate_first_name}
                    </span>,
                    <span style={styles.bold}>
                      {this.state.selectedCandidateData.candidate_last_name}
                    </span>
                  )}
                </p>
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.handleCloseReSyncPopup}
            rightButtonType={BUTTON_TYPE.success}
            rightButtonTitle={LOCALIZE.commons.confirm}
            rightButtonIcon={faCheck}
            rightButtonAction={this.handleReSync}
          />
        )}
        {Object.keys(this.state.selectedCandidateData).length > 0 && (
          <PopupBox
            show={this.state.showUnAssignPopup}
            handleClose={() => {}}
            isBackdropStatic={true}
            shouldCloseOnEsc={false}
            customPopupStyle={styles.customPopupStyle}
            title={LOCALIZE.testAdministration.activeCandidates.unAssignPopup.title}
            description={
              <div>
                <p>
                  {LOCALIZE.formatString(
                    LOCALIZE.testAdministration.activeCandidates.unAssignPopup.description,
                    <span style={styles.bold}>
                      {this.state.selectedCandidateData.candidate_first_name}
                    </span>,
                    <span style={styles.bold}>
                      {this.state.selectedCandidateData.candidate_last_name}
                    </span>
                  )}
                </p>
              </div>
            }
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            leftButtonIcon={faTimes}
            leftButtonAction={this.handleCloseUnAssignPopup}
            rightButtonType={BUTTON_TYPE.success}
            rightButtonTitle={LOCALIZE.commons.confirm}
            rightButtonIcon={faCheck}
            rightButtonAction={this.handleUnAssign}
          />
        )}
      </div>
    );
  }
}

export { ActiveCandidates as unconnectedActiveCandidates };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    currentTimer: state.timer.currentTimer,
    validTimer: state.timer.validTimer,
    timeUpdated: state.timer.timeUpdated,
    username: state.user.username,
    isTestLocked: state.testStatus.isTestLocked
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateTestTime,
      updateTimerState,
      setDefaultTimes,
      resetTimerStates,
      approveCandidate,
      unAssignCandidate,
      reSyncCandidate,
      lockCandidateTest,
      unlockCandidateTest,
      lockAllCandidatesTest,
      unlockAllCandidatesTest,
      pauseCandidateTest,
      unpauseCandidateTest,
      getAssignedTests
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveCandidates);
