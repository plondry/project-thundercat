import LOCALIZE from "../../text_resources";
import { TEST_STATUS } from "./Constants";

// getting formatted test statuses
function getFormattedTestStatus(statusId) {
  // see 'assigned_test_status.py' for references
  if (statusId === TEST_STATUS.ASSIGNED) {
    return LOCALIZE.commons.status.checkedIn;
  } else if (statusId === TEST_STATUS.READY) {
    return LOCALIZE.commons.status.ready;
  } else if (statusId === TEST_STATUS.ACTIVE) {
    return LOCALIZE.commons.status.active;
  } else if (statusId === TEST_STATUS.LOCKED) {
    return LOCALIZE.commons.status.locked;
  } else if (statusId === TEST_STATUS.PAUSED) {
    return LOCALIZE.commons.status.paused;
    // should never happen
  } else {
    return "non defined status";
  }
}

export default getFormattedTestStatus;
