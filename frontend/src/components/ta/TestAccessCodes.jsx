import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import { bindActionCreators } from "redux";
import { styles as ActivePermissionsStyles } from "../etta/permissions/ActivePermissions";
import { alternateColorsStyle } from "../commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner, faMagic, faTimes, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { deleteTestAccessCode } from "../../modules/TestAdministrationRedux";
import { getTestPermissionFinancialData } from "../../modules/PermissionsRedux";
import {
  resetTestAdministrationStates,
  getNewTestAccessCode
} from "../../modules/TestAdministrationRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import TestSessionInformationPopup from "./TestSessionInformationPopup";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  tableContainer: {
    margin: "24px 24px 24px 0"
  },
  tableLayout: {
    tableLayout: "fixed"
  },
  tableHeadTestAccessCode: {
    width: "25%",
    paddingLeft: 12
  },
  tableHeadTest: {
    width: "15%",
    textAlign: "center"
  },
  tableHeadStaffingProcessNumber: {
    width: "40%",
    textAlign: "center"
  },
  tableHeadAction: {
    width: "20%",
    textAlign: "center"
  },
  centeredText: {
    textAlign: "center"
  },
  testAccessCodeColumn: {
    paddingLeft: 12,
    overflowWrap: "anywhere"
  },
  staffingProcessNumberColumn: {
    overflowWrap: "anywhere"
  },
  staffingProcessNumberLabel: {
    width: "100%",
    wordWrap: "break-word",
    padding: "6px 0"
  },
  disableButton: {
    minWidth: 100,
    margin: "6px 0"
  },
  generateCodeButton: {
    background: "transparent",
    padding: 12,
    border: "none"
  },
  generateCodeButtonIcon: {
    marginRight: 12,
    color: "#00565e"
  },
  customPopupWidth: {
    width: 600
  },
  boldText: {
    fontWeight: "bold"
  }
};

class TestAccessCodes extends Component {
  static propTypes = {
    activeTestAccessCodes: PropTypes.array.isRequired,
    activeTestAccessCodesLoading: PropTypes.bool,
    getActiveTestAccessCodes: PropTypes.func.isRequired,
    testOrderNumberOptions: PropTypes.array.isRequired,
    populateTestToAdministerOptions: PropTypes.func.isRequired,
    testToAdministerOptions: PropTypes.array.isRequired,
    populateTestSessionLanguageOptions: PropTypes.func.isRequired,
    testSessionLanguageOptions: PropTypes.array.isRequired,
    // provided by redux
    deleteTestAccessCode: PropTypes.func,
    getTestPermissionFinancialData: PropTypes.func,
    resetTestAdministrationStates: PropTypes.func,
    getNewTestAccessCode: PropTypes.func
  };

  state = {
    activeTestAccessCodes: [],
    showGenerateTestAccessCodePopup: false,
    showDisableConfirmationPopup: false,
    popupOverflowVisible: false,
    testAccessCodeToDisable: ""
  };

  componentDidUpdate = prevProps => {
    // if activeTestAccessCodes gets updated
    if (prevProps.activeTestAccessCodes !== this.props.activeTestAccessCodes) {
      this.setState({ activeTestAccessCodes: this.props.activeTestAccessCodes });
    }
  };

  openGenerateTestAccessCodePopup = () => {
    this.setState({
      showGenerateTestAccessCodePopup: true
    });
  };

  closeGenerateTestAccessCodePopup = id => {
    this.setState({ showGenerateTestAccessCodePopup: false });
  };

  openDisableConfirmationPopup = id => {
    this.setState({
      showDisableConfirmationPopup: true,
      testAccessCodeToDisable: this.props.activeTestAccessCodes[id].test_access_code
    });
  };

  closeDisableConfirmationPopup = () => {
    this.setState({ showDisableConfirmationPopup: false, testAccessCodeToDisable: "" });
  };

  // resetting test administration redux states
  resetInputs = () => {
    this.props.resetTestAdministrationStates();
  };

  // handle disable selected test access code
  handleDisableTestAccessCode = () => {
    this.props.deleteTestAccessCode(this.state.testAccessCodeToDisable).then(response => {
      // if request succeeded
      if (response.status === 200) {
        // re-populating active test access codes table
        this.props.getActiveTestAccessCodes();
        // reseting testAccessCodeToDisable state
        this.setState({ testAccessCodeToDisable: "" });
        // should never happen
      } else {
        throw new Error("Something went wrong during disable test access code process");
      }
    });
  };

  // handling generate new test access code functionality
  handleGenerateTestAccessCode = () => {
    const testSessionLanguageId =
      this.props.testSessionLanguage.value === LOCALIZE.commons.english ? 1 : 2;
    // getting new test access code based on selected parameters
    this.props
      .getNewTestAccessCode(
        this.props.testOrderNumber.value,
        testSessionLanguageId,
        this.props.testToAdminister.value.id
      )
      .then(response => {
        // request successful
        if (response.status === 200) {
          // re-populating active test access codes table
          this.props.getActiveTestAccessCodes();
          // closing popup
          this.closeGenerateTestAccessCodePopup();
          // resetting inputs
          this.resetInputs();
          // should never happen
        } else {
          throw new Error("Something went wrong during generate new test access code process");
        }
      });
  };

  onDropdownOpen = () => {
    this.setState({ popupOverflowVisible: true });
  };

  onDropdownClose = () => {
    this.setState({ popupOverflowVisible: false });
  };

  render() {
    return (
      <div style={styles.mainContainer}>
        <div>
          <h2>{LOCALIZE.testAdministration.sideNavItems.testAccessCodes}</h2>
          <p>{LOCALIZE.testAdministration.testAccessCodes.description}</p>
        </div>
        <div style={styles.tableContainer}>
          <table style={{ ...styles.tableLayout, ...ActivePermissionsStyles.table }}>
            <thead>
              <tr style={ActivePermissionsStyles.tableHead}>
                <th id="test-access-code" scope="col" style={styles.tableHeadTestAccessCode}>
                  {LOCALIZE.testAdministration.testAccessCodes.table.testAccessCode}
                </th>
                <th id="test" scope="col" style={styles.tableHeadTest}>
                  {LOCALIZE.testAdministration.testAccessCodes.table.test}
                </th>
                <th
                  id="staffing-process-number"
                  scope="col"
                  style={styles.tableHeadStaffingProcessNumber}
                >
                  {LOCALIZE.testAdministration.testAccessCodes.table.staffingProcessNumber}
                </th>
                <th id="action" scope="col" style={styles.tableHeadAction}>
                  {LOCALIZE.testAdministration.testAccessCodes.table.action}
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.activeTestAccessCodes.length > 0 &&
                !this.props.activeTestAccessCodesLoading &&
                this.state.activeTestAccessCodes.map((testAccessCode, id) => {
                  return (
                    <tr
                      id={`table-row-${id}`}
                      key={id}
                      style={alternateColorsStyle(id, 60)}
                      tabIndex={0}
                      aria-labelledby={`test-access-code test-access-code-label-${id} test test-label-${id} staffing-process-number staffing-process-number-label-${id} action action-label-${id}`}
                    >
                      <td id={`test-access-code-label-${id}`} style={styles.testAccessCodeColumn}>
                        <label>{testAccessCode.test_access_code}</label>
                      </td>
                      <td id={`test-label-${id}`} style={styles.centeredText}>
                        <label>
                          {this.props.currentLanguage === LANGUAGES.english
                            ? testAccessCode.en_test_name
                            : testAccessCode.fr_test_name}
                        </label>
                      </td>
                      <td
                        id={`staffing-process-number-label-${id}`}
                        style={{ ...styles.centeredText, ...styles.staffingProcessNumberColumn }}
                      >
                        <label style={styles.staffingProcessNumberLabel}>
                          {testAccessCode.staffing_process_number}
                        </label>
                      </td>
                      <td id={`action-label-${id}`} style={styles.centeredText}>
                        <CustomButton
                          label={LOCALIZE.testAdministration.testAccessCodes.table.actionButton}
                          action={() => this.openDisableConfirmationPopup(id)}
                          customStyle={styles.disableButton}
                          buttonTheme={THEME.SECONDARY}
                        />
                      </td>
                    </tr>
                  );
                })}
              {!this.props.activeTestAccessCodesLoading && (
                <tr>
                  <td
                    id="generate-test-access-code-button"
                    colSpan="4"
                    style={alternateColorsStyle(this.state.activeTestAccessCodes.length, 60)}
                  >
                    <button
                      style={styles.generateCodeButton}
                      onClick={this.openGenerateTestAccessCodePopup}
                    >
                      <span style={styles.generateCodeButtonIcon}>
                        <FontAwesomeIcon icon={faMagic} />
                      </span>
                      <span>
                        {LOCALIZE.testAdministration.testAccessCodes.table.generateNewCode}
                      </span>
                    </button>
                  </td>
                  <td style={ActivePermissionsStyles.displayNone}></td>
                  <td style={ActivePermissionsStyles.displayNone}></td>
                  <td style={ActivePermissionsStyles.displayNone}></td>
                </tr>
              )}
              {this.props.activeTestAccessCodesLoading && (
                <tr style={ActivePermissionsStyles.loading}>
                  <td colSpan="4">
                    <label className="fa fa-spinner fa-spin">
                      <FontAwesomeIcon icon={faSpinner} />
                    </label>
                  </td>
                  <td style={ActivePermissionsStyles.displayNone}></td>
                  <td style={ActivePermissionsStyles.displayNone}></td>
                  <td style={ActivePermissionsStyles.displayNone}></td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <PopupBox
          show={this.state.showGenerateTestAccessCodePopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          onPopupClose={this.resetInputs}
          overflowVisible={this.props.popupOverflowVisible}
          title={LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.title}
          description={
            <TestSessionInformationPopup
              testOrderNumberOptions={this.props.testOrderNumberOptions}
              populateTestToAdministerOptions={this.props.populateTestToAdministerOptions}
              testToAdministerOptions={this.props.testToAdministerOptions}
              populateTestSessionLanguageOptions={this.props.populateTestSessionLanguageOptions}
              testSessionLanguageOptions={this.props.testSessionLanguageOptions}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeGenerateTestAccessCodePopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={
            LOCALIZE.testAdministration.testAccessCodes.generateTestAccessCodePopup.generateButton
          }
          rightButtonIcon={faMagic}
          rightButtonState={this.props.generateButtonDisabled}
          rightButtonAction={this.handleGenerateTestAccessCode}
        />
        <PopupBox
          show={this.state.showDisableConfirmationPopup}
          handleClose={this.closeDisableConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          customPopupStyle={styles.customPopupWidth}
          title={
            LOCALIZE.testAdministration.testAccessCodes.disableTestAccessCodeConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={
                  LOCALIZE.testAdministration.testAccessCodes.disableTestAccessCodeConfirmationPopup
                    .warning
                }
                message={
                  <p>
                    {LOCALIZE.formatString(
                      LOCALIZE.testAdministration.testAccessCodes
                        .disableTestAccessCodeConfirmationPopup.description,
                      <span style={ActivePermissionsStyles.boldText}>
                        {this.state.testAccessCodeToDisable}
                      </span>
                    )}
                  </p>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonIcon={faTimes}
          leftButtonAction={this.closeDisableConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.testAdministration.testAccessCodes.table.actionButton}
          rightButtonIcon={faTrashAlt}
          rightButtonIconCustomStyle={styles.customPopupButtonStyle}
          rightButtonAction={this.handleDisableTestAccessCode}
        />
      </div>
    );
  }
}

export { TestAccessCodes as unconnectedTestAccessCodes };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    username: state.user.username,
    generateButtonDisabled: state.testAdministration.generateButtonDisabled,
    popupOverflowVisible: state.testAdministration.popupOverflowVisible,
    testOrderNumber: state.testAdministration.testOrderNumber,
    testSessionLanguage: state.testAdministration.testSessionLanguage,
    testToAdminister: state.testAdministration.testToAdminister
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteTestAccessCode,
      getTestPermissionFinancialData,
      resetTestAdministrationStates,
      getNewTestAccessCode
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAccessCodes);
