import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import "../../css/test-administration.css";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import SideNavigation from "../eMIB/SideNavigation";
import TestAccessCodes from "./TestAccessCodes";
import ActiveCandidates from "./ActiveCandidates";
import Reports from "./Reports";
import {
  getActiveTestAccessCodesRedux,
  getTestAdministratorAssignedCandidates
} from "../../modules/TestAdministrationRedux";
import { getTestPermissions } from "../../modules/PermissionsRedux";
import { COMMON_STYLE } from "../commons/GenericTable";
import { getTimeInHoursMinutes } from "../../helpers/timeConversion";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClock,
  faThumbsUp,
  faLock,
  faEdit,
  faLockOpen,
  faTrashAlt,
  faSyncAlt
} from "@fortawesome/free-solid-svg-icons";
import { faThumbsUp as regularFaThumbsUp } from "@fortawesome/free-regular-svg-icons";
import getFormattedTestStatus from "./TestStatus";
import { TEST_STATUS } from "./Constants";
import {
  testAdministratorSideTestSessionChannels,
  CHANNELS_PATH,
  CHANNELS_ACTION
} from "../../helpers/testSessionChannels";
import { PATH } from "../../components/commons/Constants";
import StyledTooltip from "../authentication/StyledTooltip";
import "rc-tooltip/assets/bootstrap_white.css";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

export const styles = {
  actionButton: {
    minWidth: 25,
    background: "transparent",
    border: "none",
    padding: "6px 3px",
    margin: "0 3px",
    color: "#00565e",
    fontSize: 20
  },
  disabledActionButton: {
    // default disabled color
    color: "#DDDDDD"
  },
  time: {
    padding: "6px 12px",
    border: "2px solid #00565e",
    borderRadius: 8,
    fontWeight: "bold",
    color: "#00565e"
  },
  noMargin: {
    margin: 0
  },
  allUnset: {
    all: "unset"
  }
};

class TestAdministration extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  constructor(props) {
    super(props);
    this.languageFieldRef = React.createRef();
    this.testFieldRef = React.createRef();
  }

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func,
    getActiveTestAccessCodesRedux: PropTypes.func,
    getTestPermissions: PropTypes.func,
    getTestAdministratorAssignedCandidates: PropTypes.func
  };

  state = {
    isLoaded: false,
    testPermissions: [],
    activeTestAccessCodes: [],
    activeTestAccessCodesLoading: false,
    testOrderNumberOptions: [],
    testToAdministerOptions: [],
    testSessionLanguageOptions: [],
    testAdministratorAssignedCandidates: [],
    rowsDefinition: {},
    currentlyLoading: false,
    selectedCandidateData: {},
    triggerShowEditTimePopup: false,
    triggerShowLockPausePopup: false,
    triggerShowUnlockUnpausePopup: false,
    triggerApproveCandidate: false,
    triggerUnAssignCandidate: false,
    triggerReSyncCandidate: false,
    sockets: [],
    testAccessCodes: [],
    triggerReRender: false
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.getActiveTestAccessCodes();
        this.populateTestPermissions();
        this.populateTestAdministratorAssignedCandidates();
        this.setState({
          isLoaded: true
        });
      }
    });
  };

  componentDidUpdate(prevProps, prevState) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // if CAT language toggle button has been selected
        if (prevProps.currentLanguage !== this.props.currentLanguage) {
          this.getActiveTestAccessCodes();
          this.populateTestPermissions();
          this.populateTestAdministratorAssignedCandidates();
        }
        // once page is loaded, focus on welcome message (accessibility purposes)
        if (prevState.isLoaded !== this.state.isLoaded) {
          document.getElementById("user-welcome-message-div").focus();
        }
      }
    });
  }

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // getting all active test access codes for the current TA
  getActiveTestAccessCodes = () => {
    this.setState({ activeTestAccessCodesLoading: true }, () => {
      let activeTestAccessCodes = [];
      this.props
        .getActiveTestAccessCodesRedux()
        .then(response => {
          // looping in all active test access codes
          for (let i = 0; i < response.length; i++) {
            // pushing results in array
            activeTestAccessCodes.push(response[i]);
            // getting socket
            this.getSocket(i, response);
          }
          // saving array in activeTestAccessCodes state
          this.setState({ activeTestAccessCodes: activeTestAccessCodes });
        })
        .then(() => {
          this.setState({ activeTestAccessCodesLoading: false });
        });
    });
  };

  // populating user' test permissions object
  populateTestPermissions = () => {
    this.props.getTestPermissions().then(response => {
      this.setState({ testPermissions: response }, () => {
        // populating test order number options
        this.populateTestOrderNumberOptions();
      });
    });
  };

  // populating test order number options
  populateTestOrderNumberOptions = () => {
    let testOrderNumberOptionsDuplicatesRomoval = [];
    let testOrderNumberOptionsArray = [];
    let testOrderNumberOptions = [];
    // looping in test permissions
    for (let i = 0; i < this.state.testPermissions.length; i++) {
      // if test order number does not already exist in testOrderNumberOptionsArray push it
      if (
        testOrderNumberOptionsDuplicatesRomoval.indexOf(
          this.state.testPermissions[i].test_order_number
        ) < 0
      ) {
        testOrderNumberOptionsArray.push(
          `${this.state.testPermissions[i].test_order_number} (${this.state.testPermissions[i].staffing_process_number})`
        );
        testOrderNumberOptionsDuplicatesRomoval.push(
          this.state.testPermissions[i].test_order_number
        );
      }
    }
    // looping in testOrderNumberOptionsArray in order to create the testOrderNumberOptions array (including values and labels)
    for (let i = 0; i < testOrderNumberOptionsArray.length; i++) {
      testOrderNumberOptions.push({
        label: testOrderNumberOptionsArray[i],
        // getting only the first part of the test order number - without (<staffing_process_number>)
        value: testOrderNumberOptionsArray[i].split(" ")[0]
      });
    }
    // saving results in state
    this.setState({ testOrderNumberOptions: testOrderNumberOptions });
  };

  // populating test to administer options
  populateTestToAdministerOptions = testOrderNumber => {
    let testToAdministerOptionsArray = [];
    let testToAdministerOptions = [];
    let testToAdministerTestInternalNames = [];
    // looping in test permissions
    for (let i = 0; i < this.state.testPermissions.length; i++) {
      // getting all object attributes where test order number is the same as the one provided
      if (testOrderNumber.value === this.state.testPermissions[i].test_order_number) {
        // if test to administer does not already exist in testToAdministerOptionsArray push it
        if (
          testToAdministerOptionsArray.indexOf(
            this.state.testPermissions[i].test[`${this.props.currentLanguage}_name`]
          ) < 0
        ) {
          testToAdministerOptionsArray.push(
            this.state.testPermissions[i].test[`${this.props.currentLanguage}_name`]
          );
          testToAdministerTestInternalNames.push(this.state.testPermissions[i].test);
        }
      }
    }
    // looping in testToAdministerOptionsArray in order to create the testToAdministerOptions array (including values and labels)
    for (let i = 0; i < testToAdministerOptionsArray.length; i++) {
      testToAdministerOptions.push({
        label: testToAdministerOptionsArray[i],
        value: testToAdministerTestInternalNames[i]
      });
    }
    // saving results in state
    this.setState({ testToAdministerOptions: testToAdministerOptions });
  };

  // populating test session language options
  populateTestSessionLanguageOptions = () => {
    let testSessionLanguageOptions = [];
    // english option
    testSessionLanguageOptions.push({
      label: LOCALIZE.commons.english,
      value: LOCALIZE.commons.english
    });
    // french option
    testSessionLanguageOptions.push({
      label: LOCALIZE.commons.french,
      value: LOCALIZE.commons.french
    });
    // saving results in state
    this.setState({ testSessionLanguageOptions: testSessionLanguageOptions });
  };

  // populating assigned candidates of the current test administrator
  populateTestAdministratorAssignedCandidates = () => {
    this.setState({ currentlyLoading: true }, () => {
      // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
      let rowsDefinition = {};
      let data = [];
      // initializing testAdministratorAssignedCandidates array
      let testAdministratorAssignedCandidates = [];
      this.props
        .getTestAdministratorAssignedCandidates()
        .then(response => {
          // looping in response given
          for (let i = 0; i < response.length; i++) {
            // populating data object with provided data
            data.push({
              column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].username})`,
              column_2: response[i].candidate_dob,
              column_3: getFormattedTestStatus(Number(response[i].status)),
              column_4: this.getTotalTestTime(response[i].assigned_test_sections),
              column_5: this.populateColumnFive(i, response)
            });

            // getting socket
            this.getSocket(i, response);
          }

          // updating testAdministratorAssignedCandidates array with provided data
          testAdministratorAssignedCandidates = response;

          // updating rowsDefinition object with provided data and needed style
          rowsDefinition = {
            column_1_style: COMMON_STYLE.LEFT_TEXT,
            column_2_style: COMMON_STYLE.CENTERED_TEXT,
            column_3_style: COMMON_STYLE.CENTERED_TEXT,
            column_4_style: COMMON_STYLE.CENTERED_TEXT,
            column_5_style: COMMON_STYLE.CENTERED_TEXT,
            data: data
          };

          // saving results in state
          this.setState({
            testAdministratorAssignedCandidates: testAdministratorAssignedCandidates,
            rowsDefinition: rowsDefinition
          });
        })
        .then(() => {
          this.setState({ currentlyLoading: false });
        });
    });
  };

  getSocket = (i, assignedCandidates) => {
    let testAccessCodes = this.state.testAccessCodes;
    // checking if the current test access code has already been added to test administrator room channels
    if (testAccessCodes.indexOf(assignedCandidates[i].test_access_code) < 0) {
      let sockets = this.state.sockets;
      const socket = testAdministratorSideTestSessionChannels(
        `${CHANNELS_PATH.testAdministratorRoom}${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}/${
          assignedCandidates[i].test_access_code
        }`
      );
      // adding current socket to sockets array
      sockets.push(socket);
      // adding current test access code to testAccessCodes array
      testAccessCodes.push(assignedCandidates[i].test_access_code);
      // saving new sockets array and new testAccessCodes array in state
      this.setState(
        {
          sockets: sockets,
          testAccessCodes: testAccessCodes
        },
        () => {
          // setting socket on message functionality
          socket.onmessage = e => {
            // getting the action
            const action = JSON.parse(e.data).action;
            // conditional paths
            const conditionalPaths = [PATH.testAdministration];
            if (conditionalPaths.indexOf(window.location.pathname) >= 0) {
              if (
                action === CHANNELS_ACTION.readyToActive ||
                action === CHANNELS_ACTION.checkin ||
                action === CHANNELS_ACTION.finishOrQuit ||
                action === CHANNELS_ACTION.pauseTestTimeout
              ) {
                // updating TA's assigned candidates table
                this.updateAssignedCandidatesTable();
              }
            }
          };
        }
      );
    }
  };

  populateColumnFive = (i, response) => {
    return (
      <div>
        <StyledTooltip
          trigger={response[i].status === TEST_STATUS.ASSIGNED ? ["hover", "focus"] : []}
          placement="top"
          overlayClassName={"tooltip"}
          overlay={
            <div>
              <p>
                {LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.updateTestTimer}
              </p>
            </div>
          }
        >
          <div style={styles.allUnset}>
            <CustomButton
              label={<FontAwesomeIcon icon={faClock} />}
              action={() => {
                this.editCandidateTime(i);
              }}
              customStyle={
                response[i].status === TEST_STATUS.ASSIGNED
                  ? styles.actionButton
                  : { ...styles.actionButton, ...styles.disabledActionButton }
              }
              buttonTheme={THEME.SECONDARY}
              disabled={response[i].status === TEST_STATUS.ASSIGNED ? false : true}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.testAdministration.activeCandidates.table.updateTestTimerAriaLabel,
                response[i].candidate_first_name,
                `${response[i].candidate_last_name} (${response[i].username})`,
                response[i].candidate_dob,
                getFormattedTestStatus(Number(response[i].status)),
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedHours,
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedMinutes
              )}
            />
          </div>
        </StyledTooltip>
        <StyledTooltip
          trigger={response[i].status === TEST_STATUS.ASSIGNED ? ["hover", "focus"] : []}
          placement="top"
          overlayClassName={"tooltip"}
          overlay={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.approve}</p>
            </div>
          }
        >
          <div style={styles.allUnset}>
            <CustomButton
              label={
                <FontAwesomeIcon
                  icon={
                    response[i].status === TEST_STATUS.ASSIGNED ? regularFaThumbsUp : faThumbsUp
                  }
                />
              }
              action={() => {
                this.approveCandidate(i);
              }}
              customStyle={
                response[i].status === TEST_STATUS.ASSIGNED
                  ? styles.actionButton
                  : { ...styles.actionButton, ...styles.disabledActionButton }
              }
              buttonTheme={THEME.SECONDARY}
              disabled={response[i].status === TEST_STATUS.ASSIGNED ? false : true}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.testAdministration.activeCandidates.table.approveAriaLabel,
                response[i].candidate_first_name,
                `${response[i].candidate_last_name} (${response[i].username})`,
                response[i].candidate_dob,
                getFormattedTestStatus(Number(response[i].status)),
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedHours,
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedMinutes
              )}
            />
          </div>
        </StyledTooltip>
        <StyledTooltip
          trigger={response[i].status === TEST_STATUS.ASSIGNED ? ["hover", "focus"] : []}
          placement="top"
          overlayClassName={"tooltip"}
          overlay={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.unAssign}</p>
            </div>
          }
        >
          <div style={styles.allUnset}>
            <CustomButton
              label={<FontAwesomeIcon icon={faTrashAlt} />}
              action={() => {
                this.unAssignCandidate(i);
              }}
              customStyle={
                response[i].status === TEST_STATUS.ASSIGNED
                  ? styles.actionButton
                  : { ...styles.actionButton, ...styles.disabledActionButton }
              }
              buttonTheme={THEME.SECONDARY}
              disabled={response[i].status === TEST_STATUS.ASSIGNED ? false : true}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.testAdministration.activeCandidates.table.unAssignAriaLabel,
                response[i].candidate_first_name,
                `${response[i].candidate_last_name} (${response[i].username})`,
                response[i].candidate_dob,
                getFormattedTestStatus(Number(response[i].status)),
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedHours,
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedMinutes
              )}
            />
          </div>
        </StyledTooltip>
        <StyledTooltip
          trigger={response[i].status === TEST_STATUS.ASSIGNED ? [] : ["hover", "focus"]}
          placement="top"
          overlayClassName={"tooltip"}
          overlay={
            <div>
              <p>
                {response[i].status === TEST_STATUS.LOCKED ||
                response[i].status === TEST_STATUS.PAUSED
                  ? LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.unlockUnpause
                  : LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.lockPause}
              </p>
            </div>
          }
        >
          <div style={styles.allUnset}>
            <CustomButton
              label={
                <FontAwesomeIcon
                  icon={
                    response[i].status === TEST_STATUS.LOCKED ||
                    response[i].status === TEST_STATUS.PAUSED
                      ? faLockOpen
                      : faLock
                  }
                />
              }
              action={() => {
                this.lockUnlockPauseUnpauseCandidate(i);
              }}
              customStyle={
                response[i].status === TEST_STATUS.ASSIGNED
                  ? { ...styles.actionButton, ...styles.disabledActionButton }
                  : styles.actionButton
              }
              buttonTheme={THEME.SECONDARY}
              disabled={response[i].status === TEST_STATUS.ASSIGNED ? true : false}
              ariaLabel={LOCALIZE.formatString(
                response[i].status === TEST_STATUS.LOCKED ||
                  response[i].status === TEST_STATUS.PAUSED
                  ? LOCALIZE.testAdministration.activeCandidates.table.unlockUnpauseAriaLabel
                  : LOCALIZE.testAdministration.activeCandidates.table.lockPauseAriaLabel,
                response[i].candidate_first_name,
                `${response[i].candidate_last_name} (${response[i].username})`,
                response[i].candidate_dob,
                getFormattedTestStatus(Number(response[i].status)),
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedHours,
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedMinutes
              )}
            />
          </div>
        </StyledTooltip>
        <StyledTooltip
          trigger={["hover", "focus"]}
          placement="top"
          overlayClassName={"tooltip"}
          overlay={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.reSync}</p>
            </div>
          }
        >
          <div style={styles.allUnset}>
            <CustomButton
              label={<FontAwesomeIcon icon={faSyncAlt} />}
              action={() => {
                this.reSyncCandidate(i);
              }}
              customStyle={styles.actionButton}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.testAdministration.activeCandidates.table.reSyncAriaLabel,
                response[i].candidate_first_name,
                `${response[i].candidate_last_name} (${response[i].username})`,
                response[i].candidate_dob,
                getFormattedTestStatus(Number(response[i].status)),
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedHours,
                getTimeInHoursMinutes(Number(response[i].test_time)).formattedMinutes
              )}
            />
          </div>
        </StyledTooltip>
        <StyledTooltip
          trigger={["hover", "focus"]}
          placement="top"
          overlayClassName={"tooltip"}
          overlay={
            <div>
              <p>{LOCALIZE.testAdministration.activeCandidates.table.actionTooltips.report}</p>
            </div>
          }
        >
          <div style={styles.allUnset}>
            <CustomButton
              label={<FontAwesomeIcon icon={faEdit} />}
              action={() => {
                this.reportCandidate(i);
              }}
              customStyle={styles.actionButton}
              buttonTheme={THEME.SECONDARY}
              ariaLabel={LOCALIZE.formatString(
                LOCALIZE.testAdministration.activeCandidates.table.reportAriaLabel,
                response[i].candidate_first_name,
                `${response[i].candidate_last_name} (${response[i].username})`
              )}
            />
          </div>
        </StyledTooltip>
      </div>
    );
  };

  // getting total test time based on all timed test sections
  getTotalTestTime = assignedTestSections => {
    // initializing totalTestTime
    let totalTestTime = 0;
    // looping in all assigned test sections
    for (let i = 0; i < assignedTestSections.length; i++) {
      // test_section_time is not null
      if (assignedTestSections[i].test_section_time !== null) {
        // incrementing totalTestTime
        totalTestTime += assignedTestSections[i].test_section_time;
      }
    }
    // returning formatted test time
    return `${getTimeInHoursMinutes(Number(totalTestTime)).formattedHours} : ${
      getTimeInHoursMinutes(Number(totalTestTime)).formattedMinutes
    }`;
  };

  // edit candidate time
  editCandidateTime = id => {
    this.setState({
      triggerShowEditTimePopup: !this.state.triggerShowEditTimePopup,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[id]
    });
  };

  // approve candidate
  approveCandidate = id => {
    this.setState({
      triggerApproveCandidate: !this.state.triggerApproveCandidate,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[id]
    });
  };

  // un-assign candidate's test
  unAssignCandidate = id => {
    this.setState({
      triggerUnAssignCandidate: !this.state.triggerUnAssignCandidate,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[id]
    });
  };

  // lock/unlock & pause/unpause candidate
  lockUnlockPauseUnpauseCandidate = id => {
    // test is locked or paused
    if (
      this.state.testAdministratorAssignedCandidates[id].status === TEST_STATUS.LOCKED ||
      this.state.testAdministratorAssignedCandidates[id].status === TEST_STATUS.PAUSED
    ) {
      this.setState({
        triggerShowUnlockUnpausePopup: !this.state.triggerShowUnlockUnpausePopup,
        selectedCandidateData: this.state.testAdministratorAssignedCandidates[id]
      });
      // test is active
    } else {
      this.setState({
        triggerShowLockPausePopup: !this.state.triggerShowLockPausePopup,
        selectedCandidateData: this.state.testAdministratorAssignedCandidates[id]
      });
    }
  };

  // re-sync candidate (socket)
  reSyncCandidate = id => {
    this.setState({
      triggerReSyncCandidate: !this.state.triggerReSyncCandidate,
      selectedCandidateData: this.state.testAdministratorAssignedCandidates[id]
    });
  };

  // TODO (fnormand): implement report candidate functionality
  reportCandidate = id => {
    console.log("Report candidate #", id);
  };

  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getTestAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.testAccessCodes,
        body: (
          <TestAccessCodes
            activeTestAccessCodes={this.state.activeTestAccessCodes}
            activeTestAccessCodesLoading={this.state.activeTestAccessCodesLoading}
            getActiveTestAccessCodes={this.getActiveTestAccessCodes}
            testOrderNumberOptions={this.state.testOrderNumberOptions}
            populateTestToAdministerOptions={this.populateTestToAdministerOptions}
            testToAdministerOptions={this.state.testToAdministerOptions}
            populateTestSessionLanguageOptions={this.populateTestSessionLanguageOptions}
            testSessionLanguageOptions={this.state.testSessionLanguageOptions}
          />
        )
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.activeCandidates,
        body: (
          <ActiveCandidates
            testAdministratorAssignedCandidates={this.state.testAdministratorAssignedCandidates}
            rowsDefinition={this.state.rowsDefinition}
            currentlyLoading={this.state.currentlyLoading}
            triggerShowEditTimePopup={this.state.triggerShowEditTimePopup}
            triggerShowLockPausePopup={this.state.triggerShowLockPausePopup}
            triggerShowUnlockUnpausePopup={this.state.triggerShowUnlockUnpausePopup}
            triggerApproveCandidate={this.state.triggerApproveCandidate}
            triggerUnAssignCandidate={this.state.triggerUnAssignCandidate}
            triggerReSyncCandidate={this.state.triggerReSyncCandidate}
            updateAssignedCandidatesTable={this.updateAssignedCandidatesTable}
            selectedCandidateData={this.state.selectedCandidateData}
            populateTestAdministratorAssignedCandidates={
              this.populateTestAdministratorAssignedCandidates
            }
            sockets={this.state.sockets}
            triggerReRender={this.state.triggerReRender}
          />
        )
      },
      {
        menuString: LOCALIZE.testAdministration.sideNavItems.reports,
        body: <Reports />
      }
    ];
  };

  updateAssignedCandidatesTable = () => {
    // initializing table data
    let testAdministratorAssignedCandidates = [];
    let rowsDefinition = this.state.rowsDefinition;
    rowsDefinition.data = [];
    // getting assigned candidates
    this.props.getTestAdministratorAssignedCandidates().then(response => {
      // populating table data
      for (let i = 0; i < response.length; i++) {
        testAdministratorAssignedCandidates.push(response[i]);
        rowsDefinition.data.push({
          column_1: `${response[i].candidate_last_name}, ${response[i].candidate_first_name} (${response[i].username})`,
          column_2: response[i].candidate_dob,
          column_3: getFormattedTestStatus(Number(response[i].status)),
          column_4: this.getTotalTestTime(response[i].assigned_test_sections),
          column_5: this.populateColumnFive(i, response)
        });
      }
      // saving new data in state + triggering re-render
      this.setState({
        rowsDefinition: rowsDefinition,
        testAdministratorAssignedCandidates: testAdministratorAssignedCandidates,
        triggerReRender: !this.state.triggerReRender
      });
    });
  };

  render() {
    const specs = this.getTestAdministrationSections();
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.testAdministration}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={SystemAdministrationStyles.header}
              tabIndex={0}
              aria-labelledby="user-welcome-message"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.testAdministration.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                <div>
                  <label style={SystemAdministrationStyles.sectionContainerLabel}>
                    {LOCALIZE.testAdministration.containerLabel}
                    <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={SystemAdministrationStyles.sectionContainer}>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={SystemAdministrationStyles.tabContainer}
                  tabContentStyle={SystemAdministrationStyles.tabContent}
                  navStyle={SystemAdministrationStyles.nav}
                  bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { TestAdministration as UnconnectedTestAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      getActiveTestAccessCodesRedux,
      getTestPermissions,
      getTestAdministratorAssignedCandidates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestAdministration);
