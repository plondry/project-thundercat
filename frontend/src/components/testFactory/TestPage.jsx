import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { setLanguage } from "../../modules/LocalizeRedux";
import TestSectionComponentFactory from "./TestSectionComponentFactory";
import { triggerTimeSpentCalculation } from "../../modules/QuestionListRedux";
import { BANNER_STYLE, TEST_STYLE } from "../eMIB/constants";
import {
  getTestSection,
  setTestHeight,
  updateTestSection,
  setStartTime,
  setContentLoaded,
  setContentUnLoaded,
  resetTestFactoryState,
  updateCheatingAttempts,
  getUpdatedTestStartTime
} from "../../modules/TestSectionRedux";
import { resetAssignedTestState } from "../../modules/AssignedTestsRedux";
import { resetInboxState } from "../../modules/EmailInboxRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { getAssignedTests } from "../../modules/AssignedTestsRedux";
import scorescoreUitTestSection, { scoreUitTest } from "../../modules/ScoringRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { NextSectionButtonType, ReducerType, TestSectionType } from "./Constants";
import { Helmet } from "react-helmet";
import TestNavBar from "../../TestNavBar";
import { deactivateTest, quitTest } from "../../modules/TestStatusRedux";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import { PATH } from "../../components/commons/Constants";
import LOCALIZE from "../../text_resources";
import LockScreen from "../commons/LockScreen";
import PauseScreen from "../commons/PauseScreen";
import { TEST_STATUS } from "../ta/Constants";
import { getCurrentSocket, CHANNELS_ACTION } from "../../helpers/testSessionChannels";

const styles = {
  container: {
    maxWidth: 1400,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto",
    textAlign: "left"
  }
};

class TestPage extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      setLanguage: PropTypes.func,
      currentLanguage: PropTypes.string,
      sampleTest: PropTypes.bool,
      sockets: PropTypes.array,
      testAccessCode: PropTypes.string
    };

    TestPage.defaultProps = {
      sampleTest: false
    };
  }

  state = {
    showCheatingPopup: false
  };

  componentDidMount = () => {
    window.addEventListener("beforeunload", this.onUnload);
    // window.addEventListener("popstate", e => {
    //   // blocking back button action (back button will simply redirect to current page)
    //   history.go(1);
    // });
    document.addEventListener("visibilitychange", this.visibilityChange);
    //get the test section from the parent page
    if (!this.props.testSectionLoaded && !this.props.isTestLocked && !this.props.isTestPaused) {
      this.getTestSection(this.props.assignedTestId, 0, "", this.props.testId);
    } else if (this.props.testSectionLoaded && this.props.assignedTestId !== null) {
      this.getUpdatedTestStartTime(this.props.assignedTestId, this.props.testSection.id);
    }
  };

  getTestSection = (assignedTestId, order = 0, specialSection = "", testId) => {
    let isTimedSection = false;
    this.props
      .getTestSection(assignedTestId, order, specialSection, testId)
      .then(response => {
        if (response.error) {
          return;
        }
        this.props.setContentUnLoaded();

        if (response.redux) {
          this.reduxStore(response.redux);
        }

        // initializing start time if the current section is a timed section
        if (this.isTimedSection(response.default_time)) {
          isTimedSection = true;
          // store the start time in redux
          if (!response.start_time) {
            this.props.setStartTime(Date());
          } else {
            this.props.setStartTime(new Date(response.start_time).toString());
          }
        }

        this.props.setTestHeight(this.props.isTestActive ? TEST_STYLE : BANNER_STYLE);
        this.props.updateTestSection(response);
        this.props.setContentLoaded();

        // checking if that is the last test section (FINISH or QUIT)
        if (
          (response.section_type === TestSectionType.FINISH ||
            response.section_type === TestSectionType.QUIT) &&
          this.props.assignedTestId
        ) {
          this.sendFinishOrQuitMessageToTa();
        }
      })
      .then(() => {
        // current section is a timed section
        if (isTimedSection && this.props.assignedTestId !== null) {
          this.getUpdatedTestStartTime(assignedTestId, this.props.testSection.id);
        }
      });
  };

  // sending message to TA on finish/quit action (from candidate)
  sendFinishOrQuitMessageToTa = () => {
    // getting current socket + pushing it to sockets array
    this.props.getAssignedTests(this.props.assignedTestId).then(response => {
      // getting needed parameters
      const paramsObj = {
        username_id: response[0].username,
        test_id: response[0].test.id
      };

      // getting current socket
      getCurrentSocket(this.props.sockets, response[0].test_access_code).then(currentSocket => {
        // sending message to TA
        currentSocket.send(
          JSON.stringify({
            action: CHANNELS_ACTION.finishOrQuit,
            parameters: JSON.stringify(paramsObj),
            test_access_code: response[0].test_access_code
          })
        );
      });
    });
  };

  handleNext = () => {
    this.getTestSection(
      this.props.assignedTestId,
      this.props.testSection.order,
      "",
      this.props.testId
    );
  };

  handleFinishTest = () => {
    this.props.deactivateTest();
    // triggering time spent calculation, so the time spent of the last question selected will be calculated on submit action
    this.props.triggerTimeSpentCalculation(!this.props.timeSpentTrigger);
    window.location.href = PATH.dashboard;
  };

  // getting updated test start time (including lock/pause time calculation) + setting start time redux state
  getUpdatedTestStartTime = (assignedTestId, testSectionId) => {
    this.props.getUpdatedTestStartTime(assignedTestId, testSectionId).then(response => {
      if (!response.info && !response.error) {
        // overriding start time in redux if there are lock/pause actions (timed sections only)
        this.props.setStartTime(response);
      }
    });
  };

  isTimedSection = defaultTime => {
    if (defaultTime || defaultTime === 0) return true;
    return false;
  };

  reduxStore = reducers => {
    reducers.forEach(reducer => {
      switch (reducer) {
        case ReducerType.INBOX:
          this.props.resetInboxState();
          break;
        case ReducerType.QUESTION_LIST:
          this.props.resetQuestionListState();
          break;
        case ReducerType.NOTEPAD:
          this.props.resetNotepadState();
          break;
        default:
          break;
      }
    });
  };

  // resetting all needed redux states
  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetInboxState();
    this.props.resetNotepadState();
    this.props.resetAssignedTestState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  onUnload = e => {
    // if there is no NextSectionButtonType (it means that this is the last page, so the submit or quit test page)
    if (this.props.testSection.next_section_button_type === NextSectionButtonType.NONE) {
      // deactivating test
      this.props.deactivateTest();
      // resetting redux states
      this.resetAllRedux();
      // removing session storage (TEST_ACTIVE)
      SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);
      // redirecting to dashboard
      this.props.history.push(PATH.dashboard);
    }
  };

  visibilityChange = () => {
    if (this.props.testSection.block_cheating) {
      if (!this.state.showCheatingPopup) {
        this.openCheatingPopup();
      }
    }
  };

  openCheatingPopup = () => {
    this.setState({ showCheatingPopup: true });
    // preventing unwanted cheatingAttempts updates
    if (this.props.assignedTestId !== null) {
      this.props.updateCheatingAttempts(Number(this.props.cheatingAttempts) + 1);
    }
  };
  closeCheatingPopup = () => {
    this.setState({ showCheatingPopup: false });
  };

  timeout = () => {
    this.handleNext();
  };

  componentWillUnmount = () => {
    window.removeEventListener("beforeunload", this.onUnload);
    window.removeEventListener("popstate", this.onUnload);
    document.removeEventListener("visibilitychange", this.visibilityChange);
  };

  handleQuitTest = () => {
    this.props.quitTest();
    this.getTestSection(
      this.props.assignedTestId,
      this.props.testSection.order,
      "quit",
      this.props.testId
    );
  };

  render() {
    if (!this.props.testSectionLoaded && !this.props.isTestLocked && !this.props.isTestPaused) {
      return <h2 style={{ width: "900px", margin: "auto", paddingTop: "20px" }}>loading</h2>;
    }
    const { testSection } = this.props;
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.uitTest}</title>
        </Helmet>
        <TestNavBar
          updateBackendTest={() => {}}
          handleQuitTest={() => this.handleQuitTest()}
          quitTestHidden={
            this.props.previousStatus === TEST_STATUS.READY ||
            this.props.testSection.next_section_button_type === NextSectionButtonType.NONE
          }
        />
        <div style={styles.container}>
          {this.props.isTestLocked ? (
            <LockScreen />
          ) : this.props.isTestPaused ? (
            <PauseScreen
              sockets={this.props.sockets}
              testId={this.props.testId}
              assignedTestId={this.props.assignedTestId}
              previousStatus={this.props.previousStatus}
              testAccessCode={this.props.testAccessCode}
            />
          ) : (
            <TestSectionComponentFactory
              testSection={testSection.localize[this.props.currentLanguage]}
              handleNext={this.handleNext}
              handleFinishTest={this.handleFinishTest}
              timeLimit={testSection.default_time}
              testSectionStartTime={this.props.testSectionStartTime}
              timeout={this.timeout}
              testSectionType={testSection.section_type}
              resetAllRedux={this.resetAllRedux}
              defaultTab={testSection.default_tab}
              usesNotepad={testSection.uses_notepad}
            />
          )}
        </div>
        <PopupBox
          show={this.state.showCheatingPopup && this.props.testSection.block_cheating}
          handleClose={this.closeCheatingPopup}
          title={LOCALIZE.testBuilder.errors.cheatingPopup.title}
          description={
            <div>
              <p>{LOCALIZE.testBuilder.errors.cheatingPopup.description}</p>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.testBuilder.errors.cheatingPopup.warning,
                  3 - this.props.cheatingAttempts
                )}
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={"I understand"}
          rightButtonAction={this.closeCheatingPopup}
        />
      </div>
    );
  }
}

export { TestPage as UnconnectedTestPage };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    testSectionLoaded: state.testSection.isLoaded,
    testSectionStartTime: state.testSection.startTime,
    cheatingAttempts: state.testSection.cheatingAttempts,
    timeSpentTrigger: state.questionList.timeSpentTrigger,
    testId: state.assignedTest.testId,
    isTestActive: state.testStatus.isTestActive,
    isTestLocked: state.testStatus.isTestLocked,
    isTestPaused: state.testStatus.isTestPaused,
    previousStatus: state.testStatus.previousStatus,
    username: state.user.username
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage,
      getTestSection,
      setTestHeight,
      updateTestSection,
      setContentLoaded,
      setContentUnLoaded,
      setStartTime,
      triggerTimeSpentCalculation,
      resetTestFactoryState,
      resetAssignedTestState,
      resetInboxState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      scorescoreUitTestSection,
      scoreUitTest,
      updateCheatingAttempts,
      deactivateTest,
      quitTest,
      getUpdatedTestStartTime,
      getAssignedTests
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TestPage));
