import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import "../../css/inbox.css";
import {
  readQuestion,
  changeCurrentQuestion,
  answerQuestion,
  markForReview,
  updateTimeSpent,
  setStartTime
} from "../../modules/QuestionListRedux";
import { loadInboxAnswers } from "../../modules/EmailInboxRedux";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { saveUitTestResponse } from "../../modules/UpdateResponseRedux";
import { getTimeInSecondsBetweenTwoDates } from "../../helpers/timeConversion";
import { seenUitQuestion, getTestResponses } from "../../modules/UpdateResponseRedux";
import { loadAnswers } from "../../modules/QuestionListRedux";
import QuestionPreviewFactory from "./QuestionPreviewFactory";
import QuestionContentFactory from "./QuestionContentFactory";
import { TestSectionComponentType } from "./Constants";

class QuestionList extends Component {
  static propTypes = {
    isMain: PropTypes.bool
  };

  state = {
    previousQuestionId: null,
    seenQuestionLoaded: false
  };

  //temporary comment to see effectiveness over time
  // INBOX_HEIGHT = `calc(100vh - ${this.props.headerFooterPX}px)`;
  INBOX_HEIGHT = `800px`;

  styles = {
    rowContainer: {
      margin: 0
    },
    bodyContent: {
      minHeight: this.INBOX_HEIGHT,
      overflow: "auto"
    },
    contentColumn: {
      paddingLeft: 0,
      overflowY: "auto",
      zIndex: 1,
      paddingRight: 0
    },
    emailContainer: {
      overflow: "hidden",
      paddingLeft: 0,
      paddingRight: 0
    },
    navIntemContainer: {
      width: "100%",
      minHeight: this.INBOX_HEIGHT,
      overflow: "auto"
    },
    navItem: {
      width: "100%"
    },
    navLink: {
      padding: 0
    },
    hiddenText: {
      position: "absolute",
      left: -10000,
      top: "auto",
      width: 1,
      height: 1,
      overflow: "hidden"
    }
  };

  componentDidMount = () => {
    this.changeQuestion(this.props.currentQuestion || this.props.defaultActiveKey);
    if (this.props.assignedTestId) {
      this.props
        .getTestResponses(this.props.assignedTestId, this.props.testSectionComponentId)
        .then(response => {
          if (response.ok) {
            this.loadAnswers(response);
          } else {
            // throw new Error("could not fetch test responses");
          }
        });
    }
    // initializing startTime
    this.props.setStartTime(new Date(Date.now()).toString());
  };

  loadAnswers = response => {
    switch (this.props.componentType) {
      case TestSectionComponentType.QUESTION_LIST:
        this.props.loadAnswers(response.body[0].answers);
        break;
      case TestSectionComponentType.INBOX:
        this.props.loadInboxAnswers(response.body[0].answers);
        break;
      default:
        break;
    }
  };

  componentDidUpdate = (prevProps, prevState) => {
    // if triggerTimeSpentCalculation gets updated
    if (prevProps.timeSpentTrigger !== this.props.timeSpentTrigger) {
      // updating time spent of the current question
      this.updateTimeSpent();
    }
    // if seenQuestionLoaded gets updated
    if (prevState.seenQuestionLoaded !== this.state.seenQuestionLoaded) {
      // getting test responses
      if (this.props.assignedTestId) {
        this.props
          .getTestResponses(this.props.assignedTestId, this.props.testSectionComponentId)
          .then(response => {
            // preventing errors (this condition should always be true)
            if (response.ok) {
              this.loadAnswers(response);
              // should never happen
            } else {
              // throw new Error("Something went wrong during getting answers process");
            }
          });
      }
    }
  };

  changeQuestion = eventKey => {
    this.props.readQuestion(eventKey);
    this.props.changeCurrentQuestion(eventKey);

    //scroll question to top
    let main = document.getElementById("main-content");
    let content = document.getElementById("email-content");
    if (main !== null) {
      main.scrollTop = 0;
    }
    if (content !== null) {
      content.scrollTop = 0;
    }

    // adding current question as seen with empty answer (new row in CandidateAnswers and CandidateMultipleChoiceAnswers tables)
    if (this.props.assignedTestId) {
      this.props.seenUitQuestion(this.props.assignedTestId, eventKey);
    }

    // updating previous question's time spent value
    if (this.state.previousQuestionId !== null && this.props.assignedTestId) {
      this.updateTimeSpent();
    }

    // setting previousQuestionId state
    this.setState({ previousQuestionId: eventKey, seenQuestionLoaded: true });
  };

  // updating time spent
  updateTimeSpent = () => {
    let timeSpent = 0;
    const assignedTestId = this.props.assignedTestId;
    const questionId = this.state.previousQuestionId;
    const testSectionComponentId = this.props.testSectionComponentId;
    // getting current time
    const currentTime = new Date(Date.now()).toString();
    // calculating the time spent + adding current time spent from the DB
    timeSpent = getTimeInSecondsBetweenTwoDates(this.props.startTime, currentTime);
    // updating current time spent of previous question
    this.props
      .updateTimeSpent(assignedTestId, questionId, testSectionComponentId, timeSpent)
      .then(() => {
        // updating start time redux state with updated current time
        this.props.setStartTime(currentTime);
      });
  };

  handleAnswerClick = (answerId, questionId) => {
    const assignedTestId = this.props.assignedTestId;
    this.props.answerQuestion(questionId, answerId);
    if (this.props.assignedTestId) {
      this.props.saveUitTestResponse(assignedTestId, questionId, answerId);
    }
  };

  markForReview = questionId => {
    this.props.markForReview(questionId);
  };

  render() {
    let { questions, isMain, questionSummaries } = this.props;

    if (typeof questionSummaries === "undefined") return null;

    //sort questions into email id order
    if (this.props.componentType === TestSectionComponentType.INBOX) {
      questions.sort((a, b) => (a.email.email_id > b.email.email_id ? 1 : -1));
    }
    return (
      <div id="question-list-tabs">
        <Tab.Container
          id="unit-test-question-list-tabs"
          defaultActiveKey={this.props.currentQuestion || this.props.defaultActiveKey}
          activeKey={this.props.currentQuestion || this.props.defaultActiveKey}
          onSelect={this.changeQuestion}
        >
          <Row style={this.styles.rowContainer}>
            <Col
              role="region"
              aria-label={LOCALIZE.ariaLabel.questionList}
              sm={3}
              style={this.styles.emailContainer}
            >
              <Nav role="navigation" className="flex-column">
                <div style={this.styles.navIntemContainer}>
                  {questions.map((question, index) => (
                    <Nav.Item key={question.id} style={this.styles.navItem}>
                      <Nav.Link role="tab" eventKey={question.id} style={this.styles.navLink}>
                        <div id={`question-tab-${index}`}>
                          <QuestionPreviewFactory
                            index={index}
                            question={question}
                            questionId={question.id}
                            currentQuestion={this.props.currentQuestion}
                            type={this.props.questionType}
                            isSelected={Number(question.id) === Number(this.props.currentQuestion)}
                          />
                          <span style={this.styles.hiddenText}>
                            {/* {LOCALIZE.emibTest.inboxPage.tabName} */}
                          </span>
                        </div>
                      </Nav.Link>
                    </Nav.Item>
                  ))}
                </div>
              </Nav>
            </Col>
            <Col
              id={isMain ? "main-content" : "email-content"}
              sm={8}
              tabIndex={0}
              style={this.styles.contentColumn}
              role="main"
            >
              <Tab.Content style={this.styles.bodyContent}>
                {questions.map((question, questionIndex) => (
                  <Tab.Pane eventKey={question.id} key={question.id}>
                    <div id={`question-content-${questionIndex}`}>
                      <QuestionContentFactory
                        question={question}
                        questionId={question.id}
                        index={questionIndex}
                        questionIndex={questionIndex}
                        questions={questions}
                        changeQuestion={this.changeQuestion}
                        handleAnswerClick={this.handleAnswerClick}
                        markForReview={this.markForReview}
                        type={this.props.questionType}
                        addressBook={this.props.addressBook}
                      />
                    </div>
                  </Tab.Pane>
                ))}
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    );
  }
}

export { QuestionList as UnconnectedQuestionList };
const mapStateToProps = (state, ownProps) => {
  var currentQuestion =
    typeof state.questionList.currentQuestion === "undefined"
      ? ownProps.currentQuestion
      : state.questionList.currentQuestion;
  return {
    questionSummaries: state.questionList.questionSummaries,
    currentQuestion: currentQuestion,
    assignedTestId: state.assignedTest.assignedTestId,
    timeSpentTrigger: state.questionList.timeSpentTrigger,
    startTime: state.questionList.startTime,
    readOnly: state.testSection.readOnly
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      readQuestion,
      changeCurrentQuestion,
      answerQuestion,
      markForReview,
      saveUitTestResponse,
      updateTimeSpent,
      getTestResponses,
      loadAnswers,
      setStartTime,
      seenUitQuestion,
      loadInboxAnswers
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuestionList);
