import React, { Component } from "react";
import ReactMarkdown from "react-markdown";
import LOCALIZE from "../../text_resources";
import { QuestionSectionType } from "./Constants";

class QuestionFactory extends Component {
  componentDidMount = () => {};

  factory = questionSections => {
    return (
      <div id="unit-test-question-content">
        {questionSections.map((section, index) => {
          switch (section.question_section_type) {
            case QuestionSectionType.MARKDOWN:
              return (
                <div key={index} id="unit-test-question-content-markdown">
                  <ReactMarkdown source={section.question_section_content.content} />
                </div>
              );

            case QuestionSectionType.END:
              return (
                <>
                  <h2
                    style={{
                      position: "absolute",
                      left: -10000,
                      top: "auto",
                      width: 1,
                      height: 1,
                      overflow: "hidden"
                    }}
                  >
                    Question and Answers
                  </h2>
                  <ReactMarkdown source={section.question_section_content.content} />
                </>
              );
            default:
              return (
                <div key={index} id="unit-test-question-content-undefined-section">
                  <h1>Unsupported Question Section Type: {section.question_section_type}</h1>
                </div>
              );
          }
        })}
      </div>
    );
  };
  render() {
    const { questionSections } = this.props;
    return (
      <>
        {/* add one to index since it starts at 0 */}
        <h2 id={`question-${this.props.index}`}>
          {LOCALIZE.formatString(LOCALIZE.mcTest.questionList.questionId, this.props.index + 1)}
        </h2>
        {this.factory(questionSections)}
      </>
    );
  }
}

export default QuestionFactory;
