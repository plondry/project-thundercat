import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { QuestionType } from "./Constants";
import QuestionPreview from "./QuestionPreview";
import EmailPreview from "../eMIB/EmailPreview";

class QuestionPreviewFactory extends Component {
  static propTypes = {
    question: PropTypes.object
  };

  render() {
    const {
      index,
      question,
      questionId,
      isAnswered,
      isRead,
      isSelected,
      isMarkedForReview
    } = this.props;

    switch (this.props.type) {
      case QuestionType.MULTIPLE_CHOICE:
        return (
          <div id="unit-test-multiple-choice-question-preview-type">
            <QuestionPreview
              index={index}
              questionId={questionId}
              isAnswered={isAnswered}
              isMarkedForReview={isMarkedForReview}
              isRead={isRead}
              isSelected={isSelected}
            />
          </div>
        );

      case QuestionType.EMAIL:
        const isRepliedTo =
          this.props.emailResponses.length === 0 && this.props.taskResponses.length === 0
            ? false
            : true;
        return (
          <div id="unit-test-email-question-preview-type">
            <EmailPreview
              email={question.email}
              isAnswered={isAnswered}
              isRead={isRead}
              isSelected={isSelected}
              isRepliedTo={isRepliedTo}
            />
          </div>
        );

      default:
        return (
          <div id="unit-test-unsupported-question-preview-type">
            <h1>Unsupported Question Type: {this.props.type}</h1>
          </div>
        );
    }
  }
}

export { QuestionPreviewFactory as UnconnectedQuestionPreviewFactory };
const mapStateToProps = (state, ownProps) => {
  let isRead = false;
  let isAnswered = false;
  let isMarkedForReview = false;
  if (typeof state.questionList.questionSummaries[`${ownProps.questionId}`] !== "undefined") {
    isRead = state.questionList.questionSummaries[`${ownProps.questionId}`].isRead;
    isAnswered =
      state.questionList.questionSummaries[`${ownProps.questionId}`].answer > 0 ? true : false;
    isMarkedForReview =
      state.questionList.questionSummaries[`${ownProps.questionId}`].review || false;
  }
  return {
    currentLanguage: state.localize.language,
    testContentHeight: state.testSection.testHeight,
    testSectionId: state.testSection.testSection.id,
    questionSummaries: state.questionList.questionSummaries,
    emailResponses: [...state.emailInbox.emailResponses].filter(
      x => x.questionId === ownProps.question.id
    ),
    taskResponses: [...state.emailInbox.taskResponses].filter(
      x => x.questionId === ownProps.question.id
    ),
    isRead: isRead,
    isAnswered: isAnswered,
    isMarkedForReview: isMarkedForReview
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionPreviewFactory);
