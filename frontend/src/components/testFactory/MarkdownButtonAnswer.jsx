import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ReactMarkdown from "react-markdown";
import { Row, Col } from "react-bootstrap";
import "../../css/MarkdownButtonAnswer.css";
const styles = {
  button: {
    width: "90%",
    paddingLeft: "15px",
    textAlign: "left",
    whiteSpace: "normal"
  },
  buttonContainer: {
    width: "100%",
    padding: "5px 5px 5px 5px"
  }
};

class MarkdownButtonAnswer extends Component {
  static propTypes = {
    answer: PropTypes.object
  };

  componentDidMount = () => {};

  render() {
    const { answer, answerId, questionId, currentAnswer } = this.props;
    return (
      <div style={styles.buttonContainer}>
        <label style={{ width: "100%" }} className="radio-container">
          <Row>
            <Col xs={"1"}>
              <input
                type={"radio"}
                checked={currentAnswer === answerId}
                style={styles.button}
                onChange={() => this.props.handleClick(answerId, questionId)}
                id="unit-test-markdown-button-answer-input"
              />
              <span className="checkmark"></span>
            </Col>
            <Col id="unit-test-markdown-button-answer-text">
              <ReactMarkdown source={answer.content} />
            </Col>
          </Row>
        </label>
      </div>
    );
  }
}

export { MarkdownButtonAnswer as UnconnectedMarkdownButtonAnswer };
const mapStateToProps = (state, ownProps) => {
  let currentAnswer = -1;
  if (typeof state.questionList.questionSummaries[`${ownProps.questionId}`] !== "undefined")
    currentAnswer = state.questionList.questionSummaries[`${ownProps.questionId}`].answer;
  return {
    currentLanguage: state.localize.language,
    currentAnswer: currentAnswer
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MarkdownButtonAnswer);
