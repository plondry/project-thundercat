import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { QuestionType } from "./Constants";
import QuestionFactory from "./QuestionFactory";
import MarkdownButtonAnswer from "./MarkdownButtonAnswer";
import NextPreviousButtonNav from "../commons/NextPreviousButtonNav";
import Email from "../eMIB/Email";
import { markForReview } from "../../modules/QuestionListRedux";
import QuestionContentScorerHeaderFactory from "./QuestionContentScorerHeaderFactory";
import { updateRatings } from "../../modules/ScorerRedux";

const styles = {
  mcPadding: {
    padding: 16
  }
};
class QuestionContentFactory extends Component {
  static propTypes = {
    question: PropTypes.object,
    changeQuestion: PropTypes.func
  };

  componentDidMount = () => {
    // scorer mode
    if (this.props.readOnly) {
      this.props.updateRatings(this.props.question);
    }
  };

  render() {
    return (
      <>
        <QuestionContentScorerHeaderFactory {...this.props} />
        {this.contentFactory()}
      </>
    );
  }
  contentFactory = () => {
    const { question, questions, questionIndex, questionId } = this.props;
    switch (this.props.type) {
      case QuestionType.MULTIPLE_CHOICE:
        return (
          <div id="unit-test-question-content-factory-multiple-choice" style={styles.mcPadding}>
            <QuestionFactory questionSections={question.sections} index={questionIndex} />
            {/* this could be turned into a factory for different answer types
       but for now, only markdown multiple choice */}
            {question.answers.map((answer, answerIndex) => (
              <div key={answerIndex}>
                <MarkdownButtonAnswer
                  key={answerIndex}
                  answer={answer}
                  answerId={answer.id}
                  questionId={question.id}
                  handleClick={this.props.handleAnswerClick}
                />
              </div>
            ))}
            {/* next button mark for review and prev button */}
            <hr></hr>
            <NextPreviousButtonNav
              showNext={typeof questions[questionIndex + 1] !== "undefined"}
              showPrevious={typeof questions[questionIndex - 1] !== "undefined"}
              showReview={true}
              isMarkedForReview={this.props.isMarkedForReview}
              questionId={questionId}
              onChangeToPrevious={() => {
                this.props.changeQuestion(
                  typeof questions[questionIndex - 1] !== "undefined"
                    ? questions[questionIndex - 1].id
                    : undefined
                );
              }}
              onChangeToNext={() => {
                this.props.changeQuestion(
                  typeof questions[questionIndex + 1] !== "undefined"
                    ? questions[questionIndex + 1].id
                    : undefined
                );
              }}
              onMarkForReview={() => {
                this.props.markForReview(question.id);
              }}
            />
          </div>
        );

      case QuestionType.EMAIL:
        return (
          <div id="unit-test-question-content-factory-email">
            <Email
              questionId={question.id}
              email={question.email}
              emailResponses={this.props.emailResponses}
              taskResponses={this.props.taskResponses}
              emailCount={this.props.emailResponses.length}
              taskCount={this.props.taskResponses.length}
              addressBook={this.props.addressBook}
              disabled={this.props.readOnly}
            />
          </div>
        );

      default:
        return <h1>Unsupported Question Type: {this.props.type}</h1>;
    }
  };
}

export { QuestionContentFactory as UnconnectedQuestionContentFactory };
const mapStateToProps = (state, ownProps) => {
  let isMarkedForReview = false;
  if (typeof state.questionList.questionSummaries[`${ownProps.questionId}`] !== "undefined") {
    isMarkedForReview =
      state.questionList.questionSummaries[`${ownProps.questionId}`].review || false;
  }
  return {
    testContentHeight: state.testSection.testHeight,
    testSectionId: state.testSection.testSection.id,
    questionSummaries: state.questionList.questionSummaries,
    emailResponses: [...state.emailInbox.emailResponses].filter(
      x => x.questionId === ownProps.question.id
    ),
    taskResponses: [...state.emailInbox.taskResponses].filter(
      x => x.questionId === ownProps.question.id
    ),
    readOnly: state.testSection.readOnly,
    isMarkedForReview: isMarkedForReview
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      markForReview,
      updateRatings
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(QuestionContentFactory);
