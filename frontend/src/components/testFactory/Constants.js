export const TestSectionType = {
  TOP_TABS: 1,
  SINGLE_COMPONENT: 2,
  FINISH: 3,
  QUIT: 4
};

export const NextSectionButtonType = {
  NONE: 0,
  PROCEED: 1,
  POPUP: 2
};
export const NextSectionButtonTypeObject = {
  NONE: { value: 0, label: "NONE" },
  PROCEED: { value: 1, label: "PROCEED" },
  POPUP: { value: 2, label: "POPUP" }
};

export const ReducerType = {
  INBOX: 1,
  QUESTION_LIST: 2,
  NOTEPAD: 3
};

export const TestSectionComponentType = {
  SINGLE_PAGE: 1,
  SIDE_NAVIGATION: 2,
  INBOX: 3,
  QUESTION_LIST: 4
};
export const TestSectionComponentTypeMap = {
  1: "SINGLE_PAGE",
  2: "SIDE_NAVIGATION",
  3: "INBOX",
  4: "QUESTION_LIST"
};

export const PageSectionType = {
  MARKDOWN: 1,
  ZOOM_IMAGE: 2,
  SAMPLE_EMAIL: 3,
  SAMPLE_EMAIL_RESPONSE: 4,
  SAMPLE_TASK_RESPONSE: 5,
  TREE_DESCRIPTION: 6,
  PRIVACY_NOTICE: 7
};
export const PageSectionTypeMap = {
  1: "MARKDOWN",
  2: "ZOOM_IMAGE",
  3: "SAMPLE_EMAIL",
  4: "SAMPLE_EMAIL_RESPONSE",
  5: "SAMPLE_TASK_RESPONSE",
  6: "TREE_DESCRIPTION",
  7: "PRIVACY_NOTICE"
};
export const QuestionType = {
  MULTIPLE_CHOICE: 1,
  EMAIL: 2
};

export const QuestionSectionType = {
  MARKDOWN: 1,
  END: 2
};
export const QuestionSectionTypeObject = {
  MARKDOWN: { value: 1, label: "MARKDOWN" },
  END: { value: 2, label: "END" }
};

export const QuestionDifficultyType = {
  EASY: 1,
  MEDIUM: 2,
  HARD: 3
};

export const TestSectionScoringTypeObject = {
  NOT_SCORABLE: { value: 0, label: "NOT SCORABLE" },
  AUTO_SCORE: { value: 1, label: "AUTO SCORE" },
  COMPETENCY: { value: 2, label: "COMPETENCY" }
};

// mirror of
// static files in backend/cms/static/
