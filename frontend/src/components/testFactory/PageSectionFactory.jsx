import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import ReactMarkdown from "react-markdown";
import ImageZoom from "react-medium-image-zoom";
import TreeDescriptionPopup from "../eMIB/TreeDescriptionPopup";
import SampleEmail from "../eMIB/SampleEmail";
import SampleEmailResponse from "../eMIB/SampleEmailResponse";
import { ACTION_TYPE, EMAIL_TYPE } from "../eMIB/constants";
import SampleTaskResponse from "../eMIB/SampleTaskResponse";
import { PageSectionType } from "./Constants";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import PrivacyNoticePageSection from "./PrivacyNoticePageSection";

const styles = {
  containerWidth: {
    width: "100%",
    paddingRight: 10
  },
  testImage: {
    width: "100%",
    maxWidth: 600
  },
  button: {
    margin: "12px 0 12px 5px"
  }
};

class PageSectionFactory extends Component {
  static propTypes = {
    sectionComponentPage: PropTypes.object
  };
  render() {
    const { sectionComponentPage, testId, assignedTestId } = this.props;
    return (
      <div style={styles.containerWidth}>
        {sectionComponentPage.page_sections.map((pageSection, index) => {
          switch (pageSection.page_section_type) {
            case PageSectionType.MARKDOWN:
              return (
                <div key={index} id="unit-test-page-section-factory-markdown">
                  <ReactMarkdown source={pageSection.page_section_content.content} />
                </div>
              );

            case PageSectionType.ZOOM_IMAGE:
              let imgsrc =
                `/api/get-file?JWT=${SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)}&fileName=${
                  pageSection.page_section_content.small_image
                }` +
                `&test_definition=${testId}` +
                `&assigned_test=${assignedTestId}`;

              return (
                <div key={index} id="unit-test-page-section-factory-image">
                  <ImageZoom
                    image={{
                      src: imgsrc,
                      alt: pageSection.page_section_content.small_image,
                      style: styles.testImage,
                      className: "ie-zoom-cursor"
                    }}
                    zoomImage={{
                      src: imgsrc,
                      alt: pageSection.page_section_content.large_image,
                      className: "ie-zoom-cursor"
                    }}
                  />
                </div>
              );

            case PageSectionType.TREE_DESCRIPTION:
              let teamStructure = [pageSection.page_section_content.address_book[0]];
              return (
                <div key={index} id="unit-test-page-section-factory-tree-desc">
                  <TreeDescriptionPopup contentItem={teamStructure} />
                </div>
              );

            case PageSectionType.SAMPLE_EMAIL:
              let sampleEmail = {
                id: pageSection.page_section_content.email_id,
                to: pageSection.page_section_content.to_field,
                from: pageSection.page_section_content.from_field,
                subject: pageSection.page_section_content.subject_field,
                date: pageSection.page_section_content.date_field,
                body: pageSection.page_section_content.body
              };
              return (
                <div key={index} id="unit-test-page-section-factory-sample-email">
                  <SampleEmail email={sampleEmail} />
                </div>
              );

            case PageSectionType.SAMPLE_EMAIL_RESPONSE:
              const sampleEmailResponse = {
                actionType: ACTION_TYPE.email,
                emailType: EMAIL_TYPE.reply,
                emailTo: pageSection.page_section_content.to_field,
                emailCc: pageSection.page_section_content.cc_field,
                emailBody: pageSection.page_section_content.response,
                reasonsForAction: pageSection.page_section_content.reason
              };
              return (
                <div key={index} id="unit-test-page-section-factory-sample-email-response">
                  <SampleEmailResponse action={sampleEmailResponse} />
                </div>
              );

            case PageSectionType.SAMPLE_TASK_RESPONSE:
              const exampleTaskResponse = {
                actionType: ACTION_TYPE.task,
                task: pageSection.page_section_content.response,
                reasonsForAction: pageSection.page_section_content.reason
              };
              return (
                <div key={index} id="unit-test-page-section-factory-sample-task-response">
                  <SampleTaskResponse action={exampleTaskResponse} />
                </div>
              );
            case PageSectionType.PRIVACY_NOTICE:
              return (
                <div key={index} id="unit-test-page-section-factory-pns">
                  <PrivacyNoticePageSection
                    setNextButtonDisabled={this.props.setNextButtonDisabled}
                  />
                </div>
              );

            default:
              return (
                <div key={index} id="unit-test-page-section-factory-unsupported">
                  <h1>Unsupported Page Section Type: {pageSection.page_section_type}</h1>
                </div>
              );
          }
        })}
      </div>
    );
  }
}

export { PageSectionFactory as UnconnectedPageSectionFactory };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    assignedTestId: state.assignedTest.assignedTestId,
    testId: state.assignedTest.testId
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PageSectionFactory);
