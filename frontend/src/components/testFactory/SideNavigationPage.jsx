import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SideNavigation from "./SideNavigation";
import PageSectionFactory from "./PageSectionFactory";
import { switchSideTab } from "../../modules/NavTabsRedux";

const styles = {
  iconContainer: {
    paddingLeft: 13
  },
  icon: {
    fontSize: 200
  },
  tabContainer: {
    zIndex: 1,
    overflowY: "scroll",
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  }
};

class SideNavigationPage extends Component {
  static propTypes = {
    testSection: PropTypes.array
    // Props from Redux
  };

  switchTab = key => {
    this.props.switchSideTab(key);
  };

  render() {
    return (
      <SideNavigation
        specs={this.props.testSection.map(page => {
          return {
            menuString: page.title,
            body: <PageSectionFactory sectionComponentPage={page.pages} />
          };
        })}
        startIndex={0}
        activeKey={this.props.currentTab}
        switchTab={this.switchTab}
        displayNextPreviousButton={false}
        isMain={true}
        tabContainerStyle={styles.tabContainer}
        tabContentStyle={styles.tabContent}
        navStyle={styles.nav}
      />
    );
  }
}

export { SideNavigationPage as UnconnectedSideNavigationPage };

const mapStateToProps = (state, ownProps) => {
  let currentTab = 1;
  if (typeof state.navTabs.currentSideTab[state.navTabs.currentTopTab] !== "undefined") {
    currentTab = state.navTabs.currentSideTab[state.navTabs.currentTopTab].sideTab;
  }
  return {
    currentTab: currentTab
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ switchSideTab }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SideNavigationPage);
