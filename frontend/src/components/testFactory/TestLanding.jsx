import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { PATH } from "../commons/Constants";
import TestPage from "./TestPage";
import { Route, Redirect } from "react-router-dom";
import { getAssignedTests } from "../../modules/AssignedTestsRedux";
import {
  activateTest,
  deactivateTest,
  lockTest,
  unlockTest,
  pauseTest,
  unpauseTest,
  setPreviousStatus,
  resetTestStatusState
} from "../../modules/TestStatusRedux";
import { TEST_STATUS } from "../../components/ta/Constants";
import candidateSideTestSessionChannels, {
  CHANNELS_PATH,
  CHANNELS_ACTION
} from "../../helpers/testSessionChannels";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import { setErrorStatus } from "../../modules/ErrorStatusRedux";

class TestLanding extends Component {
  state = {
    isLoading: true,
    sockets: [],
    assignedTestId: null,
    testAccessCode: null
  };

  componentDidMount = () => {
    this.checkForActiveAssignedTest();
  };

  // checking for active assigned test (active or locked)
  checkForActiveAssignedTest = () => {
    this.setState({ isLoading: true }, () => {
      let nonStartedTestIndex = null;
      this.props
        .getAssignedTests()
        .then(response => {
          // checking for assigned tests
          for (let i = 0; i < response.length; i++) {
            // test is active
            if (response[i].status === TEST_STATUS.ACTIVE) {
              // activating test
              this.props.activateTest();
              this.connectingWebSocket(
                `${CHANNELS_PATH.testAdministratorRoom}${SessionStorage(
                  ACTION.GET,
                  ITEM.AUTH_TOKEN
                )}/${response[i].test_access_code}`
              );
              this.setState({
                assignedTestId: response[i].id,
                testAccessCode: response[i].test_access_code
              });
              return;
              // test is locked
            } else if (response[i].status === TEST_STATUS.LOCKED) {
              // locking test
              this.props.lockTest();
              // previous status = READY
              if (response[i].previous_status === TEST_STATUS.READY) {
                // setting redux previousStatus state
                this.props.setPreviousStatus(TEST_STATUS.READY);
              }
              this.connectingWebSocket(
                `${CHANNELS_PATH.testAdministratorRoom}${SessionStorage(
                  ACTION.GET,
                  ITEM.AUTH_TOKEN
                )}/${response[i].test_access_code}`
              );
              this.setState({
                assignedTestId: response[i].id,
                testAccessCode: response[i].test_access_code
              });
              return;
              // test is paused
            } else if (response[i].status === TEST_STATUS.PAUSED) {
              // pausing test
              this.props.pauseTest(response[i].pause_test_time, response[i].pause_start_date);
              // previous status = READY
              if (response[i].previous_status === TEST_STATUS.READY) {
                // setting redux previousStatus state
                this.props.setPreviousStatus(TEST_STATUS.READY);
              }
              this.connectingWebSocket(
                `${CHANNELS_PATH.testAdministratorRoom}${SessionStorage(
                  ACTION.GET,
                  ITEM.AUTH_TOKEN
                )}/${response[i].test_access_code}`
              );
              this.setState({
                assignedTestId: response[i].id,
                testAccessCode: response[i].test_access_code
              });
              return;
              // test has been locked and it's now ready (approved by TA, but not started yet)
            } else if (response[i].status === TEST_STATUS.READY) {
              // previous status is READY (meaning that the test was locked before the candidate started the test)
              if (this.props.previousStatus === TEST_STATUS.READY) {
                // setting nonStartedTestIndex
                nonStartedTestIndex = i;
              }
            }
            // that is the last assigned test + nonStartedTestIndex has been set
            if (i === response.length - 1 && nonStartedTestIndex >= 0) {
              // reset test status redux states
              this.props.resetTestStatusState();
            }
          }
        })
        .then(() => {
          this.setState({ isLoading: false });
        });
    });
  };

  // connecting candidate's websocket to test administrator room
  // not calling this function if the status is READY, since the AssignedTestTable component will take care of that part
  connectingWebSocket = path => {
    let sockets = this.state.sockets;
    const currentSocket = candidateSideTestSessionChannels(path, this.props.username);
    sockets.push(currentSocket);
    // when socket messages are received here, it means that the test has been locked/unlocked or pause/unpaused
    // in these cases, the candidate needs a page refresh
    currentSocket.onmessage = e => {
      // getting the parameters
      const message = JSON.parse(e.data);
      const parameters = JSON.parse(JSON.parse(e.data).parameters);
      // conditional paths
      const conditionalPaths = [PATH.dashboard, PATH.testBase];
      // username is the same as the one received in the websocket message + pathname is part of the conditionalPaths array
      if (
        this.props.username === parameters.username_id &&
        conditionalPaths.indexOf(window.location.pathname) >= 0
      ) {
        if (message.action === CHANNELS_ACTION.lockCandidate) {
          this.props.lockTest();
        } else if (message.action === CHANNELS_ACTION.unlockCandidate) {
          this.props.unlockTest();
        } else if (message.action === CHANNELS_ACTION.pauseCandidate) {
          this.props.pauseTest(String(message.pause_time), Date.now());
        } else if (message.action === CHANNELS_ACTION.unpauseCandidate) {
          this.props.unpauseTest();
        }
      }
    };
    currentSocket.onerror = () => {
      // deactivating the test
      this.props.deactivateTest();
      // removing TEST_ACTIVE session storage item
      SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);
      // setting error status
      this.props.setErrorStatus(true);
      // redirecting to error page
      this.props.history.push(PATH.websocketConnectionError);
    };
  };

  render() {
    return (
      <>
        {!this.state.isLoading && (
          <Route
            path={PATH.testBase}
            component={() =>
              this.props.isTestActive ? (
                <TestPage
                  assignedTestId={this.state.assignedTestId}
                  sampleTest={false}
                  sockets={this.state.sockets}
                  testAccessCode={this.state.testAccessCode}
                />
              ) : (
                <Redirect to={PATH.dashboard} />
              )
            }
          />
        )}
      </>
    );
  }
}
export { TestLanding as UnconnectedUITLanding };
const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.testStatus.isTestActive,
    isTestLocked: state.testStatus.isTestLocked,
    previousStatus: state.testStatus.previousStatus,
    username: state.user.username
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      activateTest,
      deactivateTest,
      lockTest,
      unlockTest,
      pauseTest,
      unpauseTest,
      setPreviousStatus,
      resetTestStatusState,
      setErrorStatus
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(TestLanding);
