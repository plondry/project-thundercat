import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setLanguage } from "../../modules/LocalizeRedux";
import TestSectionComponentFactory from "./TestSectionComponentFactory";
import {
  getTestSection,
  updateTestSection,
  setStartTime,
  setContentLoaded,
  setContentUnLoaded
} from "../../modules/TestSectionRedux";
import { Helmet } from "react-helmet";
import LOCALIZE from "../../text_resources";
import { tryTestSection } from "../../modules/TestBuilderRedux";
import { Button } from "react-bootstrap";
import { PATH } from "../commons/Constants";

const styles = {
  container: {
    maxWidth: 1400,
    minWidth: 900,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto",
    textAlign: "left"
  }
};

class TestBuilderTestPage extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      setLanguage: PropTypes.func,
      currentLanguage: PropTypes.string,
      sampleTest: PropTypes.bool
    };

    TestBuilderTestPage.defaultProps = {
      sampleTest: false
    };
  }

  state = {
    error: false,
    errorMessage: ""
  };

  componentDidMount = () => {
    if (!this.props.testSectionLoaded) {
      this.getTestSection();
    }
  };

  // componentWillUnmount =()

  getTestSection = () => {
    let td = this.props.newTestDefinition;
    td.question_section_definitions = td.question_section_definitions.filter(obj => obj.id);
    this.props.tryTestSection(this.props.viewTestSectionOrderNumber, td).then(response => {
      if (!response.ok) {
        this.setState({ error: true, errorMessage: response.body });
      } else {
        response = response.body;

        this.props.updateTestSection(response);

        if (this.isTimedSection(response.default_time)) {
          //store the start time in redux
          if (!response.start_time) {
            this.props.setStartTime(Date());
          } else {
            this.props.setStartTime(new Date(response.start_time).toString());
          }
        }

        this.props.setContentLoaded();
      }
    });
  };

  handleNext = () => {};

  handleFinishTest = () => {};

  isTimedSection = defaultTime => {
    if (defaultTime || defaultTime === 0) return true;
    return false;
  };

  timeout = () => {};

  handleGoBack = () => {
    this.props.setContentUnLoaded();
    this.props.history.push(PATH.testBuilder);
  };

  render() {
    if (this.state.error) {
      return (
        <div style={styles.container}>
          <Button className="btn" variant="secondary" onClick={this.handleGoBack}>
            {LOCALIZE.testBuilder.goBackButton}
          </Button>
          <div
            style={{ width: "900px", margin: "auto", paddingTop: "20px", color: "red" }}
            id="test-page-error-message"
          >
            <h1>{LOCALIZE.commons.error}</h1>
            {this.state.errorMessage.message &&
              this.state.errorMessage.message.map((message, index) => {
                return <h2 key={index}>{message}</h2>;
              })}
            {!this.state.errorMessage.message && (
              <h2>{LOCALIZE.testBuilder.errors.nondescriptError}</h2>
            )}
            <p>{this.state.errorMessage.error}</p>
          </div>
        </div>
      );
    }

    if (!this.props.testSectionLoaded) {
      return <h2 style={{ width: "900px", margin: "auto", paddingTop: "20px" }}>loading</h2>;
    }
    const { testSection } = this.props;

    return (
      <>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.uitTest}</title>
        </Helmet>
        <div style={styles.container}>
          <Button className="btn" variant="secondary" onClick={this.handleGoBack}>
            {LOCALIZE.testBuilder.goBackButton}
          </Button>
          <TestSectionComponentFactory
            testSection={testSection.localize[this.props.currentLanguage]}
            handleNext={this.handleNext}
            handleFinishTest={this.handleFinishTest}
            timeLimit={testSection.default_time}
            testSectionStartTime={this.props.testSectionStartTime}
            timeout={this.timeout}
            testSectionType={testSection.section_type}
            resetAllRedux={this.resetAllRedux}
            defaultTab={testSection.default_tab}
            usesNotepad={testSection.uses_notepad}
          />
        </div>
      </>
    );
  }
}

export { TestBuilderTestPage as UnconnectedTestBuilderTestPage };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testSection.testSection,
    testSectionLoaded: state.testSection.isLoaded,
    testSectionStartTime: state.testSection.startTime,
    newTestDefinition: state.testBuilder,
    viewTestSectionOrderNumber: state.testBuilder.viewTestSectionOrderNumber
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLanguage,
      getTestSection,
      updateTestSection,
      setContentLoaded,
      setContentUnLoaded,
      setStartTime,
      tryTestSection
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TestBuilderTestPage));
