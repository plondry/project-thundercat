import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import SideNavigationPage from "./SideNavigationPage";
import PageSectionFactory from "./PageSectionFactory";
import QuestionList from "./QuestionList";
import { TestSectionComponentType } from "./Constants";
import { QuestionType } from "./Constants";
import { changeCurrentQuestion, readQuestion } from "../../modules/QuestionListRedux";
import { seenUitQuestion } from "../../modules/UpdateResponseRedux";
import { getTestLanguage } from "../testBuilder/helpers";

const styles = {
  sectionPadding: { paddingBottom: 80 }
};

class SingleComponentFactory extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      testSection: PropTypes.object
    };
  }

  state = {
    _isMounted: false
  };

  componentDidMount = () => {
    //remove warnings about updating unmounted children
    this.setState({ _isMounted: true });
  };

  componentWillUnmount = () => {
    this.setState({ _isMounted: false });
  };

  render() {
    if (!this.state._isMounted) {
      return null;
    }

    let lang = "";

    switch (this.props.type) {
      case TestSectionComponentType.SINGLE_PAGE:
        let component = this.props.testSection.test_section_component[0];
        lang = getTestLanguage(this.props.testSection.language, this.props.currentLanguage);
        if (component === undefined) {
          //prevent errors on loading
          //creates empty page
          component = [{ pages: { page_sections: [] } }];
        }
        return (
          <div id="unit-test-page-section-factory" style={styles.sectionPadding} lang={lang}>
            <PageSectionFactory
              sectionComponentPage={component.pages}
              setNextButtonDisabled={this.props.setNextButtonDisabled}
            />
          </div>
        );

      case TestSectionComponentType.SIDE_NAVIGATION:
        lang = getTestLanguage(this.props.testSection.language, this.props.currentLanguage);
        return (
          <div id="unit-test-side-navigation-page" lang={lang}>
            <SideNavigationPage testSection={this.props.testSection.test_section_component} />
          </div>
        );
      case TestSectionComponentType.INBOX:
        let questions = this.props.testSection.test_section_component.questions;
        lang = getTestLanguage(this.props.testSection.language, this.props.currentLanguage);
        return (
          <div id="unit-test-email-question-list" lang={lang}>
            <QuestionList
              questions={questions}
              defaultActiveKey={questions[0] === undefined ? 0 : questions[0].id}
              headerFooterPX={this.props.testContentHeight}
              questionSummaries={this.props.questionSummaries}
              testSectionId={this.props.testSectionId}
              testSectionComponentId={this.props.testSection.id}
              questionType={QuestionType.EMAIL}
              componentType={TestSectionComponentType.INBOX}
              addressBook={this.props.testSection.test_section_component.address_book}
            />
          </div>
        );

      case TestSectionComponentType.QUESTION_LIST:
        // get question id incase question list is undefined
        let questionId = 0;
        lang = getTestLanguage(
          this.props.testSection.test_section_component.language,
          this.props.currentLanguage
        );

        if (this.props.testSection.test_section_component.questions[0] !== undefined) {
          questionId = this.props.testSection.test_section_component.questions[0].id;
          if (
            this.props.testSection.test_section_component.questions
              .map(obj => {
                return obj.id;
              })
              .indexOf(questionId) >= 0
          ) {
            this.props.changeCurrentQuestion(questionId);
            if (this.props.assignedTestId) {
              this.props.seenUitQuestion(this.props.assignedTestId, questionId);
            }
            this.props.readQuestion(questionId);
          }
        }
        return (
          <div id="unit-test-question-list" lang={lang}>
            <QuestionList
              questions={this.props.testSection.test_section_component.questions}
              defaultActiveKey={questionId}
              headerFooterPX={this.props.testContentHeight}
              questionSummaries={this.props.questionSummaries}
              testSectionId={this.props.testSectionId}
              testSectionComponentId={this.props.testSection.id}
              questionType={QuestionType.MULTIPLE_CHOICE}
              componentType={TestSectionComponentType.QUESTION_LIST}
            />
          </div>
        );

      default:
        return (
          <h1 id="unit-test-single-component-factory-unsupported">
            Unsupported Page Type: {this.props.testSection.type}
          </h1>
        );
    }
  }
}

export { SingleComponentFactory as UnconnectedSingleComponentFactory };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testContentHeight: state.testSection.testHeight,
    testSectionId: state.testSection.testSection.id,
    questionSummaries: state.questionList.questionSummaries,
    assignedTestId: state.assignedTest.assignedTestId
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ changeCurrentQuestion, seenUitQuestion, readQuestion }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SingleComponentFactory);
