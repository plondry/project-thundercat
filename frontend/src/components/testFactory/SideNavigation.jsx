import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Tab, Row, Col, Nav } from "react-bootstrap";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import NextPreviousButtonNav from "../commons/NextPreviousButtonNav";
import { BANNER_STYLE, TEST_STYLE } from "../eMIB/constants";

class SideNavigation extends Component {
  BODY_HEIGHT = `calc(100vh - ${this.props.isTestActive ? TEST_STYLE : BANNER_STYLE}px)`;

  EVENT_KEYS = [];

  styles = {
    rowContainer: {
      margin: 0
    },
    sideNavPadding: {
      paddingLeft: 0
    },
    bodyContent: {
      paddingTop: 10,
      paddingBottom: 10,
      height: this.BODY_HEIGHT,
      minHeight: 400,
      textAlign: "left"
    },
    hiddenText: {
      position: "absolute",
      left: -10000,
      top: "auto",
      width: 1,
      height: 1,
      overflow: "hidden"
    },
    noStyle: {}
  };
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      startIndex: PropTypes.number.isRequired,
      specs: PropTypes.arrayOf(
        PropTypes.shape({
          menuString: PropTypes.string,
          body: PropTypes.object
        })
      ).isRequired,
      isMain: PropTypes.bool,
      parentTabName: PropTypes.string,
      tabContainerStyle: PropTypes.object,
      tabContentStyle: PropTypes.object,
      navStyle: PropTypes.object,
      bodyContentCustomStyle: PropTypes.object
    };
    SideNavigation.defaultProps = {
      tabContainerStyle: this.styles.noStyle,
      tabContentStyle: this.styles.noStyle,
      navStyle: this.styles.noStyle,
      showNavButtons: true
    };
  }
  state = {
    currentIndex: this.props.startIndex + 1
  };

  /* populating the event keys
  Instructions: from index 1 to index 10
  Background: from index 11 to index 20*/
  populateEventKeys = () => {
    for (let i = 1; i <= 20; i++) {
      this.EVENT_KEYS.push(i);
    }
    this.setState(this.EVENT_KEYS);
  };

  componentDidMount = () => {
    this.populateEventKeys();
    // if background tab is rendered
    if (this.props.startIndex === 10) {
      // focusing on side navigation items of background tab
      document.getElementById("navigation-items-section").focus();
    }
  };

  componentDidUpdate = () => {};

  onChangeToNext = index => {
    this.props.switchTab(index);
    document.getElementById("main-content").scrollTop = 0;
  };

  onChangeToPrevious = index => {
    this.props.switchTab(index);
    document.getElementById("main-content").scrollTop = 0;
  };

  onSelect = eventKey => {
    this.setState({ currentIndex: eventKey });
    this.props.switchTab(eventKey);
    document.getElementById("main-content").scrollTop = 0;
  };

  render() {
    const { startIndex, specs, isMain } = this.props;
    return (
      <Tab.Container
        id="left-tabs-navigation"
        activeKey={this.props.activeKey}
        onSelect={this.onSelect}
      >
        <Row style={this.styles.rowContainer}>
          <Col
            role="region"
            aria-label={LOCALIZE.ariaLabel.sideNavigationSection}
            style={this.styles.sideNavPadding}
            sm={3}
          >
            <Nav
              role="navigation"
              variant="pills"
              className="flex-column"
              style={this.props.navStyle}
            >
              <div id="navigation-items-section" tabIndex={-1}>
                {specs.map((item, index) => {
                  return (
                    <Nav.Item key={index}>
                      <Nav.Link role="tab" eventKey={this.EVENT_KEYS[index + startIndex]}>
                        {specs[index].menuString}{" "}
                        <span style={this.styles.hiddenText}>{this.props.parentTabName}</span>
                      </Nav.Link>
                    </Nav.Item>
                  );
                })}
              </div>
            </Nav>
          </Col>
          <Col
            id={isMain ? "main-content" : ""}
            role="main"
            sm={9}
            style={this.props.tabContainerStyle}
          >
            <Tab.Content tabIndex={0} style={this.props.tabContentStyle}>
              {specs.map((item, index) => {
                return (
                  <Tab.Pane key={index} eventKey={this.EVENT_KEYS[index + startIndex]}>
                    <div
                      style={
                        this.props.bodyContentCustomStyle
                          ? { ...this.styles.bodyContent, ...this.props.bodyContentCustomStyle }
                          : this.styles.bodyContent
                      }
                    >
                      <Col>
                        <Row>{specs[index].body}</Row>
                        {this.props.showNavButtons && (
                          <NextPreviousButtonNav
                            showNext={index < specs.length - 1}
                            showPrevious={index > 0}
                            onChangeToPrevious={() => {
                              this.onChangeToPrevious(this.EVENT_KEYS[index + startIndex] - 1);
                            }}
                            onChangeToNext={() => {
                              this.onChangeToNext(this.EVENT_KEYS[index + startIndex] + 1);
                            }}
                          />
                        )}
                      </Col>
                    </div>
                  </Tab.Pane>
                );
              })}
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    );
  }
}

export { SideNavigation as UnconnectedSideNavigation };
const mapStateToProps = (state, ownProps) => {
  return {
    isTestActive: state.testStatus.isTestActive
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SideNavigation);
