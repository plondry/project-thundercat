import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import "../../css/inbox.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEye,
  faEyeSlash,
  faExclamationCircle,
  faCircle as faCircleSolid
} from "@fortawesome/free-solid-svg-icons";
import { Row, Col } from "react-bootstrap";
import { faCircle } from "@fortawesome/free-regular-svg-icons";

export const styles = {
  //buttons
  button: {
    textAlign: "left",
    padding: 10,
    borderWidth: "0px 1px 1px 1px",
    borderStyle: "solid",
    borderColor: "#00565E",
    cursor: "pointer",
    fontSize: 14,
    width: "100%",
    margin: "0px 0px 0px 0px"
  },
  buttonSelectedBackground: {
    backgroundColor: "#00565E"
  },
  buttonReadBackground: {
    backgroundColor: "#F5F5F5"
  },
  buttonUnreadBackground: {
    backgroundColor: "white"
  },
  buttonSelectedText: {
    color: "#D3FCFF"
  },
  buttonUnselectedText: {
    color: "black"
  },
  buttonUnselectedSymbol: {
    color: "#00565E"
  },
  emptyIcon: {
    textAlign: "left",
    padding: 10,
    cursor: "pointer",
    fontSize: 14,
    width: "100%"
  }
};

class QuestionPreview extends Component {
  static propTypes = {
    isRead: PropTypes.bool.isRequired,
    isAnswered: PropTypes.bool.isRequired,
    isSelected: PropTypes.bool.isRequired
  };

  render() {
    //READ/UNREAD CHECK
    //defaults, or if unread
    let buttonBackgroundColor = styles.buttonUnreadBackground;
    if (this.props.isRead) {
      //if it is read
      buttonBackgroundColor = styles.buttonReadBackground;
    }

    //SELECTED/UNSELECTED CHECK
    //defaults, or unselected
    let buttonTextColor = styles.buttonUnselectedText;
    let imageStyle = styles.buttonUnselectedSymbol;
    if (this.props.isSelected) {
      //if it is selected
      buttonBackgroundColor = styles.buttonSelectedBackground;
      buttonTextColor = styles.buttonSelectedText;
      imageStyle = styles.buttonSelectedText;
    }

    let buttonStyle = { ...styles.button, ...buttonTextColor, ...buttonBackgroundColor };
    let emptyIcon = { ...styles.emptyIcon, ...buttonTextColor, ...buttonBackgroundColor };

    const { index } = this.props;
    return (
      <Row
        id={
          this.props.isSelected
            ? "unit-test-selected-question-preview"
            : "unit-test-unselected-question-preview"
        }
        style={buttonStyle}
      >
        <Col xs={12} md={8} id="unit-test-question-id-label">
          {LOCALIZE.formatString(LOCALIZE.mcTest.questionList.questionId.toUpperCase(), index + 1)}
        </Col>
        <Col xs={6} md={4}>
          <Row style={{ float: "right" }} id="unit-test-question-preview-icons">
            <Col xs lg="2">
              {this.props.isRead ? (
                <FontAwesomeIcon
                  icon={faEye}
                  style={imageStyle}
                  title={LOCALIZE.mcTest.questionList.seenQuestion}
                />
              ) : (
                <FontAwesomeIcon
                  icon={faEyeSlash}
                  style={imageStyle}
                  title={LOCALIZE.mcTest.questionList.unseenQuestion}
                />
              )}
            </Col>
            <Col xs lg="2">
              {this.props.isAnswered ? (
                <FontAwesomeIcon
                  icon={faCircleSolid}
                  style={imageStyle}
                  title={LOCALIZE.mcTest.questionList.answeredQuestion}
                />
              ) : (
                <FontAwesomeIcon
                  icon={faCircle}
                  style={imageStyle}
                  title={LOCALIZE.mcTest.questionList.unansweredQuestion}
                />
              )}
            </Col>
            <Col xs lg="2">
              {this.props.isMarkedForReview ? (
                <FontAwesomeIcon
                  icon={faExclamationCircle}
                  style={imageStyle}
                  title={LOCALIZE.mcTest.questionList.reviewQuestion}
                />
              ) : (
                <div style={emptyIcon}></div>
              )}
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default QuestionPreview;
