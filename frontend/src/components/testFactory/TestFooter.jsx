import React, { Component } from "react";
import PropTypes from "prop-types";
import { Navbar } from "react-bootstrap";
import Timer from "../commons/Timer";
import CustomButton, { THEME } from "../../components/commons/CustomButton";
import LOCALIZE from "../../text_resources";

const styles = {
  footer: {
    borderTop: "1px solid #96a8b2",
    backgroundColor: "#D5DEE0"
  },
  content: {
    width: 1400,
    margin: "0 auto",
    padding: "0px 20px"
  },
  button: {
    float: "right"
  },
  footerIndex: {
    zIndex: 1
  }
};

class TestFooter extends Component {
  static propTypes = {
    submitTest: PropTypes.func,
    timeout: PropTypes.func,
    startTime: PropTypes.string
  };

  render() {
    return (
      <div role="contentinfo" className="fixed-bottom" style={styles.footerIndex}>
        <Navbar fixed="bottom" style={styles.footer}>
          <div style={styles.content} id="unit-test-timer">
            {this.props.timeLimit && (
              <Timer
                timeout={this.props.timeout}
                startTime={this.props.startTime}
                timeLimit={this.props.timeLimit}
              />
            )}
            <div style={styles.button}>
              <CustomButton
                label={LOCALIZE.commons.nextButton}
                id="unit-test-submit-btn"
                action={this.props.submitTest}
                customStyle={styles.nextButton}
                buttonTheme={THEME.SUCCESS}
                ariaLabel={LOCALIZE.ariaLabel.nextButton}
              >
              </CustomButton>
            </div>
          </div>
        </Navbar>
      </div>
    );
  }
}

export default TestFooter;
