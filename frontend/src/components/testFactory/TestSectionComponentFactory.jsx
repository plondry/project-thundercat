import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import TopTabs from "../eMIB/TopTabs";
import SingleComponentFactory from "./SingleComponentFactory";
import TestFooter from "./TestFooter";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { NextSectionButtonType, TestSectionType } from "./Constants";
import ReactMarkdown from "react-markdown";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  startTestBtn: {
    textAlign: "center",
    marginTop: 32
  }
};

class TestSectionComponentFactory extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      testSection: PropTypes.object
    };

    TestSectionComponentFactory.defaultProps = {
      isContentLoaded: false
    };
  }

  state = {
    showSubmitTestPopup: false,
    showStartTestPopup: false,
    proceedCheckbox: false,
    popupVisible: false,
    isNextButtonDisabled: false
  };

  setNextButtonDisabled = state => {
    this.setState({ isNextButtonDisabled: state });
  };

  openStartTestPopup = () => {
    this.setState({ showStartTestPopup: true });
  };
  closeStartTestPopup = () => {
    this.setState({ showStartTestPopup: false });
  };
  openSubmitTestPopup = () => {
    this.setState({ showSubmitTestPopup: true });
  };
  closeSubmitTestPopup = () => {
    this.setState({ showSubmitTestPopup: false });
  };

  factory = () => {
    if (this.props.testSection.components[0] === undefined) {
      return (
        <>
          <h1>Error</h1>
          <h3>Malformed Test Definition</h3>
          <h4>missing Test Section Component child</h4>
        </>
      );
    }
    switch (this.props.testSection.type) {
      case TestSectionType.SINGLE_COMPONENT:
        return (
          <div id="unit-test-single-component-factory">
            <SingleComponentFactory
              testSection={this.props.testSection.components[0]}
              type={this.props.testSection.components[0].component_type}
              setNextButtonDisabled={this.setNextButtonDisabled}
            />
          </div>
        );
      case TestSectionType.TOP_TABS:
        return (
          <div id="unit-test-top-tabs-component">
            <TopTabs
              testSection={this.props.testSection}
              currentLanguage={this.props.currentLanguage}
              usesNotepad={this.props.usesNotepad}
              defaultTab={this.props.defaultTab}
            />
          </div>
        );
      case TestSectionType.FINISH:
        return (
          <div id="unit-test-single-component-factory">
            <SingleComponentFactory
              testSection={this.props.testSection.components[0]}
              type={this.props.testSection.components[0].component_type}
              setNextButtonDisabled={this.setNextButtonDisabled}
            />
          </div>
        );
      case TestSectionType.QUIT:
        return (
          <div id="unit-test-single-component-factory">
            <SingleComponentFactory
              testSection={this.props.testSection.components[0]}
              type={this.props.testSection.components[0].component_type}
            />
          </div>
        );
      default:
        return (
          <div id="unit-test-unsupported-component">
            <h1>Unsupported Page Type: {this.props.testSection.type}</h1>
          </div>
        );
    }
  };

  buttonFactory = () => {
    const button = this.props.testSection.next_section_button;
    if (this.props.testSectionType === TestSectionType.FINISH) {
      return (
        <div style={styles.startTestBtn} id="unit-test-go-home-button">
          <Button
            onClick={this.props.handleFinishTest}
            className="btn btn-primary btn-wide"
            style={styles.startTestBtn}
            disabled={this.state.isNextButtonDisabled}
          >
            {LOCALIZE.mcTest.finishPage.homeButton}
          </Button>
        </div>
      );
    } else if (this.props.testSectionType === TestSectionType.QUIT) {
      return (
        <div style={styles.startTestBtn} id="unit-test-go-home-button">
          <Button
            onClick={this.props.handleFinishTest}
            className="btn btn-primary btn-wide"
            style={styles.startTestBtn}
          >
            {LOCALIZE.mcTest.finishPage.homeButton}
          </Button>
        </div>
      );
    }
    if (button.button_type === NextSectionButtonType.POPUP) {
      let rightButtonState = button.confirm_proceed
        ? !this.state.proceedCheckbox
        : this.state.proceedCheckbox;
      return (
        <div id="unit-test-popup-button">
          <TestFooter
            submitTest={() => {
              this.setState({ popupVisible: !this.state.popupVisible });
            }}
            timeout={this.props.timeout}
            timeLimit={this.props.timeLimit}
            startTime={this.props.testSectionStartTime}
            buttonText={button.button_text}
          />
          <PopupBox
            show={this.state.popupVisible}
            handleClose={this.closePopup}
            title={button.title}
            description={this.popupDescription(button)}
            leftButtonType={BUTTON_TYPE.secondary}
            leftButtonTitle={LOCALIZE.commons.cancel}
            rightButtonState={rightButtonState}
            rightButtonType={BUTTON_TYPE.primary}
            rightButtonTitle={button.button_text}
            rightButtonAction={this.props.handleNext}
          />
        </div>
      );
    }
    if (button.button_type === NextSectionButtonType.PROCEED) {
      return (
        <div style={styles.startTestBtn} id="unit-test-proceed-button" >
          <CustomButton
            label={LOCALIZE.commons.nextButton}
            action={this.props.handleNext}
            customStyle={styles.startTestBtn}
            buttonTheme={THEME.PRIMARY_WIDE}
            ariaLabel={LOCALIZE.ariaLabel.startTestBtn}
            disabled={this.state.isNextButtonDisabled}
          >
          </CustomButton>
        </div>
      );
    }
  };

  closePopup = () => {
    this.setState({ popupVisible: !this.state.popupVisible, proceedCheckbox: false });
    document.getElementById("unit-test-popup-button").focus();
  };

  popupDescription = button => {
    const timePhrase = LOCALIZE.formatString(
      LOCALIZE.commons.confirmStartTest.newtimerWarning,
      <b>
        {button.next_section_time_limit
          ? LOCALIZE.formatString(
              LOCALIZE.commons.confirmStartTest.numberMinutes,
              button.next_section_time_limit
            )
          : LOCALIZE.commons.confirmStartTest.timeUnlimited}
      </b>
    );

    return (
      <>
        <div id="unit-test-popup-description">
          <ReactMarkdown source={button.content} />
        </div>
        {button.next_section_time_limit && (
          <div id="unit-test-popup-next-section-time-limit">{timePhrase}</div>
        )}
        {button.confirm_proceed && (
          <div
            id="unit-test-popup-confirm-procced"
            className="custom-control custom-checkbox"
            style={{ paddingTop: "5px" }}
          >
            <input
              type="checkbox"
              className="custom-control-input"
              id={"procced-to-next-section-checkbox"}
              checked={this.state.proceedCheckbox}
              onChange={() => {
                this.setState({ proceedCheckbox: !this.state.proceedCheckbox });
              }}
            />
            <label className="custom-control-label" htmlFor={"procced-to-next-section-checkbox"}>
              {LOCALIZE.commons.confirmStartTest.confirmProceed}
            </label>
          </div>
        )}
      </>
    );
  };

  render() {
    const noSelect = !this.props.readOnly;
    return (
      <div className={noSelect ? "no-select" : ""} id="main-content">
        {this.factory()}
        {!this.props.readOnly && this.buttonFactory()}
      </div>
    );
  }
}

export default TestSectionComponentFactory;
