import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlusCircle,
  faTimes,
  faShare,
  faAngleUp,
  faAngleDown,
  faSort,
  faSortDown,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button } from "react-bootstrap";
import "../../css/profile-permissions.css";
import {
  getUserPermissions,
  getUserPendingPermissions,
  getAvailablePermissions,
  getExistingPermissions,
  getTestPermissions
} from "../../modules/PermissionsRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import RequestPermissionForm from "./RequestPermissionForm";
import {
  resetUserProfileStates,
  updatePermissionRequestDataState,
  sendPermissionRequest
} from "../../modules/UserProfileRedux";
import "rc-tooltip/assets/bootstrap_white.css";
import CustomButton from "../../components/commons/CustomButton";
import StyledTooltip from "../../components/authentication/StyledTooltip";

const styles = {
  container: {
    padding: "12px 0 0 24px"
  },
  tableContainer: {
    margin: "24px 22px 0 0",
    minHeight: 100,
    border: "1px solid #00565e",
    borderRadius: "4px 4px 0 0"
  },
  permissionsTable: {
    titleContainer: {
      width: "100%",
      height: 50,
      color: "white",
      backgroundColor: "#00565e",
      display: "table",
      paddingLeft: 12
    },
    title: {
      display: "table-cell",
      verticalAlign: "middle",
      fontWeight: "bold"
    },
    rowContainerBasicStyle: {
      width: "100%",
      minHeight: 60,
      padding: "8px 0 8px 12px",
      display: "table"
    },
    rowContainerDark: {
      backgroundColor: "#F3F3F3"
    },
    rowContainerLight: {
      backgroundColor: "white"
    },
    rowContainerBorder: {
      borderTop: "1px solid #CECECE"
    },
    existingPermissions: {
      label: {
        width: "30%",
        padding: "0 12px",
        display: "table-cell",
        verticalAlign: "middle"
      },
      description: {
        width: "55%",
        padding: "0 12px",
        display: "table-cell",
        verticalAlign: "middle"
      },
      status: {
        width: "15%",
        padding: "0 12px",
        display: "table-cell",
        textAlign: "center",
        verticalAlign: "middle"
      }
    },
    addPermission: {
      containerBasicStyle: {
        height: 60,

        display: "table",
        padding: "0 12px",
        borderTop: "1px solid #CECECE"
      },
      containerLight: {
        backgroundColor: "white"
      },
      containerDark: {
        backgroundColor: "#F3F3F3"
      },
      content: {
        width: "calc(100vw)",
        display: "table-cell",
        verticalAlign: "middle"
      },
      icon: {
        color: "#00565e",
        verticalAlign: "middle"
      },
      button: {
        padding: 12
      },
      label: {
        marginLeft: 18,
        verticalAlign: "middle",
        fontWeight: "bold"
      }
    }
  },
  testPermissionsTable: {
    spacer: {
      margin: "24px 0"
    },
    title: {
      width: "calc(100vw)",
      height: 50,
      color: "white",
      backgroundColor: "#00565e",
      display: "table-cell",
      verticalAlign: "middle",
      fontWeight: "bold",
      paddingLeft: 12
    },
    testRowContainerTopRow: {
      backgroundColor: "white"
    },
    testRowContainerLight: {
      borderTop: "1px solid #CECECE",
      backgroundColor: "white"
    },
    testRowContainerDark: {
      borderTop: "1px solid #CECECE",
      backgroundColor: "#F3F3F3"
    },
    testRowSeparator: {
      margin: "12px 0 0 0"
    },
    collapsingRowContainer: {
      width: "100%",
      padding: "12px 0 12px 24px",
      fontWeight: "bold",
      border: "none",
      background: "transparent"
    },
    collapsingIconContainer: {
      marginRight: 24
    },
    collapsingIcon: {
      fontSize: 24,
      color: "#00565e",
      verticalAlign: "middle"
    },
    container: {
      width: "95%",
      margin: "0 auto",
      borderCollapse: "collapse"
    },
    testPermissionsTableRowContainer: {
      borderBottom: "1px solid #CECECE"
    },
    testVersionsTable: {
      head: {
        width: "100%",
        color: "#00565e",
        borderBottom: "1px solid #00565e"
      },
      title: {
        width: "23.333%",
        verticalAlign: "middle",
        textAlign: "center",
        padding: "0 12px"
      },
      testVersionTitle: {
        width: "30%",
        verticalAlign: "middle",
        padding: "0 12px 0 48px"
      },
      testVersionLabel: {
        padding: "6px 12px 6px 48px",
        overflowWrap: "anywhere"
      },
      orderNumberLabel: {
        textAlign: "center",
        padding: "6px 12px",
        overflowWrap: "anywhere"
      },
      expirationLabel: {
        textAlign: "center",
        padding: "6px 12px",
        overflowWrap: "anywhere"
      },
      iconButton: {
        border: "none",
        background: "transparent",
        color: "#00565e"
      },
      sortIcon: {
        marginLeft: 6,
        cursor: "pointer"
      },
      sortIconActif: {
        marginLeft: 6,
        cursor: "pointer",
        verticalAlign: "top"
      }
    }
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipIcon: {
    minHeight: 36,
    minWidth: 40,
    color: "white",
    padding: 6
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  displayNone: {
    display: "none"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  }
};

class Permissions extends Component {
  static propTypes = {
    username: PropTypes.string.isRequired,
    // Props from Redux
    getExistingPermissions: PropTypes.func,
    getUserPermissions: PropTypes.func,
    getTestPermissions: PropTypes.func,
    resetUserProfileStates: PropTypes.func,
    updatePermissionRequestDataState: PropTypes.func,
    sendPermissionRequest: PropTypes.func,
    getUserPendingPermissions: PropTypes.func,
    getAvailablePermissions: PropTypes.func
  };

  state = {
    permissions: [],
    permissionsDefinition: [],
    showAddPermissionPopup: false,
    testPermissions: [],
    extendedTestVersionsDetails: false,
    priOrMilitaryNbrUpdated: "",
    triggerValidation: false,
    noAvailablePermission: false,
    testPermissionsTableSortingElement: 1,
    isLoadingTestPermissions: false
  };

  componentDidMount = () => {
    // reseting user profile redux states to make sure that old user profile data is removed from redux states
    this.props.resetUserProfileStates();
    this.populateCurrentUserPermissions();
    this.populatePermissionsDefinition();
    this.populateCurrentTestPermissions(this.state.testPermissionsTableSortingElement);
  };

  componentDidUpdate = (prevProps, prevState) => {
    // when username gets updated
    if (prevProps.username !== this.props.username) {
      this.populateCurrentUserPermissions();
      this.populatePermissionsDefinition();
      this.populateCurrentTestPermissions(this.state.testPermissionsTableSortingElement);
    }
    // when language gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateCurrentUserPermissions();
      this.populatePermissionsDefinition();
      this.populateCurrentTestPermissions(this.state.testPermissionsTableSortingElement);
    }
    // if testPermissionsTableSortingElement state gets updated
    if (
      prevState.testPermissionsTableSortingElement !== this.state.testPermissionsTableSortingElement
    ) {
      this.populateCurrentTestPermissions(this.state.testPermissionsTableSortingElement);
    }
  };

  // populate permissions table based on the user' permissions
  populateCurrentUserPermissions = () => {
    const currentUserPermissions = [];
    // getting user' permissions
    this.props
      .getUserPermissions()
      .then(response => {
        // if user is a super user
        if (this.props.isSuperUser) {
          currentUserPermissions.push({
            text: LOCALIZE.profile.permissions.systemPermissionInformation.superUser.title,
            description:
              LOCALIZE.profile.permissions.systemPermissionInformation.superUser.permission,
            pendingStatus: false
          });
        } else {
          // looping in user' permissions
          for (let i = 0; i < response.length; i++) {
            // if user is not a super user and has at least one permission
            if (response[0] !== "No Permission") {
              // pushing results in currentUserPermissions array
              currentUserPermissions.push({
                text: response[i][`${this.props.currentLanguage}_name`],
                description: response[i][`${this.props.currentLanguage}_description`],
                pendingStatus: false
              });
            }
          }
        }
      })
      .then(() => {
        // getting user' pending permissions
        this.props.getUserPendingPermissions().then(pendingPermissionsResponse => {
          // if not a super user
          if (!this.props.isSuperUser) {
            for (let i = 0; i < pendingPermissionsResponse.length; i++) {
              currentUserPermissions.push({
                text: pendingPermissionsResponse[i][`${this.props.currentLanguage}_name`],
                description:
                  pendingPermissionsResponse[i][`${this.props.currentLanguage}_description`],
                pendingStatus: true
              });
            }
          }
          // saving populated array in the state
          this.setState({ permissions: currentUserPermissions });
        });
      });
  };

  // populate permissions definition for add permission popup based on all existing permissions
  populatePermissionsDefinition = () => {
    const existingPermissionsArray = [];
    this.props.getAvailablePermissions().then(response => {
      let noAvailablePermission = false;
      // if there is at least one permission available
      if (response.length > 0) {
        // looping in custom permissions
        for (let i = 0; i < response.length; i++) {
          // pushing results in existingPermissionsArray array
          existingPermissionsArray.push({
            permission_id: response[i].permission_id,
            name: response[i][`${this.props.currentLanguage}_name`],
            description: response[i][`${this.props.currentLanguage}_description`],
            checked: false
          });
        }
      } else {
        noAvailablePermission = true;
      }
      // saving populated array in the state
      this.setState({
        permissionsDefinition: existingPermissionsArray,
        noAvailablePermission: noAvailablePermission
      });
    });
  };

  // populate test permissions based on the user' test permissions
  /* sorting sources:
      1: sorting by test versions (default)
      2: sorting by test order number
      3: sorting by expiry date
  */
  populateCurrentTestPermissions = sortingSource => {
    this.setState({ isLoadingTestPermissions: true }, () => {
      const currentTestPermissionsArray = [];
      this.props
        .getTestPermissions()
        .then(response => {
          // looping in test permissions
          for (let i = 0; i < response.length; i++) {
            // pushing results in currentTestPermissionsArray array
            currentTestPermissionsArray.push({
              en_test_name: response[i].test.en_name,
              fr_test_name: response[i].test.fr_name,
              test_order_number: response[i].test_order_number,
              staffing_process_number: response[i].staffing_process_number,
              expiry_date: response[i].expiry_date
            });
            // sorting test permissions
            if (sortingSource === 1) {
              currentTestPermissionsArray.sort(this.sortByTestVersion);
            } else if (sortingSource === 2) {
              currentTestPermissionsArray.sort(this.sortByTestOrderNumber);
            } else if (sortingSource === 3) {
              currentTestPermissionsArray.sort(this.sortByStaffingProcessNumber);
            } else if (sortingSource === 4) {
              currentTestPermissionsArray.sort(this.sortByExpiryDate);
            }
            // saving populated array in the state
            this.setState({
              testPermissions: currentTestPermissionsArray
            });
          }
        })
        .then(() => {
          this.setState({ isLoadingTestPermissions: false });
        });
    });
  };

  // sorting functions (https://www.sitepoint.com/sort-an-array-of-objects-in-javascript/)
  // sorting by test versions
  sortByTestVersion = (a, b) => {
    let item_a = "";
    let item_b = "";
    if (this.props.currentLanguage === LANGUAGES.english) {
      item_a = a.en_test_name.toUpperCase();
      item_b = b.en_test_name.toUpperCase();
    } else {
      item_a = a.fr_test_name.toUpperCase();
      item_b = b.fr_test_name.toUpperCase();
    }

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // sorting by test order number
  sortByTestOrderNumber = (a, b) => {
    const item_a = a.test_order_number.toUpperCase();
    const item_b = b.test_order_number.toUpperCase();

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // sorting by staffing process number
  sortByStaffingProcessNumber = (a, b) => {
    const item_a = a.staffing_process_number.toUpperCase();
    const item_b = b.staffing_process_number.toUpperCase();

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // sorting by expiry date
  sortByExpiryDate = (a, b) => {
    const item_a = a.expiry_date.toUpperCase();
    const item_b = b.expiry_date.toUpperCase();

    let comparison = 0;
    if (item_a > item_b) {
      comparison = 1;
    } else if (item_a < item_b) {
      comparison = -1;
    }
    return comparison;
  };

  // handle add permission action
  handleAddPermission = () => {
    // display popup box
    this.setState({ showAddPermissionPopup: true });
    // initialize permissions definition state
    this.populatePermissionsDefinition();
  };

  // closing add permission popup
  closeAddPermissionPopup = () => {
    //hide popup box
    this.setState({ showAddPermissionPopup: false });
  };

  // handling collapse test versions details funtionality
  handleCollapseTestVerionsDetails = () => {
    this.setState({ extendedTestVersionsDetails: !this.state.extendedTestVersionsDetails });
  };

  // reseting all permissionRequestData attributes except priOrMilitaryNbr
  handlePopupClose = () => {
    const { permissionRequestData } = this.props;
    this.props.updatePermissionRequestDataState(
      (permissionRequestData.gocEmail = ""),
      (permissionRequestData.supervisor = ""),
      (permissionRequestData.supervisorEmail = ""),
      (permissionRequestData.rationale = ""),
      (permissionRequestData.permissions = []),
      (permissionRequestData.isValidForm = false)
    );
    // re-populate current user' permissions
    this.populateCurrentUserPermissions();
    // re-populate permissions definition
    this.populatePermissionsDefinition();
  };

  //handle send request
  handleSendRequest = () => {
    //trigger validation
    this.setState({ triggerValidation: !this.state.triggerValidation }, () => {
      // if permission request form is valid
      if (this.props.permissionRequestData.isValidForm) {
        // getting existing pending permissions for the specified user
        this.props.getUserPendingPermissions().then(pendingPermissionsResponse => {
          // verifying for each permission checked if it is already existing in permissionrequest table (for the specified user)
          let isValidPermissionRequest = true;
          for (let i = 0; i < this.props.permissionRequestData.permissions.length; i++) {
            for (let y = 0; y < pendingPermissionsResponse.length; y++) {
              // if one permission requested already exists in permissionrequest table (for the specified user)
              if (
                this.props.permissionRequestData.permissions[i] ===
                pendingPermissionsResponse[y].permission_requested
              ) {
                // not a valid permission request
                isValidPermissionRequest = false;
              }
            }
          }
          // if valid permission request
          if (isValidPermissionRequest) {
            // looping in requested permissions
            for (let i = 0; i < this.props.permissionRequestData.permissions.length; i++) {
              // sending request for each requested permission
              this.props
                .sendPermissionRequest(
                  this.props.permissionRequestData,
                  this.props.priOrMilitaryNbr,
                  i
                )
                .then(response => {
                  // if request is sent successfully
                  if (response.status === 200) {
                    // closing popup box
                    this.closeAddPermissionPopup();
                    // should never happen
                  } else {
                    throw new Error("An error occurred during the send permission request process");
                  }
                });
            }
          }
        });
      }
    });
  };

  handleSortByTestVertions = () => {
    this.setState({ testPermissionsTableSortingElement: 1 });
  };

  handleSortByTestOrderNumber = () => {
    this.setState({ testPermissionsTableSortingElement: 2 });
  };

  handleSortByStaffingProcessNumber = () => {
    this.setState({ testPermissionsTableSortingElement: 3 });
  };

  handleSortByExpiryDate = () => {
    this.setState({ testPermissionsTableSortingElement: 4 });
  };

  render() {
    let accommodationStyles = {
      fontSize: this.props.accommodations.fontSize
    };

    return (
      <div tabIndex={-1}>
        <section aria-labelledby="permissions-section" tabIndex={0}>
          <h2 id="permissions-section">{LOCALIZE.profile.permissions.title}</h2>
          <div style={styles.container}>
            <p>{LOCALIZE.profile.permissions.description}</p>
            <div style={styles.tableContainer}>
              <div style={styles.permissionsTable.titleContainer}>
                <div style={styles.permissionsTable.title}>
                  <span>{LOCALIZE.profile.permissions.systemPermissionInformation.title}</span>
                  <StyledTooltip
                    trigger={["hover", "focus"]}
                    placement="top"
                    overlayClassName={"tooltip"}
                    overlay={
                      <div>
                        <p>
                          {LOCALIZE.profile.permissions.systemPermissionInformation.titleTooltip}
                        </p>
                      </div>
                    }
                  >
                    <Button tabIndex="-1" variant="link" style={styles.tooltipIconContainer}>
                      <FontAwesomeIcon
                        icon={faQuestionCircle}
                        style={{ ...styles.tooltipIcon, ...accommodationStyles }}
                      ></FontAwesomeIcon>
                    </Button>
                  </StyledTooltip>
                </div>
              </div>
              {this.state.permissions.length > 0 &&
                this.state.permissions.map((permission, id) => {
                  return (
                    <div
                      key={id}
                      style={
                        id === 0
                          ? {
                              ...styles.permissionsTable.rowContainerBasicStyle,
                              ...styles.permissionsTable.rowContainerLight
                            }
                          : id % 2 === 0
                          ? {
                              ...styles.permissionsTable.rowContainerBasicStyle,
                              ...styles.permissionsTable.rowContainerBorder,
                              ...styles.permissionsTable.rowContainerLight
                            }
                          : {
                              ...styles.permissionsTable.rowContainerBasicStyle,
                              ...styles.permissionsTable.rowContainerBorder,
                              ...styles.permissionsTable.rowContainerDark
                            }
                      }
                      tabIndex={0}
                      aria-labelledby={`permissions-table-title-accessibility permission-title-accessibility permission-label-${id} permission-description-accessibility permission-description-${id} permission-pending-status-accessibility permission-pending-status-${id}`}
                    >
                      <label id="permissions-table-title-accessibility" style={styles.hiddenText}>
                        {LOCALIZE.profile.permissions.systemPermissionInformation.title}:
                      </label>
                      {id === 0 && (
                        <label
                          id="permissions-table-title-tooltip-for-accessibility"
                          style={styles.hiddenText}
                        >
                          , {LOCALIZE.profile.permissions.systemPermissionInformation.titleTooltip}
                        </label>
                      )}
                      <label id="permission-title-accessibility" style={styles.hiddenText}>
                        {LOCALIZE.ariaLabel.selectedPermission}
                      </label>
                      <div
                        id={`permission-label-${id}`}
                        style={styles.permissionsTable.existingPermissions.label}
                      >
                        <span>{permission.text}</span>
                      </div>
                      <label id="permission-description-accessibility" style={styles.hiddenText}>
                        {LOCALIZE.ariaLabel.description}
                      </label>
                      <div
                        id={`permission-description-${id}`}
                        style={styles.permissionsTable.existingPermissions.description}
                      >
                        <span>{permission.description}</span>
                      </div>
                      <div
                        id={`permission-pending-status-${id}`}
                        style={styles.permissionsTable.existingPermissions.status}
                      >
                        {permission.pendingStatus ? (
                          <label
                            id="permission-pending-status-accessibility"
                            style={styles.hiddenText}
                          >
                            {LOCALIZE.ariaLabel.pendingStatus}
                          </label>
                        ) : (
                          ""
                        )}

                        <span>
                          {permission.pendingStatus
                            ? LOCALIZE.profile.permissions.systemPermissionInformation.pending
                            : ""}
                        </span>
                      </div>
                    </div>
                  );
                })}
              {!this.props.isSuperUser && !this.state.noAvailablePermission && (
                <div
                  style={
                    this.state.permissions.length % 2 === 0
                      ? {
                          ...styles.permissionsTable.addPermission.containerBasicStyle,
                          ...styles.permissionsTable.addPermission.containerLight
                        }
                      : {
                          ...styles.permissionsTable.addPermission.containerBasicStyle,
                          ...styles.permissionsTable.addPermission.containerDark
                        }
                  }
                  aria-labelledby="add-permission-button"
                >
                  <div style={styles.permissionsTable.addPermission.content}>
                    <div
                      className="add-permission-icon"
                      aria-label={LOCALIZE.ariaLabel.addPermission}
                      tabIndex={0}
                      style={styles.permissionsTable.addPermission.button}
                      onClick={this.handleAddPermission}
                    >
                      <label id="add-permission-button" style={styles.hiddenText}>
                        {LOCALIZE.ariaLabel.add}
                      </label>
                      <FontAwesomeIcon
                        icon={faPlusCircle}
                        style={styles.permissionsTable.addPermission.icon}
                      />
                      <span style={styles.permissionsTable.addPermission.label}>
                        {LOCALIZE.profile.permissions.systemPermissionInformation.addPermission}
                      </span>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </section>
        {this.state.testPermissions.length > 0 && !this.state.isLoadingTestPermissions && (
          <section
            aria-labelledby="test-permissions-section"
            style={styles.testPermissionsTable.spacer}
            tabIndex={0}
          >
            <h2 id="test-permissions-section">{LOCALIZE.profile.testPermissions.title}</h2>
            <div style={styles.container}>
              <p>{LOCALIZE.profile.testPermissions.description}</p>
              <div style={styles.tableContainer}>
                <div style={styles.testPermissionsTable.title}>
                  <span>{LOCALIZE.profile.testPermissions.table.title}</span>
                </div>
                <div style={styles.testPermissionsTable.testRowContainerTopRow}>
                  <CustomButton
                    label={
                      <>
                        <div style={styles.floatLeft}>
                          <span>{LOCALIZE.profile.testPermissions.eMIB}</span>
                        </div>
                        <div style={styles.floatRight}>
                          <span style={styles.testPermissionsTable.collapsingIconContainer}>
                            <FontAwesomeIcon
                              icon={
                                this.state.extendedTestVersionsDetails ? faAngleUp : faAngleDown
                              }
                              style={styles.testPermissionsTable.collapsingIcon}
                            />
                          </span>
                        </div>
                      </>
                    }
                    action={this.handleCollapseTestVerionsDetails}
                    customStyle={styles.testPermissionsTable.collapsingRowContainer}
                    buttonTheme={"test-row-container-button"}
                    ariaLabel={LOCALIZE.settings.systemSettings}
                  />
                  {this.state.extendedTestVersionsDetails && (
                    <div>
                      <table style={styles.testPermissionsTable.container} tabIndex={0}>
                        <thead>
                          <tr style={styles.testPermissionsTable.testVersionsTable.head}>
                            <th
                              scope="col"
                              style={styles.testPermissionsTable.testVersionsTable.testVersionTitle}
                            >
                              <span id="test-access-title-column">
                                {LOCALIZE.profile.testPermissions.table.column1}
                              </span>
                              <button
                                style={styles.testPermissionsTable.testVersionsTable.iconButton}
                                onClick={this.handleSortByTestVertions}
                              >
                                <FontAwesomeIcon
                                  icon={
                                    this.state.testPermissionsTableSortingElement === 1
                                      ? faSortDown
                                      : faSort
                                  }
                                  style={
                                    this.state.testPermissionsTableSortingElement === 1
                                      ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                      : styles.testPermissionsTable.testVersionsTable.sortIcon
                                  }
                                ></FontAwesomeIcon>
                                <span style={styles.hiddenText}>
                                  {LOCALIZE.profile.testPermissions.table.column1OrderItem}
                                </span>
                              </button>
                            </th>
                            <th
                              scope="col"
                              style={styles.testPermissionsTable.testVersionsTable.title}
                            >
                              <span id="test-order-number-column">
                                {LOCALIZE.profile.testPermissions.table.column2}
                              </span>
                              <button
                                style={styles.testPermissionsTable.testVersionsTable.iconButton}
                                onClick={this.handleSortByTestOrderNumber}
                              >
                                <FontAwesomeIcon
                                  icon={
                                    this.state.testPermissionsTableSortingElement === 2
                                      ? faSortDown
                                      : faSort
                                  }
                                  style={
                                    this.state.testPermissionsTableSortingElement === 2
                                      ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                      : styles.testPermissionsTable.testVersionsTable.sortIcon
                                  }
                                ></FontAwesomeIcon>
                                <span style={styles.hiddenText}>
                                  {LOCALIZE.profile.testPermissions.table.column2OrderItem}
                                </span>
                              </button>
                            </th>
                            <th
                              scope="col"
                              style={styles.testPermissionsTable.testVersionsTable.title}
                            >
                              <span id="staffing-process-number-column">
                                {LOCALIZE.profile.testPermissions.table.column3}
                              </span>
                              <button
                                style={styles.testPermissionsTable.testVersionsTable.iconButton}
                                onClick={this.handleSortByStaffingProcessNumber}
                              >
                                <FontAwesomeIcon
                                  icon={
                                    this.state.testPermissionsTableSortingElement === 3
                                      ? faSortDown
                                      : faSort
                                  }
                                  style={
                                    this.state.testPermissionsTableSortingElement === 3
                                      ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                      : styles.testPermissionsTable.testVersionsTable.sortIcon
                                  }
                                ></FontAwesomeIcon>
                                <span style={styles.hiddenText}>
                                  {LOCALIZE.profile.testPermissions.table.column3OrderItem}
                                </span>
                              </button>
                            </th>
                            <th
                              scope="col"
                              style={styles.testPermissionsTable.testVersionsTable.title}
                            >
                              <span id="expiry-date-column">
                                {LOCALIZE.profile.testPermissions.table.column4}
                              </span>
                              <button
                                style={styles.testPermissionsTable.testVersionsTable.iconButton}
                                onClick={this.handleSortByExpiryDate}
                              >
                                <FontAwesomeIcon
                                  icon={
                                    this.state.testPermissionsTableSortingElement === 3
                                      ? faSortDown
                                      : faSort
                                  }
                                  style={
                                    this.state.testPermissionsTableSortingElement === 3
                                      ? styles.testPermissionsTable.testVersionsTable.sortIconActif
                                      : styles.testPermissionsTable.testVersionsTable.sortIcon
                                  }
                                ></FontAwesomeIcon>
                                <span style={styles.hiddenText}>
                                  {LOCALIZE.profile.testPermissions.table.column4OrderItem}
                                </span>
                              </button>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.testPermissions.map((testPermission, id) => (
                            <tr
                              key={id}
                              style={styles.testPermissionsTable.testPermissionsTableRowContainer}
                              tabIndex={0}
                              aria-labelledby={`test-access-title-column test-permission-label-${id} test-order-number-column test-permission-order-number-${id} staffing-process-number-column test-permission-staffing-process-number-${id} expiry-date-column test-permission-expiration-${id}`}
                            >
                              <td
                                id={`test-permission-label-${id}`}
                                style={
                                  styles.testPermissionsTable.testVersionsTable.testVersionLabel
                                }
                              >
                                <span>
                                  {this.props.currentLanguage === LANGUAGES.english
                                    ? testPermission.en_test_name
                                    : testPermission.fr_test_name}
                                </span>
                              </td>
                              <td
                                id={`test-permission-order-number-${id}`}
                                style={
                                  styles.testPermissionsTable.testVersionsTable.orderNumberLabel
                                }
                              >
                                <span>{testPermission.test_order_number}</span>
                              </td>
                              <td
                                id={`test-permission-staffing-process-number-${id}`}
                                style={
                                  styles.testPermissionsTable.testVersionsTable.orderNumberLabel
                                }
                              >
                                <span>{testPermission.staffing_process_number}</span>
                              </td>
                              <td
                                id={`test-permission-expiration-${id}`}
                                style={
                                  styles.testPermissionsTable.testVersionsTable.expirationLabel
                                }
                              >
                                <span>{testPermission.expiry_date}</span>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </section>
        )}
        {this.state.isLoadingTestPermissions && (
          <div style={styles.loading}>
            <label className="fa fa-spinner fa-spin">
              <FontAwesomeIcon icon={faSpinner} />
            </label>
          </div>
        )}
        <PopupBox
          show={this.state.showAddPermissionPopup}
          handleClose={() => {}}
          shouldCloseOnEsc={false}
          isBackdropStatic={true}
          onPopupClose={this.handlePopupClose}
          title={LOCALIZE.profile.permissions.addPermissionPopup.title}
          description={
            <RequestPermissionForm
              permissionsDefinition={this.state.permissionsDefinition}
              triggerValidation={this.state.triggerValidation}
            />
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAddPermissionPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faShare}
          rightButtonLabel={LOCALIZE.commons.sendRequest}
          rightButtonAction={this.handleSendRequest}
        />
      </div>
    );
  }
}

export { Permissions as unconnectedPermissions };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    isSuperUser: state.user.isSuperUser,
    permissionRequestData: state.userProfile.permissionRequestData,
    priOrMilitaryNbr: state.user.priOrMilitaryNbr,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getExistingPermissions,
      getUserPermissions,
      getTestPermissions,
      resetUserProfileStates,
      updatePermissionRequestDataState,
      sendPermissionRequest,
      getUserPendingPermissions,
      getAvailablePermissions
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Permissions);
