import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons";
import SideNavigation from "../eMIB/SideNavigation";
import PersonalInfo from "./PersonalInfo";
import Password from "./Password";
import Preferences from "./Preferences";
import Permissions from "./Permissions";
import ProfileMerge from "./ProfileMerge";

const styles = {
  iconContainer: {
    paddingLeft: 6
  },
  tabContainer: {
    zIndex: 1,
    marginTop: "-185px",
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent"
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    maxWidth: 210,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  }
};

class Profile extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.setState({
          isLoaded: true
        });
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevState => {
    if (prevState.isLoaded !== this.state.isLoaded) {
      // focusing on welcome message after content load
      document.getElementById("user-welcome-message-div").focus();
    }
  };

  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getProfileSections = () => {
    return [
      {
        menuString: LOCALIZE.profile.sideNavItems.personalInfo,
        body: <PersonalInfo />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.password,
        body: <Password username={this.props.username} />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.preferences,
        body: <Preferences />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.permissions,
        body: <Permissions username={this.props.username} />
      },
      {
        menuString: LOCALIZE.profile.sideNavItems.profileMerge,
        body: <ProfileMerge />
      }
    ];
  };

  render() {
    const specs = this.getProfileSections();

    // converting current font size in Int
    const fontSizeInt = parseInt(this.props.accommodations.fontSize.substring(0, 2));
    // converting profile icon font size
    const iconStyle = { fontSize: `${fontSizeInt + 200}px` };

    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.profile}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div id="user-welcome-message-div" tabIndex={0} aria-labelledby="user-welcome-message">
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={styles.iconContainer}>
                <FontAwesomeIcon style={iconStyle} icon={faUserCircle} />
              </div>
              <div>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={styles.tabContainer}
                  tabContentStyle={styles.tabContent}
                  navStyle={styles.nav}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { Profile as unconnectedProfile };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    primaryEmail: state.user.primaryEmail,
    secondaryEmail: state.user.secondaryEmail,
    dateOfBirth: state.user.dateOfBirth,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
