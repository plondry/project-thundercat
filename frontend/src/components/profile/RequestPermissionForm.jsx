import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "../../css/profile-permissions.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button } from "react-bootstrap";
import { updatePermissionRequestDataState } from "../../modules/UserProfileRedux";
import { updatePriOrMilitaryNbr } from "../../modules/UserRedux";
import validateName, {
  validateEmail,
  validatePriOrMilitaryNbr
} from "../../helpers/regexValidator";
import StyledTooltip from "../authentication/StyledTooltip";
import "rc-tooltip/assets/bootstrap_white.css";

const styles = {
  container: {
    width: "100%",
    position: "relative"
  },
  formContainer: {
    padding: "24px 6px"
  },
  label: {
    width: "40%",
    paddingRight: 12
  },
  labelHeight: {
    minHeight: 36
  },
  rationaleLabel: {
    verticalAlign: "top",
    paddingTop: 4,
    float: "left"
  },
  input: {
    width: "60%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    marginTop: 12
  },
  rationaleInput: {
    minHeight: 85,
    overflowY: "scroll",
    resize: "none",
    display: "block"
  },
  permissionsContainer: {
    paddingTop: 24
  },
  permissionChoicesContainer: {
    width: "96%",
    margin: "0 auto",
    paddingBottom: 12
  },
  permissionContainer: {
    width: 595,
    height: 38,
    display: "table",
    margin: "12px auto 0 auto",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  clickableZone: {
    width: "100%",
    display: "table",
    minHeight: 40
  },
  permissionCheckboxAndLabelContainer: {
    width: "90%",
    display: "table-cell",
    verticalAlign: "middle"
  },
  checkbox: {
    margin: "0 12px",
    verticalAlign: "middle"
  },
  permissionLabel: {
    width: "90%",
    overflowWrap: "anywhere",
    padding: 6,
    margin: 0,
    verticalAlign: "middle"
  },
  permissionsTooltipContainer: {
    display: "table-cell",
    textAlign: "center",
    verticalAlign: "middle"
  },
  iconContainer: {
    padding: 0,
    verticalAlign: "middle"
  },
  icon: {
    minHeight: 30,
    minWidth: 40,
    //padding: 3,
    color: "#00565e",
    marginTop: "-1px",
    verticalAlign: "middle"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  gocEmailgocEmailTooltipIconContainer: {
    padding: 0
  },
  gocEmailTooltipIcon: {
    minHeight: 36,
    minWidth: 40,
    color: "#00565e",
    padding: 6
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    margin: "6px 0 0 40%"
  },
  customMarginForPermissionsRequestedError: {
    margin: "6px 0 0 20px"
  }
};

class RequestPermissionForm extends Component {
  static propTypes = {
    permissionsDefinition: PropTypes.array.isRequired,
    triggerValidation: PropTypes.bool.isRequired,
    // provided by redux
    updatePermissionRequestDataState: PropTypes.func,
    updatePriOrMilitaryNbr: PropTypes.func
  };

  state = {
    gocEmailContent: "",
    isValidGocEmail: true,
    priOrMilitaryNbrContent: this.props.priOrMilitaryNbr,
    isValidPriOrMilitaryNbr: true,
    supervisorContent: "",
    isValidSupervisor: true,
    supervisorEmailContent: "",
    isValidSupervisorEmail: true,
    rationaleContent: "",
    isValidRational: true,
    permissionsRequestedArray: [],
    isValidPermissionsRequestedArray: true
  };

  componentDidMount = () => {
    // updating permission request data (priOrMilitaryNbr attribute) based on initial value
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.priOrMilitaryNbr = this.props.priOrMilitaryNbr.toUpperCase())
    );
  };

  componentDidUpdate = prevProps => {
    // initialize permissionsRequestedArray state on permissionsDefinition props load
    if (prevProps.permissionsDefinition !== this.props.permissionsDefinition) {
      // get initial permissions checkboxes states
      this.setState({ permissionsRequestedArray: this.props.permissionsDefinition });
    }

    // when form validation gets called
    if (prevProps.triggerValidation !== this.props.triggerValidation) {
      // populating requested permissions (IDs)
      let permissionRequestedIds = [];
      // looping in existing permissions
      for (let i = 0; i < this.state.permissionsRequestedArray.length; i++) {
        // permission item is checked
        if (this.state.permissionsRequestedArray[i].checked) {
          // pushing result in permissionRequestedIds array
          permissionRequestedIds.push(this.state.permissionsRequestedArray[i].permission_id);
        }
      }
      // saving the array in redux state
      this.props.updatePermissionRequestDataState(
        (this.props.permissionRequestData.permissions = permissionRequestedIds)
      );
      // validating permission request form
      this.validateForm();
    }
  };

  // update GoC email content
  updateGocEmailContent = event => {
    const gocEmailContent = event.target.value;
    this.setState({
      gocEmailContent: gocEmailContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.gocEmail = gocEmailContent.toLowerCase())
    );
  };

  // update supervisor content
  updatePriOrMilitaryNbrContent = event => {
    const priOrMilitaryNbrContent = event.target.value;
    /* only the following can be inserted into this field:
          - 1 letter followed by 0 to 6 numbers
          - 0 to 9 numbers
    */
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$|^([0-9]{0,9})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        priOrMilitaryNbrContent: priOrMilitaryNbrContent
      });
      // updating priOrMilitaryNbr redux state
      this.props.updatePriOrMilitaryNbr(priOrMilitaryNbrContent);
    }
  };

  // update supervisor content
  updateSupervisorContent = event => {
    const supervisorContent = event.target.value;
    this.setState({
      supervisorContent: supervisorContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.supervisor = supervisorContent)
    );
  };

  // update supervisor email content
  updateSupervisorEmailContent = event => {
    const supervisorEmailContent = event.target.value;
    this.setState({
      supervisorEmailContent: supervisorEmailContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.supervisorEmail = supervisorEmailContent.toLowerCase())
    );
  };

  // update rationale content
  updateRationaleContent = event => {
    const rationaleContent = event.target.value;
    this.setState({
      rationaleContent: rationaleContent
    });
    // updating permission request data redux state
    this.props.updatePermissionRequestDataState(
      (this.props.permissionRequestData.rationale = rationaleContent)
    );
  };

  // update permissions requested checkboxes status
  togglePermissionCheckbox = id => {
    let updatedPermissionsRequestedArray = Array.from(this.state.permissionsRequestedArray);
    updatedPermissionsRequestedArray[id].checked = !updatedPermissionsRequestedArray[id].checked;
    this.setState({ permissionsRequestedArray: updatedPermissionsRequestedArray });
  };

  // handle form validation
  validateForm = () => {
    const {
      gocEmailContent,
      priOrMilitaryNbrContent,
      supervisorContent,
      supervisorEmailContent,
      rationaleContent,
      permissionsRequestedArray
    } = this.state;

    // goc email validation
    const isValidGocEmail = validateEmail(gocEmailContent);

    // pri or military number validation
    const isValidPriOrMilitaryNbr =
      priOrMilitaryNbrContent !== "" ? validatePriOrMilitaryNbr(priOrMilitaryNbrContent) : false;

    // supervisor name validation
    const isValidSupervisor = validateName(supervisorContent);

    // supervisor email validation
    const isValidSupervisorEmail = validateEmail(supervisorEmailContent);

    // rationale validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidRational = regexExpression.test(rationaleContent) ? true : false;

    // permissions choice validation
    let isValidPermissionsRequestedArray = false;
    // looping in permissionsRequestedArray
    for (let i = 0; i < permissionsRequestedArray.length; i++) {
      // if at least one permission is requested, this section is valid
      if (permissionsRequestedArray[i].checked) {
        isValidPermissionsRequestedArray = true;
      }
    }

    // saving all validation results in states
    this.setState(
      {
        isValidGocEmail: isValidGocEmail,
        isValidPriOrMilitaryNbr: isValidPriOrMilitaryNbr,
        isValidSupervisor: isValidSupervisor,
        isValidSupervisorEmail: isValidSupervisorEmail,
        isValidRational: isValidRational,
        isValidPermissionsRequestedArray: isValidPermissionsRequestedArray
      },
      () => {
        this.focusOnHighestErrorField();
      }
    );

    // checking if all validations are met
    if (
      isValidGocEmail &&
      isValidPriOrMilitaryNbr &&
      isValidSupervisor &&
      isValidSupervisorEmail &&
      isValidRational &&
      isValidPermissionsRequestedArray
    ) {
      // updating permission request data redux state
      this.props.updatePermissionRequestDataState(
        (this.props.permissionRequestData.isValidForm = true)
      );
    } else {
      // updating permission request data redux state
      this.props.updatePermissionRequestDataState(
        (this.props.permissionRequestData.isValidForm = false)
      );
    }
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidGocEmail) {
      document.getElementById("goc-email").focus();
    } else if (!this.state.isValidPriOrMilitaryNbr) {
      document.getElementById("pri-or-military-nbr").focus();
    } else if (!this.state.isValidSupervisor) {
      document.getElementById("supervisor").focus();
    } else if (!this.state.isValidSupervisorEmail) {
      document.getElementById("supervisor-email").focus();
    } else if (!this.state.isValidRational) {
      document.getElementById("rationale").focus();
    } else if (!this.state.isValidPermissionsRequestedArray) {
      document.getElementById("permissions-error").focus();
    }
  };

  render() {
    const {
      gocEmailContent,
      isValidGocEmail,
      priOrMilitaryNbrContent,
      isValidPriOrMilitaryNbr,
      supervisorContent,
      isValidSupervisor,
      supervisorEmailContent,
      isValidSupervisorEmail,
      rationaleContent,
      isValidRational,
      isValidPermissionsRequestedArray
    } = this.state;

    const accommodationsStyle = {
      fontSize: this.props.accommodations.fontSize
    };

    // converting current font size in Int
    const fontSizeInt = parseInt(this.props.accommodations.fontSize.substring(0, 2));
    // initializing transform scale (used for checkbox size depending on the font size selected)
    let checkboxTransformScale = "";
    // converting checkbox scale based on font size
    switch (true) {
      case fontSizeInt <= 22:
        checkboxTransformScale = "scale(1)";
        break;
      case fontSizeInt > 22 && fontSizeInt <= 30:
        checkboxTransformScale = "scale(1.5)";
        break;
      case fontSizeInt > 30 && fontSizeInt <= 40:
        checkboxTransformScale = "scale(2)";
        break;
      case fontSizeInt > 40:
        checkboxTransformScale = "scale(2.2)";
        break;
      default:
        checkboxTransformScale = "scale(1)";
    }

    return (
      <div style={styles.container}>
        <p>{LOCALIZE.profile.permissions.addPermissionPopup.title}</p>
        <div style={styles.formContainer}>
          <div>
            <div>
              <label style={{ ...styles.label, ...styles.labelHeight }}>
                <label id="goc-email-title">
                  {LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}
                </label>
                <StyledTooltip
                  trigger={["hover", "focus"]}
                  placement="top"
                  overlayClassName={"tooltip"}
                  overlay={
                    <div>
                      <p>{LOCALIZE.profile.permissions.addPermissionPopup.gocEmailTooltip}</p>
                    </div>
                  }
                >
                  <Button
                    tabIndex="-1"
                    variant="link"
                    style={styles.gocEmailgocEmailTooltipIconContainer}
                  >
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={{ ...styles.gocEmailTooltipIcon, ...accommodationsStyle }}
                    ></FontAwesomeIcon>
                  </Button>
                </StyledTooltip>
                <label id="goc-email-tooltip-for-accessibility" style={styles.hiddenText}>
                  , {LOCALIZE.profile.permissions.addPermissionPopup.gocEmailTooltip}
                </label>
              </label>
              <input
                id="goc-email"
                className={isValidGocEmail ? "valid-field" : "invalid-field"}
                aria-labelledby="goc-email-title goc-email-error goc-email-tooltip-for-accessibility"
                aria-required={true}
                aria-invalid={!isValidGocEmail}
                style={styles.input}
                type="text"
                value={gocEmailContent}
                onChange={this.updateGocEmailContent}
              ></input>
              {!isValidGocEmail && (
                <label id="goc-email-error" htmlFor="goc-email" style={styles.errorMessage}>
                  {LOCALIZE.profile.permissions.addPermissionPopup.gocEmailError}
                </label>
              )}
            </div>
            <div>
              <label id="pri-or-military-number-title" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.pri}
              </label>
              <input
                id="pri-or-military-nbr"
                className={isValidPriOrMilitaryNbr ? "valid-field" : "invalid-field"}
                aria-labelledby="pri-or-military-number-title pri-or-military-number-error"
                aria-required={true}
                aria-invalid={!isValidPriOrMilitaryNbr}
                style={styles.input}
                type="text"
                value={priOrMilitaryNbrContent}
                onChange={this.updatePriOrMilitaryNbrContent}
              ></input>
              {!isValidPriOrMilitaryNbr && (
                <label
                  id="pri-or-military-number-error"
                  htmlFor="pri-or-military-nbr"
                  style={styles.errorMessage}
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.priError}
                </label>
              )}
            </div>
            <div>
              <label id="supervisor-title" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.supervisor}
              </label>
              <input
                id="supervisor"
                className={isValidSupervisor ? "valid-field" : "invalid-field"}
                aria-labelledby="supervisor-title supervisor-error"
                aria-required={true}
                aria-invalid={!isValidSupervisor}
                style={styles.input}
                type="text"
                value={supervisorContent}
                onChange={this.updateSupervisorContent}
              ></input>
              {!isValidSupervisor && (
                <label id="supervisor-error" htmlFor="supervisor" style={styles.errorMessage}>
                  {LOCALIZE.profile.permissions.addPermissionPopup.supervisorError}
                </label>
              )}
            </div>
            <div>
              <label id="supervisor-email-title" style={styles.label}>
                {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmail}
              </label>
              <input
                id="supervisor-email"
                className={isValidSupervisorEmail ? "valid-field" : "invalid-field"}
                aria-labelledby="supervisor-email-title supervisor-email-error"
                aria-required={true}
                aria-invalid={!isValidSupervisorEmail}
                style={styles.input}
                type="text"
                value={supervisorEmailContent}
                onChange={this.updateSupervisorEmailContent}
              ></input>
              {!isValidSupervisorEmail && (
                <label
                  id="supervisor-email-error"
                  htmlFor="supervisor-email"
                  style={styles.errorMessage}
                >
                  {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmailError}
                </label>
              )}
            </div>
            <div>
              <label id="rationale-title" style={{ ...styles.label, ...styles.rationaleLabel }}>
                {LOCALIZE.profile.permissions.addPermissionPopup.rationale}
              </label>
              <textarea
                id="rationale"
                className={isValidRational ? "valid-field" : "invalid-field"}
                aria-labelledby="rationale-title rationale-error"
                aria-required={true}
                aria-invalid={!isValidRational}
                style={{ ...styles.input, ...styles.rationaleInput }}
                value={rationaleContent}
                onChange={this.updateRationaleContent}
                maxLength="300"
              ></textarea>
              {!isValidRational && (
                <label id="rationale-error" htmlFor="rationale" style={styles.errorMessage}>
                  {LOCALIZE.profile.permissions.addPermissionPopup.rationaleError}
                </label>
              )}
            </div>
          </div>
          <div style={styles.permissionsContainer}>
            <label id="permissions-title">
              {LOCALIZE.profile.permissions.addPermissionPopup.permissions}
            </label>
            <div
              className={!isValidPermissionsRequestedArray ? "invalid-field" : ""}
              style={styles.permissionChoicesContainer}
            >
              {this.props.permissionsDefinition.map((permission, id) => {
                return (
                  <div key={id} style={styles.permissionContainer}>
                    <div
                      style={styles.clickableZone}
                      onClick={() => {
                        this.togglePermissionCheckbox(id);
                      }}
                    >
                      <div style={styles.permissionCheckboxAndLabelContainer}>
                        <input
                          id={`permission-checkbox-${id}`}
                          aria-describedby={`permissions-title permission-name-${id} permission-description-${id}`}
                          aria-invalid={!isValidPermissionsRequestedArray}
                          type="checkbox"
                          style={{ ...styles.checkbox, ...{ transform: checkboxTransformScale } }}
                          checked={permission.checked}
                          onChange={() => {}}
                        ></input>
                        <label
                          htmlFor={`permission-checkbox-${id}`}
                          id={`permission-name-${id}`}
                          style={styles.permissionLabel}
                          onClick={() => {
                            this.togglePermissionCheckbox(id);
                          }}
                        >
                          {permission.name}
                        </label>
                      </div>
                      <StyledTooltip
                        trigger={["hover", "focus"]}
                        placement="left"
                        overlayClassName={"tooltip"}
                        overlay={
                          <div>
                            <p>{permission.description}</p>
                          </div>
                        }
                      >
                        <div style={styles.permissionsTooltipContainer}>
                          <Button tabIndex="-1" variant="link" style={styles.iconContainer}>
                            <FontAwesomeIcon
                              icon={faQuestionCircle}
                              style={{ ...styles.icon, ...accommodationsStyle }}
                            ></FontAwesomeIcon>
                          </Button>
                        </div>
                      </StyledTooltip>
                      <label id={`permission-description-${id}`} style={styles.hiddenText}>
                        {permission.description}
                      </label>
                    </div>
                  </div>
                );
              })}
            </div>
            {!isValidPermissionsRequestedArray && (
              <label
                id="permissions-error"
                tabIndex={0}
                style={{
                  ...styles.errorMessage,
                  ...styles.customMarginForPermissionsRequestedError
                }}
              >
                {LOCALIZE.profile.permissions.addPermissionPopup.permissionsError}
              </label>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export { RequestPermissionForm as unconnectedRequestPermissionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    permissionRequestData: state.userProfile.permissionRequestData,
    priOrMilitaryNbr: state.user.priOrMilitaryNbr,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updatePermissionRequestDataState,
      updatePriOrMilitaryNbr
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(RequestPermissionForm);
