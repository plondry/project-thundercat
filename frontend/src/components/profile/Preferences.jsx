import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FontSize from "../commons/FontSize";
import FontFamily from "../commons/FontFamily";
import Spacing from "../commons/Spacing";

const styles = {
  container: {
    padding: "12px 0 0 24px"
  },
  contentContainer: {
    display: "inline-block",
    marginTop: 24
  },
  subtitle: {
    fontWeight: "bold"
  },
  checkbox: {
    margin: "0 24px 0 48px",
    transform: "scale(1.5)",
    verticalAlign: "middle"
  },
  label: {
    margin: 0,
    padding: 6
  }
};

// notifications preferences definition
const notificationsPreferences = () => {
  return [
    { text: LOCALIZE.profile.preferences.notifications.checkBoxOne, checked: false },
    { text: LOCALIZE.profile.preferences.notifications.checkBoxTwo, checked: false }
  ];
};

// display preferences definition
const displayPreferences = () => {
  return [
    { text: LOCALIZE.profile.preferences.display.checkBoxOne, checked: false },
    { text: LOCALIZE.profile.preferences.display.checkBoxTwo, checked: false }
  ];
};

class Preferences extends Component {
  state = {
    notificationsPreferences: notificationsPreferences(),
    displayPreferences: displayPreferences()
  };

  // update notifications checkboxes status
  toggleNotificationsCheckbox = id => {
    let updatedNotificationsPreferences = Array.from(this.state.notificationsPreferences);
    updatedNotificationsPreferences[id].checked = !updatedNotificationsPreferences[id].checked;
    this.setState({ notificationsPreferences: updatedNotificationsPreferences });
  };

  // update display checkboxes status
  toggleDisplayCheckbox = id => {
    let updatedDisplayPreferences = Array.from(this.state.displayPreferences);
    updatedDisplayPreferences[id].checked = !updatedDisplayPreferences[id].checked;
    this.setState({ displayPreferences: updatedDisplayPreferences });
  };

  render() {
    return (
      <div>
        <h2>{LOCALIZE.profile.preferences.title}</h2>
        <div style={styles.container}>
          <p>{LOCALIZE.profile.preferences.description}</p>
          <div style={styles.contentContainer}>
            {/* TODO: Uncomment related code in Preferences.test when we'll put back these preferences /*}
            {/* <span style={styles.subtitle}>{LOCALIZE.profile.preferences.notifications.title}</span>
            <div>
              {this.state.notificationsPreferences.map((preference, id) => {
                return (
                  <div key={id}>
                    <input
                      id={`notification-checkbox-${id}`}
                      type="checkbox"
                      style={styles.checkbox}
                      checked={preference.checked}
                      onChange={() => {
                        this.toggleNotificationsCheckbox(id);
                      }}
                    ></input>
                    <label htmlFor={`notification-checkbox-${id}`} style={styles.label}>
                      {preference.text}
                    </label>
                  </div>
                );
              })}
            </div>
          </div>
          <div style={styles.contentContainer}>
            <span style={styles.subtitle}>{LOCALIZE.profile.preferences.display.title}</span>
            <div>
              {this.state.displayPreferences.map((preference, id) => {
                return (
                  <div key={id}>
                    <input
                      id={`display-checkbox-${id}`}
                      type="checkbox"
                      style={styles.checkbox}
                      checked={preference.checked}
                      onChange={() => {
                        this.toggleDisplayCheckbox(id);
                      }}
                    ></input>
                    <label htmlFor={`display-checkbox-${id}`} style={styles.label}>
                      {preference.text}
                    </label>
                  </div>
                );
              })}
            </div> */}
          </div>
          <FontSize />
          <FontFamily />
          <Spacing />
        </div>
      </div>
    );
  }
}

export { Preferences as unconnectedPreferences };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Preferences);
