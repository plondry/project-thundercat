import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { faSave } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import { updateUserPersonalInfo } from "../../modules/UserProfileRedux";
import { validatePriOrMilitaryNbr, validateEmail } from "../../helpers/regexValidator";
import { setUserInformation } from "../../modules/UserRedux";
import StyledTooltip from "../authentication/StyledTooltip";
import CustomButton, { THEME } from "../commons/CustomButton";
import DatePicker from "../commons/DatePicker";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

// CSS
import "../../css/personal-info.css";
import "rc-tooltip/assets/bootstrap_white.css";

// Constants
import LOCALIZE from "../../text_resources";

const styles = {
  mainContent: {
    padding: "12px 0 0 24px"
  },
  input: {
    width: 270,
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4
  },
  secondaryEmailLabel: {
    padding: "3px 0px 0 4px"
  },
  label: {
    padding: "3px 24px 0 4px"
  },
  optionalLabel: {
    paddingLeft: 4
  },
  dateOfBirthContainer: {
    display: "flex"
  },
  yearField: {
    width: 95,
    marginRight: 36
  },
  monthAndDayField: {
    width: 75,
    marginRight: 36
  },
  titleFieldLabel: {
    margin: 0
  },
  priContainer: {
    display: "inline-block"
  },
  floatLeft: {
    float: "left"
  },
  floatRight: {
    float: "right"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipMarginTop: {
    marginTop: 1
  },
  tooltipIcon: {
    minHeight: 36,
    minWidth: 40,
    color: "#00565e",
    padding: 6
  },
  secondaryEmailTooltipIcon: {
    minHeight: 22,
    minWidth: 24,
    color: "#00565e",
    paddingLeft: 5
  },
  updateButtonContainer: {
    float: "right"
  },
  updateButton: {
    marginRight: 30,
    minWidth: 100
  },
  buttonIcon: {
    transform: "scale(2)",
    padding: "1.5px"
  },
  saveButtonLabel: {
    marginLeft: 18
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  noMargin: {
    margin: 0
  }
};

class PersonalInfo extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // provided by redux
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    primaryEmail: PropTypes.string.isRequired,
    secondaryEmail: PropTypes.string,
    dateOfBirth: PropTypes.string.isRequired,
    priOrMilitaryNbr: PropTypes.string,
    updateUserPersonalInfo: PropTypes.func,
    setUserInformation: PropTypes.func
  };

  state = {
    firstNameContent: this.props.firstName,
    lastNameContent: this.props.lastName,
    primaryEmailContent: this.props.primaryEmail,
    secondaryEmailContent: this.props.secondaryEmail === null ? "" : this.props.secondaryEmail,
    isValidSecondaryEmail: true,
    dateOfBirthYearOptions: [],
    dateOfBirthYearSelectedValue: {
      value: parseInt(this.props.dateOfBirth.split("-")[0]),
      label: `${this.props.dateOfBirth.split("-")[0]}`
    },
    dateOfBirthMonthOptions: [],
    dateOfBirthMonthSelectedValue: {
      value: parseInt(this.props.dateOfBirth.split("-")[1]),
      label: `${this.props.dateOfBirth.split("-")[1]}`
    },
    dateOfBirthDayOptions: [],
    dateOfBirthDaySelectedValue: {
      value: parseInt(this.props.dateOfBirth.split("-")[2]),
      label: `${this.props.dateOfBirth.split("-")[2]}`
    },
    priOrMilitaryNbrContent: this.props.priOrMilitaryNbr,
    isValidPriOrMilitaryNbr: true,
    displayDataUpdatedConfirmationMsgPopup: false
  };

  handleCloseSaveConfirmationPopup = () => {
    this.setState({ displayDataUpdatedConfirmationMsgPopup: false });
  };

  componentDidMount = () => {
    this._isMounted = true;
    this.populateDateOfBirthYearOptions();
    this.populateDateOfBirthMonthOptions();
    this.populateDateOfBirthDayOptions();
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevProps => {
    // update first name field based on the prop value
    if (prevProps.firstName !== this.props.firstName) {
      this.setState({ firstNameContent: this.props.firstName });
    }
    // update last name field based on the prop value
    if (prevProps.lastName !== this.props.lastName) {
      this.setState({ lastNameContent: this.props.lastName });
    }
    // update primary email field based on the prop value
    if (prevProps.primaryEmail !== this.props.primaryEmail) {
      this.setState({ primaryEmailContent: this.props.primaryEmail });
    }
    // update secondary email field based on the prop value
    if (prevProps.secondaryEmail !== this.props.secondaryEmail) {
      this.setState({
        secondaryEmailContent: this.props.secondaryEmail === null ? "" : this.props.secondaryEmail
      });
    }
    // update date of birth selected value field based on the prop value
    if (prevProps.dateOfBirth !== this.props.dateOfBirth) {
      this.setState({
        // year
        dateOfBirthYearSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[0]),
          label: `${this.props.dateOfBirth.split("-")[0]}`
        },
        // month
        dateOfBirthMonthSelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[1]),
          label: `${this.props.dateOfBirth.split("-")[1]}`
        },
        // day
        dateOfBirthDaySelectedValue: {
          value: parseInt(this.props.dateOfBirth.split("-")[2]),
          label: `${this.props.dateOfBirth.split("-")[2]}`
        }
      });
    }
    // update pri or military number field based on the prop value
    if (prevProps.priOrMilitaryNbr !== this.props.priOrMilitaryNbr) {
      this.setState({ priOrMilitaryNbrContent: this.props.priOrMilitaryNbr });
    }
  };

  // populate year (DOB) from 1900 to current year
  populateDateOfBirthYearOptions = () => {
    const yearOptionsArray = [];
    // loop from 1900 to current year
    for (let i = 1900; i <= new Date().getFullYear(); i++) {
      // push each value in yearOptionsArray
      yearOptionsArray.push({ value: i, label: `${i}` });
    }
    // sorting (descending order)
    yearOptionsArray.reverse();
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthYearOptions: yearOptionsArray });
    }
  };

  // populate month (DOB)
  populateDateOfBirthMonthOptions = () => {
    const monthOptionsArray = [];
    // loop from 1 to 12
    for (let i = 1; i <= 12; i++) {
      // push each value in monthOptionsArray
      monthOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthMonthOptions: monthOptionsArray });
    }
  };

  // populate day (DOB)
  populateDateOfBirthDayOptions = () => {
    const dayOptionsArray = [];
    // loop from 1 to 31
    for (let i = 1; i <= 31; i++) {
      // push each value in dayOptionsArray
      dayOptionsArray.push({ value: i, label: `${i}` });
    }
    // saving result in state
    if (this._isMounted) {
      this.setState({ dateOfBirthDayOptions: dayOptionsArray });
    }
  };

  // update first name value
  updateFirstNameValue = event => {
    const firstNameContent = event.target.value;
    this.setState({
      firstNameContent
    });
  };

  // update last name value
  updateLastNameValue = event => {
    const lastNameContent = event.target.value;
    this.setState({
      lastNameContent
    });
  };

  // update primary email value
  updatePrimaryEmailValue = event => {
    const primaryEmailContent = event.target.value;
    this.setState({
      primaryEmailContent
    });
  };

  // update secondary email value
  updateSecondaryEmailValue = event => {
    const secondaryEmailContent = event.target.value;
    this.setState({
      secondaryEmailContent
    });
  };

  // get selected year (DOB)
  getSelectedDateOfBirthYear = selectedOption => {
    this.setState({ dateOfBirthYearSelectedValue: selectedOption });
  };

  // get selected month (DOB)
  getSelectedDateOfBirthMonth = selectedOption => {
    this.setState({ dateOfBirthMonthSelectedValue: selectedOption });
  };

  // get selected day (DOB)
  getSelectedDateOfBirthDay = selectedOption => {
    this.setState({ dateOfBirthDaySelectedValue: selectedOption });
  };

  // update pri or military number value
  updatePriOrMilitaryNbrValue = event => {
    const priOrMilitaryNbrContent = event.target.value;
    /* only the following can be inserted into this field:
          - 1 letter followed by 0 to 6 numbers
          - 0 to 9 numbers
    */
    const regex = /^(([A-Za-z]{1})([0-9]{0,6}))$|^([0-9]{0,9})$/;
    if (event.target.value === "" || regex.test(event.target.value)) {
      this.setState({
        priOrMilitaryNbrContent
      });
    }
  };

  // save user personal info in the database
  handleSaveData = () => {
    // data is valid
    if (this.validatePersonalInfoData()) {
      const data = {
        firstName: this.state.firstNameContent,
        lastName: this.state.lastNameContent,
        username: this.state.primaryEmailContent, // should be username if we're adding username field
        primaryEmail: this.state.primaryEmailContent,
        secondaryEmail: this.state.secondaryEmailContent,
        dob: `${this.state.dateOfBirthYearSelectedValue.label}-${this.state.dateOfBirthMonthSelectedValue.label}-${this.state.dateOfBirthDaySelectedValue.label}`,
        priOrMilitaryNbr: this.state.priOrMilitaryNbrContent.toUpperCase()
      };
      this.props.setUserInformation(
        data.firstName,
        data.lastName,
        data.username,
        this.props.isSuperUser,
        this.props.lastPasswordChange, // this might not be right....
        data.primaryEmail,
        data.secondaryEmail,
        data.dob,
        data.priOrMilitaryNbr
      );
      this.props.updateUserPersonalInfo(data).then(response => {
        // request succeeded
        if (response === 200) {
          this.setState({ displayDataUpdatedConfirmationMsgPopup: true });
          // should never happen
        } else {
          throw new Error("An error has occurred during the data update");
        }
      });
    }
  };

  // validate personal info data that is editable only (needs to be done before sending data in the database)
  validatePersonalInfoData = () => {
    // validating PRI or Military number field
    const isValidPriOrMilitaryNbr = validatePriOrMilitaryNbr(this.state.priOrMilitaryNbrContent);

    // validating secondary email field
    let isValidSecondaryEmail = false;
    // if secondary email string is not empty, validate it using the regex validator
    if (this.state.secondaryEmailContent !== "") {
      isValidSecondaryEmail = validateEmail(this.state.secondaryEmailContent);
      // else the field is empty, so valid since this is not a mandatory field
    } else {
      isValidSecondaryEmail = true;
    }

    // updating fields state
    this.setState(
      {
        isValidPriOrMilitaryNbr,
        isValidSecondaryEmail
      },
      () => {
        // focusing on highest invalid field
        this.focusOnHighestInvalidField();
      }
    );

    // making sure that all needed fields are valid
    if (isValidPriOrMilitaryNbr && isValidSecondaryEmail) {
      // whole data is valid
      return true;
      // there is at least one invalid field
    }
    return false;
  };

  // focus on highest invalid field
  focusOnHighestInvalidField = () => {
    if (!this.state.isValidSecondaryEmail) {
      document.getElementById("secondary-email-input").focus();
    } else if (!this.state.isValidPriOrMilitaryNbr) {
      document.getElementById("pri-or-military-number-input").focus();
    }
  };

  render() {
    const {
      firstNameContent,
      lastNameContent,
      primaryEmailContent,
      secondaryEmailContent,
      isValidSecondaryEmail,
      // TODO (fnormand): depending on the decision of dob editable or not, delete/uncomment all date of birth options states
      // dateOfBirthYearOptions,
      dateOfBirthYearSelectedValue,
      // dateOfBirthMonthOptions,
      dateOfBirthMonthSelectedValue,
      // dateOfBirthDayOptions,
      dateOfBirthDaySelectedValue,
      priOrMilitaryNbrContent,
      isValidPriOrMilitaryNbr
    } = this.state;

    const accommodationStyles = {
      fontSize: this.props.accommodations.fontSize
    };

    return (
      <div>
        <h2>{LOCALIZE.profile.personalInfo.title}</h2>
        <div style={styles.mainContent}>
          <div id="personal-info-names" className="profile-personal-info-grid">
            <div id="name-title" className="profile-personal-info-first-column">
              <p>{LOCALIZE.profile.personalInfo.nameSection.title}</p>
            </div>
            <div className="profile-personal-info-second-column">
              <label id="first-name" htmlFor="first-name-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.nameSection.firstName}
              </label>
              <input
                id="first-name-input"
                disabled
                tabIndex={0}
                className="valid-field"
                aria-labelledby="name-title first-name"
                aria-required
                style={styles.input}
                type="text"
                value={firstNameContent}
                onChange={this.updateFirstNameValue}
              />
            </div>
            <div className="profile-personal-info-third-column">
              <label id="last-name" htmlFor="last-name-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.nameSection.lastName}
              </label>
              <input
                id="last-name-input"
                disabled
                className="valid-field"
                aria-labelledby="name-title last-name"
                aria-required
                style={styles.input}
                type="text"
                value={lastNameContent}
                onChange={this.updateLastNameValue}
              />
            </div>
          </div>
          <div id="personal-info-emails" className="profile-personal-info-grid">
            <div id="email-title" className="profile-personal-info-first-column">
              <p>{LOCALIZE.profile.personalInfo.emailAddressesSection.title}</p>
            </div>
            <div className="profile-personal-info-second-column">
              <label id="primary-email" htmlFor="primary-email-input" style={styles.label}>
                {LOCALIZE.profile.personalInfo.emailAddressesSection.primary}
              </label>
              <input
                id="primary-email-input"
                disabled
                className="valid-field"
                aria-labelledby="email-title primary-email"
                aria-required
                style={styles.input}
                type="text"
                value={primaryEmailContent}
                onChange={this.updatePrimaryEmailValue}
              />
            </div>
            <div className="profile-personal-info-third-column">
              <label
                id="secondary-email"
                htmlFor="secondary-email-input"
                style={styles.secondaryEmailLabel}
              >
                {LOCALIZE.profile.personalInfo.emailAddressesSection.secondary}
              </label>
              <label htmlFor="secondary-email-input" style={styles.optionalLabel}>
                {LOCALIZE.profile.personalInfo.optionalField}
              </label>
              <label id="secondary-email-tooltip-for-accessibility" style={styles.hiddenText}>
                , {LOCALIZE.profile.personalInfo.emailAddressesSection.secondaryTooltip}
              </label>
              <StyledTooltip
                trigger={["hover", "focus"]}
                placement="right"
                overlayClassName="tooltip"
                overlay={
                  <div>
                    <p>{LOCALIZE.profile.personalInfo.emailAddressesSection.secondaryTooltip}</p>
                  </div>
                }
              >
                <Button tabIndex="-1" variant="link" style={styles.tooltipIconContainer}>
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    style={{ ...styles.secondaryEmailTooltipIcon, ...accommodationStyles }}
                  />
                </Button>
              </StyledTooltip>
              {!isValidSecondaryEmail && (
                <label
                  id="invalid-secondary-email-msg"
                  style={{ ...styles.errorMessage, ...styles.noMargin }}
                >
                  {LOCALIZE.profile.personalInfo.emailAddressesSection.emailError}
                </label>
              )}
              <input
                id="secondary-email-input"
                className={isValidSecondaryEmail ? "valid-field" : "invalid-field"}
                aria-labelledby="email-title secondary-email secondary-email-tooltip-for-accessibility invalid-secondary-email-msg"
                aria-required={false}
                aria-invalid={!isValidSecondaryEmail}
                style={styles.input}
                type="text"
                value={secondaryEmailContent}
                onChange={this.updateSecondaryEmailValue}
              />
            </div>
          </div>
          <div
            id="personal-info-date-of-birth"
            className="profile-personal-info-grid"
            style={styles.dateOfBirthContainer}
          >
            <div id="dob-title" className="profile-personal-info-first-column">
              <p>
                <label id="date-of-birth" style={styles.titleFieldLabel}>
                  {LOCALIZE.profile.personalInfo.dateOfBirth.title}
                </label>
                <StyledTooltip
                  trigger={["hover", "focus"]}
                  placement="top"
                  overlayClassName="tooltip"
                  overlay={
                    <div>
                      <p>{LOCALIZE.profile.personalInfo.dateOfBirth.titleTooltip}</p>
                    </div>
                  }
                >
                  <Button tabIndex="-1" variant="link" style={styles.tooltipIconContainer}>
                    <FontAwesomeIcon
                      icon={faQuestionCircle}
                      style={{ ...styles.tooltipIcon, ...accommodationStyles }}
                    />
                  </Button>
                </StyledTooltip>
              </p>
            </div>
            <DatePicker
              dateLabelId="date-of-birth"
              customDayOptions={{}}
              customMonthOptions={{}}
              customYearOptions={{}}
              initialDateDayValue={dateOfBirthDaySelectedValue}
              initialDateMonthValue={dateOfBirthMonthSelectedValue}
              initialDateYearValue={dateOfBirthYearSelectedValue}
            />
          </div>
          <div
            id="personal-info-pri-or-military-nbr"
            className="profile-personal-info-grid"
            style={styles.priContainer}
          >
            <div
              id="pri-or-military-nbr-title"
              className="profile-personal-info-first-column"
              style={styles.floatLeft}
            >
              <label id="pri-or-military-number" htmlFor="pri-or-military-number-input">
                {LOCALIZE.profile.personalInfo.priOrMilitaryNbr.title}
              </label>
              <label htmlFor="pri-or-military-number-input">
                {LOCALIZE.profile.personalInfo.optionalField}
              </label>
              <StyledTooltip
                trigger={["hover", "focus"]}
                placement="bottom"
                overlayClassName="tooltip"
                overlay={
                  <div>
                    <p>{LOCALIZE.profile.personalInfo.priOrMilitaryNbr.titleTooltip}</p>
                  </div>
                }
              >
                <Button
                  tabIndex="-1"
                  variant="link"
                  style={{ ...styles.tooltipIconContainer, ...styles.tooltipMarginTop }}
                >
                  <FontAwesomeIcon
                    icon={faQuestionCircle}
                    style={{ ...styles.tooltipIcon, ...accommodationStyles }}
                  />
                </Button>
              </StyledTooltip>
              <label
                id="pri-or-military-number-tooltip-for-accessibility"
                style={styles.hiddenText}
              >
                , {LOCALIZE.profile.personalInfo.priOrMilitaryNbr.titleTooltip}
              </label>
            </div>
            <div style={styles.floatRight}>
              <div>
                <input
                  id="pri-or-military-number-input"
                  className={isValidPriOrMilitaryNbr ? "valid-field" : "invalid-field"}
                  aria-labelledby="pri-or-military-number pri-or-military-number-tooltip-for-accessibility invalid-pri-or-military-number-msg"
                  aria-required={false}
                  aria-invalid={!isValidPriOrMilitaryNbr}
                  style={styles.input}
                  type="text"
                  value={priOrMilitaryNbrContent}
                  onChange={this.updatePriOrMilitaryNbrValue}
                />
              </div>
              {!isValidPriOrMilitaryNbr && (
                <label id="invalid-pri-or-military-number-msg" style={styles.errorMessage}>
                  {LOCALIZE.profile.personalInfo.priOrMilitaryNbr.priOrMilitaryNbrError}
                </label>
              )}
            </div>
          </div>
        </div>
        <div id="data-saved-successfully" style={styles.updateButtonContainer}>
          <CustomButton
            label={
              <>
                <span>
                  <FontAwesomeIcon icon={faSave} style={styles.buttonIcon} />
                </span>
                <span style={styles.saveButtonLabel}>{LOCALIZE.commons.saveButton}</span>
              </>
            }
            action={this.handleSaveData}
            customStyle={styles.updateButton}
            buttonTheme={THEME.PRIMARY}
            ariaLabel={LOCALIZE.profile.saveButton}
            ariaDescribedBy="data-saved-successfully"
          />
        </div>
        <PopupBox
          show={this.state.displayDataUpdatedConfirmationMsgPopup}
          title={LOCALIZE.profile.personalInfo.saveConfirmationPopup.title}
          handleClose={() => {}}
          description={
            <div>
              <p>{LOCALIZE.profile.personalInfo.saveConfirmationPopup.description}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.handleCloseSaveConfirmationPopup}
        />
      </div>
    );
  }
}

export { PersonalInfo as unconnectedPersonalInfo };

const mapStateToProps = (state, ownProps) => {
  return {
    firstName: state.user.firstName,
    lastName: state.user.lastName,
    username: state.user.username,
    primaryEmail: state.user.primaryEmail,
    secondaryEmail: state.user.secondaryEmail,
    dateOfBirth: state.user.dateOfBirth,
    priOrMilitaryNbr: state.user.priOrMilitaryNbr,
    currentLanguage: state.localize.language,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateUserPersonalInfo,
      setUserInformation
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PersonalInfo);
