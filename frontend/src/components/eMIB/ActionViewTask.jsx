import React, { Component } from "react";
import PropTypes from "prop-types";
import "../../css/collapsing-item.css";
import LOCALIZE from "../../text_resources";
import EditActionDialog from "./EditActionDialog";
import { ACTION_TYPE, EDIT_MODE, actionShape, emailShape } from "./constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { deleteTaskResponse } from "../../modules/EmailInboxRedux";
import { updateEmailTestResponse, UPDATE_EMAIL_ACTIONS } from "../../modules/UpdateResponseRedux";

const styles = {
  headings: {
    fontWeight: "bold"
  },
  deleteButton: {
    margin: 4
  },
  editButton: {
    float: "right",
    margin: 4
  },
  preWrap: {
    whiteSpace: "pre-wrap"
  },
  topDivider: {
    marginTop: 16,
    paddingTop: 16,
    borderTop: "1px solid rgba(0, 0, 0, 0.1)"
  },
  disabledExampleComponent: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    padding: 16
  }
};

class ActionViewTask extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      action: actionShape,
      actionId: PropTypes.number.isRequired,
      email: emailShape.isRequired,
      // optional prop to disable the entire component
      disabled: PropTypes.bool,
      // Props from Redux
      deleteTask: PropTypes.func,
      // used to prevent collapsible container closing when editing
      userIsEditing: PropTypes.bool,
      showEditDialog: PropTypes.func,
      closeEditDialog: PropTypes.func
    };

    ActionViewTask.defaultProps = {
      disabled: false
    };
  }

  state = {
    showDeleteConfirmationDialog: false
  };

  showEditDialog = () => this.props.showEditDialog();

  closeEditDialog = () => this.props.closeEditDialog();

  showDeleteConfirmationDialog = () => {
    this.setState({ showDeleteConfirmationDialog: true });
  };

  closeDeleteConfirmationDialog = () => {
    this.setState({ showDeleteConfirmationDialog: false });
  };

  deleteResponse = () => {
    // called when the user presses confirm in the 'Delete' popup
    this.props.updateEmailTestResponse(
      UPDATE_EMAIL_ACTIONS.DELETE_TASK,
      this.props.assignedTestId,
      this.props.questionId,
      this.props.action.answerId
    );
    this.props.deleteTaskResponse(this.props.questionId, this.props.action.answerId);
  };

  render() {
    const action = this.props.action;
    const { userIsEditing, showEditDialog, closeEditDialog } = this.props;
    return (
      <div>
        <div aria-label={LOCALIZE.ariaLabel.taskDetails}>
          {!userIsEditing && (
            <div>
              <div>
                <div style={styles.headings}>{LOCALIZE.emibTest.inboxPage.taskContent.task}</div>
                <p style={styles.preWrap}>{action.task}</p>
              </div>
              <div style={styles.topDivider}>
                <div style={styles.headings}>
                  {LOCALIZE.emibTest.inboxPage.emailResponse.reasonsForAction}
                </div>
                <p style={styles.preWrap}>{action.reasonsForAction}</p>
              </div>
            </div>
          )}
          {!this.props.disabled && (
            <div>
              {!userIsEditing && (
                <div style={styles.topDivider} aria-label={LOCALIZE.ariaLabel.taskOptions}>
                  <button
                    id="unit-test-view-task-edit-button"
                    className="btn btn-primary"
                    style={styles.editButton}
                    onClick={showEditDialog}
                  >
                    {LOCALIZE.emibTest.inboxPage.emailCommons.editButton}
                  </button>
                  <button
                    id="unit-test-view-task-delete-button"
                    className="btn btn-danger"
                    style={styles.deleteButton}
                    onClick={this.showDeleteConfirmationDialog}
                  >
                    {LOCALIZE.emibTest.inboxPage.emailCommons.deleteButton}
                  </button>
                  <PopupBox
                    show={this.state.showDeleteConfirmationDialog}
                    handleClose={this.closeDeleteConfirmationDialog}
                    title={LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation.title}
                    description={
                      <div>
                        <div>
                          <SystemMessage
                            messageType={MESSAGE_TYPE.error}
                            title={
                              LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation
                                .systemMessageTitle
                            }
                            message={
                              <p>
                                {
                                  LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation
                                    .systemMessageDescription
                                }
                              </p>
                            }
                          />
                        </div>
                        <div>
                          {LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation.description}
                        </div>
                      </div>
                    }
                    leftButtonType={BUTTON_TYPE.danger}
                    leftButtonTitle={LOCALIZE.emibTest.inboxPage.emailCommons.deleteButton}
                    leftButtonAction={this.deleteResponse}
                    rightButtonType={BUTTON_TYPE.primary}
                    rightButtonTitle={LOCALIZE.commons.returnToTest}
                  />
                </div>
              )}
              <div>
                <EditActionDialog
                  email={this.props.email}
                  showDialog={userIsEditing}
                  handleClose={closeEditDialog}
                  actionType={ACTION_TYPE.task}
                  editMode={EDIT_MODE.update}
                  action={action}
                  actionId={this.props.actionId}
                  questionId={this.props.questionId}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export { ActionViewTask as UnconnectedActionViewTask };
const mapStateToProps = (state, ownProps) => {
  return {
    assignedTestId: state.assignedTest.assignedTestId
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteTaskResponse,
      updateEmailTestResponse
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(ActionViewTask);
