import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import "../../css/inbox.css";
import { emailShape } from "./constants";

const styles = {
  replyAndUser: {
    color: "#00565E"
  },
  preWrap: {
    whiteSpace: "pre-wrap"
  },
  subject: {
    fontSize: 20
  },
  metaData: {
    marginBottom: 12,
    paddingBottom: 12,
    borderBottom: "1px solid #96a8b2"
  }
};

class EmailContent extends Component {
  static propTypes = {
    email: emailShape
  };

  fieldString = field => {
    let string = "";
    for (let contact of field) {
      string += contact.label + "; ";
    }
    return string;
  };

  render() {
    const { email } = this.props;
    const toField = this.fieldString(email.to_field);
    const ccField = this.fieldString(email.cc_field);
    return (
      <div>
        <div style={styles.metaData}>
          <div style={styles.subject}>
            {LOCALIZE.emibTest.inboxPage.subject + ": " + email.subject_field}
          </div>
          <div>
            {LOCALIZE.emibTest.inboxPage.from}:{" "}
            <span style={styles.replyAndUser}>{email.from_field.label}</span>
          </div>
          <div>
            {LOCALIZE.emibTest.inboxPage.to}: <span style={styles.replyAndUser}>{toField}</span>
          </div>
          <div>
            {email.cc_field ? (
              <>
                {LOCALIZE.emibTest.inboxPage.emailCommons.cc}{" "}
                <span style={styles.replyAndUser}>{ccField}</span>
              </>
            ) : (
              <></>
            )}
          </div>
          <div>
            {LOCALIZE.emibTest.inboxPage.date}: {email.date_field}
          </div>
        </div>
        <div style={styles.preWrap}>{email.body}</div>
      </div>
    );
  }
}

export default EmailContent;
