import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import "../../css/inbox.css";
import EditActionDialog from "./EditActionDialog";
import { ACTION_TYPE, EDIT_MODE, emailShape, EMAIL_TYPE } from "./constants";
import ActionViewEmail from "./ActionViewEmail";
import ActionViewTask from "./ActionViewTask";
import CollapsingItemContainer, { ICON_TYPE } from "./CollapsingItemContainer";
import EmailContent from "../eMIB/EmailContent";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSignOutAlt,
  faTasks,
  faReply,
  faReplyAll,
  faShareSquare
} from "@fortawesome/free-solid-svg-icons";

const styles = {
  empty: {},
  header: {
    borderBottom: "1px solid #00565E",
    marginBottom: 8,
    paddingBottom: 8
  },
  headerInfo: {
    // ensures text remains on its own line
    display: "flex",
    justifyContent: "space-between"
  },
  emailId: {
    marginRight: 12,
    fontSize: 16,
    fontWeight: 700,
    color: "#252525",
    marginBottom: 8,
    marginTop: 0
  },
  email: {
    textAlign: "left",
    padding: 16,
    overflowY: "auto"
  },
  replyIcon: {
    color: "#00565E"
  },
  actionIcon: {
    marginRight: 5
  },
  addEmailButton: {
    marginRight: 20
  },
  actionArea: {
    borderTop: "1px solid #00565E",
    marginTop: 35,
    paddingTop: 16
  },
  disabledExampleComponentNoPadding: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    margin: 0
  },
  responseIconsRow: {
    display: "flex",
    width: "100%"
  },
  responseButton: {
    cursor: "pointer"
  },
  responseTypeIcons: {
    marginRight: 10,
    padding: 9,
    border: "1px solid #00565E",
    borderRadius: 4,
    width: 40,
    height: 40,
    verticalAlign: "middle"
  },
  responseTypeIconsSelected: {
    backgroundColor: "#00565E",
    color: "white"
  },
  responseTypeEmailText: {
    marginRight: 10
  },
  taskResponseIcon: {
    marginLeft: 40
  }
};

class Email extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      email: emailShape,
      emailCount: PropTypes.number,
      taskCount: PropTypes.number,
      // optional prop to disable the entire component
      disabled: PropTypes.bool
    };

    Email.defaultProps = {
      disabled: false
    };
  }

  state = {
    showAddEmailDialog: false,
    showAddTaskDialog: false
  };

  showAddEmailDialog = type => {
    this.setState({
      showAddEmailDialog: true,
      addEmailDialogType: type
    });
  };

  closeEmailDialog = () => {
    this.setState({ showAddEmailDialog: false });
  };

  showAddTaskDialog = () => {
    this.setState({ showAddTaskDialog: true });
  };

  closeTaskDialog = () => {
    this.setState({ showAddTaskDialog: false });
  };

  render() {
    const { email, emailCount, taskCount, addressBook } = this.props;
    const { addEmailDialogType, showAddEmailDialog } = this.state;
    const hasTakenAction = emailCount + taskCount > 0;
    let emailNumber = 0;
    let taskNumber = 0;
    return (
      <div>
        <div style={styles.email}>
          <div style={styles.header}>
            <div style={styles.headerInfo}>
              <div style={styles.emailId}>
                {LOCALIZE.emibTest.inboxPage.emailId.toUpperCase()}
                {email.email_id}
              </div>
              {hasTakenAction && (
                <div className="font-weight-bold">
                  <FontAwesomeIcon icon={faSignOutAlt} style={styles.replyIcon} />
                  {LOCALIZE.formatString(
                    LOCALIZE.emibTest.inboxPage.yourActions,
                    emailCount,
                    taskCount
                  )}
                </div>
              )}
            </div>
            {!this.props.disabled && !this.state.showAddTaskDialog && (
              <div style={styles.responseIconsRow}>
                <div
                  onClick={this.showAddEmailDialog.bind(this, EMAIL_TYPE.reply)}
                  style={styles.responseButton}
                  id="unit-test-email-reply-button"
                >
                  <FontAwesomeIcon
                    icon={faReply}
                    style={{
                      ...styles.responseTypeIcons,
                      ...(showAddEmailDialog && addEmailDialogType === EMAIL_TYPE.reply
                        ? styles.responseTypeIconsSelected
                        : {})
                    }}
                  />
                  <span style={styles.responseTypeEmailText}>
                    {LOCALIZE.emibTest.inboxPage.emailCommons.reply}
                  </span>
                </div>
                <div
                  onClick={this.showAddEmailDialog.bind(this, EMAIL_TYPE.replyAll)}
                  style={styles.responseButton}
                >
                  <FontAwesomeIcon
                    icon={faReplyAll}
                    style={{
                      ...styles.responseTypeIcons,
                      ...(showAddEmailDialog && addEmailDialogType === EMAIL_TYPE.replyAll
                        ? styles.responseTypeIconsSelected
                        : {})
                    }}
                  />
                  <span style={styles.responseTypeEmailText}>
                    {LOCALIZE.emibTest.inboxPage.emailCommons.replyAll}
                  </span>
                </div>
                <div
                  onClick={this.showAddEmailDialog.bind(this, EMAIL_TYPE.forward)}
                  style={styles.responseButton}
                >
                  <FontAwesomeIcon
                    icon={faShareSquare}
                    style={{
                      ...styles.responseTypeIcons,
                      ...(showAddEmailDialog && addEmailDialogType === EMAIL_TYPE.forward
                        ? styles.responseTypeIconsSelected
                        : {})
                    }}
                  />
                  <span style={styles.responseTypeEmailText}>
                    {LOCALIZE.emibTest.inboxPage.emailCommons.forward}
                  </span>
                </div>
                {!this.state.showAddEmailDialog && (
                  <div
                    onClick={this.showAddTaskDialog}
                    style={{ ...styles.responseButton, ...styles.taskResponseIcon }}
                    id="unit-test-email-task-button"
                  >
                    <FontAwesomeIcon icon={faTasks} style={styles.responseTypeIcons} />
                    Tasks
                  </div>
                )}
              </div>
            )}
            {!this.props.disabled && (
              <>
                <EditActionDialog
                  email={email}
                  emailResponseId={this.props.emailResponseId}
                  questionId={this.props.questionId}
                  addressBook={addressBook}
                  showDialog={this.state.showAddEmailDialog}
                  handleClose={this.closeEmailDialog}
                  actionType={ACTION_TYPE.email}
                  editMode={EDIT_MODE.create}
                  emailType={addEmailDialogType}
                />
                <EditActionDialog
                  email={email}
                  emailResponseId={this.props.emailResponseId}
                  questionId={this.props.questionId}
                  showDialog={this.state.showAddTaskDialog}
                  handleClose={this.closeTaskDialog}
                  actionType={ACTION_TYPE.task}
                  editMode={EDIT_MODE.create}
                />
              </>
            )}
          </div>
          <EmailContent email={email} />
          <div>
            {this.props.emailResponses.map((response, index) => {
              emailNumber++;
              return (
                <CollapsingItemContainer
                  key={index}
                  iconType={ICON_TYPE.email}
                  title={
                    <label>
                      {`${LOCALIZE.emibTest.inboxPage.emailResponse.title}${emailNumber}`}
                    </label>
                  }
                  bodyIsEditable={true}
                  body={
                    <ActionViewEmail
                      action={response}
                      actionId={response.answerId}
                      email={email}
                      addressBook={addressBook}
                      questionId={this.props.questionId}
                      disabled={this.props.disabled}
                    />
                  }
                />
              );
            })}
            {this.props.taskResponses.map((response, index) => {
              taskNumber++;
              return (
                <CollapsingItemContainer
                  key={index}
                  iconType={ICON_TYPE.task}
                  title={
                    <label>{`${LOCALIZE.emibTest.inboxPage.taskContent.title}${taskNumber}`}</label>
                  }
                  bodyIsEditable={true}
                  body={
                    <ActionViewTask
                      action={response}
                      actionId={response.answerId}
                      email={email}
                      emailSubject={email.subject}
                      questionId={this.props.questionId}
                      disabled={this.props.disabled}
                    />
                  }
                />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

export { Email as UnconnectedEmail };

const mapStateToProps = (state, ownProps) => {
  return {
    emailResponseId: state.emailInbox.emailResponseId
  };
};

export default connect(mapStateToProps, null)(Email);
