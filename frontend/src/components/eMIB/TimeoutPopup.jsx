import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import LOCALIZE from "../../text_resources";
import { deactivateTest } from "../../modules/TestStatusRedux";
import { PATH } from "../commons/Constants";
import PropTypes from "prop-types";
/* This component is just a blank page with a popup box that explains to the candidate that the time is up
The button of the popup box is redirecting the user to the confirmation page.
This blank page has been created to avoid any attempt of cheating based on if this popup box would have been added directly in the Emib component.
In this way, the candidate has no way to return in the test, since the page has already changed as soon as the time was up. */
class TimeoutPopup extends Component {
  static propTypes = {
    handleTimeout: PropTypes.func
  };

  state = {
    showPopup: true
  };

  closeTimeoutPopup = () => {
    this.setState({ showTimeoutPopup: false });
  };

  timeoutTest = () => {
    this.props.handleTimeout();
    this.props.history.push(PATH.testBase + PATH.submit);
  };

  render() {
    return (
      <div>
        <PopupBox
          show={this.state.showPopup}
          isBackdropStatic={true}
          handleClose={this.closeTimeoutPopup}
          shouldCloseOnEsc={false}
          title={LOCALIZE.emibTest.timeoutConfirmation.timeoutConfirmedTitle}
          description={
            <div>
              <p>{LOCALIZE.emibTest.timeoutConfirmation.timeoutSaved}</p>
              <p>{LOCALIZE.emibTest.timeoutConfirmation.timeoutIssue}</p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.continue}
          rightButtonAction={() => this.timeoutTest()}
        />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deactivateTest
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(withRouter(TimeoutPopup));
