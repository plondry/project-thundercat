import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../text_resources";
import EditEmail from "./EditEmail";
import EditTask from "./EditTask";
import { ACTION_TYPE, EDIT_MODE, EMAIL_TYPE, emailShape } from "./constants";
import {
  addEmailResponse,
  addTaskResponse,
  updateEmailResponse,
  updateTaskResponse
} from "../../modules/EmailInboxRedux";
import { updateEmailTestResponse, UPDATE_EMAIL_ACTIONS } from "../../modules/UpdateResponseRedux";

const styles = {
  header: {
    height: 65,
    padding: "15px 15px 5px 15px",
    backgroundColor: "#00565e",
    color: "#FFFFFF",
    borderRadiusTopLeft: 2,
    borderRadiusTopRight: 2
  },
  title: {
    color: "#FFFFFF",
    fontSize: 28
  },
  container: {
    width: "100%",
    // prevents Select outline from being cut off
    padding: 1
  },
  footer: {
    padding: 15,
    height: 70
  },
  originalEmail: {
    padding: 12,
    marginRight: 12,
    border: "1px #00565E solid",
    borderRadius: 4
  },
  icon: {
    float: "left",
    marginTop: 10,
    marginRight: 8,
    fontSize: 24
  },
  leftButton: {
    margin: 4
  },
  rightButton: {
    float: "right",
    margin: 4
  },
  dataBodyDivider: {
    width: "100%",
    borderTop: "1px solid #96a8b2",
    margin: 0
  }
};

class EditActionDialog extends Component {
  static propTypes = {
    email: emailShape,
    showDialog: PropTypes.bool.isRequired,
    emailType: PropTypes.string,
    handleClose: PropTypes.func.isRequired,
    actionType: PropTypes.oneOf(Object.keys(ACTION_TYPE)).isRequired,
    editMode: PropTypes.oneOf(Object.keys(EDIT_MODE)).isRequired,
    // Only needed when updating an existing one
    action: PropTypes.object,
    actionId: PropTypes.number
  };

  state = {
    action: {
      ...this.props.action,
      ...{ emailType: this.setEmailType() }
    },
    showCancelConfirmationDialog: false,
    toFieldValid: true,
    triggerPropsUpdate: false
  };

  setEmailType() {
    // undefined for a task list
    if (this.props.actionType === ACTION_TYPE.task) {
      return undefined;
    }

    // emailType is passed as props when first creating a response
    if (this.props.emailType !== undefined) {
      return this.props.emailType;
    }

    // return the previously saved emailType (editing a response)
    if (this.props.action !== undefined && this.props.action.emailType !== undefined) {
      return this.props.action.emailType;
    }

    return EMAIL_TYPE.reply;
  }

  handleSave = () => {
    this.triggerPropsUpdate();
    // determine which function to call depending on action type and edit mode
    let response = this.state.action;
    if (
      this.props.actionType === ACTION_TYPE.email &&
      this.props.editMode === EDIT_MODE.create &&
      this.state.action.emailTo.length !== 0
    ) {
      // create an email response
      this.props.updateEmailTestResponse(
        UPDATE_EMAIL_ACTIONS.ADD_EMAIL,
        this.props.assignedTestId,
        this.props.questionId,
        this.props.emailResponseId,
        response
      );
      this.props.addEmailResponse(this.props.questionId, this.props.emailResponseId, response);
      // reset action state to 0
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else if (
      this.props.actionType === ACTION_TYPE.task &&
      this.props.editMode === EDIT_MODE.create
    ) {
      // create task
      // in case a response has no content -> the response is undefined
      if (!response.hasOwnProperty("task")) {
        response = {
          task: "",
          reasonsForAction: ""
        };
      }
      this.props.updateEmailTestResponse(
        UPDATE_EMAIL_ACTIONS.ADD_TASK,
        this.props.assignedTestId,
        this.props.questionId,
        this.props.emailResponseId,
        response
      );
      this.props.addTaskResponse(this.props.questionId, this.props.emailResponseId, response);
      // reset action state to 0
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else if (
      this.props.actionType === ACTION_TYPE.email &&
      this.props.editMode === EDIT_MODE.update &&
      this.state.action.emailTo.length !== 0
    ) {
      //update email
      this.props.updateEmailTestResponse(
        UPDATE_EMAIL_ACTIONS.UPDATE_EMAIL,
        this.props.assignedTestId,
        this.props.questionId,
        this.props.actionId,
        response
      );
      this.props.updateEmailResponse(this.props.questionId, this.props.actionId, response);
      // reset action state to 0
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else if (
      this.props.actionType === ACTION_TYPE.task &&
      this.props.editMode === EDIT_MODE.update
    ) {
      //update a task response
      this.props.updateEmailTestResponse(
        UPDATE_EMAIL_ACTIONS.UPDATE_TASK,
        this.props.assignedTestId,
        this.props.questionId,
        this.props.actionId,
        response
      );
      this.props.updateTaskResponse(this.props.questionId, this.props.actionId, response);
      // reset action state to 0
      this.setState({ action: {}, toFieldValid: true });
      this.props.handleClose();
    } else {
      this.setState({ toFieldValid: false });
    }
  };

  // triggering props update (need that trigger in EditEmail.jsx component)
  triggerPropsUpdate = () => {
    this.setState({ triggerPropsUpdate: !this.state.triggerPropsUpdate });
  };

  // updatedAction is the PropType shape actionShape and represents a single action a candidate takes on an email
  editAction = updatedAction => {
    this.setState({ action: updatedAction });
  };

  closeCancelConfirmationDialog = () => {
    this.setState({ showCancelConfirmationDialog: false });
  };

  // this function is called when 'Cancel response' button is selected from the cancel confirmation message dialog
  handleClose = event => {
    const escapeKeyCode = 27;
    // prevent closing on 'escape' key press
    if (event.type === "keydown" && event.keyCode === escapeKeyCode) {
      return;
    } else {
      // resetting all current form variables at their original values from the props
      this.setState({ action: { ...this.props.action }, toFieldValid: true });
      // closing response action dialog
      this.props.handleClose();
    }
  };

  render() {
    const { showDialog, actionType, editMode, email, addressBook, emailType } = this.props;

    return (
      <div>
        {showDialog && (
          <div>
            <div style={styles.container}>
              {actionType === ACTION_TYPE.email && (
                <EditEmail
                  email={email}
                  emailType={emailType}
                  addressBook={addressBook}
                  onChange={this.editAction}
                  action={editMode === EDIT_MODE.update ? this.props.action : null}
                  originalFrom={
                    Array.isArray(email.from_field) ? email.from_field : [email.from_field]
                  }
                  originalTo={this.props.email.to_field}
                  originalCC={this.props.email.cc_field ? this.props.email.cc_field : ""}
                  editMode={editMode}
                  toFieldValid={this.state.toFieldValid}
                  triggerPropsUpdate={this.state.triggerPropsUpdate}
                />
              )}
              {actionType === ACTION_TYPE.task && (
                <EditTask
                  emailNumber={this.props.email.id}
                  emailSubject={this.props.email.subject}
                  onChange={this.editAction}
                  action={editMode === EDIT_MODE.update ? this.props.action : null}
                />
              )}
            </div>

            <div style={styles.footer}>
              <button
                style={styles.leftButton}
                className={"btn btn-danger"}
                onClick={this.handleClose}
              >
                {LOCALIZE.commons.cancelResponse}
              </button>
              <button
                style={styles.rightButton}
                id="unit-test-email-response-button"
                type="button"
                className="btn btn-primary"
                onClick={this.handleSave}
              >
                {LOCALIZE.emibTest.inboxPage.editActionDialog.save}
              </button>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export { EditActionDialog as UnconnectedEditActionDialog };
const mapStateToProps = (state, ownProps) => {
  return {
    assignedTestId: state.assignedTest.assignedTestId
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addEmailResponse,
      addTaskResponse,
      updateEmailResponse,
      updateTaskResponse,
      updateEmailTestResponse
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(EditActionDialog);
