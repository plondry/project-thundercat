import React, { Component } from "react";
import PropTypes from "prop-types";
import "../../css/collapsing-item.css";
import LOCALIZE from "../../text_resources";
import EditActionDialog from "./EditActionDialog";
import { ACTION_TYPE, EDIT_MODE, EMAIL_TYPE, actionShape, emailShape } from "./constants";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { deleteEmailResponse } from "../../modules/EmailInboxRedux";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import SystemMessage, { MESSAGE_TYPE } from "../commons/SystemMessage";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faReply, faReplyAll, faShareSquare } from "@fortawesome/free-solid-svg-icons";
import { contactNameFromId } from "../../helpers/transformations";
import { addressBookOptionShape } from "./constants";
import { updateEmailTestResponse, UPDATE_EMAIL_ACTIONS } from "../../modules/UpdateResponseRedux";

const styles = {
  type: {
    minHeight: 35
  },
  replyAndUser: {
    color: "#00565E"
  },
  headings: {
    fontWeight: "bold"
  },
  responseType: {
    icon: {
      color: "white",
      margin: "0 8px",
      padding: 3,
      backgroundColor: "#00565E",
      border: "3px solid #009FAE",
      borderRadius: 4,
      fontSize: 24
    },
    attribute: {
      color: "#00565E",
      textDecoration: "underline"
    }
  },
  deleteButton: {
    margin: 4
  },
  editButton: {
    float: "right",
    margin: 4
  },
  preWrap: {
    whiteSpace: "pre-wrap"
  },
  topDivider: {
    marginTop: 16,
    paddingTop: 16,
    borderTop: "1px solid rgba(0, 0, 0, 0.1)"
  },
  disabledExampleComponent: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    padding: 16
  }
};

class ActionViewEmail extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      action: actionShape,
      // actionId: PropTypes.number.isRequired,
      email: emailShape,
      // optional prop to disable the entire component
      disabled: PropTypes.bool,
      isInstructions: PropTypes.bool,
      // Props from Redux
      addressBook: PropTypes.arrayOf(addressBookOptionShape),
      deleteEmail: PropTypes.func,
      // used to prevent collapsible container closing when editing
      userIsEditing: PropTypes.bool,
      showEditDialog: PropTypes.func,
      closeEditDialog: PropTypes.func
    };

    ActionViewEmail.defaultProps = {
      disabled: false
    };
  }

  state = {
    showDeleteConfirmationDialog: false
  };

  showEditDialog = () => this.props.showEditDialog();

  closeEditDialog = () => this.props.closeEditDialog();

  showDeleteConfirmationDialog = () => {
    this.setState({ showDeleteConfirmationDialog: true });
  };

  closeDeleteConfirmationDialog = () => {
    this.setState({ showDeleteConfirmationDialog: false });
  };

  // generate a string of contacts and their roles for display purposes
  // (namely in the To/CC fields)
  // contactIdList is a list of ids that need to be looked up in the address book
  // and transformed into a string that will be displayed to the candidate
  // the return is a string in the following format:
  //  "<name 1> (<role 1>), <name 2> (<role 2>), ...""
  generateEmailNameList(contactIdList) {
    let visibleContactNames = [];
    if (contactIdList === undefined) {
      return "";
    }
    for (let id of contactIdList) {
      //on reload answers the response come back with only id so we have to check both
      visibleContactNames.push(contactNameFromId(this.props.addressBook, id.value || id.id));
    }
    return visibleContactNames.join("; ");
  }

  deleteResponse = () => {
    // called when the user presses confirm in the 'Delete' popup
    this.props.updateEmailTestResponse(
      UPDATE_EMAIL_ACTIONS.DELETE_EMAIL,
      this.props.assignedTestId,
      this.props.questionId,
      this.props.action.answerId
    );
    this.props.deleteEmailResponse(this.props.questionId, this.props.action.answerId);
  };

  render() {
    const action = this.props.action;
    const visibleToNames = this.generateEmailNameList(action.emailTo);
    const visibleCcNames = this.generateEmailNameList(action.emailCc);
    const { userIsEditing, showEditDialog, closeEditDialog } = this.props;

    if (typeof this.props.disabled === undefined) {
      return null;
    }
    return (
      <div>
        {!userIsEditing && (
          <div aria-label={LOCALIZE.ariaLabel.responseDetails}>
            <div>
              <div style={styles.type}>
                <span style={styles.headings}>
                  {LOCALIZE.emibTest.inboxPage.emailResponse.description}
                </span>
                {action.emailType === EMAIL_TYPE.reply && (
                  <>
                    <FontAwesomeIcon icon={faReply} style={styles.responseType.icon} />
                    <span style={styles.responseType.attribute}>
                      {LOCALIZE.emibTest.inboxPage.emailCommons.reply}
                    </span>
                  </>
                )}

                {action.emailType === EMAIL_TYPE.replyAll && (
                  <>
                    <FontAwesomeIcon icon={faReplyAll} style={styles.responseType.icon} />
                    <span style={styles.responseType.attribute}>
                      {LOCALIZE.emibTest.inboxPage.emailCommons.replyAll}
                    </span>
                  </>
                )}
                {action.emailType === EMAIL_TYPE.forward && (
                  <>
                    <FontAwesomeIcon icon={faShareSquare} style={styles.responseType.icon} />
                    <span style={styles.responseType.attribute}>
                      {LOCALIZE.emibTest.inboxPage.emailCommons.forward}
                    </span>
                  </>
                )}
              </div>
              <div>
                {LOCALIZE.emibTest.inboxPage.emailCommons.to}{" "}
                <span style={styles.replyAndUser}>{visibleToNames}</span>
              </div>
              <div>
                {LOCALIZE.emibTest.inboxPage.emailCommons.cc}{" "}
                <span style={styles.replyAndUser}>{visibleCcNames}</span>
              </div>
            </div>
            <div style={styles.topDivider}>
              <div style={styles.headings}>
                {LOCALIZE.emibTest.inboxPage.emailResponse.response}
              </div>
              <p style={styles.preWrap}>{action.emailBody}</p>
            </div>
            <div style={styles.topDivider}>
              <div style={styles.headings}>
                {LOCALIZE.emibTest.inboxPage.emailResponse.reasonsForAction}
              </div>
              <p style={styles.preWrap}>{action.reasonsForAction}</p>
            </div>
          </div>
        )}
        {!this.props.disabled && (
          <div>
            {!userIsEditing && (
              <div style={styles.topDivider} aria-label={LOCALIZE.ariaLabel.emailOptions}>
                <button
                  id="unit-test-view-email-edit-button"
                  className="btn btn-primary"
                  style={styles.editButton}
                  onClick={showEditDialog}
                >
                  {LOCALIZE.emibTest.inboxPage.emailCommons.editButton}
                </button>
                <button
                  id="unit-test-view-email-delete-button"
                  className="btn btn-danger"
                  style={styles.deleteButton}
                  onClick={this.showDeleteConfirmationDialog}
                >
                  {LOCALIZE.emibTest.inboxPage.emailCommons.deleteButton}
                </button>
                <PopupBox
                  show={this.state.showDeleteConfirmationDialog}
                  handleClose={this.closeDeleteConfirmationDialog}
                  title={LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation.title}
                  description={
                    <div>
                      <div>
                        <SystemMessage
                          messageType={MESSAGE_TYPE.error}
                          title={
                            LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation
                              .systemMessageTitle
                          }
                          message={
                            <p>
                              {
                                LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation
                                  .systemMessageDescription
                              }
                            </p>
                          }
                        />
                      </div>
                      <div>
                        {LOCALIZE.emibTest.inboxPage.deleteResponseConfirmation.description}
                      </div>
                    </div>
                  }
                  leftButtonType={BUTTON_TYPE.danger}
                  leftButtonTitle={LOCALIZE.emibTest.inboxPage.emailCommons.deleteButton}
                  leftButtonAction={this.deleteResponse}
                  rightButtonType={BUTTON_TYPE.primary}
                  rightButtonTitle={LOCALIZE.commons.returnToTest}
                />
              </div>
            )}
            <EditActionDialog
              email={this.props.email}
              emailResponseId={this.props.actionId}
              questionId={this.props.questionId}
              addressBook={this.props.addressBook}
              showDialog={userIsEditing}
              handleClose={closeEditDialog}
              actionType={ACTION_TYPE.email}
              editMode={EDIT_MODE.update}
              action={action}
              actionId={this.props.actionId}
            />
          </div>
        )}
      </div>
    );
  }
}

export { ActionViewEmail as UnconnectedActionViewEmail };
const mapStateToProps = (state, ownProps) => {
  return {
    assignedTestId: state.assignedTest.assignedTestId
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      deleteEmailResponse,
      updateEmailTestResponse
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActionViewEmail);
