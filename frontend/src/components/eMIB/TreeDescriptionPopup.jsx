import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import "../../css/react-medium-image-zoom.css";
import TreeNode from "../commons/TreeNode";
import { processTreeContent } from "../../helpers/transformations";

const styles = {
  containerWidth: {
    width: "100%"
  },
  testImage: {
    width: "100%",
    maxWidth: 600
  },
  button: {
    margin: "12px 0 12px 5px"
  }
};

class TreeDescriptionPopup extends Component {
  static propTypes = {
    contentItem: PropTypes.array,
    currentLanguage: PropTypes.string
  };

  state = {
    showPopupBox: false
  };

  openPopup = () => {
    this.setState({ showPopupBox: true });
  };

  closePopup = () => {
    this.setState({ showPopupBox: false });
  };

  render() {
    const { contentItem } = this.props;

    // Process the tree content.
    // For now, the treeType value is based on two available org charts (organizational structure & team information)
    // If we add more treeType / org chart, this code will need to be updated
    let treeType = "team_information_tree_child";
    // saving different tree view content in variables
    let treeView = [];
    // if there is more than one org chart in the specified page
    treeView = processTreeContent(contentItem, treeType);

    return (
      <div style={styles.containerWidth}>
        <button
          id="org-image-description"
          onClick={() => this.openPopup(contentItem.organizational_structure_tree_child)}
          className="btn btn-secondary"
          style={styles.button}
          aria-label={LOCALIZE.emibTest.background.orgCharts.ariaLabel}
        >
          {LOCALIZE.emibTest.background.orgCharts.link}
        </button>
        <PopupBox
          show={this.state.showPopupBox}
          handleClose={this.closePopup}
          title={contentItem.title}
          description={
            <div>
              <p>{LOCALIZE.emibTest.background.orgCharts.treeViewInstructions}</p>

              <TreeNode nodes={treeView} />
            </div>
          }
          rightButtonType={BUTTON_TYPE.secondary}
          rightButtonTitle={LOCALIZE.commons.close}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

export default connect(mapStateToProps, null)(TreeDescriptionPopup);
