import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import { Tabs, Tab, Row, Col } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import Notepad from "../commons/Notepad";
import { BANNER_STYLE, TEST_STYLE } from "./constants";
import SingleComponentFactory from "../testFactory/SingleComponentFactory";
import { switchTopTab } from "../../modules/NavTabsRedux";
import { getTestLanguage } from "../testBuilder/helpers";

const styles = {
  container: {
    maxWidth: 1400,
    minWidth: 900,
    paddingTop: 20,
    paddingRight: 20,
    paddingLeft: 20,
    margin: "0px auto"
  },
  tabsContainerWidth: {
    // preventing the notepad to go under the EmibTabs container
    width: "calc(1%)",
    paddingRight: 0
  },
  notepad: {
    paddingLeft: 0
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class TopTabs extends Component {
  state = {
    // to enable or disable hidden message related to the only tab available as far as the test is not fully started (for screen reader users only)
    oneTabOnlyHiddenMsgEnabled: false
  };

  componentDidUpdate = prevProps => {
    // if defaultTab gets updated
    if (prevProps.defaultTab !== this.props.defaultTab) {
      this.switchTab(this.props.defaultTab);
    }
  };

  // enabling the "single tab available" message for screen reader users on enter eMIB action, meaning before the test fully starts
  componentDidMount = () => {
    this.switchTab(this.props.currentTab >= 1 ? this.props.currentTab : this.props.defaultTab);
  };

  switchTab = tab => {
    this.props.switchTopTab(tab);
  };

  getTabs = () => {
    return this.props.testSection.components.map(tab => {
      return {
        key: tab.order,
        tabName: tab.title,
        lang: getTestLanguage(tab.language, this.props.currentLanguage),
        body: <SingleComponentFactory testSection={tab} type={tab.component_type} />
      };
    });
  };

  render() {
    let TABS = this.getTabs();
    let foundKey = false;
    let currentTab = this.props.currentTab;
    for (let element of TABS) {
      if (String(element.key) === String(currentTab)) {
        foundKey = true;
        break;
      }
    }
    if (!foundKey && TABS[0]) {
      currentTab = TABS[0].key;
    }

    return (
      <div>
        <Helmet>
          <title>{LOCALIZE.titles.CAT}</title>
        </Helmet>
        <Row>
          <Col
            role="region"
            aria-label={LOCALIZE.ariaLabel.topNavigationSection}
            style={styles.tabsContainerWidth}
          >
            <div id="tab-container" tabIndex={-1}>
              {this.state.oneTabOnlyHiddenMsgEnabled && (
                <span id="only-one-tab-available-for-now-msg" style={styles.hiddenText}>
                  {LOCALIZE.emibTest.howToPage.testInstructions.onlyOneTabAvailableForNowMsg}
                </span>
              )}
              <Tabs
                defaultActiveKey={currentTab}
                id="emib-tabs"
                activeKey={currentTab}
                onSelect={key => this.switchTab(key)}
                aria-labelledby="only-one-tab-available-for-now-msg"
              >
                {TABS.map((tab, index) => {
                  return (
                    <Tab key={index} eventKey={tab.key} title={tab.tabName}>
                      <div lang={tab.lang}>{tab.body}</div>
                    </Tab>
                  );
                })}
              </Tabs>
            </div>
          </Col>
          {this.props.usesNotepad && !this.props.readOnly && (
            <Col
              role="complementary"
              aria-label={LOCALIZE.ariaLabel.notepadSection}
              md="auto"
              style={styles.notepad}
            >
              <Notepad headerFooterPX={this.props.isTestActive ? TEST_STYLE : BANNER_STYLE} />
            </Col>
          )}
        </Row>
      </div>
    );
  }
}

export { TopTabs as UnconnectedTopTabs };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    currentTab: state.navTabs.currentTopTab,
    isTestActive: state.testStatus.isTestActive,
    readOnly: state.testSection.readOnly
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      switchTopTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TopTabs);
