import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getAssignedTests, updateAssignedTestId } from "../../modules/AssignedTestsRedux";
import { PATH } from "../commons/Constants";
import { LANGUAGES } from "../commons/Translation";
import CandidateCheckIn from "../../CandidateCheckIn";
import { alternateColorsStyle } from "../commons/Constants";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faSpinner } from "@fortawesome/free-solid-svg-icons";
import SessionStorage, { ACTION, ITEM } from "../../SessionStorage";
import { TEST_STATUS } from "../../components/ta/Constants";
import { activateTest, deactivateTest, updateTestStatus } from "../../modules/TestStatusRedux";
import { resetTestFactoryState, setContentUnLoaded } from "../../modules/TestSectionRedux";
import { resetAssignedTestState } from "../../modules/AssignedTestsRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import candidateSideTestSessionChannels, {
  getCurrentSocket,
  CHANNELS_PATH,
  CHANNELS_ACTION,
  validateSocketConnection
} from "../../helpers/testSessionChannels";
import { styles as ActivePermissionsStyles } from "../../components/etta/permissions/ActivePermissions";
import { setErrorStatus } from "../../modules/ErrorStatusRedux";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  table: {
    width: "75%",
    margin: "24px auto",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#CECECE",
    borderTop: "1px solid #00565e"
  },
  th: {
    height: 60,
    backgroundColor: "#00565e",
    color: "white",
    width: "50%"
  },
  td: {
    padding: "12px 0 12px 24px"
  },
  paddingLeft: {
    paddingLeft: 24
  },
  centerText: {
    textAlign: "center"
  },
  viewButton: {
    minWidth: 150
  },
  buttonIcon: {
    marginRight: 12,
    transform: "scale(1.5)"
  }
};

class AssignedTestTable extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    username: PropTypes.string,
    isUserLoading: PropTypes.bool,
    // Props from Redux
    getAssignedTests: PropTypes.func,
    updateAssignedTestId: PropTypes.func,
    activateTest: PropTypes.func,
    deactivateTest: PropTypes.func,
    setErrorStatus: PropTypes.func
  };

  state = {
    username: "",
    assigned_tests: [],
    activeUitTest: false,
    id: null,
    testId: null,
    startTime: null,
    isCheckedIn: false,
    sockets: [],
    isLoading: true
  };

  componentDidMount = () => {
    this._isMounted = true;
    //remove active test variable if it exists
    SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);

    if (this._isMounted) {
      //will be defined except in tests
      if (typeof this.props.getAssignedTests !== "undefined") {
        // TODO: improve this functionality, since adding a delay is not optimal
        // adding delay here to avoid websocket connection error
        // this error was happening when the logged in user was having permissions
        setTimeout(() => {
          this.setState({ isLoading: true }, () => {
            this.populateTable();
          });
        }, 500);
      }
    }
  };

  componentDidUpdate = prevProps => {
    // if language gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      // updating assigned tests table
      this.props.getAssignedTests().then(response => {
        this.setState({ assigned_tests: response });
      });
    }
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  // populating assigned tests table
  // if test access code is provided: candidate has just checked in a test, meaning that a message needs to be sent to respective TA (to update the TA assigned candidates table)
  // if test access code is not provided: simply get the assigned tests and populate the table
  populateTable = testAccessCode => {
    let assignedTests = null;
    let activeTestObj = {};

    /* displayTableContent state is used here to avoid unwanted table content display, such as
        the flashing view button when refreshing the page */
    if (this._isMounted) {
      // getting assigned tests
      this.props
        .getAssignedTests()
        .then(assigned_tests => {
          // verifying if there is an active test
          activeTestObj = this.checkForActiveTest(assigned_tests);
          assignedTests = assigned_tests;
        })
        .then(() => {
          // there is an active test
          if (activeTestObj.activeTest) {
            validateSocketConnection(
              `${CHANNELS_PATH.testAdministratorRoom}${SessionStorage(
                ACTION.GET,
                ITEM.AUTH_TOKEN
              )}/${activeTestObj.testAccessCode}`
            ).then(valid => {
              // socket connection is valid
              if (valid) {
                this.props.history.push(PATH.testBase);
                // socket connection is invalid
              } else {
                this.props.history.push(PATH.websocketConnectionError);
              }
            });
            // there is no active test
          } else {
            // initializing sockets state
            let sockets = this.state.sockets;
            // looping in assigned tests
            for (let i = 0; i < assignedTests.length; i++) {
              // connecting to test administrator room using test access code of each assigned test
              const currentSocket = candidateSideTestSessionChannels(
                `${CHANNELS_PATH.testAdministratorRoom}${SessionStorage(
                  ACTION.GET,
                  ITEM.AUTH_TOKEN
                )}/${assignedTests[i].test_access_code}`,
                this.props.username
              );
              sockets.push(currentSocket);
              // when socket messages are received here, it means one of the following:
              //    1. the candidate has been approved (update assigned tests table)
              //    2. the test has been locked/unlocked or pause/unpaused (trigger a page refresh)
              currentSocket.onmessage = e => {
                // getting the action
                const action = JSON.parse(e.data).action;
                // getting the parameters
                const parameters = JSON.parse(JSON.parse(e.data).parameters);
                // conditional paths
                const conditionalPaths = [PATH.dashboard, PATH.testBase];
                // username is the same as the one received in the websocket message + pathname is part of the conditionalPaths array
                if (
                  this.props.username === parameters.username_id &&
                  conditionalPaths.indexOf(window.location.pathname) >= 0
                ) {
                  switch (action) {
                    // candidate gets approved
                    case CHANNELS_ACTION.approveCandidate:
                      // updating assigned tests table
                      this.props.getAssignedTests().then(response => {
                        this.setState({ assigned_tests: response });
                      });
                      break;
                    // candidate gets his test un-assigned
                    case CHANNELS_ACTION.unAssignCandidate:
                      // updating assigned tests table
                      this.props.getAssignedTests().then(response => {
                        this.setState({ assigned_tests: response });
                      });
                      break;
                    default:
                      // disconnecting sockets
                      for (let i = 0; i < sockets.length; i++) {
                        sockets[i].close();
                      }
                      // redirecting to test page (lock/pause screen)
                      this.props.history.push(PATH.testBase);
                      break;
                  }
                }
              };
              // error can be triggered only when populateTable function is called without testAccessCode parameter (meaning on mount only, not when the candidate is checking in a new test)
              if (!testAccessCode) {
                // this function could be triggered if the user is already connected to the socket while redering the assigned tests table (no current active test)
                currentSocket.onerror = () => {
                  // deactivating the test
                  this.props.deactivateTest();
                  // removing TEST_ACTIVE session storage item
                  SessionStorage(ACTION.REMOVE, ITEM.TEST_ACTIVE);
                  // setting error status
                  this.props.setErrorStatus(true);
                  // redirecting to error page
                  this.props.history.push(PATH.websocketConnectionError);
                };
              }
            }
            this.setState({ sockets: sockets }, () => {
              // candidate just checked in a test
              if (testAccessCode) {
                let testId = null;
                // looping in assigned tests
                for (let i = 0; i < assignedTests.length; i++) {
                  // current test access code matches the provided test access code
                  if (assignedTests[i].test_access_code === testAccessCode) {
                    // getting testId
                    testId = assignedTests[i].test.id;
                    break;
                  }
                }
                // sending message to TA in order to update the test status (websocket)
                // getting needed parameters to send to socket
                const paramsObj = {
                  username_id: this.props.username,
                  test_id: testId
                };
                getCurrentSocket(this.state.sockets, testAccessCode).then(currentSocket => {
                  currentSocket.send(
                    JSON.stringify({
                      action: CHANNELS_ACTION.checkin,
                      parameters: JSON.stringify(paramsObj),
                      test_access_code: testAccessCode
                    })
                  );
                });
              }
            });
          }
        })
        .then(() => {
          this.setState({ assigned_tests: assignedTests, isLoading: false });
        });
    }
  };

  viewTest = (index, currentAssignedTestId, testId, testAccessCode) => {
    this.props.setContentUnLoaded();
    this.resetAllRedux();
    this.props.history.push(PATH.testBase);
    this.props.updateAssignedTestId(currentAssignedTestId, testId);
    this.props.activateTest();
    SessionStorage(ACTION.SET, ITEM.TEST_ACTIVE, 1);

    this.props.updateTestStatus(currentAssignedTestId, TEST_STATUS.ACTIVE);

    // sending message to TA in order to update the test status (websocket)
    // getting needed parameters to send to socket
    const paramsObj = {
      username_id: this.props.username,
      test_id: testId
    };
    const currentSocket = this.state.sockets[index];
    currentSocket.send(
      JSON.stringify({
        action: CHANNELS_ACTION.readyToActive,
        parameters: JSON.stringify(paramsObj),
        test_access_code: testAccessCode
      })
    );
  };

  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetNotepadState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  /* checking in the assigned tests list if there is an active test (active, locked or paused)
  return true if that's the case */
  checkForActiveTest = assigned_tests => {
    let activeTest = false;
    let testAccessCode = null;
    for (let i = 0; i < assigned_tests.length; i++) {
      // if test is active
      if (
        assigned_tests[i].status === TEST_STATUS.ACTIVE ||
        assigned_tests[i].status === TEST_STATUS.LOCKED ||
        assigned_tests[i].status === TEST_STATUS.PAUSED
      ) {
        this.props.updateAssignedTestId(assigned_tests[i].id.toString(), assigned_tests[i].test.id);
        testAccessCode = assigned_tests[i].test_access_code;
        activeTest = true;
      }
    }
    return { activeTest: activeTest, testAccessCode: testAccessCode };
  };

  handleCheckIn = testAccessCode => {
    this.populateTable(testAccessCode);
    this.setState({ ...this.state, isCheckedIn: true });
  };

  render() {
    return (
      <div>
        <div id="candidate-check-in">
          <CandidateCheckIn
            username={this.props.username}
            handleCheckIn={this.handleCheckIn}
            isCheckedIn={this.state.isCheckedIn}
            isLoaded={!this.props.isUserLoading}
          />
        </div>
        <div role="region" aria-labelledby="test-assignment-table">
          <table id="test-assignment-table" style={styles.table}>
            <tbody>
              <tr>
                <th id="name-of-test-label" style={{ ...styles.th, ...styles.paddingLeft }}>
                  {LOCALIZE.dashboard.table.columnOne}
                </th>
                <th style={{ ...styles.th, ...styles.centerText }}>
                  {LOCALIZE.dashboard.table.columnTwo}
                </th>
              </tr>
              {!this.props.isUserLoading &&
                this.state.assigned_tests instanceof Array &&
                this.state.assigned_tests.map((test, index) => {
                  return (
                    <tr
                      key={index}
                      id={"assigned-test-" + index}
                      style={alternateColorsStyle(index, 60)}
                      tabIndex={0}
                      aria-labelledby={`name-of-test-label name-of-test-${index}`}
                    >
                      <td id={`name-of-test-${index}`} style={styles.td}>
                        {this.props.currentLanguage === LANGUAGES.english
                          ? test.test.en_name
                          : test.test.fr_name}
                      </td>
                      <td style={styles.centerText}>
                        <CustomButton
                          id={`unit-test-button-${index}`}
                          label={
                            <>
                              <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                              <span>{LOCALIZE.dashboard.table.viewButton}</span>
                            </>
                          }
                          action={() =>
                            this.viewTest(index, test.id, test.test.id, test.test_access_code)
                          }
                          customStyle={styles.viewButton}
                          buttonTheme={THEME.SECONDARY}
                          disabled={
                            this.state.assigned_tests[index].status === TEST_STATUS.ASSIGNED
                              ? true
                              : false
                          }
                          ariaLabel={LOCALIZE.formatString(
                            LOCALIZE.dashboard.table.viewButtonAccessibilityLabel,
                            test.test[`${this.props.currentLanguage}_name`]
                          )}
                        />
                      </td>
                    </tr>
                  );
                })}
              {this.props.isUserLoading ||
                (this.state.isLoading && (
                  <tr>
                    <td colSpan={2}>
                      <label
                        className="fa fa-spinner fa-spin"
                        style={ActivePermissionsStyles.loading}
                      >
                        <FontAwesomeIcon icon={faSpinner} />
                      </label>
                    </td>
                  </tr>
                ))}
              {!this.props.isUserLoading &&
                !this.state.isLoading &&
                this.state.assigned_tests.length === 0 && (
                  <tr key={0} id="no-assigned-test-row" style={alternateColorsStyle(0, 60)}>
                    <td style={styles.td}>{LOCALIZE.dashboard.table.noTests}</td>
                    <td></td>
                  </tr>
                )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export { AssignedTestTable as UnconnectedAssignedTestTable };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAssignedTests,
      updateAssignedTestId,
      activateTest,
      deactivateTest,
      resetTestFactoryState,
      setContentUnLoaded,
      resetAssignedTestState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      updateTestStatus,
      setErrorStatus
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AssignedTestTable));

export { styles as TEST_TABLE_STYLES };
