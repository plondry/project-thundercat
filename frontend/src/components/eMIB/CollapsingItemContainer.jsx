import React, { Component } from "react";
import PropTypes from "prop-types";
import "../../css/collapsing-item.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleDown, faAngleUp, faEnvelope, faTasks } from "@fortawesome/free-solid-svg-icons";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  container: {
    position: "relative",
    margin: "16px 0 16px 0",
    wordWrap: "break-word"
  },
  button: {
    width: "100%",
    textAlign: "left",
    minHeight: 48
  },
  emailAndTaskIcon: {
    margin: 8
  },
  titleContainer: {
    float: "left",
    paddingTop: 4
  },
  expandIcon: {
    float: "right",
    margin: "auto 8px",
    pointerEvents: "none"
  },
  iconStyle: {
    marginTop: 6
  },
  contentContainer: {
    margin: 12
  }
};

export const ICON_TYPE = {
  email: faEnvelope,
  task: faTasks
};

class CollapsingItemContainer extends Component {
  static propTypes = {
    iconType: PropTypes.object,
    title: PropTypes.object.isRequired,
    body: PropTypes.object.isRequired,
    // used so we only pass props for editing when it makes sense
    // this prevents an error from occurring in AssignTestAccess.jsx
    bodyIsEditable: PropTypes.bool
  };

  state = {
    isCollapsed: true,
    // used by containers that contain an editable response (Sample e-MIB)
    userIsEditing: false
  };

  componentDidMount = () => {};

  expandItem = () => {
    if (!this.state.userIsEditing) {
      this.setState({ isCollapsed: !this.state.isCollapsed });
    }
  };

  showEditDialog = () => {
    this.setState({ userIsEditing: true });
  };

  closeEditDialog = () => {
    this.setState({ userIsEditing: false });
  };

  render() {
    const { iconType, title, body, bodyIsEditable } = this.props;
    const { isCollapsed, userIsEditing } = this.state;
    let { buttonClass, iconClass, containerClass } = "";

    if (isCollapsed) {
      buttonClass = `${THEME.SECONDARY}`;
      iconClass = faAngleDown;
      containerClass = "";
    } else if (!isCollapsed) {
      buttonClass = `${THEME.PRIMARY} expanded-button-style`;
      iconClass = faAngleUp;
      containerClass = "expanded-container-style";
    }

    return (
      <div className={`${containerClass}`} style={styles.container}>
        <CustomButton
          label={
            <>
              {this.props.iconType && (
                <FontAwesomeIcon icon={iconType} style={styles.emailAndTaskIcon} />
              )}
              <div style={styles.titleContainer}>
                <span>{title}</span>
              </div>
              <div style={styles.expandIcon}>
                <FontAwesomeIcon
                  icon={iconClass}
                  style={styles.iconStyle}
                  aria-hidden="true"
                  focusable="false"
                />
              </div>
            </>
          }
          action={this.expandItem}
          customStyle={styles.button}
          buttonTheme={buttonClass}
          ariaExpanded={!this.state.isCollapsed}
        />
        {!isCollapsed && (
          <div style={styles.contentContainer}>
            {React.cloneElement(body, {
              expandItem: this.expandItem,
              ...(bodyIsEditable
                ? {
                    userIsEditing,
                    showEditDialog: this.showEditDialog,
                    closeEditDialog: this.closeEditDialog
                  }
                : {})
            })}
          </div>
        )}
      </div>
    );
  }
}

export default CollapsingItemContainer;
