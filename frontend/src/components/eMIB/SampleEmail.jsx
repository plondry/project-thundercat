import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import "../../css/inbox.css";
import { emailShape } from "./constants";

const styles = {
  empty: {},
  header: {
    borderBottom: "1px solid #00565E",
    marginBottom: 16,
    paddingBottom: 35
  },
  emailId: {
    float: "left",
    marginRight: 12,
    fontSize: 16,
    fontWeight: 700,
    color: "#252525",
    marginBottom: 8,
    marginTop: 0
  },
  replyStatus: {
    float: "right"
  },
  email: {
    textAlign: "left",
    padding: 16
  },
  replyIcon: {
    color: "#00565E"
  },
  actionIcon: {
    marginRight: 5
  },
  addEmailButton: {
    marginRight: 20
  },
  actionArea: {
    borderTop: "1px solid #00565E",
    marginTop: 35,
    paddingTop: 16
  },
  disabledExampleComponentNoPadding: {
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 4,
    marginBottom: 4,
    margin: 0
  },
  replyAndUser: {
    color: "#00565E"
  },
  preWrap: {
    whiteSpace: "pre-wrap"
  },
  subject: {
    fontSize: 20
  },
  metaData: {
    marginBottom: 12,
    paddingBottom: 12,
    borderBottom: "1px solid #96a8b2"
  }
};

class SampleEmail extends Component {
  constructor(props, context) {
    super(props, context);

    this.PropTypes = {
      email: emailShape,
      emailCount: PropTypes.number,
      taskCount: PropTypes.number,
      // Provided by Redux
      emailActionsArray: PropTypes.array
    };
  }

  render() {
    const { email } = this.props;
    return (
      <div style={styles.disabledExampleComponentNoPadding}>
        <div style={styles.email}>
          <div style={styles.header}>
            <div style={styles.emailId} id="unit-test-email-id">
              {LOCALIZE.emibTest.inboxPage.emailId.toUpperCase()}
              {email.id}
            </div>
          </div>
          <div style={styles.metaData}>
            <div style={styles.subject} id="unit-test-email-subject">
              {LOCALIZE.emibTest.inboxPage.subject + ": " + email.subject}
            </div>
            <div id="unit-test-email-from">
              {LOCALIZE.emibTest.inboxPage.from}:{" "}
              <span style={styles.replyAndUser}>{email.from}</span>
            </div>
            <div id="unit-test-email-to">
              {LOCALIZE.emibTest.inboxPage.to}: <span style={styles.replyAndUser}>{email.to}</span>
            </div>
            <div>
              {email.cc ? (
                <div id="unit-test-email-cc">
                  {LOCALIZE.emibTest.inboxPage.emailCommons.cc}{" "}
                  <span style={styles.replyAndUser}>{email.cc}</span>
                </div>
              ) : (
                <></>
              )}
            </div>
            <div id="unit-test-email-date">
              {LOCALIZE.emibTest.inboxPage.date}: {email.date}
            </div>
          </div>
          <div style={styles.preWrap} id="unit-test-email-body">
            {email.body}
          </div>
        </div>
      </div>
    );
  }
}

export default SampleEmail;
