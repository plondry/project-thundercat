import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import LOCALIZE from "../../text_resources";
import {
  loginAction,
  handleAuthResponseAndState,
  updatePageHasErrorState,
  updateLastLoginField
} from "../../modules/LoginRedux";
import { getUserInformation } from "../../modules/LoginRedux";
import { setUserInformation, setIsUserLoading } from "../../modules/UserRedux";
import { connect } from "react-redux";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  loginContent: {
    padding: "12px 32px 0 32px",
    border: "1px solid #cdcdcd"
  },
  inputTitles: {
    padding: "12px 0 6px 0",
    fontWeight: "bold"
  },
  inputs: {
    width: "100%",
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565E",
    borderRadius: 4
  },
  loginBtn: {
    display: "block",
    margin: "24px auto"
  },
  loginError: {
    marginTop: 12,
    color: "#923534",
    fontWeight: "bold"
  },
  validationError: {
    color: "red",
    paddingTop: 8
  }
};

class LoginForm extends Component {
  static propTypes = {
    // Props from Redux
    loginAction: PropTypes.func,
    handleAuthResponseAndState: PropTypes.func,
    setLoginState: PropTypes.func,
    authenticated: PropTypes.bool,
    updatePageHasErrorState: PropTypes.func,
    updateLastLoginField: PropTypes.func,
    getUserInformation: PropTypes.func,
    setUserInformation: PropTypes.func,
    setIsUserLoading: PropTypes.func
  };

  state = {
    username: "",
    password: "",
    wrongCredentials: false
  };

  handleSubmit = event => {
    this.props
      .loginAction({ username: this.state.username.toLowerCase(), password: this.state.password })
      .then(response => {
        // credentials are wrong
        if (response.non_field_errors || typeof response.token === "undefined") {
          this.setState({
            wrongCredentials: true
          });
          this.props.updatePageHasErrorState(true);
          // focus on password field
          document.getElementById("password").focus();
          // right authentication
        } else {
          this.props.setIsUserLoading(true);
          this.setState({ wrongCredentials: false });
          this.props.handleAuthResponseAndState(response, this.props.dispatch).then(_ => {
            this.props.updatePageHasErrorState(false);
            // update last login field
            this.props.updateLastLoginField({
              username: this.state.username,
              password: this.state.password
            });
            this.props.getUserInformation().then(response => {
              this.props.setUserInformation(
                response.first_name,
                response.last_name,
                response.username,
                response.is_staff,
                response.last_password_change,
                response.email,
                response.secondary_email,
                response.birth_date,
                response.pri_or_military_nbr
              );
            });
          });
        }
      });
    event.preventDefault();
  };

  handleUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  render() {
    return (
      <div>
        {!this.props.authenticated && (
          <div role="form">
            <div style={styles.loginContent}>
              <h2>{LOCALIZE.authentication.login.content.title}</h2>
              <span>{LOCALIZE.authentication.login.content.description}</span>
              <form onSubmit={this.handleSubmit}>
                <div>
                  <div style={styles.inputTitles}>
                    <label htmlFor={"username"}>
                      {LOCALIZE.authentication.login.content.inputs.emailTitle}
                    </label>
                  </div>
                  <input
                    aria-required={"true"}
                    type="text"
                    id="username"
                    style={styles.inputs}
                    onChange={this.handleUsernameChange}
                    value={this.state.username}
                  />
                </div>
                <div>
                  <div style={styles.inputTitles}>
                    <label htmlFor={"password"}>
                      {LOCALIZE.authentication.login.content.inputs.passwordTitle}
                    </label>
                  </div>
                  <input
                    aria-label={
                      this.state.wrongCredentials
                        ? LOCALIZE.authentication.login.invalidCredentials +
                          LOCALIZE.authentication.login.passwordFieldSelected
                        : LOCALIZE.authentication.login.content.inputs.passwordTitle
                    }
                    aria-invalid={this.state.wrongCredentials}
                    aria-required={"true"}
                    type="password"
                    id="password"
                    style={styles.inputs}
                    onChange={this.handlePasswordChange}
                    value={this.state.password}
                  />
                </div>
                <div>
                  {this.state.wrongCredentials && (
                    <label htmlFor={"password"} style={styles.loginError}>
                      {LOCALIZE.authentication.login.invalidCredentials}
                    </label>
                  )}
                </div>
                <CustomButton
                  label={LOCALIZE.authentication.login.button}
                  type={"submit"}
                  customStyle={styles.loginBtn}
                  buttonTheme={THEME.PRIMARY}
                />
              </form>
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    authenticated: state.login.authenticated,
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => ({
  loginAction: data => dispatch(loginAction(data)),
  handleAuthResponseAndState: (userData, dispatch, location, push) =>
    dispatch(handleAuthResponseAndState(userData, dispatch, location, push)),
  updatePageHasErrorState: bool => dispatch(updatePageHasErrorState(bool)),
  updateLastLoginField: data => dispatch(updateLastLoginField(data)),
  getUserInformation: token => dispatch(getUserInformation(token)),
  setUserInformation: (
    firstName,
    lastName,
    username,
    isSuperUser,
    lastPasswordChange,
    primaryEmail,
    secondaryEmail,
    dateOfBirth,
    priOrMilitaryNbr
  ) =>
    dispatch(
      setUserInformation(
        firstName,
        lastName,
        username,
        isSuperUser,
        lastPasswordChange,
        primaryEmail,
        secondaryEmail,
        dateOfBirth,
        priOrMilitaryNbr
      )
    ),
  setIsUserLoading: bool => dispatch(setIsUserLoading(bool)),
  dispatch
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginForm));
