import React, { Component } from "react";
import Tooltip from "rc-tooltip";
import { connect } from "react-redux";
import { getLineSpacingCSS } from "../../modules/AccommodationsRedux";

class StyledTooltip extends Component {
  render() {
    let style = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      style = { ...style, ...getLineSpacingCSS() };
    }

    const overlay = <div style={style}>{this.props.overlay}</div>;
    return (
      <Tooltip {...this.props} overlay={overlay}>
        {this.props.children}
      </Tooltip>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    accommodations: state.accommodations
  };
};

export default connect(mapStateToProps, null)(StyledTooltip);
