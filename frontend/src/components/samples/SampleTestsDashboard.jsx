import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router-dom";
import ContentContainer from "../commons/ContentContainer";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setCurrentTest } from "../../modules/TestStatusRedux";
import { TEST_TABLE_STYLES as AssignedTestTableStyles } from "../eMIB/AssignedTestTable";
import { Helmet } from "react-helmet";
import { resetTestStatusState } from "../../modules/SampleTestStatusRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetTestFactoryState } from "../../modules/TestSectionRedux";
import { resetTopTabsState } from "../../modules/NavTabsRedux";
import { getPublicTests } from "../../modules/AssignedTestsRedux";
import GenericTable, { COMMON_STYLE } from "../commons/GenericTable";
import { updateAssignedTestId } from "../../modules/AssignedTestsRedux";
import CustomButton, { THEME } from "../../components/commons/CustomButton";

const styles = {
  tableContainer: {
    paddingTop: 24,
    width: "75%",
    margin: "auto"
  }
};

class SampleTestDashboard extends Component {
  static propTypes = {
    testNameId: PropTypes.string,
    setCurrentTest: PropTypes.func
  };

  state = {
    tests: [],
    loaded: false
  };

  componentDidMount = () => {
    this.resetRedux();

    this.props.getPublicTests().then(response => {
      if (!response.ok) {
        console.log("error occured");
      } else {
        this.setState({ tests: response.body, loaded: true });
      }
    });
  };

  resetRedux = () => {
    this.props.resetTestStatusState();
    this.props.resetNotepadState();
    this.props.resetTestFactoryState();
    this.props.resetTopTabsState();
  };

  handleClick = testId => {
    // set the test definition
    this.props.updateAssignedTestId(null, testId);
    this.props.history.push(this.props.match.url + "/test-factory");
  };

  makeRows = () => {
    let data = this.state.tests.map(testDefinition => {
      return {
        column_1: testDefinition[`${this.props.currentLanguage}_name`],
        column_2: (
          <CustomButton
            label={LOCALIZE.sampleTestDashboard.table.viewButton}
            action={() => this.handleClick(testDefinition.id)}
            customStyle={AssignedTestTableStyles.viewButton}
            buttonTheme={THEME.PRIMARY}
            ariaLabel={LOCALIZE.formatString(
              LOCALIZE.sampleTestDashboard.table.viewButtonAccessibilityLabel,
              testDefinition[`${this.props.currentLanguage}_name`]
            )}
          />
        )
      };
    });
    let rowsDefinition = {
      column_1_style: {},
      column_2_style: COMMON_STYLE.CENTERED_TEXT,
      data: data
    };
    return rowsDefinition;
  };

  render() {
    const rowsDefinition = this.makeRows();
    const columnsDefinition = [
      {
        label: LOCALIZE.dashboard.table.columnOne,
        style: { width: "50%" }
      },
      {
        label: LOCALIZE.dashboard.table.columnTwo,
        style: { width: "50%", ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.sampleTests}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <h1 className="green-divider">{LOCALIZE.sampleTestDashboard.title}</h1>
            <p>{LOCALIZE.sampleTestDashboard.description}</p>
          </div>
          <div style={styles.tableContainer}>
            <GenericTable
              columnsDefinition={columnsDefinition}
              rowsDefinition={rowsDefinition}
              emptyTableMessage={
                LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.noResultsFound
              }
              currentlyLoading={this.props.currentlyLoading}
            />
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { SampleTestDashboard as UnconnectedSampleTestDashboard };
const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setCurrentTest,
      resetTestStatusState,
      resetNotepadState,
      resetTestFactoryState,
      getPublicTests,
      updateAssignedTestId,
      resetTopTabsState
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SampleTestDashboard));
