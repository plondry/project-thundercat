import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";

class IncidentReports extends Component {
  render() {
    return (
      <div>
        <h2>{LOCALIZE.systemAdministrator.sideNavItems.incidentReports}</h2>
        <p>COMING SOON</p>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IncidentReports);
