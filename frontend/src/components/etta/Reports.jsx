import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import ReportGenerator, { REPORT_REQUESTOR } from "../commons/reports/ReportGenerator";
import { reportTypesDefinition } from "../commons/reports/Constants";

const styles = {
  mainContainer: {
    width: "100%"
  }
};

class Reports extends Component {
  render() {
    return (
      <div style={styles.mainContainer}>
        <h2>{LOCALIZE.systemAdministrator.reports.title}</h2>
        <p>{LOCALIZE.systemAdministrator.reports.description}</p>
        <ReportGenerator
          reportRequestor={REPORT_REQUESTOR.etta}
          reportTypeOptions={reportTypesDefinition()}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Reports);
