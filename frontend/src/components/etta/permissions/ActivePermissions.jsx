import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrashAlt,
  faSave,
  faTimes,
  faSearch,
  faCaretLeft,
  faCaretRight
} from "@fortawesome/free-solid-svg-icons";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import validateName, { validateEmail } from "../../../helpers/regexValidator";
import "../../../css/etta.css";
import {
  updateActivePermission,
  deleteActivePermission,
  updateCurrentPermissionsPageState,
  updatePermissionsPageSizeState,
  updateSearchActivePermissionsStates
} from "../../../modules/PermissionsRedux";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import Select from "react-select";
import ReactPaginate from "react-paginate";
import { getDisplayOptions } from "../../../helpers/getDisplayOptions";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton, { THEME } from "../../../components/commons/CustomButton";

export const styles = {
  searchBarAndDisplayContainer: {
    margin: "18px 0",
    display: "table",
    width: "100%"
  },
  searchBarContainerWithResultsFound: {
    marginBottom: "-12px",
    display: "table-cell"
  },
  searchBarContainerWithoutResultsFound: {
    display: "table-cell",
    width: "60%"
  },
  searchBarLabel: {
    fontWeight: "bold",
    marginRight: 12,
    display: "table-cell"
  },
  searchBarContainer: {
    position: "relative",
    width: 350,
    display: "table-cell",
    paddingLeft: 12
  },
  searchBarInput: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px",
    border: "1px solid #00565e",
    borderRadius: "4px 0 0 4px"
  },
  searchIconButtonContainer: {
    display: "table-cell"
  },
  searchIconButton: {
    padding: "3px 12px",
    minWidth: 25,
    minHeight: 38,
    borderLeft: "none",
    borderRadius: "0 4px 4px 0"
  },
  resultsFound: {
    paddingLeft: 4
  },
  clearSearchResults: {
    background: "transparent",
    border: "none",
    marginLeft: 18,
    cursor: "pointer",
    padding: "4px 8px",
    // default link color code
    color: "#0000EE"
  },
  displayOptionContainer: {
    display: "table-cell",
    textAlign: "right",
    verticalAlign: "middle"
  },
  displayOptionLabel: {
    display: "inline-block",
    fontWeight: "bold",
    paddingTop: 6
  },
  displayOptionDropdown: {
    display: "inline-block",
    float: "right",
    minWidth: "45%",
    textAlign: "center",
    paddingLeft: 12
  },
  table: {
    width: "100%",
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#CECECE",
    borderTop: "1px solid #00565e"
  },
  tableHead: {
    height: 60,
    backgroundColor: "#00565e",
    color: "white",
    fontWeight: "bold"
  },
  tableHeadViewEditDetails: {
    textAlign: "center"
  },
  noResultsFoundRow: {
    width: "100%",
    height: 60,
    padding: "8px 0 8px 12px"
  },
  noResultsFoundLabel: {
    paddingLeft: 12
  },
  viewEditDetailsBtn: {
    width: 100
  },
  buttonIcon: {
    transform: "scale(2)",
    padding: "1.5px"
  },
  popupContainer: {
    padding: "10px 15px"
  },
  userName: {
    fontWeight: "bold"
  },
  description: {
    marginBottom: 12
  },
  label: {
    width: "40%"
  },
  labelHeight: {
    minHeight: 36
  },
  reasonLabel: {
    paddingTop: 16
  },
  input: {
    width: "60%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 12
  },
  reasonInput: {
    height: 85,
    resize: "none",
    margin: "0 0 6px 0",
    width: "100%"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginLeft: 205
  },
  ReasonErrorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0
  },
  boldText: {
    fontWeight: "bold"
  },
  deleteConfirmationPopupWidth: {
    width: 600
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  paginationContainer: {
    display: "flex"
  },
  paginationIcon: {
    padding: "0 6px"
  },
  loading: {
    width: "100%",
    height: 60,
    textAlign: "center",
    fontSize: "32px",
    padding: 6
  },
  displayNone: {
    display: "none"
  }
};

class ActivePermissions extends Component {
  static propTypes = {
    activePermissions: PropTypes.array.isRequired,
    selectedActivePermission: PropTypes.object.isRequired,
    rowsDefinition: PropTypes.object.isRequired,
    populateActivePermissions: PropTypes.func.isRequired,
    populateFoundActivePermissions: PropTypes.func.isRequired,
    numberOfPages: PropTypes.number.isRequired,
    resultsFound: PropTypes.number.isRequired,
    currentlyLoading: PropTypes.bool,
    // provided by redux
    updateActivePermission: PropTypes.func,
    deleteActivePermission: PropTypes.func,
    updateCurrentPermissionsPageState: PropTypes.func,
    updatePermissionsPageSizeState: PropTypes.func,
    updateSearchActivePermissionsStates: PropTypes.func
  };

  state = {
    showViewEditDetailsPopup: false,
    showDeleteConfirmationPopup: false,
    showUpdatePermissionDataConfirmationPopup: false,
    isValidGocEmail: true,
    isValidSupervisor: true,
    isValidSupervisorEmail: true,
    reasonForModifications: "",
    isValidReasonForModifications: true,
    searchBarContent: "",
    displayResultsFound: false,
    selectedActivePermission: {},
    rowsDefinition: {},
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.permissionsPaginationPageSize}`,
      value: this.props.permissionsPaginationPageSize
    }
  };

  componentDidMount = () => {
    this.populateDisplayOptions();
  };

  componentDidUpdate = prevProps => {
    // if rowsDefinition gets updated
    if (prevProps.rowsDefinition !== this.props.rowsDefinition) {
      this.setState({ rowsDefinition: this.props.rowsDefinition });
    }
    // if selectedActivePermission gets updated
    if (prevProps.selectedActivePermission !== this.props.selectedActivePermission) {
      // open view/edit popup
      this.setState({
        showViewEditDetailsPopup: true,
        selectedActivePermission: this.props.selectedActivePermission
      });
    }
  };

  // update gocEmail content
  updateGocEmailContent = event => {
    const gocEmail = event.target.value;
    this.setState(prevState => {
      let selectedActivePermission = { ...prevState.selectedActivePermission };
      selectedActivePermission.gocEmail = gocEmail;
      return { selectedActivePermission };
    });
  };

  // update supervisor content
  updateSupervisorContent = event => {
    const supervisor = event.target.value;
    this.setState(prevState => {
      let selectedActivePermission = { ...prevState.selectedActivePermission };
      selectedActivePermission.supervisor = supervisor;
      return { selectedActivePermission };
    });
  };

  // update supervisorEmail content
  updateSupervisorEmailContent = event => {
    const supervisorEmail = event.target.value;
    this.setState(prevState => {
      let selectedActivePermission = { ...prevState.selectedActivePermission };
      selectedActivePermission.supervisorEmail = supervisorEmail;
      return { selectedActivePermission };
    });
  };

  // update reasonForModifications content
  updateReasonForModificationsContent = event => {
    const reasonForModifications = event.target.value;
    this.setState({
      reasonForModifications: reasonForModifications
    });
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  closeViewEditDetailsPopup = () => {
    this.setState({ showViewEditDetailsPopup: false });
  };

  // resetting states whenever the popup is being closed
  handleCloseViewEditDetailsPopup = () => {
    this.setState({
      isValidGocEmail: true,
      isValidSupervisor: true,
      isValidSupervisorEmail: true,
      isValidReasonForModifications: true,
      reasonForModifications: ""
    });
  };

  // handling delete permission action
  handleDeletePermission = () => {
    // deleting specified user permission
    this.props
      .deleteActivePermission(this.state.selectedActivePermission.userPermissionId)
      .then(response => {
        // permission has been deleted
        if (response.status === 200) {
          // re-populating active permissions table
          if (this.props.permissionsActiveSearch) {
            this.props.populateFoundActivePermissions();
          } else {
            this.props.populateActivePermissions();
          }
          this.setState({ showViewEditDetailsPopup: false, showDeleteConfirmationPopup: false });
          // should never happen
        } else {
          throw new Error("Something went wrong during delete permission process");
        }
      });
  };

  openDeleteConfirmationPopup = () => {
    this.setState({ showDeleteConfirmationPopup: true });
  };

  closeDeleteConfirmationPopup = () => {
    this.setState({ showDeleteConfirmationPopup: false });
  };

  // handling save data action
  handleSaveData = () => {
    if (this.validateForm()) {
      let data = {
        userPermissionId: this.state.selectedActivePermission.userPermissionId,
        gocEmail: this.state.selectedActivePermission.gocEmail,
        supervisor: this.state.selectedActivePermission.supervisor,
        supervisorEmail: this.state.selectedActivePermission.supervisorEmail
      };
      this.props.updateActivePermission(data).then(response => {
        // permission data has been saved
        if (response.status === 200) {
          // re-populating active permissions table
          if (this.props.permissionsActiveSearch) {
            this.props.populateFoundActivePermissions();
          } else {
            this.props.populateActivePermissions();
          }
          this.setState({
            showViewEditDetailsPopup: false,
            showUpdatePermissionDataConfirmationPopup: true
          });
          // should never happen
        } else {
          throw new Error("Something went wrong during update permission data process");
        }
      });
    }
  };

  closeUpdateConfirmationPopup = () => {
    this.setState({ showUpdatePermissionDataConfirmationPopup: false });
  };

  // validating form (on save data action)
  validateForm = () => {
    const { gocEmail, supervisor, supervisorEmail } = this.state.selectedActivePermission;
    const { reasonForModifications } = this.state;

    let isFormValid = false;

    // goc email validation
    const isValidGocEmail = validateEmail(gocEmail);

    // supervisor validation
    const isValidSupervisor = validateName(supervisor);

    // supervisor email validation
    const isValidSupervisorEmail = validateEmail(supervisorEmail);

    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = regexExpression.test(reasonForModifications)
      ? true
      : false;

    // saving all validation results in states
    this.setState(
      {
        isValidGocEmail: isValidGocEmail,
        isValidSupervisor: isValidSupervisor,
        isValidSupervisorEmail: isValidSupervisorEmail,
        isValidReasonForModifications: isValidReasonForModifications
      },
      () => {
        this.focusOnHighestErrorField();
      }
    );

    // checking if all validations are met
    if (
      isValidGocEmail &&
      isValidSupervisor &&
      isValidSupervisorEmail &&
      isValidReasonForModifications
    ) {
      isFormValid = true;
    }

    return isFormValid;
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidGocEmail) {
      document.getElementById("goc-email").focus();
    } else if (!this.state.isValidSupervisor) {
      document.getElementById("supervisor").focus();
    } else if (!this.state.isValidSupervisorEmail) {
      document.getElementById("supervisor-email").focus();
    } else if (!this.state.isValidReasonForModifications) {
      document.getElementById("reason-for-modifications").focus();
    }
  };

  // handling search functionality
  handleSearch = () => {
    // initializing searchBarContent state to null so search functionality is triggered even if the search bar content is still the same
    this.props.updateSearchActivePermissionsStates(null, true);
    this.setState({ displayResultsFound: true }, () => {
      // go back to the first page to avoid display bugs
      this.props.updateCurrentPermissionsPageState(1);
      // updating search permissionsKeyword, permissionsActiveSearch redux states
      this.props.updateSearchActivePermissionsStates(this.state.searchBarContent, true);
    });
  };

  // handling clear search functionality
  handleClearSearch = () => {
    // updating search permissionsKeyword, permissionsActiveSearch redux states
    this.props.updateSearchActivePermissionsStates("", false);
    // hide results found label + put back all existing active permissions + delete search bar content
    this.setState(
      {
        displayResultsFound: false,
        activePermissions: this.props.activePermissions,
        searchBarContent: ""
      },
      () => {
        // go back to the first page to avoid display bugs
        this.props.updateCurrentPermissionsPageState(1);
        // focusing on search bar
        document.getElementById("permissions-search-bar").focus();
      }
    );
  };

  // get selected display option
  getSelectedDisplayOption = selectedOption => {
    this.setState(
      {
        displayOptionSelectedValue: selectedOption
      },
      () => {
        // update page size
        this.props.updatePermissionsPageSizeState(selectedOption.value);
        // go back to the first page to avoid display bugs
        this.props.updateCurrentPermissionsPageState(1);
      }
    );
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  // handling page changes based on pagination selection
  handlePageChange = id => {
    // "+1" because redux permissionsPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateCurrentPermissionsPageState(selectedPage);
    // focusing on the first table's row
    document.getElementById("active-permissions-button-row-0").focus();
  };

  render() {
    const columnsDefinition = [
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table.user,
        style: { width: "40%" }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table.permission,
        style: { width: "30%" }
      },
      {
        label: LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.table.action,
        style: { ...{ width: "30%" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div>
        <div style={styles.searchBarAndDisplayContainer}>
          <div
            style={
              this.state.displayResultsFound
                ? styles.searchBarContainerWithResultsFound
                : styles.searchBarContainerWithoutResultsFound
            }
          >
            <label
              htmlFor="permissions-search-bar"
              id="permissions-search-bar-title"
              style={styles.searchBarLabel}
            >
              {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.searchBarTitle}
            </label>
            <div style={styles.searchBarContainer}>
              <input
                id="permissions-search-bar"
                aria-labelledby="permissions-search-bar-title"
                style={styles.searchBarInput}
                type="text"
                value={this.state.searchBarContent}
                onChange={this.updateSearchBarContent}
                onKeyPress={event => {
                  if (event.key === "Enter") {
                    this.handleSearch();
                  }
                }}
              ></input>
              {this.state.displayResultsFound && !this.props.currentlyLoading && (
                <div style={styles.resultsFound}>
                  <label
                    id="active-permissions-results-found-label"
                    htmlFor="permissions-search-bar"
                    tabIndex={0}
                  >
                    {this.props.activePermissions.length > 1
                      ? LOCALIZE.formatString(
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .multipleResultsFound,
                          this.props.resultsFound
                        )
                      : LOCALIZE.formatString(
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .singularResultFound,
                          this.props.resultsFound
                        )}
                  </label>
                  <button
                    className="clear-search"
                    style={styles.clearSearchResults}
                    tabIndex={0}
                    onClick={this.handleClearSearch}
                  >
                    {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.clearSearch}
                  </button>
                </div>
              )}
            </div>
            <div style={styles.searchIconButtonContainer}>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faSearch} />
                    <label style={styles.hiddenText}>
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .searchBarTitle
                      }
                    </label>
                  </>
                }
                action={this.handleSearch}
                customStyle={styles.searchIconButton}
                buttonTheme={THEME.SECONDARY}
                disabled={this.props.currentlyLoading}
              />
            </div>
          </div>
          <div style={styles.displayOptionContainer}>
            <div style={styles.displayOptionLabel}>
              <label id="display-options-label">
                {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.displayOptionLabel}
              </label>
              <label id="display-option-accessibility" style={styles.hiddenText}>
                {
                  LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                    .displayOptionAccessibility
                }
              </label>
            </div>
            <div style={styles.displayOptionDropdown}>
              <Select
                id="display-options-dropdown"
                name="display-options"
                aria-labelledby="display-options-label display-option-accessibility display-option-current-value-accessibility"
                placeholder=""
                options={this.state.displayOptionsArray}
                onChange={this.getSelectedDisplayOption}
                clearable={false}
                value={this.state.displayOptionSelectedValue}
              ></Select>
              <label
                id="display-option-current-value-accessibility"
                style={styles.hiddenText}
              >{`${LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.displayOptionCurrentValueAccessibility} ${this.state.displayOptionSelectedValue.value}`}</label>
            </div>
          </div>
        </div>
        <div>
          <GenericTable
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={
              LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.noResultsFound
            }
            currentlyLoading={this.props.currentlyLoading}
          />
          <div style={styles.paginationContainer}>
            <ReactPaginate
              pageCount={this.props.numberOfPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and permissionsPaginationPage redux state uses index 1
              forcePage={this.props.permissionsPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .previousPageButton
                    }
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={styles.paginationIcon}>
                  <label style={styles.hiddenText}>
                    {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showViewEditDetailsPopup}
          handleClose={() => {}}
          closeButtonAction={this.closeViewEditDetailsPopup}
          shouldCloseOnEsc={true}
          onPopupClose={this.handleCloseViewEditDetailsPopup}
          displayCloseButton={true}
          isBackdropStatic={true}
          title={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .title
          }
          description={
            <div style={styles.popupContainer}>
              <p style={styles.description}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                    .viewEditDetailsPopup.description,
                  <span style={styles.userName}>
                    {this.state.selectedActivePermission.firstName}{" "}
                    {this.state.selectedActivePermission.lastName}
                  </span>
                )}
              </p>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label id="goc-email-title" htmlFor="goc-email">
                    {LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}
                  </label>
                </label>
                <input
                  id="goc-email"
                  className={this.state.isValidGocEmail ? "valid-field" : "invalid-field"}
                  aria-labelledby="goc-email-title goc-email-error"
                  aria-required={true}
                  aria-invalid={!this.state.isValidGocEmail}
                  style={styles.input}
                  type="text"
                  value={this.state.selectedActivePermission.gocEmail}
                  onChange={this.updateGocEmailContent}
                ></input>
                {!this.state.isValidGocEmail && (
                  <label id="goc-email-error" htmlFor="goc-email" style={styles.errorMessage}>
                    {LOCALIZE.profile.permissions.addPermissionPopup.gocEmailError}
                  </label>
                )}
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>{LOCALIZE.profile.permissions.addPermissionPopup.pri}</label>
                </label>
                <input
                  id="pri-or-military-number"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.selectedActivePermission.priOrMilitaryNbr}
                  onChange={() => {}}
                ></input>
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label id="supervisor-title" htmlFor="supervisor">
                    {LOCALIZE.profile.permissions.addPermissionPopup.supervisor}
                  </label>
                </label>
                <input
                  id="supervisor"
                  className={this.state.isValidSupervisor ? "valid-field" : "invalid-field"}
                  aria-labelledby="supervisor-title supervisor-error"
                  aria-required={true}
                  aria-invalid={!this.state.isValidSupervisor}
                  style={styles.input}
                  type="text"
                  value={this.state.selectedActivePermission.supervisor}
                  onChange={this.updateSupervisorContent}
                ></input>
                {!this.state.isValidSupervisor && (
                  <label id="supervisor-error" htmlFor="supervisor" style={styles.errorMessage}>
                    {LOCALIZE.profile.permissions.addPermissionPopup.supervisorError}
                  </label>
                )}
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label id="supervisor-email-title" htmlFor="supervisor-email">
                    {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmail}
                  </label>
                </label>
                <input
                  id="supervisor-email"
                  className={this.state.isValidSupervisorEmail ? "valid-field" : "invalid-field"}
                  aria-labelledby="supervisor-email-title supervisor-email-error"
                  aria-required={true}
                  aria-invalid={!this.state.isValidSupervisorEmail}
                  style={styles.input}
                  type="text"
                  value={this.state.selectedActivePermission.supervisorEmail}
                  onChange={this.updateSupervisorEmailContent}
                ></input>
                {!this.state.isValidSupervisorEmail && (
                  <label
                    id="supervisor-email-error"
                    htmlFor="supervisor-email"
                    style={styles.errorMessage}
                  >
                    {LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmailError}
                  </label>
                )}
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>{LOCALIZE.profile.permissions.addPermissionPopup.permissions}</label>
                </label>
                <input
                  id="permission"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.selectedActivePermission.permission}
                  onChange={() => {}}
                ></input>
              </div>
              <div>
                <div>
                  <label style={styles.reasonLabel}>
                    <label id="reason-for-modifications-title" htmlFor="reason-for-modifications">
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .viewEditDetailsPopup.reasonForModification
                      }
                    </label>
                  </label>
                </div>
                <div>
                  <textarea
                    id="reason-for-modifications"
                    className={
                      this.state.isValidReasonForModifications ? "valid-field" : "invalid-field"
                    }
                    aria-labelledby="reason-for-modifications-title reason-for-modifications-error"
                    aria-required={true}
                    aria-invalid={!this.state.isValidReasonForModifications}
                    style={{ ...styles.input, ...styles.reasonInput }}
                    value={this.state.reasonForModifications}
                    onChange={this.updateReasonForModificationsContent}
                    maxLength="300"
                  ></textarea>
                  {!this.state.isValidReasonForModifications && (
                    <label
                      id="reason-for-modifications-error"
                      htmlFor="reason-for-modifications"
                      style={styles.ReasonErrorMessage}
                    >
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .viewEditDetailsPopup.reasonForModificationError
                      }
                    </label>
                  )}
                </div>
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faTrashAlt}
          leftButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .deleteButton
          }
          leftButtonAction={this.openDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faSave}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .saveButton
          }
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonAction={this.handleSaveData}
        />
        <PopupBox
          show={this.state.showDeleteConfirmationPopup}
          handleClose={this.closeDeleteConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          customPopupStyle={styles.deleteConfirmationPopupWidth}
          title={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
              .deletePermissionConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={
                  LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                    .deletePermissionConfirmationPopup.systemMessageTitle
                }
                message={
                  <div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                          .deletePermissionConfirmationPopup.systemMessageDescription,
                        <span style={styles.boldText}>
                          {this.state.selectedActivePermission.permission}
                        </span>,
                        <span style={styles.boldText}>
                          {this.state.selectedActivePermission.firstName}{" "}
                          {this.state.selectedActivePermission.lastName}
                        </span>
                      )}
                    </p>
                  </div>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.primary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrashAlt}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.viewEditDetailsPopup
              .deleteButton
          }
          rightButtonAction={this.handleDeletePermission}
        />
        <PopupBox
          show={this.state.showUpdatePermissionDataConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
              .updatePermissionDataConfirmationPopup.title
          }
          description={
            <div>
              {LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                  .updatePermissionDataConfirmationPopup.description,
                <span style={styles.boldText}>
                  {this.state.selectedActivePermission.firstName}{" "}
                  {this.state.selectedActivePermission.lastName}
                </span>
              )}
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeUpdateConfirmationPopup}
        />
      </div>
    );
  }
}

export { ActivePermissions as unconnectedActivePermissions };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    permissionsPaginationPageSize: state.userPermissions.permissionsPaginationPageSize,
    permissionsPaginationPage: state.userPermissions.permissionsPaginationPage,
    permissionsActiveSearch: state.userPermissions.permissionsActiveSearch
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateActivePermission,
      deleteActivePermission,
      updateCurrentPermissionsPageState,
      updatePermissionsPageSizeState,
      updateSearchActivePermissionsStates
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActivePermissions);
