import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBinoculars, faMinusCircle, faCheck } from "@fortawesome/free-solid-svg-icons";
import LOCALIZE from "../../../text_resources";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import { grantPermission, denyPermission } from "../../../modules/PermissionsRedux";
import "../../../css/etta.css";
import { alternateColorsStyle } from "../../commons/Constants";
import CustomButton, { THEME } from "../../../components/commons/CustomButton";

const styles = {
  mainContainer: {
    borderStyle: "solid",
    borderWidth: "0 1px 1px 1px",
    borderColor: "#CECECE"
  },
  rowLabel: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "80%"
  },
  rowButton: {
    display: "table-cell",
    verticalAlign: "middle",
    width: "20%",
    textAlign: "center"
  },
  displayTable: {
    display: "table"
  },
  buttonIcon: {
    transform: "scale(2)",
    padding: "1.5px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  formContainer: {
    padding: "24px 6px"
  },
  label: {
    width: "40%"
  },
  labelHeight: {
    minHeight: 36
  },
  textAreaLabel: {
    marginTop: 18
  },
  input: {
    width: "60%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4,
    marginTop: 12
  },
  textAreaContainer: {
    marginBottom: 6,
    width: "100%",
    height: 98
  },
  textAreaBox: {
    float: "right",
    width: "60%"
  },
  textAreaInput: {
    height: 85,
    overflowY: "scroll",
    resize: "none",
    display: "block",
    width: "calc(100% - 0.5px)"
  },
  floatLeft: {
    float: "left"
  },
  description: {
    marginBottom: 12
  },
  userName: {
    fontWeight: "bold"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  noPermission: {
    height: 60,
    padding: 12
  }
};

class PermissionRequests extends Component {
  static propTypes = {
    pendingPermissions: PropTypes.array.isRequired,
    populatePendingPermissions: PropTypes.func.isRequired,
    populateActivePermissions: PropTypes.func.isRequired,
    // provided by redux
    grantPermission: PropTypes.func,
    denyPermission: PropTypes.func
  };

  state = {
    showViewPermissionRequestPopup: false,
    username: "",
    firstName: "",
    lastName: "",
    gocEmail: "",
    priOrMilitaryNbr: "",
    supervisor: "",
    supervisorEmail: "",
    rationale: "",
    permissionRequested: "",
    permissionRequestedId: "",
    permissionRequestId: "",
    deniedReason: "",
    isValidDeniedReasonOnDeny: true,
    isValidDeniedReasonOnApprove: true,
    usernameNoLongerExistsError: false
  };

  handleOpenPermissionRequestPopup = id => {
    this.setState({
      showViewPermissionRequestPopup: true,
      username: this.props.pendingPermissions[id].username,
      firstName: this.props.pendingPermissions[id].first_name,
      lastName: this.props.pendingPermissions[id].last_name,
      gocEmail: this.props.pendingPermissions[id].goc_email,
      priOrMilitaryNbr: this.props.pendingPermissions[id].pri_or_military_nbr,
      supervisor: this.props.pendingPermissions[id].supervisor,
      supervisorEmail: this.props.pendingPermissions[id].supervisor_email,
      rationale: this.props.pendingPermissions[id].rationale,
      permissionRequested: this.props.pendingPermissions[id].permission_requested,
      permissionRequestedId: this.props.pendingPermissions[id].permission_requested_id,
      permissionRequestId: this.props.pendingPermissions[id].permission_request_id
    });
  };

  closePermissionRequestPopop = () => {
    this.setState({ showViewPermissionRequestPopup: false });
  };

  // resetting state whenever the popup is being closed
  handleClosePermissionRequestPopup = () => {
    this.setState({
      username: "",
      firstName: "",
      lastName: "",
      gocEmail: "",
      priOrMilitaryNbr: "",
      supervisor: "",
      supervisorEmail: "",
      rationale: "",
      permissionRequested: "",
      permissionRequestedId: "",
      permissionRequestId: "",
      deniedReason: "",
      isValidDeniedReasonOnDeny: true,
      isValidDeniedReasonOnApprove: true,
      usernameNoLongerExistsError: false
    });
  };

  // handle approve permission request
  handleApprovePermissionRequest = () => {
    if (this.validateFormOnApprove()) {
      // grant specified permission to specified user (note that this API is also deleting the related permission request)
      this.props
        .grantPermission(this.state.username, this.state.permissionRequestedId)
        .then(response => {
          // if permission has been granted successfully
          if (response.status === 200) {
            // re-populating pending permissions and active permissions tables
            this.props.populatePendingPermissions();
            this.props.populateActivePermissions();
            // closing popup
            this.setState({ showViewPermissionRequestPopup: false });
            // if there is an error (shouldn't happen)
          } else if (response.error !== "") {
            // username has been deleted, but the request still open
            if (response.error === "the specified username does not exist") {
              this.setState({ usernameNoLongerExistsError: true });
            }
            // should never happen
          } else {
            throw new Error("Something went wrong during grant permission process");
          }
        });
    }
  };

  //handle deny permission request
  handleDenyPermissionRequest = () => {
    if (this.validateFormOnDeny()) {
      // deleting specified permission_request_id from permission request table
      this.props.denyPermission(this.state.permissionRequestId).then(response => {
        // if permission has been deleted successfully
        if (response.status === 200) {
          // re-populating pending permissions table
          this.props.populatePendingPermissions();
          // closing popup
          this.setState({ showViewPermissionRequestPopup: false });
          // should never happen
        } else {
          throw new Error("Something went wrong during deny permission process");
        }
      });
    }
  };

  // update denied reason content
  updateDeniedReasonContent = event => {
    const deniedReasonContent = event.target.value;
    this.setState({
      deniedReason: deniedReasonContent
    });
  };

  // handles form validation and returns bool based on fields validity
  validateFormOnDeny = () => {
    let isValidForm = false;

    // denied reason validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidDeniedReasonOnDeny = regexExpression.test(this.state.deniedReason) ? true : false;

    // if all needed fields are valid
    if (isValidDeniedReasonOnDeny) {
      isValidForm = true;
    } else {
      // focusing denied reason field
      document.getElementById("denied-reason").focus();
    }

    // setting states
    this.setState({
      isValidDeniedReasonOnDeny: isValidDeniedReasonOnDeny,
      isValidDeniedReasonOnApprove: true
    });

    return isValidForm;
  };

  validateFormOnApprove = () => {
    let isValidForm = false;

    let isValidDeniedReasonOnApprove = this.state.deniedReason === "" ? true : false;

    // if denied reasons is empty
    if (isValidDeniedReasonOnApprove) {
      isValidForm = true;
    } else {
      // focusing denied reason field
      document.getElementById("denied-reason").focus();
    }

    this.setState({
      isValidDeniedReasonOnApprove: isValidDeniedReasonOnApprove,
      isValidDeniedReasonOnDeny: true
    });

    return isValidForm;
  };

  render() {
    return (
      <div style={styles.mainContainer}>
        {this.props.pendingPermissions.length > 0 &&
          this.props.pendingPermissions.map((permission, id) => {
            return (
              <div
                key={id}
                style={{ ...styles.displayTable, ...alternateColorsStyle(id, 60) }}
                tabIndex={0}
              >
                <div style={styles.rowLabel}>
                  <label aria-hidden={true}>
                    {permission.permission_requested} [{permission.last_name},{" "}
                    {permission.first_name}]
                  </label>
                  <label style={styles.hiddenText}>
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests
                        .permissionRequestedAccessibility
                    }{" "}
                    {permission.permission_requested},{" "}
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests
                        .lastNameAccessibility
                    }{" "}
                    {permission.last_name},{" "}
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests
                        .firstNameAccessibility
                    }{" "}
                    {permission.first_name}
                  </label>
                </div>
                <div style={styles.rowButton}>
                  <CustomButton
                    label={<FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />}
                    action={() => this.handleOpenPermissionRequestPopup(id)}
                    buttonTheme={THEME.SECONDARY}
                    ariaLabel={
                      LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests
                        .viewButtonAccessibility
                    }
                  />
                </div>
              </div>
            );
          })}
        {this.props.pendingPermissions.length <= 0 && (
          <div style={styles.noPermission}>
            <p>{LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.noPermission}</p>
          </div>
        )}
        <PopupBox
          show={this.state.showViewPermissionRequestPopup}
          handleClose={() => {}}
          closeButtonAction={this.closePermissionRequestPopop}
          shouldCloseOnEsc={true}
          onPopupClose={this.handleClosePermissionRequestPopup}
          displayCloseButton={true}
          isBackdropStatic={true}
          title={LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.title}
          description={
            <div style={styles.formContainer}>
              <p style={styles.description}>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                    .description,
                  <span style={styles.userName}>
                    {this.state.firstName} {this.state.lastName}
                  </span>
                )}
              </p>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>{LOCALIZE.profile.permissions.addPermissionPopup.gocEmail}</label>
                </label>
                <input
                  id="goc-email"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.gocEmail}
                  onChange={() => {}}
                ></input>
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>{LOCALIZE.profile.permissions.addPermissionPopup.pri}</label>
                </label>
                <input
                  id="pri-or-military-number"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.priOrMilitaryNbr}
                  onChange={() => {}}
                ></input>
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>{LOCALIZE.profile.permissions.addPermissionPopup.supervisor}</label>
                </label>
                <input
                  id="supervisor"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.supervisor}
                  onChange={() => {}}
                ></input>
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>{LOCALIZE.profile.permissions.addPermissionPopup.supervisorEmail}</label>
                </label>
                <input
                  id="supervisor-email"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.supervisorEmail}
                  onChange={() => {}}
                ></input>
              </div>
              <div style={styles.textAreaContainer}>
                <div style={{ ...styles.label, ...styles.labelHeight, ...styles.floatLeft }}>
                  <label style={styles.textAreaLabel}>
                    <label>{LOCALIZE.profile.permissions.addPermissionPopup.rationale}</label>
                  </label>
                </div>
                <div style={styles.textAreaBox}>
                  <textarea
                    id="rationale"
                    disabled={true}
                    style={{ ...styles.input, ...styles.textAreaInput }}
                    type="text"
                    value={this.state.rationale}
                    onChange={() => {}}
                  ></textarea>
                </div>
              </div>
              <div>
                <label style={{ ...styles.label, ...styles.labelHeight }}>
                  <label>
                    {LOCALIZE.profile.permissions.addPermissionPopup.permissionRequested}
                  </label>
                </label>
                <input
                  id="permission-requested"
                  disabled={true}
                  style={styles.input}
                  type="text"
                  value={this.state.permissionRequested}
                  onChange={() => {}}
                ></input>
              </div>
              <div style={styles.textAreaContainer}>
                <div style={{ ...styles.label, ...styles.labelHeight, ...styles.floatLeft }}>
                  <label style={styles.textAreaLabel}>
                    <label id="denied-reason-label" htmlFor="denied-reason">
                      {
                        LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                          .deniedReason
                      }
                    </label>
                  </label>
                </div>
                <div style={styles.textAreaBox}>
                  <textarea
                    className={
                      this.state.isValidDeniedReasonOnApprove &&
                      this.state.isValidDeniedReasonOnDeny
                        ? "valid-field"
                        : "invalid-field"
                    }
                    id="denied-reason"
                    style={{ ...styles.input, ...styles.textAreaInput }}
                    type="text"
                    aria-labelledby="denied-reason-label denied-reason-missing-content-error denied-reason-empty-content-error"
                    value={this.state.deniedReason}
                    onChange={this.updateDeniedReasonContent}
                  ></textarea>
                  <div>
                    {!this.state.isValidDeniedReasonOnDeny && (
                      <label
                        id="denied-reason-missing-content-error"
                        htmlFor="denied-reason"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                            .grantPermissionErrors.missingDeniedReasonError
                        }
                      </label>
                    )}
                    {!this.state.isValidDeniedReasonOnApprove && (
                      <label
                        id="denied-reason-empty-content-error"
                        htmlFor="denied-reason"
                        style={styles.errorMessage}
                      >
                        {
                          LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                            .grantPermissionErrors.notEmptyDeniedReasonError
                        }
                      </label>
                    )}
                  </div>
                </div>
              </div>
              {this.state.usernameNoLongerExistsError && (
                <div>
                  <label style={styles.errorMessage}>
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup
                        .grantPermissionErrors.usernameDoesNotExistError
                    }
                  </label>
                </div>
              )}
            </div>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faMinusCircle}
          leftButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.denyButton
          }
          leftButtonAction={this.handleDenyPermissionRequest}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faCheck}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.permissions.tabs.permissionRequests.popup.grantButton
          }
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonAction={this.handleApprovePermissionRequest}
        />
      </div>
    );
  }
}

export { PermissionRequests as unconnectedPermissionRequests };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      grantPermission,
      denyPermission
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(PermissionRequests);
