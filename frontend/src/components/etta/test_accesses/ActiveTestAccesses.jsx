import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import LOCALIZE from "../../../text_resources";
import { LANGUAGES } from "../../../modules/LocalizeRedux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSearch,
  faBinoculars,
  faCaretLeft,
  faCaretRight,
  faTrashAlt,
  faSave,
  faTimes
} from "@fortawesome/free-solid-svg-icons";
import Select from "react-select";
import { styles as ActivePermissionsStyles } from "../permissions/ActivePermissions";
import { styles as AssignTestAccessesStyles } from "./AssignTestAccesses";
import {
  getAllActiveTestPermissions,
  getFoundActiveTestPermissions,
  updateCurrentTestPermissionsPageState,
  updateTestPermissionsPageSizeState,
  updateSearchActiveTestPermissionsStates,
  deleteTestPermission,
  updateTestPermission
} from "../../../modules/PermissionsRedux";
import ReactPaginate from "react-paginate";
import "../../../css/etta.css";
import { getDisplayOptions } from "../../../helpers/getDisplayOptions";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import DatePicker from "../../../components/commons/DatePicker";
import populateCustomFiveYearsFutureDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import SystemMessage, { MESSAGE_TYPE } from "../../commons/SystemMessage";
import GenericTable, { COMMON_STYLE } from "../../commons/GenericTable";
import CustomButton, { THEME } from "../../../components/commons/CustomButton";

const styles = {
  viewButton: {
    minWidth: 100
  },
  buttonIcon: {
    transform: "scale(1.5)",
    padding: "1.5px",
    marginRight: 12
  },
  popupContainer: {
    padding: "10px 15px"
  },
  labelContainer: {
    display: "table-cell"
  },
  boldText: {
    fontWeight: "bold"
  },
  testAdminRow: {
    padding: "6px 6px 6px 12px"
  },
  tableRow: {
    padding: 6
  },
  actionRow: {
    padding: 12
  }
};

class ActiveTestAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  expiryDateRef = React.createRef();

  static propTypes = {
    // provided by redux
    getAllActiveTestPermissions: PropTypes.func,
    getFoundActiveTestPermissions: PropTypes.func,
    updateCurrentTestPermissionsPageState: PropTypes.func,
    updateTestPermissionsPageSizeState: PropTypes.func,
    updateSearchActiveTestPermissionsStates: PropTypes.func,
    deleteTestPermission: PropTypes.func,
    updateTestPermission: PropTypes.func
  };

  state = {
    displayResultsFound: false,
    searchBarContent: "",
    resultsFound: 0,
    clearSearchTriggered: false,
    displayOptionsArray: [],
    displayOptionSelectedValue: {
      label: `${this.props.testPermissionsPaginationPageSize}`,
      value: this.props.testPermissionsPaginationPageSize
    },
    activeTestPermissions: [],
    rowsDefinition: {},
    numberOfPages: 1,
    nextPageNumber: 0,
    previousPageNumber: 0,
    currentlyLoading: false,
    showViewTestPermissionPopup: false,
    test_permission_id: "",
    user: "",
    username: "",
    testOrderNumber: "",
    staffingProcessNumber: "",
    departmentMinistryCode: "",
    billingContact: "",
    billingContactInfo: "",
    isOrg: "",
    isRef: "",
    testAccess: "",
    expiryDate: "",
    reasonForModifications: "",
    isValidReasonForModifications: true,
    triggerExpiryDateValidation: false,
    showDeleteConfirmationPopup: false,
    showSaveConfirmationPopup: false
  };

  componentDidMount = () => {
    // initialize pagination page redux state
    this.props.updateCurrentTestPermissionsPageState(1);
    // initialize active search redux state
    this.props.updateSearchActiveTestPermissionsStates("", false);
    this.populateActiveTestPermissions();
    this.populateDisplayOptions();
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    //if currentLanguage gets updated
    if (prevProps.currentLanguage !== this.props.currentLanguage) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if testPermissionsPaginationPage gets updated
    if (prevProps.testPermissionsPaginationPage !== this.props.testPermissionsPaginationPage) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if testPermissionsPaginationPageSize get updated
    if (
      prevProps.testPermissionsPaginationPageSize !== this.props.testPermissionsPaginationPageSize
    ) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if search testPermissionsKeyword gets updated
    if (prevProps.testPermissionsKeyword !== this.props.testPermissionsKeyword) {
      // if testPermissionsKeyword redux state is empty
      if (this.props.testPermissionsKeyword !== "") {
        this.populateFoundActiveTestPermissions();
      } else if (
        this.props.testPermissionsKeyword === "" &&
        this.props.testPermissionsActiveSearch
      ) {
        this.populateFoundActiveTestPermissions();
      }
    }
    // if testPermissionsActiveSearch gets updated
    if (prevProps.testPermissionsActiveSearch !== this.props.testPermissionsActiveSearch) {
      // there is no current active search (on clear search action)
      if (!this.props.testPermissionsActiveSearch) {
        // triggering clear search action state
        this.setState({ clearSearchTriggered: true }, () => {
          this.populateActiveTestPermissions();
        });
        // there is a current active search (on search action)
      } else {
        this.populateFoundActiveTestPermissions();
      }
    }
    // if triggerPopulateTestPermissions gets updated
    if (prevProps.triggerPopulateTestPermissions !== this.props.triggerPopulateTestPermissions) {
      this.populateActiveTestPermissionsBasedOnActiveSearch();
    }
    // if completeDatePicked gets updated
    if (prevProps.completeDatePicked !== this.props.completeDatePicked) {
      // set new expiryDate state
      this.setState({ expiryDate: this.props.completeDatePicked });
    }
  };

  // update searchBarContent content
  updateSearchBarContent = event => {
    const searchBarContent = event.target.value;
    this.setState({
      searchBarContent: searchBarContent
    });
  };

  // get selected display option
  getSelectedDisplayOption = selectedOption => {
    this.setState(
      {
        displayOptionSelectedValue: selectedOption
      },
      () => {
        // update page size
        this.props.updateTestPermissionsPageSizeState(selectedOption.value);
        // go back to the first page to avoid display bugs
        this.props.updateCurrentTestPermissionsPageState(1);
      }
    );
  };

  // populating active test permissions
  populateActiveTestPermissions = () => {
    this._isMounted = true;
    this.setState({ currentlyLoading: true }, () => {
      let activeTestPermissionsArray = [];
      this.props
        .getAllActiveTestPermissions(
          this.props.testPermissionsPaginationPage,
          this.props.testPermissionsPaginationPageSize
        )
        .then(response => {
          if (this._isMounted) {
            this.populateActiveTestPermissionsObject(activeTestPermissionsArray, response);
          }
        })
        .then(() => {
          if (this._isMounted) {
            if (this.state.clearSearchTriggered) {
              // go back to the first page to avoid display bugs
              this.props.updateCurrentTestPermissionsPageState(1);
              this.setState({ clearSearchTriggered: false });
            }
          }
        })
        .then(() => {
          if (this._isMounted) {
            this.setState({ currentlyLoading: false });
          }
        });
    });
  };

  // populating all found active test permissions based on a search
  populateFoundActiveTestPermissions = () => {
    this.setState({ currentlyLoading: true }, () => {
      let activeTestPermissionsArray = [];
      this.props
        .getFoundActiveTestPermissions(
          this.props.currentLanguage,
          this.props.testPermissionsKeyword,
          this.props.testPermissionsPaginationPage,
          this.props.testPermissionsPaginationPageSize
        )
        .then(response => {
          // there are no results found
          if (response[0] === "no results found") {
            this.setState({
              activeTestPermissions: [],
              rowsDefinition: {},
              numberOfPages: 1,
              nextPageNumber: 0,
              previousPageNumber: 0,
              resultsFound: 0
            });
            // there is at least one result found
          } else {
            this.populateActiveTestPermissionsObject(activeTestPermissionsArray, response);
            this.setState({ resultsFound: response.count });
          }
        })
        .then(() => {
          this.setState({ currentlyLoading: false, displayResultsFound: true }, () => {
            // if there is at least one result found
            if (activeTestPermissionsArray.length > 0) {
              // focusing on results found label only if save confirmation popup is not displayed (avoiding focus bug)
              if (!this.state.showSaveConfirmationPopup) {
                document.getElementById("active-test-accesses-results-found-label").focus();
              }
              // no results found
            } else {
              // focusing on no results found row
              document.getElementById("no-data").focus();
            }
          });
        });
    });
  };

  populateActiveTestPermissionsObject = (activeTestPermissionsArray, response) => {
    // initializing needed object and array for rowsDefinition props (needed for GenericTable component)
    let rowsDefinition = {};
    let data = [];
    // looping in response given
    for (let i = 0; i < response.results.length; i++) {
      let currentResult = response.results[i];
      // pushing needed data in activeTestPermissions array
      activeTestPermissionsArray.push({
        id: currentResult.id,
        first_name: currentResult.first_name,
        last_name: currentResult.last_name,
        en_test_name: currentResult.test.en_name,
        fr_test_name: currentResult.test.fr_name,
        version: currentResult.test.version,
        test_order_number: currentResult.test_order_number,
        expiry_date: currentResult.expiry_date,
        username: currentResult.username,
        staffing_process_number: currentResult.staffing_process_number,
        department_ministry_code: currentResult.department_ministry_code,
        billing_contact: currentResult.billing_contact,
        billing_contact_info: currentResult.billing_contact_info,
        is_org: currentResult.is_org,
        is_ref: currentResult.is_ref
      });
      // pushing needed data in data array (needed for GenericTable component)
      data.push({
        column_1: `${currentResult.last_name}, ${currentResult.first_name}`,
        column_2: `
          ${currentResult.test[`${this.props.currentLanguage}_name`]} v${
          currentResult.test.version
        }`,
        column_3: currentResult.test_order_number,
        column_4: currentResult.expiry_date,
        column_5: (
          <CustomButton
            id={`active-test-accesses-button-row-${i}`}
            label={
              <>
                <FontAwesomeIcon icon={faBinoculars} style={styles.buttonIcon} />
                <span>
                  {
                    LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                      .actionButtonLabel
                  }
                </span>
              </>
            }
            action={() => this.handleOpenViewPopup(i)}
            customStyle={styles.viewButton}
            buttonTheme={THEME.SECONDARY}
            ariaLabel={LOCALIZE.formatString(
              LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table
                .actionButtonAriaLabel,
              currentResult.first_name,
              currentResult.last_name,
              currentResult.test[`${this.props.currentLanguage}_name`],
              currentResult.test_order_number,
              currentResult.expiry_date
            )}
          />
        )
      });
    }

    // updating rowsDefinition object with provided data and needed style
    rowsDefinition = {
      column_1_style: styles.testAdminRow,
      column_2_style: { ...COMMON_STYLE.CENTERED_TEXT, ...styles.tableRow },
      column_3_style: { ...COMMON_STYLE.CENTERED_TEXT, ...styles.tableRow },
      column_4_style: { ...COMMON_STYLE.CENTERED_TEXT, ...styles.tableRow },
      column_5_style: { ...COMMON_STYLE.CENTERED_TEXT, ...styles.actionRow },
      data: data
    };

    // saving results in state
    this.setState({
      activeTestPermissions: activeTestPermissionsArray,
      rowsDefinition: rowsDefinition,
      numberOfPages: Math.ceil(response.count / this.props.testPermissionsPaginationPageSize),
      nextPageNumber: response.next_page_number,
      previousPageNumber: response.previous_page_number
    });
  };

  // populating active test permissions based on the testPermissionsActiveSearch redux state
  populateActiveTestPermissionsBasedOnActiveSearch = () => {
    // current search
    if (this.props.testPermissionsActiveSearch) {
      this.populateFoundActiveTestPermissions();
      // no current search
    } else {
      this.populateActiveTestPermissions();
    }
  };

  // handling search functionality
  handleSearch = () => {
    // initializing searchBarContent state to null so search functionality is triggered even if the search bar content is still the same
    this.props.updateSearchActiveTestPermissionsStates(null, true);
    // setting displayResultsFound to false here in order to create an asyn functions to call updateSearchActiveTestPermissionsStates
    // displayResultsFound will be set to true in populateFoundActiveTestPermissions function
    this.setState({ displayResultsFound: false }, () => {
      // go back to the first page to avoid display bugs
      this.props.updateCurrentTestPermissionsPageState(1);
      // updating search testPermissionsKeyword, testPermissionsActiveSearch redux states
      this.props.updateSearchActiveTestPermissionsStates(this.state.searchBarContent, true);
    });
  };

  // handling clear search functionality
  handleClearSearch = () => {
    // updating search testPermissionsKeyword, testPermissionsActiveSearch redux states
    this.props.updateSearchActiveTestPermissionsStates("", false);
    // hide results found label + put back all existing active permissions + delete search bar content
    this.setState(
      {
        displayResultsFound: false,
        searchBarContent: ""
      },
      () => {
        // go back to the first page to avoid display bugs
        this.props.updateCurrentTestPermissionsPageState(1);
        // focusing on search bar
        document.getElementById("search-bar").focus();
        this.populateActiveTestPermissions();
      }
    );
  };

  // populate display options
  populateDisplayOptions = () => {
    this.setState({ displayOptionsArray: getDisplayOptions() });
  };

  // handling page changes based on pagination selection
  handlePageChange = id => {
    // "+1" because redux permissionsPaginationPage uses index 1 and react-paginate (id) uses index 0
    const selectedPage = id.selected + 1;
    // update current page redux state
    this.props.updateCurrentTestPermissionsPageState(selectedPage);
    // focusing on the first table's row
    document.getElementById("active-test-accesses-button-row-0").focus();
  };

  // handling open selected test permission view popup
  handleOpenViewPopup = id => {
    let testAccess =
      this.props.currentLanguage === LANGUAGES.english
        ? this.state.activeTestPermissions[id].en_test_name
        : this.state.activeTestPermissions[id].fr_test_name;
    this.setState(
      {
        test_permission_id: this.state.activeTestPermissions[id].id,
        user: `${this.state.activeTestPermissions[id].first_name} ${this.state.activeTestPermissions[id].last_name}`,
        username: this.state.activeTestPermissions[id].username,
        testOrderNumber: this.state.activeTestPermissions[id].test_order_number,
        staffingProcessNumber: this.state.activeTestPermissions[id].staffing_process_number,
        departmentMinistryCode: this.state.activeTestPermissions[id].department_ministry_code,
        billingContact: this.state.activeTestPermissions[id].billing_contact,
        billingContactInfo: this.state.activeTestPermissions[id].billing_contact_info,
        isOrg: this.state.activeTestPermissions[id].is_org,
        isRef: this.state.activeTestPermissions[id].is_ref,
        testAccess: `${testAccess} v${this.state.activeTestPermissions[id].version}`,
        expiryDate: this.state.activeTestPermissions[id].expiry_date
      },
      () => {
        this.setState({ showViewTestPermissionPopup: true });
      }
    );
  };

  // getting initial date year value
  getInitialDateYearValue = expiryDate => {
    let initialDateYearValue = expiryDate.split("-")[0];
    return { label: initialDateYearValue, value: Number(initialDateYearValue) };
  };

  // getting initial date month value
  getInitialDateMonthValue = expiryDate => {
    let initialDateMonthValue = expiryDate.split("-")[1];
    let croppedInitialDateMonthValue = initialDateMonthValue;
    // needs to be defnied
    if (typeof initialDateMonthValue !== "undefined") {
      // if month is between 01 and 09
      if (initialDateMonthValue.charAt(0) === "0") {
        // remove first char to only get the number (03 -> 3)
        croppedInitialDateMonthValue = initialDateMonthValue.substr(1);
      }
    }
    return { label: initialDateMonthValue, value: Number(croppedInitialDateMonthValue) };
  };

  // getting initial date day value
  getInitialDateDayValue = expiryDate => {
    let initialDateDayValue = expiryDate.split("-")[2];
    let croppedInitialDateDayValue = initialDateDayValue;
    // needs to be defnied
    if (typeof initialDateDayValue !== "undefined") {
      // if day is between 01 and 09
      if (initialDateDayValue.charAt(0) === "0") {
        // remove first char to only get the number (03 -> 3)
        croppedInitialDateDayValue = initialDateDayValue.substr(1);
      }
    }
    return { label: initialDateDayValue, value: Number(croppedInitialDateDayValue) };
  };

  // update reasonForModifications content
  updateReasonForModificationsContent = event => {
    const reasonForModifications = event.target.value;
    this.setState({
      reasonForModifications: reasonForModifications
    });
  };

  handleCloseViewPopup = () => {
    this.setState({
      showViewTestPermissionPopup: false,
      isValidReasonForModifications: true,
      reasonForModifications: ""
    });
  };

  handleDeleteTestPermission = () => {
    this.props.deleteTestPermission(this.state.test_permission_id).then(response => {
      if (response.status === 200) {
        // re-populating test permissions table
        this.populateActiveTestPermissionsBasedOnActiveSearch();
        this.setState({ showViewTestPermissionPopup: false });
      } else {
        throw new Error("An error occurred during the delete test permission request process");
      }
    });
  };

  openDeleteConfirmationPopup = () => {
    this.setState({ showDeleteConfirmationPopup: true });
  };

  closeDeleteConfirmationPopup = () => {
    this.setState({ showDeleteConfirmationPopup: false });
  };

  closeSaveConfirmationPopup = () => {
    this.setState({ showSaveConfirmationPopup: false });
  };

  handleSaveTestPermission = () => {
    // reason for modifications validation
    // must contain at least one letter to be valid
    const regexExpression = /^(.*[A-Za-z].*)$/;
    const isValidReasonForModifications = regexExpression.test(this.state.reasonForModifications)
      ? true
      : false;

    // trigger date validation
    this.setState({ triggerExpiryDateValidation: !this.state.triggerExpiryDateValidation }, () => {
      // saving validation results in states
      this.setState(
        {
          isValidReasonForModifications: isValidReasonForModifications
        },
        () => {
          // all fields are valid
          if (this.state.isValidReasonForModifications && this.props.completeDateValidState) {
            // TODO (fnormand): implement save test permission data (including new API)
            this.props
              .updateTestPermission(this.state.test_permission_id, this.state.expiryDate)
              .then(response => {
                if (response.status === 200) {
                  // re-populating test permissions table
                  this.populateActiveTestPermissionsBasedOnActiveSearch();
                  this.setState({
                    showViewTestPermissionPopup: false,
                    showSaveConfirmationPopup: true,
                    reasonForModifications: ""
                  });
                } else {
                  throw new Error(
                    "An error occurred during the delete test permission request process"
                  );
                }
              });
            // there is at least one field invalid, so focus on the highest error field
          } else {
            this.focusOnHighestErrorField();
          }
        }
      );
    });
  };

  // analysing field by field and focusing on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.props.completeDateValidState) {
      this.expiryDateRef.current.focus();
    } else if (!this.state.isValidReasonForModifications) {
      document.getElementById("reason-for-modifications").focus();
    }
  };

  render() {
    const columnsDefinition = [
      {
        label:
          LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.testAdministrator,
        style: { width: "28%", overflowWrap: "anywhere" }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.test,
        style: { ...{ width: "18%", overflowWrap: "anywhere" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.orderNumber,
        style: { ...{ width: "18%", overflowWrap: "anywhere" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.expiryDate,
        style: { ...{ width: "18%", overflowWrap: "anywhere" }, ...COMMON_STYLE.CENTERED_TEXT }
      },
      {
        label: LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.table.action,
        style: { ...{ width: "18%", overflowWrap: "anywhere" }, ...COMMON_STYLE.CENTERED_TEXT }
      }
    ];
    return (
      <div>
        <div style={ActivePermissionsStyles.searchBarAndDisplayContainer}>
          <div
            style={
              this.state.displayResultsFound
                ? ActivePermissionsStyles.searchBarContainerWithResultsFound
                : ActivePermissionsStyles.searchBarContainerWithoutResultsFound
            }
          >
            <label
              htmlFor="search-bar"
              id="search-bar-title"
              style={ActivePermissionsStyles.searchBarLabel}
            >
              {LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.searchBarTitle}
            </label>
            <div style={ActivePermissionsStyles.searchBarContainer}>
              <input
                id="search-bar"
                aria-labelledby="search-bar-title"
                style={ActivePermissionsStyles.searchBarInput}
                type="text"
                value={this.state.searchBarContent}
                onChange={this.updateSearchBarContent}
                onKeyPress={event => {
                  if (event.key === "Enter") {
                    this.handleSearch();
                  }
                }}
              ></input>
              {this.state.displayResultsFound && !this.props.currentlyLoading && (
                <div style={ActivePermissionsStyles.resultsFound}>
                  <label
                    id="active-test-accesses-results-found-label"
                    htmlFor="search-bar"
                    tabIndex={0}
                  >
                    {this.state.activeTestPermissions.length > 1
                      ? LOCALIZE.formatString(
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .multipleResultsFound,
                          this.state.resultsFound
                        )
                      : LOCALIZE.formatString(
                          LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                            .singularResultFound,
                          this.state.resultsFound
                        )}
                  </label>
                  <button
                    className="clear-search"
                    style={ActivePermissionsStyles.clearSearchResults}
                    tabIndex={0}
                    onClick={this.handleClearSearch}
                  >
                    {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.clearSearch}
                  </button>
                </div>
              )}
            </div>
            <div style={ActivePermissionsStyles.searchIconButtonContainer}>
              <CustomButton
                label={
                  <>
                    <FontAwesomeIcon icon={faSearch} />
                    <label style={ActivePermissionsStyles.hiddenText}>
                      {
                        LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                          .searchBarTitle
                      }
                    </label>
                  </>
                }
                action={this.handleSearch}
                customStyle={ActivePermissionsStyles.searchIconButton}
                buttonTheme={THEME.SECONDARY}
                disabled={this.state.currentlyLoading}
              />
            </div>
          </div>
          <div style={ActivePermissionsStyles.displayOptionContainer}>
            <div style={ActivePermissionsStyles.displayOptionLabel}>
              <label id="display-options-label">
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .displayOptionLabel
                }
              </label>
              <label id="display-option-accessibility" style={ActivePermissionsStyles.hiddenText}>
                {
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .displayOptionAccessibility
                }
              </label>
            </div>
            <div style={ActivePermissionsStyles.displayOptionDropdown}>
              <Select
                id="display-options-dropdown"
                name="display-options"
                aria-labelledby="display-options-label display-option-accessibility display-option-current-value-accessibility"
                placeholder=""
                options={this.state.displayOptionsArray}
                onChange={this.getSelectedDisplayOption}
                clearable={false}
                value={this.state.displayOptionSelectedValue}
              ></Select>
              <label
                id="display-option-current-value-accessibility"
                style={ActivePermissionsStyles.hiddenText}
              >{`${LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.displayOptionCurrentValueAccessibility} ${this.state.displayOptionSelectedValue.value}`}</label>
            </div>
          </div>
        </div>
        <div>
          <GenericTable
            columnsDefinition={columnsDefinition}
            rowsDefinition={this.state.rowsDefinition}
            emptyTableMessage={
              LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.noResultsFound
            }
            currentlyLoading={this.state.currentlyLoading}
          />
          <div style={ActivePermissionsStyles.paginationContainer}>
            <ReactPaginate
              pageCount={this.state.numberOfPages}
              containerClassName={"pagination"}
              breakClassName={"break"}
              activeClassName={"active-page"}
              marginPagesDisplayed={3}
              // "-1" because react-paginate uses index 0 and permissionsPaginationPage redux state uses index 1
              forcePage={this.props.testPermissionsPaginationPage - 1}
              onPageChange={page => {
                this.handlePageChange(page);
              }}
              previousLabel={
                <div style={ActivePermissionsStyles.paginationIcon}>
                  <label style={ActivePermissionsStyles.hiddenText}>
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .previousPageButton
                    }
                  </label>
                  <FontAwesomeIcon icon={faCaretLeft} />
                </div>
              }
              nextLabel={
                <div style={ActivePermissionsStyles.paginationIcon}>
                  <label style={ActivePermissionsStyles.hiddenText}>
                    {LOCALIZE.systemAdministrator.permissions.tabs.activePermissions.nextPageButton}
                  </label>
                  <FontAwesomeIcon icon={faCaretRight} />
                </div>
              }
            />
          </div>
        </div>
        <PopupBox
          show={this.state.showViewTestPermissionPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          displayCloseButton={true}
          closeButtonAction={this.handleCloseViewPopup}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.title
          }
          description={
            <div style={styles.popupContainer}>
              <p>
                {LOCALIZE.formatString(
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .viewTestPermissionPopup.description,
                  <span style={styles.boldText}>{this.state.user}</span>
                )}
              </p>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.username}
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="username"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.username}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .testOrderNumberLabel
                    }
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="test-order-number"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.testOrderNumber}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .staffingProcessNumber
                    }
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="staffing-process-number"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.staffingProcessNumber}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .departmentMinistry
                    }
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="department-ministry-code"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.departmentMinistryCode}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .billingContact
                    }
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="billing-contact"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.billingContact}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {
                      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                        .billingContactInfo
                    }
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="billing-contact-info"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.billingContactInfo}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isOrg}
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="is-org"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.isOrg}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.isRef}
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="is-ref"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.isRef}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={styles.labelContainer}>
                  <label style={AssignTestAccessesStyles.label}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testAccess}
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <input
                    id="test-access"
                    type="text"
                    disabled={true}
                    style={AssignTestAccessesStyles.input}
                    value={this.state.testAccess}
                    onChange={() => {}}
                  ></input>
                </div>
              </div>
              <div style={AssignTestAccessesStyles.fieldSeparator}>
                <div style={AssignTestAccessesStyles.expiryDateLabelContainer}>
                  <label id="expiry-date-label" style={AssignTestAccessesStyles.expiryDateLabel}>
                    {LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.expiryDate}
                  </label>
                </div>
                <div style={AssignTestAccessesStyles.inputContainer}>
                  <DatePicker
                    dateDayFieldRef={this.expiryDateRef}
                    dateLabelId={"expiry-date-label"}
                    triggerValidation={this.state.triggerExpiryDateValidation}
                    displayValidIcon={false}
                    customYearOptions={populateCustomFiveYearsFutureDateOptions()}
                    initialDateYearValue={this.getInitialDateYearValue(this.state.expiryDate)}
                    initialDateMonthValue={this.getInitialDateMonthValue(this.state.expiryDate)}
                    initialDateDayValue={this.getInitialDateDayValue(this.state.expiryDate)}
                    futureDateValidation={true}
                  />
                </div>
              </div>
              <div>
                <label
                  id="reason-for-modification-label"
                  style={ActivePermissionsStyles.reasonLabel}
                >
                  {
                    LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                      .viewEditDetailsPopup.reasonForModification
                  }
                </label>

                <textarea
                  id="reason-for-modifications"
                  className={
                    this.state.isValidReasonForModifications ? "valid-field" : "invalid-field"
                  }
                  aria-labelledby="reason-for-modification-label reason-for-modifications-error"
                  aria-required={true}
                  aria-invalid={!this.state.isValidReasonForModifications}
                  style={{
                    ...ActivePermissionsStyles.input,
                    ...ActivePermissionsStyles.reasonInput
                  }}
                  value={this.state.reasonForModifications}
                  onChange={this.updateReasonForModificationsContent}
                  maxLength="300"
                ></textarea>
                {!this.state.isValidReasonForModifications && (
                  <label
                    id="reason-for-modifications-error"
                    htmlFor="reason-for-modifications"
                    style={ActivePermissionsStyles.ReasonErrorMessage}
                  >
                    {
                      LOCALIZE.systemAdministrator.permissions.tabs.activePermissions
                        .viewEditDetailsPopup.reasonForModificationError
                    }
                  </label>
                )}
              </div>
            </div>
          }
          leftButtonType={BUTTON_TYPE.danger}
          leftButtonIcon={faTrashAlt}
          leftButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          leftButtonAction={this.openDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonCustomStyle={{ backgroundColor: "#278400" }}
          rightButtonIcon={faSave}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.saveButtonAccessibility
          }
          rightButtonAction={this.handleSaveTestPermission}
        />
        <PopupBox
          show={this.state.showDeleteConfirmationPopup}
          handleClose={this.closeDeleteConfirmationPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          customPopupStyle={ActivePermissionsStyles.deleteConfirmationPopupWidth}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .deleteConfirmationPopup.title
          }
          description={
            <div>
              <SystemMessage
                messageType={MESSAGE_TYPE.error}
                title={
                  LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                    .deleteConfirmationPopup.systemMessageTitle
                }
                message={
                  <div>
                    <p>
                      {LOCALIZE.formatString(
                        LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                          .deleteConfirmationPopup.systemMessageDescription,
                        <span style={ActivePermissionsStyles.boldText}>
                          {this.state.testAccess} (
                          {
                            LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                              .testOrderNumberLabel
                          }{" "}
                          {this.state.testOrderNumber})
                        </span>,
                        <span style={ActivePermissionsStyles.boldText}>{this.state.user}</span>
                      )}
                    </p>
                  </div>
                }
              />
            </div>
          }
          leftButtonType={BUTTON_TYPE.primary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDeleteConfirmationPopup}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonIcon={faTrashAlt}
          rightButtonLabel={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
              .viewTestPermissionPopup.deleteButtonAccessibility
          }
          rightButtonAction={this.handleDeleteTestPermission}
        />
        <PopupBox
          show={this.state.showSaveConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses.saveConfirmationPopup
              .title
          }
          description={
            <div>
              {LOCALIZE.formatString(
                LOCALIZE.systemAdministrator.testAccesses.tabs.activeTestAccesses
                  .saveConfirmationPopup.description,
                <span style={ActivePermissionsStyles.boldText}>{this.state.user}</span>
              )}
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeSaveConfirmationPopup}
        />
      </div>
    );
  }
}

export { ActiveTestAccesses as unconnectedActiveTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testPermissionsPaginationPageSize: state.userPermissions.testPermissionsPaginationPageSize,
    testPermissionsPaginationPage: state.userPermissions.testPermissionsPaginationPage,
    testPermissionsKeyword: state.userPermissions.testPermissionsKeyword,
    triggerPopulateTestPermissions: state.userPermissions.triggerPopulateTestPermissions,
    testPermissionsActiveSearch: state.userPermissions.testPermissionsActiveSearch,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllActiveTestPermissions,
      getFoundActiveTestPermissions,
      updateCurrentTestPermissionsPageState,
      updateTestPermissionsPageSizeState,
      updateSearchActiveTestPermissionsStates,
      deleteTestPermission,
      updateTestPermission
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ActiveTestAccesses);
