import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import LOCALIZE from "../../../text_resources";

const styles = {
  boxContainer: {
    margin: "auto 24px",
    minHeight: 66,
    border: "1px solid #CECECE",
    backgroundColor: "white",
    padding: "8px"
  },
  boxItem: {
    padding: 0
  },
  expiryDateContainer: {
    margin: "18px 0"
  },
  confirmationMessage: {
    marginBottom: 12,
    fontWeight: "bold"
  }
};

class ConfirmTestAccessAssignmentPopup extends Component {
  static propTypes = {
    users: PropTypes.array.isRequired,
    tests: PropTypes.array.isRequired,
    testOrderNumber: PropTypes.string.isRequired,
    staffingProcessNumber: PropTypes.string.isRequired,
    departmentMinistryCode: PropTypes.string.isRequired
  };

  render() {
    return (
      <div>
        <div>
          <p>
            {
              LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                .saveConfirmationPopup.usersSection
            }
          </p>
          <div style={styles.boxContainer}>
            {this.props.users.map((user, id) => {
              return (
                <div key={id}>
                  <p style={styles.boxItem}>{`- ${user.label} (${user.value})`}</p>
                </div>
              );
            })}
          </div>
        </div>
        <div>
          <p>
            {
              LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                .saveConfirmationPopup.testsSection
            }
          </p>
          <div style={styles.boxContainer}>
            {this.props.tests.map((test, id) => {
              return (
                <div key={id}>
                  <p style={styles.boxItem}>{`- ${test}`}</p>
                </div>
              );
            })}
          </div>
        </div>
        <div>
          <p>
            {
              LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                .saveConfirmationPopup.orderInfoSection.title
            }
          </p>
          <div style={styles.boxContainer}>
            <p
              style={styles.boxItem}
            >{`${LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveConfirmationPopup.orderInfoSection.testOrderNumber} ${this.props.testOrderNumber}`}</p>
            <p
              style={styles.boxItem}
            >{`${LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveConfirmationPopup.orderInfoSection.staffingProcessNumber} ${this.props.staffingProcessNumber}`}</p>
            <p
              style={styles.boxItem}
            >{`${LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveConfirmationPopup.orderInfoSection.departmentMinistry} ${this.props.departmentMinistryCode}`}</p>
          </div>
        </div>
        <div style={styles.expiryDateContainer}>
          <p>{`${LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.saveConfirmationPopup.expiriDateSection} ${this.props.completeDatePicked}`}</p>
        </div>
        <div>
          <p style={styles.confirmationMessage}>
            {
              LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses
                .saveConfirmationPopup.confirmation
            }
          </p>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked
  };
};

export default connect(mapStateToProps, null)(ConfirmTestAccessAssignmentPopup);
