import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Select from "react-select";
import LOCALIZE from "../../../text_resources";
import {
  getUsersBasedOnSpecifiedPermission,
  grantTestPermission,
  updateTriggerPopulateTestPermissionsState,
  getTestDefinitionVersionsCollected
} from "../../../modules/PermissionsRedux";
import getActiveNonPublicTests from "../../../modules/TestRedux";
import DatePicker from "../../../components/commons/DatePicker";
import CollapsingItemContainer from "../../../components/eMIB/CollapsingItemContainer";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSave,
  faUndo,
  faTimes,
  faSearch,
  faEdit,
  faSpinner
} from "@fortawesome/free-solid-svg-icons";
import { resetDatePickedStates } from "../../../modules/DatePickerRedux";
import PopupBox, { BUTTON_TYPE } from "../../commons/PopupBox";
import "../../../css/etta.css";
import ConfirmTestAccessAssignmentPopup from "./ConfirmTestAccessAssignmentPopup";
import populateCustomFiveYearsFutureDateOptions from "../../../helpers/populateCustomDatePickerOptions";
import { styles as ActivePermissionsStyles } from "../permissions/ActivePermissions";
import getTicsData from "../../../modules/TicsRedux";
import StyledTooltip from "../../../components/authentication/StyledTooltip";
import "rc-tooltip/assets/bootstrap_white.css";
import { Row } from "react-bootstrap";
import { makeCheckBoxFieldFromObject } from "../../testBuilder/helpers";
import CustomButton, { THEME } from "../../../components/commons/CustomButton";

export const styles = {
  container: {
    margin: "24px 60px"
  },
  labelContainer: {
    display: "table-cell",
    verticalAlign: "middle"
  },
  label: {
    width: 300,
    overflowWrap: "break-word",
    margin: "0 12px 0 0"
  },
  expiryDateLabelContainer: {
    display: "table-cell",
    verticalAlign: "top"
  },
  expiryDateLabel: {
    width: 300,
    margin: "0 12px 0 0",
    paddingTop: 8
  },
  dropdown: {
    width: "100%",
    display: "table-cell",
    verticalAlign: "middle"
  },
  fieldSeparator: {
    marginTop: 18
  },
  usersFieldSeparator: {
    marginTop: 60
  },
  inputContainer: {
    display: "table-cell",
    width: "100%",
    verticalAlign: "middle"
  },
  input: {
    width: "100%",
    minHeight: 38,
    padding: "3px 6px 3px 6px",
    border: "1px solid #00565e",
    borderRadius: 4
  },
  customSearchBarContainer: {
    padding: 0,
    width: "100%",
    verticalAlign: "middle"
  },
  testOrderNbrInputContainer: {
    width: "100%",
    float: "left"
  },
  searchEditBar: {
    padding: "3px 30% 3px 6px"
  },
  searchEditIconsContainer: {
    position: "absolute",
    right: 0
  },
  customSearchButton: {
    padding: "3px 12px",
    borderRadius: 0,
    borderLeft: "ini"
  },
  customEditButton: {
    padding: "3px 12px",
    borderLeft: "none"
  },
  collapsingItemLabel: {
    verticalAlign: "middle",
    margin: 0,
    padding: "6px 12px 6px 24px"
  },
  collapsingItemContainerBody: {
    marginLeft: 18
  },
  testAccessItemContainer: {
    padding: "0 12px"
  },
  testAccessItemLabel: {
    margin: 0
  },
  checkbox: {
    transform: "scale(1.5)",
    verticalAlign: "middle"
  },
  buttonsContainer: {
    textAlign: "center",
    marginTop: 48
  },
  refreshButton: {
    width: 100,
    marginRight: 60
  },
  saveButton: {
    width: 100
  },
  saveButtonIcon: {
    transform: "scale(1.5)"
  },
  refreshButtonIcon: {
    transform: "scale(1.3)"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  testOrderNumberErrorMsgMargin: {
    marginLeft: 312 // label width + label margin-right
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  }
};

class AssignTestAccesses extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  usersRef = React.createRef();
  expiryDateRef = React.createRef();

  static propTypes = {
    triggerResetDateFieldValuesProps: PropTypes.string,
    // provided by redux
    getUsersBasedOnSpecifiedPermission: PropTypes.func,
    getActiveNonPublicTests: PropTypes.func,
    resetDatePickedStates: PropTypes.func,
    grantTestPermission: PropTypes.func,
    updateTriggerPopulateTestPermissionsState: PropTypes.func,
    getTicsData: PropTypes.func
  };

  state = {
    testOrderNumber: "",
    testOrderNumberSearching: false,
    noTestOrderNumberFound: false,
    isValidTestOrderNumber: true,
    staffingProcessNumber: "",
    staffingProcessNumberFieldDisabled: false,
    isValidStaffingProcessNumber: true,
    departmentMinistryCode: "",
    departmentMinistryCodeFieldDisabled: false,
    isValidDepartmentMinistryCode: true,
    isOrg: "",
    isOrgFieldDisabled: false,
    isValidIsOrg: true,
    isRef: "",
    isRefFieldDisabled: false,
    isValidIsRef: true,
    billingContact: "",
    billingContactFieldDisabled: false,
    isValidBillingContact: true,
    billingContactInfo: "",
    billingContactInfoFieldDisabled: false,
    isValidBillingContactInfo: true,
    usersOptions: [],
    usersSelectedOptions: [],
    isValidUsers: true,
    currentSelectedOptionsAccessibility:
      LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.noUserAccessibility,
    triggerExpiryDateValidation: false,
    testAccessesOptions: [],
    selectedTestAccessesNames: [],
    isValidTestAccesses: true,
    triggerResetDateFieldValues: false,
    showClearFieldsPopup: false,
    showAssignTestPermissionsConfirmationPopup: false,
    showTestAccessesGrantedPopup: false,
    financialDataFieldsVisible: false,
    unableToAccessOrderingServiceError: false,
    testPermissionExistsError: {},
    displayTestPermissionExistsError: false,
    testDefinitionOptions: [],
    testDefinitionPermission: []
  };

  componentDidMount = () => {
    this.populateUsersOptions();
    this.populateTestAccessesOptions();
    this.props.getTestDefinitionVersionsCollected().then(response => {
      if (response.ok) {
        this.setState({ testDefinitionOptions: response.body });
      }
    });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidUpdate = prevProps => {
    // if triggerResetDateFieldValuesProps get updated
    if (
      prevProps.triggerResetDateFieldValuesProps !== this.props.triggerResetDateFieldValuesProps
    ) {
      // if selected tab is assign test access (first tab)
      if (this.props.triggerResetDateFieldValuesProps === "assign-test-access") {
        // trigger reset date field values
        this.setState({ triggerResetDateFieldValues: !this.state.triggerResetDateFieldValues });
      }
    }
  };

  // get selected test order number content
  getTestOrderNumberContent = event => {
    const testOrderNumber = event.target.value;
    this.setState({
      testOrderNumber: testOrderNumber
    });
  };

  // get staffing process number content
  getStaffingProcessNumberContent = event => {
    const staffingProcessNumber = event.target.value;
    this.setState({ staffingProcessNumber: staffingProcessNumber });
  };

  // get department/ministry content
  getDepartmentMinistryCodeContent = event => {
    const departmentMinistryCode = event.target.value;
    this.setState({ departmentMinistryCode: departmentMinistryCode });
  };

  // get is org content
  getIsOrgContent = event => {
    const isOrg = event.target.value;
    this.setState({ isOrg: isOrg });
  };

  // get is ref content
  getIsRefContent = event => {
    const isRef = event.target.value;
    this.setState({ isRef: isRef });
  };

  // get billing contact content
  getBillingContactContent = event => {
    const billingContact = event.target.value;
    this.setState({ billingContact: billingContact });
  };

  // get billing contact info content
  getBillingContactInfoContent = event => {
    const billingContactInfo = event.target.value;
    this.setState({ billingContactInfo: billingContactInfo });
  };

  //add test definition to permission array
  addTestDefinitionPermission = (name, value, dataObj) => {
    let found = false;
    let newPermissions = this.state.testDefinitionPermission.filter(obj => {
      if (obj.id === dataObj.id) {
        found = true;
        return null;
      } else {
        return obj;
      }
    });
    if (!found) {
      newPermissions.push(dataObj);
    }
    let selectedTestAccessesNames = newPermissions.map(obj =>
      LOCALIZE.formatString(
        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.testDescription,
        obj.test_code,
        obj.version
      )
    );
    this.setState({
      testDefinitionPermission: newPermissions,
      selectedTestAccessesNames: selectedTestAccessesNames
    });
  };

  // populate users options (all users that have test administrators permission/role)
  populateUsersOptions = () => {
    let usersOptions = [];
    this.props.getUsersBasedOnSpecifiedPermission("is_test_administrator").then(response => {
      for (let i = 0; i < response.length; i++) {
        usersOptions.push({
          label: `${response[i].last_name}, ${response[i].first_name} - ${response[i].pri_or_military_nbr}`,
          value: `${response[i].user}`
        });
      }
    });
    this.setState({ usersOptions: usersOptions });
  };

  // get selected test order number option
  getSelectedUsersOptions = selectedOption => {
    if (selectedOption === null) {
      selectedOption = [];
    }
    this.setState({
      usersSelectedOptions: selectedOption
    });
    this.getSelectedUsersOptionsAccessibility(selectedOption);
  };

  // getting/formatting current selected users options (for accessibility only)
  getSelectedUsersOptionsAccessibility = options => {
    let currentOptions = "";
    if (options.length > 0) {
      for (let i = 0; i < options.length; i++) {
        currentOptions += `${options[i].label}, `;
      }
    } else {
      currentOptions =
        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.noUserAccessibility;
    }
    this.setState({ currentSelectedOptionsAccessibility: currentOptions });
  };

  // populate test accesses options
  populateTestAccessesOptions = () => {
    this._isMounted = true;
    let testAccessesOptions = [];
    this.props.getActiveNonPublicTests().then(response => {
      if (this._isMounted) {
        for (let i = 0; i < response.length; i++) {
          testAccessesOptions.push({
            id: response[i].id,
            test_code: response[i].test_code,
            en_test_name: response[i].en_name,
            fr_test_name: response[i].fr_name,
            checked: false
          });
        }
        this.setState({ testAccessesOptions: testAccessesOptions });
      }
    });
  };

  // update test accesses checkboxes status
  toggleTestAccessesCheckbox = id => {
    let updatedtestAccessesOptions = Array.from(this.state.testAccessesOptions);
    updatedtestAccessesOptions[id].checked = !updatedtestAccessesOptions[id].checked;
    this.setState({ testAccessesOptions: updatedtestAccessesOptions });
  };

  openClearAllFieldsPopup = () => {
    this.setState({ showClearFieldsPopup: true });
  };

  closeClearAllFieldsPopup = () => {
    this.setState({ showClearFieldsPopup: false });
  };

  openAssignTestPermissionsConfirmationPopup = () => {
    this.setState({ showAssignTestPermissionsConfirmationPopup: true });
  };

  closeAssignTestPermissionsConfirmationPopup = () => {
    this.setState({ showAssignTestPermissionsConfirmationPopup: false });
  };

  openTestAccessesGrantedConfirmationPopup = () => {
    this.setState({ showTestAccessesGrantedPopup: true });
  };

  closeTestAccessesGrantedConfirmationPopup = () => {
    this.setState({ showTestAccessesGrantedPopup: false });
    this.props.updateTriggerPopulateTestPermissionsState(
      !this.props.triggerPopulateTestPermissions
    );
  };

  // handling refresh action
  handleRefreshAction = () => {
    // resetting all checkboxes (test accesses options)
    let updatedtestAccessesOptions = Array.from(this.state.testAccessesOptions);
    for (let i = 0; i < updatedtestAccessesOptions.length; i++) {
      updatedtestAccessesOptions[i].checked = false;
    }

    // resetting all needed states
    this.setState({
      testOrderNumber: "",
      noTestOrderNumberFound: false,
      isValidTestOrderNumber: true,
      staffingProcessNumber: "",
      staffingProcessNumberFieldDisabled: false,
      isValidStaffingProcessNumber: true,
      departmentMinistryCode: "",
      departmentMinistryCodeFieldDisabled: false,
      isValidDepartmentMinistryCode: true,
      isOrg: "",
      isOrgFieldDisabled: false,
      isValidIsOrg: true,
      isRef: "",
      isRefFieldDisabled: false,
      isValidIsRef: true,
      billingContact: "",
      billingContactFieldDisabled: false,
      isValidBillingContact: true,
      billingContactInfo: "",
      billingContactInfoFieldDisabled: false,
      isValidBillingContactInfo: true,
      usersSelectedOptions: "",
      currentSelectedOptionsAccessibility:
        LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses.noUserAccessibility,
      isValidUsers: true,
      testAccessesOptions: updatedtestAccessesOptions,
      selectedTestAccessesNames: [],
      selectedTestAccessesArray: [],
      isValidTestAccesses: true,
      triggerResetDateFieldValues: !this.state.triggerResetDateFieldValues,
      showClearFieldsPopup: false,
      financialDataFieldsVisible: false,
      unableToAccessOrderingServiceError: false,
      testPermissionExistsError: {},
      displayTestPermissionExistsError: false,
      testDefinitionPermission: []
    });

    // resetting DatePicker redux states
    this.props.resetDatePickedStates();
  };

  // handling submit form
  handleSubmitForm = () => {
    // creating data object
    const data = {
      expiryDate: this.props.completeDatePicked,
      testOrderNumber: this.state.testOrderNumber,
      staffingProcessNumber: this.state.staffingProcessNumber,
      departmentMinistryCode: this.state.departmentMinistryCode,
      isOrg: this.state.isOrg,
      isRef: this.state.isRef,
      billingContact: this.state.billingContact,
      billingContactInfo: this.state.billingContactInfo
    };
    // getting selected TA users
    let selectedUsersArray = [];
    for (let i = 0; i < this.state.usersSelectedOptions.length; i++) {
      selectedUsersArray.push(this.state.usersSelectedOptions[i].value);
    }
    // getting checked test accesses options
    let selectedTestAccessesArray = this.state.testDefinitionPermission.map(obj => obj.id);

    // granting all needed test permissions based on selectedUsersArray and selectedTestAccessesArray
    this.props
      .grantTestPermission(selectedUsersArray, selectedTestAccessesArray, data)
      .then(response => {
        // all test permissions have been granted
        if (response.status === 200) {
          // close data confirmation popup
          this.closeAssignTestPermissionsConfirmationPopup();
          //open test accesses granted confirmation popup
          this.openTestAccessesGrantedConfirmationPopup();
          // reset form
          this.handleRefreshAction();
          // at least one of the selected TAs already has this test permission
        } else if (response.status === 409) {
          const testPermissionExistsError = {
            name: `${response.first_name} ${response.last_name}`,
            test_order_number: response.test_order_number,
            en_test_name: response.en_test_name,
            fr_test_name: response.fr_test_name
          };
          this.setState({
            testPermissionExistsError: testPermissionExistsError,
            showAssignTestPermissionsConfirmationPopup: false,
            displayTestPermissionExistsError: true
          });
        } else {
          throw new Error("An error occurred during the grant test permission process");
        }
      });
  };

  // validating form (except expiry_date field)
  validateForm = () => {
    const {
      testOrderNumber,
      staffingProcessNumber,
      departmentMinistryCode,
      isOrg,
      isRef,
      billingContact,
      billingContactInfo,
      usersSelectedOptions
    } = this.state;

    // test order number validation
    const isValidTestOrderNumber =
      testOrderNumber !== "" && testOrderNumber.length <= 12 ? true : false;

    // staffing process number validation (max DB field length = 50)
    const isValidStaffingProcessNumber =
      staffingProcessNumber !== "" && staffingProcessNumber.length <= 50 ? true : false;

    // department/ministry validation (max DB field length = 10)
    const isValidDepartmentMinistryCode =
      departmentMinistryCode !== "" && departmentMinistryCode.length <= 10 ? true : false;

    // is org validation (max DB field length = 16)
    const isValidIsOrg = isOrg !== "" && isOrg.length <= 16 ? true : false;

    // is ref validation (max DB field length = 20)
    const isValidIsRef = isRef !== "" && isRef.length <= 20 ? true : false;

    // billing contact validation (max DB field length = 180)
    const isValidBillingContact =
      billingContact !== "" && billingContact.length <= 180 ? true : false;

    // billing contact info validation (max DB field length = 255)
    const isValidBillingContactInfo =
      billingContactInfo !== "" && billingContactInfo.length <= 255 ? true : false;

    // users validation
    const isValidUsers = usersSelectedOptions.length <= 0 ? false : true;

    // test accesses validation
    let isValidTestAccesses = this.state.selectedTestAccessesNames.length > 0;

    // updating validation states
    this.setState(
      {
        triggerExpiryDateValidation: !this.state.triggerExpiryDateValidation
      },
      () => {
        this.setState(
          {
            isValidTestOrderNumber: isValidTestOrderNumber,
            isValidStaffingProcessNumber: isValidStaffingProcessNumber,
            isValidDepartmentMinistryCode: isValidDepartmentMinistryCode,
            isValidIsOrg: isValidIsOrg,
            isValidIsRef: isValidIsRef,
            isValidBillingContact: isValidBillingContact,
            isValidBillingContactInfo: isValidBillingContactInfo,
            isValidUsers: isValidUsers,
            isValidTestAccesses: isValidTestAccesses
          },
          () => {
            if (
              this.state.isValidTestOrderNumber &&
              this.state.isValidStaffingProcessNumber &&
              this.state.isValidDepartmentMinistryCode &&
              this.state.isValidIsOrg &&
              this.state.isValidIsRef &&
              this.state.isValidBillingContact &&
              this.state.isValidBillingContactInfo &&
              this.state.isValidUsers &&
              this.props.completeDateValidState &&
              this.state.isValidTestAccesses &&
              this.state.financialDataFieldsVisible
            ) {
              // open confirmation popup
              this.openAssignTestPermissionsConfirmationPopup();
            } else {
              this.focusOnHighestErrorField();
            }
          }
        );
      }
    );
  };

  // analyses field by field and focus on the highest error field
  focusOnHighestErrorField = () => {
    if (!this.state.isValidTestOrderNumber) {
      document.getElementById("test-order-number").focus();
    } else if (this.state.financialDataFieldsVisible) {
      if (!this.state.isValidStaffingProcessNumber) {
        document.getElementById("staffing-process-number").focus();
      } else if (!this.state.isValidDepartmentMinistryCode) {
        document.getElementById("department-ministry-code").focus();
      } else if (!this.state.isValidIsOrg) {
        document.getElementById("is-org").focus();
      } else if (!this.state.isValidIsRef) {
        document.getElementById("is-ref").focus();
      } else if (!this.state.isValidBillingContact) {
        document.getElementById("billing-contact").focus();
      } else if (!this.state.isValidBillingContactInfo) {
        document.getElementById("billing-contact-info").focus();
      }
    } else if (!this.state.isValidUsers) {
      this.usersRef.current.focus();
    } else if (!this.props.completeDateValidState) {
      this.expiryDateRef.current.focus();
    } else if (!this.state.isValidTestAccesses) {
      document.getElementById("test-accesses-error").focus();
    }
  };

  handleSearch = () => {
    this.setState(
      {
        noTestOrderNumberFound: false,
        financialDataFieldsVisible: false,
        testOrderNumberSearching: true
      },
      () => {
        this.props
          .getTicsData(this.state.testOrderNumber)
          .then(response => {
            // update all fields valid state to true
            this.setState({
              isValidTestOrderNumber: true,
              isValidStaffingProcessNumber: true,
              isValidDepartmentMinistryCode: true,
              isValidIsOrg: true,
              isValidIsRef: true,
              isValidBillingContact: true,
              isValidBillingContactInfo: true
            });
            // test order number found
            if (response.responseCode === 200) {
              this.setState(
                {
                  financialDataFieldsVisible: true,
                  unableToAccessOrderingServiceError: false,
                  staffingProcessNumber:
                    response.assessmentProcessNumber === null
                      ? ""
                      : response.assessmentProcessNumber,
                  departmentMinistryCode: response.deptId === null ? "" : response.deptId,
                  isOrg:
                    response.financialData === null
                      ? ""
                      : response.financialData.isOrg === null
                      ? ""
                      : response.financialData.isOrg,
                  isRef:
                    response.financialData === null
                      ? ""
                      : response.financialData.isRef === null
                      ? ""
                      : response.financialData.isRef,
                  billingContact:
                    response.financialData === null
                      ? ""
                      : response.financialData.billingContact === null
                      ? ""
                      : response.financialData.billingContact,
                  billingContactInfo:
                    response.financialData === null
                      ? ""
                      : response.financialData.billingContactInfo === null
                      ? ""
                      : response.financialData.billingContactInfo
                },
                () => {
                  // all null fields (if there are some) will be enabled, so users can edit them before submiting the new test accesses
                  this.enablingNullFields();
                  // focusing on financial data section (accessibility purposes)
                  document.getElementById("financial-data-section").focus();
                }
              );
            }
            // test order does not exist in TICS database
            else if (response.status === 204) {
              // show no test order number found error + hide financial data fields + remove unable to access ordering service error (if it is there)
              this.setState({
                noTestOrderNumberFound: true,
                unableToAccessOrderingServiceError: false,
                financialDataFieldsVisible: false
              });
              // focusing on test order number field
              document.getElementById("test-order-number").focus();
              // unable to access ordering service API or token invalid
            } else if (
              response.status === 503 ||
              response.status === 404 ||
              response.status === 401 ||
              response.status === 400
            ) {
              this.setState({
                unableToAccessOrderingServiceError: true,
                financialDataFieldsVisible: false
              });
            } else {
              throw new Error("An error occurred during the search test order number process");
            }
          })
          .then(() => {
            this.setState({ testOrderNumberSearching: false });
          });
      }
    );
  };

  // enabling all fields that are empty
  enablingNullFields = () => {
    let staffingProcessNumberFieldDisabled = true;
    let departmentMinistryCodeFieldDisabled = true;
    let isOrgFieldDisabled = true;
    let isRefFieldDisabled = true;
    let billingContactFieldDisabled = true;
    let billingContactInfoFieldDisabled = true;

    if (this.state.staffingProcessNumber === "") {
      staffingProcessNumberFieldDisabled = false;
    }
    if (this.state.departmentMinistryCode === "") {
      departmentMinistryCodeFieldDisabled = false;
    }
    if (this.state.isOrg === "") {
      isOrgFieldDisabled = false;
    }
    if (this.state.isRef === "") {
      isRefFieldDisabled = false;
    }
    if (this.state.billingContact === "") {
      billingContactFieldDisabled = false;
    }
    if (this.state.billingContactInfo === "") {
      billingContactInfoFieldDisabled = false;
    }

    // updating all financial field states
    this.setState({
      staffingProcessNumberFieldDisabled: staffingProcessNumberFieldDisabled,
      departmentMinistryCodeFieldDisabled: departmentMinistryCodeFieldDisabled,
      isOrgFieldDisabled: isOrgFieldDisabled,
      isRefFieldDisabled: isRefFieldDisabled,
      billingContactFieldDisabled: billingContactFieldDisabled,
      billingContactInfoFieldDisabled: billingContactInfoFieldDisabled
    });
  };

  handleManualEntry = () => {
    /* 
      - displaying and enabling all financial data fields
      - resetting all financial date field values
      - removing all errors related to financial data
    */
    this.setState({
      financialDataFieldsVisible: true,
      staffingProcessNumber: "",
      staffingProcessNumberFieldDisabled: false,
      departmentMinistryCode: "",
      departmentMinistryCodeFieldDisabled: false,
      isOrg: "",
      isOrgFieldDisabled: false,
      isRef: "",
      isRefFieldDisabled: false,
      billingContact: "",
      billingContactFieldDisabled: false,
      billingContactInfo: "",
      billingContactInfoFieldDisabled: false,
      noTestOrderNumberFound: false,
      unableToAccessOrderingServiceError: false
    });
    // focusing on test order number field
    document.getElementById("test-order-number").focus();
  };

  render() {
    const assignTestAccesses = LOCALIZE.systemAdministrator.testAccesses.tabs.assignTestAccesses;
    return (
      <div style={styles.container}>
        <div>
          <div style={styles.labelContainer}>
            <label id="test-order-number-label" htmlFor="test-order-number" style={styles.label}>
              {assignTestAccesses.testOrderNumberLabel}
            </label>
          </div>
          <div
            style={{
              ...ActivePermissionsStyles.searchBarContainer,
              ...styles.customSearchBarContainer
            }}
          >
            <div style={styles.testOrderNbrInputContainer}>
              <input
                id="test-order-number"
                className={
                  this.state.isValidTestOrderNumber &&
                  !this.state.noTestOrderNumberFound &&
                  !this.state.unableToAccessOrderingServiceError
                    ? "valid-field"
                    : "invalid-field"
                }
                aria-invalid={!this.state.isValidTestOrderNumber}
                aria-required={true}
                type="text"
                style={{ ...styles.input, ...styles.searchEditBar }}
                value={this.state.testOrderNumber}
                onChange={this.getTestOrderNumberContent}
                onKeyPress={event => {
                  if (event.key === "Enter" && !this.state.testOrderNumberSearching) {
                    this.handleSearch();
                  }
                }}
              ></input>
            </div>
            <div style={styles.searchEditIconsContainer}>
              <StyledTooltip
                trigger={["hover", "focus"]}
                placement="top"
                overlayClassName={"tooltip"}
                overlay={
                  <div>
                    <p>{assignTestAccesses.searchButton}</p>
                  </div>
                }
              >
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon icon={faSearch} />
                      <label style={styles.hiddenText}>{assignTestAccesses.searchButton}</label>
                    </>
                  }
                  action={this.handleSearch}
                  customStyle={{
                    ...ActivePermissionsStyles.searchIconButton,
                    ...styles.customSearchButton,
                    ...styles.customSearchButtonErrorStyle
                  }}
                  buttonTheme={`${THEME.SECONDARY} ${
                    !this.state.isValidTestOrderNumber ||
                    this.state.noTestOrderNumberFound ||
                    this.state.unableToAccessOrderingServiceError
                      ? "custom-search-button-error"
                      : ""
                  }`}
                  disabled={this.state.testOrderNumberSearching ? true : false}
                />
              </StyledTooltip>
              <StyledTooltip
                trigger={["hover", "focus"]}
                placement="top"
                overlayClassName={"tooltip"}
                overlay={
                  <div>
                    <p>{assignTestAccesses.manuelEntryButton}</p>
                  </div>
                }
              >
                <CustomButton
                  label={
                    <>
                      <FontAwesomeIcon icon={faEdit} />
                      <label style={styles.hiddenText}>
                        {assignTestAccesses.manuelEntryButton}
                      </label>
                    </>
                  }
                  action={this.handleManualEntry}
                  customStyle={{
                    ...ActivePermissionsStyles.searchIconButton,
                    ...styles.customEditButton
                  }}
                  buttonTheme={`${THEME.SECONDARY} ${
                    !this.state.isValidTestOrderNumber ||
                    this.state.noTestOrderNumberFound ||
                    this.state.unableToAccessOrderingServiceError
                      ? "custom-edit-button-error"
                      : ""
                  }`}
                  disabled={this.state.testOrderNumberSearching ? true : false}
                />
              </StyledTooltip>
            </div>
          </div>
          <div>
            {this.state.testOrderNumberSearching && (
              <label className="fa fa-spinner fa-spin" style={ActivePermissionsStyles.loading}>
                <FontAwesomeIcon icon={faSpinner} />
              </label>
            )}
            {this.state.noTestOrderNumberFound && !this.state.testOrderNumberSearching && (
              <label
                id="no-test-order-number-found"
                htmlFor="test-order-number"
                style={styles.errorMessage}
              >
                {assignTestAccesses.noTestOrderNumberFound}
              </label>
            )}
            {!this.state.isValidTestOrderNumber && !this.state.testOrderNumberSearching && (
              <label
                id="test-order-number-error"
                htmlFor="test-order-number"
                style={styles.errorMessage}
              >
                {assignTestAccesses.emptyFieldError}
              </label>
            )}
            {this.state.unableToAccessOrderingServiceError && !this.state.testOrderNumberSearching && (
              <label
                id="unable-to-access-ordering-service-error"
                htmlFor="test-order-number"
                style={{ ...styles.errorMessage, ...styles.testOrderNumberErrorMsgMargin }}
              >
                {assignTestAccesses.unableToAccessOrderingServiceError}
              </label>
            )}
          </div>
        </div>
        {this.state.financialDataFieldsVisible && (
          <div id="financial-data-section">
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label
                  id="staffing-process-number-label"
                  htmlFor="staffing-process-number"
                  style={styles.label}
                >
                  {assignTestAccesses.staffingProcessNumber}
                </label>
              </div>
              <div
                style={styles.inputContainer}
                tabIndex={this.state.staffingProcessNumberFieldDisabled ? 0 : "-1"}
              >
                <input
                  id="staffing-process-number"
                  className={
                    this.state.isValidStaffingProcessNumber ? "valid-field" : "invalid-field"
                  }
                  aria-invalid={!this.state.isValidStaffingProcessNumber}
                  aria-required={true}
                  disabled={this.state.staffingProcessNumberFieldDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.staffingProcessNumber}
                  onChange={this.getStaffingProcessNumberContent}
                ></input>
                {!this.state.isValidStaffingProcessNumber && (
                  <label
                    id="staffing-process-number-error"
                    htmlFor="staffing-process-number"
                    style={styles.errorMessage}
                  >
                    {assignTestAccesses.emptyFieldError}
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label
                  id="department-ministry-code-label"
                  htmlFor="department-ministry-code"
                  style={styles.label}
                >
                  {assignTestAccesses.departmentMinistry}
                </label>
              </div>
              <div
                style={styles.inputContainer}
                tabIndex={this.state.staffingProcessNumberFieldDisabled ? 0 : "-1"}
              >
                <input
                  id="department-ministry-code"
                  className={
                    this.state.isValidDepartmentMinistryCode ? "valid-field" : "invalid-field"
                  }
                  aria-invalid={!this.state.isValidDepartmentMinistryCode}
                  aria-required={true}
                  disabled={this.state.departmentMinistryCodeFieldDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.departmentMinistryCode}
                  onChange={this.getDepartmentMinistryCodeContent}
                ></input>
                {!this.state.isValidDepartmentMinistryCode && (
                  <label
                    id="department-ministry-code-error"
                    htmlFor="department-ministry-code"
                    style={styles.errorMessage}
                  >
                    {assignTestAccesses.emptyFieldError}
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label id="is-org-label" htmlFor="is-org" style={styles.label}>
                  {assignTestAccesses.isOrg}
                </label>
              </div>
              <div
                style={styles.inputContainer}
                tabIndex={this.state.staffingProcessNumberFieldDisabled ? 0 : "-1"}
              >
                <input
                  id="is-org"
                  className={this.state.isValidIsOrg ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidIsOrg}
                  aria-required={true}
                  disabled={this.state.isOrgFieldDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.isOrg}
                  onChange={this.getIsOrgContent}
                ></input>
                {!this.state.isValidIsOrg && (
                  <label id="is-orgr-error" htmlFor="is-org" style={styles.errorMessage}>
                    {assignTestAccesses.emptyFieldError}
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label id="is-ref-label" htmlFor="is-ref" style={styles.label}>
                  {assignTestAccesses.isRef}
                </label>
              </div>
              <div
                style={styles.inputContainer}
                tabIndex={this.state.staffingProcessNumberFieldDisabled ? 0 : "-1"}
              >
                <input
                  id="is-ref"
                  className={this.state.isValidIsRef ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidIsRef}
                  aria-required={true}
                  disabled={this.state.isRefFieldDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.isRef}
                  onChange={this.getIsRefContent}
                ></input>
                {!this.state.isValidIsRef && (
                  <label id="is-ref-error" htmlFor="is-ref" style={styles.errorMessage}>
                    {assignTestAccesses.emptyFieldError}
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label id="billing-contact-label" htmlFor="billing-contact" style={styles.label}>
                  {assignTestAccesses.billingContact}
                </label>
              </div>
              <div
                style={styles.inputContainer}
                tabIndex={this.state.staffingProcessNumberFieldDisabled ? 0 : "-1"}
              >
                <input
                  id="billing-contact"
                  className={this.state.isValidBillingContact ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidBillingContact}
                  aria-required={true}
                  disabled={this.state.billingContactFieldDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.billingContact}
                  onChange={this.getBillingContactContent}
                ></input>
                {!this.state.isValidBillingContact && (
                  <label
                    id="billing-contact-error"
                    htmlFor="billing-contact"
                    style={styles.errorMessage}
                  >
                    {assignTestAccesses.emptyFieldError}
                  </label>
                )}
              </div>
            </div>
            <div style={styles.fieldSeparator}>
              <div style={styles.labelContainer}>
                <label
                  id="billing-contact-info-label"
                  htmlFor="billing-contact-info"
                  style={styles.label}
                >
                  {assignTestAccesses.billingContactInfo}
                </label>
              </div>
              <div
                style={styles.inputContainer}
                tabIndex={this.state.staffingProcessNumberFieldDisabled ? 0 : "-1"}
              >
                <input
                  id="billing-contact-info"
                  className={this.state.isValidBillingContactInfo ? "valid-field" : "invalid-field"}
                  aria-invalid={!this.state.isValidBillingContactInfo}
                  aria-required={true}
                  disabled={this.state.billingContactInfoFieldDisabled}
                  type="text"
                  style={styles.input}
                  value={this.state.billingContactInfo}
                  onChange={this.getBillingContactInfoContent}
                ></input>
                {!this.state.isValidBillingContactInfo && (
                  <label
                    id="billing-contact-info-error"
                    htmlFor="billing-contact-info"
                    style={styles.errorMessage}
                  >
                    {assignTestAccesses.emptyFieldError}
                  </label>
                )}
              </div>
            </div>
          </div>
        )}
        <div style={styles.usersFieldSeparator}>
          <div style={styles.labelContainer}>
            <label id="users-label" style={styles.label}>
              {assignTestAccesses.users}
            </label>
          </div>
          <div style={styles.dropdown}>
            <Select
              ref={this.usersRef}
              id="users-dropdown"
              name="users-options"
              className={this.state.isValidUsers ? "valid-field" : "invalid-field"}
              aria-invalid={!this.state.isValidUsers}
              aria-required={true}
              aria-labelledby="users-label users-current-value-accessibility users-dropdown-error"
              placeholder=""
              options={this.state.usersOptions}
              onChange={this.getSelectedUsersOptions}
              clearable={false}
              value={this.state.usersSelectedOptions}
              isMulti={true}
            ></Select>
            <label
              id="users-current-value-accessibility"
              style={styles.hiddenText}
            >{`${assignTestAccesses.usersCurrentValueAccessibility} ${this.state.currentSelectedOptionsAccessibility}`}</label>
            {!this.state.isValidUsers && (
              <label id="users-dropdown-error" htmlFor="users-dropdown" style={styles.errorMessage}>
                {assignTestAccesses.emptyFieldError}
              </label>
            )}
          </div>
        </div>
        <div style={styles.fieldSeparator}>
          <div style={styles.expiryDateLabelContainer}>
            <label id="expiry-date-label" style={styles.expiryDateLabel}>
              {assignTestAccesses.expiryDate}
            </label>
          </div>
          <div style={styles.inputContainer}>
            <DatePicker
              dateDayFieldRef={this.expiryDateRef}
              dateLabelId={"expiry-date-label"}
              triggerValidation={this.state.triggerExpiryDateValidation}
              triggerResetFieldValues={this.state.triggerResetDateFieldValues}
              displayValidIcon={false}
              customYearOptions={populateCustomFiveYearsFutureDateOptions()}
              futureDateValidation={true}
            />
          </div>
        </div>
        <div>
          <label>{assignTestAccesses.testAccesses}</label>
          <div id="unit-test-test-accesses">
            {this.state.testDefinitionOptions.map((test, key) => {
              return (
                <CollapsingItemContainer
                  key={key}
                  title={<label style={styles.testAccessItemLabel}>{test.test_code}</label>}
                  body={
                    <>
                      {test.details.map((version, key) => {
                        return (
                          <Row style={styles.rowStyle} key={key}>
                            {makeCheckBoxFieldFromObject(
                              {
                                inputName: `${version.id}`,
                                toolTip: LOCALIZE.formatString(
                                  assignTestAccesses.version.title,
                                  test.test_code,
                                  version.version
                                ),
                                property:
                                  this.state.testDefinitionPermission.filter(
                                    obj => obj.id === version.id
                                  ).length > 0,
                                onChange: this.addTestDefinitionPermission,
                                idName: `${test.test_code}-version-${version.version}`,
                                dataObj: version
                              },
                              this.props.accommodations.fontSize
                            )}
                          </Row>
                        );
                      })}
                    </>
                  }
                />
              );
            })}
          </div>
          {!this.state.isValidTestAccesses && (
            <label id="test-accesses-error" tabIndex={0} style={styles.errorMessage}>
              <span style={styles.hiddenText}>{assignTestAccesses.testAccesses}</span>
              <span>{assignTestAccesses.testAccessesError}</span>
            </label>
          )}
          {this.state.displayTestPermissionExistsError && (
            <label id="test-access-already-exists-error" tabIndex={0} style={styles.errorMessage}>
              <span>
                {LOCALIZE.formatString(
                  assignTestAccesses.testAccessAlreadyExistsError,
                  this.state.testPermissionExistsError.name,
                  this.state.testPermissionExistsError[`${this.props.currentLanguage}_test_name`],
                  this.state.testPermissionExistsError.test_order_number
                )}
              </span>
            </label>
          )}
        </div>
        <div style={styles.buttonsContainer}>
          <StyledTooltip
            trigger={["hover", "focus"]}
            placement="top"
            overlayClassName={"tooltip"}
            overlay={
              <div>
                <p>{assignTestAccesses.refreshButton}</p>
              </div>
            }
          >
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faUndo} style={styles.refreshButtonIcon} />
                  <label id="refresh-button" style={styles.hiddenText}>
                    {assignTestAccesses.refreshButton}
                  </label>
                </>
              }
              action={this.openClearAllFieldsPopup}
              customStyle={styles.refreshButton}
              type={"button"}
              buttonTheme={THEME.SECONDARY}
            />
          </StyledTooltip>
          <StyledTooltip
            trigger={["hover", "focus"]}
            placement="top"
            overlayClassName={"tooltip"}
            overlay={
              <div>
                <p>{assignTestAccesses.saveButton}</p>
              </div>
            }
          >
            <CustomButton
              label={
                <>
                  <FontAwesomeIcon icon={faSave} style={styles.saveButtonIcon} />
                  <label id="save-button" style={styles.hiddenText}>
                    {assignTestAccesses.saveButton}
                  </label>
                </>
              }
              action={this.validateForm}
              customStyle={styles.saveButton}
              buttonTheme={THEME.PRIMARY}
              disabled={this.state.financialDataFieldsVisible ? false : true}
            />
          </StyledTooltip>
        </div>
        <PopupBox
          show={this.state.showClearFieldsPopup}
          handleClose={this.closeClearAllFieldsPopup}
          isBackdropStatic={false}
          shouldCloseOnEsc={true}
          title={assignTestAccesses.refreshConfirmationPopup.title}
          description={
            <div>
              <p>{assignTestAccesses.refreshConfirmationPopup.description}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeClearAllFieldsPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faUndo}
          rightButtonIconCustomStyle={{ transform: "scale(1.5)", padding: "1.5px" }}
          rightButtonLabel={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleRefreshAction}
        />
        <PopupBox
          show={this.state.showAssignTestPermissionsConfirmationPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={assignTestAccesses.saveConfirmationPopup.title}
          description={
            <ConfirmTestAccessAssignmentPopup
              users={this.state.usersSelectedOptions}
              tests={this.state.selectedTestAccessesNames}
              testOrderNumber={this.state.testOrderNumber}
              staffingProcessNumber={this.state.staffingProcessNumber}
              departmentMinistryCode={this.state.departmentMinistryCode}
            />
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonIcon={faTimes}
          leftButtonLabel={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeAssignTestPermissionsConfirmationPopup}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonIcon={faSave}
          rightButtonLabel={LOCALIZE.commons.confirm}
          rightButtonAction={this.handleSubmitForm}
        />
        <PopupBox
          show={this.state.showTestAccessesGrantedPopup}
          handleClose={() => {}}
          isBackdropStatic={true}
          shouldCloseOnEsc={false}
          title={
            assignTestAccesses.saveConfirmationPopup.testAccessesGrantedConfirmationPopup.title
          }
          description={
            <div>
              <p>
                {
                  assignTestAccesses.saveConfirmationPopup.testAccessesGrantedConfirmationPopup
                    .description
                }
              </p>
            </div>
          }
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.ok}
          rightButtonAction={this.closeTestAccessesGrantedConfirmationPopup}
        />
      </div>
    );
  }
}

export { AssignTestAccesses as unconnectedAssignTestAccesses };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    completeDatePicked: state.datePicker.completeDatePicked,
    completeDateValidState: state.datePicker.completeDateValidState,
    triggerPopulateTestPermissions: state.userPermissions.triggerPopulateTestPermissions,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getUsersBasedOnSpecifiedPermission,
      getActiveNonPublicTests,
      resetDatePickedStates,
      grantTestPermission,
      updateTriggerPopulateTestPermissionsState,
      getTestDefinitionVersionsCollected,
      getTicsData
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(AssignTestAccesses);
