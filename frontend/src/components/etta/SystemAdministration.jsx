import React, { Component } from "react";
import PropTypes from "prop-types";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import ContentContainer from "../commons/ContentContainer";
import { Helmet } from "react-helmet";
import SideNavigation from "../eMIB/SideNavigation";
import Dashboard from "./Dashboard";
import ActiveTests from "./ActiveTests";
import TestAccessCodes from "./TestAccessCodes";
import Permissions from "./permissions/Permissions";
import TestAccesses from "./test_accesses/TestAccesses";
import IncidentReports from "./IncidentReports";
import Scoring from "./Scoring";
import ReScoring from "./ReScoring";
import Reports from "./Reports";

export const styles = {
  header: {
    marginBottom: 24
  },
  sectionContainerLabelDiv: {
    paddingLeft: 18,
    marginBottom: "-3px"
  },
  sectionContainerLabel: {
    padding: "4px 24px",
    marginBottom: 0,
    position: "relative"
  },
  tabStyleBorder: {
    borderBottom: "5px solid #00565e",
    width: "100%",
    position: "absolute",
    bottom: 0,
    left: 0,
    borderRadius: "4px 4px 0 0"
  },
  sectionContainer: {
    borderStyle: "solid",
    borderWidth: "3px 1px 1px 1px",
    borderColor: "#CECECE",
    minHeight: 400,
    padding: "12px 0",
    marginBottom: 24
  },
  tabContainer: {
    zIndex: 1,
    padding: 0
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto",
    height: "100%",
    padding: 5
  },
  nav: {
    marginTop: 10,
    marginLeft: 10,
    width: 206,
    textAlign: "center"
  },
  body: {
    paddingLeft: 12
  },
  sideNavBodyContent: {
    marginTop: "-12px"
  }
};

class SystemAdministration extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
    isTokenStillValid: PropTypes.func
  };

  state = {
    isLoaded: false
  };

  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        this.setState({
          isLoaded: true
        });
      }
    });
  };

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  componentDidUpdate = prevState => {
    if (prevState.isLoaded !== this.state.isLoaded) {
      // focusing on welcome message after content load
      document.getElementById("user-welcome-message-div").focus();
    }
  };

  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getSystemAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.dashboard,
        body: <Dashboard />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.activeTests,
        body: <ActiveTests />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.testAccessCodes,
        body: <TestAccessCodes />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.permissions,
        body: <Permissions triggerPopulatePendingPermissions={this.state.isLoaded} />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.testAccesses,
        body: <TestAccesses />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.incidentReports,
        body: <IncidentReports />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.scoring,
        body: <Scoring />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.reScoring,
        body: <ReScoring />
      },
      {
        menuString: LOCALIZE.systemAdministrator.sideNavItems.reports,
        body: <Reports />
      }
    ];
  };

  render() {
    const specs = this.getSystemAdministrationSections();
    return (
      <div>
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.systemAdministration}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div
              id="user-welcome-message-div"
              style={styles.header}
              tabIndex={0}
              aria-labelledby="user-welcome-message"
            >
              <h1 id="user-welcome-message" className="green-divider">
                {LOCALIZE.formatString(
                  LOCALIZE.profile.title,
                  this.props.firstName,
                  this.props.lastName
                )}
              </h1>
            </div>
            <div>
              <div style={styles.sectionContainerLabelDiv}>
                <div>
                  <label style={styles.sectionContainerLabel}>
                    {LOCALIZE.systemAdministrator.containerLabel}
                    <span style={styles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>
              <div style={styles.sectionContainer}>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  isMain={true}
                  tabContainerStyle={styles.tabContainer}
                  tabContentStyle={styles.tabContent}
                  navStyle={styles.nav}
                  bodyContentCustomStyle={styles.sideNavBodyContent}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { SystemAdministration as unconnectedSystemAdministration };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    firstName: state.user.firstName,
    lastName: state.user.lastName
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SystemAdministration);
