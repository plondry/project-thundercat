import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeSaveDeleteButtons,
  isNumber,
  makeTextAreaField,
  makeTextBoxField,
  allValid
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { LANGUAGES } from "../commons/Translation";
import { PageSectionType } from "../testFactory/Constants";

class SampleEmailPageSectionForm extends Component {
  state = {
    showDialog: false,
    isENContentValid: true,
    isFrContentValid: true,
    isEmailIDValid: true,
    enPageDefinition: { page_section_type: PageSectionType.SAMPLE_EMAIL },
    frPageDefinition: { page_section_type: PageSectionType.SAMPLE_EMAIL },
    pageDefinition: { email_id: 1 }
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.state.pageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ pageDefinition: newObj });
  };

  modifyEnPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enPageDefinition: newObj });
  };
  modifyFrPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frPageDefinition: newObj });
  };

  emailIDValidation = (name, value) => {
    let orginalValue = value;
    if (isNumber(value)) {
      this.setState({ isEmailIDValid: true });
      this.modifyField(name, parseInt(value));
    } else {
      this.setState({ isEmailIDValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = () => {
    if (allValid(this.state)) {
      let en = {
        ...this.props.enPageDefinition,
        ...this.state.enPageDefinition,
        email_id: this.state.pageDefinition.email_id
      };
      let fr = {
        ...this.props.frPageDefinition,
        ...this.state.frPageDefinition,
        email_id: this.state.pageDefinition.email_id
      };
      this.props.modifyTestDefinitionField(
        PAGE_SECTION_DEFINITIONS,
        en,
        this.props.enPageDefinition.id
      );
      this.props.modifyTestDefinitionField(
        PAGE_SECTION_DEFINITIONS,
        fr,
        this.props.frPageDefinition.id
      );
      this.props.handleSave(PageSectionType.SAMPLE_EMAIL);
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  getLanguageTabs = () => {
    const enPageDefinition = {
      ...this.props.enPageDefinition,
      ...this.state.enPageDefinition
    };
    const frPageDefinition = {
      ...this.props.frPageDefinition,
      ...this.state.frPageDefinition
    };
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("from_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "from_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            enPageDefinition.from_field,
            true,
            this.modifyEnPageDefinition,
            "en-from_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("to_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "to_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            enPageDefinition.to_field,
            true,
            this.modifyEnPageDefinition,
            "en-to_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("subject_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "subject_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            enPageDefinition.subject_field,
            true,
            this.modifyEnPageDefinition,
            "en-subject_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("date_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "date_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            enPageDefinition.date_field,
            true,
            this.modifyEnPageDefinition,
            "en-date_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("body", LOCALIZE.testBuilder.sampleEmailPageSection)}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "body",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            enPageDefinition.body,
            true,
            this.modifyEnPageDefinition,
            "en-body_field"
          )}
        </Row>
      </>
    );
    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("from_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "from_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            frPageDefinition.from_field,
            true,
            this.modifyFrPageDefinition,
            "fr-from_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("to_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "to_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            frPageDefinition.to_field,
            true,
            this.modifyFrPageDefinition,
            "fr-to_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("subject_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "subject_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            frPageDefinition.subject_field,
            true,
            this.modifyFrPageDefinition,
            "fr-subject_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("date_field", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "date_field",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            frPageDefinition.date_field,
            true,
            this.modifyFrPageDefinition,
            "fr-date_field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("body", LOCALIZE.testBuilder.sampleEmailPageSection)}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "body",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            frPageDefinition.body,
            true,
            this.modifyFrPageDefinition,
            "fr-body_field"
          )}
        </Row>
      </>
    );
    const { testLanguage } = this.props;
    let TABS = [];
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.english) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.french) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    let TABS = this.getLanguageTabs();
    let pageDefinition = this.state.pageDefinition;
    return (
      <div id="unit-test-sample-email-page-section">
        <Row style={styles.rowStyle}>
          {makeLabel("email_id", LOCALIZE.testBuilder.sampleEmailPageSection)}
          {makeTextBoxField(
            "email_id",
            LOCALIZE.testBuilder.sampleEmailPageSection,
            pageDefinition.email_id,
            this.state.isEmailIDValid,
            this.emailIDValidation
          )}
        </Row>
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { SampleEmailPageSectionForm as unconnectedSampleEmailPageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SampleEmailPageSectionForm);
