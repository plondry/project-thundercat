import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeSaveDeleteButtons,
  makeTextAreaField,
  allValid,
  testLanguageHasFrench,
  testLanguageHasEnglish
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTION_SITUATIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class QuestionSituationForm extends Component {
  state = {
    showDialog: false,
    enSituation: {},
    frSituation: {}
  };

  modifyFrSituation = (inputName, value) => {
    let newObj = { ...this.props.situationDetails.fr, ...this.state.frSituation };
    newObj[`${inputName}`] = value;
    this.setState({ frSituation: newObj });
  };
  modifyEnSituation = (inputName, value) => {
    let newObj = { ...this.props.situationDetails.en, ...this.state.enSituation };
    newObj[`${inputName}`] = value;
    this.setState({ enSituation: newObj });
  };

  handleSave = () => {
    if (allValid(this.state)) {
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        const enSituation = this.props.situationDetails.en;
        this.props.modifyTestDefinitionField(
          QUESTION_SITUATIONS,
          { ...enSituation, ...this.state.enSituation },
          enSituation.id
        );
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        const frSituation = this.props.situationDetails.fr;
        this.props.modifyTestDefinitionField(
          QUESTION_SITUATIONS,
          { ...frSituation, ...this.state.frSituation },
          frSituation.id
        );
      }
      //call parents handle save
      this.props.handleSave();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  getLanguageTabs = (enSituation, frSituation) => {
    const localize = LOCALIZE.testBuilder.questionSituations;

    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("situation", localize, "question-situation-fr-situation")}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "situation",
            localize,
            frSituation.situation,
            true,
            this.modifyFrSituation,
            "question-situation-fr-situation"
          )}
        </Row>
      </>
    );
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("situation", localize, "question-situation-en-situation")}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "situation",
            localize,
            enSituation.situation,
            true,
            this.modifyEnSituation,
            "question-situation-en-situation"
          )}
        </Row>
      </>
    );
    let TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    const { situationDetails } = this.props;

    if (!situationDetails) {
      return null;
    }
    let enSituation = { ...situationDetails.en, ...this.state.enSituation };
    let frSituation = { ...situationDetails.fr, ...this.state.frSituation };
    let TABS = this.getLanguageTabs(enSituation, frSituation);

    return (
      <div>
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={LOCALIZE.commons.deleteConfirmation}
          description={
            <div>
              <p>{LOCALIZE.testBuilder.emailQuestions.deleteDescription}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { QuestionSituationForm as UnconnectedQuestionSituationForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionSituationForm);
