import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import {
  makeLabel,
  makeTextBoxField,
  makeSaveDeleteButtons,
  makeDropDownField,
  getNextInNumberSeries,
  allValid,
  makeTextAreaField,
  makeCheckBoxField
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  NEXT_SECTION_BUTTONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { NextSectionButtonTypeObject } from "../testFactory/Constants";

const options = Object.getOwnPropertyNames(NextSectionButtonTypeObject).map(property => {
  return NextSectionButtonTypeObject[property];
});

class NextSectionButtonForm extends Component {
  state = {
    showDialog: false,
    isEnTitleValid: true,
    isENContentValid: true,
    isOrderValid: true,
    next_section_button_type: options.filter(
      obj => obj.value === this.props.nextSectionButtonType
    )[0],
    enButtonDetails: {
      language: LANGUAGES.english
    },
    frButtonDetails: {
      language: LANGUAGES.french
    },
    selectedParent: {}
  };

  modifyEnButtonDetails = (inputName, value) => {
    const newObj = { ...this.props.enButtonDetails, ...this.state.enButtonDetails };
    newObj[`${inputName}`] = value;
    this.setState({ enButtonDetails: newObj });
  };

  modifyFrButtonDetails = (inputName, value) => {
    const newObj = { ...this.props.frButtonDetails, ...this.state.frButtonDetails };
    newObj[`${inputName}`] = value;
    this.setState({ frButtonDetails: newObj });
  };

  handleSave = () => {
    if (allValid(this.state)) {
      const enButton = {
        ...this.props.enButtonDetails,
        ...this.state.enButtonDetails,
        next_section_button_type: this.state.next_section_button_type.value
      };
      if (this.props.enButtonDetails.id === undefined) {
        enButton.id = getNextInNumberSeries(this.props.nextSectionButtons, "id");
      }
      this.props.modifyTestDefinitionField(NEXT_SECTION_BUTTONS, enButton, enButton.id);

      const frButton = {
        ...this.props.frButtonDetails,
        ...this.state.frButtonDetails,
        next_section_button_type: this.state.next_section_button_type.value
      };

      if (this.props.frButtonDetails.id === undefined) {
        frButton.id = getNextInNumberSeries(this.props.nextSectionButtons, "id") + 1;
      }
      this.props.modifyTestDefinitionField(NEXT_SECTION_BUTTONS, frButton, frButton.id);

      // call parent save function
      this.props.handleSave(this.state.next_section_button_type.value);
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    // this.props.expandItem();
    this.closeDialog();
  };

  onSelectChange = (event, action) => {
    this.setState({ next_section_button_type: event });
  };

  getLanguageTabs = () => {
    const enButtonDetails = {
      ...this.props.enButtonDetails,
      ...this.state.enButtonDetails
    };
    const frButtonDetails = {
      ...this.props.frButtonDetails,
      ...this.state.frButtonDetails
    };
    const frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            "next-section-button-fr-button-text"
          )}
          {makeTextBoxField(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            frButtonDetails.button_text,
            true,
            this.modifyFrButtonDetails,
            "next-section-button-fr-button-text"
          )}
        </Row>
        {this.state.next_section_button_type === NextSectionButtonTypeObject.POPUP && (
          <>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-fr-popup-title"
              )}
              {makeTextBoxField(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                frButtonDetails.title,
                true,
                this.modifyFrButtonDetails,
                "next-section-button-fr-popup-title"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "confirm_proceed",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-popup-confirm_proceed"
              )}
              {makeCheckBoxField(
                "confirm_proceed",
                LOCALIZE.testBuilder.nextSectionButton,
                frButtonDetails.confirm_proceed,
                this.modifyFrButtonDetails,
                "next-section-button-fr-popup-confirm_proceed"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-fr-content"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeTextAreaField(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                frButtonDetails.content,
                true,
                this.modifyFrButtonDetails,
                "next-section-button-fr-popup-content"
              )}
            </Row>
          </>
        )}
      </>
    );
    const enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            "next-section-button-en-button-text"
          )}
          {makeTextBoxField(
            "button_text",
            LOCALIZE.testBuilder.nextSectionButton,
            enButtonDetails.button_text,
            true,
            this.modifyEnButtonDetails,
            "next-section-button-en-button-text"
          )}
        </Row>
        {this.state.next_section_button_type === NextSectionButtonTypeObject.POPUP && (
          <>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-popup-title"
              )}
              {makeTextBoxField(
                "title",
                LOCALIZE.testBuilder.nextSectionButton,
                enButtonDetails.title,
                true,
                this.modifyEnButtonDetails,
                "next-section-button-en-popup-title"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "confirm_proceed",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-popup-confirm_proceed"
              )}
              {makeCheckBoxField(
                "confirm_proceed",
                LOCALIZE.testBuilder.nextSectionButton,
                enButtonDetails.confirm_proceed,
                this.modifyEnButtonDetails,
                "next-section-button-en-popup-confirm_proceed"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeLabel(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                "next-section-button-en-content"
              )}
            </Row>
            <Row style={styles.rowStyle}>
              {makeTextAreaField(
                "content",
                LOCALIZE.testBuilder.nextSectionButton,
                enButtonDetails.content,
                true,
                this.modifyEnButtonDetails,
                "next-section-button-en-popup-content"
              )}
            </Row>
          </>
        )}
      </>
    );

    const TABS = [
      {
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      },
      {
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      }
    ];

    return TABS;
  };

  render() {
    // let selectedType = selectedOption(this.props.testSectionType);

    const TABS = this.getLanguageTabs();
    return (
      <>
        <h4>{LOCALIZE.testBuilder.nextSectionButton.header}</h4>
        <Row style={styles.rowStyle}>
          {makeLabel("button_type", LOCALIZE.testBuilder.nextSectionButton)}
          {makeDropDownField(
            "button_type",
            LOCALIZE.testBuilder.nextSectionButton,
            this.state.next_section_button_type,
            options,
            this.onSelectChange
          )}
        </Row>

        <TopTabs TABS={TABS} defaultTab={TABS[0].key} activeKey={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title="Delete Confirmation"
          description={<div>{this.props.deletePopupContent}</div>}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </>
    );
  }
}

export { NextSectionButtonForm as unconnectedNextSectionButtonForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    nextSectionButtons: state.testBuilder.next_section_buttons
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(NextSectionButtonForm);
