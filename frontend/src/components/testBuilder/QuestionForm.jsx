import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeDropDownField,
  makeCheckBoxField,
  allValid,
  makeMultiDropDownField,
  isNumberOrEmpty,
  makeTextBoxField
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { QuestionType } from "../testFactory/Constants";
import EmailQuestionForm from "./EmailQuestionForm";
import MultipleChoiceQuestionForm from "./MultipleChoiceQuestionForm";

class QuestionForm extends Component {
  state = {
    showDialog: false,
    isOrderValid: true,
    question: {},
    selectedParent: {}
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.question, ...this.state.question };
    newObj[`${inputName}`] = value;
    this.setState({ question: newObj });
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  onMultiSelectChange = (event, action) => {
    let question = { ...this.props.question, ...this.state.question };
    let newArray = [];
    if (action.action === "remove-value") {
      newArray = [...question[`${action.name}`]].filter(obj => obj !== action.removedValue.value);
    } else if (action.action !== "clear") {
      newArray = [...question[`${action.name}`], action.option.value];
    }

    this.modifyField(action.name, newArray);
  };

  orderValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, parseInt(value));
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      let question = { ...this.props.question, ...this.state.question };
      this.props.modifyTestDefinitionField(QUESTIONS, question, this.props.question.id);
      this.props.expandItem();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.question.id);
    this.props.expandItem();
    this.closeDialog();
  };

  selectedOption = question => {
    for (let property of Object.getOwnPropertyNames(QuestionType)) {
      if (question.question_type === QuestionType[`${property}`]) {
        return { label: property, value: question.question_type };
      }
    }
    return undefined;
  };

  selectedBlock = question => {
    for (let block of this.props.questionBlockTypes) {
      if (question.question_block_type === block.id) {
        return { label: block.name, value: block.id };
      }
    }
    return undefined;
  };

  selectedDependencies = fieldArray => {
    fieldArray = Array.isArray(fieldArray) ? fieldArray : [fieldArray];
    let array = [];
    for (let field of fieldArray) {
      for (let option of this.props.dependencyOptions) {
        if (field === option.id) {
          array.push({ label: option.id, value: option.id });
        }
      }
    }
    return array;
  };

  render() {
    const question = {
      ...this.props.question,
      ...this.state.question
    };
    let questionType = question.question_type;

    let typeOptions = Object.getOwnPropertyNames(QuestionType).map(property => {
      return { label: property, value: QuestionType[property] };
    });

    let blockOptions = this.props.questionBlockTypes.map(block => {
      return { label: block.name, value: block.id };
    });

    let dependencyOptions = this.props.dependencyOptions.map(question => {
      return { label: question.id, value: question.id };
    });

    let selectedOption = this.selectedOption(question);
    let selectedBlock = this.selectedBlock(question);
    let selectedDependencies = this.selectedDependencies(question.dependencies);

    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("question_type", LOCALIZE.testBuilder.questions)}
          {makeDropDownField(
            "question_type",
            LOCALIZE.testBuilder.questions,
            selectedOption,
            typeOptions,
            this.onSelectChange,
            true
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("question_block_type", LOCALIZE.testBuilder.questions)}
          {makeDropDownField(
            "question_block_type",
            LOCALIZE.testBuilder.questions,
            selectedBlock,
            blockOptions,
            this.onSelectChange
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("dependencies", LOCALIZE.testBuilder.questions)}
          {makeMultiDropDownField(
            "dependencies",
            LOCALIZE.testBuilder.questions,
            selectedDependencies,
            dependencyOptions,
            this.onMultiSelectChange
          )}
        </Row>
        {question.dependencies !== null && question.dependencies.length !== 0 && (
          <Row style={styles.rowStyle}>
            {makeLabel("order", LOCALIZE.testBuilder.questions)}
            {makeTextBoxField(
              "order",
              LOCALIZE.testBuilder.questions,
              question.order,
              this.state.isOrderValid,
              this.orderValidation
            )}
          </Row>
        )}
        <Row style={styles.rowStyle}>
          {makeLabel("pilot", LOCALIZE.testBuilder.questions)}
          {makeCheckBoxField(
            "pilot",
            LOCALIZE.testBuilder.questions,
            question.pilot,
            this.modifyField
          )}
        </Row>

        {questionType === QuestionType.EMAIL && (
          <EmailQuestionForm
            question={question}
            questionDetails={this.props.questionDetails}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
        {questionType === QuestionType.MULTIPLE_CHOICE && (
          <MultipleChoiceQuestionForm
            question={question}
            questionDetails={this.props.questionDetails}
            handleSave={this.handleSave}
            handleDelete={this.confirmDelete}
            testLanguage={this.props.testLanguage}
          />
        )}
      </div>
    );
  }
}

export { QuestionForm as unconnectedQuestionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionForm);
