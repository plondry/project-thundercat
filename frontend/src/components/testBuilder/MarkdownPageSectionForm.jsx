import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeSaveDeleteButtons,
  makeTextAreaField,
  allValid,
  makeLabelFromObject,
  testLanguageHasFrench,
  testLanguageHasEnglish
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { PageSectionType } from "../testFactory/Constants";

class MarkdownPageSectionForm extends Component {
  state = {
    showDialog: false,
    enPageDefinition: {},
    frPageDefinition: {}
  };

  modifyEnPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enPageDefinition: newObj });
  };
  modifyFrPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frPageDefinition: newObj });
  };

  enContentValidation = (name, value) => {
    this.modifyEnPageDefinition(name, value);
  };
  frContentValidation = (name, value) => {
    this.modifyFrPageDefinition(name, value);
  };

  handleSave = () => {
    if (allValid(this.state)) {
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        let enPage = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
        this.props.modifyTestDefinitionField(
          PAGE_SECTION_DEFINITIONS,
          enPage,
          this.props.enPageDefinition.id,
          PageSectionType.MARKDOWN
        );
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        let frPage = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
        this.props.modifyTestDefinitionField(
          PAGE_SECTION_DEFINITIONS,
          frPage,
          this.props.frPageDefinition.id,
          PageSectionType.MARKDOWN
        );
      }
      this.props.handleSave(PageSectionType.MARKDOWN);
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  getLanguageTabs = () => {
    const enPageDefinition = {
      ...this.props.enPageDefinition,
      ...this.state.enPageDefinition
    };
    const frPageDefinition = {
      ...this.props.frPageDefinition,
      ...this.state.frPageDefinition
    };
    const markdownLocalize = LOCALIZE.testBuilder.markdownPageSection;
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabelFromObject({
            name: "en-content",
            title: markdownLocalize.content.title,
            toolTip: markdownLocalize.content.titleTooltip
          })}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.markdownPageSection,
            enPageDefinition.content,
            true,
            this.enContentValidation,
            "en-content"
          )}
        </Row>
      </>
    );
    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabelFromObject({
            name: "fr-content",
            title: markdownLocalize.content.title,
            toolTip: markdownLocalize.content.titleTooltip
          })}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.markdownPageSection,
            frPageDefinition.content,
            true,
            this.frContentValidation,
            "fr-content"
          )}
        </Row>
      </>
    );
    let TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    let TABS = this.getLanguageTabs();
    return (
      <div id="unit-test-markdown-page-section">
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { MarkdownPageSectionForm as UnconnectedMarkdownPageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MarkdownPageSectionForm);
