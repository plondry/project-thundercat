import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import LOCALIZE from "../../text_resources";
import {
  makeLabel,
  makeTextBoxField,
  makeDropDownField,
  makeCheckBoxField,
  isNumber,
  isNumberOrEmpty,
  noSpecialCharacters,
  allValid
} from "./helpers";
import { modifyTestDefinitionField, TEST_SECTIONS } from "../../modules/TestBuilderRedux";
import { TestSectionType, TestSectionScoringTypeObject } from "../testFactory/Constants";
import NextSectionButtonForm from "./NextSectionButtonForm";

const scoringOptions = Object.getOwnPropertyNames(TestSectionScoringTypeObject).map(property => {
  return TestSectionScoringTypeObject[property];
});

export const styles = {
  mainContainer: {
    width: "95%",
    margin: "auto"
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px",
    margin: 0
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipMarginTop: {
    marginTop: 1
  },
  tooltipIcon: {
    height: 36,
    width: 40,
    color: "#00565e",
    padding: 6
  },

  rowStyle: {
    margin: "5px"
  },
  buttonRowStyle: {
    margin: "15px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  addButton: {
    margins: "auto",
    marginRight: "5px"
  },
  padding15: {
    paddingTop: 15
  }
};

class TestSectionForm extends Component {
  state = {
    isOrderValid: true,
    isDefaultTabValid: true,
    isFrTitleValid: true,
    isEnTitleValid: true,
    testSection: {},
    showDialog: false,
    isDefaultTimeValid: true,
    scoring: scoringOptions.filter(obj => obj.value === this.props.testSection.scoring_type)[0]
  };

  modifyField = (inputName, value) => {
    const newObj = { ...this.props.testSection, ...this.state.testSection };
    newObj[`${inputName}`] = value;
    this.setState({ testSection: newObj });
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  onScoringChange = (event, action) => {
    this.setState({ scoring: event });
  };

  orderValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, parseInt(value));
    }
  };

  defaultTimeValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isDefaultTimeValid: true });
      this.modifyField(name, value);
    }
  };

  defaultTabValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isDefaultTabValid: true });
      this.modifyField(name, value);
    }
  };

  frTitleValidation = (name, value) => {
    const orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isFrTitleValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isFrTitleValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  enTitleValidation = (name, value) => {
    const orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isEnTitleValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isEnTitleValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  handleSave = buttonType => {
    if (allValid(this.state)) {
      const newTestSection = {
        ...this.props.testSection,
        ...this.state.testSection,
        next_section_button_type: buttonType,
        scoring_type: this.state.scoring.value
      };
      this.props.modifyTestDefinitionField(
        TEST_SECTIONS,
        newTestSection,
        this.props.testSection.id
      );
      this.props.expandItem();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  selectedOption = testSection => {
    for (const property of Object.getOwnPropertyNames(TestSectionType)) {
      if (testSection.section_type === TestSectionType[`${property}`]) {
        return { label: property, value: testSection.section_type };
      }
    }
    return undefined;
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.testSection.id);
    this.props.expandItem();
  };

  render() {
    const testSection = { ...this.props.testSection, ...this.state.testSection };
    const selectedType = this.selectedOption(testSection);

    const options = Object.getOwnPropertyNames(TestSectionType).map(property => {
      return { label: property, value: TestSectionType[property] };
    });

    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("fr_title", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "fr_title",
            LOCALIZE.testBuilder.testSection,
            testSection.fr_title,
            this.state.isFrTitleValid,
            this.frTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("en_title", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "en_title",
            LOCALIZE.testBuilder.testSection,
            testSection.en_title,
            this.state.isEnTitleValid,
            this.enTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("default_time", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "default_time",
            LOCALIZE.testBuilder.testSection,
            testSection.default_time,
            this.state.isDefaultTimeValid,
            this.defaultTimeValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("uses_notepad", LOCALIZE.testBuilder.testSection)}
          {makeCheckBoxField(
            "uses_notepad",
            LOCALIZE.testBuilder.testSection,
            testSection.uses_notepad,
            this.modifyField
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("block_cheating", LOCALIZE.testBuilder.testSection)}
          {makeCheckBoxField(
            "block_cheating",
            LOCALIZE.testBuilder.testSection,
            testSection.block_cheating,
            this.modifyField
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("scoring_type", LOCALIZE.testBuilder.testSection)}
          {makeDropDownField(
            "scoring_type",
            LOCALIZE.testBuilder.testSection,
            this.state.scoring,
            scoringOptions,
            this.onScoringChange
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.testSection)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.testSection,
            testSection.order,
            this.state.isOrderValid,
            this.orderValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("section_type", LOCALIZE.testBuilder.testSection)}
          {makeDropDownField(
            "section_type",
            LOCALIZE.testBuilder.testSection,
            selectedType,
            options,
            this.onSelectChange
          )}
        </Row>
        {testSection.section_type === TestSectionType.TOP_TABS && (
          <Row style={styles.rowStyle}>
            {makeLabel("default_tab", LOCALIZE.testBuilder.testSection)}
            {makeTextBoxField(
              "default_tab",
              LOCALIZE.testBuilder.testSection,
              String(testSection.default_tab),
              this.state.isDefaultTabValid,
              this.defaultTabValidation
            )}
          </Row>
        )}
        <NextSectionButtonForm
          nextSectionButtons={this.props.nextSectionButtons}
          nextSectionButtonType={testSection.next_section_button_type}
          handleSave={this.handleSave}
          handleDelete={this.confirmDelete}
          testSectionType={testSection.section_type}
          isTestSectionValid={this.allValid}
          enButtonDetails={this.props.enButtonDetails}
          frButtonDetails={this.props.frButtonDetails}
          deletePopupContent={LOCALIZE.testBuilder.testSection.deletePopup.content}
        />
      </div>
    );
  }
}

export { TestSectionForm as unconnectedTestSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSection: state.testBuilder.test_sections[ownProps.INDEX]
  };
};

const mapDispatchToProps = dispatch => bindActionCreators({ modifyTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionForm);
