import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeSaveDeleteButtons,
  makeDropDownField,
  noSpecialCharacters,
  allValid,
  makeMultiDropDownField,
  testLanguageHasEnglish,
  testLanguageHasFrench
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  ADDRESS_BOOK,
  CONTACT_DETAILS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class AddressBookContactForm extends Component {
  state = {
    showDialog: false,
    isEnTitleValid: true,
    isFrTitleValid: true,
    isENContentValid: true,
    isOrderValid: true,
    isNameValid: true,
    contact: {},
    enContactDetails: {},
    frContactDetails: {},
    selectedParent: {},
    selectedTestSection: undefined
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.contact, ...this.state.contact };
    newObj[`${inputName}`] = value;
    this.setState({ contact: newObj });
  };

  modifyEnContactDetails = (inputName, value) => {
    let newObj = { ...this.props.enContactDetails, ...this.state.enContactDetails };
    newObj[`${inputName}`] = value;
    this.setState({ enContactDetails: newObj });
  };
  modifyFrContactDetails = (inputName, value) => {
    let newObj = { ...this.props.frContactDetails, ...this.state.frContactDetails };
    newObj[`${inputName}`] = value;
    this.setState({ frContactDetails: newObj });
  };

  nameValidation = (name, value) => {
    let orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isNameValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  enTitleValidation = (name, value) => {
    this.setState({ isEnTitleValid: true });
    this.modifyEnContactDetails(name, value);
  };

  frTitleValidation = (name, value) => {
    this.setState({ isFrTitleValid: true });
    this.modifyFrContactDetails(name, value);
  };

  onSelectChange = (event, action) => {
    if (event.value !== this.props.contact.id) {
      this.modifyField(action.name, event.value);
    }
  };

  handleSave = () => {
    const { testLanguage } = this.props;
    if (allValid(this.state)) {
      let contact = { ...this.props.contact, ...this.state.contact };
      let enDetails = { ...this.props.enContactDetails, ...this.state.enContactDetails };
      let frDetails = { ...this.props.frContactDetails, ...this.state.frContactDetails };
      this.props.modifyTestDefinitionField(ADDRESS_BOOK, contact, this.props.contact.id);
      if (testLanguageHasEnglish(testLanguage)) {
        this.props.modifyTestDefinitionField(
          CONTACT_DETAILS,
          enDetails,
          this.props.enContactDetails.id
        );
      }
      if (testLanguageHasFrench(testLanguage)) {
        this.props.modifyTestDefinitionField(
          CONTACT_DETAILS,
          frDetails,
          this.props.frContactDetails.id
        );
      }
      this.props.expandItem();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.contact.id);
    this.props.expandItem();
    this.closeDialog();
  };

  selectTestSection = (event, action) => {
    let values = event.map(obj => obj.value);
    this.modifyField(action.name, values);
  };

  getParent = value => {
    for (let option of this.props.options) {
      if (option.value === value) {
        return option;
      }
    }
  };

  getTestSections = contact => {
    return this.props.tsOptions.map(item => {
      for (let testSection of contact.test_section) {
        if (testSection === item.value) {
          return item;
        }
      }
      return null;
    });
  };

  getLanguageTabs = () => {
    const enContactDetails = {
      ...this.props.enContactDetails,
      ...this.state.enContactDetails
    };
    const frContactDetails = {
      ...this.props.frContactDetails,
      ...this.state.frContactDetails
    };
    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("title", LOCALIZE.testBuilder.addressBook, "address-book-contact-fr-title")}
          {makeTextBoxField(
            "title",
            LOCALIZE.testBuilder.addressBook,
            frContactDetails.title,
            this.state.isFrTitleValid,
            this.frTitleValidation,
            "address-book-contact-fr-title"
          )}
        </Row>
      </>
    );
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("title", LOCALIZE.testBuilder.addressBook, "address-book-contact-en-title")}
          {makeTextBoxField(
            "title",
            LOCALIZE.testBuilder.addressBook,
            enContactDetails.title,
            this.state.isEnTitleValid,
            this.enTitleValidation,
            "address-book-contact-en-title"
          )}
        </Row>
      </>
    );
    const { testLanguage } = this.props;
    let TABS = [];
    if (testLanguageHasEnglish(testLanguage)) {
      TABS.push({
        key: 0,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(testLanguage)) {
      TABS.push({
        key: 1,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    const contact = {
      ...this.props.contact,
      ...this.state.contact
    };

    let selectedTestSection = this.getTestSections(contact);

    let selectedParent = this.getParent(contact.parent);

    let { options } = this.props;

    let TABS = this.getLanguageTabs();
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle} id="unit-test-name">
          {makeLabel("name", LOCALIZE.testBuilder.addressBook, "address-book-contact-name")}
          {makeTextBoxField(
            "name",
            LOCALIZE.testBuilder.addressBook,
            contact.name,
            this.state.isNameValid,
            this.nameValidation,
            "address-book-contact-name"
          )}
        </Row>
        <Row style={styles.rowStyle} id="unit-test-parent">
          {makeLabel("parent", LOCALIZE.testBuilder.addressBook)}
          {makeDropDownField(
            "parent",
            LOCALIZE.testBuilder.addressBook,
            selectedParent,
            options,
            this.onSelectChange
          )}
        </Row>
        <Row style={styles.rowStyle} id="unit-test-test-section">
          {makeLabel("test_section", LOCALIZE.testBuilder.addressBook)}
          {makeMultiDropDownField(
            "test_section",
            LOCALIZE.testBuilder.addressBook,
            selectedTestSection,
            this.props.tsOptions,
            this.selectTestSection
          )}
        </Row>

        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { AddressBookContactForm as unconnectedAddressBookContactForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    // test lang null to support moving the test lang down to components.
    // TODO fix for possible inbox unilingual tests. Not important for SLE ACC
    testLanguage: null
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddressBookContactForm);
