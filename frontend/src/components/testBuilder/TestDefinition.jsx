import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Button, Row, Col, Container } from "react-bootstrap";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import {
  makeCheckBoxField,
  makeDropDownField,
  makeLabel,
  makeTextBoxField,
  noSpecialCharacters,
  makeSaveDeleteButtons,
  allValid,
  isNumberOrEmpty,
  makeTextAreaField
} from "./helpers";
import {
  modifyTestDefinitionField,
  uploadTest,
  getTestDefinitionLabels,
  uploadTestJSON,
  getTestDefinitionExtract,
  loadTestDefinition,
  activateTestDefinition,
  emptyState,
  TEST_DEFINITION
} from "../../modules/TestBuilderRedux";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  rowStyle: {
    margin: "5px"
  },
  wideRowStyle: {
    width: "72%",
    margin: "auto",
    paddingTop: "15px",
    paddingBottom: "5px"
  },
  widerRowStyle: {
    width: "70%",
    margin: "auto",
    paddingTop: "5px"
  },
  wideButton: { 
    width: "100%",
    margin: "5px" 
  },
  topButtons: { 
    width: "100%", 
    margin: "5px" 
  },
  forceWidePopup: { 
    width: "700px"
  }
};

class TestDefinition extends Component {
  localize = LOCALIZE.testBuilder.testDefinition

  state = {
    isValid: true,
    showDialog: false,
    showUploadDialog: false,
    showUploadJSONDialog: false,
    showActivatePopup: false,
    showDeleteTestDialog: false,
    showEmptyTestDialog: false,
    test_definition: {},
    testDefinitionOptions: [],
    selectedTestDefinition: { label: "--", value: "--" },
    loading: true,
    isVersionValid: true,
    isTestCodeValid: true,
    isFrNameValid: true,
    isEnNameValid: true,
    isJSONcorrect: false,
    JSONUpload: ""
  };

  openDialog = () => {
    this.props.getTestDefinitionLabels().then(response => {
      if (response.ok) {
        this.setState({ testDefinitionOptions: response.body, showDialog: true });
      }
    });
  };

  closeDialog = () => {
    this.setState({ 
      showDialog: false,
      showUploadDialog: false,
      showUploadJSONDialog: false,
      showActivatePopup: false,
      showDeleteTestDialog: false,
      showEmptyTestDialog: false
     });
  };

  openDeleteTestDialog = () => {
    this.setState({ showDialog: true });
  };

  openEmptyTestDialog = () => {
    this.setState({ showEmptyTestDialog: true });
  };

  openUploadDialog = () => {
    this.setState({ showUploadDialog: true });
  };

  confirmUpload = () => {
    this.props.uploadTest(this.props.newTestDefinition);
  };

  openUploadJSONDialog = () => {
    this.setState({ showUploadJSONDialog: true });
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.test_definition, ...this.state.test_definition };
    newObj[`${inputName}`] = value;
    this.setState({ test_definition: newObj });
  };

  versionValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isVersionValid: true });
      this.modifyField(name, value);
    }
  };

  testCodeValidation = (name, value) => {
    let orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isTestCodeValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isTestCodeValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  enNameValidation = (name, value) => {
    let orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isEnNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isEnNameValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  frNameValidation = (name, value) => {
    let orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isFrNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isFrNameValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      let newObj = {
        ...this.props.test_definition,
        ...this.state.test_definition,
        active: false
      };
      this.props.modifyTestDefinitionField(TEST_DEFINITION, newObj, this.props.test_definition.id);
    }
  };

  handleDeativate = () => {
    this.props.activateTestDefinition(this.props.test_definition.id, false).then(response => {
      if(response.ok){
        this.modifyField("active", false);
        this.setState({showActivatePopup: false});
      }
    })
  };

  handleActivate = () => {
    this.props.activateTestDefinition(this.props.test_definition.id, true).then(response => {
      if(response.ok){
        this.modifyField("active", true);
        this.setState({showActivatePopup: false});
      }
    })
  };

  openActivatePopup = () => {
    this.setState({ showActivatePopup: true });
  };

  confirmNewTestDefinition = () => {
    this.props.loadTestDefinition(emptyState);
  };

  onSelectTestDefinition = event => {
    this.setState({ selectedTestDefinition: event });
  };

  downloadTestDefinition = async () => {
    this.props.getTestDefinitionExtract(this.state.selectedTestDefinition.value).then(response => {
      if(response.ok){
        this.downloadFile(response.body);
      }
      // TODO raise an error message
    });
  };

  downloadFile = async (response) => {
    const fileName = `${response.new_test.test_definition[0].test_code}`;
    const json = JSON.stringify(response);
    const blob = new Blob([json],{type:'application/json'});
    const href = await URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = href;
    link.download = fileName + ".json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  uploadJSON = () => {
    this.props.uploadTestJSON(this.state.JSONUpload).then(response => {
      // TODO let user know
    });
  }

  modifyJSON = (_, value) => {
    // simple check if JSON is valid
    try {
      JSON.parse(value);
      this.setState({ JSONUpload: value, isJSONcorrect: true });
    } catch (e) {
      this.setState({ JSONUpload: value, isJSONcorrect: false });
    }
  };

  populateTestDefinition = () => {
    this.props.getTestDefinitionExtract(this.state.selectedTestDefinition.value).then(response => {
      this.props.loadTestDefinition(response.body.new_test);
      this.setState({ loading: false });
      this.closeDialog();
    });
  };

  render() {
    // const { test_definition } = this.props;
    let test_definition = { ...this.props.test_definition, ...this.state.test_definition };
    let testName =
      ": " + this.props.test_definition.test_code + " v" + this.props.test_definition.version;
    const isTestActive = test_definition.active;
    return (
      <div style={styles.mainContainer}>
        <h2>{this.localize.title + testName}</h2>
        <p>{this.localize.description}</p>

        <Container>
          <Row className="justify-content-md-center">
            <Col sm={4}>
              <Button
                variant={"secondary"}
                onClick={this.openDialog}
                style={styles.topButtons}
              >
                {this.localize.selectTestDefinition.button}
              </Button>
            </Col>
            <Col sm={4}>
              <Button
                variant={"secondary"}
                onClick={this.openEmptyTestDialog}
                style={styles.topButtons}
              >
                {this.localize.createTestDefinition.button}
              </Button>
            </Col>
          </Row>
          <div style={styles.borderBox} />
          {this.props.test_definition && (
            <>
              <Row style={styles.rowStyle}>
                {makeLabel("test_code", this.localize)}
                {makeTextBoxField(
                  "test_code",
                  this.localize,
                  test_definition.test_code,
                  this.state.isTestCodeValid,
                  this.testCodeValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("en_name", this.localize)}
                {makeTextBoxField(
                  "en_name",
                  this.localize,
                  test_definition.en_name,
                  this.state.isEnNameValid,
                  this.enNameValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("fr_name", this.localize)}
                {makeTextBoxField(
                  "fr_name",
                  this.localize,
                  test_definition.fr_name,
                  this.state.isFrNameValid,
                  this.frNameValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("version", this.localize)}
                {makeTextBoxField(
                  "version",
                  this.localize,
                  test_definition.version,
                  this.state.isVersionValid,
                  this.versionValidation
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("is_public", this.localize)}
                {makeCheckBoxField(
                  "is_public",
                  this.localize,
                  test_definition.is_public,
                  this.modifyField,
                  "public"
                )}
              </Row>
              <Row style={styles.rowStyle}>
                {makeLabel("active", this.localize)}
                {makeCheckBoxField(
                  "active",
                  this.localize,
                  test_definition.active,
                  this.modifyField,
                  "active",
                  true
                )}
              </Row>

              <Row
                style={styles.wideRowStyle}
              >
                {makeSaveDeleteButtons(
                  this.handleSave,
                  this.openDeleteTestDialog,
                  LOCALIZE.commons.applyButton,
                  LOCALIZE.commons.deleteButton
                )}
              </Row>
              {/* activate test popup */}
              <Row
                style={styles.widerRowStyle}
              >
                <Button
                  onClick={this.openActivatePopup}
                  style={styles.wideButton}
                >
                    {isTestActive
                    ? this.localize.deactivate.button
                    : this.localize.activate.button}
                </Button>
              </Row>
              {/* upload current edited definition popup */}
              <Row
                style={styles.widerRowStyle}
              >
                <Button
                  onClick={this.openUploadDialog}
                  style={styles.wideButton}
                >
                  {this.localize.uploadTestDefinition.button}
                </Button>
              </Row>
              {/* upload as json popup */}
              <Row
                style={styles.widerRowStyle}
              >
                <Button
                  onClick={this.openUploadJSONDialog}
                  style={styles.wideButton}
                >
                  {this.localize.uploadJSONTestDefinition.button}
                </Button>
              </Row>
              {/* download database json */}
              <Row
                style={styles.widerRowStyle}
              >
                <Button
                  onClick={this.downloadTestDefinition}
                  style={styles.wideButton}
                >
                  {this.localize.downloadTestDefinition.button}
                </Button>
              </Row>
            </>
          )}
        </Container>

        <PopupBox
          show={this.state.showDialog}
          overflowVisible={true}
          handleClose={this.closeDialog}
          title={this.localize.selectTestDefinition.header}
          description={
            <div style={styles.forcedWidePopup}>
              <Row style={styles.rowStyle}>
                {makeLabel("selectTestDefinition", this.localize)}
                {makeDropDownField(
                  "selectTestDefinition",
                  this.localize,
                  // { label: test_definition.test_code, value: test_definition.id },
                  this.state.selectedTestDefinition,
                  this.state.testDefinitionOptions,
                  this.onSelectTestDefinition
                )}
              </Row>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.populateTestDefinition}
        />
        <PopupBox
          show={this.state.showUploadDialog}
          handleClose={this.closeDialog}
          title={this.localize.uploadTestDefinition.title}
          description={
            <div>{this.localize.uploadTestDefinition.description}</div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.confirmUpload}
        />
        <PopupBox
          show={this.state.showDeleteTestDialog}
          handleClose={this.closeDialog}
          title={this.localize.deleteTestDefinition.title}
          description={
            <div>{this.localize.deleteTestDefinition.description}</div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDeleteTest}
        />
        <PopupBox
          show={this.state.showEmptyTestDialog}
          handleClose={this.closeDialog}
          title={this.localize.createTestDefinition.title}
          description={this.localize.createTestDefinition.description}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.confirmNewTestDefinition}
        />
        <PopupBox
          show={this.state.showActivatePopup}
          handleClose={this.closeDialog}
          title={
            isTestActive ? this.localize.deactivate.popupTitle : this.localize.activate.popupTitle
          }
          description={
            <div>
              {isTestActive
                ? this.localize.deactivate.popupContent
                : this.localize.activate.popupContent}
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={isTestActive ? this.handleDeativate : this.handleActivate}
        />
        {/* upload as json popup */}
        <PopupBox
          show={this.state.showUploadJSONDialog}
          handleClose={this.closeDialog}
          title={this.localize.uploadJSONTestDefinition.title}
          description={
            <div>
              {this.localize.uploadJSONTestDefinition.description}
              {makeLabel("JSONUpload", this.localize)}
                {makeTextAreaField(
                  "JSONUpload",
                  this.localize,
                  this.state.JSONUpload,
                  this.state.isJSONcorrect,
                  this.modifyJSON
                )}
            </div>
            }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.commons.confirm}
          rightButtonAction={this.uploadJSON}
          rightButtonState={!this.state.isJSONcorrect}
        />
      </div>
    );
  }
}

export { TestDefinition as unconnectedTestDefinition };

const mapStateToProps = (state, ownProps) => {
  //done because only one test definition is returned ever
  let INDEX = 0;
  return {
    test_definition: state.testBuilder.test_definition[INDEX],
    currentLanguage: state.localize.language,
    newTestDefinition: state.testBuilder
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      modifyTestDefinitionField,
      uploadTest,
      getTestDefinitionLabels,
      uploadTestJSON,
      getTestDefinitionExtract,
      loadTestDefinition,
      activateTestDefinition
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestDefinition);
