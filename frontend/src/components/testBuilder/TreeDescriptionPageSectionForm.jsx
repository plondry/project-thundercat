import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import { makeLabel, makeSaveDeleteButtons, allValid, makeDropDownField } from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { LANGUAGES } from "../commons/Translation";
import { PageSectionType } from "../testFactory/Constants";

class TreeDescriptionPageSectionForm extends Component {
  state = {
    showDialog: false,
    enPageDefinition: { page_section_type: PageSectionType.TREE_DESCRIPTION },
    frPageDefinition: { page_section_type: PageSectionType.TREE_DESCRIPTION },
    selectedContact: undefined
  };

  modifyEnPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enPageDefinition: newObj });
  };
  modifyFrPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frPageDefinition: newObj });
  };

  handleSave = () => {
    if (allValid(this.state)) {
      let frPage = {
        ...this.props.frPageDefinition,
        ...this.state.frPageDefinition
      };
      let enPage = {
        ...this.props.enPageDefinition,
        ...this.state.enPageDefinition
      };
      this.props.modifyTestDefinitionField(
        PAGE_SECTION_DEFINITIONS,
        enPage,
        this.props.enPageDefinition.id,
        PageSectionType.TREE_DESCRIPTION
      );
      this.props.modifyTestDefinitionField(
        PAGE_SECTION_DEFINITIONS,
        frPage,
        this.props.frPageDefinition.id,
        PageSectionType.TREE_DESCRIPTION
      );
      this.props.handleSave();
    }
  };

  onSelectChange = (event, action) => {
    this.setState({ selectedContact: event });
    this.modifyEnPageDefinition(action.name, event.value);
    this.modifyFrPageDefinition(action.name, event.value);
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  getParent = value => {
    for (let option of this.props.options) {
      if (option.value === value) {
        return option;
      }
    }
  };

  makeContactOptions = () => {
    return this.props.addressBook.map(contact => {
      return { label: contact.name, value: contact.id };
    });
  };

  getSelectedContact = () => {
    const { testLanguage } = this.props;
    if (this.state.selectedContact !== undefined) {
      return this.state.selectedContact;
    }
    let en = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
    let fr = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
    let idToFind = 0;
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.english) {
      idToFind = en.address_book_contact;
    }
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.french) {
      idToFind = fr.address_book_contact;
    }
    for (let contact of this.props.addressBook) {
      if (contact.id === idToFind) {
        return { label: contact.name, value: contact.id };
      }
    }
  };

  render() {
    let options = this.makeContactOptions();
    let selectedContact = this.getSelectedContact();
    return (
      <div id="unit-test-tree-desc-page-section">
        <Row style={styles.rowStyle}>
          {makeLabel("address_book_contact", LOCALIZE.testBuilder.treeDescriptionPageSection)}
          {makeDropDownField(
            "address_book_contact",
            LOCALIZE.testBuilder.treeDescriptionPageSection,
            selectedContact,
            options,
            this.onSelectChange
          )}
        </Row>

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { TreeDescriptionPageSectionForm as unconnectedTreeDescriptionPageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    addressBook: state.testBuilder.address_book
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TreeDescriptionPageSectionForm);
