import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import { makeDropDownField, makeLabel, getNextInNumberSeries } from "./helpers";
import TestSectionComponentForm from "./TestSectionComponentForm";
import {
  addTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS,
  TEST_SECTION_COMPONENTS,
  SECTION_COMPONENT_PAGES
} from "../../modules/TestBuilderRedux";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  }
};

class TestSectionComponents extends Component {
  state = {
    selectedParentTestSection: undefined
  };

  addTestSectionComponent = () => {
    let newObj = {
      id: 1,
      order: 1,
      component_type: 1,
      en_title: "template",
      fr_title: "template",
      test_section: [this.state.selectedParentTestSection.value]
    };
    newObj.order = getNextInNumberSeries(this.props.testSectionComponents, "order");
    newObj.id = getNextInNumberSeries(this.props.testSectionComponents, "id");
    this.props.addTestDefinitionField(TEST_SECTION_COMPONENTS, newObj, newObj.order);
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  selectParentTestSection = event => {
    this.setState({ selectedParentTestSection: event });
  };

  handleDelete = id => {
    // delete the test section component
    let objArray = this.props.testSectionComponents.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(TEST_SECTION_COMPONENTS, objArray);
    //delete the section component page
    let pageSectionsToDelete = [];
    objArray = this.props.sectionComponentPages.filter(obj => {
      if (obj.test_section_component !== id) {
        return obj;
      } else {
        pageSectionsToDelete.push(obj.id);
        return null;
      }
    });
    this.props.replaceTestDefinitionField(SECTION_COMPONENT_PAGES, objArray);

    // delete the page section
    let pageSectionDefinitionsToDelete = [];
    objArray = this.props.pageSections.filter(obj => {
      if (pageSectionsToDelete.indexOf(obj.section_component_page) === -1) {
        return obj;
      } else {
        pageSectionDefinitionsToDelete.push(obj.id);
        return null;
      }
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);

    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(obj => {
      if (pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1) {
        return obj;
      } else {
        return null;
      }
    });
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);
  };

  render() {
    let { testSectionComponents } = this.props;

    let options = this.makeTestSectionOptions(this.props.testSections, this.props.currentLanguage);
    return (
      <div style={styles.mainContainer}>
        <h2>{LOCALIZE.testBuilder.testSectionComponents.title}</h2>
        <p>{LOCALIZE.testBuilder.testSectionComponents.description}</p>
        <Container>
          <form onChange={this.onChange}>
            <Row style={styles.rowStyle}>
              {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
              {makeDropDownField(
                "parentTestSection",
                LOCALIZE.testBuilder.testSectionComponents,
                this.state.selectedParentTestSection,
                options,
                this.selectParentTestSection
              )}
            </Row>
          </form>
        </Container>
        <Button
          variant={"primary"}
          onClick={this.addTestSectionComponent}
          disabled={this.state.selectedParentTestSection === undefined}
        >
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.testSectionComponents.addButton}
        </Button>
        <div style={styles.borderBox} />

        {this.state.selectedParentTestSection !== undefined &&
          testSectionComponents.map((section, index) => {
            if (section.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
              let questionListRules = this.props.questionListRules.filter(
                obj => obj.test_section_component === section.id
              );
              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  // iconType={}
                  title={
                    <label>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.testSectionComponents.collapsableItemName,
                        section.order,
                        section[`${this.props.currentLanguage}_title`]
                      )}
                    </label>
                  }
                  body={
                    <TestSectionComponentForm
                      testSectionComponent={section}
                      questionListRules={questionListRules}
                      handleDelete={this.handleDelete}
                    />
                  }
                />
              );
            }
            return null;
          })}
      </div>
    );
  }
}

export { TestSectionComponents as unconnectedTestSectionComponents };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    pageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    questionListRules: state.testBuilder.question_list_rules
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ addTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionComponents);
