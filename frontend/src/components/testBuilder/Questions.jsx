import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container, Col } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import {
  makeDropDownField,
  makeLabel,
  getNextInNumberSeries,
  makeMultiDropDownField,
  testSectionComponentLanguage,
  testLanguageHasEnglish,
  testLanguageHasFrench
} from "./helpers";
import {
  replaceTestDefinitionField,
  addTestDefinitionField,
  QUESTIONS,
  EMAIL_DETAILS,
  EMAILS,
  MULTIPLE_CHOICE_QUESTION_DETAILS,
  QUESTION_SECTIONS,
  QUESTION_SECTION_DEFINITIONS,
  ANSWER_DETAILS,
  ANSWERS,
  sortById
} from "../../modules/TestBuilderRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";
import QuestionForm from "./QuestionForm";
import {
  QuestionType,
  QuestionDifficultyType,
  TestSectionComponentType
} from "../testFactory/Constants";
import { cascadeDeleteQuestion } from "../../modules/TestBuilderReduxUtility";
import { styles } from "./helpers";

class Questions extends Component {
  state = {
    selectedParentTestSection: undefined,
    selectedParentTestSectionComponent: undefined,
    selectedBlock: undefined,
    componentType: 0
  };

  addQuestion = () => {
    const { componentType } = this.state;
    const testLanguage = testSectionComponentLanguage(this.state.selectedParentTestSectionComponent.value, this.props.testSectionComponents);

    //add a new inbox question
    if (componentType === TestSectionComponentType.INBOX) {
      let newQuestion = {
        question_type: QuestionType.EMAIL,
        pilot: false,
        test_section_component: this.state.selectedParentTestSectionComponent.value,
        dependencies: []
      };
      newQuestion.id = getNextInNumberSeries(this.props.questions, "id");
      this.props.addTestDefinitionField(QUESTIONS, newQuestion, newQuestion.id);
      //create new email question
      let newEmail = {
        question: newQuestion.id,
        from_field: 0,
        to_field: [],
        cc_field: []
      };
      newEmail.id = getNextInNumberSeries(this.props.emails, "id");
      newEmail.email_id = getNextInNumberSeries(this.props.emails, "email_id");
      this.props.addTestDefinitionField(EMAILS, newEmail, newEmail.email_id);

      //create test language email details stuff
      let newEmailDetails = {};
      if (testLanguageHasEnglish(testLanguage)) {
        newEmailDetails = {
          subject_field: "Temp",
          date_field: "Temp",
          body: "Temp",
          email_question: newEmail.id,
          language: LANGUAGES.english
        };
        newEmailDetails.id = getNextInNumberSeries(this.props.emailDetails, "id");
        this.props.addTestDefinitionField(EMAIL_DETAILS, newEmailDetails, newEmailDetails.id);
      }
      if (testLanguageHasFrench(testLanguage)) {
        newEmailDetails = {
          subject_field: "Temp",
          date_field: "Temp",
          body: "Temp",
          email_question: newEmail.id,
          language: LANGUAGES.french
        };
        newEmailDetails.id = getNextInNumberSeries(this.props.emailDetails, "id") + 1;
        this.props.addTestDefinitionField(EMAIL_DETAILS, newEmailDetails, newEmailDetails.id);
      }
    }
    if (componentType === TestSectionComponentType.QUESTION_LIST) {
      let newQuestion = {
        question_type: QuestionType.MULTIPLE_CHOICE,
        pilot: false,
        test_section_component: this.state.selectedParentTestSectionComponent.value,
        dependencies: []
      };
      newQuestion.id = getNextInNumberSeries(this.props.questions, "id");
      this.props.addTestDefinitionField(QUESTIONS, newQuestion, newQuestion.id);

      let newMCQuestion = {
        question_difficulty_type: QuestionDifficultyType.EASY,
        question: newQuestion.id
      };
      newMCQuestion.id = getNextInNumberSeries(this.props.multipleChoiceQuestions, "id");
      this.props.addTestDefinitionField(
        MULTIPLE_CHOICE_QUESTION_DETAILS,
        newMCQuestion,
        newMCQuestion.id
      );
    }
  };

  onSelectChange = event => {
    this.setState({ selectedBlock: event });
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  makeTestSectionComponentOptions = (array, lang) => {
    let array2 = array
      .filter(item => item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0)
      .map(item => {
        if (item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
          return { label: item[`${lang}_title`], value: item.id };
        }
        return null;
      });
    return array2;
  };

  selectParentTestSection = event => {
    if (event !== this.state.selectedParentTestSection) {
      this.setState({
        selectedParentTestSection: event,
        selectedParentTestSectionComponent: undefined
      });
    }
  };

  selectParentTestSectionComponent = event => {
    let componentType = 0;
    for (let component of this.props.testSectionComponents) {
      if (component.id === event.value) {
        componentType = component.component_type;
      }
    }
    this.setState({ selectedParentTestSectionComponent: event, componentType: componentType });
  };

  makeHeader = questions => {
    let tsOptions = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );
    let tscOptions = [];
    if (this.state.selectedParentTestSection !== undefined) {
      tscOptions = this.makeTestSectionComponentOptions(
        this.props.testSectionComponents,
        this.props.currentLanguage
      );
    }
    let blockOptions = this.props.questionBlockTypes.map(block => {
      return { label: block.name, value: block.id };
    });
    let hideControls =
      this.state.selectedParentTestSectionComponent === undefined ||
      (this.state.componentType !== TestSectionComponentType.INBOX &&
        this.state.componentType !== TestSectionComponentType.QUESTION_LIST);
    return (
      <>
        <h2>{LOCALIZE.testBuilder.questions.topTitle}</h2>
        <p>{LOCALIZE.testBuilder.questions.description}</p>
        <Container>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
            {makeDropDownField(
              "parentTestSection",
              LOCALIZE.testBuilder.testSectionComponents,
              this.state.selectedParentTestSection,
              tsOptions,
              this.selectParentTestSection
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSectionComponent", LOCALIZE.testBuilder.sectionComponentPages)}
            {makeDropDownField(
              "parentTestSectionComponent",
              LOCALIZE.testBuilder.sectionComponentPages,
              this.state.selectedParentTestSectionComponent,
              tscOptions,
              this.selectParentTestSectionComponent,
              this.state.selectedParentTestSection === undefined
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("questionBlockSelection", LOCALIZE.testBuilder.questions)}
            {makeMultiDropDownField(
              "questionBlockSelection",
              LOCALIZE.testBuilder.questions,
              this.state.selectedBlock,
              blockOptions,
              this.onSelectChange,
              this.state.selectedParentTestSection === undefined
            )}
          </Row>
        </Container>
        <Container>
          <Row>
            <Col sm={2}>
              <Button variant={"primary"} onClick={this.addQuestion} disabled={hideControls}>
                <FontAwesomeIcon
                  icon={faPlusCircle}
                  style={{ margins: "auto", marginRight: "5px" }}
                />
                {LOCALIZE.testBuilder.questions.addButton}
              </Button>
            </Col>
            {!hideControls && (
              <Col sm={3} style={styles.centerLabel}>
                {LOCALIZE.formatString(
                  LOCALIZE.testBuilder.questions.searchResults,
                  questions.length
                )}
              </Col>
            )}
          </Row>
        </Container>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleDelete = id => {
    //delete the question
    let objArray = this.props.questions.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(QUESTIONS, objArray);
    if (this.state.componentType === TestSectionComponentType.INBOX) {
      // delete the email questions
      let emailDetailsToDelete = [];
      objArray = this.props.emails.filter(obj => {
        if (obj.question !== id) {
          return obj;
        } else {
          emailDetailsToDelete.push(obj.id);
          return null;
        }
      });

      this.props.replaceTestDefinitionField(EMAILS, objArray);
      // delete the page section definitions
      objArray = this.props.emailDetails.filter(
        obj => emailDetailsToDelete.indexOf(obj.email_question) === -1
      );
      this.props.replaceTestDefinitionField(EMAIL_DETAILS, objArray);
    }
    if (this.state.componentType === TestSectionComponentType.QUESTION_LIST) {
      // delete the mc questions
      let mcToDelete = [];
      objArray = this.props.multipleChoiceQuestions.filter(obj => {
        if (obj.question !== id) {
          return obj;
        } else {
          mcToDelete.push(obj.id);
          return null;
        }
      });
      this.props.replaceTestDefinitionField(MULTIPLE_CHOICE_QUESTION_DETAILS, objArray);

      // delete the question sections
      let sectionsToDelete = [];
      objArray = this.props.questionSections.filter(obj => {
        if (obj.question === id) {
          sectionsToDelete.push(obj.id);
          return null;
        } else {
          return obj;
        }
      });
      this.props.replaceTestDefinitionField(QUESTION_SECTIONS, objArray);

      // delete the question section definitions
      objArray = this.props.questionSectionDefinitions.filter(
        obj => sectionsToDelete.indexOf(obj.question_section) === -1
      );
      this.props.replaceTestDefinitionField(QUESTION_SECTION_DEFINITIONS, objArray);

      //delete the answers
      let answersToDelete = [];
      objArray = this.props.answers.filter(obj => {
        if (obj.question !== id) {
          return obj;
        } else {
          answersToDelete.push(obj.id);
          return null;
        }
      });
      this.props.replaceTestDefinitionField(ANSWERS, objArray);

      //delete the answer definitions
      objArray = this.props.answerDetails.filter(obj => answersToDelete.indexOf(obj.answer) === -1);
      this.props.replaceTestDefinitionField(ANSWER_DETAILS, objArray);
    }
  };

  getQuestionDetails = question => {
    let returnObj = {};

    switch (question.question_type) {
      case QuestionType.EMAIL:
        for (let email of this.props.emails) {
          if (email.question === question.id) {
            returnObj.email = email;
            let fromField = "";
            for (let contact of this.props.addressBook) {
              if (contact.id === email.from_field) {
                fromField = contact.name;
              }
            }
            returnObj.title = LOCALIZE.formatString(
              LOCALIZE.testBuilder.questions.emailCollapsableItemName,
              email.email_id,
              fromField
            );
            for (let obj of this.props.emailDetails) {
              if (obj.email_question === email.id && obj.language === LANGUAGES.english) {
                returnObj.en = obj;
              } else if (obj.email_question === email.id && obj.language === LANGUAGES.french) {
                returnObj.fr = obj;
              }
            }

            for (let obj of this.props.questionSituations) {
              if (obj.question === question.id && obj.language === LANGUAGES.english) {
                returnObj.enSituation = obj;
              } else if (obj.question === question.id && obj.language === LANGUAGES.french) {
                returnObj.frSituation = obj;
              }
            }
            let newId = getNextInNumberSeries(this.props.questionSituations);
            if (!returnObj.enSituation) {
              returnObj.enSituation = {
                id: newId,
                question: question.id,
                language: LANGUAGES.english
              };
              newId += 1;
            }
            if (!returnObj.frSituation) {
              returnObj.frSituation = {
                id: newId,
                question: question.id,
                language: LANGUAGES.french
              };
            }
          }
        }

        break;
      case QuestionType.MULTIPLE_CHOICE:
        let sectionDefs = [];
        //question sections
        returnObj.questionSections = this.props.questionSections.filter(obj => {
          if (obj.question === question.id) {
            sectionDefs.push(obj.id);
            return obj;
          } else {
            return null;
          }
        });
        //question section defs
        returnObj.questionSectionDefinitions = this.props.questionSectionDefinitions.filter(
          obj => sectionDefs.indexOf(obj.question_section) >= 0
        );
        //answers
        let answers = [];
        returnObj.answers = this.props.answers.filter(obj => {
          if (obj.question === question.id) {
            answers.push(obj.id);
            return obj;
          } else {
            return null;
          }
        });
        returnObj.answerDetails = this.props.answerDetails.filter(
          obj => answers.indexOf(obj.answer) >= 0
        );
        let blockType = this.props.questionBlockTypes.filter(
          obj => obj.id === question.question_block_type
        );
        returnObj.title = LOCALIZE.formatString(
          LOCALIZE.testBuilder.questions.collapsableItemName,
          question.id,
          blockType[0] ? blockType[0].name : "None"
        );
        break;
      default:
        break;
    }
    return returnObj;
  };

  render() {
    let { questions } = this.props;
    if (this.state.selectedParentTestSectionComponent) {
      questions = questions.filter(
        obj => obj.test_section_component === this.state.selectedParentTestSectionComponent.value
      );
    }
    if (this.state.selectedBlock) {
      questions = questions.filter(
        obj => this.state.selectedBlock.map(ob => ob.value).indexOf(obj.question_block_type) >= 0
      );
    }
    questions.sort(sortById);
    return (
      <div style={styles.mainContainer}>
        {this.makeHeader(questions)}

        {this.state.selectedParentTestSectionComponent !== undefined &&
          questions.map((question, index) => {
            if (
              question.test_section_component ===
              this.state.selectedParentTestSectionComponent.value
            ) {
              let questionDetails = this.getQuestionDetails(question);
              const testLanguage = testSectionComponentLanguage(question.test_section_component, this.props.testSectionComponents);

              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  title={<label>{questionDetails.title}</label>}
                  body={
                    <QuestionForm
                      question={question}
                      questionDetails={questionDetails}
                      dependencyOptions={questions.filter(obj => obj.id !== question.id)}
                      questionBlockTypes={this.props.questionBlockTypes}
                      handleDelete={this.handleDelete}
                      testLanguage={testLanguage}
                    />
                  }
                />
              );
            }
            return null;
          })}
      </div>
    );
  }
}

export { Questions as unconnectedQuestions };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    questions: state.testBuilder.questions,
    emails: state.testBuilder.emails,
    emailDetails: state.testBuilder.email_details,
    addressBook: state.testBuilder.address_book,
    contactDetails: state.testBuilder.address_book_contacts,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    multipleChoiceQuestions: state.testBuilder.multiple_choice_question_details,
    questionBlockTypes: state.testBuilder.question_block_types,
    questionSituations: state.testBuilder.question_situations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    { replaceTestDefinitionField, cascadeDeleteQuestion, addTestDefinitionField },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Questions);
