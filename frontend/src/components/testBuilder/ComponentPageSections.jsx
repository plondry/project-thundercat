import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import { makeDropDownField, makeLabel, getNextInNumberSeries, languageOptions, testLanguageHasFrench, testLanguageHasEnglish, testSectionComponentLanguage } from "./helpers";
import { PageSectionTypeMap, PageSectionType } from "../testFactory/Constants";
import ComponentPageSectionForm from "./ComponentPageSectionForm";
import {
  addTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { LANGUAGES } from "../commons/Translation";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  }
};

class ComponentPageSections extends Component {
  state = {
    selectedParentTestSection: undefined,
    selectedParentTestSectionComponent: undefined,
    selectedParentSectionComponentPage: undefined,
    selectedDisplayLanguage: languageOptions[0]
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  makeTestSectionComponentOptions = (array, lang) => {
    return array
      .filter(item => item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0)
      .map(item => {
        if (item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
          return { label: item[`${lang}_title`], value: item.id };
        }
        return null;
      });
  };

  makeSectionComponentPageOptions = (array, lang) => {
    return array
      .filter(item => {
        return item.test_section_component === this.state.selectedParentTestSectionComponent.value;
      })
      .map(item => {
        if (item.test_section_component === this.state.selectedParentTestSectionComponent.value) {
          return { label: item[`${lang}_title`], value: item.id };
        }
        return null;
      });
  };

  selectParentTestSection = event => {
    if (event !== this.state.selectedParentTestSection)
      this.setState({
        selectedParentTestSection: event,
        selectedParentTestSectionComponent: undefined,
        selectedParentSectionComponentPage: undefined
      });
  };

  selectParentTestSectionComponent = event => {
    if (event !== this.state.selectedParentTestSectionComponent)
      this.setState({
        selectedParentTestSectionComponent: event,
        selectedParentSectionComponentPage: undefined
      });
  };

  selectParentSectionComponentPage = event => {
    this.setState({ selectedParentSectionComponentPage: event });
  };

  makeHeader = () => {
    let tsOptions = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );
    let tscOptions = [];
    if (this.state.selectedParentTestSection !== undefined) {
      tscOptions = this.makeTestSectionComponentOptions(
        this.props.testSectionComponents,
        this.props.currentLanguage
      );
    }
    let scpOptions = [];
    if (this.state.selectedParentTestSectionComponent !== undefined) {
      scpOptions = this.makeSectionComponentPageOptions(
        this.props.sectionComponentPages,
        this.props.currentLanguage
      );
    }
    return (
      <>
        <h2>{LOCALIZE.testBuilder.componentPageSections.title}</h2>
        <p>{LOCALIZE.testBuilder.componentPageSections.description}</p>
        <Container>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
            {makeDropDownField(
              "parentTestSection",
              LOCALIZE.testBuilder.testSectionComponents,
              this.state.selectedParentTestSection,
              tsOptions,
              this.selectParentTestSection
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSectionComponent", LOCALIZE.testBuilder.sectionComponentPages)}
            {makeDropDownField(
              "parentTestSectionComponent",
              LOCALIZE.testBuilder.sectionComponentPages,
              this.state.selectedParentTestSectionComponent,
              tscOptions,
              this.selectParentTestSectionComponent,
              this.state.selectedParentTestSection === undefined
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("parentSectionComponentPage", LOCALIZE.testBuilder.componentPageSections)}
            {makeDropDownField(
              "parentSectionComponentPage",
              LOCALIZE.testBuilder.componentPageSections,
              this.state.selectedParentSectionComponentPage,
              scpOptions,
              this.selectParentSectionComponentPage,
              this.state.selectedParentTestSectionComponent === undefined
            )}
          </Row>
        </Container>
        <Button
          variant={"primary"}
          onClick={this.handleAdd}
          disabled={this.state.selectedParentSectionComponentPage === undefined}
        >
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.sectionComponentPages.addButton}
        </Button>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleAdd = () => {
    let pageSectionArray = this.props.componentPageSections.filter((section, index) => {
      if (section.section_component_page === this.state.selectedParentSectionComponentPage.value) {
        return section;
      } else {
        return null;
      }
    });
    let newPageSectionObj = {
      id: getNextInNumberSeries(this.props.componentPageSections, "id"),
      order: getNextInNumberSeries(pageSectionArray, "order"),
      page_section_type: PageSectionType.MARKDOWN,
      section_component_page: this.state.selectedParentSectionComponentPage.value
    };
    let newPageSectionDefinitionObjEN = {
      id: getNextInNumberSeries(this.props.pageSectionDefinitions, "id"),
      content: "## Temporary Filler Content",
      page_section: newPageSectionObj.id,
      language: LANGUAGES.english,
      page_section_type: PageSectionType.MARKDOWN
    };
    let newPageSectionDefinitionObjFR = {
      id: newPageSectionDefinitionObjEN.id + 1,
      content: "## Temporary Filler Content",
      page_section: newPageSectionObj.id,
      language: LANGUAGES.french,
      page_section_type: PageSectionType.MARKDOWN
    };
    this.props.addTestDefinitionField(PAGE_SECTIONS, newPageSectionObj);

    // check test language before adding
    const testLanguage = testSectionComponentLanguage(this.state.selectedParentTestSectionComponent.value, this.props.testSectionComponents);
    if(testLanguageHasEnglish(testLanguage)){
      this.props.addTestDefinitionField(PAGE_SECTION_DEFINITIONS, newPageSectionDefinitionObjEN);
    }
    if(testLanguageHasFrench(testLanguage)){
      this.props.addTestDefinitionField(PAGE_SECTION_DEFINITIONS, newPageSectionDefinitionObjFR);
    }
  };

  handleDelete = id => {
    let pageSectionDefinitionsToDelete = [];
    let objArray = this.props.componentPageSections.filter(obj => {
      if (obj.id !== id) {
        return obj;
      } else {
        pageSectionDefinitionsToDelete.push(obj.id);
        return null;
      }
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);
    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(obj => {
      if (pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1) {
        return obj;
      } else {
        return null;
      }
    });
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);
  };

  render() {
    let { pageSectionDefinitions, componentPageSections } = this.props;

    return (
      <div style={styles.mainContainer}>
        {this.makeHeader()}
        {this.state.selectedParentSectionComponentPage !== undefined &&
          componentPageSections.map((section, index) => {
            if (
              section.section_component_page !== this.state.selectedParentSectionComponentPage.value
            ) {
              return null;
            }
            let enPageDefinition = {};
            let frPageDefinition = {};
            for (let definition of pageSectionDefinitions) {
              if (
                definition.page_section === section.id &&
                definition.language === LANGUAGES.french
              ) {
                frPageDefinition = definition;
              }
              if (
                definition.page_section === section.id &&
                definition.language === LANGUAGES.english
              ) {
                enPageDefinition = definition;
              }
            }
            const testLanguage = testSectionComponentLanguage(this.state.selectedParentTestSectionComponent.value, this.props.testSectionComponents);
            return (
              <CollapsingItemContainer
                key={index}
                index={index}
                // iconType={}
                title={
                  <label>
                    {LOCALIZE.formatString(
                      LOCALIZE.testBuilder.componentPageSections.collapsableItemName,
                      section.order,
                      PageSectionTypeMap[section.page_section_type]
                    )}
                  </label>
                }
                body={
                  <ComponentPageSectionForm
                    enPageDefinition={enPageDefinition}
                    frPageDefinition={frPageDefinition}
                    componentPageSection={section}
                    INDEX={index}
                    handleDelete={this.handleDelete}
                    testLanguage={testLanguage}
                  />
                }
              />
            );
          })}
      </div>
    );
  }
}

export { ComponentPageSections as unconnectedComponentPageSections };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    componentPageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addTestDefinitionField,
      replaceTestDefinitionField
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ComponentPageSections);
