import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import { makeLabel, getNextInNumberSeries, makeMultiDropDownField } from "./helpers";
import {
  replaceTestDefinitionField,
  addTestDefinitionField,
  ADDRESS_BOOK,
  CONTACT_DETAILS
} from "../../modules/TestBuilderRedux";
import AddressBookContactForm from "./AddressBookContactForm";
import { LANGUAGES } from "../../modules/LocalizeRedux";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  }
};

class AddressBookContacts extends Component {
  state = {
    contact: [],
    selectedParentTestSection: undefined
  };

  addAddressBookContact = () => {
    const { testLanguage } = this.props;
    let testSections = this.state.selectedParentTestSection.map(obj => obj.value);
    let newContactObj = {
      id: 1,
      name: "Clayton Perroni",
      parent: null,
      test_section: testSections
    };
    newContactObj.id = getNextInNumberSeries(this.props.addressBook, "id");
    this.props.addTestDefinitionField(ADDRESS_BOOK, newContactObj, newContactObj.id);
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.english) {
      let newObj = {
        id: 1,
        title: "Creator of the Test Builder",
        language: LANGUAGES.english,
        contact: newContactObj.id
      };
      newObj.id = getNextInNumberSeries(this.props.contactDetails, "id");
      this.props.addTestDefinitionField(CONTACT_DETAILS, newObj, newObj.id);
    }
    if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.french) {
      let newObj = {
        id: 1,
        title: "FR Creator of the Test Builder",
        language: LANGUAGES.french,
        contact: newContactObj.id
      };
      newObj.id = getNextInNumberSeries(this.props.contactDetails, "id") + 1;
      this.props.addTestDefinitionField(CONTACT_DETAILS, newObj, newObj.id);
    }
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  selectParentTestSection = event => {
    if (event !== this.state.selectedParentTestSection)
      this.setState({
        selectedParentTestSection: event
      });
  };

  makeHeader = tsOptions => {
    return (
      <>
        <h2>{LOCALIZE.testBuilder.addressBook.topTitle}</h2>
        <p>{LOCALIZE.testBuilder.addressBook.description}</p>
        <Container>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
            {makeMultiDropDownField(
              "parentTestSection",
              LOCALIZE.testBuilder.testSectionComponents,
              this.state.selectedParentTestSection,
              tsOptions,
              this.selectParentTestSection
            )}
          </Row>
        </Container>
        <Button
          variant={"primary"}
          onClick={this.addAddressBookContact}
          disabled={this.state.selectedParentTestSection === undefined}
        >
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.addressBook.addButton}
        </Button>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleDelete = id => {
    let objArray = this.props.addressBook.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(ADDRESS_BOOK, objArray);

    objArray = this.props.contactDetails.filter(obj => obj.contact !== id);
    this.props.replaceTestDefinitionField(CONTACT_DETAILS, objArray);
  };

  makeParentOptions = () => {
    return this.props.addressBook.map(contact => {
      return { label: contact.name, value: contact.id };
    });
  };

  render() {
    let { addressBook, contactDetails } = this.props;
    let options = this.makeParentOptions();
    let tsOptions = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );
    return (
      <div style={styles.mainContainer}>
        {this.makeHeader(tsOptions)}

        {this.state.selectedParentTestSection !== undefined &&
          this.state.selectedParentTestSection !== null &&
          addressBook.map((contact, index) => {
            for (let test_section of contact.test_section) {
              let testSectionArray = this.state.selectedParentTestSection.map(obj => obj.value);

              if (testSectionArray !== [] && testSectionArray.indexOf(test_section) >= 0) {
                let enContactDetails = {};
                let frContactDetails = {};
                for (let contactDetail of contactDetails) {
                  if (
                    contactDetail.contact === contact.id &&
                    contactDetail.language === LANGUAGES.french
                  ) {
                    frContactDetails = contactDetail;
                  }
                  if (
                    contactDetail.contact === contact.id &&
                    contactDetail.language === LANGUAGES.english
                  ) {
                    enContactDetails = contactDetail;
                  }
                }
                return (
                  <CollapsingItemContainer
                    key={index}
                    index={index}
                    // iconType={}
                    title={
                      <label>
                        {LOCALIZE.formatString(
                          LOCALIZE.testBuilder.addressBook.collapsableItemName,
                          contact.name
                        )}
                      </label>
                    }
                    body={
                      <AddressBookContactForm
                        enContactDetails={enContactDetails}
                        frContactDetails={frContactDetails}
                        options={options}
                        contact={contact}
                        tsOptions={tsOptions}
                        INDEX={index}
                        handleDelete={this.handleDelete}
                      />
                    }
                  />
                );
              }
            }
            return null;
          })}
      </div>
    );
  }
}

export { AddressBookContacts as unconnectedAddressBookContacts };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    sectionComponentPages: state.testBuilder.section_component_pages,
    addressBook: state.testBuilder.address_book,
    contactDetails: state.testBuilder.contact_details,
    // test lang null to support moving the test lang down to components.
    // TODO fix for possible inbox unilingual tests. Not important for SLE ACC
    testLanguage: null
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ replaceTestDefinitionField, addTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AddressBookContacts);
