import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabelFromObject,
  makeSaveDeleteButtons,
  allValid,
  makeTextBoxField,
  testLanguageHasEnglish,
  testLanguageHasFrench
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import { PageSectionType } from "../testFactory/Constants";

class ZoomImagePageSectionForm extends Component {
  state = {
    showDialog: false,
    enPageDefinition: { page_section_type: PageSectionType.ZOOM_IMAGE },
    frPageDefinition: { page_section_type: PageSectionType.ZOOM_IMAGE }
  };

  modifyEnPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.enPageDefinition, ...this.state.enPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enPageDefinition: newObj });
  };
  modifyFrPageDefinition = (inputName, value) => {
    let newObj = { ...this.props.frPageDefinition, ...this.state.frPageDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frPageDefinition: newObj });
  };

  handleSave = () => {
    if (allValid(this.state)) {
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        let en = {
          ...this.props.enPageDefinition,
          ...this.state.enPageDefinition
        };
        en.large_image = en.small_image;
        this.props.modifyTestDefinitionField(
          PAGE_SECTION_DEFINITIONS,
          en,
          this.props.enPageDefinition.id,
          PageSectionType.ZOOM_IMAGE
        );
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        let fr = {
          ...this.props.frPageDefinition,
          ...this.state.frPageDefinition
        };

        fr.large_image = fr.small_image;

        this.props.modifyTestDefinitionField(
          PAGE_SECTION_DEFINITIONS,
          fr,
          this.props.frPageDefinition.id,
          PageSectionType.ZOOM_IMAGE
        );
      }
      this.props.handleSave();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  getLanguageTabs = () => {
    const enPageDefinition = {
      ...this.props.enPageDefinition,
      ...this.state.enPageDefinition
    };
    const frPageDefinition = {
      ...this.props.frPageDefinition,
      ...this.state.frPageDefinition
    };
    const zoomImageLocalize = LOCALIZE.testBuilder.zoomImagePageSection;
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabelFromObject({
            name: "en-small_image",
            title: zoomImageLocalize.small_image.title,
            toolTip: zoomImageLocalize.small_image.titleTooltip
          })}
          {makeTextBoxField(
            "small_image",
            LOCALIZE.testBuilder.zoomImagePageSection,
            enPageDefinition.small_image,
            true,
            this.modifyEnPageDefinition,
            "en-small_image"
          )}
        </Row>
      </>
    );
    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabelFromObject({
            name: "fr-small_image",
            title: zoomImageLocalize.small_image.title,
            toolTip: zoomImageLocalize.small_image.titleTooltip
          })}
          {makeTextBoxField(
            "small_image",
            LOCALIZE.testBuilder.zoomImagePageSection,
            frPageDefinition.small_image,
            true,
            this.modifyFrPageDefinition,
            "fr-small_image"
          )}
        </Row>
      </>
    );
    let TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    let TABS = this.getLanguageTabs();
    return (
      <div id="unit-test-zoom-image-page-section">
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { ZoomImagePageSectionForm as UnconnectedZoomImagePageSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ZoomImagePageSectionForm);
