import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeSaveDeleteButtons,
  makeTextAreaField,
  testLanguageHasEnglish,
  testLanguageHasFrench
} from "./helpers";
import {
  modifyTestDefinitionField,
  QUESTION_SECTION_DEFINITIONS,
  replaceTestDefinitionField
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class EndQuestionSectionForm extends Component {
  state = {
    showDialog: false,
    enDefinition: {},
    frDefinition: {}
  };

  modifyEnDefinition = (inputName, value) => {
    let newObj = { ...this.props.enDefinition, ...this.state.enDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enDefinition: newObj });
  };
  modifyFrDefinition = (inputName, value) => {
    let newObj = { ...this.props.frDefinition, ...this.state.frDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frDefinition: newObj });
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  handleSave = () => {
    const questionSection = this.props.questionSection;
    const enDefinition = {
      ...this.props.enDefinition,
      ...this.state.enDefinition,
      question_section_type: questionSection.question_section_type
    };
    const frDefinition = {
      ...this.props.frDefinition,
      ...this.state.frDefinition,
      question_section_type: questionSection.question_section_type
    };

    if (testLanguageHasEnglish(this.props.testLanguage)) {
      this.props.modifyTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        enDefinition,
        enDefinition.id
      );
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      this.props.modifyTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        frDefinition,
        frDefinition.id
      );
    }
    this.props.handleSave(questionSection.question_section_type);
  };

  getMarkdownSectionTabs = (enDefinition, frDefinition) => {
    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "content",
            LOCALIZE.testBuilder.questionSectionEnd,
            "question-section-fr-content-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.questionSectionEnd,
            frDefinition.content,
            true,
            this.modifyFrDefinition,
            "question-section-fr-content-field"
          )}
        </Row>
      </>
    );
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "content",
            LOCALIZE.testBuilder.questionSectionEnd,
            "question-section-en-content-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.questionSectionEnd,
            enDefinition.content,
            true,
            this.modifyEnDefinition,
            "question-section-en-content-field"
          )}
        </Row>
      </>
    );

    let TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }
    return TABS;
  };

  render() {
    let enDefinition = { ...this.props.enDefinition, ...this.state.enDefinition };
    let frDefinition = { ...this.props.frDefinition, ...this.state.frDefinition };

    let TABS = this.getMarkdownSectionTabs(enDefinition, frDefinition);

    return (
      <div style={styles.mainContiner}>
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={LOCALIZE.testBuilder.questionSectionEnd.delete.title}
          description={<div>{LOCALIZE.testBuilder.questionSectionEnd.delete.content}</div>}
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { EndQuestionSectionForm as unconnectedEndQuestionSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EndQuestionSectionForm);
