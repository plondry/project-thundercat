import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button, Row, Container } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import { makeDropDownField, makeLabel, getNextInNumberSeries } from "./helpers";
import SectionComponentPageForm from "./SectionComponentPageForm";
import {
  SECTION_COMPONENT_PAGES,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS,
  replaceTestDefinitionField,
  addTestDefinitionField
} from "../../modules/TestBuilderRedux";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  }
};

class SectionComponentPages extends Component {
  state = {
    testSections: [
      {
        order: 1,
        en_title: "temp",
        fr_title: "FR temp",
        test_section_component: undefined
      }
    ],
    selectedParentTestSection: undefined,
    selectedParentTestSectionComponent: undefined
  };

  addTestSectionComponent = () => {
    let newObj = {
      order: 1,
      en_title: "",
      fr_title: "",
      test_section_component: this.state.selectedParentTestSectionComponent.value
    };
    newObj.order = getNextInNumberSeries(this.props.sectionComponentPages, "order");
    newObj.id = getNextInNumberSeries(this.props.sectionComponentPages, "id");
    this.props.addTestDefinitionField(SECTION_COMPONENT_PAGES, newObj, newObj.order);
  };

  makeTestSectionOptions = (array, lang) => {
    return array.map(item => {
      return { label: item[`${lang}_title`], value: item.id };
    });
  };

  makeTestSectionComponentOptions = (array, lang) => {
    return array
      .filter(item => item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0)
      .map(item => {
        if (item.test_section.indexOf(this.state.selectedParentTestSection.value) >= 0) {
          return { label: item[`${lang}_title`], value: item.id };
        } else {
          return null;
        }
      });
  };

  selectParentTestSection = event => {
    if (event !== this.state.selectedParentTestSection)
      this.setState({
        selectedParentTestSection: event,
        selectedParentTestSectionComponent: undefined
      });
  };

  selectParentTestSectionComponent = event => {
    this.setState({ selectedParentTestSectionComponent: event });
  };

  makeHeader = () => {
    let tsOptions = this.makeTestSectionOptions(
      this.props.testSections,
      this.props.currentLanguage
    );
    let tscOptions = [];
    if (this.state.selectedParentTestSection !== undefined) {
      tscOptions = this.makeTestSectionComponentOptions(
        this.props.testSectionComponents,
        this.props.currentLanguage
      );
    }
    return (
      <>
        <h2>{LOCALIZE.testBuilder.sectionComponentPages.title}</h2>
        <p>{LOCALIZE.testBuilder.sectionComponentPages.description}</p>
        <Container>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSection", LOCALIZE.testBuilder.testSectionComponents)}
            {makeDropDownField(
              "parentTestSection",
              LOCALIZE.testBuilder.testSectionComponents,
              this.state.selectedParentTestSection,
              tsOptions,
              this.selectParentTestSection
            )}
          </Row>
          <Row style={styles.rowStyle}>
            {makeLabel("parentTestSectionComponent", LOCALIZE.testBuilder.sectionComponentPages)}
            {makeDropDownField(
              "parentTestSectionComponent",
              LOCALIZE.testBuilder.sectionComponentPages,
              this.state.selectedParentTestSectionComponent,
              tscOptions,
              this.selectParentTestSectionComponent,
              this.state.selectedParentTestSection === undefined
            )}
          </Row>
        </Container>
        <Button
          variant={"primary"}
          onClick={this.addTestSectionComponent}
          disabled={this.state.selectedParentTestSectionComponent === undefined}
        >
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.sectionComponentPages.addButton}
        </Button>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleDelete = id => {
    //delete the section component page
    let objArray = this.props.sectionComponentPages.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(SECTION_COMPONENT_PAGES, objArray);
    // delete the page section
    let pageSectionDefinitionsToDelete = [];
    objArray = this.props.pageSections.filter(obj => {
      if (obj.section_component_page !== id) {
        return obj;
      } else {
        pageSectionDefinitionsToDelete.push(obj.id);
        return null;
      }
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);
    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(
      obj => pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1
    );
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);
  };

  getPageSections = section => {
    return this.props.pageSections.filter(obj => {
      if (obj.section_component_page === section.id) {
        return obj;
      } else {
        return null;
      }
    });
  };

  render() {
    let { sectionComponentPages } = this.props;

    return (
      <div style={styles.mainContainer}>
        {this.makeHeader()}

        {this.state.selectedParentTestSectionComponent !== undefined &&
          sectionComponentPages.map((section, index) => {
            if (
              section.test_section_component === this.state.selectedParentTestSectionComponent.value
            ) {
              let pageSections = this.getPageSections(section);
              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  // iconType={}
                  title={
                    <label>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.sectionComponentPages.collapsableItemName,
                        section.order,
                        section[`${this.props.currentLanguage}_title`]
                      )}
                    </label>
                  }
                  body={
                    <SectionComponentPageForm
                      sectionComponentPage={section}
                      pageSections={pageSections}
                      INDEX={index}
                      handleDelete={this.handleDelete}
                    />
                  }
                />
              );
            }
            return null;
          })}
      </div>
    );
  }
}

export { SectionComponentPages as unconnectedSectionComponentPages };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    pageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ replaceTestDefinitionField, addTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SectionComponentPages);
