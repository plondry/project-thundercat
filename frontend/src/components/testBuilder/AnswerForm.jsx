import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeSaveDeleteButtons,
  makeTextAreaField,
  isNumber,
  allValid,
  testLanguageHasFrench,
  testLanguageHasEnglish
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  ANSWERS,
  ANSWER_DETAILS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class AnswerForm extends Component {
  state = {
    showDialog: false,
    isScoringValueValid: true,
    isEnEmailIdValid: true,
    isFrEmailIdValid: true,
    question: {},
    enDefinition: {},
    frDefinition: {},
    answer: {}
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.answer, ...this.state.answer };
    newObj[`${inputName}`] = value;
    this.setState({ answer: newObj });
  };
  modifyEnDefinition = (inputName, value) => {
    let newObj = { ...this.props.enDefinition, ...this.state.enDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ enDefinition: newObj });
  };
  modifyFrDefinition = (inputName, value) => {
    let newObj = { ...this.props.frDefinition, ...this.state.frDefinition };
    newObj[`${inputName}`] = value;
    this.setState({ frDefinition: newObj });
  };

  scoringValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isScoringValueValid: true });
      this.modifyField(name, value);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = () => {
    const enDefinition = this.props.enDefinition;
    const frDefinition = this.props.frDefinition;
    const answer = this.props.answer;

    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(ANSWERS, { ...answer, ...this.state.answer }, answer.id);
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        this.props.modifyTestDefinitionField(
          ANSWER_DETAILS,
          { ...enDefinition, ...this.state.enDefinition },
          enDefinition.id
        );
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        this.props.modifyTestDefinitionField(
          ANSWER_DETAILS,
          { ...frDefinition, ...this.state.frDefinition },
          frDefinition.id
        );
      }
      this.props.expandItem();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    // delete the answer
    let id = this.props.answer.id;
    let objArray = this.props.answers.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(ANSWERS, objArray);

    //delete the answer details
    objArray = this.props.answerDetails.filter(obj => obj.answer !== id);
    this.props.replaceTestDefinitionField(ANSWER_DETAILS, objArray);

    this.closeDialog();
    this.props.expandItem();
  };

  getMarkdownSectionTabs = (enDefinition, frDefinition) => {
    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "content",
            LOCALIZE.testBuilder.answers,
            "question-section-fr-content-title-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.answers,
            frDefinition.content,
            true,
            this.modifyFrDefinition,
            "question-section-fr-content-field"
          )}
        </Row>
      </>
    );
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel(
            "content",
            LOCALIZE.testBuilder.answers,
            "question-section-en-content-title-field"
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "content",
            LOCALIZE.testBuilder.answers,
            enDefinition.content,
            true,
            this.modifyEnDefinition,
            "question-section-en-content-field"
          )}
        </Row>
      </>
    );
    let TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: 0,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: 1,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }
    return TABS;
  };

  render() {
    let answer = { ...this.props.answer, ...this.state.answer };
    let enDefinition = { ...this.props.enDefinition, ...this.state.enDefinition };
    let frDefinition = { ...this.props.frDefinition, ...this.state.frDefinition };

    let TABS = this.getMarkdownSectionTabs(enDefinition, frDefinition);
    return (
      <div style={styles.mainContiner}>
        <Row style={styles.rowStyle}>
          {makeLabel("scoring_value", LOCALIZE.testBuilder.answers)}
          {makeTextBoxField(
            "scoring_value",
            LOCALIZE.testBuilder.answers,
            String(answer.scoring_value),
            this.state.isScoringValueValid,
            this.scoringValidation
          )}
        </Row>

        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { AnswerForm as unconnectedAnswerForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AnswerForm);
