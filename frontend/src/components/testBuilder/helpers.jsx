import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import { Button, Col } from "react-bootstrap";
import ReactSelect from "react-select";
import "../../css/collapsing-item.css";
import Tooltip from "rc-tooltip";
import LOCALIZE from "../../text_resources";
import { LANGUAGES } from "../commons/Translation";
import "rc-tooltip/assets/bootstrap_white.css";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  input: {
    width: "100%",
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  textAreaInput: {
    width: "100%",
    height: 270,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipMarginTop: {
    marginTop: 1
  },
  tooltipIcon: {
    height: 36,
    width: 40,
    color: "#00565e",
    padding: 6
  },
  secondaryEmailTooltipIcon: {
    height: "23.25px",
    width: "23.25px",
    color: "#00565e",
    margin: "0 6px"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red",
    margin: "5px"
  },
  rowStyle: {
    margin: "5px"
  },
  buttonRowStyle: {
    margin: "15px"
  },
  button: {
    width: "100%"
  },
  centerLabel: {
    marginTop: "auto",
    marginBottom: "auto"
  },
  checkboxFieldContainer: {
    marginLeft: 6,
    marginRight: "auto"
  },
  checkbox: {
    margin: "0 0 6px 12px",
    verticalAlign: "middle"
  },
  checkboxLabel: {
    paddingLeft: "15px",
    margin: 0,
    maxWidth: "95%",
    display: "inline"
  }
};

const getValuesThenPassTo = (event, action, func, dataObj = {}) => {
  let value = "";
  let name = "";
  // validationtype

  // check if select input
  if (event.target === undefined) {
    value = action.option.value;
    name = action.name;
  }
  // else do regular input info get
  else {
    value = event.target.value;
    name = event.target.name;
    if (event.target.type === "checkbox") {
      value = event.target.checked;
    }
  }
  func(name, value, dataObj);
};

export const makeLabel = (inputName, localize, idName = inputName) => {
  return (
    <Col style={styles.centerLabel}>
      <div id={`${idName}-title`}>
        <label id={`${idName}`} htmlFor={`${inputName}-input`}>
          {localize[`${inputName}`].title}
        </label>

        <Tooltip
          trigger={["hover", "focus"]}
          placement="top"
          overlayClassName="tooltip"
          overlay={
            <div>
              <p>{localize[`${inputName}`].titleTooltip}</p>
            </div>
          }
        >
          <Button
            tabIndex="-1"
            variant="link"
            style={{ ...styles.tooltipIconContainer, ...styles.tooltipMarginTop }}
          >
            <FontAwesomeIcon icon={faQuestionCircle} style={styles.secondaryEmailTooltipIcon} />
          </Button>
        </Tooltip>
        <label id={`${idName}-tooltip-for-accessibility`} style={styles.hiddenText}>
          , {localize[`${inputName}`].titleTooltip}
        </label>
      </div>
    </Col>
  );
};

export const makeLabelFromObject = obj => {
  const { name, title, toolTip, idName = name } = obj;
  return (
    <Col style={styles.centerLabel}>
      <div id={`${idName}-title`}>
        <label id={`${idName}`} htmlFor={`${name}-input`}>
          {title}
        </label>

        <Tooltip
          trigger={["hover", "focus"]}
          placement="top"
          overlayClassName="tooltip"
          overlay={
            <div>
              <p>{toolTip}</p>
            </div>
          }
        >
          <Button
            tabIndex="-1"
            variant="link"
            style={{ ...styles.tooltipIconContainer, ...styles.tooltipMarginTop }}
          >
            <FontAwesomeIcon icon={faQuestionCircle} style={styles.secondaryEmailTooltipIcon} />
          </Button>
        </Tooltip>
        <label id={`${idName}-tooltip-for-accessibility`} style={styles.hiddenText}>
          , {toolTip}
        </label>
      </div>
    </Col>
  );
};

export const makeTextBoxField = (
  inputName,
  localize,
  property = "",
  isValid = true,
  onChange = () => {},
  idName = inputName
) => {
  return (
    <Col>
      <input
        name={inputName}
        id={`${idName}-input`}
        className={isValid ? "valid-field" : "invalid-field"}
        aria-labelledby={`${idName} ${idName}-tooltip-for-accessibility invalid-${idName}-msg`}
        aria-required={false}
        aria-invalid={!isValid}
        style={{ ...styles.input, width: "100%" }}
        type="text"
        value={property || ""}
        onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
      />

      {!isValid && (
        <label id={`invalid-${idName}-msg`} style={styles.errorMessage}>
          {localize[`${inputName}`].errorMessage}
        </label>
      )}
    </Col>
  );
};

export const makeTextAreaField = (
  inputName,
  localize,
  property = "",
  isValid = true,
  onChange = () => {},
  idName = inputName
) => {
  return (
    <Col>
      <textarea
        name={inputName}
        id={`${idName}-input`}
        className={isValid ? "valid-field" : "invalid-field"}
        aria-labelledby={`${idName} ${idName}-tooltip-for-accessibility invalid-${idName}-msg`}
        aria-required={false}
        aria-invalid={!isValid}
        style={styles.textAreaInput}
        type="textarea"
        value={property}
        onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
      />

      {!isValid && (
        <label id={`invalid-${idName}-msg`} style={styles.errorMessage}>
          {localize[`${inputName}`].errorMessage}
        </label>
      )}
    </Col>
  );
};

export const makeCheckBoxField = (
  inputName,
  localize,
  property = false,
  onChange = () => {},
  idName = inputName,
  disabled = false
) => {
  return (
    <Col>
      <div className="custom-control custom-checkbox" style={styles.checkboxZone}>
        {disabled ? (
          <input
            name={inputName}
            type="checkbox"
            className="custom-control-input"
            id={`${idName}-id`}
            checked={property}
            disabled
            onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
          />
        ) : (
          <input
            name={inputName}
            type="checkbox"
            className="custom-control-input"
            id={`${idName}-id`}
            checked={property}
            onChange={(event, action) => getValuesThenPassTo(event, action, onChange)}
          />
        )}
        <label className="custom-control-label" htmlFor={`${idName}-id`}>
          {localize[`${inputName}`].titleTooltip}
        </label>
      </div>
    </Col>
  );
};

export const makeCheckBoxFieldFromObject = (obj, fontSize) => {
  const {
    inputName,
    toolTip,
    property = false,
    onChange = () => {},
    idName = inputName,
    dataObj
  } = obj;

  // converting current font size in Int
  const fontSizeInt = parseInt(fontSize.substring(0, 2));
  // initializing transform scale (used for checkbox size depending on the font size selected)
  let transformScale = "";
  // converting checkbox scale based on font size
  switch (true) {
    case fontSizeInt <= 22:
      transformScale = "scale(1)";
      break;
    case fontSizeInt > 22 && fontSizeInt <= 30:
      transformScale = "scale(1.5)";
      break;
    case fontSizeInt > 30 && fontSizeInt <= 40:
      transformScale = "scale(2)";
      break;
    case fontSizeInt > 40:
      transformScale = "scale(2.2)";
      break;
    default:
      transformScale = "scale(1)";
  }
  return (
    <Col>
      <div style={styles.checkboxFieldContainer}>
        <input
          name={inputName}
          type="checkbox"
          style={{ ...styles.checkbox, ...{ transform: transformScale } }}
          id={`${idName}-id`}
          checked={property}
          onChange={(event, action) => getValuesThenPassTo(event, action, onChange, dataObj)}
        />

        <label htmlFor={`${idName}-id`} style={styles.checkboxLabel}>
          {toolTip}
        </label>
      </div>
    </Col>
  );
};

export const makeDropDownField = (
  inputName,
  localize,
  value = null,
  options,
  onChange,
  isDisabled = false
) => {
  return (
    <Col>
      <ReactSelect
        classNamePrefix={inputName}
        styles={{
          // Fixes the overlapping problem of the component
          menu: provided => ({ ...provided, zIndex: 9999 })
        }}
        menuPlacement="auto"
        isDisabled={isDisabled}
        id={inputName}
        name={inputName}
        aria-labelledby={`${inputName}-selected ${inputName}-placeholder`}
        placeholder={localize[`${inputName}`].selectPlaceholder}
        onChange={onChange}
        value={value}
        options={options}
        clearable={false}
        searchable={false}
      />
    </Col>
  );
};

export const makeMultiDropDownField = (
  inputName,
  localize,
  value = null,
  options,
  onChange,
  isDisabled = false
) => {
  return (
    <Col>
      <ReactSelect
        classNamePrefix={inputName}
        styles={{
          // Fixes the overlapping problem of the component
          menu: provided => ({ ...provided, zIndex: 9999 })
        }}
        menuPlacement="auto"
        isMulti="true"
        isDisabled={isDisabled}
        name={inputName}
        aria-labelledby={`${inputName}-selected ${inputName}-placeholder`}
        placeholder={localize[`${inputName}`].selectPlaceholder}
        onChange={onChange}
        value={value}
        options={options}
        clearable={false}
        searchable={false}
      />
    </Col>
  );
};

export const getNextInNumberSeries = (array, fieldName) => {
  let max = 0;
  for (const obj of array) {
    if (obj[`${fieldName}`] >= max) {
      max = obj[`${fieldName}`];
    }
  }
  return max + 1;
};

export const makeSaveDeleteButtons = (saveFunc, deleteFunc, saveText, deleteText) => {
  return (
    <>
      <Col md={4}>
        <button
          style={styles.button}
          id="unit-test-delete-item-button"
          type="button"
          className="btn btn-danger"
          onClick={deleteFunc}
        >
          {deleteText}
        </button>
      </Col>
      <Col md={{ span: 4, offset: 4 }}>
        <button
          style={styles.button}
          id="unit-test-save-item-button"
          type="button"
          className="btn btn-primary"
          onClick={saveFunc}
        >
          {saveText}
        </button>
      </Col>
    </>
  );
};

export const isDecimal = value => {
  return /^\d+\.\d$/.test(value);
};

export const isNumber = value => {
  return /^[0-9]+$/.test(value);
};

export const isNumberOrEmpty = value => {
  return /^[0-9]*$/.test(value);
};

export const noSpecialCharacters = value => {
  return /^[a-zA-ZÀ-Ÿ0-9-. ]*$/.test(value);
};

// whether the test is bilingual or english
export const testLanguageHasEnglish = testLanguage => {
  if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.english) {
    return true;
  }
  return false;
};
// whether the test is bilingual or french
export const testLanguageHasFrench = testLanguage => {
  if (testLanguage === "--" || testLanguage === null || testLanguage === LANGUAGES.french) {
    return true;
  }
  return false;
};

export const testSectionComponentLanguage = (componentID, components) => {
  for (const component of components){
    if(component.id === componentID){
      return component.language
    }
  }
  // return the identifier for bilingual test
  return "--";
}

// return lang for html lang tag
export const getTestLanguage = (componentLanguage, stateLanguage) => {
  if (componentLanguage === "--" || componentLanguage === null) {
    return stateLanguage;
  }
  return componentLanguage;
};

export const languageOptions = [
  { label: LOCALIZE.commons.bilingual, value: "--" },
  { label: LOCALIZE.commons.english, value: LANGUAGES.english },
  { label: LOCALIZE.commons.french, value: LANGUAGES.french }
];

export const allValid = state => {
  for (const property of Object.keys(state)) {
    if (property.indexOf("Valid") >= 0 && !state[property]) {
      return false;
    }
  }
  return true;
};
