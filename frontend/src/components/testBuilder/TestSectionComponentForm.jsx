import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Button } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeDropDownField,
  makeSaveDeleteButtons,
  noSpecialCharacters,
  isNumber,
  makeCheckBoxField,
  allValid,
  getNextInNumberSeries,
  styles as helperStyles,
  languageOptions
} from "./helpers";
import {
  modifyTestDefinitionField,
  QUESTION_LIST_RULES,
  addTestDefinitionField,
  sortById
} from "../../modules/TestBuilderRedux";
import { TestSectionComponentType } from "../testFactory/Constants";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import QuestionListRuleForm from "./QuestionListRuleForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";
import Tooltip from "rc-tooltip";
import "rc-tooltip/assets/bootstrap_white.css";
import { LANGUAGES_SHORT } from "../../modules/LocalizeRedux";

class TestSectionComponentForm extends Component {
  state = {
    showDialog: false,
    isEnTitleValid: true,
    isFrTitleValid: true,
    isOrderValid: true,
    testSectionComponent: {}
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.testSectionComponent, ...this.state.testSectionComponent };
    newObj[`${inputName}`] = value;
    this.setState({ testSectionComponent: newObj });
  };

  orderValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, parseInt(value));
    }
  };

  frTitleValidation = (name, value) => {
    let orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isFrTitleValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isFrTitleValid: false });
      this.modifyField(name, orginalValue);
    }
  };

  enTitleValidation = (name, value) => {
    let orginalValue = value;
    if (noSpecialCharacters(value)) {
      this.setState({ isEnTitleValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isEnTitleValid: false });
      this.modifyField(name, orginalValue);
    }
  };
  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  handleSave = () => {
    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(
        "test_section_components",
        { ...this.props.testSectionComponent, ...this.state.testSectionComponent },
        this.props.testSectionComponent.id
      );
      this.props.expandItem();
    }
  };

  addQuestionListRule = () => {
    let newRule = {
      number_of_questions: 0,
      question_block_type: [],
      test_section_component: this.props.testSectionComponent.id,
      shuffle: false
    };
    newRule.id = getNextInNumberSeries(this.props.questionListRules, "id");
    this.props.addTestDefinitionField(QUESTION_LIST_RULES, newRule, newRule.id);
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  selectedOption = testSectionComponent => {
    for (let property of Object.getOwnPropertyNames(TestSectionComponentType)) {
      if (testSectionComponent.component_type === TestSectionComponentType[`${property}`]) {
        return { label: property, value: testSectionComponent.component_type };
      }
    }
    return undefined;
  };
  selectedLanguage = language => {
    if (language === LANGUAGES_SHORT.en) {
      return { label: LOCALIZE.commons.english, value: LANGUAGES_SHORT.en };
    } else if (language === LANGUAGES_SHORT.fr) {
      return { label: LOCALIZE.commons.french, value: LANGUAGES_SHORT.fr };
    } else {
      return { label: LOCALIZE.commons.bilingual, value: undefined };
    }
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.testSectionComponent.id);
  };

  render() {
    const testSectionComponent = {
      ...this.props.testSectionComponent,
      ...this.state.testSectionComponent
    };

    let options = Object.getOwnPropertyNames(TestSectionComponentType).map(property => {
      return { label: property, value: TestSectionComponentType[property] };
    });
    let selectedOption = this.selectedOption(testSectionComponent);
    let selectedLanguage = this.selectedLanguage(testSectionComponent.language);

    // only question lists can have rules-> question blocks
    let canHaveRules =
      testSectionComponent.component_type === TestSectionComponentType.QUESTION_LIST ||
      testSectionComponent.component_type === TestSectionComponentType.INBOX;
    let questionListRules = this.props.questionListRules.sort(sortById);
    let disabledAddButton = this.props.questionBlockTypes.length < 1;
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("fr_title", LOCALIZE.testBuilder.testSectionComponents)}
          {makeTextBoxField(
            "fr_title",
            LOCALIZE.testBuilder.testSectionComponents,
            testSectionComponent.fr_title,
            this.state.isFrTitleValid,
            this.frTitleValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("en_title", LOCALIZE.testBuilder.testSectionComponents)}
          {makeTextBoxField(
            "en_title",
            LOCALIZE.testBuilder.testSectionComponents,
            testSectionComponent.en_title,
            this.state.isEnTitleValid,
            this.enTitleValidation
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.testSectionComponents)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.testSectionComponents,
            testSectionComponent.order,
            this.state.isOrderValid,
            this.orderValidation
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel("component_type", LOCALIZE.testBuilder.testSectionComponents)}
          {makeDropDownField(
            "component_type",
            LOCALIZE.testBuilder.testSectionComponents,
            selectedOption,
            options,
            this.onSelectChange
          )}
        </Row>

        <Row style={styles.rowStyle}>
          {makeLabel("language", LOCALIZE.testBuilder.testSectionComponents)}
          {makeDropDownField(
            "language",
            LOCALIZE.testBuilder.testSectionComponents,
            selectedLanguage,
            languageOptions,
            this.onSelectChange
          )}
        </Row>

        {canHaveRules && (
          <>
            <Row style={styles.rowStyle}>
              {makeLabel("shuffle_all_questions", LOCALIZE.testBuilder.testSectionComponents)}
              {makeCheckBoxField(
                "shuffle_all_questions",
                LOCALIZE.testBuilder.testSectionComponents,
                testSectionComponent.shuffle_all_questions,
                this.modifyField,
                true
              )}
            </Row>
            <h4>{LOCALIZE.testBuilder.questionListRules.title}</h4>
            {questionListRules.map((rule, index) => {
              return (
                <CollapsingItemContainer
                  key={index}
                  index={index}
                  title={
                    <label>
                      {LOCALIZE.formatString(
                        LOCALIZE.testBuilder.questionListRules.collapsableItemName,
                        index + 1
                      )}
                    </label>
                  }
                  body={
                    <QuestionListRuleForm rule={rule} testSectionComponent={testSectionComponent} />
                  }
                />
              );
            })}

            <Button
              variant={"primary"}
              onClick={this.addQuestionListRule}
              disabled={disabledAddButton}
            >
              <FontAwesomeIcon
                icon={faPlusCircle}
                style={{ margins: "auto", marginRight: "5px" }}
              />
              {LOCALIZE.testBuilder.questionListRules.addButton.title}
            </Button>
            <Tooltip
              trigger={["hover", "focus"]}
              placement="top"
              overlayClassName={"tooltip"}
              overlay={
                <div>
                  <p>{LOCALIZE.testBuilder.questionListRules.addButton.titleTooltip}</p>
                </div>
              }
            >
              <Button
                tabIndex="-1"
                variant="link"
                style={{ ...helperStyles.tooltipIconContainer, ...helperStyles.tooltipMarginTop }}
              >
                <FontAwesomeIcon
                  icon={faQuestionCircle}
                  style={helperStyles.secondaryEmailTooltipIcon}
                ></FontAwesomeIcon>
              </Button>
            </Tooltip>
            <label id={`add-rule-tooltip-for-accessibility`} style={helperStyles.hiddenText}>
              , {LOCALIZE.testBuilder.questionListRules.addButton.titleTooltip}
            </label>
          </>
        )}
        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed and all child objects that
                are related to this object. Are you sure you want to delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { TestSectionComponentForm as unconnectedTestSectionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionBlockTypes: state.testBuilder.question_block_types
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, addTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TestSectionComponentForm);
