import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row, Button } from "react-bootstrap";
import {
  getNextInNumberSeries,
  makeSaveDeleteButtons,
  allValid,
  testLanguageHasEnglish,
  testLanguageHasFrench
} from "./helpers";
import {
  modifyTestDefinitionField,
  addTestDefinitionField,
  ANSWERS,
  ANSWER_DETAILS,
  QUESTION_SECTIONS,
  QUESTION_SECTION_DEFINITIONS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";
import QuestionSectionForm from "./QuestionSectionForm";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import AnswerForm from "./AnswerForm";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { LANGUAGES } from "../commons/Translation";
import { QuestionSectionType } from "../testFactory/Constants";

class MultipleChoiceQuestionForm extends Component {
  state = {
    showDialog: false,
    question: {}
  };

  addAnswer = () => {
    const { testLanguage, question } = this.props;

    let newAnswer = {
      scoring_value: 0,
      question: question.id
    };
    newAnswer.id = getNextInNumberSeries(this.props.answers, "id");
    this.props.addTestDefinitionField(ANSWERS, newAnswer, newAnswer.id);

    //create test language answer details stuff
    let newAnswerDetails = {};
    if (testLanguageHasEnglish(testLanguage)) {
      newAnswerDetails = {
        content: "Temp",
        answer: newAnswer.id,
        language: LANGUAGES.english
      };
      newAnswerDetails.id = getNextInNumberSeries(this.props.answerDetails, "id");
      this.props.addTestDefinitionField(ANSWER_DETAILS, newAnswerDetails, newAnswerDetails.id);
    }
    if (testLanguageHasFrench(testLanguage)) {
      newAnswerDetails = {
        content: "Temp",
        answer: newAnswer.id,
        language: LANGUAGES.french
      };
      newAnswerDetails.id = getNextInNumberSeries(this.props.answerDetails, "id") + 1;
      this.props.addTestDefinitionField(ANSWER_DETAILS, newAnswerDetails, newAnswerDetails.id);
    }
  };

  addQuestionSection = () => {
    const { testLanguage, question } = this.props;
    let newQuestionSection = {
      question_section_type: QuestionSectionType.MARKDOWN,
      question: question.id
    };
    newQuestionSection.id = getNextInNumberSeries(this.props.questionSections, "id");
    newQuestionSection.order = getNextInNumberSeries(
      this.props.questionSections.filter(obj => obj.question === question.id),
      "order"
    );
    this.props.addTestDefinitionField(QUESTION_SECTIONS, newQuestionSection, newQuestionSection.id);

    //create test language answer details stuff
    let newQuestionSectionDefinition = {};
    if (testLanguageHasEnglish(testLanguage)) {
      newQuestionSectionDefinition = {
        content: "Temp",
        question_section: newQuestionSection.id,
        language: LANGUAGES.english,
        question_section_type: QuestionSectionType.MARKDOWN
      };
      newQuestionSectionDefinition.id = getNextInNumberSeries(
        this.props.questionSectionDefinitions,
        "id"
      );
      this.props.addTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        newQuestionSectionDefinition,
        newQuestionSectionDefinition.id
      );
    }
    if (testLanguageHasFrench(testLanguage)) {
      newQuestionSectionDefinition = {
        content: "FR Temp",
        question_section: newQuestionSection.id,
        language: LANGUAGES.french,
        question_section_type: QuestionSectionType.MARKDOWN
      };
      newQuestionSectionDefinition.id =
        getNextInNumberSeries(this.props.questionSectionDefinitions, "id") + 1;
      this.props.addTestDefinitionField(
        QUESTION_SECTION_DEFINITIONS,
        newQuestionSectionDefinition,
        newQuestionSectionDefinition.id
      );
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      //call parents handle save
      this.props.handleSave();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete();
    this.closeDialog();
  };

  selectedOption = field => {
    field = Array.isArray(field) ? field : [field];
    let array = [];
    for (let contactId of field) {
      for (let contact of this.props.addressBook) {
        if (contactId === contact.id) {
          array.push({ label: contact.name, value: contact.id });
        }
      }
    }
    return array;
  };

  render() {
    return (
      <div>
        <h4>{LOCALIZE.testBuilder.questionSections.title}</h4>
        {this.props.questionDetails.questionSections.map((questionSection, index) => {
          let enDefinition = {};
          let frDefinition = {};
          for (let definition of this.props.questionDetails.questionSectionDefinitions) {
            if (
              definition.question_section === questionSection.id &&
              definition.language === LANGUAGES.english
            ) {
              enDefinition = definition;
            }
            if (
              definition.question_section === questionSection.id &&
              definition.language === LANGUAGES.french
            ) {
              frDefinition = definition;
            }
          }

          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              // iconType={}
              title={
                <label>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.questionSections.collapsableItemName,
                    questionSection.order
                  )}
                </label>
              }
              body={
                <QuestionSectionForm
                  question={this.props.question}
                  questionSection={questionSection}
                  frDefinition={frDefinition}
                  enDefinition={enDefinition}
                  testLanguage={this.props.testLanguage}
                />
              }
            />
          );
        })}
        <Button variant={"primary"} onClick={this.addQuestionSection}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.questionSections.addButton}
        </Button>

        <h4>{LOCALIZE.testBuilder.answers.title}</h4>
        {this.props.questionDetails.answers.map((answer, index) => {
          let enDefinition = {};
          let frDefinition = {};
          for (let definition of this.props.questionDetails.answerDetails) {
            if (definition.answer === answer.id && definition.language === LANGUAGES.english) {
              enDefinition = definition;
            }
            if (definition.answer === answer.id && definition.language === LANGUAGES.french) {
              frDefinition = definition;
            }
          }

          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              // iconType={}
              title={
                <label>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.answers.collapsableItemName,
                    answer.scoring_value
                  )}
                </label>
              }
              body={
                <AnswerForm
                  question={this.props.question}
                  answer={answer}
                  frDefinition={frDefinition}
                  enDefinition={enDefinition}
                  testLanguage={this.props.testLanguage}
                />
              }
            />
          );
        })}
        <Button variant={"primary"} onClick={this.addAnswer}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.answers.addButton}
        </Button>

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { MultipleChoiceQuestionForm as unconnectedMultipleChoiceQuestionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    answers: state.testBuilder.answers,
    answerDetails: state.testBuilder.answer_details,
    questionSections: state.testBuilder.question_sections,
    questionSectionDefinitions: state.testBuilder.question_section_definitions
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, addTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MultipleChoiceQuestionForm);
