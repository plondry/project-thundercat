import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  noSpecialCharacters,
  allValid,
  makeTextBoxField,
  makeSaveDeleteButtons
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTION_BLOCK_TYPES
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class QuestionForm extends Component {
  state = {
    showDialog: false,
    isNameValid: true,
    questionBlockType: {}
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.questionBlockType, ...this.state.questionBlockType };
    newObj[`${inputName}`] = value;
    this.setState({ questionBlockType: newObj });
  };

  nameValidation = (name, value) => {
    if (noSpecialCharacters(value)) {
      this.setState({ isNameValid: true });
      this.modifyField(name, value);
    } else {
      this.setState({ isNameValid: false });
      this.modifyField(name, value);
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(
        QUESTION_BLOCK_TYPES,
        { ...this.props.questionBlockType, ...this.state.questionBlockType },
        this.props.questionBlockType.id
      );
      this.props.expandItem();
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.questionBlockType.id);
    this.props.expandItem();
    this.closeDialog();
  };

  render() {
    const questionBlockType = {
      ...this.props.questionBlockType,
      ...this.state.questionBlockType
    };
    return (
      <div style={styles.mainContainer}>
        <Row style={styles.rowStyle}>
          {makeLabel("name", LOCALIZE.testBuilder.questionBlockTypes)}
          {makeTextBoxField(
            "name",
            LOCALIZE.testBuilder.questionBlockTypes,
            questionBlockType.name,
            this.state.isNameValid,
            this.nameValidation
          )}
        </Row>

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { QuestionForm as unconnectedQuestionForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionForm);
