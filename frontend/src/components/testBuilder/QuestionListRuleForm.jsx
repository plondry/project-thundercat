import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeTextBoxField,
  makeSaveDeleteButtons,
  isNumber,
  makeCheckBoxField,
  allValid,
  isNumberOrEmpty,
  makeMultiDropDownField
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  QUESTION_LIST_RULES
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class QuestionListRuleForm extends Component {
  state = {
    showDialog: false,
    isNumberValid: true,
    isOrderValid: true,
    selectedType: undefined,
    rule: {}
  };

  modifyField = (inputName, value) => {
    let newObj = { ...this.props.rule, ...this.state.rule };
    newObj[`${inputName}`] = value;
    this.setState({ rule: newObj });
  };

  numberValidation = (name, value) => {
    if (isNumber(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, value);
    }
  };

  orderValidation = (name, value) => {
    if (isNumberOrEmpty(value)) {
      this.setState({ isOrderValid: true });
      this.modifyField(name, value);
    }
  };

  onSelectChange = (event, action) => {
    this.modifyField(action.name, event.value);
  };

  onMultiSelectChange = (event, action) => {
    let rule = { ...this.props.rule, ...this.state.rule };
    let newArray = [];
    if (action.action === "remove-value") {
      newArray = [...rule[`${action.name}`]].filter(obj => obj !== action.removedValue.value);
    } else if (action.action !== "clear") {
      newArray = [...rule[`${action.name}`], action.option.value];
    }

    this.modifyField(action.name, newArray);
  };

  handleSave = () => {
    const rule = this.props.rule;

    if (allValid(this.state)) {
      this.props.modifyTestDefinitionField(
        QUESTION_LIST_RULES,
        { ...rule, ...this.state.rule },
        rule.id
      );
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    // delete the question list rule
    let id = this.props.rule.id;
    let objArray = this.props.questionListRules.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(QUESTION_LIST_RULES, objArray);

    this.closeDialog();
    this.props.expandItem();
  };

  selectedBlock = rule => {
    for (let block of this.props.questionBlockTypes) {
      if (rule.question_block_type === block.id) {
        return { label: block.name, value: block.id };
      }
    }
    return undefined;
  };

  selectedBlocks = fieldArray => {
    fieldArray = Array.isArray(fieldArray) ? fieldArray : [fieldArray];
    let array = [];
    for (let field of fieldArray) {
      for (let block of this.props.questionBlockTypes) {
        if (field === block.id) {
          array.push({ label: block.name, value: block.id });
        }
      }
    }
    return array;
  };

  render() {
    let rule = { ...this.props.rule, ...this.state.rule };
    let blockOptions = this.props.questionBlockTypes.map(block => {
      return { label: block.name, value: block.id };
    });
    let selectedBlocks = this.selectedBlocks(rule.question_block_type);
    return (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("number_of_questions", LOCALIZE.testBuilder.questionListRules)}
          {makeTextBoxField(
            "number_of_questions",
            LOCALIZE.testBuilder.questionListRules,
            rule.number_of_questions,
            this.state.isNumberValid,
            this.numberValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("order", LOCALIZE.testBuilder.questionListRules)}
          {makeTextBoxField(
            "order",
            LOCALIZE.testBuilder.questionListRules,
            rule.order,
            this.state.isOrderValid,
            this.orderValidation
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("question_block_type", LOCALIZE.testBuilder.questionListRules)}
          {makeMultiDropDownField(
            "question_block_type",
            LOCALIZE.testBuilder.questionListRules,
            selectedBlocks,
            blockOptions,
            this.onMultiSelectChange
          )}
        </Row>
        <Row style={styles.rowStyle}>
          {makeLabel("shuffle", LOCALIZE.testBuilder.questionListRules)}
          {makeCheckBoxField(
            "shuffle",
            LOCALIZE.testBuilder.questionListRules,
            rule.shuffle,
            this.modifyField,
            true
          )}
        </Row>

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={"Delete Confirmation"}
          description={
            <div>
              <p>
                Deleting this item will delete all the content displayed. Are you sure you want to
                delete this object?
              </p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </>
    );
  }
}

export { QuestionListRuleForm as unconnectedQuestionListRuleForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    questionBlockTypes: state.testBuilder.question_block_types,
    questionListRules: state.testBuilder.question_list_rules
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuestionListRuleForm);
