import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import TestSectionForm from "./TestSectionForm";
import { getNextInNumberSeries } from "./helpers";
import {
  addTestDefinitionField,
  replaceTestDefinitionField,
  PAGE_SECTIONS,
  PAGE_SECTION_DEFINITIONS,
  TEST_SECTION_COMPONENTS,
  SECTION_COMPONENT_PAGES,
  TEST_SECTIONS,
  NEXT_SECTION_BUTTONS
} from "../../modules/TestBuilderRedux";
import { LANGUAGES } from "../../modules/LocalizeRedux";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  collapsingContainer: {
    width: "90%",
    margin: "auto"
  },
  input: {
    width: 270,
    height: 38,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    display: "block"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  hiddenText: {
    position: "absolute",
    left: -10000,
    top: "auto",
    width: 1,
    height: 1,
    overflow: "hidden"
  },
  popover: {
    zIndex: 9999,
    padding: "0 12px",
    margin: 0
  },
  tooltipIconContainer: {
    padding: 0
  },
  tooltipMarginTop: {
    marginTop: 1
  },
  tooltipIcon: {
    height: 36,
    width: 40,
    color: "#00565e",
    padding: 6
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6
  },
  rowStyle: {
    margin: "5px"
  }
};

class TestSections extends Component {
  addTestSection = () => {
    let newObj = {
      id: 0,
      order: 1,
      section_type: 2,
      default_time: null,
      en_title: "",
      fr_title: "",
      next_section_button_type: 0,
      scoring_type: 0,
      uses_notepad: false,
      block_cheating: false,
      default_tab: null,
      test_definition: 1
    };
    newObj.order = getNextInNumberSeries(this.props.testSections, "order");
    newObj.id = getNextInNumberSeries(this.props.testSections, "id");
    this.props.addTestDefinitionField(TEST_SECTIONS, newObj, newObj.order);
  };

  handleDelete = id => {
    // delete the test section component
    let objArray = this.props.testSections.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(TEST_SECTIONS, objArray);

    //delete the next section button
    objArray = this.props.nextSectionButtons.filter(obj => obj.test_section !== id);
    this.props.replaceTestDefinitionField(NEXT_SECTION_BUTTONS, objArray);

    //delete the section component page
    let sectionCompsToDelete = [];
    objArray = this.props.testSectionComponents.filter(obj => {
      if (obj.test_section.indexOf(id) === -1) {
        return obj;
      } else {
        obj.test_section = obj.test_section.filter(ts => ts !== id);
        if (obj.test_section.length > 0) {
          return obj;
        } else {
          sectionCompsToDelete.push(obj.id);
          return null;
        }
      }
    });
    this.props.replaceTestDefinitionField(TEST_SECTION_COMPONENTS, objArray);

    //delete the section component page
    let pageSectionsToDelete = [];
    objArray = this.props.sectionComponentPages.filter(obj => {
      if (sectionCompsToDelete.indexOf(obj.test_section_component) === -1) {
        return obj;
      } else {
        pageSectionsToDelete.push(obj.id);
        return null;
      }
    });
    this.props.replaceTestDefinitionField(SECTION_COMPONENT_PAGES, objArray);

    // delete the page section
    let pageSectionDefinitionsToDelete = [];
    objArray = this.props.pageSections.filter(obj => {
      if (pageSectionsToDelete.indexOf(obj.section_component_page) === -1) {
        return obj;
      } else {
        pageSectionDefinitionsToDelete.push(obj.id);
        return null;
      }
    });
    this.props.replaceTestDefinitionField(PAGE_SECTIONS, objArray);

    // delete the page section definitions
    objArray = this.props.pageSectionDefinitions.filter(
      obj => pageSectionDefinitionsToDelete.indexOf(obj.page_section) === -1
    );
    this.props.replaceTestDefinitionField(PAGE_SECTION_DEFINITIONS, objArray);
  };

  render() {
    let { testSections } = this.props;
    return (
      <div style={styles.mainContainer}>
        <h2>{LOCALIZE.testBuilder.testSection.title}</h2>
        <p>{LOCALIZE.testBuilder.testSection.description}</p>
        <Button variant={"primary"} onClick={this.addTestSection}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.testSection.addButton}
        </Button>
        <div style={styles.borderBox} />
        {testSections.map((section, index) => {
          let enButtonDetails = {};
          let frButtonDetails = {};
          for (let obj of this.props.nextSectionButtons) {
            if (obj.test_section === section.id && obj.language === LANGUAGES.english) {
              enButtonDetails = obj;
            }
            if (obj.test_section === section.id && obj.language === LANGUAGES.french) {
              frButtonDetails = obj;
            }
          }
          enButtonDetails.test_section = section.id;
          frButtonDetails.test_section = section.id;

          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              // iconType={}
              title={
                <label>
                  {LOCALIZE.formatString(
                    LOCALIZE.testBuilder.testSection.collapsableItemName,
                    section.order,
                    section[`${this.props.currentLanguage}_title`]
                  )}
                </label>
              }
              body={
                <TestSectionForm
                  testSection={section}
                  INDEX={index}
                  enButtonDetails={enButtonDetails}
                  frButtonDetails={frButtonDetails}
                  handleDelete={this.handleDelete}
                />
              }
            />
          );
        })}
      </div>
    );
  }
}

export { TestSections as unconnectedTestSections };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    testSectionComponents: state.testBuilder.test_section_components,
    sectionComponentPages: state.testBuilder.section_component_pages,
    pageSections: state.testBuilder.page_sections,
    pageSectionDefinitions: state.testBuilder.page_section_definitions,
    nextSectionButtons: state.testBuilder.next_section_buttons
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ addTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TestSections);
