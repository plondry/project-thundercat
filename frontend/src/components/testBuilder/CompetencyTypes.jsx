import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { Button } from "react-bootstrap";
import CollapsingItemContainer from "../eMIB/CollapsingItemContainer";
import { getNextInNumberSeries } from "./helpers";
import {
  replaceTestDefinitionField,
  addTestDefinitionField,
  COMPETENCY_TYPES
} from "../../modules/TestBuilderRedux";
import CompetencyTypeForm from "./CompetencyTypeForm";

export const styles = {
  mainContainer: {
    width: "100%"
  },
  label: {
    padding: "3px 0 0 4px"
  },
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    padding: "5px",
    borderColor: "#CECECE",
    color: "red"
  },
  rowStyle: {
    margin: "5px"
  }
};

class CompetencyTypes extends Component {
  addCompetencyType = () => {
    let newCompetencyType = {
      name: "Temp"
    };
    newCompetencyType.id = getNextInNumberSeries(this.props.competencyTypes, "id");
    this.props.addTestDefinitionField(COMPETENCY_TYPES, newCompetencyType, newCompetencyType.id);
  };

  makeHeader = () => {
    return (
      <>
        <h2>{LOCALIZE.testBuilder.competencyTypes.topTitle}</h2>
        <p>{LOCALIZE.testBuilder.competencyTypes.description}</p>
        <Button variant={"primary"} onClick={this.addCompetencyType}>
          <FontAwesomeIcon icon={faPlusCircle} style={{ margins: "auto", marginRight: "5px" }} />
          {LOCALIZE.testBuilder.competencyTypes.addButton}
        </Button>
        <div style={styles.borderBox} />
      </>
    );
  };

  handleDelete = id => {
    let objArray = this.props.competencyTypes.filter(obj => obj.id !== id);
    this.props.replaceTestDefinitionField(COMPETENCY_TYPES, objArray);
  };

  render() {
    let { competencyTypes } = this.props;
    return (
      <div style={styles.mainContainer}>
        {this.makeHeader()}

        {competencyTypes.map((competencyType, index) => {
          return (
            <CollapsingItemContainer
              key={index}
              index={index}
              title={<label>{competencyType[`${this.props.currentLanguage}_name`]}</label>}
              body={
                <CompetencyTypeForm
                  competencyType={competencyType}
                  handleDelete={this.handleDelete}
                />
              }
            />
          );
        })}
      </div>
    );
  }
}

export { CompetencyTypes as UnconnectedCompetencyTypes };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    competencyTypes: state.testBuilder.competency_types
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ replaceTestDefinitionField, addTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CompetencyTypes);
