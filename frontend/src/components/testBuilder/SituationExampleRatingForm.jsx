import React, { Component } from "react";
import { connect } from "react-redux";
import LOCALIZE from "../../text_resources";
import { bindActionCreators } from "redux";
import { Row } from "react-bootstrap";
import {
  makeLabel,
  makeSaveDeleteButtons,
  makeTextAreaField,
  allValid,
  testLanguageHasFrench,
  testLanguageHasEnglish,
  makeTextBoxField,
  isDecimal
} from "./helpers";
import {
  modifyTestDefinitionField,
  replaceTestDefinitionField,
  SITUATION_EXAMPLE_RATING_DETAILS,
  SITUATION_EXAMPLE_RATINGS
} from "../../modules/TestBuilderRedux";
import { styles } from "./TestSectionForm";
import { UnconnectedTopTabs as TopTabs } from "../commons/TopTabs";
import PopupBox, { BUTTON_TYPE } from "../commons/PopupBox";

class SituationExampleRatingForm extends Component {
  state = {
    showDialog: false,
    isScoreValid: true,
    enDetails: {},
    frDetails: {},
    rating: {}
  };

  modifyRating = (inputName, value) => {
    let newObj = { ...this.props.rating, ...this.state.rating };
    newObj[`${inputName}`] = value;
    this.setState({ rating: newObj });
  };

  modifyFrSituation = (inputName, value) => {
    let newObj = { ...this.props.details.fr, ...this.state.frDetails };
    newObj[`${inputName}`] = value;
    this.setState({ frDetails: newObj });
  };
  modifyEnSituation = (inputName, value) => {
    let newObj = { ...this.props.details.en, ...this.state.enDetails };
    newObj[`${inputName}`] = value;
    this.setState({ enDetails: newObj });
  };

  validateScore = (name, value) => {
    if (isDecimal(value)) {
      this.setState({ isScoreValid: true });
      this.modifyRating(name, Number(value).toFixed(1));
    } else {
      this.setState({ isScoreValid: false });
      this.modifyRating(name, value);
    }
  };

  handleSave = () => {
    if (allValid(this.state)) {
      const { rating } = this.props;
      this.props.modifyTestDefinitionField(
        SITUATION_EXAMPLE_RATINGS,
        { ...rating, ...this.state.rating },
        rating.id
      );
      if (testLanguageHasEnglish(this.props.testLanguage)) {
        const enDetails = { ...this.props.details.en, ...this.state.enDetails };
        this.props.modifyTestDefinitionField(
          SITUATION_EXAMPLE_RATING_DETAILS,
          enDetails,
          enDetails.id
        );
      }
      if (testLanguageHasFrench(this.props.testLanguage)) {
        const frDetails = this.props.details.fr;
        this.props.modifyTestDefinitionField(
          SITUATION_EXAMPLE_RATING_DETAILS,
          { ...frDetails, ...this.state.frDetails },
          frDetails.id
        );
      }
    }
  };

  openDialog = () => {
    this.setState({ showDialog: true });
  };

  closeDialog = () => {
    this.setState({ showDialog: false });
  };

  confirmDelete = () => {
    this.props.handleDelete(this.props.rating.id);
    this.closeDialog();
  };

  getLanguageTabs = (enDetails, frDetails) => {
    const localize = LOCALIZE.testBuilder.situationRating;

    let frPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("example", localize, "question-rating-fr-example")}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "example",
            localize,
            frDetails.example,
            true,
            this.modifyFrSituation,
            "question-rating-fr-example"
          )}
        </Row>
      </>
    );
    let enPageSectionTypeContent = (
      <>
        <Row style={styles.rowStyle}>
          {makeLabel("example", localize, "question-rating-en-example")}
        </Row>
        <Row style={styles.rowStyle}>
          {makeTextAreaField(
            "example",
            localize,
            enDetails.example,
            true,
            this.modifyEnSituation,
            "question-rating-en-example"
          )}
        </Row>
      </>
    );
    let TABS = [];
    if (testLanguageHasEnglish(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.english,
        tabName: LOCALIZE.commons.english,
        body: enPageSectionTypeContent
      });
    }
    if (testLanguageHasFrench(this.props.testLanguage)) {
      TABS.push({
        key: LOCALIZE.commons.french,
        tabName: LOCALIZE.commons.french,
        body: frPageSectionTypeContent
      });
    }

    return TABS;
  };

  render() {
    const { details } = this.props;
    let rating = { ...this.props.rating, ...this.state.rating };
    if (!details) {
      return null;
    }
    let enDetails = { ...details.en, ...this.state.enDetails };
    let frDetails = { ...details.fr, ...this.state.frDetails };
    let TABS = this.getLanguageTabs(enDetails, frDetails);
    return (
      <div>
        <Row style={styles.rowStyle}>
          {makeLabel("score", LOCALIZE.testBuilder.situationRating)}
          {makeTextBoxField(
            "score",
            LOCALIZE.testBuilder.situationRating,
            rating.score,
            this.state.isScoreValid,
            this.validateScore
          )}
        </Row>
        <TopTabs TABS={TABS} defaultTab={TABS[0].key} />

        <Row style={styles.buttonRowStyle}>
          {makeSaveDeleteButtons(
            this.handleSave,
            this.openDialog,
            LOCALIZE.commons.applyButton,
            LOCALIZE.commons.deleteButton
          )}
        </Row>
        <PopupBox
          show={this.state.showDialog}
          handleClose={this.closeDialog}
          title={LOCALIZE.commons.deleteConfirmation}
          description={
            <div>
              <p>{LOCALIZE.testBuilder.emailQuestions.deleteDescription}</p>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          rightButtonType={BUTTON_TYPE.danger}
          rightButtonTitle={LOCALIZE.commons.deleteButton}
          rightButtonAction={this.confirmDelete}
        />
      </div>
    );
  }
}

export { SituationExampleRatingForm as UnconnectedSituationExampleRatingForm };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ modifyTestDefinitionField, replaceTestDefinitionField }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SituationExampleRatingForm);
