import React, { Component } from "react";
import LOCALIZE from "../../text_resources";
import { connect } from "react-redux";
import { isTokenStillValid } from "../../modules/LoginRedux";
import { bindActionCreators } from "redux";
import { Helmet } from "react-helmet";
import ContentContainer from "../commons/ContentContainer";
import "../../css/test-administration.css";
import { styles as SystemAdministrationStyles } from "../etta/SystemAdministration";
import SideNavigation from "../eMIB/SideNavigation";
import TestDefinition from "./TestDefinition";
import TestSections from "./TestSections";
import TestSectionComponents from "./TestSectionComponents";
import SectionComponentPages from "./SectionComponentPages";
import ComponentPageSections from "./ComponentPageSections";
import { Button, Row, Col } from "react-bootstrap";
import Select from "react-select";
import AddressBookContacts from "./AddressBookContacts";
import { HEADER_HEIGHT } from "../eMIB/constants";
import { history } from "../../store-index";
import { PATH } from "../commons/Constants";
import { resetTestFactoryState } from "../../modules/TestSectionRedux";
import { resetNotepadState } from "../../modules/NotepadRedux";
import { resetQuestionListState } from "../../modules/QuestionListRedux";
import { resetTopTabsState, switchSideTab, switchTopTab } from "../../modules/NavTabsRedux";
import { setTestSectionToView } from "../../modules/TestBuilderRedux";
import Questions from "./Questions";
import QuestionBlockTypes from "./QuestionBlockTypes";
import CompetencyTypes from "./CompetencyTypes";

const styles = {
  borderBox: {
    borderBottom: "1px solid",
    borderRadius: "5px",
    margin: "0px 15px 0px 15px",
    padding: "15px",
    borderColor: "#CECECE"
  },
  tabContent: {
    backgroundColor: "transparent",
    overflowY: "auto",
    padding: "5px 5px 5px 5px"
  }
};

class TestBuilder extends Component {
  // preventing memory leak by using this '_isMounted' const (source: https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component)
  _isMounted = false;

  static propTypes = {
    // Props from Redux
  };

  state = {
    viewTestSectionOrderNumber: undefined
  };

  // calling redux state on page load to get the first name and last name
  componentDidMount = () => {
    this._isMounted = true;
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        //populate test definition
        this.setState({ isLoaded: true });
        this.props.switchTopTab(100);
      }
    });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    // check if the token is still valid before getting user information
    this.props.isTokenStillValid().then(bool => {
      // if the token is still valid
      if (this._isMounted && bool) {
        // if CAT language toggle button has been selected
        if (prevProps.currentLanguage !== this.props.currentLanguage) {
          // populate test definition
        }
        // once page is loaded, focus on welcome message (accessibility purposes)
        // if (prevState.isLoaded !== this.state.isLoaded) {
        // document.getElementById("user-welcome-message-div").focus();
        // }
      }
    });
  }

  componentWillUnmount = () => {
    this._isMounted = false;
  };

  //Returns array where each item indicates specifications related to How To Page including the title and the body
  getTestAdministrationSections = () => {
    return [
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testDefinition,
        body: <TestDefinition />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testSections,
        body: <TestSections />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.testSectionComponents,
        body: <TestSectionComponents />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.sectionComponentPages,
        body: <SectionComponentPages />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.componentPageSections,
        body: <ComponentPageSections />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.addressBook,
        body: <AddressBookContacts />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.questions,
        body: <Questions />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.questionBlockTypes,
        body: <QuestionBlockTypes />
      },
      {
        menuString: LOCALIZE.testBuilder.sideNavItems.competencyTypes,
        body: <CompetencyTypes />
      }
    ];
  };

  goToTestView = () => {
    this.props.setTestSectionToView(this.state.viewTestSectionOrderNumber);
    this.resetAllRedux();
    history.push(PATH.testBuilderTest);
  };

  resetAllRedux = () => {
    this.props.resetTestFactoryState();
    this.props.resetNotepadState();
    this.props.resetQuestionListState();
    this.props.resetTopTabsState();
  };

  selectTestSection = event => {
    this.setState({ viewTestSectionOrderNumber: event.value });
  };

  switchTab = key => {
    this.props.switchSideTab(key);
  };

  render() {
    const specs = this.getTestAdministrationSections();
    const testSectionOptions = this.props.testSections.map(testSection => {
      return {
        label: testSection[`${this.props.currentLanguage}_title`],
        value: testSection.order
      };
    });
    return (
      <div className="app">
        <Helmet>
          <html lang={this.props.currentLanguage} />
          <title>{LOCALIZE.titles.testAdministration}</title>
        </Helmet>
        <ContentContainer>
          <div id="main-content" role="main">
            <div>
              <div style={SystemAdministrationStyles.sectionContainerLabelDiv}>
                <div>
                  <label style={SystemAdministrationStyles.sectionContainerLabel}>
                    {LOCALIZE.testBuilder.containerLabel}
                    <span style={SystemAdministrationStyles.tabStyleBorder}></span>
                  </label>
                </div>
              </div>

              <div style={SystemAdministrationStyles.sectionContainer}>
                <div style={styles.borderBox}>
                  <Row>
                    <Col>
                      <Select
                        styles={{
                          // Fixes the overlapping problem of the component
                          menu: provided => ({ ...provided, zIndex: 9999 })
                        }}
                        options={testSectionOptions}
                        placeholder={LOCALIZE.testBuilder.testDefinitionSelectPlaceholder}
                        onChange={this.selectTestSection}
                      />
                    </Col>
                    <Col>
                      <Button
                        disabled={this.state.viewTestSectionOrderNumber === undefined}
                        variant={this.props.variant}
                        onClick={this.goToTestView}
                        style={{ width: "100%" }}
                      >
                        {LOCALIZE.testBuilder.inTestView}
                      </Button>
                    </Col>
                  </Row>
                </div>
                <SideNavigation
                  specs={specs}
                  startIndex={0}
                  displayNextPreviousButton={false}
                  activeKey={this.props.currentTab}
                  switchTab={this.switchTab}
                  isMain={true}
                  tabContainerStyle={SystemAdministrationStyles.tabContainer}
                  tabContentStyle={styles.tabContent}
                  navStyle={SystemAdministrationStyles.nav}
                  bodyContentCustomStyle={SystemAdministrationStyles.sideNavBodyContent}
                  headerFooterPX={HEADER_HEIGHT * 2.6}
                  showNavButtons={false}
                />
              </div>
            </div>
          </div>
        </ContentContainer>
      </div>
    );
  }
}

export { TestBuilder as UnconnectedTestBuilder };

const mapStateToProps = (state, ownProps) => {
  let currentTab = 1;
  if (typeof state.navTabs.currentSideTab[state.navTabs.currentTopTab] !== "undefined") {
    currentTab = state.navTabs.currentSideTab[state.navTabs.currentTopTab].sideTab;
  }
  return {
    currentLanguage: state.localize.language,
    testSections: state.testBuilder.test_sections,
    currentTab: currentTab
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      isTokenStillValid,
      resetTestFactoryState,
      resetNotepadState,
      resetQuestionListState,
      resetTopTabsState,
      setTestSectionToView,
      switchSideTab,
      switchTopTab
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(TestBuilder);
