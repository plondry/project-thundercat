import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Router, Route, Redirect, Switch } from "react-router-dom";
import { authenticateAction, logoutAction, isTokenStillValid } from "./modules/LoginRedux";
import "./css/lib/aurora.min.css";
import "./css/cat-theme.css";
import { Helmet } from "react-helmet";
import Status from "./Status";
import Home from "./Home";
import TestAdministration from "./components/ta/TestAdministration";
import { history } from "./store-index";
import SelectLanguage from "./SelectLanguage";
import SiteNavBar from "./SiteNavBar";
import { PATH } from "./components/commons/Constants";
import { refreshAuthToken } from "./modules/LoginRedux";
import SampleTestsRoutes from "./components/samples/SampleTestRoutes";
import {
  getUserPermissions,
  updateCurrentHomePageState,
  updatePermissionsState
} from "./modules/PermissionsRedux";
import { setIsUserLoading } from "./modules/UserRedux";
import { PrivateRoute } from "./components/commons/PrivateRoute";
import Dashboard from "./Dashboard";
import Profile from "./components/profile/Profile";
import IncidentReport from "./IncidentReport";
import MyTests from "./MyTests";
import ContactUs from "./ContactUs";
import GenericErrorPage from "./components/commons/errorPages/GenericErrorPage";
import WebsocketConnectionErrorPage from "./components/commons/errorPages/WebsocketConnectionErrorPage";
import SystemAdministration from "./components/etta/SystemAdministration";
import PpcAdministration from "./PpcAdministration";
import ScorerRoutes from "./components/scorer/ScorerRoutes";
import { resetTestStatusState } from "./modules/TestStatusRedux";
import { resetNotepadState } from "./modules/NotepadRedux";
import { resetTestStatusState as resetSampleTestStatusState } from "./modules/SampleTestStatusRedux";
import { resetPermissionsState } from "./modules/PermissionsRedux";
import { resetAccommodations } from "./modules/AccommodationsRedux";
import { getAssignedTests } from "./modules/AssignedTestsRedux";
import ExpiredTokenPopop from "./ExpiredTokenPopup";
import TestLanding from "./components/testFactory/TestLanding";
import SessionStorage, { ACTION, ITEM } from "./SessionStorage";
import { TEST_STATUS } from "./components/ta/Constants";
import TestBuilder from "./components/testBuilder/TestBuilder";
import TestBuilderTestPage from "./components/testFactory/TestBuilderTestPage";
import LOCALIZE from "./text_resources";
import Accommodations from "./Accommodations";
import { getLineSpacingCSS } from "./modules/AccommodationsRedux";

class App extends Component {
  static propTypes = {
    // Props from Redux
    authenticateAction: PropTypes.func,
    logoutAction: PropTypes.func,
    currentTestId: PropTypes.string,
    refreshAuthToken: PropTypes.func,
    getUserPermissions: PropTypes.func,
    updatePermissionsState: PropTypes.func,
    resetTestStatusState: PropTypes.func,
    resetSampleTestStatusState: PropTypes.func,
    resetNotepadState: PropTypes.func,
    resetPermissionsState: PropTypes.func,
    resetAccommodations: PropTypes.func,
    isTokenStillValid: PropTypes.func,
    getAssignedTests: PropTypes.func,
    updateCurrentHomePageState: PropTypes.func,
    setIsUserLoading: PropTypes.func
  };

  state = {
    setFocusOnQuitTestButton: false,
    isLanguageSelected: SessionStorage(ACTION.GET, ITEM.CAT_LANGUAGE),
    showTokenExpiredDialog: false
  };

  handleExpiredTokenAction = () => {
    // reset all states in case of token expiration
    this.props.logoutAction();
    this.props.resetTestStatusState();
    this.props.resetSampleTestStatusState();
    this.props.resetNotepadState();
    this.props.resetPermissionsState();
    this.props.resetAccommodations();
    // push to select language page (application root)
    history.push("/oec-cat/");
    // display token expired popup
    this.setState({ showTokenExpiredDialog: true });
  };

  closePopup = () => {
    this.setState({ showTokenExpiredDialog: false });
    // reloading page to make sure that all states are reseted
    window.location.reload();
  };

  initializeLanguage = () => {
    this.setState({ isLanguageSelected: true });
  };

  componentDidMount = () => {
    // set focus on quit test button on component load
    this.setState({ setFocusOnQuitTestButton: true });
    // getting the authentication token from the local storage
    const token = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);

    // if there is no token, then there is no point in trying to verify it
    if (token === undefined) {
      // update authenticated state to false
      this.props.authenticateAction(false);
      return;
    }
    // refresh the token
    else {
      this.refreshToken();
    }
  };

  componentDidUpdate = prevProps => {
    this.props.setIsUserLoading(true);
    // if the user is authenticated and if the user is not yet defined (avoid to constantly call this function)
    // as soon as the user is defined, this function will not no longer be called
    if (this.props.authenticated) {
      // checks if the token is still valid
      this.props.isTokenStillValid().then(bool => {
        // token is still valid
        if (bool) {
          // update authenticated state to true
          this.props.authenticateAction(true);
          // initialize permission flags
          this.props.getUserPermissions().then(response => {
            let isSuperUser = this.props.isSuperUser;
            let isEtta = false;
            let isPpc = false;
            let isTa = false;
            let isScorer = false;
            // specified user is a super user
            if (isSuperUser) {
              isEtta = true;
              isPpc = true;
              isTa = true;
              isScorer = true;
              // // specified user is not a super user
            } else {
              // looping in user permissions to update the needed permission flags
              for (let i = 0; i < response.length; i++) {
                if (response[i].codename === "is_etta") {
                  isEtta = true;
                } else if (response[i].codename === "is_ppc") {
                  isPpc = true;
                } else if (response[i].codename === "is_scorer") {
                  isScorer = true;
                } else if (response[i].codename === "is_test_administrator") {
                  isTa = true;
                }
              }
            }
            // update permissions states based on the user permission flags
            this.props.updatePermissionsState({
              isEtta: isEtta,
              isPpc: isPpc,
              isTa: isTa,
              isScorer: isScorer
            });
            // update the current home page state based on the permissions
            // is super user or ETTA
            if (isEtta || isSuperUser) {
              this.props.updateCurrentHomePageState(PATH.systemAdministration);
              // is PPC
            } else if (isPpc) {
              this.props.updateCurrentHomePageState(PATH.ppcAdministration);
              // is scorer
            } else if (isScorer) {
              this.props.updateCurrentHomePageState(PATH.scorerBase);
              // is TA
            } else if (isTa) {
              this.props.updateCurrentHomePageState(PATH.testAdministration);
              //   // is candidate
            } else {
              this.props.updateCurrentHomePageState(PATH.dashboard);
            }
            // No more user data is loading
            this.props.setIsUserLoading(false);
          });

          // getting assigned test for the current user
          this.props.getAssignedTests().then(response => {
            // check for each assigned test
            for (let i = 0; i < response.length; i++) {
              // if there is an active test (active, locked or paused)
              if (
                response[i].status === TEST_STATUS.ACTIVE ||
                response[i].status === TEST_STATUS.LOCKED ||
                response[i].status === TEST_STATUS.PAUSED
              ) {
                // redirect to test page
                history.push(PATH.testBase);
              }
            }
          });
          // token is expired
        } else {
          // update authenticated state to false
          this.props.authenticateAction(false);
          this.handleExpiredTokenAction();
          // this.props.getUserPermissions is not called in this case, so not loading user data
          this.props.setIsUserLoading(false);
        }
      });
      // if home page changes, push new path
      if (prevProps.currentHomePage !== this.props.currentHomePage) {
        history.push(this.props.currentHomePage);
      }
    } else {
      // this.props.isTokenStillValid is not called in this case, so not loading user data
      this.props.setIsUserLoading(false);
    }
  };

  // refreshing user auth token
  refreshToken = () => {
    const token = SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);
    // token exists
    if (token !== null && this.props.authenticated) {
      this.props.isTokenStillValid().then(bool => {
        // checks if the token is still valid
        if (bool) {
          this.props.refreshAuthToken().then(response => {
            // removing the old token from the local storage
            SessionStorage(ACTION.REMOVE, ITEM.AUTH_TOKEN);
            // updating the local storage with the new refreshed token
            SessionStorage(ACTION.SET, ITEM.AUTH_TOKEN, response.token);
          });
          // token is expired
        } else {
          this.handleExpiredTokenAction();
        }
      });
    }
  };

  render() {
    let accommodationStyles = {
      fontFamily: this.props.accommodations.fontFamily,
      fontSize: this.props.accommodations.fontSize
    };
    if (this.props.accommodations.spacing) {
      accommodationStyles = {
        ...accommodationStyles,
        ...getLineSpacingCSS()
      };
    }

    // Determine if user has already selected a language. Based on local storage, not props
    const isLanguageSelected = SessionStorage(ACTION.GET, ITEM.CAT_LANGUAGE);
    return (
      <div id="AppRoot" onBlur={this.refreshToken} style={accommodationStyles}>
        {!isLanguageSelected && <SelectLanguage initializeLanguage={this.initializeLanguage} />}
        {isLanguageSelected && (
          <div>
            <Helmet>
              <html lang={this.props.currentLanguage} />
            </Helmet>
            <div>
              <Accommodations accommodations={this.props.accommodations} />
              <Router history={history}>
                <div>
                  <Route component={SiteNavBar} />
                  <Switch>
                    <PrivateRoute
                      exact
                      auth={!this.props.authenticated}
                      path={PATH.login}
                      component={Home}
                      redirectTo={this.props.currentHomePage}
                    />
                    <Route
                      path={PATH.websocketConnectionError}
                      component={() => {
                        return (
                          <GenericErrorPage
                            tabTitle={LOCALIZE.titles.websocketConnectionError}
                            title={LOCALIZE.websocketConnectionErrorPage.title}
                            description={<WebsocketConnectionErrorPage />}
                          />
                        );
                      }}
                    />
                    <Route path={PATH.status} component={Status} />
                    <Route path={PATH.sampleTests} component={SampleTestsRoutes} />
                    <Route
                      auth={this.props.authenticated}
                      path={PATH.testBuilderTest}
                      component={() => {
                        return <TestBuilderTestPage />;
                      }}
                    />
                    <Route
                      auth={this.props.authenticated}
                      path={PATH.testBase}
                      component={TestLanding}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={this.props.currentHomePage}
                      component={
                        this.props.isSuperUser || this.props.isEtta
                          ? SystemAdministration
                          : this.props.isPpc
                          ? PpcAdministration
                          : this.props.isScorer
                          ? ScorerRoutes
                          : this.props.isTa
                          ? TestAdministration
                          : Dashboard
                      }
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.isTa && this.props.authenticated}
                      path={PATH.testAdministration}
                      component={TestAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isEtta && this.props.authenticated}
                      path={PATH.systemAdministration}
                      component={SystemAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isPpc && this.props.authenticated}
                      path={PATH.ppcAdministration}
                      component={PpcAdministration}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.isScorer && this.props.authenticated}
                      path={PATH.scorerBase}
                      component={ScorerRoutes}
                      redirectTo={this.props.currentHomePage}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.dashboard}
                      component={Dashboard}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.profile}
                      component={Profile}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.incidentReport}
                      component={IncidentReport}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.testBuilder}
                      component={TestBuilder}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.myTests}
                      component={MyTests}
                      redirectTo={PATH.login}
                    />
                    <PrivateRoute
                      auth={this.props.authenticated}
                      path={PATH.contactUs}
                      component={ContactUs}
                      redirectTo={PATH.login}
                    />

                    <Redirect to={PATH.login} />
                  </Switch>
                </div>
              </Router>
            </div>
          </div>
        )}
        <ExpiredTokenPopop
          showTokenExpiredDialog={this.state.showTokenExpiredDialog}
          closePopupFunction={this.closePopup}
        />
      </div>
    );
  }
}
export { PATH };

const mapStateToProps = (state, ownProps) => {
  return {
    currentLanguage: state.localize.language,
    authenticated: state.login.authenticated,
    testNameId: state.testStatus.currentTestId,
    isSuperUser: state.user.isSuperUser,
    isEtta: state.userPermissions.isEtta,
    isPpc: state.userPermissions.isPpc,
    isTa: state.userPermissions.isTa,
    isScorer: state.userPermissions.isScorer,
    currentHomePage: state.userPermissions.currentHomePage,
    username: state.user.username,
    activeTestPath: state.testStatus.activeTestPath,
    accommodations: state.accommodations
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      authenticateAction,
      logoutAction,
      refreshAuthToken,
      getUserPermissions,
      updatePermissionsState,
      resetTestStatusState,
      resetSampleTestStatusState,
      resetNotepadState,
      resetPermissionsState,
      isTokenStillValid,
      getAssignedTests,
      updateCurrentHomePageState,
      setIsUserLoading,
      resetAccommodations
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(App);
