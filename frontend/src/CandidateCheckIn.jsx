import React, { Component } from "react";
import LOCALIZE from "./text_resources";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PopupBox, { BUTTON_TYPE } from "./components/commons/PopupBox";
import updateCheckInRoom from "./modules/UpdateCheckInRoomRedux";
import CustomButton, { THEME } from "./components/commons/CustomButton";

const styles = {
  checkInBtnContainer: {
    textAlign: "center",
    marginTop: 80
  },
  checkInBtn: {
    minWidth: 400
  },
  textInput: {
    width: 250,
    padding: "3px 6px 3px 6px",
    borderRadius: 4,
    border: "1px solid rgb(0, 86, 94)",
    minHeight: 32
  },
  invalidInput: {
    border: "3px solid #923534"
  },
  errorMessage: {
    color: "#923534",
    fontWeight: "bold",
    padding: 0,
    marginTop: 6,
    width: 350
  },
  popupWidth: {
    width: "100%"
  },
  inputsTable: {
    margin: "18px 0 36px 0",
    width: "100%"
  },
  inputsTableTitles: {
    margin: "24px 24px 0 90px"
  },
  inputsTableTitlesWhenInvalid: {
    margin: "-10px 24px 0 90px"
  },
  inputsTableInputs: {
    float: "right",
    margin: "20px 90px 0 0",
    width: 250
  }
};

class CandidateCheckIn extends Component {
  static propTypes = {
    // Props from Redux
    updateCheckInRoom: PropTypes.func
  };

  state = {
    showCheckInPopup: false,
    testAccessCode: "",
    isInvalidTestAccessCode: false
  };

  handleCheckInPopup = () => {
    this.setState({ ...this.state, showCheckInPopup: true });
  };

  handleCheckin = () => {
    const { testAccessCode } = this.state;

    this.props.updateCheckInRoom(testAccessCode).then(response => {
      if (response.ok) {
        this.setState({
          ...this.state,
          showCheckInPopup: false
        });
        this.props.handleCheckIn(testAccessCode);
      } else {
        this.setState({ ...this.state, isInvalidTestAccessCode: true });
      }
    });
  };

  getTestAccessCode = event => {
    this.setState({ testAccessCode: event.target.value });
  };

  closeDialog = () => {
    this.setState({
      ...this.state,
      testAccessCode: "",
      showCheckInPopup: false,
      isInvalidTestAccessCode: false
    });
  };

  render() {
    return (
      <div id="unit-test-candidate-check-in">
        {this.props.isCheckedIn ? (
          <div id="unit-test-current-room-code">
            <p>
              {LOCALIZE.candidateCheckIn.checkedInText}
              <strong>{this.state.testAccessCode}</strong>
            </p>
          </div>
        ) : this.props.isLoaded ? (
          <div style={styles.checkInBtnContainer} id="unit-test-check-in-button">
            <CustomButton
              label={LOCALIZE.candidateCheckIn.button}
              customStyle={styles.checkInBtn}
              action={this.handleCheckInPopup}
              buttonTheme={THEME.PRIMARY}
            />
          </div>
        ) : (
          <div style={styles.checkInBtnContainer} id="unit-test-check-in-loading-button">
            <CustomButton
              label={LOCALIZE.candidateCheckIn.loading}
              customStyle={styles.checkInBtn}
              buttonTheme={THEME.PRIMARY}
              disabled={true}
            />
          </div>
        )}
        <PopupBox
          show={this.state.showCheckInPopup}
          title={LOCALIZE.candidateCheckIn.popup.title}
          handleClose={() => {}}
          description={
            <div style={styles.popupWidth}>
              <p>{LOCALIZE.candidateCheckIn.popup.description}</p>

              <table style={styles.inputsTable}>
                <tbody>
                  <tr>
                    <th>
                      <div
                        style={
                          this.state.isInvalidTestAccessCode
                            ? styles.inputsTableTitlesWhenInvalid
                            : styles.inputsTableTitles
                        }
                      >
                        <label htmlFor="test-access-code-field">
                          {LOCALIZE.candidateCheckIn.popup.textLabel}
                        </label>
                      </div>
                    </th>
                    <th>
                      <div style={styles.inputsTableInputs}>
                        <input
                          aria-required={"false"}
                          id="test-access-field"
                          type="text"
                          style={
                            this.state.isInvalidTestAccessCode
                              ? { ...styles.textInput, ...styles.invalidInput }
                              : styles.textInput
                          }
                          value={this.state.testAccessCode}
                          onChange={this.getTestAccessCode}
                        />
                        <div>
                          {this.state.isInvalidTestAccessCode && (
                            <label id="invalid-test-access-error" style={styles.errorMessage}>
                              {LOCALIZE.candidateCheckIn.popup.textLabelError}
                            </label>
                          )}
                        </div>
                      </div>
                    </th>
                  </tr>
                </tbody>
              </table>
            </div>
          }
          leftButtonType={BUTTON_TYPE.secondary}
          leftButtonTitle={LOCALIZE.commons.cancel}
          leftButtonAction={this.closeDialog}
          rightButtonType={BUTTON_TYPE.primary}
          rightButtonTitle={LOCALIZE.candidateCheckIn.button}
          rightButtonAction={this.handleCheckin}
        />
      </div>
    );
  }
}

export { CandidateCheckIn as UnconnectedCandidateCheckIn };

const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      updateCheckInRoom
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(CandidateCheckIn);
