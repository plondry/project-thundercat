// Action Types
const SWITCH_TOP_TAB = "navTabs/SWITCH_TOP_TAB";
const SWITCH_SIDE_TAB = "navTabs/SWITCH_SIDE_TAB";
const RESET_STATE = "navTabs/RESET_STATE";
const FULL_RESET_STATE = "RESET_STATE";

// Action Creators
const switchTopTab = key => ({
  type: SWITCH_TOP_TAB,
  key
});
const switchSideTab = key => ({
  type: SWITCH_SIDE_TAB,
  key
});
const resetTopTabsState = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  currentTopTab: null,
  currentSideTab: {}
};

// Reducer
const navTabs = (state = initialState, action) => {
  switch (action.type) {
    case SWITCH_TOP_TAB:
      return {
        ...state,
        currentTopTab: action.key
      };
    case SWITCH_SIDE_TAB:
      var currentTopTab = String(state.currentTopTab);
      var sideTabValue = String(action.key);

      var newCurrentSideTab = state.currentSideTab || {};

      var sideTab = {
        ...newCurrentSideTab[currentTopTab],
        sideTab: sideTabValue
      };
      newCurrentSideTab[currentTopTab] = { ...sideTab };
      return {
        ...state,
        currentSideTab: newCurrentSideTab
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    case FULL_RESET_STATE:
      return {
        ...initialState
      };

    default:
      return state;
  }
};

export default navTabs;
export { initialState, switchTopTab, switchSideTab, resetTopTabsState };
