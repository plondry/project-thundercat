// Action Types
export const SET_COMPLETE_DATE = "date_picker/SET_COMPLETE_DATE";
export const SET_COMPLETE_DATE_VALID_STATE = "date_picker/SET_COMPLETE_DATE_VALID_STATE";
export const RESET_STATE = "date_picker/RESET_STATE";

// update complete date picked (YYYY/MM/DD)
const updateDatePicked = completeDatePicked => ({
  type: SET_COMPLETE_DATE,
  completeDatePicked
});

// update complete DOB value (YYYY/MM/DD)
const updateDatePickedValidState = completeDateValidState => ({
  type: SET_COMPLETE_DATE_VALID_STATE,
  completeDateValidState
});

// reset DOB state
const resetDatePickedStates = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  completeDatePicked: `${null}-${null}-${null}`,
  completeDateValidState: false
};

// Reducer
const datePicker = (state = initialState, action) => {
  switch (action.type) {
    case SET_COMPLETE_DATE:
      return {
        ...state,
        completeDatePicked: action.completeDatePicked
      };
    case SET_COMPLETE_DATE_VALID_STATE:
      return {
        ...state,
        completeDateValidState: action.completeDateValidState
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default datePicker;
export { initialState, updateDatePicked, updateDatePickedValidState, resetDatePickedStates };
