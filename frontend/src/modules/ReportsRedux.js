import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting all TA assigned test order numbers
function getTaAssignedTestOrderNumbers(ta_id) {
  let endpoint = "";
  // ta_id parameter is not provided
  if (!ta_id) {
    endpoint = "/oec-cat/api/get-ta-assigned-test-order-numbers";
  }
  // ta_id is provided
  else {
    endpoint = `/oec-cat/api/get-ta-assigned-test-order-numbers?ta_id=${ta_id}`;
  }
  return async function() {
    let response = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting all existing test order numbers
function getAllExistingTestOrderNumbers() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-all-existing-test-order-numbers", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting all TA assigned test order numbers
function getTestsBasedOnTestOrderNumber(test_order_number) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-tests-based-on-test-order-number?test_order_number=${test_order_number}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting report data (based on test order number, test_id and potentially ta_id and username_id)
// this API can also be used to get all the candidates based on the provided test_order_number and test_ids
function getResultsReportData(test_order_number, test_ids, remove_duplicates, ta_id, username_id) {
  let endpoint = "";
  // checking if ta_id has been provided and not null if so
  const provided_ta_id = ta_id && ta_id !== "" ? true : false;
  // ta_id and username_id parameters are provided
  if (provided_ta_id && username_id) {
    endpoint = `/oec-cat/api/get-results-report-data?test_order_number=${test_order_number}&test_ids=${test_ids}&remove_duplicates=${remove_duplicates}&ta_id=${ta_id}&username_id=${username_id}`;
    // ta_id parameter is provided
  } else if (provided_ta_id && !username_id) {
    endpoint = `/oec-cat/api/get-results-report-data?test_order_number=${test_order_number}&test_ids=${test_ids}&remove_duplicates=${remove_duplicates}&ta_id=${ta_id}`;
    // username_id parameter is provided
  } else if (!provided_ta_id && username_id) {
    endpoint = `/oec-cat/api/get-results-report-data?test_order_number=${test_order_number}&test_ids=${test_ids}&remove_duplicates=${remove_duplicates}&username_id=${username_id}`;
  }
  // ta_is and username_id parameters are not provided
  else {
    endpoint = endpoint = `/oec-cat/api/get-results-report-data?test_order_number=${test_order_number}&test_ids=${test_ids}&remove_duplicates=${remove_duplicates}`;
  }
  return async function() {
    let response = await fetch(endpoint, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting financial report data
function getFinancialReportData(test_order_number) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-financial-report-data?test_order_number=${test_order_number}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

export default getTaAssignedTestOrderNumbers;
export {
  getAllExistingTestOrderNumbers,
  getTestsBasedOnTestOrderNumber,
  getResultsReportData,
  getFinancialReportData
};
