// Action Types
const LOAD_ANSWERS = "emailInbox/LOAD_ANSWERS";
const ADD_EMAIL_RESPONSE = "emailInbox/ADD_EMAIL_RESPONSE";
const UPDATE_EMAIL_RESPONSE = "emailInbox/UPDATE_EMAIL_RESPONSE";
const DELETE_EMAIL_RESPONSE = "emailInbox/DELETE_EMAIL_RESPONSE";
const ADD_TASK_RESPONSE = "emailInbox/ADD_TASK_RESPONSE";
const UPDATE_TASK_RESPONSE = "emailInbox/UPDATE_TASK_RESPONSE";
const DELETE_TASK_RESPONSE = "emailInbox/DELETE_TASK_RESPONSE";
const RESET_STATE = "emailInbox/RESET_STATE";

// Action Creators

const loadInboxAnswers = answers => ({ type: LOAD_ANSWERS, answers });
const addEmailResponse = (questionId, emailResponseId, emailResponse) => ({
  type: ADD_EMAIL_RESPONSE,
  questionId,
  emailResponseId,
  emailResponse
});
const updateEmailResponse = (questionId, emailResponseId, taskResponse) => ({
  type: UPDATE_EMAIL_RESPONSE,
  questionId,
  emailResponseId,
  taskResponse
});
const deleteEmailResponse = (questionId, answerId) => ({
  type: DELETE_EMAIL_RESPONSE,
  questionId,
  answerId
});
const addTaskResponse = (questionId, emailResponseId, taskResponse) => ({
  type: ADD_TASK_RESPONSE,
  questionId,
  emailResponseId,
  taskResponse
});
const updateTaskResponse = (questionId, emailResponseId, taskResponse) => ({
  type: UPDATE_TASK_RESPONSE,
  questionId,
  emailResponseId,
  taskResponse
});
const deleteTaskResponse = (questionId, answerId) => ({
  type: DELETE_TASK_RESPONSE,
  questionId,
  answerId
});

const resetInboxState = () => ({
  type: RESET_STATE
});

// Initial State
const initialState = {
  emailResponseId: 0,
  emailResponses: [],
  taskResponsesId: 0,
  taskResponses: []
};

// Reducer
const emailInbox = (state = initialState, action) => {
  let questionId = Number(action.questionId);
  let answerId = Number(action.emailResponseId);
  let newEntry = {};

  switch (action.type) {
    case LOAD_ANSWERS:
      let newEmailResponses = [];
      let newTaskResponses = [];
      let answerIdArray = [0];

      for (let question of action.answers) {
        questionId = question["question_id"];
        for (let answer of question["email_answers"]) {
          newEmailResponses.push({
            questionId: questionId,
            emailType: answer["email_type"],
            emailBody: answer["email_body"],
            reasonsForAction: answer["reasons_for_action"],
            answerId: answer["answer_id"],
            emailTo: answer["email_to"].map(contactId => {
              return { id: contactId };
            }),
            emailCc: answer["email_cc"].map(contactId => {
              return { id: contactId };
            })
          });

          answerIdArray.push(answer["answer_id"]);
        }

        for (let answer of question["task_answers"]) {
          newTaskResponses.push({
            questionId: questionId,
            task: answer["task"],
            reasonsForAction: answer["reasons_for_action"],
            answerId: answer["answer_id"]
          });

          answerIdArray.push(answer["answer_id"]);
        }
      }

      return {
        ...state,
        emailResponses: newEmailResponses,
        taskResponses: newTaskResponses,
        emailResponseId: Math.max(...answerIdArray) + 1
      };

    case ADD_EMAIL_RESPONSE:
      newEntry = { ...action.emailResponse, questionId: questionId, answerId: answerId };

      return {
        ...state,
        emailResponses: [...state.emailResponses, newEntry],
        emailResponseId: state.emailResponseId + 1
      };

    case UPDATE_EMAIL_RESPONSE:
      newEntry = { ...action.taskResponse, questionId: questionId, answerId: answerId };

      return {
        ...state,
        emailResponses: [...state.emailResponses].map(response => {
          if (response.answerId === answerId && response.questionId === questionId) {
            return newEntry;
          } else {
            return response;
          }
        }),
        emailResponseId: state.emailResponseId + 1
      };

    case DELETE_EMAIL_RESPONSE:
      answerId = Number(action.answerId);

      return {
        ...state,
        emailResponses: [...state.emailResponses].filter(x => x.answerId !== answerId)
      };

    case ADD_TASK_RESPONSE:
      newEntry = { ...action.taskResponse, questionId: questionId, answerId: answerId };

      return {
        ...state,
        taskResponses: [...state.taskResponses, newEntry],
        emailResponseId: state.emailResponseId + 1
      };

    case UPDATE_TASK_RESPONSE:
      newEntry = { ...action.taskResponse, questionId: questionId, answerId: answerId };

      return {
        ...state,
        taskResponses: [...state.taskResponses].map(response => {
          if (response.answerId === answerId && response.questionId === questionId) {
            return newEntry;
          } else {
            return response;
          }
        }),
        emailResponseId: state.emailResponseId + 1
      };

    case DELETE_TASK_RESPONSE:
      answerId = Number(action.answerId);

      return {
        ...state,
        taskResponses: [...state.taskResponses].filter(x => x.answerId !== answerId)
      };

    case RESET_STATE:
      return {
        ...initialState
      };

    default:
      return {
        ...state
      };
  }
};

export default emailInbox;
export {
  loadInboxAnswers,
  addEmailResponse,
  updateEmailResponse,
  deleteEmailResponse,
  addTaskResponse,
  updateTaskResponse,
  deleteTaskResponse,
  resetInboxState
};
