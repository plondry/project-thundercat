import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting all tests where "is_public" is set to false
function getActiveNonPublicTests() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-active-non-public-tests", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

export default getActiveNonPublicTests;
