import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// scoring UIT test section
function scoreUitTestSection(assigned_test_id, test_section_id) {
  let requestObj = {
    assigned_test_id: assigned_test_id,
    test_section_id: test_section_id
  };
  return async function() {
    let response = await fetch(`/oec-cat/api/uit-test-section-scoring/`, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        cache: "default",
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(requestObj)
    });
    // successful request
    if (response.status === 200) {
      let responseJson = await response.json();
      return responseJson;
      //  there is an error
    } else {
      return response;
    }
  };
}

// scoring UIT test (whole test)
function scoreUitTest(assigned_test_id) {
  let requestObj = {
    assigned_test_id: assigned_test_id
  };
  return async function() {
    let response = await fetch(
      `/oec-cat/api/uit-score-test/?assigned_test_id=${assigned_test_id}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          cache: "default",
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify(requestObj)
      }
    );
    // successful request
    if (response.status === 200) {
      let responseJson = await response.json();
      return responseJson;
      //  there is an error
    } else {
      return response;
    }
  };
}

export default scoreUitTestSection;
export { scoreUitTest };
