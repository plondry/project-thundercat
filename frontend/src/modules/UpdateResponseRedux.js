import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

function seenUitQuestion(assignedTestId, questionId) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/uit-seen-question/?assigned_test_id=${assignedTestId}&question_id=${questionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

function saveUitTestResponse(assignedTestId, questionId, answerId) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/uit-save-answer/?assigned_test_id=${assignedTestId}&question_id=${questionId}&answer_id=${answerId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

function getTestResponses(assigned_test_id, test_section_component_id) {
  return async function() {
    let url = `/oec-cat/api/get-test-answers/?assigned_test_id=${assigned_test_id}&test_section_component_id=${test_section_component_id}`;
    let tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let response = {
      body: await tests.json(),
      ok: await tests.ok
    };
    return response;
  };
}

//New Email Inbox functions
// if you update these statuses, don't forget to also update
// "UpdateEmailAnswerType" in ".../backend/static/update_email_answer_type.py" file (backend)
export const UPDATE_EMAIL_ACTIONS = {
  ADD_EMAIL: 1,
  UPDATE_EMAIL: 2,
  DELETE_EMAIL: 3,
  ADD_TASK: 4,
  UPDATE_TASK: 5,
  DELETE_TASK: 6
};

function updateEmailTestResponse(OPERATION, assignedTestId, questionId, answerId, answerObj = {}) {
  let newObj = {
    answerObj,
    assignedTestId,
    questionId,
    answerId,
    OPERATION
  };

  return async function() {
    let response = await fetch(`/oec-cat/api/update-email-answer/`, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

export { seenUitQuestion, saveUitTestResponse, getTestResponses, updateEmailTestResponse };
