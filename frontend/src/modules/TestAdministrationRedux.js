import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_TEST_ORDER_NUMBER_SELECTED_VALUE =
  "testAdministration/SET_TEST_ORDER_NUMBER_SELECTED_VALUE";
export const SET_TEST_TO_ADMINISTER_SELECTED_VALUE =
  "testAdministration/SET_TEST_TO_ADMINISTER_SELECTED_VALUE";
export const SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED =
  "testAdministration/SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED";
export const SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE =
  "testAdministration/SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE";
export const SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED =
  "testAdministration/SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED";
export const SET_GENERATE_BUTTON_DISABLED_STATE =
  "testAdministration/SET_GENERATE_BUTTON_DISABLED_STATE";
export const SET_POPUP_OVERFLOW_VISIBLE_STATE =
  "testAdministration/SET_POPUP_OVERFLOW_VISIBLE_STATE";
const RESET_STATE = "testAdministration/RESET_STATE";

// update test order number state
const updateTestOrderNumberState = testOrderNumber => ({
  type: SET_TEST_ORDER_NUMBER_SELECTED_VALUE,
  testOrderNumber
});

// update test to administer state
const updateTestToAdministerState = testToAdminister => ({
  type: SET_TEST_TO_ADMINISTER_SELECTED_VALUE,
  testToAdminister
});

// update test to administer dropwdown disabled state
const updateTestToAdministerDropdownDisabledState = testToAdministerDropdownDisabled => ({
  type: SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED,
  testToAdministerDropdownDisabled
});

// update test session language state
const updateTestSessionLanguageState = testSessionLanguage => ({
  type: SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE,
  testSessionLanguage
});

// update test session language dropwdown disabled state
const updateTestSessionLanguageDropdownDisabledState = testSessionLanguageDropdownDisabled => ({
  type: SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED,
  testSessionLanguageDropdownDisabled
});

// update generate button disabled state
const updateGenerateButtonDisabledState = generateButtonDisabled => ({
  type: SET_GENERATE_BUTTON_DISABLED_STATE,
  generateButtonDisabled
});

// update popup overflow visible state
const updatePopupOverflowVisibleState = popupOverflowVisible => ({
  type: SET_POPUP_OVERFLOW_VISIBLE_STATE,
  popupOverflowVisible
});

// reset states
const resetTestAdministrationStates = () => ({
  type: RESET_STATE
});

// gets randomly generated test access code
function getNewTestAccessCode(testOrderNumber, testSessionLanguage, test) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-new-test-access-code/?test_order_number=${testOrderNumber}&test_session_language=${testSessionLanguage}&test_id=${test}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// delete test access code
function deleteTestAccessCode(testAccessCode) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/delete-test-access-code/?test_access_code=${testAccessCode}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// get active test access code data for a specified TA username
function getActiveTestAccessCodesRedux() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-active-test-access-codes/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// get assigned candidates for a specified TA username
function getTestAdministratorAssignedCandidates() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-test-administrator-assigned-candidates/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// updating test time (useful for TAs to allow them to customize test time for a specific candidate)
const updateTestTime = (assignedTestSectionId, testTime) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/update-test-time/?assigned_test_section_id=${assignedTestSectionId}&test_time=${testTime}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
};

// approving candidate
const approveCandidate = (username, test) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/approve-candidate/?username_id=${username}&test_id=${test}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// re-sync candidate
const reSyncCandidate = (username, test, testAccessCode) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/re-sync-candidate/?username_id=${username}&test_id=${test}&test_access_code=${testAccessCode}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// locking single candidate's test
const lockCandidateTest = (username, test, testSectionId) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/lock-candidate-test/?username_id=${username}&test_id=${test}&test_section_id=${testSectionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// locking all candidates test
const lockAllCandidatesTest = () => {
  return async function() {
    let response = await fetch("/oec-cat/api/lock-all-candidates-test/", {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
};

// unlocking single candidate's test
const unlockCandidateTest = (username, test, testSectionId) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/unlock-candidate-test/?username_id=${username}&test_id=${test}&test_section_id=${testSectionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// unlocking all candidates test
const unlockAllCandidatesTest = () => {
  return async function() {
    let response = await fetch("/oec-cat/api/unlock-all-candidates-test/", {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
};

// pausing candidate's test
const pauseCandidateTest = (username, test, testSectionId, pauseTestTime) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/pause-candidate-test/?username_id=${username}&test_id=${test}&pause_test_time=${pauseTestTime}&test_section_id=${testSectionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// unpausing candidate's test
const unpauseCandidateTest = (username, test, testSectionId) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/unpause-candidate-test/?username_id=${username}&test_id=${test}&test_section_id=${testSectionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// un-assign candidate's test
const unAssignCandidate = (username, test) => {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/un-assign-candidate/?username_id=${username}&test_id=${test}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
};

// Initial State
const initialState = {
  testOrderNumber: { label: "", value: "" },
  testToAdminister: { label: "", value: "" },
  testToAdministerDropdownDisabled: true,
  testSessionLanguage: { label: "", value: "" },
  testSessionLanguageDropdownDisabled: true,
  popupOverflowVisible: false,
  generateButtonDisabled: true
};

const testAdministration = (state = initialState, action) => {
  switch (action.type) {
    case SET_TEST_ORDER_NUMBER_SELECTED_VALUE:
      return {
        ...state,
        testOrderNumber: action.testOrderNumber
      };
    case SET_TEST_TO_ADMINISTER_SELECTED_VALUE:
      return {
        ...state,
        testToAdminister: action.testToAdminister
      };
    case SET_TEST_TO_ADMINISTER_DROPDOWN_DISABLED:
      return {
        ...state,
        testToAdministerDropdownDisabled: action.testToAdministerDropdownDisabled
      };
    case SET_TEST_SESSION_LANGUAGE_SELECTED_VALUE:
      return {
        ...state,
        testSessionLanguage: action.testSessionLanguage
      };
    case SET_TEST_SESSION_LANGUAGE_DROPDOWN_DISABLED:
      return {
        ...state,
        testSessionLanguageDropdownDisabled: action.testSessionLanguageDropdownDisabled
      };
    case SET_GENERATE_BUTTON_DISABLED_STATE:
      return {
        ...state,
        generateButtonDisabled: action.generateButtonDisabled
      };
    case SET_POPUP_OVERFLOW_VISIBLE_STATE:
      return {
        ...state,
        popupOverflowVisible: action.popupOverflowVisible
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default testAdministration;
export {
  initialState,
  getNewTestAccessCode,
  updateTestOrderNumberState,
  updateTestToAdministerState,
  updateTestToAdministerDropdownDisabledState,
  updateTestSessionLanguageState,
  updateTestSessionLanguageDropdownDisabledState,
  updateGenerateButtonDisabledState,
  updatePopupOverflowVisibleState,
  resetTestAdministrationStates,
  deleteTestAccessCode,
  getActiveTestAccessCodesRedux,
  getTestAdministratorAssignedCandidates,
  updateTestTime,
  approveCandidate,
  reSyncCandidate,
  lockCandidateTest,
  lockAllCandidatesTest,
  unlockCandidateTest,
  unlockAllCandidatesTest,
  pauseCandidateTest,
  unpauseCandidateTest,
  unAssignCandidate
};
