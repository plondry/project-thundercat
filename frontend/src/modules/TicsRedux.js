import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting tics data
function getTicsData(orderNumber) {
  return async function() {
    let response = await fetch(`/oec-cat/api/get-tics-data/?order_number=${orderNumber}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    // successful request
    if (response.status === 200) {
      let responseJson = await response.json();
      return responseJson;
      //  there is an error or there is no result found
    } else {
      return response;
    }
  };
}

export default getTicsData;
