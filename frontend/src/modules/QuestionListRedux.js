import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const READ_QUESTION = "questionList/READ_QUESTION";
const MARK_QUESTION = "questionList/MARK_QUESTION";
const LOAD_ANSWERS = "questionList/LOAD_ANSWERS";
const ANSWER_QUESTION = "questionList/ANSWER_QUESTION";
const SET_START_TIME = "questionList/SET_START_TIME";
const SET_TRIGGER_TIME_SPENT_CALCULATION = "questionList/SET_TRIGGER_TIME_SPENT_CALCULATION";
const RESET_STATE = "questionList/RESET_STATE";

const CHANGE_CURRENT_QUESTION = "questionList/CHANGE_CURRENT_QUESTION";

// Action Creators

const loadAnswers = answers => ({ type: LOAD_ANSWERS, answers });
const readQuestion = questionId => ({ type: READ_QUESTION, questionId });
const answerQuestion = (questionId, answerId) => ({ type: ANSWER_QUESTION, questionId, answerId });

// emailIndex refers to the index of the currently visible email
const changeCurrentQuestion = questionId => ({
  type: CHANGE_CURRENT_QUESTION,
  questionId
});

// trigger time spent calculation
const triggerTimeSpentCalculation = timeSpentTrigger => ({
  type: SET_TRIGGER_TIME_SPENT_CALCULATION,
  timeSpentTrigger
});

// setting start time of current question
const setStartTime = startTime => ({
  type: SET_START_TIME,
  startTime
});

const markForReview = questionId => ({
  type: MARK_QUESTION,
  questionId
});
const resetQuestionListState = () => ({
  type: RESET_STATE
});

// updating time spent of a specified question
function updateTimeSpent(assigned_test_id, question_id, test_section_component_id, time) {
  let url = `/oec-cat/api/update-time-spent?assigned_test_id=${assigned_test_id}&question_id=${question_id}&test_section_component_id=${test_section_component_id}&time=${time}`;

  return async function() {
    let response = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        cache: "default"
      }
    });
    return response;
  };
}

// Initial State
// emails - represents an array of emailShape objects in the currently selected language.
// emailSummaries - represents an array of objects indicating read state of each email.
// emailActions - represents an array of arrays, each array contains actionShape objects, representing an ACTION_TYPE.
const initialState = {
  assignedQuestionIds: [],
  isTestLoadedFromDB: false,
  isContentLoaded: false,
  // new vars
  answers: [],
  readQuestions: [],
  questionSummaries: {},
  timeSpentTrigger: false,
  startTime: null
};

// Reducer
const questionList = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_ANSWERS:
      var newQuestionSummaries = {};
      var answers = action.answers;
      for (let i = 0; i < answers.length; i++) {
        newQuestionSummaries[answers[i].question_id] = {
          ...state.questionSummaries[answers[i].question_id],
          isRead: true,
          answer: answers[i].answer_id
        };
      }
      // preventing empty questionSummaries state
      if (answers.length > 0) {
        return {
          ...state,
          questionSummaries: newQuestionSummaries
        };
      } else {
        return {
          ...state
        };
      }
    case READ_QUESTION:
      var readQuestionId = Number(action.questionId);
      var newReadQuestionSummaries = state.questionSummaries || {};
      var readQuestion = {
        ...newReadQuestionSummaries[readQuestionId],
        isRead: true
      };
      newReadQuestionSummaries[readQuestionId] = { ...readQuestion };
      return {
        ...state,
        questionSummaries: newReadQuestionSummaries
      };
    case MARK_QUESTION:
      var markQuestionId = Number(action.questionId);
      var newMarkQuestionSummaries = state.questionSummaries || {};
      var markQuestion = {
        ...newMarkQuestionSummaries[markQuestionId],
        review: newMarkQuestionSummaries[markQuestionId].review === true ? false : true
      };

      newMarkQuestionSummaries[markQuestionId] = { ...markQuestion };
      return {
        ...state,
        questionSummaries: newMarkQuestionSummaries
      };
    case ANSWER_QUESTION:
      var answerQuestionId = Number(action.questionId);
      var answerId = Number(action.answerId);

      var newAnswerQuestionSummaries = state.questionSummaries || {};

      var answerQuestion = {
        ...newAnswerQuestionSummaries[answerQuestionId],
        answer: answerId
      };
      newAnswerQuestionSummaries[answerQuestionId] = { ...answerQuestion };
      return {
        ...state,
        questionSummaries: newAnswerQuestionSummaries
      };
    case CHANGE_CURRENT_QUESTION:
      return {
        ...state,
        currentQuestion: action.questionId
      };
    case SET_TRIGGER_TIME_SPENT_CALCULATION:
      return {
        ...state,
        timeSpentTrigger: action.timeSpentTrigger
      };
    case SET_START_TIME:
      return {
        ...state,
        startTime: action.startTime
      };
    case RESET_STATE:
      return {
        ...initialState
      };

    default:
      return state;
  }
};

export default questionList;
export {
  initialState,
  readQuestion,
  loadAnswers,
  answerQuestion,
  changeCurrentQuestion,
  markForReview,
  updateTimeSpent,
  triggerTimeSpentCalculation,
  setStartTime,
  resetQuestionListState
};
