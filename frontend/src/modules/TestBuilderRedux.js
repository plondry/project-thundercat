import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

function tryTestSection(order, newTestDef) {
  let newObj = {
    order: order,
    new_test: newTestDef
  };

  return async function() {
    let response = await fetch(`/oec-cat/api/try-test-section/`, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function uploadTestJSON(newTestDef) {
  return async function() {
    let response = await fetch(`/oec-cat/api/upload-test/`, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: newTestDef
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getTestDefinitionLabels() {
  return async function() {
    let response = await fetch(`/oec-cat/api/get-test-definition-labels/`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function activateTestDefinition(test_definition, active) {
  let newObj = {
    test_definition,
    active
  };

  return async function() {
    let response = await fetch(`/oec-cat/api/test-definition`, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function getTestDefinitionExtract(test) {
  return async function() {
    let response = await fetch(`/oec-cat/api/get-test-extract?test_definition=${test}`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

function uploadTest(newTestDef) {
  let newObj = {
    new_test: newTestDef
  };

  return async function() {
    let response = await fetch(`/oec-cat/api/upload-test`, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(newObj)
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// constant strings for the python data object
export const TEST_DEFINITION = "test_definition";
export const TEST_SECTIONS = "test_sections";
export const NEXT_SECTION_BUTTONS = "next_section_buttons";
export const TEST_SECTION_COMPONENTS = "test_section_components";
export const SECTION_COMPONENT_PAGES = "section_component_pages";
export const PAGE_SECTIONS = "page_sections";
export const PAGE_SECTION_DEFINITIONS = "page_section_definitions";
export const ADDRESS_BOOK = "address_book";
export const CONTACT_DETAILS = "contact_details";
export const QUESTIONS = "questions";
export const EMAILS = "emails";
export const EMAIL_DETAILS = "email_details";
export const QUESTION_SECTIONS = "question_sections";
export const QUESTION_SECTION_DEFINITIONS = "question_section_definitions";
export const ANSWERS = "answers";
export const ANSWER_DETAILS = "answer_details";
export const MULTIPLE_CHOICE_QUESTION_DETAILS = "multiple_choice_question_details";
export const COMPETENCY_TYPES = "competency_types";
export const QUESTION_BLOCK_TYPES = "question_block_types";
export const QUESTION_LIST_RULES = "question_list_rules";
export const QUESTION_SITUATIONS = "question_situations";
export const SITUATION_EXAMPLE_RATINGS = "situation_example_ratings";
export const SITUATION_EXAMPLE_RATING_DETAILS = "situation_example_rating_details";

// Action Types
const LOAD_DEFINITION = "testBuilder/LOAD_DEFINITION";
const MODIFY_FIELD = "testBuilder/MODIFY_FIELD";
const ADD_FIELD = "testBuilder/ADD_FIELD";
const REPLACE_FIELD = "testBuilder/REPLACE_FIELD";
const SET_TEST_SECTION_TO_VIEW = "testBuilder/SET_TEST_SECTION_TO_VIEW";
const RESET_STATE = "testBuilder/RESET_STATE";

// Action Creators
const loadTestDefinition = state => ({ type: LOAD_DEFINITION, state });
const modifyTestDefinitionField = (rootObj, newField, id, reorderBy = undefined) => ({
  type: MODIFY_FIELD,
  rootObj,
  newField,
  id,
  reorderBy
});
const addTestDefinitionField = (rootObj, newField) => ({
  type: ADD_FIELD,
  rootObj,
  newField
});
const replaceTestDefinitionField = (rootObj, newField) => ({
  type: REPLACE_FIELD,
  rootObj,
  newField
});

const setTestSectionToView = order => ({
  type: SET_TEST_SECTION_TO_VIEW,
  order
});
const resetTestBuilderState = () => ({
  type: RESET_STATE
});

const sortByOrder = (a, b) => (a.order > b.order ? 1 : -1);
export const sortById = (a, b) => (a.id > b.id ? 1 : -1);
const sortByEmailId = (a, b) => (a.email_id > b.email_id ? 1 : -1);

// Reducer
const testBuilder = (state = emptyState, action) => {
  let newState = {
    ...state
  };
  let newStateArray = [];

  switch (action.type) {
    case LOAD_DEFINITION:
      return {
        ...action.state
      };

    case MODIFY_FIELD:
      newStateArray = [];
      newState = {
        ...state
      };
      let modified = false;
      newStateArray = newState[`${action.rootObj}`].map(content => {
        if (content.id === action.id) {
          modified = true;
          return action.newField;
        }
        return content;
      });
      if (!modified) {
        newStateArray.push(action.newField);
      }

      // sort by email id to keep questions ordered
      if (action.rootObj === EMAILS) {
        newStateArray.sort(sortByEmailId);
      } else if (action.newField.order !== undefined) {
        newStateArray.sort(sortByOrder);
      }

      //re-order based on obj property
      if (action.reorderBy !== undefined) {
        let reorderState = {};
        for (let content of newStateArray) {
          if (reorderState[content[`${action.reorderBy}`]] === undefined) {
            reorderState[content[`${action.reorderBy}`]] = [content];
          } else {
            reorderState[content[`${action.reorderBy}`]].push(content);
          }
        }

        let newArr = [];
        Object.keys(reorderState).map(key => {
          reorderState[key].sort(sortByOrder).map((obj, index) => {
            newArr.push({ ...obj, order: index + 1 });
            return null;
          });
          return null;
        });
        newStateArray = newArr;
      }
      // end re-ordering

      newState[`${action.rootObj}`] = newStateArray;
      return {
        ...newState
      };

    case ADD_FIELD:
      newStateArray = [];
      newState = {
        ...state
      };
      newStateArray = newState[`${action.rootObj}`];
      newStateArray.push(action.newField);

      newStateArray.sort(sortByOrder);
      // trigger new state
      let newArray = newStateArray.map(x => {
        return x;
      });
      newState[`${action.rootObj}`] = newArray;
      return {
        ...newState
      };

    case REPLACE_FIELD:
      newStateArray = action.newField;
      newState = {
        ...state
      };
      newStateArray.sort(sortByOrder);
      newState[`${action.rootObj}`] = newStateArray;
      return {
        ...newState
      };

    case SET_TEST_SECTION_TO_VIEW:
      return {
        ...state,
        viewTestSectionOrderNumber: action.order
      };

    case RESET_STATE:
      return {
        ...emptyState
      };

    default:
      return {
        ...state
      };
  }
};

export default testBuilder;
export {
  loadTestDefinition,
  modifyTestDefinitionField,
  resetTestBuilderState,
  addTestDefinitionField,
  setTestSectionToView,
  replaceTestDefinitionField,
  //oec-cat/api
  tryTestSection,
  uploadTest,
  getTestDefinitionLabels,
  getTestDefinitionExtract,
  activateTestDefinition,
  uploadTestJSON
};

export const emptyState = {
  test_definition: [
    {
      id: 1,
      test_code: "TEMPLATE001",
      version: 1,
      en_name: "template test",
      fr_name: "FR template test",
      is_public: true,
      active: false,
      test_language: null
    }
  ],
  test_sections: [],
  next_section_buttons: [],
  test_section_components: [],
  section_component_pages: [],
  page_sections: [],
  page_section_definitions: [],
  address_book: [],
  address_book_contacts: [],
  questions: [],
  question_list_rules: [],
  question_block_types: [],
  question_sections: [],
  question_section_definitions: [],
  emails: [],
  email_details: [],
  contact_details: [],
  answers: [],
  answer_details: [],
  multiple_choice_question_details: [],
  competency_types: [],
  question_situations: [],
  situation_example_ratings: [],
  situation_example_rating_details: []
};
