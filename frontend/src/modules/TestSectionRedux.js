import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// getting the test section content based on the id
const getTestSection = (assignedTestId, order = -1, specialSection = "", testId) => {
  return async function() {
    let headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      cache: "default"
    };
    if (SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN)) {
      headers.Authorization = "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN);
    }
    const url =
      `/oec-cat/api/get-test-section?assigned_test_id=${assignedTestId}` +
      `&test_id=${testId}` +
      (order !== -1 ? `&order=${order}` : ``) +
      (specialSection !== "" ? `&special_section=${specialSection}` : ``);
    let response = await fetch(url, {
      method: "GET",
      headers: headers
    });
    let testSection = await response.json();
    return testSection;
  };
};

// getting updated test start time after lock/pause actions
function getUpdatedTestStartTime(assigned_test_id, test_section_id) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-updated-time-after-lock-pause-actions/?assigned_test_id=${assigned_test_id}&test_section_id=${test_section_id}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

//Action creators
const UPDATE_TEST_SECTION = "testSection/UPDATE_TEST_SECTION";
const SET_CONTENT_LOADED = "testSection/SET_CONTENT_LOADED";
const SET_SCORER_CONTENT_LOADED = "testSection/SET_SCORER_CONTENT_LOADED";
const SET_CONTENT_UNLOADED = "testSection/SET_CONTENT_UNLOADED";
const RESET_STATE = "testSection/RESET_STATE";
const SET_TEST_HEIGHT = "testSection/SET_TEST_HEIGHT";
const SET_START_TIME = "testSection/SET_START_TIME";
const SET_CHEATING_ATTEMPTS = "testSection/SET_CHEATING_ATTEMPTS";

const updateTestSection = testSection => ({
  type: UPDATE_TEST_SECTION,
  testSection
});
const setContentLoaded = () => ({
  type: SET_CONTENT_LOADED
});
const setScorerContentLoaded = () => ({
  type: SET_SCORER_CONTENT_LOADED
});
const setContentUnLoaded = () => ({
  type: SET_CONTENT_UNLOADED
});
const setStartTime = startTime => ({
  type: SET_START_TIME,
  startTime
});
const updateCheatingAttempts = cheatingAttempts => ({
  type: SET_CHEATING_ATTEMPTS,
  cheatingAttempts
});
const resetTestFactoryState = () => ({
  type: RESET_STATE
});
const setTestHeight = height => ({
  type: SET_TEST_HEIGHT,
  height
});

const initialState = {
  testSection: {},
  isLoaded: false,
  isScorerLoaded: false,
  startTime: "",
  testHeight: 0,
  cheatingAttempts: 0,
  defaultTab: null,
  readOnly: false
};

// Reducer
const testSection = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_TEST_SECTION:
      return {
        ...state,
        testSection: action.testSection,
        readOnly: action.testSection.read_only || false
      };
    case SET_CONTENT_LOADED:
      return {
        ...state,
        isLoaded: true
      };
    case SET_SCORER_CONTENT_LOADED:
      return {
        ...state,
        isScorerLoaded: true
      };
    case SET_CONTENT_UNLOADED:
      return {
        ...state,
        isLoaded: false,
        isScorerLoaded: false
      };
    case SET_START_TIME:
      return {
        ...state,
        startTime: action.startTime
      };
    case SET_CHEATING_ATTEMPTS:
      return {
        ...state,
        cheatingAttempts: action.cheatingAttempts
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    case SET_TEST_HEIGHT:
      return {
        ...state,
        testHeight: action.height
      };
    default:
      return state;
  }
};

export default testSection;
export {
  getTestSection,
  updateTestSection,
  resetTestFactoryState,
  setStartTime,
  setContentLoaded,
  setScorerContentLoaded,
  setContentUnLoaded,
  setTestHeight,
  updateCheatingAttempts,
  getUpdatedTestStartTime
};
