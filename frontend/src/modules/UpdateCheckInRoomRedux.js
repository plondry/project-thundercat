import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

//pass in strict api urls to update the test status
function updateCheckInRoom(testAccessCode) {
  let url = `/oec-cat/api/room-check-in/?test_access_code=${testAccessCode}`;

  return async function() {
    let tests = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        cache: "default"
      }
    });
    return await tests;
  };
}

export default updateCheckInRoom;
