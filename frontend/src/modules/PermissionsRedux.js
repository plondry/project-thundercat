import { PATH } from "../components/commons/Constants";
import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
// Permissions
export const SET_PERMISSIONS = "permissions/SET_PERMISSIONS";
// Home Page
export const SET_CURRENT_HOME_PAGE = "permissions/SET_CURRENT_HOME_PAGE";
// Active Permissions Pagination
export const SET_PERMISSIONS_PAGINATION_PAGE = "permissions/SET_PERMISSIONS_PAGINATION_PAGE";
export const SET_PERMISSIONS_PAGINATION_PAGE_SIZE =
  "permissions/SET_PERMISSIONS_PAGINATION_PAGE_SIZE";
export const SET_PERMISSIONS_SEARCH_PARAMETERS = "permissions/SET_PERMISSIONS_SEARCH_PARAMETERS";
// Active Test Permissions Pagination
export const SET_TEST_PERMISSIONS_PAGINATION_PAGE =
  "permissions/SET_TEST_PERMISSIONS_PAGINATION_PAGE";
export const SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE =
  "permissions/SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE";
export const SET_TEST_PERMISSIONS_SEARCH_PARAMETERS =
  "permissions/SET_TEST_PERMISSIONS_SEARCH_PARAMETERS";
export const SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS =
  "permissions/SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS";
const RESET_STATE = "permissions/SET_IS_TEST_ADMINISTRATOR";

// update permissions state
const updatePermissionsState = permissions => ({
  type: SET_PERMISSIONS,
  permissions
});
// update current home page state
const updateCurrentHomePageState = currentHomePage => ({
  type: SET_CURRENT_HOME_PAGE,
  currentHomePage
});
// update pagination page state (active permissions)
const updateCurrentPermissionsPageState = permissionsPaginationPage => ({
  type: SET_PERMISSIONS_PAGINATION_PAGE,
  permissionsPaginationPage
});
// update pagination pageSize state (active permissions)
const updatePermissionsPageSizeState = permissionsPaginationPageSize => ({
  type: SET_PERMISSIONS_PAGINATION_PAGE_SIZE,
  permissionsPaginationPageSize
});
// update search keyword state (active permissions)
const updateSearchActivePermissionsStates = (permissionsKeyword, permissionsActiveSearch) => ({
  type: SET_PERMISSIONS_SEARCH_PARAMETERS,
  permissionsKeyword,
  permissionsActiveSearch
});
// update pagination page state (active test permissions)
const updateCurrentTestPermissionsPageState = testPermissionsPaginationPage => ({
  type: SET_TEST_PERMISSIONS_PAGINATION_PAGE,
  testPermissionsPaginationPage
});
// update pagination pageSize state (active test permissions)
const updateTestPermissionsPageSizeState = testPermissionsPaginationPageSize => ({
  type: SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE,
  testPermissionsPaginationPageSize
});
// update search keyword state (active test permissions)
const updateSearchActiveTestPermissionsStates = (
  testPermissionsKeyword,
  testPermissionsActiveSearch
) => ({
  type: SET_TEST_PERMISSIONS_SEARCH_PARAMETERS,
  testPermissionsKeyword,
  testPermissionsActiveSearch
});
// update triggerPopulateTestPermissions state (active test permissions)
const updateTriggerPopulateTestPermissionsState = triggerPopulateTestPermissions => ({
  type: SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS,
  triggerPopulateTestPermissions
});
const resetPermissionsState = () => ({
  type: RESET_STATE
});

// get the test definitions with versions
function getTestDefinitionVersionsCollected() {
  return async function() {
    let response = await fetch(`/oec-cat/api/get-test-definition-versions-collected/`, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseObj = {
      body: await response.json(),
      ok: response.ok
    };
    return responseObj;
  };
}

// get permissions (all existing permissions)
function getExistingPermissions() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-permissions/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// get user permissions based on the username
function getUserPermissions() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-user-permissions/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting specified user'pending permissions
function getUserPendingPermissions() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-user-pending-permissions/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting all pending permissions
function getPendingPermissions() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-pending-permissions", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// getting available permissions for permission requests
// only getting permissions that are not part or specified user' permissions and user' pending permissions
function getAvailablePermissions() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-available-permissions/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// grant permission to user
function grantPermission(username, permissionId) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/grant-permission/?username=${username}&permission_id=${permissionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// deny requested permission
function denyPermission(permissionRequestId) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/deny-permission/?permission_request_id=${permissionRequestId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// get active permissions
function getActivePermissions(page, pageSize) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-active-permissions?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// get found active permissions
function getFoundActivePermissions(currentLanguage, keyword, page, pageSize) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-found-active-permissions?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// update specified active permission
function updateActivePermission(data) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/update-active-permission/?user_permission_id=${data.userPermissionId}&goc_email=${data.gocEmail}&supervisor=${data.supervisor}&supervisor_email=${data.supervisorEmail}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// delete specified active permission
function deleteActivePermission(userPermissionId) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/delete-active-permission/?user_permission_id=${userPermissionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// grant test permission to user
function grantTestPermission(usernames, test_ids, data) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/grant-test-permission/?usernames=${usernames}&test_ids=${test_ids}&expiry_date=${data.expiryDate}&test_order_number=${data.testOrderNumber}&staffing_process_number=${data.staffingProcessNumber}&department_ministry_code=${data.departmentMinistryCode}&is_org=${data.isOrg}&is_ref=${data.isRef}&billing_contact=${data.billingContact}&billing_contact_info=${data.billingContactInfo}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// get test permissions for a specific user
function getTestPermissions() {
  return async function() {
    let response = await fetch("/oec-cat/api/get-test-permissions/", {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// get test permission's financial data for a specific ta, test order number and test ID
function getTestPermissionFinancialData(testOrderNumber, testToAdminister) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-test-permission-financial-data/?test_order_number=${testOrderNumber}&test_to_administer=${testToAdminister}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// get all active test permissions
function getAllActiveTestPermissions(page, pageSize) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-all-active-test-permissions/?page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// get found active test permissions
function getFoundActiveTestPermissions(currentLanguage, keyword, page, pageSize) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-found-active-test-permissions/?current_language=${currentLanguage}&keyword=${keyword}&page=${page}&page_size=${pageSize}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// delete test permission
function deleteTestPermission(testPermissionId) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/delete-test-permission/?test_permission_id=${testPermissionId}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// update test permission
function updateTestPermission(testPermissionId, expiryDate) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/update-test-permission/?test_permission_id=${testPermissionId}&expiry_date=${expiryDate}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// get all users that have the specified permission
function getUsersBasedOnSpecifiedPermission(codename) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/get-users-based-on-specified-permission/?permission_codename=${codename}`,
      {
        method: "GET",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// Initial State
const initialState = {
  isEtta: false,
  isPpc: false,
  isTa: false,
  isScorer: false,
  currentHomePage: PATH.dashboard,
  permissionsPaginationPage: 1,
  permissionsPaginationPageSize: 25,
  permissionsKeyword: "",
  permissionsActiveSearch: false,
  testPermissionsPaginationPage: 1,
  testPermissionsPaginationPageSize: 25,
  testPermissionsKeyword: "",
  testPermissionsActiveSearch: false,
  triggerPopulateTestPermissions: false
};

// Reducer
const userPermissions = (state = initialState, action) => {
  switch (action.type) {
    case SET_PERMISSIONS:
      return {
        ...state,
        isEtta: action.permissions.isEtta,
        isPpc: action.permissions.isPpc,
        isTa: action.permissions.isTa,
        isScorer: action.permissions.isScorer
      };
    case SET_CURRENT_HOME_PAGE:
      return {
        ...state,
        currentHomePage: action.currentHomePage
      };
    case SET_PERMISSIONS_PAGINATION_PAGE:
      return {
        ...state,
        permissionsPaginationPage: action.permissionsPaginationPage
      };
    case SET_PERMISSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        permissionsPaginationPageSize: action.permissionsPaginationPageSize
      };
    case SET_PERMISSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        permissionsKeyword: action.permissionsKeyword,
        permissionsActiveSearch: action.permissionsActiveSearch
      };
    case SET_TEST_PERMISSIONS_PAGINATION_PAGE:
      return {
        ...state,
        testPermissionsPaginationPage: action.testPermissionsPaginationPage
      };
    case SET_TEST_PERMISSIONS_PAGINATION_PAGE_SIZE:
      return {
        ...state,
        testPermissionsPaginationPageSize: action.testPermissionsPaginationPageSize
      };
    case SET_TEST_PERMISSIONS_SEARCH_PARAMETERS:
      return {
        ...state,
        testPermissionsKeyword: action.testPermissionsKeyword,
        testPermissionsActiveSearch: action.testPermissionsActiveSearch
      };
    case SET_TRIGGER_POPULATE_ACTIVE_TEST_PERMISSIONS:
      return {
        ...state,
        triggerPopulateTestPermissions: action.triggerPopulateTestPermissions
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userPermissions;
export {
  initialState,
  getTestDefinitionVersionsCollected,
  getExistingPermissions,
  getUserPermissions,
  getPendingPermissions,
  getUserPendingPermissions,
  getAvailablePermissions,
  grantPermission,
  denyPermission,
  getActivePermissions,
  getFoundActivePermissions,
  updateActivePermission,
  deleteActivePermission,
  grantTestPermission,
  getTestPermissions,
  getTestPermissionFinancialData,
  getAllActiveTestPermissions,
  getFoundActiveTestPermissions,
  updatePermissionsState,
  updateCurrentHomePageState,
  updateCurrentPermissionsPageState,
  updatePermissionsPageSizeState,
  updateSearchActivePermissionsStates,
  updateCurrentTestPermissionsPageState,
  updateTestPermissionsPageSizeState,
  updateSearchActiveTestPermissionsStates,
  getUsersBasedOnSpecifiedPermission,
  updateTriggerPopulateTestPermissionsState,
  deleteTestPermission,
  updateTestPermission,
  resetPermissionsState
};
