// Action Types
const SET_USER_INFORMATION = "user/SET_USER_INFORMATION";
const UPDATE_PRI_OR_MILITARY_NBR = "user/UPDATE_PRI_OR_MILITARY_NBR";
const SET_LAST_PASSWORD_CHANGE = "user/SET_LAST_PASSWORD_CHANGE";
const SET_IS_USER_LOADING = "user/SET_IS_USER_LOADING";
const RESET_STATE = "RESET_STATE";

// Action Creators
const setUserInformation = (
  firstName,
  lastName,
  username,
  isSuperUser,
  lastPasswordChange,
  psrsAppID,
  primaryEmail,
  secondaryEmail,
  dateOfBirth,
  priOrMilitaryNbr
) => ({
  type: SET_USER_INFORMATION,
  firstName,
  lastName,
  username,
  isSuperUser,
  lastPasswordChange,
  psrsAppID,
  primaryEmail,
  secondaryEmail,
  dateOfBirth,
  priOrMilitaryNbr
});

const updatePriOrMilitaryNbr = priOrMilitaryNbr => ({
  type: UPDATE_PRI_OR_MILITARY_NBR,
  priOrMilitaryNbr
});

const setLastPasswordChange = lastPasswordChange => ({
  type: SET_LAST_PASSWORD_CHANGE,
  lastPasswordChange
});

const setIsUserLoading = isUserLoading => ({
  type: SET_IS_USER_LOADING,
  isUserLoading
});

const resetTestStatusState = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  firstName: "",
  lastName: "",
  username: "",
  isSuperUser: "",
  lastPasswordChange: "",
  psrsAppID: "",
  primaryEmail: "",
  secondaryEmail: "",
  dateOfBirth: "",
  priOrMilitaryNbr: "",
  isUserLoading: false
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_INFORMATION:
      //set the user information
      return {
        ...state,
        firstName: action.firstName,
        lastName: action.lastName,
        username: action.username,
        isSuperUser: action.isSuperUser,
        lastPasswordChange: action.lastPasswordChange,
        psrsAppID: action.psrsAppID,
        primaryEmail: action.primaryEmail,
        secondaryEmail: action.secondaryEmail,
        dateOfBirth: action.dateOfBirth,
        priOrMilitaryNbr: action.priOrMilitaryNbr
      };
    case UPDATE_PRI_OR_MILITARY_NBR:
      return {
        ...state,
        priOrMilitaryNbr: action.priOrMilitaryNbr
      };
    case SET_LAST_PASSWORD_CHANGE:
      return {
        ...state,
        lastPasswordChange: action.lastPasswordChange
      };
    case SET_IS_USER_LOADING:
      return {
        ...state,
        isUserLoading: action.isUserLoading
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default user;
export {
  setUserInformation,
  updatePriOrMilitaryNbr,
  setLastPasswordChange,
  setIsUserLoading,
  resetTestStatusState
};
