// Action Types
const SET_NOTEPAD_CONTENT = "notepad/SET_NOTEPAD_CONTENT";
const TOGGLE_NOTEPAD = "notepad/TOGGLE_NOTEPAD";
const RESET_STATE = "notepad/RESET_STATE";

// Action Creators
const setNotepadContent = notepadContent => ({
  type: SET_NOTEPAD_CONTENT,
  notepadContent
});
const toggleNotepad = () => ({
  type: TOGGLE_NOTEPAD
});
const resetNotepadState = () => ({ type: RESET_STATE });

// Initial State
const initialState = {
  notepadContent: null,
  isNotepadHidden: false
};

// Reducer
const notepad = (state = initialState, action) => {
  switch (action.type) {
    case SET_NOTEPAD_CONTENT:
      //set current test in local storage so that url change doesn't
      //remove the test type information
      return {
        ...state,
        notepadContent: action.notepadContent
      };
    case TOGGLE_NOTEPAD:
      //set current test in local storage so that url change doesn't
      //remove the test type information
      return {
        ...state,
        isNotepadHidden: !state.isNotepadHidden
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default notepad;
export { setNotepadContent, resetNotepadState, toggleNotepad };
