import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
const SET_CURRENT_TEST = "testStatus/SET_CURRENT_TEST";
const ACTIVATE_TEST = "testStatus/ACTIVATE_TEST";
const LOCK_TEST = "testStatus/LOCK_TEST";
const UNLOCK_TEST = "testStatus/UNLOCK_TEST";
const PAUSE_TEST = "testStatus/PAUSE_TEST";
const UNPAUSE_TEST = "testStatus/UNPAUSE_TEST";
const START_TEST = "testStatus/START_TEST";
const DEACTIVATE_TEST = "testStatus/DEACTIVATE_TEST";
const QUIT_TEST = "testStatus/QUIT_TEST";
const TIMEOUT_TEST = "testStatus/TIMEOUT_TEST";
const SET_PREVIOUS_STATUS = "testStatus/SET_PREVIOUS_STATUS";
const RESET_STATE = "testStatus/RESET_STATE";

// Action Creators
const setCurrentTest = (currentAssignedTestId, testId, startTime) => ({
  type: SET_CURRENT_TEST,
  currentAssignedTestId,
  testId,
  startTime
});
const activateTest = () => ({ type: ACTIVATE_TEST });
const lockTest = () => ({ type: LOCK_TEST });
const unlockTest = () => ({ type: UNLOCK_TEST });
const pauseTest = (pauseTime, pauseStartDate) => ({ type: PAUSE_TEST, pauseTime, pauseStartDate });
const unpauseTest = () => ({ type: UNPAUSE_TEST });
const startTest = (timeLimit, currentAssignedTestId, startTime) => ({
  type: START_TEST,
  timeLimit,
  currentAssignedTestId,
  startTime
});
const deactivateTest = () => ({ type: DEACTIVATE_TEST });
const quitTest = () => ({ type: QUIT_TEST });
const timeoutTest = () => ({ type: TIMEOUT_TEST });
const setPreviousStatus = previousStatus => ({ previousStatus, type: SET_PREVIOUS_STATUS });
const resetTestStatusState = () => ({ type: RESET_STATE });

function updateTestStatus(assignedTestId, status, previousStatus) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/update-test-status/?assigned_test_id=${assignedTestId}&test_status=${status}&previous_status=${previousStatus}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    return response;
  };
}

// Initial State
const initialState = {
  isTestActive: false,
  isTestLocked: false,
  isTestPaused: false,
  pauseTime: 0,
  pauseStartDate: null,
  startTime: null,
  timeLimit: null,
  currentAssignedTestId: null,
  currentTestId: null,
  isTestStarted: false,
  previousStatus: null
};

// Reducer
const testStatus = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_TEST:
      //set current test in local storage so that url change doesn't
      //remove the test type information
      return {
        ...state,
        currentAssignedTestId: action.currentAssignedTestId,
        currentTestId: action.testId,
        startTime: action.startTime
      };
    case ACTIVATE_TEST:
      return {
        ...state,
        isTestActive: true,
        isTestLocked: false,
        isTestPaused: false
      };
    case LOCK_TEST:
      return {
        ...state,
        isTestActive: true,
        isTestLocked: true,
        isTestPaused: false
      };
    case UNLOCK_TEST:
      return {
        ...state,
        isTestLocked: false
      };
    case PAUSE_TEST:
      return {
        ...state,
        isTestActive: true,
        isTestPaused: true,
        isTestLocked: false,
        pauseTime: action.pauseTime,
        pauseStartDate: action.pauseStartDate
      };
    case UNPAUSE_TEST:
      return {
        ...state,
        isTestPaused: false
      };
    case START_TEST:
      const limit = action.timeLimit === undefined ? null : action.timeLimit;

      return {
        ...state,
        isTestStarted: true,
        isTestActive: true,
        startTime: action.startTime,
        timeLimit: limit
      };
    case DEACTIVATE_TEST:
      // Ensure local storage is cleaned up once test is complete.
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentAssignedTestId: null,
        currentTestId: null,
        startTime: null
      };
    case QUIT_TEST:
      return {
        ...state,
        isTestLocked: false,
        isTestPaused: false
      };
    case TIMEOUT_TEST:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...state,
        isTestActive: false,
        isTestStarted: false,
        currentTestId: null,
        currentAssignedTestId: null,
        startTime: null
      };
    case SET_PREVIOUS_STATUS:
      return {
        ...state,
        previousStatus: action.previousStatus
      };
    case RESET_STATE:
      // Ensure local storage is cleaned up once the candidate is timed out of the test.
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default testStatus;
export {
  setCurrentTest,
  activateTest,
  lockTest,
  unlockTest,
  pauseTest,
  unpauseTest,
  startTest,
  initialState,
  deactivateTest,
  quitTest,
  timeoutTest,
  setPreviousStatus,
  resetTestStatusState,
  updateTestStatus
};
