import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

//update the notepad in the backened
function updateNotepad(assigned_test_id, notepadContent) {
  let url =
    "/oec-cat/api/update_notepad" +
    `?assigned_test_id=${assigned_test_id}&notepad=${encodeURIComponent(notepadContent)}`;

  return async function() {
    let tests = await fetch(url, {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        cache: "default"
      }
    });
    return await tests;
  };
}

//update the notepad in the backened
function getNotepad(assigned_test_id) {
  let url = "/oec-cat/api/get_notepad?assigned_test_id=" + assigned_test_id;

  return async function() {
    let tests = await fetch(url, {
      method: "GET",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        cache: "default"
      }
    });
    let response = await tests.json();
    return response;
  };
}

export { updateNotepad, getNotepad };
