import SessionStorage, { ACTION, ITEM } from "../SessionStorage";

// Action Types
export const SET_PERMISSION_REQUEST_DATE = "user_profile/SET_PERMISSION_REQUEST_DATE";
export const RESET_STATE = "user_profile/RESET_STATE";

// update permission request data state
const updatePermissionRequestDataState = permissionRequestData => ({
  type: SET_PERMISSION_REQUEST_DATE,
  permissionRequestData
});

// reset states
const resetUserProfileStates = () => ({ type: RESET_STATE });

// updating user's personal info
function updateUserPersonalInfo(data) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/update-user-personal-info/?first_name=${data.firstName}&last_name=${data.lastName}&username=${data.username}&email=${data.primaryEmail}&secondary_email=${data.secondaryEmail}&birth_date=${data.dob}&pri_or_military_nbr=${data.priOrMilitaryNbr}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    let responseJson = await response.json();
    return responseJson;
  };
}

// updating user's password
function updateUserPassword(data) {
  return async function() {
    let response = await fetch("/oec-cat/api/auth/password/", {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      },
      body: JSON.stringify(data)
    });
    // password updated successfully
    if (response.status === 204) {
      return response;
      // json response is useful for password errors, such as password too common, password too similar to username, first name, last name and email (response.status !== 204)
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// validating user credentials
function validateUserCredentials(data) {
  return async function() {
    let response = await fetch("/oec-cat/api/auth/token/login/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    });
    return response;
  };
}

// updating user's last password change time (date)
function updateUserLastPasswordChangeTime() {
  return async function() {
    let response = await fetch("/oec-cat/api/update-user-last-password-change-time/", {
      method: "POST",
      headers: {
        Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
        Accept: "application/json",
        "Content-Type": "application/json",
        cache: "default"
      }
    });
    let responseJson = await response.json();
    return responseJson;
  };
}

// sending permission request
function sendPermissionRequest(data, priOrMilitaryNbr, i) {
  return async function() {
    let response = await fetch(
      `/oec-cat/api/send-permission-request/?goc_email=${data.gocEmail}&pri_or_military_nbr=${priOrMilitaryNbr}&supervisor=${data.supervisor}&supervisor_email=${data.supervisorEmail}&rationale=${data.rationale}&permission_requested=${data.permissions[i]}`,
      {
        method: "POST",
        headers: {
          Authorization: "JWT " + SessionStorage(ACTION.GET, ITEM.AUTH_TOKEN),
          Accept: "application/json",
          "Content-Type": "application/json",
          cache: "default"
        }
      }
    );
    // if successful request
    if (response.status === 200) {
      return response;
      // else there is an error
    } else {
      let responseJson = await response.json();
      return responseJson;
    }
  };
}

// Initial State
const initialState = {
  priOrMilitaryNbr: null,
  permissionRequestData: {
    gocEmail: "",
    supervisor: "",
    supervisorEmail: "",
    rationale: "",
    permissions: [],
    isValidForm: false
  }
};

// Reducer
const userProfile = (state = initialState, action) => {
  switch (action.type) {
    case SET_PERMISSION_REQUEST_DATE:
      return {
        ...state,
        permissionRequestData: {
          gocEmail: state.permissionRequestData.gocEmail,
          supervisor: state.permissionRequestData.supervisor,
          supervisorEmail: state.permissionRequestData.supervisorEmail,
          rationale: state.permissionRequestData.rationale,
          permissions: state.permissionRequestData.permissions,
          isValidForm: state.permissionRequestData.isValidForm
        }
      };
    case RESET_STATE:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

export default userProfile;
export {
  updateUserPersonalInfo,
  updateUserPassword,
  validateUserCredentials,
  updateUserLastPasswordChangeTime,
  updatePermissionRequestDataState,
  resetUserProfileStates,
  sendPermissionRequest
};
